<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CarStatus extends Model{	

	protected $table = 'car_status';
	public $timestamps = true;
	protected $fillable = ['car_id','dealer_id','is_sold','is_inactive','is_featured'];   
}
