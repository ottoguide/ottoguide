<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model{	
	protected $table = 'subscriber_list';
	public $timestamps = true;
	protected $fillable = ['dealer_id','package_id','card_type','card_number','card_expiry','security_code','card_holder_name'];   
}
