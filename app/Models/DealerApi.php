<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class DealerApi extends Model
{
	protected $table = 'dealer_api';
	protected $primaryKey = 'dealer_id';
	public $timestamps = true;
	protected $fillable = ['dealer_id', 'dealer_name', 'dealer_email','dealer_street','password',
		'dealer_city','dealer_country',	'dealer_zip','dealer_phone','login_phone','dealer_website',
	   	'registration_pin'
	];   
}
