<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class RBO extends Model{	
	protected $table = 'request_best_offer';
	public $timestamps = true;
	protected $fillable = ['car_id','user_id','dealer_id','dealer_quote_offer','user_quote_offer','is_email_send','is_sms_send','is_rbo_active'];   
}
