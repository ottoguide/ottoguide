<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class RBO_QUOTE extends Model{	
	protected $table = 'rbo_dealer_quote';
	public $timestamps = true;
	protected $fillable = ['car_price','dealer_price_description','dealer_price_values','doc_fees_description','doc_fees_values','rebate_discount_description','rebate_discount_values','sales_price','customer_options','customer_offer','action','status','sale_price','expiry_date'];   
}
