<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class QuizQuery extends Model{	
	protected $table = 'quiz_query';
	//public $timestamps = false;
	protected $fillable = ['user_id','session_id','quiz_query'];
}
