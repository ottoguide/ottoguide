<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ContactRequest extends Model{	
	protected $table = 'contact_request';
	public $timestamps = false;
	protected $fillable = ['client_id','dealer_id','car_id','chat_request','email_request','call_request','sms_request'];   
}
