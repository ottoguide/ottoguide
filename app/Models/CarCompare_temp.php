<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CarCompare_temp extends Model{	

	protected $table = 'car_compare_temp';
	public $timestamps = true;
	protected $fillable = ['session_carid'];   
}
