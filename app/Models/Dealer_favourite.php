<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dealer_favourite extends Model
{
	protected $table = 'dealer_favourite';
	
	public $timestamps = true;
	
	protected $fillable = [	'client_id','dealer_id'];
}
