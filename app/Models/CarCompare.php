<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CarCompare extends Model{	

	protected $table = 'car_campare';
	public $timestamps = true;
	protected $fillable = ['car_id','client_id'];   
}
