<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Sms extends Model{	
	protected $table = 'sms';
	public $timestamps = true;
	protected $fillable = ['client_id','dealer_id','sms','status'];   
}
