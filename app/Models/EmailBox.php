<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class EmailBox extends Model{	
	protected $table = 'email_box';
	public $timestamps = true;
	protected $fillable = ['car_id','client_id','dealer_id','subject','message','services','message_by','reply_to'];   
}
