<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class RBO_QUOTE_CUSTOMER extends Model{	
	protected $table = 'rbo_user_quote';
	public $timestamps = true;
	protected $fillable = ['car_price','user_price_options','doc_fees','rebate_discount','sales_price','customer_options','customer_offer','action','status','sale_price','expiry_date'];   
}
