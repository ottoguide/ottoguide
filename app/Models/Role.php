<?php
namespace App\Models;
use Zizaco\Entrust\EntrustRole;
use Illuminate\Support\Facades\Config;
class Role extends EntrustRole
{
	protected $fillable = [
		'name', 'display_name', 'description'
	];

}