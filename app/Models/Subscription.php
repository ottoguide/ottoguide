<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model{	
	protected $table = 'subscription_packages';
	public $timestamps = false;
	protected $fillable = ['title','featured_ad','expire_days','description','chats','lease_purchase','is_active','updated_on'];   
}
