<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Offers extends Model{	
	protected $table = 'offers';
	public $timestamps = false;
	protected $fillable = ['client_id','dealer_id','car_id','status'];   
}
