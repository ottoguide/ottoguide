<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client_favourite extends Model
{
	protected $table = 'client_favourite';
	
	public $timestamps = true;
	
	protected $fillable = [	'client_id','json_data'];
}
