<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Search_cache extends Model
{
	protected $table = 'search_cache';

	protected $fillable = ['query_url', 'result', 'expire_on' ];   
}
