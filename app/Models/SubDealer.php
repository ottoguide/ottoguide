<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SubDealer extends Model
{
	protected $table = 'dealer_subaccount';
	public $timestamps = true;
	protected $fillable = ['name', 'email', 'phone','dealer_id','password'];   
}
