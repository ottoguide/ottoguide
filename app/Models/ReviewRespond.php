<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ReviewRespond extends Model{	

	protected $table = 'dealer_views';
	public $timestamps = true;
	protected $fillable = ['title','message'];   
}
