<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Dealer extends Model
{
	protected $table = 'dealer';
	protected $primaryKey = 'dealer_id';
	public $timestamps = true;
	protected $fillable = ['dealer_id', 'dealer_name', 'dealer_email','dealer_other_emails','dealer_contact_name','dealer_other_phone','dealer_street','password',
		'dealer_city','dealer_country',	'radius', 'dealer_zip','dealer_phone','login_phone','dealer_website',
	   	'registration_pin'
	];   
}
