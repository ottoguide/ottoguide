<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AdminReviewControl extends Model{	

	protected $table = 'admin_review_control';
	public $timestamps = true;
	protected $fillable = ['car_id','client_id','dealer_id','message_report','report_reply','block_vehicle','block_current_dealer','block_all_dealers','reason'];   
}
