<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserUnblockRequest extends Model{	

	protected $table = 'user_unblock_request';
	public $timestamps = true;
	protected $fillable = ['user_id','subject','message','status'];   
}
