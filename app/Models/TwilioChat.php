<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TwilioChat extends Model{	
	protected $table = 'twilio_chat';
	public $timestamps = true;
	protected $fillable = ['car_id','user_id','dealer_id','notify','chat_channel','is_block_current','is_block_all'];   
}
