<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class DealerSearchCar extends Model
{
	protected $table = 'dealer_car_search';
	protected $primaryKey = 'id';
	public $timestamps = true;
	protected $fillable = ['dealer_id', 'car_id'];   
}
