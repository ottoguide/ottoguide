<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class rbo_dealer_useroffer_email_send extends Mailable
{
    use Queueable, SerializesModels;

	public $data;
	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        // 
	    $this->data = $data;
    }
	
    /**
     * Build the message.
     *
     * @return $this
     */
	 
    public function build()
    {
	return $this->from('admin@ottoguide.com', 'OttoGuide.com')
    ->subject('Request Best Offer')
    ->view('emails.rbouseroffer')->with(['data', $this->data]);
}


}