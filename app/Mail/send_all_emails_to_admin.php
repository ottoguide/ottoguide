<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class send_all_emails_to_admin extends Mailable
{
    use Queueable, SerializesModels;

	public $data;
	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        // 
	    $this->data = $data;
    }
	
    /**
     * Build the message.
     *
     * @return $this
     */
	 
    public function build()
    {
	return $this->from('admin@ottoguide.com', 'www.ottoguide.com')
    ->subject('Request Best Offer')
    ->view('emails.alladminemails')->with(['data', $this->data]);
}


}