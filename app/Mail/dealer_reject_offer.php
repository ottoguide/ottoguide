<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class dealer_reject_offer extends Mailable
{
    use Queueable, SerializesModels;

	public $data;
	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        // 
	    $this->data = $data;
    }
	
    /**
     * Build the message.
     *
     * @return $this
     */
	 
    public function build()
    {
	return $this->from('admin@ottoguide.com', 'OttoGuide.com')
    ->subject('Your RBO Offer Rejected')
    ->view('emails.dealerrejectedoffer')->with(['data', $this->data]);
}


}