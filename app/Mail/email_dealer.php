<?php
namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class email_dealer extends Mailable
{
    use Queueable, SerializesModels;
	public $data;
	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
		$this->data = $data;
    }
	
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$view =View::make('emails.register_dealer',$this->data);
        //$html = $view->render();        
    	return $this->from('admin@ottoguide.com', 'Otto Guide Dealer Car Search')
        ->subject('OttoGuide Dealer Car Search')
        ->view('emails.dealer_signup')->with(['data', $this->data]);
    }
}