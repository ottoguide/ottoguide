<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class contact_us_submit extends Mailable
{
    use Queueable, SerializesModels;

	public $data;
	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        // 
		$this->data = $data;
    }
	
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
	return $this->from('postman@ottoguide.com', 'OTTO Guide Admin')
    ->subject('User Contact Us')
    ->view('emails.usercontactus')->with(['data', $this->data]);
}


}