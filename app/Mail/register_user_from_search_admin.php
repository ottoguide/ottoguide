<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class register_user_from_search_admin extends Mailable
{
    use Queueable, SerializesModels;

	public $data;
	
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        // 
		$this->data = $data;
    }
	
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	return $this->from('noreply@ottoguide.com', 'Otto Guide')
        ->subject('OttoGuide User Signup')
        ->view('emails.register_user_from_search_admin')->with(['data', $this->data]);
    }
}