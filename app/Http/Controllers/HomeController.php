<?php

namespace App\Http\Controllers;

use App\Helpers\Common;
use App\Models\DealerSearchCar;
use App\Models\Search_cache;
use App\Models\SearchCarViews;
use App\Models\Dealer;
use App\Models\DealerApi;
use App\Models\DealerCron;
use App\Models\Role;
use DB;
use Charts;
use App\Models\role_user;
use Illuminate\Http\RedirectResponse;
use App\Models\Questions;
use App\Models\QuizQuery;
use App\Helpers\ApiCaller;
use Mail;
use App\Mail\contact_us_submit;
use App\Mail\affiliate_submit;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Cookie;
use Response;


define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");



class HomeController extends Controller {
	
	
	public static function run_api($url)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Host: marketcheck-prod.apigee.net"
            ),
        ));

        $response = @curl_exec($curl);
        $err = @curl_error($curl);

        curl_close($curl);

        if ($err)
            return 'fail';

        return $response;
    }


	public function index() {
	
	session()->forget('car_type');
	
	if(session::has('vdp_session')){
		
	$create_temp_session = [
	'session_id'=> Session::getId(),
	'session_car_id'=> Session::get('vdp_session')['session_car_id'],
	'session_status'=> Session::get('vdp_session')['session_status'],
	'session_radius'=> Session::get('vdp_session')['session_radius'],
	'session_latitude'=>  Session::get('vdp_session')['session_latitude'],	
	'session_longitude'=> Session::get('vdp_session')['session_longitude'],
	];	
	
	session()->forget('vdp_session');	
	
	$check_auth = Auth::user()->id;

	if($create_temp_session['session_id'] == Session::getId() && $check_auth ==''){
	return view('web_pages/welcome')->with(array('home' => true));
	}
	
	else{
	return redirect(url('car/detail').'/'.$create_temp_session['session_car_id'].'?filter_search=filter&radius='.$create_temp_session['session_radius'].'&latitude='.$create_temp_session['session_latitude'].'&longitude='.$create_temp_session['session_longitude']);	
	}
	
	}
	
	if(session::has('srp_session')){
		
	$create_temp_session = [
	'session_id'=> Session::getId(),
	'session_car_id'=> Session::get('srp_session')['session_car_id'],
	'session_status'=> Session::get('srp_session')['session_status'],
	'session_radius'=> Session::get('srp_session')['session_radius'],
	'session_latitude'=>  Session::get('srp_session')['session_latitude'],	
	'session_longitude'=> Session::get('srp_session')['session_longitude'],
	];
	
	session()->forget('srp_session');	
	
	$check_auth = Auth::user()->id;

	if($create_temp_session['session_id'] == Session::getId() && $check_auth ==''){
	return view('web_pages/welcome')->with(array('home' => true));
	}
	
	else{
	return redirect(url('car/detail').'/'.$create_temp_session['session_car_id'].'?filter_search=filter&radius='.$create_temp_session['session_radius'].'&latitude='.$create_temp_session['session_latitude'].'&longitude='.$create_temp_session['session_longitude']);	
	}
	
	}
	
	if(session::has('search_button_session')){
		
	$temp_search_buttons_session = [
	'session_id'=> Session::getId(),
	'session_car_title'=> Session::get('search_button_session')['session_car_title'],
	'session_car_id'=> Session::get('search_button_session')['session_car_id'],
	'session_car_vin'=> Session::get('search_button_session')['session_car_vin'],
	'session_status'=> Session::get('search_button_session')['session_status'],
	'session_radius'=> Session::get('search_button_session')['session_radius'],
	'session_latitude'=>  Session::get('search_button_session')['session_latitude'],	
	'session_longitude'=> Session::get('search_button_session')['session_longitude'],
	];	
		
	session()->forget('search_button_session');		
    	
	$check_auth = Auth::user()->id;

	if($temp_search_buttons_session['session_id'] == Session::getId() && $check_auth ==''){
	return view('web_pages/welcome')->with(array('home' => true));
	}
	
	else{
	return redirect(url('car/detail').'/'.$temp_search_buttons_session['session_car_id'].'?filter_search=filter&radius='.$temp_search_buttons_session['session_radius'].'&latitude='.$temp_search_buttons_session['session_latitude'].'&longitude='.$temp_search_buttons_session['session_longitude']);	
	}
		
	}
	
	return view('web_pages/welcome')->with(array('home' => true));
	}
	

	
	public function create_vdp_session(Request $request) {
	
	if(session::has('srp_session')){
	$request->session()->forget('srp_session');	
	}	
	
	$store_session = [
	'session_id'=> Session::getId(),
	'session_car_id'=> $request->get('session_car_id'),
	'session_status'=> $request->get('session_status'),
	'session_radius'=> $request->get('session_radius'),
	'session_latitude'=> $request->get('session_latitude'),	
	'session_longitude'=> $request->get('session_longitude'),
	];
	
	$request->session()->put('vdp_session', $store_session);

	return redirect(url('/login'));

	}
	
	public function create_srp_session(Request $request) {
		
	
	if(session::has('vdp_session')){
	$request->session()->forget('vdp_session');	
	}	
	
	$store_session = [
	'session_id'=> Session::getId(),
	'session_car_id'=> $request->get('session_car_id'),
	'session_status'=> $request->get('session_status'),
	'session_radius'=> $request->get('session_radius'),
	'session_latitude'=> $request->get('session_latitude'),	
	'session_longitude'=> $request->get('session_longitude'),
	];

	$request->session()->put('srp_session', $store_session);	
	
	return redirect(url('/login'));	
	}
	
	
	
	// SEARCH BUTTON SESSION
	
	public function create_search_button_session (Request $request){
	
	$id = $request->get('session_car_id');
	$car_info = ApiCaller::get_car_detail($id);
 
	$store_button_session = [
	
	'session_id'=> Session::getId(),
	'session_car_id'=> $request->get('session_car_id'),
	'session_car_vin'=> $request->get('session_car_vin'),
	'session_car_title'=> $request->get('session_car_title'),
	'session_status'=> $request->get('session_status'),
	'session_radius'=> $request->get('session_radius'),
	'session_latitude'=> $request->get('session_latitude'),	
	'session_longitude'=> $request->get('session_longitude'),
	'session_make'=> $car_info->build->make ?? '',
	'session_modal'=> $car_info->build->model ?? '',
	'session_trim'=> $car_info->build->trim ?? '',
	'session_body'=> $car_info->build->body_type ?? '',
	'session_vehicle_type'=> $car_info->build->vehicle_type ?? '',
	'session_miles'=> $car_info->miles ?? '',
	'session_inventory_type'=> $car_info->inventory_type ?? '',
	'session_color'=> $car_info->exterior_color ?? '',
	'session_dealer_id'=> $car_info->dealer->id ?? '',
	'session_dealer_name'=> $car_info->dealer->name ?? '',
	'session_dealer_city'=> $car_info->dealer->city ?? '',
	'session_dealer_street'=> $car_info->dealer->street ?? '',
	'session_dealer_state'=> $car_info->dealer->state ?? '',
	'session_dealer_lat'=> $car_info->dealer->latitude ?? '',	
	'session_dealer_long'=> $car_info->dealer->longitude ?? '',
	'session_dealer_zip'=> $car_info->dealer->zip ?? '',
	'session_dealer_phone'=> $car_info->dealer->phone ?? '',
	'session_dealer_website'=> $car_info->dealer->website ?? '',
	
	];	
	
	$request->session()->put('search_button_session', $store_button_session);

	return redirect(url('/login'));

	}
	
	
	public function who_we_are(){
	
	/* $get_dealer_phones = DB::table('dealer_api')->get();
	
	foreach($get_dealer_phones as $get_dealer_phone){
	
	$remove1 = str_replace(array("(",")"," ","-"),array("","","",""),$get_dealer_phone->dealer_phone);
	$remove1 = substr_replace($remove1, '',10);	
	
	DealerApi::where('dealer_id',$get_dealer_phone->dealer_id)->update(array('dealer_email' => $get_dealer_phone->dealer_id.'@ottoguide.com')); 
 
	}  */

	return view('web_pages/who_we_are');
	}

	public function how_it_work() {
		return view('web_pages/how_it_work');
	}
	
	
	public function dealership() {
		return view('web_pages/dealership');
	}
	
	public function contact_us(){
		return view('web_pages/contact_us');
	}

	public function contact_us_submit(Request $request){
		$name = $request->user_name; 
		$email = $request->user_email;
		$phone = $request->user_phone;
		$zipcode = $request->user_zip;
		$message = $request->user_message;

		$validator = Validator::make($request->all(), [
			'user_email' => 'required|email',
			'user_name' => 'required|string|max:50',
			'user_zip' => 'required|numeric',
		]);
		 
		if ($validator->fails()) {
			 Session::flash('error', $validator->messages()->first());
			 return redirect()->back()->withInput();
		}
	
		$data = [
			'name' => $name,
			'email' => $email,
			'phone' => $phone,
			'zipcode' => $zipcode,
			'message' => $message,
			];

		Mail::to("ottoadmin1@ottoguide.com")->send(new contact_us_submit($data));

		return redirect(url('/contact_us_thankyou.html'));
	}

	public function contact_us_thankyou(){
		return view('web_pages/contact_us_thankyou');
	}

	public function affiliate(){
		return view('web_pages/affiliate');
	}

	public function affiliate_submit(Request $request){
		$affiliatename = $request->affiliate_name;
		$affiliateemail = $request->affiliate_email;
		$affiliatephone = $request->affiliate_phone;
		$affiliatezipcode = $request->affiliate_zipcode;
		$affiliatesales = $request->affiliate_sales;
		$affiliateindustry = $request->affiliate_industry;
		$affiliatemessage = $request->affiliate_message;
		

		$validator = Validator::make($request->all(), [
			'affiliate_name' => 'required|string|max:50',
			'affiliate_email' => 'required|email',
			'affiliate_phone' => 'required|numeric|size:11',
			'affiliate_zipcode' => 'required|numeric',
		]);
		 
		if ($validator->fails()) {
			 Session::flash('error', $validator->messages()->first());
			 return redirect()->back()->withInput();
		}
	
		$data = [
			'affiliate_name' => $affiliatename,
			'affiliate_email' => $affiliateemail,
			'affiliate_phone' => $affiliatephone,
			'affiliate_zipcode' => $affiliatezipcode,
			'affiliate_sales' => $affiliatesales,
			'affiliate_industry' => $affiliateindustry,
			'affiliate_message' => $affiliatemessage,
			];

		Mail::to("ottoadmin1@ottoguide.com")->send(new affiliate_submit($data));

		return redirect(url('/affiliate_thankyou.html'));
	}

	public function affiliate_thankyou(){
		return view('web_pages/affiliate_thankyou');
	}

	public function career() {
		
	  
	  //******** Removed Cookie From Browser ******
	   
	   /* $cookie = Cookie::forget('otto_data');
       return Response::make('cookie has bee deleted')->withCookie($cookie); */
	  
		
	// ******** SET COOKIE IN TO BROWSER *********
		
		/* $set_data_cookie = array(
		'name'=>  'misbah uddin',
		'designation' =>  'web developer',
		'subject'=> 'save cookies in browser'
		);
		
	    $cookie_data = json_encode($set_data_cookie);	
		
		setcookie('otto_data', $cookie_data, time()+60*60*24*2);
    
		$get_cookie = json_decode($_COOKIE['otto_data'], true);

		print_r($get_cookie['name']);  */

        
		return view('web_pages/career');
	
	}

	public function search_option(){
		return view('web_pages/search_options');
	}


   
	public function check_user_activation(Request $request){
		
	$check_email = $request->input('check_email');
		
	$check_activation = DB::table('users')->where('email',$check_email)->where('verified',1)->get();
	
	if(count($check_activation)){
		
		echo "Activated";	
		exit;
	
	}
	
	else{
		echo "Not-Activated";
		exit;
	}
		
	die;	
		
	}
	

		//GET DEALER FROM API

		  public function get_dealers(){
			
			$get_dealer_count = DB::table('dealer_api')->count()+1;
			
			$limit='50';
			
			$url = API_URL . "dealers/car/?api_key=" . API_KEY . '&start=' . $get_dealer_count . '&rows=' . $limit;
			
			$response = self::run_api($url);
				
			$dealer_list = json_decode($response); 
			
		    foreach($dealer_list->dealers as $dealers){
			
			$dealerRecord =  new DealerApi();
			
			$dealerRecord->dealer_id = $dealers->id;
			
			$dealerRecord->dealer_name = (isset($dealers->seller_name) ? $dealers->seller_name : '');
			
			$dealerRecord->dealer_email = $dealers->id.'@ottoguide.com';//(isset($dealers->seller_name) ? $dealers->seller_name : '');
            
			$dealerRecord->dealer_latitude = (isset($dealers->latitude) ? $dealers->latitude : '');
            
			$dealerRecord->dealer_longitude = (isset($dealers->longitude) ? $dealers->longitude : '');
            
			$dealerRecord->dealer_country = (isset($dealers->country) ? $dealers->country : '');
            
			$dealerRecord->dealer_website = (isset($dealers->inventory_url) ? $dealers->inventory_url : ''); 
			
			$dealerRecord->dealer_phone = (isset($dealers->seller_phone) ? $dealers->seller_phone : '');
            
			$dealerRecord->dealer_street = (isset($dealers->street) ?$dealers->street : '');
            
			$dealerRecord->dealer_city = (isset($dealers->city) ? $dealers->city : '');
			
			$dealerRecord->dealer_state = (isset($dealers->state) ? $dealers->state : '');
            
			$dealerRecord->dealer_zip = (isset($dealers->zip) ? $dealers->zip : '');
			
			$dealerRecord->save();	

		  }
			
		  return redirect('dealer-list-api.html')->with('success', '50 more dealers has been added in List');

		
		   }
	 
	
	
	
	
	/* 	public function get_dealers(){
			
			$get_dealer_count = DB::table('dealer_api')->limit(50)->count()+1;
			
			$limit='50';
			
			$url = API_URL . "dealers/?api_key=" . API_KEY . '&start=' . $get_dealer_count . '&rows=' . $limit;
			
			$response = self::run_api($url);
			
			$dealer_list = json_decode($response); 
			
			
			foreach($dealer_list->dealers as $dealers){
				
			DealerApi::where('dealer_id',$dealers->id)->update(array('dealer_state' => $dealers->state)); 
				
		   }
			
		} */
		
	
	public function quiz_list(Request $request){

	 $get_quizes = DB::table('quiz')->where('is_active',1)->get();

	  return view('web_pages/quiz_home',compact('get_quizes'));
	}

	public function quiz_show(Request $request){

	  $quiz_id = decrypt($request->get('quiz_id'));
	  $quiz = DB::table('quiz')->where('id',$quiz_id)->first();
	  $quiz_routes = DB::table('quiz_routes')->where('quiz_id',$quiz_id)->get();
	  $get_quiz_show = DB::table('quiz_detail')->where('quiz_id',$quiz_id)->orderBy('id','ASC')->get();

	  return view('web_pages/faq_search',compact('get_quiz_show','quiz','quiz_routes'));
	}


    public function check_route(Request $request){
		
        $quiz_id = $request->quiz_id;
        $question_id = $request->question_id;
        $answer = $request->answer;

        $quiz_routes = DB::table('quiz_routes')
            ->where('quiz_id',$quiz_id)
            ->where('select_question',$question_id)
            ->where('select_response',$answer)
            ->first();


        if(!empty($quiz_routes)){
            return json_encode(array('status'=>true,'next_question'=>$quiz_routes->next_question));
        }
        else{
            return json_encode(array('status'=>false,'next_question'=>''));
        }

    }



      public function quiz_search_submit(Request $request){


	  $lat = $request->input('lat');
	  $long = $request->input('long');	

	  $Qanswers = $request->input('answer');

	  $query='';

	  foreach($Qanswers as $key=> $answers){

	  $get_questions = DB::table('quiz_detail')->where('question_id', $key)->where('is_query',1)->get();

      foreach($get_questions as $key=> $get_param){
		  
	  if(strtolower($get_param->query_param) =='doors'){ 
		$query .= 'door='. $answers.'&';
	  }
		else{
			$query .= strtolower($get_param->query_param) .'='. $answers.'&';
		}

	  

	  }

	  }

	  $quiz_query =  new QuizQuery();

	  if(empty($get_param->quiz_id)){
	  return redirect(url('/search-car.html?filter_search=filter&car_type=all&radius=10&latitude='.$lat.'&longitude='.$long.''))->with('error','Sorry not found your desire result');
	  }
	   
	  if(Auth::id()){

	  $quiz_query->user_id = Auth::id();

	  $quiz_query->quiz_id = (isset($get_param->quiz_id) ? $get_param->quiz_id :'');

	  $quiz_query->quiz_query = $query;

	  $quiz_query->quiz_stats = json_encode($Qanswers);

	  $quiz_query->save();

	  }

	  else{
		  
	   $session_data =[
	   'session_quiz_id' => $get_param->quiz_id,
	   'session_user_id' => Session::getId(),
	   'session_quiz_query' => $query,
	   'session_Qanswer' => json_encode($Qanswers)
	   ];
	   
	   $request->session()->put('quiz_session', $session_data);
	  
	   return redirect(url('/login'));

	  }   

	  return redirect(url('/search-car.html?'.$query.'filter_search=filter&car_type=all&radius=10&latitude='.$lat.'&longitude='.$long.''))->with('message','Your desire result has been found successfully');

	}

	
	
	 public function quiz_session_submit(Request $request){
		 
	  $lat = (isset(Session::get('geo_location_session')['latitude']) ? Session::get('geo_location_session')['latitude'] :'');
	  $long = (isset(Session::get('geo_location_session')['longitude']) ? Session::get('geo_location_session')['longitude'] :'');

		
	if(Auth::id()){
			
	  $quiz_query =  new QuizQuery();

	  $quiz_query->user_id = Auth::id();

	  $quiz_query->quiz_id = Session::get('quiz_session')['session_quiz_id'];

	  $quiz_query->quiz_query = Session::get('quiz_session')['session_quiz_query'];

	  $quiz_query->quiz_stats = Session::get('quiz_session')['session_Qanswer'];

	  $quiz_query->save();	
	  
	  
	 $temp_session = Session::get('quiz_session')['session_quiz_query'];	
	 
	 $request->session()->put('temp_session', $temp_session);
	  
	 $request->session()->forget('quiz_session');	
	  
	 return redirect(url('/search-car.html?'.Session::get('temp_session').'filter_search=filter&car_type=all&radius=10&latitude='.$lat.'&longitude='.$long.''))->with('message','Your desire result has been found successfully');

	 }
	
	 else{
		
	 return redirect(url('/login'));
	 
	 } 

	
	 }
	
	
	public function faq(){
		$response = ApiCaller::search_car();
		$result = json_decode($response);
		return view('web_pages/faq');
	}


	public function search_car(Request $request){
		
		$total="";
		$all_cars=array();

		if($request->has('filter_search')){

			$cache_result = Search_cache::where('query_url',$request->getQueryString())->count();

			if($cache_result == 0){
				$response = ApiCaller::search_car();
				$result = json_decode($response);

				if(isset($result->listings)) {
					$searchCache = new Search_cache;

					$searchCache->result = $response;
					$searchCache->query_url = $request->getQueryString();
					$searchCache->expire_on =date('Y-m-d', strtotime("+2 days"));
					$searchCache->save();

					$all_cars=$result->listings;
					$total = $result->num_found;
				}
			} else {
				$result_array = Search_cache::where('query_url',$request->getQueryString())->get()->first();

				$result_array= $result_array->result;
				$result = json_decode($result_array);
				$all_cars = $result->listings;
				$total = $result->num_found;
			}

			$page = LengthAwarePaginator::resolveCurrentPage();
			$limit= ($request->input('per_page') ? $request->input('per_page') : 10);
			$page = ($request->input('page') ? $request->input('page') : 1 );

			$car_after_paginate = new LengthAwarePaginator($all_cars, $total , $limit, $page, [
				'path' => LengthAwarePaginator::resolveCurrentPath(),
				'query' => $request->query(),
			]);

			$dealer_model = new Dealer();
			$dealer_car_model = new DealerSearchCar();
			$SearchCarViews_model = new SearchCarViews();
			$dealer_cron = new DealerCron();
		}


		return  view('web_pages.search')->with(array(
			'all_cars' => (isset($car_after_paginate) ? $car_after_paginate : array()),
			'dealer_model' => (isset($dealer_model) ? $dealer_model : ''),
			'dealer_car_model' => (isset($dealer_car_model) ? $dealer_car_model : ''),
			'SearchCarViews_model' => (isset($SearchCarViews_model) ? $SearchCarViews_model : ''),
			'dealer_cron' => (isset($dealer_cron) ? $dealer_cron : ''),
			'zip' => session::get('city').' '.session::get('country')
		));
	}

	//SEARCH CAR
	function search_car_monthly_views(Request $request){
		return Datatables::of(SearchCarViews::all())
			->make(true);
	}


	public function car_detail($id){
		$car_detail = ApiCaller::get_car_detail($id);
		
		// Save car details
		$SearchCarViews = new SearchCarViews;
		$SearchCarViews->car_id = 	$car_detail->id;
		$SearchCarViews->make = 	$car_detail->build->make;
		$SearchCarViews->model = 	$car_detail->build->model;
		$SearchCarViews->dealer_id = $car_detail->dealer->id;
		$SearchCarViews->search_type = "detail";
		$SearchCarViews->save();

		return view('web_pages/car_detail')->with(array('car_detail' => $car_detail));
	}
}