<?php
namespace App\Http\Controllers\Dealer;
use App\Http\Controllers\Controller;
use App\Http\Middleware\DealerAuth;
use Illuminate\Http\Request;
use App\Helpers\Common;
use Illuminate\Support\Facades\Validator;
use App\Models\Dealer;
use App\Models\SearchCarViews;
use App\Helpers\ApiCaller;
use App\Models\CarRating;
use App\Models\EmailBox;
use App\Models\EmailBoxDealer;
use App\Models\ContactRequest;
use App\Models\ReviewRespond;
use App\Models\DealerSearchCar;
use App\Mail\register_dealer;
use App\Mail\all_customers_emails;
use Illuminate\Pagination\LengthAwarePaginator;
use Mail;
use DB;
use Session;
use Yajra\Datatables\Datatables;

define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class Mailbox extends Controller {

    public function mail_box_page(){

         $dealer_id = \Session::get('dealer_id');
		
        // Validation to check if dealer subscribed to pricing tool addon
		
        $dealer_data = DB::table('dealer')->where('dealer_id', $dealer_id)->first();
		
        if($dealer_data->email_lead_addon == 0){
            Session::flash('message', 'You are not subscribed to Email');
            Session::flash('alert-class', 'alert-danger');
            return redirect('dealer/portal');
        }


	 return view('dealer/mail-box');	
		
	}	

	
 	public function mail_box_table(Request $request){										

	
	$email_list = DB::table('email_box');
	
	if($_GET['email']=='inbox'){
		
	$email_list->where('dealer_id',Session()->get('dealer_id'))->where('message_by','client')->where('temp_delete_by_dealer',0)->where('is_delete_by_dealer',0)->where('reply_to',null)->orderBy('email_box.id','DESC');
	
	}	
	
	if($_GET['email']=='sent'){
	
	$email_list->where('dealer_id',Session()->get('dealer_id'))->where('message_by','dealer')->where('temp_delete_by_dealer',0)->where('is_delete_by_dealer',0)->where('dealer_first_reply',1)->orderBy('email_box.id','DESC');
	
	}	
	
	if($_GET['email']=='trash'){
	
	$email_list->where('dealer_id',Session()->get('dealer_id'))->where('temp_delete_by_dealer',1)->where('is_delete_by_dealer',0)->orderBy('email_box.id','DESC');
	
	}	
	
	 $email_list->join('users','email_box.client_id','=','users.id')
	
	->select('email_box.id as email_id', 'users.id as user_id','dealer_id','client_id',
	 'image','subject','name','message','message_by','reply_to','read_by_dealer',
	 'email_box.created_at as email_date','users.created_at as user_date')    	
	->get(); 
	
	 return Datatables::of($email_list)
		
	 ->addIndexColumn()
	
	
	->addColumn('action', function ($row) {
		      
			$html = '
			<i class="fa fa-star is_read'.$row->read_by_dealer.'" aria-hidden="true"></i>';
				return "<center>{$html}</center>";
			},NULL)	
	
	
	->addColumn('username', function ($row) {
		
		 $id = $row->email_id;
		 if (!is_null($row->reply_to)){
		 $id = $row->reply_to;
		 }
		 
		 $html = '<a href="'.route('message/read', ['email_id' => encrypt($id)]).'">'.$row->name."</a>";
	
		 return "{$html}";

		},NULL)	
		
	
		->addColumn('email', function ($row) {
			
		 $count_reply = EmailBox::where('reply_to',$row->email_id)->where('read_by_dealer',0)->where('message_by','client')->get();
	
		 if(!empty($count_reply)){
		 $new_message = "";	 
		 }
		 else{
		 $new_message = ""; 
		 }	
	     
		 
		 $message_reply = EmailBox::where('reply_to',$row->email_id)->where('message_by','client')->orderBy('id','desc')->get()->first();

		 if(!empty($message_reply)){
		 $message = $message_reply->message;
		 }
		 
		 else{
		 $message = $row->message; 
		 }
		 
		 // if read reply envelope show
		 if($message_reply->read_by_dealer =='0'){
			 $read = '<i class="fa fa-envelope faa-flash animated" aria-hidden="true"></i>';
		 }
		 else{
			  $read = '';
		 }
		 

		 $html = '<b>'.$row->subject.' </b> ' .' ' . $read;
				
		 return "{$html}";

		},NULL)	
		
	
	->addColumn('date', function ($row) {
		
		$message_reply = EmailBox::where('reply_to',$row->email_id)->where('is_read',0)->where('message_by','client')->orderBy('id','desc')->get()->first();
		 
		 if(!empty($message_reply)){
			
		 $message_date = date("d-m-Y", strtotime($message_reply->created_at));
		 
		 }
		 
		 else{
		 $message_date = date("d-m-Y", strtotime($row->email_date));	 
		 }
		
		
	     $html = ''. $message_date."\n".'';
				
				return "<center>{$html}</center>";

		 },NULL)	

		 
       ->addColumn('delete', function ($row) {

		$html = '<input type="checkbox" value="'.$row->email_id.'" class="delete_email">';
				return "<center>{$html}</center>";

			},NULL)	

		->rawColumns(['username','action','delete','date','email'])		
		
		->make(true);
		
		}	
	
	
	
	
	public function compose_email_page(){
		
	return view('dealer/compose-box');		
		
	}
	
	
	public function email_send(Request $request){
		
	$dealer_email_send = new EmailBox();
	 
	$dealer_email_send->dealer_id = Session()->get('dealer_id');
	$dealer_email_send->client_id = $request->input('users');
	$dealer_email_send->subject = $request->input('subject');
	$dealer_email_send->message = $request->input('message');
	$dealer_email_send->message_by = "dealer";
	$dealer_email_send->save();
	

	$delete_duplicate =  $services_email::where('message',$request->input('message'))->orderBy('id', 'DESC')->get();
	
	if(count($delete_duplicate) > 1){
		
	DB::table('email_box')->where('message',$request->input('message'))->orderBy('id','DESC')->limit(1)->delete();
	
	}
		else{
			
		}
	
	return redirect(url('dealer-mail-box.html?email=sent'))->with('message', 'Message sent successfully.');
		
   }
	
	
	
	
	
	 public function mail_read(Request $request){

		
	  $get_email_data = EmailBox::where('email_box.id',decrypt($request->get('email_id')))
	 
	 ->join('users','client_id','=','users.id')
	
	 ->join('dealer','email_box.dealer_id','=','dealer.dealer_id')
	
	 ->select('email_box.id as email_id', 'users.id as user_id','email_box.dealer_id','client_id',
	  'image','name','subject','message','services','message_by','reply_to','is_read',
	  'email_box.created_at as email_date','users.created_at as user_date','dealer_name')		
	
	  ->get()->first();	
	 
	 //update email status
	 
	  
	 EmailBox::where('reply_to',$request->get('email_id'))->update(array('is_read' => 1)); 
	  
	 return view('dealer/read-email',compact('get_email_data'));	
		
 	}	
	
	
	public function mail_reply_dealer(Request $request){
	
	$dealer_reply_save = new EmailBox();
	
	$check_dealer_reply = $dealer_reply_save::where('reply_to',$request->input('email_id'))->get();
	
	if(count($check_dealer_reply) > 1){
	$dealer_first_reply = 0;
	}
	else{
	$dealer_first_reply = 1;	
	}
	
	$dealer_reply_save->client_id = $request->input('client_id');
	$dealer_reply_save->dealer_id = Session()->get('dealer_id');
	$dealer_reply_save->subject = $request->input('subject');
	$dealer_reply_save->message = $request->input('message');
	$dealer_reply_save->services = $request->input('services');
	$dealer_reply_save->message_by = 'dealer';	
	$dealer_reply_save->reply_to = $request->input('email_id');
	$dealer_reply_save->dealer_first_reply = $dealer_first_reply;
	$dealer_reply_save->save();
	
	
	$delete_duplicate =  $dealer_reply_save::where('message',$request->input('message'))->orderBy('id', 'DESC')->get();
	
	if(count($delete_duplicate) > 1){
		
	DB::table('email_box')->where('message',$request->input('message'))->orderBy('id','DESC')->limit(1)->delete();
	
	}
	else{
		
	}
	
	
	// sent notification email to user
	
	  $get_dealer = DB::table('dealer')->where('dealer_id',Session()->get('dealer_id'))->get()->first();

	   $message = "".$get_dealer->dealer_name." has been sent you email kindly check your ottoguide email box";
		
	   $get_user_email = DB::table('users')->where('id',$request->input('client_id'))->get()->first();
	
		$data =[
			'name'=> $get_user_email->name,
			'message'=> $message,
			'car_link'=> '',
			'details'=>'',
			'subject'=> 'Email Message from '.$get_dealer->dealer_name.''
		 ];

	  
	    Mail::to($get_user_email->email)->send(new all_customers_emails($data)); 
	
	
	return redirect(url('dealer-mail-box.html?email=sent'))->with('message', 'Message sent successfully.');
	
	}
	
	
	public function mail_temp_delete (Request $request){

	 $delete_ids = explode(',', $request->input('delete_ids'));
		
	  foreach($delete_ids as $delete_id){
	
		if($request->input('email_status')=='trash'){
			
		$email_delet_permanent = EmailBox::where('id',$delete_id);
		
		     $update_data = [
			 'is_delete_by_dealer' => 1,
		     ];
		
		$email_delet_permanent->update($update_data);	
		
	  } 
	
	else{
	
	EmailBox::where('id',$delete_id)->update(array('temp_delete_by_dealer' => 1)); 
	
	}

	} // end foreach
	
	return redirect()->back()->with('message', 'Email deleted successfully');
	 	
	}

	public function empty_trash(){
	
	if(Session()->get('dealer_id')){
	
	$empty_trash = EmailBox::where('dealer_id',Session()->get('dealer_id'))->where('temp_delete_by_dealer',1);
	
	         $update_data = [
			 'is_delete_by_dealer' => 1,
			 ];
	
	$empty_trash->update($update_data);		
	
	}	
		
	return redirect()->back()->with('message', 'Trash has been empty');
	
	}

	
	public function chat(){
        $dealer_id = \Session::get('dealer_id');
        // Validation to check if dealer subscribed to pricing tool addon
        $dealer_data = DB::table('dealer')->where('dealer_id', $dealer_id)->first();
        if($dealer_data->chat_lead_addon == 0){
            Session::flash('message', 'You are not subscribed to Chat');
            Session::flash('alert-class', 'alert-danger');
            return redirect('dealer/portal');
        }

	
		//ADD CHAT ADDON UNIT 
		$dealer_id = Session::get('dealer_id');
		Common::addon_inc($dealer_id,'chat_lead_addon',1);

		  $id = $_GET['car_id'];
		  
		  $car_datas = ApiCaller::get_car_detail($id);
		  
	return view('dealer/chat',['car_title'=> $car_datas->heading]);
	}
	
	
	
	/* echo"<pre>";	
	print_r($leeds_details);
	die; */
}