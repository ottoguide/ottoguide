<?php
namespace App\Http\Controllers\Dealer;
use App\Http\Controllers\Controller;
use App\Http\Middleware\DealerAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Dealer;
use App\Models\SearchCarViews;
use App\Helpers\ApiCaller;
use App\Models\CarRating;use App\Models\ContactRequest;
use App\Models\ReviewRespond;
use App\Models\DealerSearchCar;
use App\Mail\register_dealer;
use Illuminate\Pagination\LengthAwarePaginator;
use Mail;
use DB;
use Session;
use Yajra\Datatables\Datatables;

define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class Criagslist extends Controller {

	public static function run_api($url, $params = array()) {
		$final_url = API_URL . $url . '?api_key=' . API_KEY;
		if ($params && count($params) > 0) {
			$final_url .= '&' . http_build_query($params);
		}
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $final_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Host: marketcheck-prod.apigee.net"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	    echo "cURL Error #:" . $err;
		exit;
		}
		return $response;
	}
	



	  public function criagslist(){
	

	  $dealer_id = session()->get('dealer_id');

		$get_dealer = array(
	
		'dealer_id' => $dealer_id,

		);


		$response = self::run_api('search/car/active', $get_dealer);

		$response = json_decode($response);
		
		
		
		/* $currentPage = LengthAwarePaginator::resolveCurrentPage();
		
			$perPage = 5;

		$currentItems = array_slice($response , $perPage * ($currentPage - 1), $perPage);

	    $paginator = new LengthAwarePaginator($currentItems, count($response), $perPage, $currentPage, [
		  'path' => LengthAwarePaginator::resolveCurrentPath(),
		 ]); */
	
		return view('dealer/criagslist')->with('response',$response);	
		
	}	


	
	/* echo"<pre>";	
	print_r($leeds_details);
	die; */
}