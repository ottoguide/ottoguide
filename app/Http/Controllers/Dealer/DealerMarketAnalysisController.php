<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use App\Http\Middleware\DealerAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Dealer;
use App\Models\SearchCarViews;
use App\Helpers\ApiCaller;
use App\Models\CarRating;
use App\Models\ReviewRespond;
use App\Models\DealerSearchCar;
use App\Mail\register_dealer;
use Illuminate\Pagination\LengthAwarePaginator;
use Mail;
use DB;
use Yajra\Datatables\Datatables;
use Session;

define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class DealerMarketAnalysisController extends Controller {

	public static function run_api($url, $params = array()) {
		$final_url = API_URL . $url . '?api_key=' . API_KEY;
		if ($params && count($params) > 0) {
			$final_url .= '&' . http_build_query($params);
		}
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $final_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Host: marketcheck-prod.apigee.net"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			echo "cURL Error #:" . $err;
			exit;
		}
		return $response;
	}


	public function market_analysis() {
        $dealer_id = \Session::get('dealer_id');

        // Validation to check if dealer subscribed to pricing tool addon
        $dealer_data = DB::table('dealer')->where('dealer_id', $dealer_id)->first();
        if($dealer_data->market_analysis_addon == 0){
            Session::flash('message', 'You are not subscribed to Market Analysis');
            Session::flash('alert-class', 'alert-danger');
            return redirect('dealer/portal');
        }

        return view('dealer/market_analysis');
	}

	public function top_searches_table() {
		$searches = SearchCarViews::select(
			DB::raw('CONCAT(make, " ", model) as car_name'),
			DB::raw('COUNT(CONCAT(make, " ", model)) as search')
		)
			->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-1 month')))
			->groupBy('car_name')
			->get();

		return Datatables::of($searches)
			->addIndexColumn()

			->addColumn('change', function ($row) {
				$html = '';
				if ($row['car_name']) {
					$previous_month = SearchCarViews::select(
						DB::raw('CONCAT(make, " ", model) as car_name'),
						DB::raw('COUNT(CONCAT(make, " ", model)) as search')
					)
						->where('created_at', '<', date('Y-m-d H:i:s', strtotime('-1 month')))
						->where('created_at', '>', date('Y-m-d H:i:s', strtotime('-2 month')))
						->where(DB::raw('CONCAT(make, " ", model)'), $row['car_name'])
						->groupBy('car_name')
						->first();

					$searches = (isset($previous_month)) ? $previous_month->search : 0;

					$total_change = ($row['search'] - $searches);
					$change_percent = round((($total_change * 100) / $row['search']), 1);
					$sign = '<i class="fa fa-arrow-up text-success"></i>';
					if ($change_percent <= 0) {
						$sign = '<i class="fa fa-arrow-down text-danger"></i>';
					}
					$html = $total_change . ' ('.$change_percent.'%) ' . $sign;
				}
				return $html;
			},NULL)
			->addColumn('inventory', function ($row) {
				$html = 0;
				if ($row['car_name']) {
					$total_inventory = SearchCarViews::select(
						DB::raw('COUNT(DISTINCT(car_id)) AS total')
					)
						->where(DB::raw('CONCAT(make, " ", model)'), $row['car_name'])
						->first();

					$html = (isset($total_inventory)) ? $total_inventory->total : 0;
				}
				return $html;
			},NULL)

			->rawColumns(['change'])
			->make(true);
	}


	public function in_demand_table(Request $request) {
		$dealer_id = $request->session()->get('dealer_id');

		$searches = SearchCarViews::select(
			DB::raw('CONCAT(make, " ", model) as car_name'),
			DB::raw('COUNT(DISTINCT(car_id)) AS search_inventory'),
			DB::raw('COUNT(CONCAT(make, " ", model)) as search'),
			DB::raw('"'.$dealer_id.'" as dealer_id')
		)
			->where('created_at', '>=', date('Y-m-d H:i:s', strtotime('-1 month')))
			->groupBy('car_name')
			->get();

		return Datatables::of($searches)
			->addIndexColumn()


			->addColumn('inventory', function ($row) {
				$html = 0;
				if ($row['car_name']) {
					$local_inventory = SearchCarViews::select(
						DB::raw('COUNT(DISTINCT(car_id)) AS local_inventory')
					)
						->where(DB::raw('CONCAT(make, " ", model)'), $row['car_name'])
						->where('dealer_id', $row['dealer_id'])
						->first();

					$html = (isset($local_inventory)) ? $local_inventory->local_inventory : 0;
				}
				return $html;
			},NULL)

			//->rawColumns(['change'])
			->make(true);
	}
}