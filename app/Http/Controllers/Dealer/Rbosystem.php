<?php
namespace App\Http\Controllers\Dealer;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Http\Middleware\DealerAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Dealer;
use App\Models\SearchCarViews;
use App\Helpers\ApiCaller;
use App\Models\CarRating;
use App\Models\ContactRequest;
use App\Models\ReviewRespond;
use App\Models\RBO;
use App\Models\RBO_QUOTE;
use App\Models\DealerSearchCar;
use App\Mail\register_dealer;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Session;
use Mail;
use Common;
use App\Mail\dealer_reject_offer;
use App\Mail\dealer_accepted_offer;

use App\Mail\all_customers_emails;


use Yajra\Datatables\Datatables;

define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class Rbosystem extends Controller {

	public static function run_api($url, $params = array()) {
		$final_url = API_URL . $url . '?api_key=' . API_KEY;
		if ($params && count($params) > 0) {
			$final_url .= '&' . http_build_query($params);
		}
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $final_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Host: marketcheck-prod.apigee.net"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	    echo "cURL Error #:" . $err;
		exit;
		}
		return $response;
	}
	



     public function request_best_offer_list(){

	 // Validation to check if dealer subscribed to pricing tool addon
		$dealer_id = \Session::get('dealer_id');
		$dealer_data = DB::table('dealer')->where('dealer_id', $dealer_id)->first();
        if($dealer_data->rbo_lead_addon == 0){
            Session::flash('message', 'You are not subscribed to Request Best Offer');
            Session::flash('alert-class', 'alert-danger');
            return redirect('dealer/portal');
        }

	 return view('dealer/request-best-offer-list');	
		
	}	

	public function request_best_offer_table(){										

	 $request_best_offers = DB::table('request_best_offer')
	->join('users','request_best_offer.user_id','=','users.id')		
	->select(
                  'request_best_offer.id as rbo_id',
				  'users.name',
				  'users.id as user_id',
                  'car_id',
				  'dealer_quote_offer',
				  'user_quote_offer',
				  'dealer_id',
				  'rbo_status',
                  'request_best_offer.created_at as rbo_date'
          )
		->get();
	
		
		return Datatables::of($request_best_offers)
			
			->addIndexColumn()
		
			
				
			->addIndexColumn()
		
			->addColumn('RBO#', function ($row) {


				return '<b>'.$row->rbo_id.'</b>';
						
			},NULL)
			
			
			
			->addColumn('carimage', function ($row) {

				$id = $row->car_id;
				
				$car_image = ApiCaller::get_car_detail($id);
				
				$html = '<img src='.$car_image->media->photo_links[0].' width="120" height="90">';

				return "{$html}";
						
			},NULL)
			
			
			->addColumn('cartitle', function ($row) {

				$id = $row->car_id;
				
				$car_image = ApiCaller::get_car_detail($id);
				
				$html = '<h5>'.$car_image->heading.' </h5>';

				return "{$html}";
						
			},NULL)
			
		
		
		
		
		->addColumn('dealer_status', function ($row) {

		  $get_dealer_status = DB::table('rbo_dealer_quote')->where('rbo_id',$row->rbo_id)->get()->first();	

				return $get_dealer_status->dealer_status;
						
			},NULL)
		
		
		
		->addColumn('customer_status', function ($row) {

		  $get_customer_status = DB::table('rbo_dealer_quote')->where('rbo_id',$row->rbo_id)->get()->first();	

				return $get_customer_status->customer_status;
						
			},NULL)
			
		
		
			->addColumn('action', function ($row) {

			$check_rbo_policy =  RBO::where('id',$row->rbo_id)->where('legalpolicy',0)->get()->first();
			
			$check_rbo_quote =   RBO_QUOTE::where('rbo_id',$row->rbo_id)->get()->first();
			
//			if(count($check_rbo_policy)){
//			$html = '<br><a href="'.route('request-best-offer-legal-page', ['car' => $row->car_id, 'user'=> $row->user_id, 'dealer'=> $row->dealer_id, 'rbo'=> $row->rbo_id]).'"><button type="button" class="btn btn-warning btn-sm align-middle">Legal Agreement</button></a>';
//			}
//			else{
				
				
			if(($check_rbo_quote->customer_status =="Modified" || $check_rbo_quote->viewed_by_customer =="1") && $check_rbo_quote->customer_status !="Saved" &&$check_rbo_quote->customer_status !="Rejected" && $check_rbo_quote->customer_status !="Pending"){
			$html = '<br><a href="'.route('rbo-quote-form-view', ['car' => $row->car_id, 'rbo'=> encrypt($row->rbo_id)]).'"><button type="button" class="btn btn-success btn-sm  align-middle">View Offer</button></a>';	
			}
			if(($check_rbo_quote->customer_status =="Rejected" || $check_rbo_quote->customer_status =="Pending")&& $check_rbo_quote->customer_status !="Saved" && $check_rbo_quote->viewed_by_customer !="1"){
			$html .= '<a href="'.route('rbo-edit', ['car' => $row->car_id, 'rbo'=> encrypt($row->rbo_id)]).'"><button type="button" class="btn btn-info btn-sm align-middle">Edit Offer</button></a>';	
			}
				
			if(!count($check_rbo_quote)){
			
			$html = '<br><a href="'.route('rbo-quote-create', ['car_id' => $row->car_id, 'user'=> encrypt($row->user_id),'rbo_id'=> $row->rbo_id]).'"><button type="button" class="btn btn-primary btn-sm  align-middle">Add Form</button></a>';

			}
			
			
			
//			} // end main else

		
			
		return "<center>{$html}</center>";

			},NULL)
					
		    ->rawColumns(['carimage','cartitle','action','RBO#'])
			 
			->make(true);
	
	}
	
	
	public function rbo_legal_page(Request $request){
		
	
	$check_dealer_subscription = Dealer::where('dealer_id',Session::get('dealer_id'))->get()->first();
	
    if($check_dealer_subscription->rbo_lead_addon == 1){
		
       $id = $request->get('car');
	   $user_id = $request->get('user');
	   $dealer_id = $request->get('dealer');
	   $rbo_id = $request->get('rbo');

	  $car_datas = ApiCaller::get_car_detail($id);
	  
	  return view('dealer/rbo-detail')->with('car_datas',$car_datas)->with('user_id',$user_id)->with('rbo_id',$rbo_id);
	
	}
	
	else{
	
	$quote_expire_date =  date('Y-m-d H:i:s', strtotime("+48 hours"));
	
	RBO_QUOTE::where('dealer_id',Session::get('dealer_id'))->update(array('quote_expire_date' => ''.$quote_expire_date.''));
	
	return redirect()->back()->with('error', 'Your are not subscribed for RBO. Kindly subscribe with us in TWO WORKING DAYS for avail this offer.');	  

	}
	
	}


	public function check_subscription_2days_time(Request $request){
		
		
	}
	
	
	public function rbo_accepted(Request $request){
		
	 $user_id = decrypt($request->get('user'));
	 
	 $rbo_id = $request->get('rbo_id');
	 
	 RBO::where('user_id',$user_id)->where('id',$request->get('rbo_id'))
	->update(array('legalpolicy' => '1'));	
	
	return redirect(url('request-best-offer-list.html'))->with('message', 'Your legal agreement has been done. Now you can create Quote Offer for customer.');

	}
	
	public function quote_create(Request $request){

	 $rbo_id = $request->get('rbo_id');
	 $user_id = decrypt($request->get('user'));

	 RBO::where('id',$rbo_id)
	->update(array('rbo_status' => 'Created'));

	 $id = $request->get('car_id');

	 $car_datas = ApiCaller::get_car_detail($id);

	 return view('dealer/rbo-quote-form')->with('car_datas',$car_datas)->with('user_id',$user_id)->with('rbo_id',$rbo_id);

	}	
	
	
			
  public function rbo_rejected(Request $request){

	$get_user_email = DB::table('users')->where('id',decrypt($request->get('user')))->first();

	 RBO::where('user_id',decrypt($request->get('user')))->where('car_id',$request->get('car_id'))
	->update(array('rbo_status' => 'Rejected'));

	$id = $request->get('car_id');

	$car_datas = ApiCaller::get_car_detail($id);



	$to_email = $get_user_email->email;

	 $data =[
	'name'=> $get_user_email->name,
	'car_title'=> $car_datas->heading,
	'car_link'=> $car_datas->id,
	'similar_car'=> $car_datas->vin . '&radius='. $request->get('radius').'&latitude='. $request->get('latitude'). '&longitude='. $request->get('longitude'),
	 ];

	//send contact request to dealer email
	$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
 
   	Mail::to($to_email)
	->cc($get_admin_email->email)
	->send(new dealer_reject_offer($data));


	}



	public function rbo_quoteform_submit(Request $request){


	//dealer Option Price
		$dealer_options_description = '';
		if($request->input('dealer_options_description')){
		$dealer_options_description = implode(',', $request->input('dealer_options_description'));
		}

		$dealer_options_values = '';
		if($request->input('dealer_options_value')){
		$dealer_options_values = implode(',', $request->input('dealer_options_value'));
		}

	//document fees

		$doc_fees_description = '';
		if($request->input('doc_fees_description')){
		$doc_fees_description = implode(',', $request->input('doc_fees_description'));
		}

		$doc_fees_values = '';
		if($request->input('doc_fees_value')){
		$doc_fees_values = implode(',', $request->input('doc_fees_value'));
		}

	//rebate_discount

		$rebate_discount_description = '';
		if($request->input('rebate_discount_description')){
		$rebate_discount_description = implode(',', $request->input('rebate_discount_description'));
		}

		$rebate_discount_values = '';
		if($request->input('rebate_discount_value')){
		$rebate_discount_values = implode(',', $request->input('rebate_discount_value'));
		}

		// calculate sale price

		 $i = 0; $i2 = 0; $i3 = 0; $sum = 0; $sum2 = 0; $sum3 = 0;

		 foreach ($request->input('dealer_options_value') as $row)
		 {
			$i++;
			$dealer_option = $sum += $row;
		 }

		 foreach ($request->input('doc_fees_value') as $row2)
		 {
			$i2++;
			$doc_fees = $sum2 += $row2;
		 }

		 foreach ($request->input('rebate_discount_value') as $row3)
		 {
			$i3++;
			$rabates_discount = $sum3 += $row3;
		 }


		// get sale price
	    $sale_price = $request->input('car_price') + $dealer_option + $doc_fees - $rabates_discount;

		
		
	    $rbo_quote_submit = new RBO_QUOTE();

	    $rbo_quote_submit->user_id = $request->input('user_id');
		$rbo_quote_submit->dealer_id = Session::get('dealer_id');
		$rbo_quote_submit->car_id = $request->input('car_id');
		$rbo_quote_submit->rbo_id = $request->input('rbo_id');
		$rbo_quote_submit->car_price = $request->input('car_price');

		$rbo_quote_submit->dealer_price_description = $dealer_options_description;
		$rbo_quote_submit->dealer_price_values = $dealer_options_values;

		$rbo_quote_submit->doc_fees_description = $doc_fees_description;
		$rbo_quote_submit->doc_fees_values = $doc_fees_values;

		$rbo_quote_submit->rebate_discount_description = $rebate_discount_description;
		$rbo_quote_submit->rebate_discount_values = $rebate_discount_values;

		$rbo_quote_submit->action = '';
		$rbo_quote_submit->customer_status = 'Pending';
		$rbo_quote_submit->status = 'Created';
		$rbo_quote_submit->dealer_status = 'Created';
		$rbo_quote_submit->sale_price = $sale_price;
		$rbo_quote_submit->expiry_date = $request->input('quote_expiry_date');

		$rbo_quote_submit->save();

		//add rbo 
		$dealer_id = Session::get('dealer_id');
		Common::addon_inc($dealer_id,'rbo_lead_addon',1);
		
		//Update status main RBO TABLE
		RBO::where('id',$request->input('rbo_id'))->update(array('rbo_status' => 'Created'));
	
		
		// SENT QUOTE FOR EMAIL TO USER
		
		$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
		$get_user_email = DB::table('users')->where('id',$request->input('user_id'))->get()->first();
		
		$id = $request->input('car_id');
		$car_datas = ApiCaller::get_car_detail($id);
		
		
		$message = "".$get_user_email->name." Concern dealer has created Quote Of your request best offer of (".$car_datas->heading.") . Now you can check user dashboard RBO Panel";
	
		$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();


		 $data =[
				'name'=> $get_user_email->name,
				'message'=> $message,
				'car_link'=> $request->input('car_id'),
				'details'=> '',
				'subject'=> 'RBO Quote Created ('.$car_datas->heading.')',
		 ];
		
		
		Mail::to($get_user_email->email)->cc($get_admin_email->email)->send(new all_customers_emails($data)); 
		
		
		
		return redirect(url('request-best-offer-list.html'))->with('message', 'Request Best Offer Quote From has been created Successfully.');

		
		}
		
	
	
     public function rbo_quoteform_edit(Request $request){
	 
	 $edit_quote_form = RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')))->first();
	
	 $id = $request->get('car');
	 
	 $car_datas = ApiCaller::get_car_detail($id);
	
	 return view('dealer/rbo-quote-form-edit')->with('edit_quote_form',$edit_quote_form)->with('car_datas',$car_datas);
	
	 }

   
	
	//QUOTE UPDATED
	
	public function rbo_quoteform_update(Request $request){
	
	 //dealer Option Price
		$dealer_options_description = '';
		if($request->input('dealer_options_description')){
		$dealer_options_description = implode(',', $request->input('dealer_options_description'));
		}

		$dealer_options_values = '';
		if($request->input('dealer_options_value')){
		$dealer_options_values = implode(',', $request->input('dealer_options_value'));
		}

	//document fees

		$doc_fees_description = '';
		if($request->input('doc_fees_description')){
		$doc_fees_description = implode(',', $request->input('doc_fees_description'));
		}

		$doc_fees_values = '';
		if($request->input('doc_fees_value')){
		$doc_fees_values = implode(',', $request->input('doc_fees_value'));
		}

	//rebate_discount

		$rebate_discount_description = '';
		if($request->input('rebate_discount_description')){
		$rebate_discount_description = implode(',', $request->input('rebate_discount_description'));
		}

		$rebate_discount_values = '';
		if($request->input('rebate_discount_value')){
		$rebate_discount_values = implode(',', $request->input('rebate_discount_value'));
		}

		// calculate sale price

		 $i = 0; $i2 = 0; $i3 = 0; $sum = 0; $sum2 = 0; $sum3 = 0;

		 foreach ($request->input('dealer_options_value') as $row)
		 {
			$i++;
			$dealer_option = $sum += $row;
		 }

		 foreach ($request->input('doc_fees_value') as $row2)
		 {
			$i2++;
			$doc_fees = $sum2 += $row2;
		 }

		 foreach ($request->input('rebate_discount_value') as $row3)
		 {
			$i3++;
			$rabates_discount = $sum3 += $row3;
		 }

		// get sale price
	    $sale_price_updated = $request->input('car_price') + $dealer_option + $doc_fees - $rabates_discount ;

		 $rbo_quote_update = new RBO_QUOTE();



			$update_data = [
					'dealer_price_description' => $dealer_options_description,
					'dealer_price_values' => $dealer_options_values,
					'doc_fees_description' => $doc_fees_description,
					'doc_fees_values' =>  $doc_fees_values,
					'rebate_discount_description' => $rebate_discount_description,
					'rebate_discount_values' => $rebate_discount_values,
					'sale_price' => $sale_price_updated,
					'customer_status' => 'Pending',
					'dealer_status' => 'Created',
					'status' => 'Created',
					'expiry_date' => $request->input('quote_expiry_date'),
					'updated_at' => date('Y-m-d H:i:s'),
			 ];
			
		$update_dealer_quote = $rbo_quote_update::where('rbo_id',$request->input('rbo_id'));
			
		$update_dealer_quote->update($update_data);		
	 
	 
	    RBO::where('id',$request->input('rbo_id'))->update(array('rbo_status' => 'Created'));

	 
		return redirect(url('request-best-offer-list.html'))->with('message', 'Your quoted offer has been updated successfully.');

		
	   }
   
   
 
 
 
	 public function rbo_quoteform_view(Request $request){

	   $view_quote_form = RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')))->first();
	   
	  /*  if($view_quote_form->customer_status !== 'Modified'){
		   
	   return redirect()->back()->with('error', 'Customer not responded you offer. When customer response your offer we will inform you.');	  
   
	   }
	   
	   else{ */
		
	   $id = $request->get('car');

	   $car_datas = ApiCaller::get_car_detail($id);

	   
	   return view('dealer/rbo-quote-view')->with('view_quote_form',$view_quote_form)->with('car_datas',$car_datas);

	    //}
	   
	   }
	   
	   
	   // DEALER EMAIL NOTIFICAITON FOR QUOTE (ACCEPT - REJECT - INVITE)
	   
	   
	   public function rbo_quoteform_dealer_status(Request $request){
		   
	    $get_rbo_quote_status = RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')))
	   
	   ->join('dealer','rbo_dealer_quote.dealer_id','=','dealer.dealer_id')
	   
	   ->join ('users','rbo_dealer_quote.user_id','=','users.id')
	   
	   ->first();
		
		$to_email = $get_rbo_quote_status->email;
		$id = $get_rbo_quote_status->car_id;
		$car_datas = ApiCaller::get_car_detail($id);
	   
	   
	   if($request->get('quote_status')== "Accepted"){
		
		$get_rbo_quote_status::where('rbo_id',decrypt($request->get('rbo')))->update(array('dealer_status' => 'Accepted'));
	
		$message = "".$get_rbo_quote_status->dealer_name." accepted your offer of ".$car_datas->heading." . Now you can communicate with dealer though Chat,Call,Email Channel.";
	
		$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();

	
		 $data =[
				'name'=> $get_rbo_quote_status->name,
				'message'=> $message,
				'car_link'=> $car_datas ->id,
				'details'=> '',
				'subject'=> 'RBO Accepted('.$car_datas->heading.')',
		 ];
	  
		Mail::to($to_email)
		->cc($get_admin_email->email)
		->send(new all_customers_emails($data)); 
		
		RBO::where('id',decrypt($request->get('rbo')))->update(array('rbo_status' => 'Accepted'));
		
		return redirect(url('request-best-offer-list.html'))->with('message', 'Request Best Offer successfully accepted');
		

		}
		
		   
		if($request->get('quote_status')== "Rejected"){
		
		$get_rbo_quote_status::where('rbo_id',decrypt($request->get('rbo')))->update(array('dealer_status' => 'Closed'));
		
		$message = "".$get_rbo_quote_status->dealer_name." Rejected your offer Of <b>".$car_datas->heading."</b>. Your offer is now closed";
	
		$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();

	
		 $data =[
				'name'=> $get_rbo_quote_status->name,
				'message'=> $message,
				'car_link'=> $car_datas ->id,
				'details'=> '',
				'subject'=> 'RBO Rejected('.$car_datas->heading.')',
		  ];
	  
		Mail::to($to_email)
		->cc($get_admin_email->email)
		->send(new all_customers_emails($data)); 
		
		RBO::where('id',decrypt($request->get('rbo')))->update(array('rbo_status' => 'Rejected'));
		
		 return redirect(url('request-best-offer-list.html'))->with('message', 'Request Best Offer has been rejected');
		
		}
	   
	   
	   	if($request->get('quote_status')== "Invited"){
		
		$get_rbo_quote_status::where('rbo_id',decrypt($request->get('rbo')))->update(array('dealer_status' => 'Invited'));
		
		$message = "".$get_rbo_quote_status->dealer_name." has invite you for you ".$car_datas->heading." offer. If you communicate with dealer through CHAT,CALL,EMAIL. Kindly go to your Dashboard RBO list under communication icons.";
	
		$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();

		 $data =[
				'name'=> $get_rbo_quote_status->name,
				'message'=> $message,
				'car_link'=> $car_datas ->id,
				'details'=> '',
				'subject'=> 'RBO Invited('.$car_datas->heading.')',
		  ];
	  
		 Mail::to($to_email)
		 ->cc($get_admin_email->email)
		 ->send(new all_customers_emails($data));  
		
		RBO::where('id',decrypt($request->get('rbo')))->update(array('rbo_status' => 'Invited'));	
		}
	   
	   return redirect(url('request-best-offer-list.html'))->with('message', 'Invitation has been sent successfully.');
	
	   
	   }
	   
	   

	
}