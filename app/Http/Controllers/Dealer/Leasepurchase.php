<?php
namespace App\Http\Controllers\Dealer;
use App\Http\Controllers\Controller;
use App\Http\Middleware\DealerAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Dealer;
use Illuminate\Support\Collection;
use App\Models\SearchCarViews;
use App\Helpers\ApiCaller;
use App\Models\Offers;
use App\Models\CarRating;use App\Models\ContactRequest;
use App\Models\ReviewRespond;
use App\Models\DealerSearchCar;
use App\Mail\register_dealer;
use Illuminate\Pagination\LengthAwarePaginator;
use Mail;
use DB;
use Session;
use Yajra\Datatables\Datatables;

define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class Leasepurchase extends Controller {

	public static function run_api($url, $params = array()) {
		$final_url = API_URL . $url . '?api_key=' . API_KEY;
		if ($params && count($params) > 0) {
			$final_url .= '&' . http_build_query($params);
		}
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $final_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Host: marketcheck-prod.apigee.net"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	    echo "cURL Error #:" . $err;
		exit;
		}
		return $response;
	}
	



    public function lease_purchase(){
			
	 return view('dealer/leasepurchase');	
		
	}	


	public function lease_purchase_table(Request $request){	
	
	
	$dealer_id = $request->session()->get('dealer_id');



		$get_dealer = array(

			//'dealer_id' => $dealer_id,
			'make' => 'Honda',

		);



		$response = self::run_api('search/car/', $get_dealer);

		$response = json_decode($response);



		$dealer_data = new Collection();



		foreach($response->listings as $get_stats){

			$dealer_data->push(array(

				'image'=> '<img src="'.$get_stats->media->photo_links[0].'" width="150" height="100">',
			
				'id'=> $get_stats->id,

				'title'=> $get_stats->heading,

				'model'=> $get_stats->build->model,

				'stock'=> (isset($get_stats->stock_no)) ? $get_stats->stock_no : 'N/A',
				
			));

		}
	
	
	return Datatables::of($dealer_data)

			->addIndexColumn()

			->addColumn('Offers', function ($row) {

				$lease = Offers::where('dealer_id',session()->get('dealer_id'))

					->where('car_id',$row['id'])

					->where('offer','lease')

					->get()

					->count('offer');


				$purchase = Offers::where('dealer_id',session()->get('dealer_id'))

					->where('car_id',$row['id'])

					->where('offer','purchase')

					->get()

					->count('offer');


			$html = '<a href=""><button type="button" class="btn btn-primary btn-sm align-middle">'.$lease.' Lease</button></a>  

			 &nbsp; &nbsp; <a href="#"><button type="button" class="btn btn-success btn-sm align-middle">'.$purchase.' Purchase</button></a>';

				return "<center>{$html}</center>";

			},NULL)



			->rawColumns(['Offers','image'])

			//->rawColumns(['image'])

			->make(true);

	}
	
	

/*  	public function lease_table(){										

	
	$lease_purchase = DB::table('Offers')
						
	->where('dealer_id',Session()->get('dealer_id'))
	
	->where('offer',$offer)	
	 
	->join('users','Offers.client_id','=','users.id')
   
	->select('Offers.*','users.*')
   
	->get();

	return Datatables::of($lease_purchase)
		
	->addIndexColumn()
	
	->make(true);
	
	}  */

	/* echo"<pre>";	
	print_r($leeds_details);
	die; */
}