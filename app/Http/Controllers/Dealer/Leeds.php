<?php
namespace App\Http\Controllers\Dealer;
use App\Http\Controllers\Controller;
use App\Http\Middleware\DealerAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Dealer;
use App\Models\SearchCarViews;
use App\Helpers\ApiCaller;
use App\Models\CarRating;use App\Models\ContactRequest;
use App\Models\ReviewRespond;
use App\Models\DealerSearchCar;
use App\Mail\register_dealer;
use Illuminate\Pagination\LengthAwarePaginator;
use Mail;
use DB;
use Session;
use Yajra\Datatables\Datatables;

define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class Leeds extends Controller {

	public static function run_api($url, $params = array()) {
		$final_url = API_URL . $url . '?api_key=' . API_KEY;
		if ($params && count($params) > 0) {
			$final_url .= '&' . http_build_query($params);
		}
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $final_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Host: marketcheck-prod.apigee.net"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	    echo "cURL Error #:" . $err;
		exit;
		}
		return $response;
	}
	



    public function leeds_stats(){
			
	 return view('dealer/leeds');	
		
	}	



	public function leeds_stats_table(){										

	$leeds_details = DB::table('contact_request')
						
	//->where('dealer_id',Session()->get('dealer_id'))	
	
	->where('request_type','email_request')
	 
	->join('users','contact_request.client_id','=','users.id')
   
	->select('contact_request.*','users.*')
    
	->get();

	return Datatables::of($leeds_details)
		
	->addIndexColumn()
	
	->make(true);
	
	}

	
	
	public function leeds_stats_phone_table(){										
	
	$leeds_details = DB::table('contact_request')
	 
	// ->where('dealer_id',Session()->get('dealer_id'))	

	 ->where('request_type','call_request')
	 
	->join('users','contact_request.client_id','=','users.id')
    
	->select('contact_request.*','users.*')
    
	->get();
	
	return Datatables::of($leeds_details)
		
	->addIndexColumn()
	
	->make(true);
	
	}
	
	

		
	public function leeds_stats_sms_table(){										
	
	
	$leeds_details = DB::table('contact_request')
	 	 
	 //->where('dealer_id',Session()->get('dealer_id'))	

	 ->where('request_type','sms_request')
	 
	->join('users','contact_request.client_id','=','users.id')
    
	->select('contact_request.*','users.*')
    
	->get();
	
	return Datatables::of($leeds_details)
		
	->addIndexColumn()
	
	->make(true);
	
	}
	
	
	
	public function leeds_stats_chat_table(){										
	
	 $leeds_details = DB::table('contact_request')
	 
	//->where('dealer_id',Session()->get('dealer_id'))	

    ->where('request_type','chat_request')
	 
	->join('users','contact_request.client_id','=','users.id')
    
	->select('contact_request.*','users.*')
    
	->get();
	
	return Datatables::of($leeds_details)
		
	->addIndexColumn()
	
	->make(true);
	
	}
	


	
	/* echo"<pre>";	
	print_r($leeds_details);
	die; */
}