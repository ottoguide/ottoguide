<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use App\Http\Middleware\DealerAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Dealer;
use App\Models\SearchCarViews;
use App\Helpers\ApiCaller;
use App\Models\CarRating;
use App\Models\ReviewRespond;
use App\Models\DealerSearchCar;
use App\Mail\register_dealer;
use Illuminate\Pagination\LengthAwarePaginator;
use Mail;
use DB;

define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class DealerSettingController extends Controller {

	public static function run_api($url, $params = array()) {
		$final_url = API_URL . $url . '?api_key=' . API_KEY;
		if ($params && count($params) > 0) {
			$final_url .= '&' . http_build_query($params);
		}
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $final_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Host: marketcheck-prod.apigee.net"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			echo "cURL Error #:" . $err;
			exit;
		}
		return $response;
	}


	public function dealer_settings() {
	   return view('dealer/settings');
	}			
	
	
	public function add_email(Request $request) {			
	
	$this->validate($request, [
	'email_address' => 'required',		
	]);				
	
	$dealer_id = $request->session()->get('dealer_id');		
	
	$check_emails = Dealer::where('dealer_id', $dealer_id)->WhereNotNull('dealer_other_emails')->get();
  
	if($check_emails->count()){
	
    $dealer_other_email = $check_emails[0]->dealer_other_emails . ','. $request->input('email_address');		

	}						
	
	else {	
	
	$dealer_other_email = $request->input('email_address');	
		
	}				
	
	Dealer::where('dealer_id', $dealer_id)->update([			
	'dealer_other_emails' => $dealer_other_email		
	]);	
	
	
	return redirect('dealer-settings.html');
	
	}
	
	
	
	public function add_phone(Request $request) {			
	
	$this->validate($request, [
	'dealer_other_phone' => 'required',		
	]);				
	
	$dealer_id = $request->session()->get('dealer_id');		
	
	$check_phone = Dealer::where('dealer_id', $dealer_id)->WhereNotNull('dealer_other_phone')->get();
  
	if($check_phone->count()){
	
    $dealer_other_phone = $check_phone[0]->dealer_other_phone . ','. $request->input('dealer_other_phone');		

	}						
	
	else {	
	
	$dealer_other_phone = $request->input('dealer_other_phone');	
		
	}				
	
	Dealer::where('dealer_id', $dealer_id)->update([			
	'dealer_other_phone' => $dealer_other_phone		
	]);	
	
	return redirect('dealer-settings.html');
	
	}
	
	
	
	public function update_phone(Request $request) {
		
	/* 	$this->validate($request, [
			'dealer_phone' => 'required',
		]); */

		$dealer_id = $request->session()->get('dealer_id');
		
		//update primary phone
		Dealer::where('dealer_id', $dealer_id)->update([
			'dealer_phone' => $request->input('dealer_primary_phone')
		]);

		
		// update secondary phones
		$update_phone = "";
		if($request->input('dealer_phone')){
		$update_phone = implode(',',$request->input('dealer_phone'));		
		}

		
		Dealer::where('dealer_id', $dealer_id)->update([
			'dealer_other_phone' => $update_phone
		]);
		
	
		
		return redirect('dealer-settings.html');
		
	}

	
	public function update_website(Request $request) {
		$this->validate($request, [
			'dealer_website' => 'required',
		]);

		$dealer_id = $request->session()->get('dealer_id');

		Dealer::where('dealer_id', $dealer_id)->update([
			'dealer_website' => $request->input('dealer_website')
		]);
		return redirect('dealer-settings.html');
	}

	public function update_email(Request $request) {
		
		$this->validate($request, [
			'dealer_email' => 'required|email|unique:dealer,dealer_email,'.$request->session()->get('dealer_id') . ',dealer_id',
		]);


		$dealer_id = $request->session()->get('dealer_id');

		Dealer::where('dealer_id', $dealer_id)->update([
			'dealer_email' => $request->input('dealer_email')
		]);
		return redirect('dealer-settings.html');
	}
	
	
	// Delete Dealer Emails 
	
	public function delete_other_emails(Request $request){
		
	$dealer_id = $request->session()->get('dealer_id');
	
	$get_dealer_email = Dealer::select('dealer_other_emails')->where('dealer_id', $dealer_id)->get();
	
	
	$string = explode(',',$get_dealer_email[0]->dealer_other_emails);
	$final_array = array();
	foreach($string as $dealer_emails){
		if ($dealer_emails != $request->input('email')) {
			$final_array[] = $dealer_emails;
		}
	}
	
	$update_emails =  implode(',', $final_array);
	
	Dealer::where('dealer_id', $dealer_id)->update([
			'dealer_other_emails' =>$update_emails
		]);
	
	
	return redirect('dealer-settings.html');
	
	
	}
	
	
	
	public function update_other_email(Request $request){
		
	 $dealer_id = $request->session()->get('dealer_id');	
		
		$this->validate($request, [
	
	    'other_email_address' => 'required',
	
    	]);
		
		
		$update_emails = "";
		if($request->input('other_email_address')){
		$update_emails = implode(',',$request->input('other_email_address'));		
		}
		
		
		Dealer::where('dealer_id', $dealer_id)->update([
			'dealer_other_emails' =>$update_emails
		]);
	
		return back()->with('message','Dealer email updated successfully');
	
		}
	

	
	// Delete Dealer Phone 
	
	public function delete_phone(Request $request){
		
	$dealer_id = $request->session()->get('dealer_id');
	
	$get_dealer_phone = Dealer::select('dealer_other_phone')->where('dealer_id', $dealer_id)->get();
	
	
	$string = explode(',',$get_dealer_phone[0]->dealer_other_phone);
	$final_array = array();
	foreach($string as $dealer_phone){
		if ($dealer_phone != $request->input('phone')) {
			$final_array[] = $dealer_phone;
		}
	}
	
	$update_phone =  implode(',', $final_array);
	
	Dealer::where('dealer_id', $dealer_id)->update([
			'dealer_other_phone' =>$update_phone
		]);
	
	
	return redirect('dealer-settings.html');

	}
	
	
	
	public function update_company_info(Request $request){
	
	$this->validate($request, [
	
			'dealer_name' => 'required',
			'dealer_email' => 'required',
			'dealer_phone' => 'required',
			'dealer_country' => 'required',
			'dealer_street' => 'required',
			'dealer_zip' => 'required',
			'dealer_website' => 'required',
	
    	]);

		
		$dealer_other_email = "";
		if($request->input('dealer_other_email')){
		$dealer_other_email = implode(',',$request->input('dealer_other_email'));		
		}
		
		
		$update_information = [
			'dealer_name' => $request->input('dealer_name'),
			'dealer_email' => $request->input('dealer_email'),
			'dealer_other_emails' => $dealer_other_email,
			'dealer_phone' => $request->input('dealer_phone'),
			'dealer_country' => $request->input('dealer_country'),
			'dealer_city' => $request->input('dealer_city'),
			'dealer_street' => $request->input('dealer_street'),
			'dealer_website' => $request->input('dealer_website'),
			'dealer_zip' => $request->input('dealer_zip'),
			'dealer_latitude' => $request->input('dealer_latitude'),
			'dealer_longitude' => $request->input('dealer_longitude'),
		];
	
	
		$dealer_id = $request->session()->get('dealer_id');
	
		$dealer = Dealer::where('dealer_id',$dealer_id);
		$dealer->update($update_information);
	
		return back()->with('message','Company Information Updated Successfully');
	
		
	}
	
	
	
	
	public function dealer_add_radius(Request $request){
		
	$dealer_id = $request->input('dealer_id');
	$update_radius = $request->input('radius');

	
	Dealer::where('dealer_id', $dealer_id)->update([
			'radius' =>$update_radius
		]);	
		
	
	return redirect()->back()->with('message', 'Dealer Radius Added Successfully');	
		
		
	}
	

		
	
}