<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use App\Http\Middleware\DealerAuth;
use App\Models\ContactRequest;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Dealer;
use App\Models\SearchCarViews;
use App\Helpers\ApiCaller;
use App\Models\CarRating;
use App\Models\ReviewRespond;
use App\Models\DealerSearchCar;
use App\Mail\register_dealer;
use Illuminate\Pagination\LengthAwarePaginator;
use Mail;
use DB;
use Yajra\Datatables\Datatables;

define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class DealerDashboardController extends Controller {

	public static function run_api($url, $params = array()) {
		$final_url = API_URL . $url . '?api_key=' . API_KEY;
		if ($params && count($params) > 0) {
			$final_url .= '&' . http_build_query($params);
		}
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $final_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Host: marketcheck-prod.apigee.net"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			echo "cURL Error #:" . $err;
			exit;
		}
		return $response;
	}

	public function dealer_portal(Request $request) {
		$dealer_id = $request->session()->get('dealer_id');

		$monthly_views = array();
		for ($i = 1; $i < 13; $i++){

			$start_month = date ( 'Y-'.$i.'-1' );
			$end_month = date ( 'Y-'.$i.'-t');

			$count = DB::table('dealer_car_search_views')
				->where('dealer_id',$dealer_id)
				->where('search_type','search')
				->where('created_at', '>=', $start_month)
				->where('created_at','<=', $end_month)
				->orderBy('created_at','ASC')
				->get()
				->count();

			$monthly_views[$start_month] = $count;
		}

		// CAR DETAILS
		$monthly_details = array();
		for ($i2=1;$i2<13;$i2++){
			$start_month2 = date ( 'Y-'.$i2.'-1' );
			$end_month2 = date ( 'Y-'.$i2.'-t');

			$count_details = DB::table('dealer_car_search_views')
				->where('dealer_id',$dealer_id)
				->where('search_type','detail')
				->where('created_at', '>=', $start_month2)
				->where('created_at','<=', $end_month2)
				->orderBy('created_at','ASC')
				->get()
				->count();

			$monthly_details[$start_month2] = $count_details;
		}

		$currentMonth = date('m');
		$current_month_views = DB::table("dealer_car_search_views")
			->where('dealer_id',$dealer_id)
			->where('search_type','search')
			->whereRaw('MONTH(created_at) = ?',[$currentMonth])
			->get()
			->count();

		$current_month_details = DB::table("dealer_car_search_views")
			->where('dealer_id',$dealer_id)
			->where('search_type','detail')
			->whereRaw('MONTH(created_at) = ?',[$currentMonth])
			->get()
			->count();


		$total_interaction = DB::table('dealer_car_search_views')->join('dealer_addon_logs','dealer_car_search_views.dealer_id','=','dealer_addon_logs.dealer_id')
		   ->where('dealer_car_search_views.dealer_id', $dealer_id)
			->count();
		$total_interaction_past_30 = ContactRequest::where('dealer_id', $dealer_id)
			->where('created_at', '>=', date('Y-m-01', strtotime('-1 month')))
			->where('created_at', '<', date('Y-m-d'))
			->count();
		$total_interaction_past_7 = ContactRequest::where('dealer_id', $dealer_id)
			->where('created_at', '>=', date('Y-m-01', strtotime('-7 days')))
			->where('created_at', '<', date('Y-m-d'))
			->count();
		$total_interaction_past_6_months = ContactRequest::where('dealer_id', $dealer_id)
			->where('created_at', '>=', date('Y-m-01', strtotime('-6 month')))
			->where('created_at', '<', date('Y-m-d'))
			->count();

		$interaction_chart = array();

		for ($i = 5; $i >= 0; $i--) {
			$date_start = date('Y-m-01', strtotime("-$i month"));
			$date_end = date('Y-m-t', strtotime("-$i month"));

			$phone_leads = DB::table('dealer_addon_logs')
				->where('dealer_id',$dealer_id)
				->where('addon','voice_lead_addon')
				->where('created_at', '>=', $date_start)
				->where('created_at', '<=', $date_end)
				->count();
			$sms_leads = DB::table('twilio_chat')
				->where('dealer_id',$dealer_id)
				->where('created_at', '>=', $date_start)
				->where('created_at', '<=', $date_end)
				->count();
			$email_leads = DB::table('email_box')
				->where('dealer_id',$dealer_id)
				->where('created_at', '>=', $date_start)
				->where('created_at', '<=', $date_end)
				->count();
			$web_links = DB::table("dealer_car_search_views")
				->where('dealer_id',$dealer_id)
				->where('search_type','detail')
				->where('created_at', '>=', $date_start)
				->where('created_at', '<=', $date_end)
				->count();

			$interaction_chart[date('F-Y', strtotime($date_start))] = array(
				'phone_leads' => $phone_leads,
				'sms_leads' => $sms_leads,
				'email_leads' => $email_leads,
				'web_links' => $web_links,
			);
		}

		//echo '<pre>'; print_r($interaction_chart); die();

		$deals_data = array();
		$deals_data[] = array('Deals', 'Total');
		$deals_data[] = array('Great Price', DealerSearchCar::where('dealer_id', $dealer_id)->where('car_deal', 'great')->count());
		$deals_data[] = array('Good Price', DealerSearchCar::where('dealer_id', $dealer_id)->where('car_deal', 'good')->count());
		$deals_data[] = array('Average Price', DealerSearchCar::where('dealer_id', $dealer_id)->where('car_deal', 'average')->count());
		$deals_data[] = array('High Price', DealerSearchCar::where('dealer_id', $dealer_id)->where('car_deal', 'high')->count());
		$deals_data[] = array('No Price Change', DealerSearchCar::where('dealer_id', $dealer_id)->where('car_deal', null)->count());


		return view('dealer/dashboard')
			->with(array(
				'monthly_views' => $monthly_views,
				'current_month_views'=>$current_month_views,
				'current_month_details'=>$current_month_details,
				'monthly_details'=>$monthly_details,
				'total_interaction'=>$total_interaction,
				'total_interaction_past_30'=>$total_interaction_past_30,
				'total_interaction_past_7'=>$total_interaction_past_7,
				'total_interaction_past_6_months'=>$total_interaction_past_6_months,
				'interaction_chart'=>$interaction_chart,
				'deals_data'=>$deals_data,
			));
	}


	public function get_dealer_stats(Request $request){

		$dealer_id = $request->session()->get('dealer_id');

		$get_dealer = array(
			'dealer_id' => $dealer_id,
		);

		$response = self::run_api('search/car/active', $get_dealer);
		$response = json_decode($response);

		$dealer_data = new Collection();

		foreach($response->listings as $get_stats){
			$dealer_data->push(array(
				'id'=> $get_stats->id,
				'title'=> $get_stats->heading,
				'model'=> $get_stats->build->model,
				'stock'=> (isset($get_stats->stock_no)) ? $get_stats->stock_no : 'N/A',
				'days'=> $get_stats->dom . ' days',
			));
		}

		return Datatables::of($dealer_data)
			->addIndexColumn()
			->addColumn('interaction', function ($row) {
				$views = SearchCarViews::where('dealer_id',session()->get('dealer_id'))
					->where('car_id',$row['id'])
					->where('search_type','search')
					->get()
					->count('search_type');

				$click = SearchCarViews::where('dealer_id',session()->get('dealer_id'))
					->where('car_id',$row['id'])
					->where('search_type','detail')
					->get()
					->count('search_type');

				$saves = DB::table('client_favourite')
					->where('car_id',$row['id'])
					->get()
					->count('car_id');

				$html = '<i class="fa fa-binoculars" aria-hidden="true"></i> &nbsp;' .$views. '  
			 Views  | <i class="fa fa-link" aria-hidden="true"></i>&nbsp;' .$click. ' Websites Click
			 | <i class="fa fa-heart" aria-hidden="true"></i> &nbsp;' .$saves. ' Saves';

				return "<center>{$html}</center>";
			},NULL)

			->rawColumns(['interaction'])

			->make(true);
	}
}