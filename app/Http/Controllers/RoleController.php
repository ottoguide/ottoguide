<?php

namespace App\Http\Controllers;

use App\Helpers\Common;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller {

	/**
	 * RoleController constructor.
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}


	public function role_list() {
		return view('admin.role.list');
	}

	public function role_list_table() {
		$roles = Role::with('perms')->orderBy('id');

		return Datatables::of($roles)
			->addIndexColumn()
			->addColumn('system_defined', function ($row) {
				return ($row->system_defined == 1) ? 'Yes' : 'No';
			})
			->addColumn('permissions', function ($row) {
				$perm = '<ul>';

				if($row->name == 'admin') {
					$perm .= '<li>' . 'All permissions'.'</li>';
				} else {
					foreach($row->perms as $p) {
						$perm .= '<li>' . $p->display_name.'</li>';
					}
				}
				$perm .= '</ul>';
				return $perm;
			})
			->addColumn('action', function ($row) {
				$btns = '<a href="'.route('roles/edit/', ['role_id' => Common::my_id($row->id)]).'" class="btn btn-primary btn-xs">
					<i class="fa fa-pencil"></i> Edit
				</a>
				<a href="'.route('roles/remove/', ['role_id' => Common::my_id($row->id)]).'"
				   class="btn btn-danger btn-xs" onclick="return confirm(\'Are you sure you want to delete?\')">
					<i class="fa fa-times"></i> Delete
				</a>';
				if($row->system_defined == 1) {
					$btns = '';
				}
				return $btns;
			})
			->rawColumns(['action', 'permissions'])
			->make(true);
	}

	public function create() {
		$perm_list = Permission::get();
		$role_perms = array();
		return view('admin.role.create', compact('perm_list', 'role_perms'));
	}

	public function store(Request $request) {
		$this->validate($request, [
			'display_name' => 'required|max:255|unique:roles',
			'description' => 'required',
			'permissions' => 'required',
		]);


		$role = new Role();
		$role->name         = str_replace(' ', '_', trim(strtolower($request->input('display_name'))));
		$role->display_name = $request->input('display_name');
		$role->description  = $request->input('description');
		$role->save();

		foreach ($request->input('permissions') as $perm) {
			$p = Permission::findOrFail($perm);
			$role->attachPermission($p);
		}

		return redirect(route('roles_list'))->with('success', 'User role successfully created!');
	}

	public function edit($role_id) {
		$role = Role::with('perms')->where(Common::my_column(), $role_id)->first();
		$perm_list = Permission::get();

		$role_perms = array();
		foreach ($role->perms as $perm) {
			$role_perms[] = $perm->id;
		}

		return view('admin.role.edit', compact('role', 'perm_list', 'role_perms'));
	}

		public function update(Request $request) {
		$this->validate($request, [
			'display_name' => 'required|max:255',
			'description' => 'required',
			'permissions' => 'required',
		]);

		$role = Role::findOrFail($request->input('role_id'));
		$role->update([
			'name' => str_replace(' ', '_', trim(strtolower($request->input('display_name')))),
			'display_name' => $request->input('display_name'),
			'description' => $request->input('description')
		]);

		$role->detachPermissions(Permission::get()); // detach all permissions
		foreach ($request->input('permissions') as $perm) {
			$p = Permission::findOrFail($perm);
			$role->attachPermission($p);
		}

		return redirect(route('roles_list'))->with('success', 'User role successfully updated!');
	}

	public function remove($role_id) {
		$role = Role::where(Common::my_column(), $role_id);

		$role->delete();
		return redirect(route('user_list'))->with('success', 'User role successfully deleted!');
	}
}