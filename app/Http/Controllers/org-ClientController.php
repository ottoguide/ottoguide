<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Save_search;
use App\Models\Client_favourite;

class ClientController extends Controller
{
    public function saved_adverts()
    {    	
		return view('client/adverts');    	
    }

	public function save_searches()
    {	
		$save_search = New Save_search();
		$save_data = $save_search::paginate(2);
		return view('client/searches')->with('save_data',$save_data);
    }   	
	
	public function my_chat()
    {    	
		return view('client/chat');    	
    }   
	
	public function my_account()
    {
		return view('client/profile');    	
    }   
		
	public function my_history()
    {    	
		return view('client/history');    	
    }  	
	
	public function contact()
    {
		return view('client/contact');    	
    }  
	
	public function contact_request()
    {    	
		return view('client/contact_request');    	
    }  

   public function save_my_search(Request $request){   
              
     $saveSearch = new Save_search;      
     $saveSearch->save_search_name = $request->input('search_name');      
     $saveSearch->client_id = Auth::id();
     $saveSearch->save_search_url = $request->input('search_query');
     $saveSearch->json_data = "some data";
     $saveSearch->save();
     return redirect()->back()->withErrors('Search Saved successfully');
    }   

    public function save_my_car(Request $request){               
        $favourite = new Client_favourite;              
        $favourite->client_id = Auth::id();
	
        $favourite->car_id = $request->input('car_post_id');
        $favourite->json_data = $request->input('car_post_data');
        $data = $favourite->save();      
    }
}