<?php

namespace App\Http\Controllers;

use App\Helpers\Common;
use App\Models\Role;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Models\User;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_user() {
        return view('admin.user.list');
    }
	
	  public function list_client() {
		  
	if(Session::has('adminSession')){
	return view('admin.client.clientlist');
	}
	else{
	return redirect(route('login_page'));	
	}
		  
        
    }

	
	 public function client_list_table() {
		$users = User::where('is_admin',0)->get();
		return Datatables::of($users)
			->addIndexColumn()
			->editColumn('image', function ($row) {
				return '<img width="50" class="img-circle img-responsive" src="' . asset($row->image) . '">';
			})
		
			->addColumn('action', function ($row) {
				$btns = '<a href="'.route('client/detail/', ['user_id' =>encrypt($row->id)]).'" class="btn btn-primary btn-xs">
					<i class="fa fa-pencil"></i> Edit
				</a>
				<a href="'.route('client/remove/', ['user_id' => Common::my_id($row->id)]).'"
				   class="btn btn-danger btn-xs" onclick="return confirm(\'Are you sure you want to delete?\')">
					<i class="fa fa-times"></i> Delete
				</a>';
			
				return $btns;
			})
			->rawColumns(['action', 'image'])
			->make(true);
	}
	

	
    public function list_user_table() {
		$users = User::with('roles')->where('is_admin',1)->get();

		return Datatables::of($users)
			->addIndexColumn()
			->editColumn('image', function ($row) {
				return '<img width="50" class="img-circle img-responsive" src="' . asset($row->image) . '">';
			})
			->addColumn('role', function ($row) {
			$role= $row->roles->pluck('display_name');
			 return preg_replace('/[^A-Za-z0-9!@\-]/', '', $role);
			})
			->addColumn('action', function ($row) {
				$btns = '<a href="'.route('user/edit/', ['user_id' => Common::my_id($row->id)]).'" class="btn btn-primary btn-xs">
					<i class="fa fa-pencil"></i> Edit
				</a>
				<a href="'.route('user/remove/', ['user_id' => Common::my_id($row->id)]).'"
				   class="btn btn-danger btn-xs" onclick="return confirm(\'Are you sure you want to delete?\')">
					<i class="fa fa-times"></i> Delete
				</a>';
				if($row->system_defined == 1) {
					$btns = '';
				}
				return $btns;
			})
			->rawColumns(['action', 'image'])
			->make(true);
	}

	
	public function create() {
		$roles_list = [];
		foreach(Role::get() as $role){
			$roles_list[$role->id] = $role->display_name;
		}
		return view('admin.user.create', compact('roles_list'));
	}


	public function store(Request $request) {
    	$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'phone' => 'required|min:10',
			'password' => 'required|min:6|max:50|confirmed',
			'password_confirmation' => 'required'
		]);

		$folder = config('url.profile');
		$photoName = '15139235195a3ca3bfeb923.jpg'; // default photo name
    	if ($request->photos) {
			if (!$request->photos->isValid()) {
				return redirect(route('/user/store'));
			}

			$photoName = uniqid(time()).'.'.$request->photos->getClientOriginalExtension();

			$request->photos->move($folder, $photoName);
		}

		$token =$this->generateToken();

			$user = User::create([
				'api_token' => $token,
				'name' => $request->input('name'),
				'lname' => $request->input('lname'),
				'email' => $request->input('email'),
				'password' => bcrypt($request->input('password')),
				'phone' => $request->input('phone'),
				'mobile' => $request->input('mobile'),
				'image' => $folder . $photoName,
			]);
		
		
			$auth = Auth::user();
			if(!empty($auth)){
			if($auth->hasRole(['administrator'])){		
			User::where('id',$user->id)->update(array('is_admin' => '1'));
			}
			}
		
		
		$role = Role::findOrFail($request->input('role')); // Pull back a given role
		$user->attachRole($role);

		return redirect(route('user_list'))->with('success', 'User successfully created!');
	}

	public function generateToken()
    {
        return str_random(60);        
    }

	
	public function edit($user_id) {
		$user = User::with('roles')->where(Common::my_column(), $user_id)->first();
		$roles_list = [];
		foreach(Role::get() as $role){
			$roles_list[$role->id] = $role->display_name;
		}

		return view('admin.user.edit', compact('user', 'roles_list'));
	}
	
	
	public function client_edit($user_id) {
		$user = User::where(Common::my_column(), decrypt($user_id))->first();
		
		return view('admin.client.clientedit', compact('user'));
	}
	
	
	
	
	
	public function update(Request $request) {
		$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255',
			'phone' => 'required|min:10',
			'password' => 'required|min:6|max:50|confirmed',
			'password_confirmation' => 'required'
		]);

		$update_data = [
			'name' => $request->input('name'),
			'lname' => $request->input('lname'),
			'email' => $request->input('email'),
			'phone' => $request->input('phone'),
			'mobile' => $request->input('mobile'),
			'password' => bcrypt($request->input('password')),
		];

		if ($request->photos) {
			if (!$request->photos->isValid()) {
				return redirect(route('/user/list'));
			}

			$folder = config('url.profile');
			$photoName = uniqid(time()).'.'.$request->photos->getClientOriginalExtension();

			$request->photos->move($folder, $photoName);
			$update_data['image'] = $folder . $photoName;
		}

		$user = User::where('id', $request->input('user_id'))->first();
		$user->update($update_data);

		$role = Role::findOrFail($request->input('role'));
		$user->detachRoles(Role::get());
		$user->attachRole($role);

		return redirect(route('user_list'))->with('success', 'User successfully updated!');
	}

	
	
	public function client_update(Request $request) {
		$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255',
			'mobile' => 'required',
			'password' => 'required|min:6|max:50|confirmed',
			'password_confirmation' => 'required'
		]);

		$update_data = [
			'name' => $request->input('name'),
			'lname' => $request->input('lname'),
			'email' => $request->input('email'),
			'mobile' => $request->input('mobile'),
			'password' => bcrypt($request->input('password')),
		];

		if ($request->photos) {
			if (!$request->photos->isValid()) {
				return redirect(route('/user/list'));
			}

			$folder = config('url.profile');
			$photoName = uniqid(time()).'.'.$request->photos->getClientOriginalExtension();

			$request->photos->move($folder, $photoName);
			$update_data['image'] = $folder . $photoName;
		}

		$user = User::where('id', $request->input('user_id'))->first();
		$user->update($update_data);

	
		return redirect(route('client_list'))->with('success', 'Client successfully updated!');
	}
	

	public function remove($user_id) {
		$user = User::with('roles')->where(Common::my_column(), $user_id)->first();

		$user->delete();
		return redirect(route('user_list'))->with('success', 'User successfully deleted!');
	}
	
	
		public function client_remove($user_id) {
		$user = User::with('roles')->where(Common::my_column(), $user_id)->first();
		$user->delete();
		return redirect(route('client_list'))->with('success', 'Client successfully deleted!');
		}
	
}
