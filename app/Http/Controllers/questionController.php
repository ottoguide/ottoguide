<?php

namespace App\Http\Controllers;

use App\Helpers\Common;
use App\Models\Role;
use Illuminate\Http\Request;
use DB;
use App\Models\Questions;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

// TODO: class not in use
class questionController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function list_question() {
		return view('admin.questions.list');
	}


	public function list_question_table() {
		$questions = Questions::all();
		return Datatables::of($questions)
			->addIndexColumn()
			->addColumn('action', function ($row) {
				$btns = '<a href="' . route('question/edit/', ['question_id' => Common::my_id($row->id)]) . '" class="btn btn-primary btn-xs">
					<i class="fa fa-pencil"></i> Edit
				</a>
				<a href="' . route('question/remove/', ['question_id' => Common::my_id($row->id)]) . '"
				   class="btn btn-danger btn-xs" onclick="return confirm(\'Are you sure you want to delete?\')">
					<i class="fa fa-times"></i> Delete
				</a>';
				return $btns;
			})
			->rawColumns(['action', 'image'])
			->make(true);
	}

	public function create() {
		//return view('admin.questions.create');
		// redirect this page to edit page and load first question from db
		$questions = Questions::first();
		$question_id = $questions->id;
		return view('admin.questions.edit', compact('questions', 'question_id'));
	}


	public function store(Request $request) {
		$this->validate($request, [
			'question_final_array' => 'required',
		]);

		$question_text = json_decode($request->input('question_final_array'))[0]->question;
		$question = new Questions();
		$question->question = $question_text;
		$question->option = $request->input('question_final_array');
		$question->save();

		return redirect(route('create_questions'))->with('success', 'Question successfully created!');
	}


	public function edit($question_id) {
		//$questions = Questions::where(Common::my_column(), $question_id)->first();
		$questions = Questions::first();
		$question_id = $questions->id;
		return view('admin.questions.edit', compact('questions', 'question_id'));
	}


	public function update(Request $request) {
		$this->validate($request, [
			'question_final_array' => 'required',
		]);

		$question_text = json_decode($request->input('question_final_array'))[0]->question;
		$update_data = [
				'question' => $question_text,
				'option' => $request->input('question_final_array'),
				'updated_at' => date('Y-m-d H:i:s')
		];

		$question = Questions::where('id', $request->input('question_id'));
		/*echo '<pre>';
		print_r($question->first());
		die();*/
		$question->update($update_data);
		return redirect(route('create_questions'))->with('success', 'Question successfully updated!');
	}


	public function remove($question_id) {
		$question = Questions::where(Common::my_column(), $question_id)->first();
		$question->delete();
		return redirect(route('question_list'))->with('success', 'Question successfully deleted!');
	}


}
