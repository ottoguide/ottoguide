<?php

namespace App\Http\Controllers;
use App\Http\Middleware\DealerAuth;
use App\Models\DealerApi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use App\Models\Dealer;
use App\Models\SubDealer;
use App\Models\SearchCarViews;
use App\Models\CarReview;
use App\Helpers\ApiCaller;
use App\Models\CarRating;
use App\Models\ReviewRespond;
use App\Models\ContactRequest;
use App\Models\DealerSearchCar;
use App\Mail\register_dealer;
use App\Mail\contactrequests;
use App\Mail\email_dealer;
use App\Mail\forget_password;
use Illuminate\Pagination\LengthAwarePaginator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Redirect;
use Mail;
use Session;
use DB;
use Common;


define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class DealerController extends Controller {

    protected $chargebeeUrl;
    protected $chargebeeAuth;
    protected $chargebeePlanId;

    public function __construct()
    {
        $this->chargebeePlanId = 'registered_dealer_plan';
//      $this->chargebeeUrl = 'https://ottoguide.chargebee.com/api/v2/';
//		$this->chargebeeAuth = 'Authorization: Basic bGl2ZV9yZzdCTjRkQzZQV1JOTkhzWU5WNUJjZEo2SzhsUEFuNUs6'; // Authorization crated by postman for Live
//        $this->chargebeeUrl = 'https://ottoguide-test.chargebee.com/api/v2/';
//        $this->chargebeeAuth = 'Authorization: Basic dGVzdF9wbGN1cE5jZGIwaDlkMkdaeWtJZHQxc3E3Y2RzUG9ZWG1lRzo='; // Authorization crated by postman for Dev
        $this->chargebeeUrl = env('CHARGE_BEE_URL', '');
        $this->chargebeeAuth = 'Authorization: Basic '.env('CHARGE_BEE_AUTH', '');
    }

    public static function run_api($url, $params = array()) {
		$final_url = API_URL . $url . '?api_key=' . API_KEY;
		if ($params && count($params) > 0) {
			$final_url .= '&' . http_build_query($params);
		}

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $final_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Host: marketcheck-prod.apigee.net"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			echo "cURL Error #:" . $err;
			exit;
		}
		return $response;
	}

	public function dealer_signup() {
        return view('dealer/signup');
	}

	public function dealer_terms() {
        return view('dealer/terms');
	}

	protected function signup_validator(Request $request) {
		$validator = Validator::make($request->all(), [
			'dealer_name' => 'required|max:255',
			'dealer_contact_name' => 'required|max:255',
			'email' => 'required|email|max:255',
			'password' => 'required|min:6',
			'confirm_password' => 'required|same:password',
			'dealer_phone' => 'required|min:10',
		]);
		if ($validator->passes())
			return response()->json(['success' => true]);
		else
			return response()->json(['error' => $validator->errors()->all()]);
	}

	
	
	  protected function subdealer_signup_validator(Request $request) {
		$validator = Validator::make($request->all(), [
			'subdealer_name' => 'required|max:255',
			'email' => 'required|email|max:255',
			'password' => 'required|min:6',
			'confirm_password' => 'required|same:password',
			'subdealer_phone' => 'required|min:10',
		]);
		if ($validator->passes())
			return response()->json(['success' => true]);
		else
			return response()->json(['error' => $validator->errors()->all()]);
	    }
		
		
		// sub dealer edit validate
	  protected function subdealer_edit_validator(Request $request) {
		$validator = Validator::make($request->all(), [
			'subdealer_name' => 'required|max:255',
			'email' => 'required|email|max:255',
			'subdealer_phone' => 'required|min:10',
		]);
		if ($validator->passes())
			return response()->json(['success' => true]);
		else
			return response()->json(['error' => $validator->errors()->all()]);
	    }
	
	
	
	
	protected function save_dealer(Request $request) {

		$dealer = Dealer::where('dealer_email',$request->input('email'))->first();

		if(!empty($dealer)) {

			if($dealer->is_registered == 0){	

			    // Creating Customer and Subscription in ChargeBee
			    $chargeBee = $this->chargeBeeCustomer($dealer->dealer_id,$request->input('dealer_name'),$request->input('email'));

				$token = uniqid() . time();
				$dealer_email = $request->input('email');
				$dealer_phone = $request->input('dealer_phone');
				$dealer_password = md5($request->input('password'));

				$dealer_data = array(
					'dealer_email' => $request->input('email'),
					'dealer_name' => $request->input('dealer_name'),
					'dealer_contact_name' => $request->input('dealer_contact_name'),
					'password' => $dealer_password,
					'login_phone' => $request->input('dealer_phone'),
					'registration_pin' => $token
				);

				Dealer::where('dealer_email', $request->input('email'))
					->update(array(
						'customer_id' => $chargeBee['customer_id'],
						'subscription_id' => $chargeBee['subscription_id'],
//						'dealer_email' => $dealer_email,
						'login_phone' => $dealer_phone,
						'password' => $dealer_password,
						'is_registered' => 1,
						'market_analysis_addon' => 1,
						'rbo_lead_addon' => 1,
						'dealer_pricing_tool_addon' => 1,
						'voice_lead_addon' => 1,
						'email_lead_addon' => 1,
						'activity_notify_addon' => 1,
						'chat_lead_addon' => 1,
						'registration_pin'=> $token,
						'updated_at'=> date('Y-m-d H:i:s')));

			$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
	
				Mail::to($request->input('email'))
				->cc($get_admin_email->email)
				->send(new register_dealer($dealer_data));
				
				return redirect('dealer/login')->with('status', 'Please check your email for activation');
			} else{
				return redirect('dealer/login')->with('status', 'You are already registered. Please login to your account!');
			}
		}
		else{

            $dealer_api = DealerApi::where('dealer_email',$request->input('email'))->first();

            if(!empty($dealer_api)){
                // Insert dealer data into dealer table
                $dealer = new Dealer();
                $dealer->dealer_id = $dealer_api->dealer_id;
                $dealer->dealer_name = $dealer_api->dealer_name;
                $dealer->dealer_email = $dealer_api->dealer_email;
                $dealer->dealer_phone = $dealer_api->dealer_phone;
                $dealer->dealer_country = $dealer_api->dealer_country;
                $dealer->dealer_state = $dealer_api->dealer_state;
                $dealer->dealer_city = $dealer_api->dealer_city;
                $dealer->dealer_latitude = $dealer_api->dealer_latitude;
                $dealer->dealer_longitude = $dealer_api->dealer_longitude;
                $dealer->dealer_zip = $dealer_api->dealer_zip;
                $dealer->dealer_street = $dealer_api->dealer_street;
                $dealer->dealer_website = $dealer_api->dealer_website;
                $dealer->is_block = $dealer_api->is_block;
                $dealer->market_analysis_addon = $dealer_api->market_analysis_addon;
                $dealer->rbo_lead_addon = $dealer_api->rbo_lead_addon;
                $dealer->dealer_pricing_tool_addon = $dealer_api->dealer_pricing_tool_addon;
                $dealer->voice_lead_addon = $dealer_api->voice_lead_addon;
                $dealer->email_lead_addon = $dealer_api->email_lead_addon;
                $dealer->activity_notify_addon = $dealer_api->activity_notify_addon;
                $dealer->chat_lead_addon = $dealer_api->chat_lead_addon;
                $dealer->is_registered = $dealer_api->is_registered;
                $dealer->save();

                // Creating Customer and Subscription in ChargeBee
                $chargeBee = $this->chargeBeeCustomer($dealer_api->dealer_id,$request->input('dealer_name'),$request->input('email'));

                $token = uniqid() . time();
                $dealer_email = $request->input('email');
                $dealer_phone = $request->input('dealer_phone');
                $dealer_password = md5($request->input('password'));

                $dealer_data = array(
                    'dealer_email' => $request->input('email'),
                    'dealer_name' => $request->input('dealer_name'),
                    'dealer_contact_name' => $request->input('dealer_contact_name'),
                    'password' => $dealer_password,
                    'login_phone' => $request->input('dealer_phone'),
                    'registration_pin' => $token
                );

                Dealer::where('dealer_email', $request->input('email'))
                    ->update(array(
                        'customer_id' => $chargeBee['customer_id'],
                        'subscription_id' => $chargeBee['subscription_id'],
//						'dealer_email' => $dealer_email,
                        'login_phone' => $dealer_phone,
                        'password' => $dealer_password,
                        'is_registered' => 1,
                        'market_analysis_addon' => 1,
                        'rbo_lead_addon' => 1,
                        'dealer_pricing_tool_addon' => 1,
                        'voice_lead_addon' => 1,
                        'email_lead_addon' => 1,
                        'activity_notify_addon' => 1,
                        'chat_lead_addon' => 1,
                        'registration_pin'=> $token,
                        'updated_at'=> date('Y-m-d H:i:s')));

                $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();

                Mail::to($request->input('email'))
                    ->cc($get_admin_email->email)
                    ->send(new register_dealer($dealer_data));

                return redirect('dealer/login')->with('status', 'Please check your email for activation');

            }
            else{
                return redirect('dealer/login')->with('status', 'You are not registered in our database');
            }


		}
	}


	
	public function verify_dealer($token) {
		$dealer = Dealer::where('registration_pin', $token)->first();
		if (isset($dealer)) {
			Dealer::where('dealer_id', $dealer->dealer_id)
				->update(array(
					'registration_pin' => null,
					'is_registered' => 1,
				));

			// login page
			return redirect('dealer/login')->with('status', 'Successfully verified. Please login to your account');
		} else {
			return view('errors/404');
		}
	}

	public function dealer_login() {
		return view('dealer/login');
	}

	protected function login_validator(Request $request) {
		$validator = Validator::make($request->all(), [
			'email' => 'required|email',
			'password' => 'required|min:6',
		]);
		
		if ($validator->passes()) {
		
	  ############# CHECK SUBDEALER
	  
	  $check_subdealer = DB::table('dealer_subaccount')
	 ->where('email',$request->Input('email'))->where('password',md5($request->Input('password')))->first();
	  
	  if(count($check_subdealer)){
		
		  $dealer = SubDealer::where('email', $request->Input('email'))
				->where('password', md5($request->Input('password')))
				->first();  
			
	      }
	  ############# END CHECK SUBDEALER
	  
	    else{
			 
	      $dealer = Dealer::where('dealer_email', $request->Input('email'))
				->where('password', md5($request->Input('password')))
				->where('is_registered', 1)
				->first();  
		  
	      }

			if (isset($dealer)) {
				return response()->json(['success' => true]);
			} else {
				return response()->json(['error' => ['Invalid login details!']]);
			}
		} else {
			return response()->json(['error' => $validator->errors()->all()]);
		}
	}

	
	
	
	protected function login_submit(Request $request) {
		
	  $check_subdealer = DB::table('dealer_subaccount')
	 ->where('email',$request->Input('email'))->where('password',md5($request->Input('password')))->first();
		
		if(count($check_subdealer)){
		
		$request->session()->put('is_dealer_login', 1);
		$request->session()->put('dealer_id', $check_subdealer->dealer_id);

		$request->session()->put('subdealer',$request->Input('email'));
		
		return redirect('dealer/portal');
		
		}	
	
	
	else{		
		
		$dealer = Dealer::where('dealer_email', $request->Input('email'))
			->where('password', md5($request->Input('password')))
			->where('is_registered', 1)
			->first();

		$request->session()->put('is_dealer_login', 1);
		$request->session()->put('dealer_id', $dealer->dealer_id);
		
		return redirect('dealer/portal');
		
		
	}	
		
	}
	
	
	// forgot password
	
	public function forgot_password_page(){
	
	 return view('dealer/forgot_password');
	
	}
	
	public function forgot_password(Request $request){
		
	$dealer_email =  $request->input('email');
		
	$check_dealer_email = DB::table('dealer')->where('dealer_email',$dealer_email)->get()->first();
	
	if(count($check_dealer_email)){
		
	$pin = rand( 10000 , 99999 );

    Dealer::where('dealer_email',$dealer_email)->update(array('reset_password_token' => $pin ));	
	

	// get admin email	
	 $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
	
	 $data =[
	'dealer_name'=> $check_dealer_email->dealer_name,
	'pin'=> $pin
	
	 ];
	
	// Send Pin to dealer email
	Mail::to($dealer_email)->cc($get_admin_email->email)->send(new forget_password($data));	

    return view('dealer/update-password')->with('status', 'Pin code has been sent to your mention email address. Kindly put pin code and new password');
		
	}
	
	else{
		return redirect('/dealer/forgot/password/')->with('status', 'Please put valid email');
	}
	
		
		
	}
	
	
	
	public function update_password(Request $request){
		
	   $pin = $request->input('pin');
	   $newpassword =  md5($request->input('newpassword'));		
	   
		   $update_data = [
		   'password' => $newpassword,
		   'reset_password_token' => ''
			];
	    
	    $update_password = Dealer::where('reset_password_token',$pin);
		$update_password->update($update_data);
	
	    return redirect('dealer/login')->with('status', 'Password has been check now you can login with your new password.');
	  
	} 
	   
	
	
	public function dealer_logout(Request $request) {
		
		$request->session()->put('is_dealer_login', 0);
		$request->session()->forget('dealer_id');
		
		$request->session()->forget('subdealer');

		return redirect('dealer/login')->with('status', 'Successfully logout to your account');;
	}

	
	
	
	public function subdealer_form(){
	return view('dealer/subdealer_signup');	
	}
	
	public function subdealer_list(){
	return view('dealer/subdealer-list');	
	}
	
	
	public function subdealer_list_table(){										

	 $get_subdealers = DB::table('dealer_subaccount')
	 ->where('dealer_id', Session::get('dealer_id'))
	 ->get();
		
		return Datatables::of($get_subdealers)
			
			->addIndexColumn()
		
		
			->addColumn('action', function ($row) {

				
			$html = '<center><a href="'.route('subdealer-delete', ['subdealer' => encrypt($row->id)]).'" ><button type="button" class="btn btn-danger btn-sm align-middle">Delete</button></a>
			<a href="'.route('subdealer-edit', ['subdealer' => encrypt($row->id)]).'" ><button type="button" class="btn btn-success btn-sm align-middle">Edit</button></a>
			';
	
				return "{$html}</center>";
						
			},NULL)
			
		
		
			->make(true);
	
	}
	
	
	protected function save_subdealer(Request $request) {
		
	$save_subdealer = new SubDealer();	
	
	$save_subdealer->name = $request->input('subdealer_name');
	$save_subdealer->email = $request->input('email');
	$save_subdealer->phone = $request->input('subdealer_phone');
	$save_subdealer->dealer_id = Session::get('dealer_id');
	$save_subdealer->password = md5($request->input('password'));
	$save_subdealer->save();
	
	return redirect('subdealer/list')->with('message', 'Sub Dealer account has been registered.');
	
	}
	
	
	// edit sub dealer
	protected function subdealer_edit(Request $request) {
	
	if(Session::get('dealer_id')){
    $get_subdealer = SubDealer::where('id', decrypt($request->get('subdealer')))->first();
	}
	return view('dealer/subdealer_edit')->with('get_subdealer',$get_subdealer);
	
	}
	
	
	
	// delete sub dealer
	protected function subdealer_update(Request $request) {
	
	if(Session::get('dealer_id')){

	$get_subdealer_data = new SubDealer();
	
	        $update_data = [
				'name' => $request->input('subdealer_name'),
				'email' => $request->input('email'),
				'phone' => $request->input('subdealer_phone'),
				'updated_at' => date('Y-m-d H:i:s'),
			 ];
			
			
	 $update_subdealer = $get_subdealer_data::where('id',$request->input('subdealer_id'));
	 $update_subdealer->update($update_data);	
	
	
	 if(!empty($request->input('password'))){
		 
	 $get_subdealer_data::where('id',$request->input('subdealer_id'))->update(array('password' => md5($request->input('password'))));
	 
	 }
	  else
	   {
	 // Old password update
	   }	
	
	 }
	
	 return redirect('subdealer/list')->with('message', 'Sub Dealer account has been deleted successfully.');
	
	 }
	
	
	// delete sub dealer
	protected function subdealer_delete(Request $request) {
	
	if(Session::get('dealer_id')){
    SubDealer::where('id', decrypt($request->get('subdealer')))->delete();
	}
	return redirect('subdealer/list')->with('message', 'Sub Dealer account has been deleted successfully.');
	
	}
	
	
	public function dealer_reviews() {
		
	return view('dealer/dealer-reviews');
	
	
	}

	public function user_reviews_table(Request $request){	
	
	$dealer_id = $request->session()->get('dealer_id');

	
		$get_dealer = array(

			'dealer_id' => $dealer_id,
			'sort_order' => 'asc',
			'rows' => 1,
			
		);

/* $params = array(
			'dealer_id' => $dealer_id,
			'start' => $page,
			'rows' => $limit,
			'sort_by' => 'price',
			'sort_order' => 'asc',
		); */
		
		
		$response = self::run_api('search/car/active', $get_dealer);

		$response = json_decode($response);

	
		$dealer_data = new Collection();


		foreach($response->listings as $get_stats){

			$dealer_data->push(array(

				'image'=> '<img src="'.$get_stats->media->photo_links[0].'" width="150" height="100">',
			
				'id'=> (isset($get_stats->id)) ? $get_stats->id : 'N/A',

				'title'=> (isset($get_stats->heading)) ? $get_stats->heading : 'N/A',

				'model'=>(isset($get_stats->build->model)) ? $get_stats->build->model : 'N/A',

				'stock'=> (isset($get_stats->stock_no)) ? $get_stats->stock_no : 'N/A',
				
			));

		}
	
	
	return Datatables::of($dealer_data)

			->addIndexColumn()

			->addColumn('Reviews', function ($row) {

				$count_reviews = CarReview::where('car_id',$row['id'])

					->where('message_by','client')

					->get()

					->count('client');


				$html = '<h2>'.$count_reviews.' </h2><h6> Reviews </h6>  

				<br> <a href="review/details/'.$row["id"].'"><button type="button" class="btn btn-success btn-sm align-middle">View Reviews</button></a>';

				return "<center>{$html}</center>";
						
			},NULL)
			
			
			->addColumn('Rating', function ($row) {

				$avg_count = CarReview::where('car_id',$row['id'])->avg('rating');
				
				if(empty($avg_count)){
				$avg_count = 0;	
				}
				$rt=round($avg_count);
				$img="";
				$i=1;
				while($i<=$rt){
				$img=$img."<img src=/public/images/star.png>";
				$i=$i+1;
				}
				while($i<=5){
				$img=$img."<img src=/public/images/star2.png>";
				$i=$i+1;
				}
				
				$html = '<h2><br>'.$img.' </h2><h6> Rating </h6>';

				return "<center>{$html}</center>";
						
				
			  },NULL)
			
			
			
			
			->addColumn('Action', function ($row) {

			$check_block_status = DB::table('settings')->where('car_id',$row['id'])->where('reviews_off',1)->get()->first();
		
			$check_reviews = CarReview::where('car_id',$row['id'])

					->where('message_by','client')

					->get()

					->count('client');
					
					
				if(empty($check_reviews)) { 
				
				if(!empty($check_block_status)){
				
				$html = '
				<br>
				<p><input type="checkbox" name="reviews_on" class="reviews_on" id="reviews_on" value="'.$row['id'].'"  ><br>
				<small><b style="color:#23a127;">Reviews Unblock</b></small> </p>';
				return "<center>{$html}</center>";		
				}
			  
			    else{
					
				$html = '
				<br>
				<p><input type="checkbox" name="reviews_off" class="reviews_off" id="reviews_off" value="'.$row['id'].'"  > <br>
				<small><b style="color:#920101;"> Reviews Block</b></small>
				</p>';
				return "<center>{$html}</center>";		
				
				 }
				
			
				}
			
				else{
		
				$html = '
				<br>
				<p><input type="checkbox" name="review_temp" class="temp_chk" checked disabled><br>
				<small>Not allowed to block reviews </small></p>
				';
				return "<center>{$html}</center>";		
				
				 }
					
			},NULL)
			
			
			->rawColumns(['Reviews','image','Rating','Action'])

			//->rawColumns(['image'])

			->make(true);

	}

	public function respond_save(Request $request) {
		$this->validate($request, [
			'respond_heading' => 'required|max:255',
			'respond_message' => 'required',
		]);
		$ReviewRespond = new ReviewRespond;
		$ReviewRespond->review_id = $request->input('review_id');
		$ReviewRespond->parent_id = $request->input('parent_id');
		$ReviewRespond->dealer_id = $request->input('dealer_id');
		$ReviewRespond->client_id = $request->input('client_id');
		$ReviewRespond->title = $request->input('respond_heading');
		$ReviewRespond->message = $request->input('respond_message');
		$ReviewRespond->save();
		return redirect()->back()->with('message', 'Replied has been sent successfully');
	}

	public function dealer_billing_tool(Request $request) {


        $market_analysis_addon = [];
        $rbo_lead_addon = [];
        $dealer_pricing_tool_addon = [];
        $voice_lead_addon = [];
        $email_lead_addon = [];
        $activity_notify_addon = [];
        $chat_lead_addon = [];

        $dealer_id = $request->session()->get('dealer_id');
        $dealer = DB::table('dealer')->where('dealer_id', $dealer_id)->first();

        $chargeBeeAddons = $this->chargeBeeAddonsPricing();

        foreach ($chargeBeeAddons['list'] as $addon){

            if($addon['addon']['id'] == 'market_analysis_addon'){

                $market_analysis_addon = [
                    'market_analysis_addon' => $dealer->market_analysis_addon,
                    'market_analysis_addon_price' => number_format($addon['addon']['price'] / 100,2)
                ];

            }

            if($addon['addon']['id'] == 'rbo_lead_addon'){

                $rbo_lead_addon = [
                    'rbo_lead_addon' => $dealer->rbo_lead_addon,
                    'rbo_lead_addon_price' => number_format($addon['addon']['price'] / 100,2)
                ];

            }

            if($addon['addon']['id'] == 'dealer_pricing_tool_addon'){

                $dealer_pricing_tool_addon = [
                    'dealer_pricing_tool_addon' => $dealer->dealer_pricing_tool_addon,
                    'dealer_pricing_tool_addon_price' => number_format($addon['addon']['price'] / 100,2)
                ];

            }

            if($addon['addon']['id'] == 'voice_lead_addon'){

                $voice_lead_addon = [
                    'voice_lead_addon' => $dealer->voice_lead_addon,
                    'voice_lead_addon_price' => number_format($addon['addon']['price'] / 100,2)
                ];

            }

            if($addon['addon']['id'] == 'email_lead_addon'){

                $email_lead_addon = [
                    'email_lead_addon' => $dealer->email_lead_addon,
                    'email_lead_addon_price' => number_format($addon['addon']['price'] / 100,2)
                ];

            }

            if($addon['addon']['id'] == 'activity_notify_addon'){

                $activity_notify_addon = [
                    'activity_notify_addon' => $dealer->activity_notify_addon,
                    'activity_notify_addon_price' => number_format($addon['addon']['price'] / 100,2)
                ];

            }

            if($addon['addon']['id'] == 'chat_lead_addon'){

                $chat_lead_addon = [
                    'chat_lead_addon' => $dealer->chat_lead_addon,
                    'chat_lead_addon_price' => number_format($addon['addon']['price'] / 100,2)
                ];

            }


        }

        $addons = array_merge(
            $market_analysis_addon,
            $rbo_lead_addon,
            $dealer_pricing_tool_addon,
            $voice_lead_addon,
            $email_lead_addon,
            $activity_notify_addon,
            $chat_lead_addon
        );

        // Dealer Invoices
        $dealer_subscription_id = $dealer->subscription_id;
        $dealer_invoices = $this->getDealerInvoices($dealer_subscription_id);

        if(!empty($dealer_invoices['list'])){
            $dealer_invoice = $dealer_invoices['list'];
        }
        else{
            $dealer_invoice = [];
        }


		return view('dealer/dealer-billing', ['addons' => $addons, 'invoices' => $dealer_invoice]);
	}

	public function add_addon_subscriptions(Request $request){

        $dealer_id = $request->session()->get('dealer_id');
        Dealer::where('dealer_id', $dealer_id)
            ->update(array(
                "".$request->addon_id => 1,
                'updated_at'=> date('Y-m-d H:i:s'))
            );

        DB::table('dealer_subscription_logs')->insert(
            [
                'dealer_id' => $dealer_id,
                'subscription_name' => $request->addon_id,
                'subscription_action' => 'add',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ]
        );

        $response = array(
            'status' => 'success',
            'msg' => $request->addon_id,
        );

        return response()->json($response);
    }

    public function remove_addon_subscriptions(Request $request){

        $dealer_id = $request->session()->get('dealer_id');
        Dealer::where('dealer_id', $dealer_id)
            ->update(array(
                    "".$request->addon_id => 0,
                    'updated_at'=> date('Y-m-d H:i:s'))
            );


        DB::table('dealer_subscription_logs')->insert(
            [
                'dealer_id' => $dealer_id,
                'subscription_name' => $request->addon_id,
                'subscription_action' => 'remove',
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ]
        );

        $response = array(
            'status' => 'success',
            'msg' => $request->addon_id,
        );

        return response()->json($response);
    }

	public function dealer_pricing_tool_x(Request $request) {
		$dealer_id = $request->session()->get('dealer_id');
		
		//$getDealer = DealerSearchCar::where('dealer_id', $dealer_id)->pluck('dealer_id');
		$getSearches = DealerSearchCar::where('dealer_id', $dealer_id)->limit(3)->get();
		$datas = array();

		//echo '<pre>'; print_r($request->input('filter_price')); die();

		if (count($getSearches)) {
			$data = array();
			foreach ($getSearches as $getSearch) {
				$car_id = $getSearch->car_id;
				$search_car_data = json_decode($getSearch->car_data);

				if ($request->has('filter_price')) {
					if (isset($search_car_data->price)) {
						$price = $search_car_data->price;
					} else if (isset($search_car_data->ref_price)) {
						$price = $search_car_data->ref_price;
					} else {
						$price = 0;
					}

					$filter_price = explode('-', $request->input('filter_price'));
					if ($price < $filter_price[0] || $price > $filter_price[1]) {
						continue;
					}
				}

				if ($request->has('filter_miles')) {
					$miles = 0;
					if (isset($search_car_data->miles)) {
						$miles = $search_car_data->miles;
					}
					$filter_miles = explode('-', $request->input('filter_miles'));
					if ($miles < $filter_miles[0] || $miles > $filter_miles[1]) {
						continue;
					}
				}


				if ($request->has('filter_transmission') && $request->input('filter_transmission') != 'Any') {
					if (isset($search_car_data->build) &&
						isset($search_car_data->build->transmission) &&
						$search_car_data->build->transmission != $request->input('filter_transmission')) {
						continue;
					}
				}

				if ($request->has('filter_market_days')) {
					$days = 0;
					if (isset($search_car_data->scraped_at_date)) {
						$count_date = explode('.', $search_car_data->scraped_at_date)[0];
						$count_date = strtotime(str_replace('T', ' ', $count_date));
						$datediff = time() - $count_date;
						$days = round($datediff / (60 * 60 * 24));
					} else if (isset($search_car_data->last_seen_at_date)) {
						$count_date = explode('.', $search_car_data->last_seen_at_date)[0];
						$count_date = strtotime(str_replace('T', ' ', $count_date));
						$datediff = time() - $count_date;
						$days = round($datediff / (60 * 60 * 24));
					} else if (isset($search_car_data->first_seen_at_date)) {
						$count_date = explode('.', $search_car_data->first_seen_at_date)[0];
						$count_date = strtotime(str_replace('T', ' ', $count_date));
						$datediff = time() - $count_date;
						$days = round($datediff / (60 * 60 * 24));
					} else {
						continue;
					}


					$filter_days = explode('-', $request->input('filter_market_days'));
					if ($days < $filter_days[0] || $days > $filter_days[1]) {
						continue;
					}
				}

				if ($request->has('filter_color')) {
					if (!isset($search_car_data->exterior_color) ||
						(isset($search_car_data->exterior_color) && !in_array($search_car_data->exterior_color, $request->input('filter_color')))) {
						continue;
					}
				}

				if ($request->has('filter_engine')) {
					if (!isset($search_car_data->build->engine) ||
						(isset($search_car_data->build->engine) && !in_array($search_car_data->build->engine, $request->input('filter_engine')))) {
						continue;
					}
				}

				$url = API_URL . "listing/car/" . $car_id . "?api_key=" . API_KEY;
				$response = self::run_api($url);
				$get_data = json_decode($response);
				//echo '<pre>'; print_r($get_data); die();

				if (isset($get_data->price)) {
					$price = $get_data->price;
				} else if (isset($get_data->ref_price)) {
					$price = $get_data->ref_price;
				} else {
					$price = 0;
				}

				$miles = 0;
				if (array_key_exists('miles', $get_data)) {
					$miles = $get_data->miles;
				}


				$market_days = 'N/A';
				if (isset($search_car_data->scraped_at_date)) {
					$count_date = explode('.', $search_car_data->scraped_at_date)[0];
					$count_date = strtotime(str_replace('T', ' ', $count_date));
					$datediff = time() - $count_date;
					$market_days = round($datediff / (60 * 60 * 24));
				} else if (isset($search_car_data->last_seen_at_date)) {
					$count_date = explode('.', $search_car_data->last_seen_at_date)[0];
					$count_date = strtotime(str_replace('T', ' ', $count_date));
					$datediff = time() - $count_date;
					$market_days = round($datediff / (60 * 60 * 24));
				} else if (isset($search_car_data->first_seen_at_date)) {
					$count_date = explode('.', $search_car_data->first_seen_at_date)[0];
					$count_date = strtotime(str_replace('T', ' ', $count_date));
					$datediff = time() - $count_date;
					$market_days = round($datediff / (60 * 60 * 24));
				}

				if (array_key_exists('id', $get_data) &&
					array_key_exists('heading', $get_data) &&
					array_key_exists('build', $get_data) &&
					array_key_exists('media', $get_data) &&
					$price > 0) {

					$images = array(asset('public/images/no-image.jpeg'));
					if (isset($get_data->media) && isset($get_data->media->photo_links) && count($get_data->media->photo_links) > 0) {
						$images = $get_data->media->photo_links;
					}

					$datas[] = array(
						'car_id' => $get_data->id,
						'title' => $get_data->heading,
						'price' => $price,
						'build' => $get_data->build,
						'miles' => $miles,
						'market_days' => $market_days,
						'pics' => $images,
						'search_data' => $getSearch
					);
				}

			}
		}
		//echo '<pre>'; print_r($datas); die();
		return view('dealerpanel/dealer-pricing', compact('datas'));
	}

	public function dealer_update_car_price_deal(Request $request) {
		$dealer_id = $request->session()->get('dealer_id');
		$new_price = $request->input('new_price');
		$car_id = $request->input('car_id');
		$dealer_data = Dealer::where('dealer_id', $dealer_id)->first();

		$getSearch = DealerSearchCar::where('dealer_id', $dealer_id)->where('car_id', $car_id)->first();

		$get_data = json_decode($getSearch->car_data);

		$return_html = '<i class="fa fa-ban text-danger"></i>';
		$return_html .= '<p>NO PRICE</p>';

		if (isset($get_data->build) && isset($get_data->build->make) && isset($get_data->build->model) && isset($get_data->build->year) && isset($get_data->build->trim)) {

			$deal_params = array(
				'rows' => 1,
				'sort_by' => 'price',
				'sort_order' => 'asc',
				'make' => $get_data->build->make,
				'model' => $get_data->build->model,
				'year' => $get_data->build->year,
				'trim' => $get_data->build->trim,
				'radius' => $dealer_data->radius,
			);

			if (isset($get_data->inventory_type)) {
				$deal_params['car_type'] = $get_data->inventory_type;
			}

			$deal_response = self::run_api('search/car/active', $deal_params);
			$deal_response = json_decode($deal_response);

			if (!isset($deal_response->code) && count($deal_response->listings) > 0) {
				$other_deal_car = $deal_response->listings[0];
				$other_car_price = (isset($other_deal_car->price)) ? $other_deal_car->price : (isset($other_deal_car->ref_price)) ? $other_deal_car->ref_price : 0;

				if ($other_car_price > 0) {
					$percent = (($new_price - $other_car_price) * 100) / $other_car_price;
					if ($percent <= 10) {
						$deal = 'great';
						$return_html = '<i class="fa fa-arrow-down text-success"></i>';
					} else if ($percent <= 30) {
						$deal = 'good';
						$return_html = '<i class="fa fa-arrow-down text-success"></i>';
					} else if ($percent <= 60) {
						$deal = 'average';
						$return_html = '<i class="fa fa-arrows-v text-warning"></i>';
					} else {
						$deal = 'high';
						$return_html = '<i class="fa fa-arrow-up text-danger"></i>';
					}
					$return_html .= '<p>' . strtoupper($deal) . ' PRICE</p>';

					$getSearch->car_price = $new_price;
					$getSearch->car_deal = $deal;
					$getSearch->car_deal_data = json_encode($other_deal_car);
					$getSearch->save();


				} else {
					$return_html = '<i class="fa fa-ban text-danger"></i>';
					$return_html .= '<p>NO PRICE</p>';
					$getSearch->car_price = $new_price;
					$getSearch->save();
				}
			} else {
				$getSearch->car_price = $new_price;
				$getSearch->save();
			}
		}

		echo $return_html;
		die();
	}

	public function dealer_pricing_tool(Request $request) {

		$dealer_id = $request->session()->get('dealer_id');

        // Validation to check if dealer subscribed to pricing tool addon
        $dealer_data = DB::table('dealer')->where('dealer_id', $dealer_id)->first();
        if($dealer_data->dealer_pricing_tool_addon == 0){
            Session::flash('message', 'You are not subscribed to Dealer Pricing Tool');
            Session::flash('alert-class', 'alert-danger');
            return redirect('dealer/portal');
        }

		$limit= ($request->input('per_page') ? $request->input('per_page') : 10);
		$page = ($request->input('page') ? (($request->input('page') * 10) - 10) : 0 );

		$params = array(
			'dealer_id' => $dealer_id,
			'start' => $page,
			'rows' => $limit,
			'sort_by' => 'price',
			'sort_order' => 'asc',
		);

		if ($request->has('sort_by')) {
			$sort = explode('_', $request->input('sort_by'));
			$params['sort_by'] = strtolower($sort[0]);
			$params['sort_order'] = strtolower($sort[1]);
		}
		if ($request->has('filter_price')) {
			$params['price_range'] = $request->input('filter_price');
		}
		if ($request->has('filter_miles')) {
			$params['miles_rage'] = $request->input('filter_miles');
		}
		if ($request->has('filter_transmission') && strtolower($request->input('filter_transmission')) != 'any') {
			$params['transmission'] = strtolower($request->input('filter_transmission'));
		}
		if ($request->has('filter_market_days')) {
			$params['dom_range'] = $request->input('filter_market_days');
		}
		if ($request->has('filter_color')) {
			$params['exterior_color'] = implode(',', $request->input('filter_color'));
		}
		if ($request->has('filter_engine')) {
			$params['engine'] = strtolower($request->input('filter_engine'));
		}


		// new car search
		if ($request->has('car_type')) {
			$params['car_type'] = strtolower($request->input('car_type'));
		}
		if ($request->has('make')) {
			$params['make'] = strtolower($request->input('make'));
		}
		if ($request->has('model')) {
			$params['model'] = strtolower($request->input('model'));
		}
		if ($request->has('zip')) {
			$params['zip'] = $request->input('zip');
		}
		if ($request->has('radius')) {
			$params['radius'] = $request->input('radius');
		}


		if ($request->has('body_type')) {
			$params['body_type'] = strtolower($request->input('body_type'));
		}

		if ($request->has('year_from') && (!$request->has('year_to') || $request->has('year_to') == '')) {
			$params['year'] = $request->input('year_from');
		} else if ($request->has('year_to') && (!$request->has('year_from') || $request->has('year_from') == '')) {
			$params['year'] = $request->input('year_to');
		} else if ($request->has('year_from') && $request->has('year_to')) {
			$years = range ($request->input('year_to'), $request->input('year_from'));
			$params['year'] = implode(',', $years);
		}

		if ($request->has('price_from') && (!$request->has('price_to') || $request->has('price_to') == '')) {
			$params['price_range'] = $request->has('price_from') . '-' . 1000000;
		} else if ($request->has('price_to') && (!$request->has('price_from') || $request->has('price_from') == '')) {
			$params['price_range'] = 0 . '-' . $request->has('price_to');
		} else if ($request->has('price_from') && $request->has('price_to')) {
			$params['price_range'] = $request->has('price_from') . '-' . $request->has('price_to');
		}

		if ($request->has('max_miles')) {
			$params['miles_rage'] = 0 . '-' . $request->input('max_miles');
		}


		$response = self::run_api('search/car/active', $params);
		$response = json_decode($response);

		if (isset($response->code)) {
			die($response->message);
		}
		//echo '<pre>'; print_r($params); die();

		$total_records = $response->num_found;

		$datas = array();
		foreach ($response->listings as $get_data) {
			$getSearch = DealerSearchCar::where('dealer_id', $dealer_id)->where('car_id', $get_data->id)->first();

			if (!isset($getSearch) || empty($getSearch)) {
				$dealerCarRecord = new DealerSearchCar();
				$dealerCarRecord->dealer_id = $dealer_id;
				$dealerCarRecord->type = 'search';
				$dealerCarRecord->car_id = $get_data->id;
				$dealerCarRecord->car_price = (isset($get_data->price)) ? $get_data->price : (isset($get_data->ref_price)) ? $get_data->ref_price : 0;
				$dealerCarRecord->search_rank = 'N/A';
				$dealerCarRecord->search_page = 'N/A';
				$dealerCarRecord->car_data = json_encode($get_data);
				$dealerCarRecord->save();

				$getSearch = DealerSearchCar::where('dealer_id', $dealer_id)->where('car_id', $get_data->id)->first();
			}

			$price = $getSearch->car_price;


			// deal calculation
			if ($getSearch->car_price == 0 && (isset($get_data->price) || isset($get_data->ref_price))) {
				$price = (isset($get_data->price)) ? $get_data->price : (isset($get_data->ref_price)) ? $get_data->ref_price : 0;


				if (isset($get_data->build) && isset($get_data->build->make) && isset($get_data->build->model) && isset($get_data->build->year) && isset($get_data->build->trim)) {
					$dealer_data = Dealer::where('dealer_id', $dealer_id)->first();

					$deal_params = array(
						'rows' => 1,
						'sort_by' => 'price',
						'sort_order' => 'asc',
						'make' => $get_data->build->make,
						'model' => $get_data->build->model,
						'year' => $get_data->build->year,
						'trim' => $get_data->build->trim,
						//'price_range' => $price . '-' . 100000000,
						'radius' => $dealer_data->radius,
					);

					if (isset($get_data->inventory_type)) {
						$deal_params['car_type'] = $get_data->inventory_type;
					}

					$deal_response = self::run_api('search/car/active', $deal_params);
					$deal_response = json_decode($deal_response);

					if (!isset($deal_response->code) && count($deal_response->listings) > 0) {
						$other_deal_car = $deal_response->listings[0];
						$other_car_price = (isset($other_deal_car->price)) ? $other_deal_car->price : (isset($other_deal_car->ref_price)) ? $other_deal_car->ref_price : 0;

						if ($other_car_price > 0) {
							$percent = (($price - $other_car_price) * 100) / $other_car_price;
							if ($percent <= 10) {
								$deal = 'great';
							} else if ($percent <= 30) {
								$deal = 'good';
							} else if ($percent <= 60) {
								$deal = 'average';
							} else {
								$deal = 'high';
							}
							$getSearch->car_price = $price;
							$getSearch->car_deal = $deal;
							$getSearch->car_deal_data = json_encode($other_deal_car);
							$getSearch->save();
						} else {
							$getSearch->car_price = $price;
							$getSearch->save();
						}
					} else {
						$getSearch->car_price = $price;
						$getSearch->save();
					}
				} else {
					$getSearch->car_price = $price;
					$getSearch->save();
				}
			}


			$miles = 0;
			if (array_key_exists('miles', $get_data)) {
				$miles = $get_data->miles;
			}

			$market_days = 'N/A';
			if (isset($get_data->dom)) {
				$market_days = $get_data->dom;
			}

			$images = array(asset('public/images/no-image.jpeg'));
			if (isset($get_data->media) && isset($get_data->media->photo_links) && count($get_data->media->photo_links) > 0) {
				$images = $get_data->media->photo_links;
			}
			$datas[] = array(
				'car_id' => $get_data->id,
				'title' => $get_data->heading,
				'price' => $price,
				'miles' => $miles,
				'market_days' => $market_days,
				'pics' => $images,
				'search_data' => $getSearch
			);
		}

		$datas = new LengthAwarePaginator($datas, $total_records, $limit, $page, [
			'path' => LengthAwarePaginator::resolveCurrentPath(),
			'query' => $request->query(),
		]);
		return view('dealerpanel/dealer-pricing', compact('datas', 'total_records'));
	}

    protected function check_dealer(Request $request){
        
		$dealer = Dealer::where('dealer_id',$request->input('dealer_id'))->first();
        
		echo $dealer->is_registered;
		
/* 		
		// email to unregistered dealer	

         if($dealer->is_registered == '0'){
     
		$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();

		$dealer_email = Dealer::where('dealer_id',$request->input('dealer_id'))->first();
	 
		Mail::to($dealer_email->dealer_email)->cc($get_admin_email->email)->send(new contactrequests($data));			
		
        }   */
		
        exit;
    }

    public function chargeBeeCustomer($dealer_id,$first_name,$email){

        $subscription_id = '';

        $data_to_post = array(
			'id' => $dealer_id,
			'first_name' => $first_name,
            'email' => $email,
            'auto_collection' => 'off'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->chargebeeUrl. "customers");
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data_to_post));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array($this->chargebeeAuth));
        $result = json_decode(curl_exec($ch), true);
        $customer_id = $result['customer']['id'];

        if($result['customer']['id'] != ''){

            $data_to_post = array(
                'plan_id' => $this->chargebeePlanId,
                'start_date' => time()
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$this->chargebeeUrl. "customers/" . $customer_id . "/subscriptions");
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data_to_post));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($ch, CURLOPT_HTTPHEADER,array($this->chargebeeAuth));
            $result = json_decode(curl_exec($ch), true);
            $subscription_id = $result['subscription']['id'];

        }

        return array('customer_id' => $customer_id, 'subscription_id' => $subscription_id);
    }

    public function chargeBeeAddonsPricing(){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->chargebeeUrl. "addons?status%5Bis%5D=active");
        curl_setopt($ch, CURLOPT_HTTPHEADER,array($this->chargebeeAuth));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return json_decode(curl_exec($ch), true);

    }

    public function invoice_url(Request $request){

        $data_to_post = array(
            'customer[id]' => $request->customer_id
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->chargebeeUrl. "hosted_pages/collect_now");
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data_to_post));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array($this->chargebeeAuth));
        return curl_exec($ch);

//        $result = json_decode(curl_exec($ch), true);
//        $customer_id = $result['hosted_page']['url'];
//        return $customer_id;

    }

    public function getDealerInvoices($dealer_subscription_id){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->chargebeeUrl. "invoices?subscription_id[is]=".$dealer_subscription_id);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array($this->chargebeeAuth));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        return json_decode(curl_exec($ch), true);
//        return curl_exec($ch);
    }

    public function dealer_update_car_price_deal_addon_log(Request $request){
        $dealer_id = $request->session()->get('dealer_id');
        $new_price = $request->input('new_price');
        $car_id = $request->input('car_id');
        $getSearch = DealerSearchCar::where('dealer_id', $dealer_id)->where('car_id', $car_id)->first();
        $get_data = json_decode($getSearch, true);

        $msg = "no change";
        if($new_price == $get_data['car_price']){
            $msg = "car price change";
            Common::addon_inc($dealer_id,'dealer_pricing_tool_addon',1);

        }

        return $msg;

    }
}