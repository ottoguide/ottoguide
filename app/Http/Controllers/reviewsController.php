<?php

namespace App\Http\Controllers;

use App\Helpers\Common;
use App\Models\Role;
use Illuminate\Http\Request;
use DB;
use App\Models\AdminReviewControl;
use App\Models\Subscription;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;


class reviewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function reviews_report() {
		
      return view('admin.reviews.list');
    }
	
	
    public function reviews_report_table() {
		
		$reviews_report = DB::table('admin_review_control')
		->orderby('admin_review_control.id','desc')
		->join('client_reviews','admin_review_control.message_id','=','client_reviews.id')		
		->join('users','admin_review_control.client_id','=','users.id')
		->join('dealer','admin_review_control.dealer_id','=','dealer.dealer_id')
		->get();
		
		return Datatables::of($reviews_report)
			
			->addIndexColumn()
		
			 ->addColumn('action', function ($row) {
		
			  $action_list = '<select class="review_action">
			  <option value="">Select Action</option>
			  <option value="current-vehicle"'.str_replace('1','selected disabled',$row->block_vehicle).'>Current Vehicle</option>
			  <option value="current-dealer"'.str_replace('1','selected disabled',$row->block_current_dealer).'>Current Dealer</option>
			  <option value="all-dealer" '.str_replace('1','selected disabled',$row->block_all_dealers).'>All Dealers</option>
			  </select>

			<input type="hidden" class="car_id" value="'.$row->car_id.'">
			<input type="hidden" class="dealer_id" value="'.$row->dealer_id.'">
			<input type="hidden" class="client_id" value="'.$row->client_id.'">
			<input type="hidden" class="message_id" value="'.$row->message_id.'">';
				return $action_list;
			}) 
			 ->rawColumns(['action','reply'])
			 ->make(true);
	}

		
    public function reviews_user_block(Request $request){
	
	$block_user_table = new AdminReviewControl();
	
 	 if($request->input('status_post')== 'current-vehicle'){
			 
	   $update_data = [
	   'block_vehicle' => 1,
	   'block_current_dealer' => 0,
	   'block_all_dealers' => 0,
	   'reason' => $request->input('reason'),
	   ];

	 }	

	if($request->input('status_post')== 'current-dealer'){
		
	   $update_data = [
	   'block_vehicle' => 0,
	   'block_current_dealer' => 1,
	   'block_all_dealers' => 0,
	   'reason' => $request->input('reason'),
	   ];
	  
	}
	
	if($request->input('status_post')== 'all-dealer'){
		
	    $update_data = [
	   'block_vehicle' => 0,
	   'block_current_dealer' => 0,
	   'block_all_dealers' => 1,
	   'reason' => $request->input('reason'),
	   ];
	  
	} 
			
	$block_user = $block_user_table::where('client_id',$request->input('client_id_post'))
	->where('car_id',$request->input('car_id_post'));
	$block_user->update($update_data);

		
    
	}
	
		
	
	}
	
