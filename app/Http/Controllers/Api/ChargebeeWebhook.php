<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ChargebeeWebhook extends Controller
{
    protected $chargebeeUrl;
    protected $chargebeeAuth;
    protected $invoice_id;
    protected $dealer_id;

    public function __construct()
    {
//      $this->chargebeeUrl = 'https://ottoguide.chargebee.com/api/v2/';
//		$this->chargebeeAuth = 'Authorization: Basic bGl2ZV9yZzdCTjRkQzZQV1JOTkhzWU5WNUJjZEo2SzhsUEFuNUs6'; // Authorization crated by postman for Live
//      $this->chargebeeUrl = 'https://ottoguide-test.chargebee.com/api/v2/';
//      $this->chargebeeAuth = 'Authorization: Basic dGVzdF9wbGN1cE5jZGIwaDlkMkdaeWtJZHQxc3E3Y2RzUG9ZWG1lRzo='; // Authorization crated by postman for Dev
        $this->chargebeeUrl = env('CHARGE_BEE_URL', '');
        $this->chargebeeAuth = 'Authorization: Basic '.env('CHARGE_BEE_AUTH', '');
    }

    public function chargebeeWebhooks(Request $request){

        if($request->event_type == 'pending_invoice_created'){

            $this->invoice_id = $request->content['invoice']['id'];
            $customer_id = $request->content['invoice']['customer_id'];
            $subscription_id = $request->content['invoice']['subscription_id'];

            $dealer = DB::table('dealer')
                ->where('customer_id',$customer_id)
                ->where('subscription_id',$subscription_id)
                ->first();
            $this->dealer_id = $dealer->dealer_id;
            $dealer_market_analysis_addon = $dealer->market_analysis_addon;


            $all_addons = DB::table('dealer_addon_logs')
                ->select(DB::raw('addon, SUM(units) AS total_units'))
                ->where('dealer_id',$this->dealer_id)
                ->where('created_at', '>', date('Y-m-d',strtotime('-1 month')))
                ->where('created_at', '<', date('Y-m-d',strtotime('+1 day')))
                ->groupBy('addon')
                ->get();


            $api_status = false;
            $api_failed_count = 0;
            if(!empty($all_addons)){
                foreach ($all_addons as $customer_addon){

                    if($customer_addon->addon != 'market_analysis_addon'){

                        $data_to_post = [
                            'addon_id' => $customer_addon->addon,
                            'addon_quantity' => $customer_addon->total_units
                        ];

                        $httpcode = $this->add_addon_item_to_pending_invoice($data_to_post);

                        if($httpcode == 200){
                            $api_status = true;
                        }
                        else{
                            $api_failed_count++;
                        }
                    }

                }
            }



            if($dealer_market_analysis_addon == 0){
                // ToDo: Check market analysis from dealer table, if is disabled then check hiostory if it was enabled in last month

                $dealer_subscription_logs = DB::table('dealer_subscription_logs')
                    ->select('id')
                    ->where('dealer_id',$this->dealer_id)
                    ->where('subscription_action','add')
                    ->where('subscription_name','market_analysis_addon')
                    ->where('created_at', '>', date('Y-m-d',strtotime('-1 month')))
                    ->where('created_at', '<', date('Y-m-d',strtotime('+1 day')))
                    ->first();

                if(!empty($dealer_subscription_logs->id) && $dealer_subscription_logs->id > 0){

                    $data_to_post = [
                        'addon_id' => 'market_analysis_addon',
                        'addon_quantity' => ''
                    ];

                    $httpcode = $this->add_addon_item_to_pending_invoice($data_to_post);

                    if($httpcode == 200){
                        $api_status = true;
                    }
                    else{
                        $api_failed_count++;
                    }

                }

            }
            else{

                $data_to_post = [
                    'addon_id' => 'market_analysis_addon',
                    'addon_quantity' => ''
                ];

                $httpcode = $this->add_addon_item_to_pending_invoice($data_to_post);

                if($httpcode == 200){
                    $api_status = true;
                }
                else{
                    $api_failed_count++;
                }
            }


            $closed_invoice_id = $this->close_invoice($this->invoice_id);



            if($api_status == true && $api_failed_count == 0){
                $resp = ["status" => true, "msg" => "success", "invoice_id" => $closed_invoice_id];
            }
            else{
                $resp = ["status" => false, "msg" => "something went wrong!!"];
            }

            return $resp;

        }

        return null;

    }


    public function add_addon_item_to_pending_invoice($data_to_post)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->chargebeeUrl. "invoices/".$this->invoice_id."/add_addon_charge");
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data_to_post));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array($this->chargebeeAuth));
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


        // Inserting API Logs in Database
        DB::table('dealer_backend_api_responses')->insert(
            [
                'dealer_id' => $this->dealer_id,
                'source' => 'ChargebeeWebhook',
                'req' => http_build_query($data_to_post),
                'res' => $result,
                'http_code' => $httpcode,
                'created_at'=> date('Y-m-d H:i:s'),
                'updated_at'=> date('Y-m-d H:i:s')
            ]
        );

        return $httpcode;

    }

    public function close_invoice($invoice_id){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->chargebeeUrl. "invoices/".$invoice_id."/close");
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array($this->chargebeeAuth));
        $result = json_decode(curl_exec($ch), true);
        return $result['invoice']['id'];
    }
}
