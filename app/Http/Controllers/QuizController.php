<?php


namespace App\Http\Controllers;

use App\Helpers\Common;
use App\Models\Role;
use Illuminate\Http\Request;
use DB;
use App\Models\Questions;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Upload;


class QuizController extends Controller {

    public function preview_quiz($quiz_id){
        $question = Questions::where(Common::my_column(), $quiz_id)->first();
        return view('admin.quiz.preview')->with('quiz', $question);
    }

	public function list_quiz() {
//		$quiz = Questions::all();
		$quiz = DB::table('quiz')
            ->where('is_deleted',0)
            ->get();

		return view('admin.quiz.list')->with('quiz', $quiz);
	}

	public function add_quiz(Request $request) {
		$validator = Validator::make($request->all(), [
			'quiz_title' => 'required',
			'quiz_description' => 'required',
		]);

		if ($validator->fails()) {
			if($request->ajax()){
				return response()->json(['error'=>$validator->errors()->all()]);
			}
			$this->throwValidationException(
				$request, $validator
			);
		}
		if($request->ajax()){
			return response()->json(['success'=>'Record is successfully added']);
		}


        $quiz = new Questions();
        $quiz->title = $request->input('quiz_title');
        $quiz->description = $request->input('quiz_description');
        $quiz->save();

//		return redirect(url('/quiz/' . Common::my_id($quiz->id)));

        $questions = DB::table('quiz_detail')->where('quiz_id', '=', $quiz->id)->get();
        $routes = DB::table('quiz_routes')->where('quiz_id', '=', $quiz->id)->get();
        return redirect(url('/quiz/' . Common::my_id($quiz->id) . '/edit/'))
            ->with('quiz', $quiz)
            ->with('questions', $questions)
            ->with('routes', $routes);
	}

	public function create_quiz($quiz_id) {
		$question = Questions::where(Common::my_column(), $quiz_id)->first();
		return view('admin.quiz.create')->with('quiz', $question);
	}

	public function save_quiz(Request $request){
        $quiz_id = $request->id;
        DB::table('quiz_detail')->where('quiz_id', '=', $quiz_id)->delete();
        $questions = json_decode($request->quizQuestions);
        $rowOrder = 1;
        foreach ($questions as $question){

            $values = '';
            if(isset($question->values) && count($question->values) > 0){
                $values = implode (",", $question->values);
            }

            $values_labels = '';
            if(isset($question->values_labels) && count($question->values_labels) > 0){
                $values_labels = implode (",", $question->values_labels);
            }

            $query_values = '';
            if(isset($question->query_values) && count($question->query_values) > 0){
                $query_values = implode ("|", $question->query_values);
            }


            DB::table('quiz_detail')->insert([
                'quiz_id' => $quiz_id,
                'question_id' => $question->id,
                'is_required' => $question->required,
                'is_query' => $question->query,
                'query_param' => $question->query_param,
                'question_text' => $question->question,
                'question_description' => $question->description,
                'question_type' => $question->type,
                'question_subtype' => $question->subtype,
                'question_options' => $values,
                'question_options_label' => $values_labels,
                'question_query_values' => $query_values,
                'question_order' => $rowOrder,
                'rating_img' => $question->rating_img,
                'min_number' => $question->min_number,
                'max_number' => $question->max_number,
                'variable_key' => $question->variable_key,
                'variable_value' => $question->variable_value
                ]);

            $rowOrder++;
        }

        return json_encode(array('status'=>'success'));

    }

	public function save_quiz_routes(Request $request){
        $quiz_id = $request->id;

        $isQuiz = DB::table('quiz_detail')->where('quiz_id', '=', $quiz_id)->first();

        if(empty($isQuiz)){
            return json_encode(array('status'=>'failed','msg'=>'save quiz before routes'));
            die();
        }

        DB::table('quiz_routes')->where('quiz_id', '=', $quiz_id)->delete();
        $routes = json_decode($request->routes);
        foreach ($routes as $route){
            $select_question='';
            $select_response='';
            $next_question='';
            foreach ($route as $key=>$val){
                if($key == 'select_question'){
                    $select_question = $val;
                }
                if($key == 'select_response'){
                    $select_response = $val;
                }
                if($key == 'next_question'){
                    $next_question = $val;
                }
            }

            if($select_question != '' && $select_response != '' && $next_question!= ''){
                DB::table('quiz_routes')->insert([
                    'quiz_id' => $quiz_id,
                    'select_question' => $select_question,
                    'select_response' => $select_response,
                    'next_question' => $next_question
                ]);
            }

        }


        return json_encode(array('status'=>'success'));

    }

    public function edit_quiz($quiz_id){
        $quiz = Questions::where(Common::my_column(), $quiz_id)->first();
        $questions = DB::table('quiz_detail')->where('quiz_id', '=', $quiz->id)->get();
        $routes = DB::table('quiz_routes')->where('quiz_id', '=', $quiz->id)->get();
        $sets = DB::table('quiz_question_set')->groupBy('set_id')->get();

        return view('admin.quiz.update')
            ->with('quiz', $quiz)
            ->with('questions', $questions)
            ->with('sets', $sets)
            ->with('routes', $routes);
    }

    public function publish_quiz(Request $request){

        $is_active = 0;
        if($request->status == 1){
            $is_active = 1;
        }

        DB::table('quiz')
            ->where('id', $request->id)
            ->update([
            'is_active' => $is_active
        ]);

        return json_encode(array('is_active'=>$is_active, 'id'=>$request->id));
    }

    public function upload_file(Request $request){
        // file name
        $uploadedFile = $request->file('file');
        $filename = time().$uploadedFile->getClientOriginalName();
        $file_extension = strtolower($uploadedFile->getClientOriginalExtension());
        $image_ext = array("jpg","png","jpeg","gif");

        $response = 0;
        if(in_array($file_extension,$image_ext)) {
            $destinationPath = public_path('images');
            $uploadedFile->move($destinationPath,$filename);
            $response = "/images/".$filename;
        }

        echo $response;

    }

    public function save_images(Request $request)
    {
        $image = $request->file('file');
        $imageName = time().$image->getClientOriginalName();
        $image->move(public_path('images'),$imageName);

        DB::table('quiz_images')->insert([
            'image_title' => $imageName,
            'image_url' => url("/images/".$imageName),
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        return response()->json(['success'=>$imageName]);
    }

    public function list_images(){

        $images = DB::table('quiz_images')->get();
        return json_encode($images);
    }

    public function save_image_label(Request $request){
        DB::table('quiz_images')->where('id',$request->id)->update(['image_title' => $request->label]);
    }

    public function update_quiz_welcome_status(Request $request){
        DB::table('quiz')->where('id',$request->id)->update(['welcome_screen' => $request->status]);
    }

    public function update_quiz_data(Request $request){
        DB::table('quiz')
            ->where('id',$request->id)
            ->update([
                'welcome_screen' => $request->status,
                'welcome_msg' => $request->welcomeMessage,
                'thankyou_msg' => $request->thankyouMessage,
                'welcome_description' => $request->welcomeDescription,
                'variable' => $request->welcomeVariables,
                'media_url' => $request->welcomeMedia,
                'action_button_label' => $request->welcomeActionButtonLabel,
                'facebook_link' => $request->facebookLink,
                'twitter_link' => $request->twitterLink,
                'google_link' => $request->googleLink,
                'linkedin_link' => $request->linkedinLink,
                'youtube_link' => $request->youtubeLink,
                'instagram_link' => $request->instagramLink,
                'pinterest_link' => $request->pinterestLink,
            ]);
    }

    public function list_question_sets(){

        $sets = DB::table('quiz_question_set')->groupBy('set_id')->get();
        return json_encode($sets);
    }

    public function list_all_question_sets(){

        $sets = DB::table('quiz_question_set')->get();
        return json_encode($sets);
    }

    public function save_question_set(Request $request){
        $data = $request->question;
        $questionSetName = $request->questionSetName;
        $questionSetId = $request->questionSetId;
        $question_id = $request->id;

        $exQuestion = DB::table('quiz_question_set')
            ->where('set_id',$questionSetId)
            ->where('question_id',$question_id)
            ->first();

        if(empty($exQuestion)){
            DB::table('quiz_question_set')->insert([
                'question_data' => $data,
                'question_id' => $question_id,
                'set_id' => $questionSetId,
                'set_name' => $questionSetName
            ]);

            $msg = 'Question successfully added to Set: '.$questionSetName;
        }
        else{
            $msg = 'Question already exists in this set. Set Name: '.$questionSetName;

        }



        return json_encode(array('msg'=>$msg));
    }

    public function delete_quiz($quiz_id){
        DB::table('quiz')
            ->where(Common::my_column(), $quiz_id)
            ->update(['is_deleted' => 1]);

        return redirect('quiz/list');
    }


}