<?php
namespace App\Http\Controllers\Auth;
use App\Models\User;
use auth;
use App\Models\SocialProvider;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Mail;
use Session;
use App\Mail\register_user;
use App\Mail\register_user_from_search;
use App\Mail\register_user_from_search_admin;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
			'lastname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone' => 'required|min:10|unique:users',
        ]);
    }


    protected function register_validator(Request $request){
      //  return response()->json(['error'=>User::all()->toArray()]);
     //   die;

        $validator = Validator::make($request->all(), [            
            'name' => 'required|max:255',
			'lastname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'phone' => 'required|min:10|unique:users',
        ]);        

        if ($validator->passes())
        return response()->json(['success'=>true]);

        else
        return response()->json(['error'=>$validator->errors()->all()]);
    }   

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
	 
	 public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    } 
	 
	 
	 
    protected function create(array $data)
    {
        $confirmation_code = str_random(30);
        
        $user =  User::create([
            'name' => $data['name'],
			'lastname' => $data['lastname'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'confirmation_code' =>$confirmation_code,
            'password' => bcrypt($data['password']),            
        ]);
	
		    $data =[
			'name'=>$data['name'],
			'email'=> $data['email'],
			'confirmation_code' =>$confirmation_code
			];
			
			
			if(session::has('search_button_session')){
		    	
	    	$data =[
			'name'=>$data['name'],
			'email'=> $data['email'],
			'confirmation_code' => $confirmation_code,
			'car_title'=> Session::get('search_button_session')['session_car_title'],
			'car_id'=> Session::get('search_button_session')['session_car_id'],
			'car_vin'=> Session::get('search_button_session')['session_car_vin'],
			'status'=> Session::get('search_button_session')['session_status'],
			'radius'=> Session::get('search_button_session')['session_radius'],
			'latitude'=>  Session::get('search_button_session')['session_latitude'],	
			'longitude'=> Session::get('search_button_session')['session_longitude'],
			'make'=> Session::get('search_button_session')['session_make'],
			'modal'=> Session::get('search_button_session')['session_modal'],
			'trim'=> Session::get('search_button_session')['session_trim'],
			'body'=> Session::get('search_button_session')['session_body'],
			'vehicle_type'=> Session::get('search_button_session')['session_vehicle_type'],
			'miles'=> Session::get('search_button_session')['session_miles'],
			'inventory_type'=> Session::get('search_button_session')['session_inventory_type'],
			'color'=> Session::get('search_button_session')['session_color'],	
			'dealer_id'=> Session::get('search_button_session')['session_dealer_id'],
			'dealer_name'=> Session::get('search_button_session')['session_dealer_name'],
			'dealer_city'=> Session::get('search_button_session')['session_dealer_city'],
			'dealer_street'=> Session::get('search_button_session')['session_dealer_street'],
			'dealer_state'=> Session::get('search_button_session')['session_dealer_state'],
			'dealer_lat'=> Session::get('search_button_session')['session_dealer_lat'],	
			'dealer_long'=> Session::get('search_button_session')['session_dealer_long'],
			'dealer_zip'=> Session::get('search_button_session')['session_dealer_zip'],
			'dealer_phone'=> Session::get('search_button_session')['session_dealer_phone'],
			'dealer_website'=> Session::get('search_button_session')['session_dealer_website'],			
			];


			 $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();

			// mail to user
			 Mail::to($data['email'])
			->send(new register_user_from_search($data));  
		
			
			// mail to admin
			 Mail::to($get_admin_email->email)
			->send(new register_user_from_search_admin($data));  
			}
		
			else{
			
			$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();

			//send contact request to dealer email
			
			 Mail::to($data['email'])
			->bcc($get_admin_email->email)
			->send(new register_user($data));  

			}
			
        return redirect('/login')->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');
    }

    public function verifyUser($confirmation_code){
		
	
        $verifyUser = User::where('confirmation_code', $confirmation_code)->first()->toArray();
        if($verifyUser){

            if($verifyUser['verified']==0 ) {                
                User::where('confirmation_code', $confirmation_code)
                ->update(['verified' => 1]);
                $status = "Your e-mail is verified. You can now login.";
                
            }else{
                $status = "Your e-mail is already verified. You can now login.";                
            }
        }else{
            echo "Sorry your email cannot be identified"; die; 
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        } 
        return redirect('/login')->with('status', $status);
    }
	
	
	
	 public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
	
	
	public function handleProviderCallback($provider)
    {
        try
        {
            $socialUser = Socialite::driver($provider)->user();
        }
        catch(\Exception $e)
        {
            return redirect('/');
        }
        //check if we have logged provider
        $socialProvider = SocialProvider::where('provider_id',$socialUser->getId())->first();
        if(!$socialProvider)
        {
            //create a new user and provider
            $user = User::firstOrCreate(
                ['email' => $socialUser->getEmail()],
                ['name' => $socialUser->getName()]
            );

            $user->socialProviders()->create(
                ['provider_id' => $socialUser->getId(), 'provider' => $provider]
            );

        }
        else
            $user = $socialProvider->user;

        auth()->login($user);

        return redirect('/home');

    }
	
	

}