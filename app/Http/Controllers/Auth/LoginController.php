<?php
namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected function authenticated(Request $request)
    {

        return redirect('/home');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		$this->redirectTo = url()->previous();
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function loginApi(Request $request) {
        
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {

            $user = $this->guard()->user();           

            $api_token = bcrypt(uniqid(time()));
			
			$user->update(['api_token' => $api_token]);

			$user_data = $user->toArray();

			//$user_data['image'] = asset($user_data['image']);
			
			unset($user_data['created_at']);
			unset($user_data['updated_at']);

            return response()->json([
                'status' => 1,
                'message' => 'Login Successfully',
                'data' => $user_data,
            ]);
        }

        //return $this->sendFailedLoginResponse($request);
        return response()->json([
            'status' => 0,
            'message' => 'Invalid login details',
            'data' => [],
        ]);
    }

    public function login_validate(Request $request){

        $validator = Validator::make($request->all(), [            
            'email' => 'required|email',
            'password' => 'required',
        ]);        

		$is_user_active = DB::table('users')->where('email',$request->input('email'))->where('verified',0)->get();
		
		if(count($is_user_active)){
		
		return response()->json(['error'=>array('Your account is not activated')]);   	
			
		}
		
		else {
		
        if ($validator->passes()){
            if(!$this->attemptLogin($request))
            return response()->json(['error'=>array('Invalid login details')]);        

            else
            return response()->json(['success'=>true]);
        }

        else
        return response()->json(['error'=>$validator->errors()->all()]);
    }
	
}



}