<?php

namespace App\Http\Controllers;

use App\Helpers\Common;
use App\Models\Role;
use Illuminate\Http\Request;
use DB;
use App\Models\AdminReviewControl;
use App\Models\TwilioChat;
use App\Models\Subscription;
use App\Models\UserUnblockRequest;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;


class chatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function chat_block() {
		
     return view('admin.chat.list');
    }
	
	
    public function chat_block_table() {
		
		$chat_block_list = DB::table('twilio_chat')
		->orderby('twilio_chat.id','desc')
		->join('users','twilio_chat.user_id','=','users.id')		
		->join('dealer','twilio_chat.dealer_id','=','dealer.dealer_id')
		->get();
		
	
		
		return Datatables::of($chat_block_list)
			
			->addIndexColumn()
		
			 ->addColumn('action', function ($row) {
			
			  $action_list = '<select class="chat_block_action">
			  <option value="">Select Action</option>
			  <option value="current-dealer"'.str_replace('1','selected disabled',$row->is_block_current).'>Current Dealer</option>
			  <option value="all-dealer"'.str_replace('1','selected disabled',$row->is_block_all).'>All Dealers</option>
			  <option value="user-unblock"'.str_replace('0',' disabled',$row->is_block_current).'>User Unblock</option>
			  </select>

			<input type="hidden" class="chat_block_id" value="'.$row->id.'">
			<input type="hidden" class="user_id" value="'.$row->user_id.'">
			<input type="hidden" class="dealer_id" value="'.$row->dealer_id.'">
			';
				return $action_list;
			}) 
		
			 ->rawColumns(['action'])
			 ->make(true);
	}

	
	
	 public function report_channel(Request $request){
			

	 $chat_channel = $request->input('report_channel');
    
	 
	 DB::table('twilio_chat')->where('chat_channel',$chat_channel)->update(array('reported_reason' => 'Dealer request to block this user','is_block_request' => '1'));

	 }
	
	
		
    public function chat_user_block(Request $request){
	
	$block_user_table = new TwilioChat();
	
 	 if($request->input('status')== 'current-dealer'){
		 
	   $update_data = [
	   'is_block_current' => 1,
	   'is_block_all' => 0
	   ];
	   
	 $block_user = $block_user_table::where('user_id',$request->input('user_id'))
	 ->where('dealer_id',$request->input('dealer_id'));
	 $block_user->update($update_data);
	 
	 }	

	if($request->input('status')== 'all-dealer'){
		
	   $update_data = [
	   'is_block_all' => 1,
	   'is_block_current' => 0,
	   ];
	 
	$block_user = $block_user_table::where('user_id',$request->input('user_id'));
	$block_user->update($update_data);  
	  
	}
	
	if($request->input('status')== 'user-unblock'){
		
	   $update_data = [
	   'is_block_all' => 0,
	   'is_block_current' => 0,
	   ];
	 
	$block_user = $block_user_table::where('user_id',$request->input('user_id'));
	$block_user->update($update_data);  
	
	
	DB::table('user_unblock_request')->where('user_id',$request->input('user_id'))->update(array('status' => 'unblocked'));
	  
	}
		

	
    
	}
	
		
		
		
		
	public function	delete_chat_channel(){
		
		
	 return view('client.channel-delete');	
		
		
	}
		
		
		
		
		
		
		
		
	
	}
	
