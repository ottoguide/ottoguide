<?php

namespace App\Http\Controllers;

use App\Helpers\Common;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\AdminReviewControl;
use App\Models\TwilioChat;
use App\Helpers\ApiCaller;
use App\Models\Subscription;
use App\Models\RBO;
use App\Models\RBO_QUOTE;
use App\Models\CarCompare_temp;
use App\Models\CarCompare;
use App\Models\UserUnblockRequest;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Mail;
use Session;
use DB;
use View;
use App\Mail\rbo_thankyou_email_send;
use App\Mail\rbo_dealer_registration_email_send;
use App\Mail\rbo_dealer_useroffer_email_send;

use App\Mail\send_all_emails_to_admin;

use App\Mail\all_customers_emails;

define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class RBOController extends Controller
{
	
	
	public static function run_api($url, $params = array()) {
		$final_url = API_URL . $url . '?api_key=' . API_KEY;
		if ($params && count($params) > 0) {
			$final_url .= '&' . http_build_query($params);
		}
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $final_url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"Host: marketcheck-prod.apigee.net"
			),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	    echo "cURL Error #:" . $err;
		exit;
		}
		return $response;
	}
 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	
		public function __construct()
		{
        $this->middleware('auth',['except'=> 'car_compare_delete']);
		} 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     
	 public function rbo_page() {
		
     return view('admin.chat.list');
    
	}
	
	
	
	public function rbo_reject(Request $request){
	
	RBO::where('user_id',Auth::id())->update(array('is_rbo_active' => 0 , 'rbo_status' => 'Rejected'));
	
	return redirect()->back()->with('error', 'Your all request best offers has been rejected'); 			

	
	}
	
	
	public function rbo_send(Request $request){
		
	$rbo_send = new RBO;
    
	$check_rbo_status = $rbo_send::where('car_id',$request->get('car_id'))->where('user_id',Auth::id())->get();
	
	$count_rbo_status = $rbo_send::where('user_id',Auth::id())->where('is_rbo_active',1)->get();

	if(count($check_rbo_status)){
	return redirect()->back()->with('error', 'You have already sent request to dealer for this car. '); 			
	}
	
	if(count($count_rbo_status)>4){
	return redirect()->back()->with('error', 'Request best offer limit exceed. You have not allowed more then 5 offers at a time.'); 			
	}
    
	
	$rbo_send->user_id = Auth::id(); 
	$rbo_send->dealer_id = $request->get('dealer_id'); 	
	$rbo_send->car_id = $request->get('car_id'); 
	$rbo_send->is_rbo_active = '1'; 
	$rbo_send->rbo_status = 'Pending'; 
	$rbo_send->save();
	
	$id = $request->get('car_id');

	$car_datas = ApiCaller::get_car_detail($id);	
	
	$to_email = Auth::user()->email;
	
	$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
	
	 $data = [
	 'name'=> Auth::user()->name,
	 'similar_car'=> $car_datas->vin . '&radius='. $request->get('radius').'&latitude='. $request->get('latitude'). '&longitude='. $request->get('longitude')
	 ];

	//send contact request to dealer email
      Mail::to($to_email)
	 ->bcc($get_admin_email->email)
	 ->send(new rbo_thankyou_email_send($data));
	
	
	
	
/* 	// send email to ADMIN of rbo request  **** START ****
	$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
	
	$admin_email = $get_admin_email->email;
	
	$message = "".Auth::user()->name."  has been sent Request Best Offer Of <a href=".url('car/detail/'.$id.'')." target='_blank'>".$car_datas->heading."</a>.To concern dealer.";

	 $data =[
	'name'=> Auth::user()->name,
	'message'=> $message
	 ];

	Mail::to($admin_email)->send(new send_all_emails_to_admin($data)); */


	// send email to ADMIN of rbo request  **** END ****
	
	
	
// Dealer Qualification ************************************* (if dealer is registered)
	 
	  $check_dealer_registered = DB::table('dealer')->where('dealer_id',$request->get('dealer_id'))->first();
	
	  if($check_dealer_registered->is_registered == '1'){
		  
	  $dealer_email = $check_dealer_registered->dealer_email; 
	  
	  $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();

	  
	  
	    $data =[
		
		'name'=> $check_dealer_registered->dealer_name,
		 
		 ];
		  
	  Mail::to($dealer_email)
	  ->bcc($get_admin_email->email)
	  ->send(new rbo_dealer_useroffer_email_send($data));	
	 
	 } 
	 
// if dealer not registered *****************************************************************
	 
	  if($check_dealer_registered->is_registered == '0'){
		  
	  $dealer_email = $check_dealer_registered->dealer_email; 
	  
	  $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
	  

	        $data =[
			
			'name'=> $check_dealer_registered->dealer_name,
			
			 ];
	  
	  Mail::to($dealer_email)
	    ->bcc($get_admin_email->email)
	  ->send(new rbo_dealer_registration_email_send($data));  
		  
	 }

	return redirect()->back()->with('message', 'Your request of best offer has been sent successfully. If you want more request click on similar car button.'); 			

	}
	
	
     public function rbo_compare_send(Request $request){
			
			$id = $request->input('car_id');
			
			$session_count = CarCompare_temp::where('user_id',Auth::id())->get();
			
			$check_car_compare = CarCompare_temp::where('user_id',Auth::id())->where('session_carid',$id)->get();
				
			if(count($session_count) > 3) {
			$msg = "You have already selected 4 vehicles. Please delete at least one vehicle for more comparison."; 
			} 
			
			if(count($check_car_compare ) ) {
			$contents = "<h3 align='center'>You Have already selected this car </h3>";
			echo $contents; 
			die;
	
			} 
			
			
				$Ccompare_temp= new CarCompare_temp();
				$Ccompare_temp->session_carid = $id;
				$Ccompare_temp->dealer_id = $request->input('dealer_id');
				$Ccompare_temp->user_id = Auth::id();
				$Ccompare_temp->status = "rbo";
				$Ccompare_temp->created_at = date('Y-m-d');
				$Ccompare_temp->save();
			
			$msg = "Your selected car now in RBO list"; 
	

			$car_compares = CarCompare_temp::where('user_id',Auth::id())->limit(4)->get();
			$all_car_datas = array();
			
			if($car_compares){
				
			 foreach($car_compares as $car_compare){
				  $id = $car_compare->session_carid;
				  $all_car_datas[] = ApiCaller::get_car_detail($id);
			 }
	

			$view = View::make('web_pages/car_compare_rbo_ajax')->with('all_car_datas',$all_car_datas);
			$contents = $view->render();
			echo "<h5 style='text-align:center;'>".$msg."</h5>";
			echo $contents; 
			die;
	
	}
	
  }
	
	
	public function car_compare_delete(Request $request)
	{	
	
	$car_id = $request->input('car_id');
	$all_car_datas = array();
	
	// Delete car compare AUTH based
	
	if(Auth::id()){
	
	$compare_delete = CarCompare::where('car_id',$car_id)->where('client_id',Auth::id());
	$compare_delete->delete();

	$car_compares = CarCompare::where('client_id',Auth::id())->limit(4)->get();
	
	
		if($car_compares){
					
			 foreach($car_compares as $car_compare){
				  $id = $car_compare->car_id;
				  $all_car_datas[] = ApiCaller::get_car_detail($id);
			 }
			 
	$view = View::make('web_pages/car_compare_rbo_ajax')->with('all_car_datas',$all_car_datas);
			$contents = $view->render();
			echo $contents; 
			
	     }
	
	
	}
	
	// Delete car compare session based
	else{
	
	$compare_delete = CarCompare_temp::where('session_carid',$car_id)->where('sessionID',Session::getId());
	
	$compare_delete->delete();

	$car_compares = CarCompare_temp::where('sessionID',Session::getId())->limit(4)->get();
	
		if($car_compares){
					
			 foreach($car_compares as $car_compare){
				  $id = $car_compare->session_carid;
				  $all_car_datas[] = ApiCaller::get_car_detail($id);
			 }
			 
	$view = View::make('web_pages/car_compare_rbo_ajax')->with('all_car_datas',$all_car_datas);
			$contents = $view->render();
			echo $contents; 
			
	    }
	
	 }
	
 }
	
	
	public function rbo_compare_save() {
	
    $check_rbo_status = RBO::where('user_id',Auth::id())->where('is_rbo_active',1)->get();
	
	$get_cars_compare = CarCompare_temp::where('user_id',Auth::id())->limit(4)->orderBy('id','DESC')->get();

	// check limit
	
	if(count($check_rbo_status) > 4){
	return redirect()->back()->with('error', 'Request best offer limit exceed. You have not allowed more then 5 offers at a time.'); 			
	}
	
	else{

	   if(count($get_cars_compare)){
				
		foreach($get_cars_compare as $get_car_compare){
			 
			 $post_cars_compare = new RBO();
			 
			 $post_cars_compare->dealer_id = $get_car_compare->dealer_id;
			 $post_cars_compare->user_id = $get_car_compare->user_id;
			 $post_cars_compare->car_id = $get_car_compare->session_carid;	
			 $post_cars_compare->rbo_status = "Pending";
			 $post_cars_compare->is_rbo_active = "1";
			 $post_cars_compare->save();
			 
			 }
	
	       //delete from temp
			$compare_delete_from_temp = CarCompare_temp::where('user_id',Auth::id());
			$compare_delete_from_temp->delete();
	
	   }
	
	
	} // close check limit
	
	 return redirect()->back()->with('message', 'Your request of best offer has been sent successfully.'); 			

	
	}
	
	//USER RBO RECORD

	public function get_user_rbo_list()
	{

		$request_dataa = RBO::where('user_id',Auth::id())->where('is_rbo_active',1)->get();

		if(count($request_dataa)){

			foreach($request_dataa as $request_data){

				$id = $request_data->car_id;

				$car_datas[] = ApiCaller::get_car_detail($id);

			}
			
			$currentPage = LengthAwarePaginator::resolveCurrentPage();

			$perPage = 6;

			$currentItems = array_slice($car_datas, $perPage * ($currentPage - 1), $perPage);

			$paginator = new LengthAwarePaginator($currentItems, count($car_datas), $perPage, $currentPage, [
				'path' => LengthAwarePaginator::resolveCurrentPath(),
			]);

			return view('client/user-rbo')->with('car_datas',$paginator);
		}
			else{
				return view('client/user-rbo');
			}


	}

     
	 public function rbo_quote_list(Request $request){
		 		
	// CHECK DEALER QUOTE ON RBO REQUEST
	
	  $check_legel_policy = RBO::where('user_id',Auth::id())->where('legalpolicy',0)->get();
	  
	  if(count($check_legel_policy)){
	   RBO::where('user_id',Auth::id())->update(array('legalpolicy' => 1));
	  }
	  
	  else {	
		
	   $get_customer_quote_list = RBO_QUOTE::where('user_id',Auth::id())->limit(5)
	 
   	  ->join('dealer','rbo_dealer_quote.dealer_id','=','dealer.dealer_id')
	   // ->where('customer_status','Pending')->orWhere('customer_status','Modified')->orWhere('customer_status','Completed')->orWhere('customer_status','Saved')->orWhere('customer_status','Accepted')
	   ->get();
	   
		
	   $car_datas = array();
	   
	   if(count($get_customer_quote_list)){
	   
	     foreach($get_customer_quote_list as $get_customer_quote){
		
			$id = $get_customer_quote->car_id;
			
			$car_datas[] = ApiCaller::get_car_detail($id);
         }
		
	   }
	
		
	  }
	
	// CHECK RBO COUNT WITHOUT DEALER QUOTE
	
   $get_rbo_list = DB::table('request_best_offer')->where('user_id',Auth::id())->where('rbo_status','Pending')->limit(5)
				   ->join('dealer','request_best_offer.dealer_id','=','dealer.dealer_id') 	
				   ->get();
	
		if(count($get_rbo_list)){
		   
			 foreach($get_rbo_list as $get_rbo_count){
			
				$id = $get_rbo_count->car_id;
				
				$rbo_count_data[] = ApiCaller::get_car_detail($id);
			 }
			
		   }
	
	 return view('client/rbo_quote_list')->with('get_customer_quote_list',$get_customer_quote_list)->with('car_datas',$car_datas)->with('rbo_count_data',$rbo_count_data)->with('get_rbo_list',$get_rbo_list);  
		 
	 }

	 public function rbo_quote_customer_view(Request $request){

	   $view_quote_form = RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')))->first();

	   $id = $request->get('car');

	   $car_datas = ApiCaller::get_car_detail($id);
	   
	   return view('client/rbo_quote_customer_view')->with('view_quote_form',$view_quote_form)->with('car_datas',$car_datas);

	  }

	
	
	public function rbo_quote_customer_details(Request $request){

	

	   $view_quote_form = RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')))->first();

	   $id = $request->get('car');

	   $car_datas = ApiCaller::get_car_detail($id);
	   
	  
	   
	   
	   return view('client/rbo_quote_customer_details')->with('view_quote_form',$view_quote_form)->with('car_datas',$car_datas);

	  }

	
	
	
	
	
	
	
	
	
	
	
	
	
     ### START CUSTOMER STATUS UPDATE MAIN FUNCITON ##########
	
	 public function rbo_quote_customer_status(Request $request){

	  $check_customer_quote_status =  RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')))
	 
	 ->join('dealer','rbo_dealer_quote.dealer_id','=','dealer.dealer_id')		
	 
	 ->get()->first();
	 
	 
	 $id =  $check_customer_quote_status->car_id;
	 $car_datas = ApiCaller::get_car_detail($id);
	 
	 
	 $to_email = $check_customer_quote_status->dealer_email;
	 
	 
	// Quote Offer accepted by user (Deal Finalizing)
	
    if($request->get('quote_status') == 'Accepted'){
	  
    $check_status = RBO_QUOTE::where('user_id',Auth::id())->where('customer_status','Accepted')->get();	  
   	
	if(count($check_status)){
		  
	return redirect()->back()->with('error', 'You can not allowed to accept more then one offer at a time.'); 			
	  
	}
	
	 else {
		
   	  RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')))->update(array('customer_status' => 'Accepted','dealer_status' =>'Accepted'));		
	  RBO::where('id',decrypt($request->get('rbo')))->update(array('rbo_status' => 'Accepted'));		
		
	  $message = "Congratulation  ".Auth::user()->name."  has been accepted your quoted offer of ".$car_datas->heading.". Kindly contact with customer.";
		
	  $details = "Customer Name= ".Auth::user()->name.", Customer Email : ".Auth::user()->email.", Customer Phone : ".Auth::user()->phone.".";	
	
	   $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
	
	   $data =[
			'name'=> $check_customer_quote_status->dealer_name,
			'message'=> $message,
			'car_link'=> $car_datas->id,
			'details'=>(isset($details)? $details:''),
			'subject'=> 'RBO Accepted('.$car_datas->heading.')'.'by'. Auth::user()->name,
		 ];
	  
	  Mail::to($to_email)
	  ->bcc($get_admin_email->email)
	  ->send(new all_customers_emails($data)); 


    //sent email to customer

	  $message = "You have accepted quoted offer of ".$car_datas->heading.". Confirmation email has been sent to concern dealer. You have 5 working days remaining to Complete Or Reject Offer.";
		
	  $details = "Dealer Name= ".$check_customer_quote_status->dealer_name.", Dealer Email : ".$check_customer_quote_status->dealer_email.", Dealer Phone : ".$check_customer_quote_status->dealer_phone.".";	
	
	   $data =[
			'name'=> Auth::user()->name,
			'message'=> $message,
			'car_link'=> $car_datas->id,
			'details'=>(isset($details)? $details:''),
			'subject'=> 'RBO Accepted ('.$car_datas->heading.')',
		 ];
	  
	  Mail::to(Auth::user()->email)->send(new all_customers_emails($data)); 

	  return redirect()->back()->with('message', 'Your accepted offer status has been sent to dealer. Dealer will communicate you'); 			

	  }
	 
	 
    }
		 
	// Quote Offer Reject by user 
	 
	 if($request->get('quote_status') == 'Rejected'){
		 
	 RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')))->update(array('customer_status' => 'Rejected'));		
	 RBO::where('id',decrypt($request->get('rbo')))->update(array('rbo_status' => 'Rejected'));		
		
	 $message = "Customer has not satisfied your offer and he has been Rejected your quoted offer.";
	
	    $data =[
		'name'=> $check_customer_quote_status->dealer_name,
		'message'=> $message,
		'car_link'=> $car_datas->id,
		'details'=> '',
		'subject'=> 'RBO Rejected('.$car_datas->heading.')'.'by'. Auth::user()->name,
		];
	  
	  $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();

	 Mail::to($to_email)
	 ->bcc($get_admin_email->email)
	 ->send(new all_customers_emails($data)); 

	return redirect()->back()->with('error', 'Offer has been rejected and status sent to concern dealer.'); 			
	
    }
	
	// Quote Offer Pending by user 
	
    if($request->get('quote_status') == 'Pending'){
	
   	RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')))->update(array('customer_status' => 'Pending','dealer_status' => 'Pending'));		
 	
	return redirect()->back()->with('message', 'Now RBO Quote status is now Pending'); 			
 
	}
	 
	 
	 // Quote Offer Closed by user 
	 
    if($request->get('quote_status') == 'Closed'){
	
   	RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')))->update(array('customer_status' => 'Closed'));		
 	
	
	  $message = "Customer has been closed RBO Quote";
	
	   $data =[
			'name'=> $check_customer_quote_status->dealer_name,
			'message'=> $message,
			'car_link'=> $car_datas->id,
			'subject'=> 'RBO Closed('.$car_datas->heading.')'.'by'. Auth::user()->name,
		 ];
	  
	  
	  $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
	  
	  Mail::to($to_email)
	  ->bcc($get_admin_email->email)
	  ->send(new all_customers_emails($data)); 
	
	
	 return redirect()->back()->with('message', 'Now RBO Quote is now closed.'); 			

	 }
	
	// Quote Offer completed by user 
	
	if($request->get('quote_status') == 'Completed'){
	
   	RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')))->update(array('customer_status' => 'Completed','dealer_status' => 'Completed'));		

	 $message = "Congratulation customer has been completed RBO quote. Now you can meet with customer through CHAT,CALL,EMAIL";
	
	   $data =[
			'name'=> $check_customer_quote_status->dealer_name,
			'message'=> $message,
			'car_link'=> $car_datas->id,
			'details'=> '',
			'subject'=> 'RBO Completed('.$car_datas->heading.')'.'by'. Auth::user()->name,
		 ];
		 
	 $get_admin_email = DB::table('users')->where('name','Admin')->get()->first(); 
	  
	  Mail::to($to_email)
	    ->bcc($get_admin_email->email)
	  ->send(new all_customers_emails($data)); 
	
	
	 return redirect()->back()->with('message', 'RBO Quote status is now Completed'); 			

	}
	
	 // Quote Offer Deleted by user
	
	if($request->get('quote_status') == 'Delete'){
	
		 /*  $update_data = [
					'customer_doc_fees_values' => null,
					'customer_price_values' => null,
					'customer_rebate_discount_values' => null,
					'customer_options_description' =>  null,
					'customer_options_value' =>  null,
					'customer_sale_price' =>  null,
					'customer_status' => "Pending",
					'status' => "pending",
					'updated_at' => date('Y-m-d H:i:s'),
			]; */
			
		 $rbo_delete = RBO::where('id',decrypt($request->get('rbo')));
		 $rbo_delete->delete();		
		
		 $rbo_delete_quote = RBO_QUOTE::where('rbo_id',decrypt($request->get('rbo')));
		 $rbo_delete_quote->delete(); 	
			


	   $message = " ".Auth::user()->name."  has not interested your offer and he has been deleted rbo quote ";
	
	   $data =[
			'name'=> $check_customer_quote_status->dealer_name,
			'message'=> $message,
			'car_link'=> $car_datas->id,
			'details'=> '',
			'subject'=> 'RBO Deleted ('.$car_datas->heading.')'.'by'. Auth::user()->name,
		 ];
		 
	 $get_admin_email = DB::table('users')->where('name','Admin')->get()->first(); 
	  
	  Mail::to($check_customer_quote_status->dealer_email)
	    ->bcc($get_admin_email->email)
	  ->send(new all_customers_emails($data)); 

	   return redirect(url('request-best-offer-quote-list.html'))->with('message', 'Your Quoted Offer has been deleted successfully');
	
		
	}
	
	 }// end cusotmer status main function
	 

	  public function rbo_quote_customer_submit(Request $request){
	   
	   //dealer Option Price
	   
		$dealer_options_values = '';
		if($request->input('dealer_options_value')){
		$dealer_options_values = implode(',', $request->input('dealer_options_value'));
		
	   }

	//document fees
		
		$doc_fees_values = '';
		if($request->input('doc_fees_value')){
		$doc_fees_values = implode(',', $request->input('doc_fees_value'));
		}

	//Rebate Discount fees
	
		$rebate_discount_value = '';
		if($request->input('rebate_discount_value')){
		$rebate_discount_value = implode(',', $request->input('rebate_discount_value'));
		}


	//customer options

		$customer_options_description = '';
		if($request->input('customer_options_description')){
		$customer_options_description = implode(',', $request->input('customer_options_description'));
		}
	
	
		$customer_options_value = '';
		if($request->input('customer_options_value')){
		$customer_options_value = implode(',', $request->input('customer_options_value'));
		}

		// calculate sale price

		 $i = 0; $i2 = 0; $i3 = 0; $sum = 0; $sum2 = 0; $sum3 = 0;

		 foreach ($request->input('dealer_options_value') as $row)
		 {
			$i++;
			$dealer_option = $sum += $row;
		 }

		 foreach ($request->input('doc_fees_value') as $row2)
		 {
			$i2++;
			$doc_fees = $sum2 += $row2;
		 }

		 foreach ($request->input('rebate_discount_value') as $row3)
		 {
			$i3++;
			$rabates_discount = $sum3 += $row3;
		 }

		// get sale price
	    $sale_price = $request->input('car_price') + $dealer_option + $doc_fees  - $rabates_discount;


	    $rbo_quote_submit = new RBO_QUOTE();

		$customer_status = "Modified";		
		if( $request->input('submit_status') == 'Saved'){
		$customer_status = "Saved";	
		}
		
			$update_data = [
					'customer_doc_fees_values' => $doc_fees_values,
					'customer_price_values' => $dealer_options_values,
					'customer_rebate_discount_values' => $rebate_discount_value,
					'customer_options_description' =>  $customer_options_description,
					'customer_options_value' =>  $customer_options_value,
					'customer_sale_price' =>  $sale_price,
					'customer_status' => $customer_status,
					'status' => "pending",
					'updated_at' => date('Y-m-d H:i:s'),
			 ];
			
			
		$update_user_quote = $rbo_quote_submit::where('rbo_id',$request->input('rbo_id'));
			
		$update_user_quote->update($update_data);
		
		
		// get dealer email and car title
		$get_car_detail = RBO_QUOTE::where('rbo_id',$request->input('rbo_id'))->get()->first();
		$get_dealer_email = DB::table('dealer')->where('dealer_id',$get_car_detail->dealer_id)->get()->first();
		
		$id =  $get_car_detail->car_id;
		$car_datas = ApiCaller::get_car_detail($id);
		
		
		// send to dealer email
		
	   $message = " ".Auth::user()->name." has been modified and sent RBO of ".$car_datas->heading." . Kindly check your Dealer Panel";
	   
	   $data =[
			'name'=> $get_dealer_email->dealer_name,
			'message'=> $message,
			'car_link'=> $car_datas->id,
			'details'=> '',
			'subject'=> 'RBO Modified ('.$car_datas->heading.')'.' by '. Auth::user()->name,
			
		 ];
	  
	    // get admin email
	    $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
 
			// modified quote and send to dealer & admin
	     Mail::to($get_dealer_email->dealer_email)
		->bcc($get_admin_email->email)
		->send(new all_customers_emails($data)); 
		
		
		if($request->input('submit_status')=='Saved'){
		
	   $message = " ".Auth::user()->name." has been saved Request Best Offer of ".$car_datas->heading.". ";
	
	   $data =[
			'name'=> $get_dealer_email->dealer_name,
			'message'=> $message,
			'car_link'=> $car_datas->id,
			'details'=> '',
			'subject'=> 'RBO Saved ('.$car_datas->heading.')'.' by '. Auth::user()->name,
			
		 ];
	  
	    $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
		
		// rbo saved email to dealer
	     Mail::to($get_dealer_email->dealer_email)
		->bcc($get_admin_email->email)
		->send(new all_customers_emails($data)); 	
			
		}
		
	    return redirect(url('request-best-offer-quote-list.html'))->with('message', 'Your quoted form has been ' .$customer_status. '');


	}
	
	 public function rbo_send_notifications(Request $rquest) {
		 
	 }
	

	  public function request_best_offer_table() {
		  
	  $request_best_offet = DB::table('request_best_offer')
	  ->join('users','request_best_offer.user_id','=','users.id')		
	  ->get();
		
		return Datatables::of($request_unblock_list)
			
			->addIndexColumn()
		
			 //->rawColumns(['action'])
			 ->make(true);
	}
			
	
	}
	
