<?php
	namespace App\Http\Controllers;
	use Illuminate\Support\Facades\Auth;
	use App\Helpers\Common;
	use App\Models\Save_search;
	use App\Models\User;
	use App\Models\Offers;
	use App\Models\Client_favourite;
	use Illuminate\Http\Request;
	use App\Models\AdminReviewControl;
	use App\Helpers\ApiCaller;
	use DB;
	use Session;
	use Illuminate\Support\Facades\Redirect;
	use Illuminate\Pagination\LengthAwarePaginator; 
	use App\Models\Dealer; 
	use App\Models\DealerApi; 
	use Yajra\Datatables\Datatables;

	
	define("API_URL", "http://api.marketcheck.com/v2/");
	define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");
	
	
	class AdminController extends Controller{
		
		
		
	    public static function run_api($url, $params = array())
		{
        $final_url = API_URL . $url . '?api_key=' . API_KEY;
        if ($params && count($params) > 0) {
            $final_url .= '&' . http_build_query($params);
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $final_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Host: marketcheck-prod.apigee.net"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            exit;
        }
        return $response;
    }
	
	
	 public function __construct()
    {
    if(Session::has('adminSession')){
	return view('admin/dashboard');	
	}
	else{
	return redirect(route('login_page'));	
	}	
	}
	

	 public function admin_login(){	

	 return view('admin/login');
			
	}



	public function login(Request $request){
	  
	 if($request->isMethod('post')){
	 
	if(Auth::attempt(['email' => $request->input('email'), 'password'=>  $request->input('password'), 'admin' => '1'])) {
	Session::put('adminSession',$request->input('email'));
	return redirect('dashboard.html');
	} else{
	return redirect('/admin')->with('flash_message_error','Invalid username and password');
	}
	}
	 return view('admin/login');
	}



	  public function logout(Request $request)
    {
		
        Auth::guard('admin')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
	
		return redirect(route('login_page'));
	 }
	

	public function dashboard() {

	if(Session::has('adminSession')){
	return view('admin/dashboard');	
	}
	else{
	return redirect(route('login_page'));	
	}	
	}

	public function dealer_list(){
	
	if(Session::has('adminSession')){
	return view('admin/dealer/dealerlist');
	}
	else{
	return redirect(route('login_page'));	
	}	
	}
	
	
	public function dealer_list_api(){
	
	if(Session::has('adminSession')){
	return view('admin/dealer/dealerlistapi');
	}
	else{
	return redirect(route('login_page'));	
	}	
	}
	
	
	
	
	 public function dealer_activity(){
	
	if(Session::has('adminSession')){
		
	return view('admin/dealer/activity/dealer-list');
	}
	else{
	return redirect(route('login_page'));	
	}	
	}
	
	
	
	
	
		
	public function dealer_table(){

	return Datatables::of(Dealer::query())

		->addColumn('action', function ($row) {
		return '
		<a href="' . route('dealer/detail/', ['dealer_id' => encrypt($row->dealer_id)]) . '"
					   class="btn btn-primary btn-xs">
						 Edit
					</a>
			<a href="' . route('dealer/remove/', ['dealer_id' => encrypt($row->dealer_id)]) . '"
					   class="btn btn-danger btn-xs">
						Delete
					</a>
		
		';
		
		})


	->make(true);
		
	}




	public function dealer_table_api(){
		
	return Datatables::of(DealerApi::query())


		->addColumn('action', function ($row) {
		return '
		<a href="'.route('dealer/api/edit/', ['dealer_id' => encrypt($row->dealer_id)]).'" class="btn btn-success btn-xs">
					<i class="fa fa-pencil"></i> Edit
				</a>
				
			<a href="' . route('dealer/api/remove/', ['dealer_id' => encrypt($row->dealer_id)]) . '"
					   class="btn btn-danger btn-xs">
						Delete
					</a>
		
		';
		
		})


	->make(true);
		
	}
	



	public function dealer_activity_table(){

	return Datatables::of(Dealer::query())

		->addColumn('action', function ($row) {
		return '
		
		
	<a href="'.route('dealer/edit/', ['dealer_id' => encrypt($row->dealer_id)]).'" class="btn btn-success btn-xs">
					<i class="fa fa-pencil"></i> Edit
				</a>
		
		
		<a href="' . route('dealer/activity/detail/', ['dealer_id' => encrypt($row->dealer_id)]) . '"
					   class="btn btn-primary btn-xs">
						 Details
					</a>
		
		';
		
		
		})
		
		
		
	  ->addColumn('status', function ($row) {
		  
		  
		 if($row->is_registered == '1'){
			
		 return '<div class="green">Registered</div>';	
			  
		 }
		  
		  else{
			return '<div class="red">unregistered</div>';	  
			  
		  }
		  
		  
		
		})


	->rawColumns(['status','action'])
	
	->make(true);
		
	}

	

	public function dealer_detail($dealer_id){
		
	$dealer = Dealer::where('dealer_id',decrypt($dealer_id))->get();
	
	$dealer_status = Dealer::where('dealer_id',decrypt($dealer_id))->where('is_block',0)->get();

	return view('admin/dealer/dealerin',compact('dealer','dealer_status'));

	
	}

	
	public function dealer_activity_detail($dealer_id){
		
	$dealer = Dealer::where('dealer_id',decrypt($dealer_id))->get();

	$getOffers = Offers::where('dealer_id',$dealer[0]->dealer_id)->get();	
	
		if(count($getOffers)){
		
		$getclients = $getOffers->pluck('client_id');
		
		$get_user = User::whereIn('id', $getclients)->get();
		
		
		foreach($get_user as  $get_users){
		
			$data[] = array(
					'client_id' =>  $get_users->id,
					'name'      =>  $get_users->name,
					'email' 	=>  $get_users->email,
					'mobile' 	=>  $get_users->mobile,
					'phone' 	=>  $get_users->phone,
					'image'     =>  $get_users->image,
					
				
			);	
		}	
		
	
		}
	
	return view('admin/dealer/activity/dealer-activity-detail',compact('dealer','data'));

	}
	
	
	
		public function dealer_edit($dealer_id) {
		
		$dealer = Dealer::where('dealer_id', decrypt($dealer_id))->first();
	
		return view('admin.dealer.dealeredit', compact('dealer'));
    	}
	
	
		public function dealer_api_edit($dealer_id) {
		
		$dealer = DealerApi::where('dealer_id', decrypt($dealer_id))->first();
	
		return view('admin.dealer.dealerapiedit', compact('dealer'));
	}
	
	
	
	
	
	public function dealer_update(Request $request) {
		$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255',
			'dealer_phone' => 'required',
			'address' => 'required',
			'dealer_zip' => 'required',
			
		]);


       if($request->input('password') == ''){
		 
		 $password = $request->input('old-password');
	   
	   }
	   
	   else{
		   
			$this->validate($request, [
			'password' => 'required|min:6|max:50|confirmed',
			'password_confirmation' => 'required'
		]);

		$password = md5($request->input('password'));
		  
	   }
		
		
		
		
		// update secondary phones
		$update_phone = "";
		if($request->input('dealer_other_phone')){
		$update_phone = implode(',',$request->input('dealer_other_phone'));		
		}
	
		
		// update secondary Emails
		$update_emails = "";
		if($request->input('dealer_other_emails')){
		$update_emails = implode(',',$request->input('dealer_other_emails'));		
		}
	
		
		$update_data = [
			'dealer_name' => $request->input('name'),	
			'dealer_contact_name' => $request->input('dealer_contact_name'),
			'dealer_email' => $request->input('email'),
			'dealer_phone' => $request->input('dealer_phone'),
			'dealer_street' => $request->input('address'),
			'dealer_zip' => $request->input('dealer_zip'),
			'dealer_website' => $request->input('dealer_website'),
			'dealer_other_phone' => $update_phone,	
			'dealer_other_emails' => $update_emails,
			'radius' => $request->input('radius'),
			'password' => $password,
		];

	
		$dealer = Dealer::where('dealer_id', $request->input('dealer_id'))->first();	
		$dealer->update($update_data);


		return redirect(route('dealer-activity-list'))->with('success', 'Dealer successfully updated!');
	}
	
	
	
	
	
	
	public function dealer_api_update(Request $request) {
		$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255',
			'dealer_phone' => 'required',
			'address' => 'required',
			'dealer_zip' => 'required',
		]);

		$update_data = [
			'dealer_name' => $request->input('name'),
			'dealer_email' => $request->input('email'),
			'dealer_phone' => $request->input('dealer_phone'),
			'dealer_street' => $request->input('address'),
			'dealer_zip' => $request->input('dealer_zip'),
			'dealer_website' => $request->input('dealer_website'),
		];

	
		$dealer = DealerApi::where('dealer_id', $request->input('dealer_id'))->first();	
		$dealer->update($update_data);


		return redirect(route('dealer-list-api'))->with('success', 'Dealer successfully updated!');
	}
	
	
	
	
	
	public function client_edit($user_id) {
		$user = User::where(Common::my_column(), decrypt($user_id))->first();
		
		return view('admin.client.clientedit', compact('user'));
	}
	
	
	
	
	
	public function dealer_account_block($dealer_id){
		
	Dealer::where('dealer_id',$dealer_id)->update(array('is_block'=>'1'));
	return redirect()->back()->with('message', 'Dealer account has been successfully deactivated');
		
	}


	public function dealer_account_unblock($dealer_id){
	
	Dealer::where('dealer_id',$dealer_id)->update(array('is_block'=>'0'));
	return redirect()->back()->with('message', 'Dealer account has been successfully activated');
		
	}

	public function client_lp_list(Request $request){
	
	$dealer = Dealer::where('dealer_id',$request->get('dealer_id'))->get();	
	
	$offers = new Offers();
	
	$get_offers = $offers::query()->where('client_id',$request->get('client_id'))
	->where('dealer_id', $request->get('dealer_id'))->get();
	
	 if(count($get_offers)){
	
		foreach($get_offers as $get_offer ){
			
		$id = $get_offer->car_id;
		
		$car_dataa[] = ApiCaller::get_car_detail($id);
		
		}	
	
		return view('admin/dealer/activity/dealer-activity-car-detail',compact('dealer','car_dataa'));
	
	 }
	
	} 
	
	// USER LeasePurchase Detail
	
	public function client_lp_detail(Request $request){
	
	$dealer = Dealer::where('dealer_id',$request->get('dealer_id'))->get();	
	
	$offers = new Offers();
	
	$get_offers = $offers::query()->where('client_id',$request->get('client_id'))
	->where('dealer_id', $request->get('dealer_id'))->where($request->get('offer'),1)->get();
	
	$id = $get_offers[0]->car_id;
	
	$car_data = ApiCaller::get_car_detail($id);
	
	return view('admin/dealer/activity/dealer-leasepurchase',compact('dealer','car_data'));

	}
	
	
	
	
	//remove dealer
	
	public function dealer_remove($dealer_id) {
		
	$dealer = Dealer::where('dealer_id', decrypt($dealer_id))->first();
	echo $dealer->dealer_id; 
		
	$dealer->delete();
	return redirect(route('dealer-list'))->with('success', 'Dealer successfully deleted!');
		
	}
	
	
	// api dealer remove
	
	public function dealer_api_remove($dealer_id) {
		
	$dealer = DealerApi::where('dealer_id', decrypt($dealer_id))->first();
	
	$dealer->delete();
	return redirect(route('dealer-list-api'))->with('success', 'Dealer successfully deleted!');
		
	}
	
	
	

	// block dealer
	public function dealer_block($dealer_id){
		
	Dealer::where('dealer_id',decrypt($dealer_id))->update(array('is_block' => '1'));
	return redirect()->back()->with('message', 'Dealer Successfully Blocked');
		
	}

	public function dealer_unblock($dealer_id){
		
	Dealer::where('dealer_id',decrypt($dealer_id))->update(array('is_block' => '0'));

	return redirect()->back()->with('message', 'Dealer Successfully Unblocked');
		
	}

	
	public function client_detail($user_id){
		
	$save_searches = Save_search::where('client_id',decrypt($user_id))->get();	
	$show_fav = Client_favourite::where('client_id',decrypt($user_id))->get();

	$user = User::where('id',decrypt($user_id))->get();

	return view('admin/client/clientin',compact('save_searches','show_fav','user'));

	}

	
	
	
	public function client_block($user_id){
		
	User::where('id',$user_id)->update(array('is_block' => '1'));
	return redirect()->back()->with('message', 'Client Successfully Blocked');
		
	}


	public function client_unblock($user_id){
		
	User::where('id',$user_id)->update(array('is_block' => '0'));
	return redirect()->back()->with('message', 'Client Successfully Unblocked');
		
	}

	
	public function request_ublock_list(){
	
	return view('admin/unblockrequest/list');
	
	}

	
	public function request_ublock_table() {
		
		 $request_unblock_list = DB::table('user_unblock_request')
		->join('users','user_unblock_request.user_id','=','users.id')		
		->get();
		
		return Datatables::of($request_unblock_list)
			
			->addIndexColumn()
		
			 //->rawColumns(['action'])
			 ->make(true);
	}
	
	
	
	
}