<?php
namespace App\Http\Controllers;
use App\Helpers\Common;
use App\Models\Role;
use Illuminate\Http\Request;
use DB;
use App\Models\Sms;
use App\Models\Subscription;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;


class SmsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function sms_compose() {
		
    return view('client.sms-compose');
    
	}
	
	
	public function notification_send(){	
	 return view('client.notification_send');	
	}
	

	
	
}
