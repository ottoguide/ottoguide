<?php

namespace App\Http\Controllers;

use App\Helpers\Common;
use App\Models\Role;
use Illuminate\Http\Request;
use DB;
use App\Models\Subscription;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;


class subscriptionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_subscription() {
        return view('admin.subscription.list');
    }
	
	
    public function list_subscription_table() {
		
		$subscriptions = Subscription::all();

		return Datatables::of($subscriptions)
			->addIndexColumn()
			
			->addColumn('action', function ($row) {
				$btns = '<a href="'.route('subscription/edit/', ['subscription_id' =>  Common::my_id($row->id)]).'" class="btn btn-primary btn-xs">
					<i class="fa fa-pencil"></i> Edit
				</a>
				<a href="'.route('subscription/remove/', ['subscription_id' =>  Common::my_id($row->id)]).'"
				   class="btn btn-danger btn-xs" onclick="return confirm(\'Are you sure you want to delete?\')">
					<i class="fa fa-times"></i> Delete
				</a>';
				return $btns;
			})
			->rawColumns(['action', 'image'])
			->make(true);
	}

		public function create() {
	
		return view('admin.subscription.create');
	}

	
	public function store(Request $request) {
    	$this->validate($request, [
			'title' => 'required',
			'expiry_days' => 'required',
			//'chats' => 'required',
			//'lease_purchase' => 'required',
			//'featured_ad' => 'required',
			
		]);

		if($request->input('ads_unlimited')){
		$featured_ad = "unlimited";			
		}
		else{
		$featured_ad = $request->input('featured_ad');	
		}	
		
		if($request->input('chats_unlimited')){
		$chats = "unlimited";			
		}
		else{
		$chats = $request->input('chats');	
		}
		
		if($request->input('chats_unlimited')){
		$leasepurchase = "unlimited";			
		}
		else{
		$leasepurchase = $request->input('lease_purchase');	
		}
		
		

			$subscription = Subscription::create([
				'title' => $request->input('title'),
				'featured_ad' => $featured_ad,	
				'chats' => $chats,
				'lease_purchase' => $leasepurchase,
				'expire_days' => $request->input('expiry_days'),
				'description' => $request->input('description'),
			]);
		
		
		
		return redirect(route('subscription_list'))->with('success', 'subscription successfully created!');
	}

	
	public function edit($subscription_id) {
		$subscription = Subscription::where(Common::my_column(), $subscription_id)->first();
		return view('admin.subscription.edit', compact('subscription'));
	}
	
	
	public function update(Request $request) {
		$this->validate($request, [
			'title' => 'required',
			'expiry_days' => 'required',
			'chats' => 'required',
			'lease_purchase' => 'required',
			'featured_ad' => 'required',
			
		]);

		$update_data = [
				'title' => $request->input('title'),
				'expire_days' => $request->input('expiry_days'),
				'featured_ad' => $request->input('featured_ad'),
				'chats' => $request->input('chats'),
				'lease_purchase' => $request->input('lease_purchase'),
				'description' => $request->input('description'),
				'updated_on' => date('Y-m-d H:i:s'),
				
		];

		$subscription = Subscription::where('id', $request->input('subscription_id'))->first();

		$subscription->update($update_data);

		return redirect(route('subscription_list'))->with('success', 'subscription successfully updated!');
	}

	
	    public function remove($subscription_id) {
		$subscription = Subscription::where(Common::my_column(), $subscription_id)->first();

		$subscription->delete();
		return redirect(route('subscription_list'))->with('success', 'subscription successfully deleted!');
	}
	
	
	
}
