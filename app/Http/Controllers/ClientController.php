<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use App\Models\Save_search;
use App\Models\DealerCron;
use App\Models\CarCompare;
use App\Models\CarCompare_temp;
use App\Models\CarRating;
use App\Models\CarReview;
use App\Models\EmailBox;
use App\Models\Sms;
use App\Models\TwilioChat;
use App\Models\EmailBoxDealer;
use App\Models\ReviewSetting;
use App\Models\AdminReviewControl;
use App\Models\CarHistory;
use App\Helpers\ApiCaller;
use App\Models\Dealer;
use App\Models\Offers;
use App\Models\ContactRequest;
use Illuminate\Support\Facades\Redirect;
use App\Models\Client_favourite;
use App\Models\Dealer_favourite;
use App\Models\UserUnblockRequest;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Hash;
use View;
use Session;
use Mail;
use App\Mail\myemail;
use App\Mail\contact_request_send;
use App\Mail\all_customers_emails;
use App\Mail\forget_password;
use App\Helpers\Common;

define("API_URL", "http://api.marketcheck.com/v2/");
define("API_KEY", "pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class ClientController extends Controller
{

    public static function run_api($url, $params = array())
    {
        $final_url = API_URL . $url . '?api_key=' . API_KEY;
        if ($params && count($params) > 0) {
            $final_url .= '&' . http_build_query($params);
        }
		
		
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $final_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Host: marketcheck-prod.apigee.net"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "cURL Error #:" . $err;
            exit;
        }
        return $response;
    }



	// forgot password
	
	 public function forgot_password_page(){
		 
	 session()->put('user_forget_password','forgot_password');	 
		 
	 return view('dealer/forgot_password');
	
	}
	
	public function forgot_password(Request $request){
		
	$user_email =  $request->input('email');
		
	$check_user_email = DB::table('users')->where('email',$user_email)->get()->first();
	
	if(count($check_user_email)){
		
	$pin = rand( 10000 , 99999 );

    DB::table('users')->where('email',$user_email)->update(array('remember_token' => $pin ));	
	

	// get admin email	
	 $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
	
	 $data =[
	'dealer_name'=> $check_user_email->name,
	'pin'=> $pin
	
	 ];
	 
	 
	// Send Pin to dealer email
	Mail::to($user_email)->bcc($get_admin_email->email)->send(new forget_password($data));	

    return view('dealer/update-password')->with('status', 'Pin code has been sent to your mention email address. Kindly put pin code and new password');
		
	}
	
	else{
		return redirect('/dealer/forgot/password/')->with('status', 'Please put valid email');
	}
	
	
	}
	

	
	public function update_password(Request $request){
		
	   $pin = $request->input('pin');
	   $newpassword =  \Hash::make($request->input('newpassword'));		
	   
		   $update_data = [
		   'password' => $newpassword,
		   'remember_token' => ''
			];
	    
	    $update_password = DB::table('users')->where('remember_token',$pin);
		$update_password->update($update_data);
	
	
		$request->session()->forget('user_forget_password');
	
	    return redirect('/login')->with('message', 'Password has been check now you can login with your new password.');
	  
	} 
	

	
    public function dealer_panel()
    {
        return view('dealerpanel/template');
    }


    public function user_dashboard()
    {

        return view('client/dashboard');

    }


    public function set_rbo_session(Request $request)
    {

        $session_data = [
            'session_rbo_id' => Session::getId(),
            'session_car_id' => $request->get('session_car_id'),
        ];

    $request->session()->put('rbo_session', $session_data);
		
		
	$id = $request->get('session_car_id');
	$car_info = ApiCaller::get_car_detail($id);
 
	$store_button_session = [
	
	'session_id'=> Session::getId(),
	'session_car_id'=> $request->get('session_car_id'),
	'session_car_vin'=> $request->get('session_car_vin'),
	'session_car_title'=> $request->get('session_car_title'),
	'session_status'=> $request->get('session_status'),
	'session_radius'=> $request->get('session_radius'),
	'session_latitude'=> $request->get('session_latitude'),	
	'session_longitude'=> $request->get('session_longitude'),
	'session_make'=> $car_info->build->make ?? '',
	'session_modal'=> $car_info->build->model ?? '',
	'session_trim'=> $car_info->build->trim ?? '',
	'session_body'=> $car_info->build->body_type ?? '',
	'session_vehicle_type'=> $car_info->build->vehicle_type ?? '',
	'session_miles'=> $car_info->miles ?? '',
	'session_inventory_type'=> $car_info->inventory_type ?? '',
	'session_color'=> $car_info->exterior_color ?? '',
	'session_dealer_id'=> $car_info->dealer->id ?? '',
	'session_dealer_name'=> $car_info->dealer->name ?? '',
	'session_dealer_city'=> $car_info->dealer->city ?? '',
	'session_dealer_street'=> $car_info->dealer->street ?? '',
	'session_dealer_state'=> $car_info->dealer->state ?? '',
	'session_dealer_lat'=> $car_info->dealer->latitude ?? '',	
	'session_dealer_long'=> $car_info->dealer->longitude ?? '',
	'session_dealer_zip'=> $car_info->dealer->zip ?? '',
	'session_dealer_phone'=> $car_info->dealer->phone ?? '',
	'session_dealer_website'=> $car_info->dealer->website ?? '',
	
	];	
		

	  $request->session()->put('search_button_session', $store_button_session);
		
	  return redirect(url('/login'));

    }


    public function saved_adverts()

    {
        $show_fav = Client_favourite::where('client_id', Auth::id())->paginate(3);
		
        return view('client/adverts', compact('show_fav'));
    }

    public function save_searches()
    {

        $save_search = New Save_search();
        $save_data = $save_search::where('client_id', Auth::id())->paginate(3);
        return view('client/searches', compact('save_data'));
    }


    public function saved_reviews()

    {
        $show_reviews = CarReview::where('client_id', Auth::id())->where('reply_to', null)->get();

        $car_datas = array();

        if ($show_reviews) {


            foreach ($show_reviews as $show_review) {

                $id = $show_review->car_id;

                $car_datas[] = ApiCaller::get_car_detail($id);

            }

            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            $perPage = 3;

            $currentItems = array_slice($car_datas, $perPage * ($currentPage - 1), $perPage);

            $paginator = new LengthAwarePaginator($currentItems, count($car_datas), $perPage, $currentPage, [
                'path' => LengthAwarePaginator::resolveCurrentPath(),
            ]);

        }

        return view('client/reviews')->with('car_datas', $paginator);
    }





	public function my_chat_list()

    {
        $show_chat_lists = DB::table('twilio_chat')->where('user_id', Auth::id())->get();

        $car_datas = array();

        if ($show_chat_lists) {


            foreach ($show_chat_lists as $show_chat_list) {

                $id = $show_chat_list->car_id;

                $car_datas[] = ApiCaller::get_car_detail($id);

            }

            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            $perPage = 3;

            $currentItems = array_slice($car_datas, $perPage * ($currentPage - 1), $perPage);

            $paginator = new LengthAwarePaginator($currentItems, count($car_datas), $perPage, $currentPage, [
                'path' => LengthAwarePaginator::resolveCurrentPath(),
            ]);

        }

			

        return view('client/mychatlist')->with('car_datas', $paginator)->with('show_chat_list',$show_chat_list);
    }




    //mychat Page
    public function my_chat(Request $request)

    {

        if (Auth::user()) {


            $check_user_block_status = DB::table('twilio_chat')->where('user_id', Auth::id())
                ->where('dealer_id', $_GET['dealer_id'])->where('is_block_current', 1)->get();

            if (count($check_user_block_status)) {

                return redirect()->back()->with('error', 'Administration has blocked you for chat communication to this dealer');

            } else {

                $check_chat_status = DB::table('twilio_chat')->where('dealer_id', $_GET['dealer_id'])
                    ->where('user_id', Auth::id())->first();

                $get_dealer_email = Dealer::where('dealer_id', $_GET['dealer_id'])->first();

                if (!count($check_chat_status)) {

                    $chat_data = new TwilioChat;
                    $chat_data->car_id = $_GET['car_id'];
                    $chat_data->dealer_id = $_GET['dealer_id'];
                    $chat_data->user_id = Auth::id();
                    $chat_data->chat_channel = $get_dealer_email->dealer_email . '-' . Auth::user()->email;
                    $chat_data->channel_display_name = $get_dealer_email->dealer_name . ' & ' . Auth::user()->name;
                    $chat_data->notify = '0';
                    $chat_data->save();

                } // end check chat status

                $id = $_GET['car_id'];

                $car_datas = ApiCaller::get_car_detail($id);
				
				
				
				

			/* $message = "Dear  ".$get_dealer_email->dealer_name.". ".Auth::user()->name."  want to chat communication with you about  ".$car_datas->heading.". Kindly login your dealer panel.";
		
			$details = "Customer Name= ".Auth::user()->name.", Customer Email : ".Auth::user()->email.", Customer Phone : ".Auth::user()->phone.".";	
	
			$get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
	
			$get_dealer = DB::table('dealer')->where('dealer_id',$_GET['dealer_id'])->get()->first();
	
	
		     $data =[
				'name'=> $get_dealer->dealer_name,
				'message'=> $message,
				'car_link'=> $car_datas->id,
				'details'=>''
			 ];
		  
		    echo'<pre>';
			print_r($get_dealer);
			die;
		  
		  
			  Mail::to($get_dealer->dealer_email)
			  ->bcc($get_admin_email->email)
			  ->send(new all_customers_emails($data)); */ 
				
				
				
            return view('client/chat')->with('car_datas', $car_datas)->with('check_chat_status', $check_chat_status);

            } // end else of check block status

        } // end Auth user
    }

    //account page
    public function my_account()
    {


        return view('client/profile');
    }


    // My history

    public function my_history()
    {

        $history_dataa = CarHistory::where('client_id', Auth::id())->get();

        $car_datas = array();

        if ($history_dataa) {
            foreach ($history_dataa as $history_data) {
                $id = $history_data->car_id;
                $car_datas[] = ApiCaller::get_car_detail($id);
            }

            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $perPage = 3;

            $currentItems = array_slice($car_datas, $perPage * ($currentPage - 1), $perPage);

            $paginator = new LengthAwarePaginator($currentItems, count($car_datas), $perPage, $currentPage, [
                'path' => LengthAwarePaginator::resolveCurrentPath(),
            ]);

        }

        return view('client/history')->with('car_datas', $paginator);
    }

    //contact Page
    public function contact()
    {
        return view('client/contact');
    }


    //UNBLOCK REQUEST FORM

    public function ublock_request()
    {
        return view('client/requestbox');
    }


    public function ublock_request_send(Request $request)
    {

        $request_send = new UserUnblockRequest;

        $check_request_status = $request_send::where('user_id', Auth::id())->where('subject', $request->input('unblock-subject'))->get();

        if (count($check_request_status)) {

            return redirect()->back()->with('error', 'Your request has already sent to administration for this subject.');

        } else {

            $request_send->user_id = Auth::id();
            $request_send->subject = $request->input('unblock-subject');
            $request_send->message = $request->input('message');
            $request_send->status = "blocked";
            $request_send->save();

            return redirect()->back()->with('message', 'Your request has been successfully registered. Administration will contact you as soon.');

        }

    }


    // QUESTIONER SEARCH

    public function user_questioner_list()
    {

        return view('client/questioner-list');
    }

    public function user_questioner_view(Request $request)
    {

        $quiz_id = decrypt($request->get('quiz_id'));

        $get_questions_answer = DB::table('quiz_query')->where('quiz_id', $quiz_id)
            ->where('user_id', Auth::id())->get()->first();

        //json decode

        $Qanswer_decode = json_decode($get_questions_answer->quiz_stats);
        $search_query = $get_questions_answer->quiz_query;


        return view('client/questioner-view', compact('Qanswer_decode', 'search_query'));
    }


    public function user_questioner_delete(Request $request)
    {

        $quiz_id = decrypt($request->get('quiz_id'));

        DB::table('quiz_query')->where('quiz_id', $quiz_id)
            ->where('user_id', Auth::id())->delete();

        return redirect()->back()->with('message', 'Quiz deleted successfully in your list.');

    }


    //Contact Request Page

    public function contact_request()
    {

        $request_dataa = ContactRequest::where('client_id', Auth::id())->get();

        if (count($request_dataa)) {

            foreach ($request_dataa as $request_data) {

                $id = $request_data->car_id;

                $car_datas[] = ApiCaller::get_car_detail($id);

            }
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            $perPage = 3;

            $currentItems = array_slice($car_datas, $perPage * ($currentPage - 1), $perPage);

            $paginator = new LengthAwarePaginator($currentItems, count($car_datas), $perPage, $currentPage, [
                'path' => LengthAwarePaginator::resolveCurrentPath(),
            ]);

            return view('client/contact-request-list')->with('car_datas', $paginator);
        } else {
            return view('client/contact-request-list');
        }


    }


    public function ContactReuqestDetail($car_id)
    {

        $contact_dataa = ContactRequest::where('car_id', $car_id)->where('client_id', Auth::id())->get();

        if (count($contact_dataa)) {
            foreach ($contact_dataa as $contact_data) {
                $id = $contact_data->car_id;
                $car_datas = ApiCaller::get_car_detail($id);
            }
        }

        return view('client/contact_request')->with('car_datas', $car_datas)->with('contact_data', $contact_data);
    }


    public function save_my_search(Request $request)
    {

        $saveSearch = new Save_search;
        $saveSearch->save_search_name = $request->input('search_name');
        $saveSearch->save_search_description = "This search saved on " . date("d/m/Y h:i A");
        $saveSearch->client_id = Auth::id();
        $saveSearch->save_search_url = $request->input('search_query');
        $saveSearch->json_data = "some data";
        $saveSearch->save();

        return redirect()->back()->withErrors('Search Saved successfully');
    }


    public function save_search_edit(Request $request)
    {

        $save_search_name = $request->input('save_search_name');
        $id = $request->input('save_search_id');
        Save_search::where('save_search_id', $id)->update(array('save_search_name' => '' . $save_search_name . ''));
        return redirect()->back()->withErrors('Search Saved successfully');
    }

    //
    public function save_searches_delete($save_searches_id)
    {
        $save_search_delete = Save_search::where('save_search_id', decrypt($save_searches_id));
        $save_search_delete->delete();
        return redirect()->back()->withErrors('');
    }


    public function save_searches_delete_all($client_id)
    {

        $save_search_delete_all = Save_search::where('client_id', decrypt($client_id));
        $save_search_delete_all->delete();
        return redirect()->back()->with('message', 'Card Saved Successfully');

    }

    public function save_my_car(Request $request)
    {
        $favourite = new Client_favourite;
        $favourite->client_id = Auth::id();
        $favourite->car_id = $request->input('car_post_id');
        $favourite->json_data = $request->input('car_post_data');
        $data = $favourite->save();
		
	return redirect()->back()->with('message', 'Card Saved Successfully');
    }
	
	
	 
    public function get_auto_location(Request $request)
    {
        $request->session()->put('search_radius', 10);

        if (Session::has('geo_location_session')) {
            return true;
        } else {

            $request->session()->put('search_radius', 10);

            $get_lat = $request->input('get_lat');
            $get_long = $request->input('get_long');
            $get_lat_long = $get_lat . ',' . $get_long;
            $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $get_lat_long . '&key=AIzaSyCAaV3MxWYWizG-Eoe0fpUlNa5iSOjYIK8');
            $geo = json_decode($geo, true); // Convert the JSON to an array


            if (isset($geo['status']) && ($geo['status'] == 'OK')) {

                foreach ($geo['results'][0]['address_components'] as $data) {
                    if ($data['types'][0] == 'locality' && $data['types'][1] == 'political') {
                        $city_name = $data['long_name'];
                    }

                    if ($data['types'][0] == 'administrative_area_level_1' && $data['types'][1] == 'political') {
                        $state = $data['short_name'];
                    }

                    //if ($data['types'][0] == 'postal_code') {
                    //    $state .= " " . $data['long_name'];
                    //}

                    if ($data['types'][0] == 'postal_code') {
                        $zipcode = $data['short_name'];
                    }

                }

                $t = count($geo['results'][0]['address_components']);
                $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
                $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude
//                $city_name = $geo['results'][0]['address_components'][3]['long_name'];
//                $state = $geo['results'][0]['address_components'][5]['short_name']." ".$geo['results'][0]['address_components'][7]['long_name'];
                $country = $geo['results'][0]['address_components'][$t - 1]['short_name'];
                $full_address = $geo['results'][0]['formatted_address'];

                if (Session::get('geo_location_session')['city'] == $city_name && Session::get('geo_location_session')['state'] == $state && Session::get('geo_location_session')['zip_code'] == $zipcode) {

                    return true;
                } else {

                    $geo_location_session = [
                        'latitude' => $latitude,
                        'longitude' => $longitude,
                        'country' => $country,
                        'city' => $city_name,
                        'state' => $state,
                        'zip_code' => $zipcode,
                        'full_address' => $full_address,
                    ];

                    $request->session()->put('geo_location_session', $geo_location_session);
                    return json_encode($geo_location_session);
                }


            }

        }
        /*  else {

             $get_lat = $request->input('get_lat');

             $get_long = $request->input('get_long');

             $get_lat_long = $get_lat . ',' . $get_long;

             $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $get_lat_long . '&key=AIzaSyDg7CdVpp0v-OppiTMYgtXoHH2zOUICcUU');

             $geo = json_decode($geo, true); // Convert the JSON to an array


             if (isset($geo['status']) && ($geo['status'] == 'OK')) {

                 foreach ($geo['results'][0]['address_components'] as $data){
                     if($data['types'][0] == 'locality' &&  $data['types'][1] == 'political'){
                         $city_name = $data['long_name'];
                     }

                     if($data['types'][0] == 'administrative_area_level_1' &&  $data['types'][1] == 'political'){
                         $state = $data['short_name'];
                     }

                     if($data['types'][0] == 'postal_code'){
                         $state .= " " . $data['long_name'];
                     }

                 }

                 $t = count($geo['results'][0]['address_components']);
                 $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
                 $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude
 //                $city_name = $geo['results'][0]['address_components'][3]['long_name'];
 //                $state = $geo['results'][0]['address_components'][5]['short_name']." ".$geo['results'][0]['address_components'][7]['long_name'];
                 $country = $geo['results'][0]['address_components'][$t - 1]['short_name'];
                 $full_address = $geo['results'][0]['formatted_address'];

                 if(Session::get('geo_location_session')['city'] == $city_name && Session::get('geo_location_session')['state'] == $state){

             return true;
                 }
                 else{

                     $geo_location_session = [
                         'latitude' => $latitude,
                         'longitude' => $longitude,
                         'country' => $country,
                         'city' => $city_name,
                         'state' => $state,
                         'full_address' => $full_address,
                     ];

                     $request->session()->put('geo_location_session', $geo_location_session);

                     return json_encode($geo_location_session);
                 }


             }


         }   // comment forcefully set location */


    }


    public function get_lat_long(Request $request)
    {

        // check session for location

        $address = $request->input('zip_code');

        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&key=AIzaSyCAaV3MxWYWizG-Eoe0fpUlNa5iSOjYIK8');

        $geo = json_decode($geo, true); // Convert the JSON to an array

        if (isset($geo['status']) && ($geo['status'] == 'OK')) {

            foreach ($geo['results'][0]['address_components'] as $data) {
                if ($data['types'][0] == 'locality' && $data['types'][1] == 'political') {
                    $city_name = $data['long_name'];
                }

                if ($data['types'][0] == 'administrative_area_level_1' && $data['types'][1] == 'political') {
                    $state = $data['short_name'];
                }

                //if ($data['types'][0] == 'postal_code') {
                //    $state .= " " . $data['long_name'];
                //}

                if ($data['types'][0] == 'postal_code') {
                    $zipcode = $data['short_name'];
                }

            }

            $t = count($geo['results'][0]['address_components']);
            $latitude = $geo['results'][0]['geometry']['location']['lat']; // Latitude
            $longitude = $geo['results'][0]['geometry']['location']['lng']; // Longitude
//                $city_name = $geo['results'][0]['address_components'][3]['long_name'];
//                $state = $geo['results'][0]['address_components'][5]['short_name']." ".$geo['results'][0]['address_components'][7]['long_name'];
            $country = $geo['results'][0]['address_components'][$t - 1]['short_name'];
            $full_address = $geo['results'][0]['formatted_address'];

            if (Session::get('geo_location_session')['city'] == $city_name && Session::get('geo_location_session')['state'] == $state && Session::get('geo_location_session')['zip_code'] == $zipcode) {

                return true;
            } else {

                $geo_location_session = [
                    'latitude' => $latitude,
                    'longitude' => $longitude,
                    'country' => $country,
                    'city' => $city_name,
                    'state' => $state,
                    'zip_code' => $zipcode,
                    'full_address' => $full_address,
                ];

                $request->session()->put('geo_location_session', $geo_location_session);

                return json_encode($geo_location_session);
            }


        }


    } // end get_lat_long main fuction

    public function remove_location()
    {
        session()->forget('geo_location_session');
        return redirect(url('/'));
    }


    public function save_advert_delete($favourite_id)
    {
        $save_advert_delete = Client_favourite::where('favourite_id', decrypt($favourite_id));
        $save_advert_delete->delete();
        return redirect()->back()->withErrors('');
    }

    public function save_advert_delete_all($client_id)
    {
        $save_advert_delete_all = Client_favourite::where('client_id', decrypt($client_id));
        $save_advert_delete_all->delete();
        return redirect()->back()->withErrors('');
    }

    public function car_compare_page()
    {

        if (Auth::id()) {

            $car_compares = CarCompare::where('client_id', Auth::id())->limit(4)->get();

            $all_car_datas = array();

            if ($car_compares) {
                foreach ($car_compares as $car_compare) {
                    $id = $car_compare->car_id;
                    $all_car_datas[] = ApiCaller::get_car_detail($id);
                }
            }
        } else {

            $car_compares = CarCompare_temp::where('sessionID', Session::getId())->limit(4)->get();

            $all_car_datas = array();

            if ($car_compares) {
                foreach ($car_compares as $car_compare) {
                    $id = $car_compare->session_carid;
                    $all_car_datas[] = ApiCaller::get_car_detail($id);
                }
            }
        }
        return view('web_pages/car_compare')->with('all_car_datas', $all_car_datas);
    }

    public function car_compare(Request $request)
    {

        $car_id = $request->input('car_id');
        $all_car_datas = array();

        if (Auth::id()) {

            $check_car_compare = CarCompare::where('client_id', Auth::id())->where('car_id', $car_id)->first();

            $auth_count = CarCompare::where('client_id', Auth::id())->get();
            if (count($auth_count) > 4) {
                echo '<h4 align="center" style="background-color: #cc0000;color:#fff;padding:5px;border-radius:5px;width:65%; font-size: 16px; margin-left: auto;margin-right: auto;">You have already picked 4 vehicles. Please delete at least one vehicle.</h4>';
            } else {

                // check car already selected

                if (!count($check_car_compare)) {

                    $id = $car_id;
                    $Ccompare = new CarCompare();
                    $Ccompare->car_id = $id;
                    $Ccompare->client_id = Auth::id();
                    $Ccompare->save();

                    $msg = "You have successfully picked a car for compare.";
                } else {
                    echo '<h4 align="center" style="background-color: #cc0000;color:#fff;padding:5px;border-radius:5px;width:65%; font-size: 16px; margin-left: auto;margin-right: auto;">You have already selected this car </h4>';
                }


            }

            $car_compares = CarCompare::where('client_id', Auth::id())->limit(4)->get();
            if ($car_compares) {
                foreach ($car_compares as $car_compare) {
                    $id = $car_compare->car_id;
                    $all_car_datas[] = ApiCaller::get_car_detail($id);
                }
            }
        } else {

            $session_count = CarCompare_temp::where('sessionID', Session::getId())->get();

            $check_car_compare = CarCompare_temp::where('sessionID', Session::getId())->where('session_carid', $car_id)->first();

            if (count($session_count) > 4) {
                echo '<h4 align="center" style="background-color: #cc0000;color:#fff;padding:5px;border-radius:5px;width:65%; font-size: 16px; margin-left: auto;margin-right: auto;">You have already picked 4 vehicles. Please delete at least one vehicle.</h4>';

            }

            if (!count($check_car_compare)) {
                $id = $car_id;
                $Ccompare_temp = new CarCompare_temp();
                $Ccompare_temp->session_carid = $id;
                $Ccompare_temp->sessionID = Session::getId();
                $Ccompare_temp->created_at = date('Y-m-d');
                $Ccompare_temp->save();
                $msg = "You have successfully picked a car for compare.";
            } else {
                echo '<h4 align="center" style="background-color: #cc0000;color:#fff;padding:5px;border-radius:5px;width:65%; font-size: 16px; margin-left: auto;margin-right: auto;">You have already selected this car </h4>';
            }


            $car_compares = CarCompare_temp::where('sessionID', Session::getId())->limit(4)->get();
            $all_car_datas = array();
            if ($car_compares) {
                foreach ($car_compares as $car_compare) {
                    $id = $car_compare->session_carid;
                    $all_car_datas[] = ApiCaller::get_car_detail($id);
                }
            }
        }

        $view = View::make('web_pages/car_compare_rbo_ajax')->with('all_car_datas', $all_car_datas);
        $contents = $view->render();
        echo $contents;
        die;
    }

    public function car_compare_delete($car_id)
    {
        if (Auth::id()) {

            $compare_delete = CarCompare::where('car_id', $car_id)->where('client_id', Auth::user()->id);
            $compare_delete->delete();

        } else {
            $compare_delete = CarCompare_temp::where('session_carid', $car_id)->where('sessionID', Session::getId());
            $compare_delete->delete();
        }
        return redirect(route('CarComparePage'))->with('message', 'Vehicle successfully deleted from list.');

    }


    public function save_history($id)
    {

        if (Auth::id()) {
            $car_history = new CarHistory();
            $car_history->car_id = $id;
            $car_history->client_id = Auth::id();
            $car_history->created_at = date('Y-m-d');
            $car_history->save();
            return redirect()->back()->with('message', 'History Saved Successfully');
        }
        return redirect()->back()->with('error', 'Please Login Before Create Vehicle History');
    }

    public function delete_history($car_id)
    {
        $delete_history = CarHistory::where('car_id', $car_id)->where('client_id', Auth::id());
        $delete_history->delete();
        return redirect()->back()->with('message', 'History Successfully Deleted');

    }


    public function car_rating($car_id)
    {

        if (Auth::id()) {
            $id = $car_id;
            $check_rating = CarRating::where('car_id', $id)->where('client_id', Auth::id())->get()->first();

            if (!empty($check_rating)) {
                return redirect()->back()->with('error', 'You have already rated this vehicle');
            } else {
                $car_rating[] = ApiCaller::get_car_detail($id);
                return view('web_pages/car_rating')->with('car_rating', $car_rating);
            }
        } else {
            return redirect()->back()->with('error', 'Please login before rate this vehicle');
        }
    }


    public function rating_save(Request $request)
    {

        if (Auth::id()) {

            $car_rating = new CarReview();

            $car_rating->car_id = $request->input('car_id');
            $car_rating->client_id = Auth::id();
            $car_rating->dealer_id = $request->input('dealer_id');
            $car_rating->rating = $request->input('rating');
            $car_rating->title = $request->input('title');
            $car_rating->message = $request->input('review_description');
            $car_rating->message_by = 'client';

            $car_rating->save();

            $id = $request->input('car_id');

            return redirect(url('car/detail/' . $id . ''))->with('message', 'Car Successfully Rated');
        }
        return redirect()->back()->with('error', 'Please login before rate this vehicle');
    }


    public function review_save(Request $request)
    {

        $this->validate($request, [
            'message' => 'required',
        ]);

        if (Auth::id()) {

            $client_review = new CarReview();

            $client_review->client_id = Auth::id();
            $client_review->dealer_id = $request->input('dealer_id');
            $client_review->car_id = $request->input('car_id');
            $client_review->title = $request->input('title');
            $client_review->message = $request->input('message');
            $client_review->rating = $request->input('rating');
            $client_review->message_by = "client";
            $client_review->save();

            /* $lastInsertId = DB::table('client_reviews')->orderBy('id', 'DESC')->first();
            $client_review::where('id',$lastInsertId->id)->update(array('reply_to' => ''.$lastInsertId->id.'')); */

            return redirect()->back()->with('message', 'Rating and review successfully submitted');

        }

        return redirect()->back()->with('error', 'Please login before rate this vehicle');
    }


    //REVIEW SAVE

    public function review_save_ajax(Request $request)
    {

        if (Auth::id()) {


            $client_review = new CarReview();

            $client_review->client_id = Auth::id();
            $client_review->dealer_id = $request->input('dealer_id');
            $client_review->car_id = $request->input('car_id');
            $client_review->title = $request->input('title');
            $client_review->message = $request->input('message');
            $client_review->rating = $request->input('rating');
            $client_review->message_by = "client";
            $client_review->reply_to = null;
            $client_review->save();

            return redirect()->back()->with('message', 'Your rate and review successfully submitted');

        }

        return redirect()->back()->with('error', 'Please login before rate this vehicle');
    }


    //REVIEW update

    public function review_update_ajax(Request $request)
    {

        if (Auth::id()) {

            $client_review = new CarReview();

            if (empty($request->input('Nrating'))) {
                $rating = $request->input('Orating');
            } else {
                $rating = $request->input('Nrating');
            }


            $update_data = [
                'dealer_id' => $request->input('dealer_id'),
                'client_id' => Auth::id(),
                'car_id' => $request->input('car_id'),
                'title' => $request->input('title'),
                'message' => $request->input('message'),
                'message_by' => "client",
                'rating' => $rating,
                'reply_to' => null,
                'updated_at' => date('Y-m-d H:i:s'),
            ];


            $update_reviews = $client_review::where('client_id', Auth::id())
                ->where('car_id', $request->input('car_id'))->where('reply_to', null);
            $update_reviews->update($update_data);

        }

        return redirect()->back()->with('error', 'Please login before rate this vehicle');
    }


    // REVIEW DELETE


    public function review_delete_ajax(Request $request)
    {

        if (Auth::id()) {

            $client_review = new CarReview();

            $delete_review = $client_review::where('id', $request->input('review_id'));

            $delete_review->delete();

        }

        return redirect()->back()->with('error', 'Please login before rate this vehicle');

    }


    // Message reply

    public function review_reply_ajax(Request $request)
    {

        if (Auth::id()) {

            $client_review = new CarReview();

            $client_review->client_id = Auth::id();
            $client_review->dealer_id = $request->input('dealer_id');
            $client_review->car_id = $request->input('car_id');
            $client_review->message = $request->input('message');
            $client_review->message_by = "client";
            $client_review->reply_to = $request->input('message_id');
            $client_review->save();

        }

        return redirect()->back()->with('error', 'Please login before rate this vehicle');

    }


    public function review_reply_dealer_ajax(Request $request)
    {


        if (session()->get('dealer_id')) {

            $client_review = new CarReview();

            $client_review->client_id = $request->input('client_id');
            $client_review->dealer_id = $request->input('dealer_id');
            $client_review->car_id = $request->input('car_id');
            $client_review->message = $request->input('message');
            $client_review->message_by = "dealer";
            $client_review->reply_to = $request->input('message_id');
            $client_review->save();

        }

        return redirect()->back()->with('error', 'Please login before rate this vehicle');
    }


    public function message_report_ajax(Request $request)
    {


        if (session()->get('dealer_id')) {


            $message_report = new AdminReviewControl();

            $message_report->client_id = $request->input('client_id');
            $message_report->dealer_id = session()->get('dealer_id');
            $message_report->car_id = $request->input('car_id');
            $message_report->message_id = $request->input('message_id');
            $message_report->message_report = $request->input('message_report');
            $message_report->save();

            $message = "Complain has been sent to OttoGuide Administration";
            echo $message;
            exit;
        }

        return redirect()->back()->with('error', 'Please login before rate this vehicle');

    }


    // REVIEWS OFF AND ON

    public function review_block_ajax(Request $request)
    {

        $Reviews_Setting = new ReviewSetting();

        $Reviews_Setting->car_id = $request->input('car_id');

        $Reviews_Setting->reviews_off = 1;

        $Reviews_Setting->save();

    }


    public function review_unblock_ajax(Request $request)
    {

        $Reviews_Setting = new ReviewSetting();

        $unblock_reviews = $Reviews_Setting::where('car_id', $request->input('car_id'));

        $unblock_reviews->delete();

    }


    public function review_details($id)
    {

        $car_detail = ApiCaller::get_car_detail($id);

        return view('dealer/reviews-details')->with('car_detail', $car_detail);

    }


    //USER SERVICES BY EMAIL COMMUNICATION START

    public function user_emailbox()
    {

        return view('client/emailbox');

    }


    public function user_services_email(Request $request)
    {

        $dealer_data = [
            'dealer_name' => $request->input('dealer_name'),
            'dealer_id' => $request->input('dealer_id'),
			'car_heading' => $request->input('car_heading'),
        ];

        return view('client/email-compose', compact('dealer_data'));
    }


        public function user_services_email_send(Request $request)
         
		{

        Common::addon_inc($request->input('dealer_id'),'email_lead_addon',1);

        $services_email = new EmailBox();

        $services_email->dealer_id = $request->input('dealer_id');//1011698;
        $services_email->client_id = Auth::id();
        $services_email->subject = $request->input('subject');
        $services_email->message = $request->input('message');
        $services_email->services = json_encode(Session::get('services'));
        $services_email->message_by = "client";
        $services_email->save();

	   //send email to dealer
		
	   $get_dealer = DB::table('dealer')->where('dealer_id',$request->input('dealer_id'))->get()->first();

	   $message = "Dear " .$get_dealer->dealer_name." kindly check email box user has been sent you a email.";
		
	   $get_admin_email = DB::table('users')->where('name','Admin')->get()->first();
	
		$data =[
			'name'=> $get_dealer->dealer_name,
			'message'=> $message,
			'car_link'=> '',
			'details'=>'',
			'subject'=> 'Email Message ('.Auth::user()->name.')'
			
		 ];
	  
	    $dealer_email = $get_dealer->dealer_email;

	  
	    Mail::to($dealer_email)->send(new all_customers_emails($data)); 
		
        $delete_duplicate = $services_email::where('message', $request->input('message'))->orderBy('id', 'DESC')->get();

        if (count($delete_duplicate) > 1) {

            DB::table('email_box')->where('message', $request->input('message'))->orderBy('id', 'DESC')->limit(1)->delete();
        } else {

        }

        //START Insert data in sms table

        $save_sms_details = new Sms();

        $dealer_id = $request->input('dealer_id'); //1011698

        $is_dealer_registered = DB::table('dealer')->where('dealer_id', $dealer_id)->get()->first();

        if ($is_dealer_registered->is_registered == 1) {
            $sms = "Hello," . $is_dealer_registered->dealer_name . " someone requested for car service. Kindly Check dev.ottoguide.com (Dealer Panel).";
        } else {
            $sms = "Hello," . $is_dealer_registered->dealer_name . " someone requested for car service. Kindly Signup to dev.ottoguide.com/dealer/signup to avail this service.";
        }

        $save_sms_details->client_id = Auth::id();
        $save_sms_details->dealer_id = $request->input('dealer_id'); // "1011698"
        $save_sms_details->phone_number = $is_dealer_registered->dealer_phone;
        $save_sms_details->sms = $sms;
        $save_sms_details->status = '0';
        $save_sms_details->save();

        //END Insert data in sms table
        return view('client/emailbox')->with('message', 'Services email has been sent to ' . $request->input('dealer_name') . '.');

    }


    public function mail_reply_user(Request $request)
    {
        Common::addon_inc($request->input('dealer_id'),'email_lead_addon',1);

        $client_reply_save = new EmailBox();

        $client_reply_save->client_id = Auth::id();
        $client_reply_save->dealer_id = $request->input('dealer_id'); //1011698
        $client_reply_save->subject = $request->input('subject');
        $client_reply_save->message = $request->input('message');
        $client_reply_save->services = $request->input('services');
        $client_reply_save->message_by = 'client';
        $client_reply_save->reply_to = $request->input('email_id');
        $client_reply_save->save();


        $delete_duplicate = $client_reply_save::where('message', $request->input('message'))->orderBy('id', 'DESC')->get();

        if (count($delete_duplicate) > 1) {

            DB::table('email_box')->where('message', $request->input('message'))->orderBy('id', 'DESC')->limit(1)->delete();

        } else {

        }


        return view('client/emailbox')->with('message', 'Email reply has been sent to dealer.');

    }


    public function email_read_user(Request $request)
    {

        $get_email_data = EmailBox::where('email_box.id', decrypt($request->get('email_id')))
            ->join('users', 'client_id', '=', 'users.id')
            ->join('dealer', 'email_box.dealer_id', '=', 'dealer.dealer_id')
            ->select('email_box.id as email_id', 'users.id as user_id', 'email_box.dealer_id', 'client_id',
                'image', 'name', 'subject', 'message', 'services', 'message_by', 'reply_to', 'is_read',
                'email_box.created_at as email_date', 'users.created_at as user_date', 'dealer_name')
            ->get();

        //update email status

        EmailBox::where('id', decrypt($request->get('email_id')))->update(array('is_read' => 1));

        return view('client/read-email-user', compact('get_email_data'));

    }


    public function mail_temp_delete(Request $request)
    {

        $delete_ids = explode(',', $request->input('delete_ids'));

        foreach ($delete_ids as $delete_id) {

            if ($request->input('email_status') == 'Trash') {

                $email_delet_permanent = EmailBox::where('id', $delete_id);

                $update_data = [
                    'is_delete_by_user' => 1,
                ];

                $email_delet_permanent->update($update_data);

            } else {

                EmailBox::where('id', $delete_id)->update(array('temp_delete_by_user' => 1));

            }

        } // end foreach

        return redirect()->back()->with('message', 'Email deleted successfully');

    }

    public function empty_trash()
    {

        if (Auth::user()) {

            $empty_trash = EmailBox::where('client_id', Auth::user()->id)->where('temp_delete_by_user', 1);

            $update_data = [
                'is_delete_by_user' => 1,
            ];


            $empty_trash->update($update_data);

        }

        return redirect()->back()->with('message', 'Trash has been empty');

    }

    //USER SERVICES BY EMAIL COMMUNICATION END

    public function car_service(Request $request)
    {

        $year = ApiCaller::get_facets_by_type('year');
        $make = ApiCaller::get_facets_by_type('make');

        return view('web_pages/car_service')->with(
            array(
                'year' => $year,
                'make' => $make
            )
        );

    }

    public function car_service_facets(Request $request)
    {

        $html = '';


        if ($request->has('year') && $request->has('make') && $request->has('model')) {
            $parent = 'year=' . $request->input('year');
            $parent .= '&make=' . $request->input('make');
            $parent .= '&model=' . $request->input('model');
            $models = ApiCaller::get_facets_by_type('trim', $parent);

            $html .= '<option value="">Select Trim</option>';
            foreach ($models->facets->trim as $model) {
                $html .= '<option value="' . $model->item . '">' . $model->item . '</option>';
            }
        } else if ($request->has('year') && $request->has('make')) {
            $parent = 'year=' . $request->input('year');
            $parent .= '&make=' . $request->input('make');
            $models = ApiCaller::get_facets_by_type('model', $parent);

            $html .= '<option value="">Select Model</option>';
            foreach ($models->facets->model as $model) {
                $html .= '<option value="' . $model->item . '">' . $model->item . '</option>';
            }
        } else if ($request->has('year')) {
            $parent = 'year=' . $request->input('year');
            $models = ApiCaller::get_facets_by_type('make', $parent);

            $html .= '<option value="">Select Make</option>';
            foreach ($models->facets->make as $make) {
                $html .= '<option value="' . $make->item . '">' . $make->item . '</option>';
            }
        }


        echo $html;
        die();
    }


    public function save_services(Request $request)
    {
        $tire_replace = "";
        if ($request->input('tire-replace')) {
            $tire_replace = implode(',', $request->input('tire-replace'));
        }

        $tire_repair = "";
        if ($request->input('tire-repair')) {
            $tire_repair = implode(',', $request->input('tire-repair'));
        }

        $services_details = [

            'Year' => $request->input('year'),
            'Model' => $request->input('model'),
            'Make' => $request->input('make'),
            'Trim' => $request->input('trim'),
            'Mileage' => $request->input('mileage'),
            'Radius' => $request->input('radius'),

            'Describe Problem' => $request->input('describe-problem'),
            'General Maintenance' => $request->input('general-maintenance'),
            'Scheduled Maintenance' => $request->input('scheduled-maintenance'),

            'Inspection Diagnostic' => $request->input('inspection-diagnostic'),
            'Vehicle Start' => $request->input('vehicle-start'),
            'Vibrations Noises' => $request->input('vibrations-noises'),

            'Visual Problem' => $request->input('visual-problem'),
            'Odor/Smell' => $request->input('odor-smell'),
            'Dashboard Service' => $request->input('dashboard-service'),

            'Air Conditioning and Heating' => $request->input('ach'),
            'Brakes' => $request->input('brakes'),
            'Electrical' => $request->input('electrical'),

            'Exhaust' => $request->input('exhaust'),
            'Fluids' => $request->input('fluids'),
            'Steering Suspension' => $request->input('steering-suspension'),

            'Wheel Alignment' => $request->input('wheel-alignment'),
            'Tire Replace' => $tire_replace,
            'Tire Repair' => $tire_repair,
        ];


        $services_details = array_filter($services_details, 'strlen');


        $request->session()->put('services', $services_details);
        Session::put('search_radius', $request->input('radius'));


        return redirect('services/show/dealers');
    }


    public function services_radius_update(Request $request)
    {

        $radius = $request->input('radius');

        Session::put('services.Radius', $radius);
        Session::put('search_radius', $radius);

    }


    public function clear_services()
    {

        session()->forget('services');

        return redirect()->back()->with('message', 'Your selected services cleared successfully');

    }


    public function services_show_dealers(Request $request)
    {

        $services_details = $request->session()->get('services');

        $lat = '';
        $long = '';
        if (Session::has('geo_location_session')) {
            $lat = Session::get('geo_location_session')['latitude'];
            $long = Session::get('geo_location_session')['longitude'];
        }


        $get_dealer = array(

            'make' => Session::get('services')['Make'],
            'year' => Session::get('services')['Year'],
            'model' => Session::get('services')['Model'],
            'mileage' => Session::get('services')['Mileage'],
            'rows' => 50,
            'sort_order' => 'desc',
            'latitude' => $lat,
            'longitude' => $long,
            'radius' => (isset(Session::get('services')['Radius']) ? Session::get('services')['Radius'] : '10'),
        );

        $get_image_api = self::run_api('search/car/active', $get_dealer);
        $response = self::run_api('dealers/car', $get_dealer);
		
        $get_image_result = json_decode($get_image_api);
        $result = json_decode($response);


        $car_image = $get_image_result->listings->media->photo_links[0];
        $all_dealers = $result->dealers;

        $total = $result->num_found;


        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $perPage = 9;

        $currentItems = array_slice($all_dealers, $perPage * ($currentPage - 1), $perPage);

        $paginator = new LengthAwarePaginator($currentItems, count($all_dealers), $perPage, $currentPage, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);


        $dealer = new Dealer();


        return view('web_pages/dealer-list')->with(
            array(
                'car_image' => $car_image,
                'all_dealers' => $paginator,
                'services_details' => $services_details,
                'dealer_model' => $dealer
            )
        );


    }


// Dealer save by client

    public function save_mydealer($dealer_id)
    {

        $check_dealer = Dealer_favourite::where('dealer_id', $dealer_id)->Where('client_id', Auth::id())->get()->first();

        if (count($check_dealer)) {

            return redirect()->back()->with('error', 'Dealer already saved in your list');

        } else {

            $favourite = new Dealer_favourite;
            $favourite->dealer_id = $dealer_id;
            $favourite->client_id = Auth::id();
            $favourite->save();

            return redirect()->back()->with('message', 'Dealer Successfully Saved');

        }

    }


    public function get_mydealer()
    {

        $contact_dataa = Dealer_favourite::where('client_id', Auth::id())->get();

        $dealer_datas = array();

        if (count($contact_dataa)) {

            foreach ($contact_dataa as $contact_data) {

                $dealer_id = $contact_data->dealer_id;

                $dealer_datas[] = Dealer::where('dealer_id', $dealer_id)->get();

            }


            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $perPage = 2;

            $currentItems = array_slice($dealer_datas, $perPage * ($currentPage - 1), $perPage);

            $paginator = new LengthAwarePaginator($currentItems, count($dealer_datas), $perPage, $currentPage, [
                'path' => LengthAwarePaginator::resolveCurrentPath(),
            ]);

        }


        return view('client/savedealer')->with('dealer_datas', $paginator);

    }


    public function delete_mydealer($dealer_id)
    {

        $my_dealer_delete = Dealer_favourite::where('dealer_id', $dealer_id);

        $my_dealer_delete->delete();

        return redirect(route('get_my_dealer'))->with('message', 'Your saved dealer successfully deleted.');

    }


    // CONTACT REQUEST

    public function ContactReuqest(Request $request)
    {

        if (Auth::id()) {

            $contact_request = $request->get('contact');

            $contact = new ContactRequest();


            $email_crons = Dealer::where('dealer_id', 1011698)->where('is_registered', 1)->get();

            foreach ($email_crons as $email_cron) {

                $to_email = $email_cron->dealer_email;
                $name = $email_cron->dealer_name;

                $data = [
                    'name' => $name,
                    'contactrequest' => $request->get('contact'),
                    'client_name' => Auth::user()->name,
                    'client_email' => Auth::user()->email,
                    'client_phone' => Auth::user()->phone,
                ];

                //send contact request to dealer email

                Mail::to($to_email)->send(new contact_request_send($data));

            }


            $contact->dealer_id = $request->get('dealer_id');
            $contact->car_id = $request->get('car_id');
            $contact->client_id = Auth::id();
            $contact->request_type = $contact_request;
            $contact->car_model = $request->get('car_model');
            $contact->car_make = $request->get('car_make');
            $contact->car_year = $request->get('car_year');
            $contact->car_vin = $request->get('car_vin');
            $contact->created_at = date('Y-m-d H:i:s');

            $contact->save();

            return redirect()->back()->with('message', 'Contact request has been sent.');

        } else {
            return redirect()->back()->with('error', 'Please login before send request.');
        }

    }

    // Lease Purchase Offers

    public function LeasePurchase(Request $request)
    {

        if (Auth::id()) {

            $offer = $request->get('offer');

            $offers = new Offers();

            $check_status = $offers::where('offer', $offer)->where('client_id', Auth::id())->where('car_id', $request->get('car_id'))->get();

            $check_offer = $offers::where('dealer_id', $request->get('dealer_id'))->where('client_id', Auth::id())->where('car_id', $request->get('car_id'))->get();


            if (count($check_status)) {

                return redirect()->back()->with('error', 'You have already sent ' . $offer . ' offer for this car.');

            }

            /* if(count($check_offer)){

            $update_data = [
                    'dealer_id' => $request->get('dealer_id'),
                    'client_id' => Auth::id(),
                    'car_id' => $request->input('car_id'),
                    'offer' => $request->get('offer'),
                    'updated_at' => date('Y-m-d H:i:s'),
            ];

            $offer_update = $offers::where('client_id',Auth::id())->first();
            $offer_update->update($update_data);

            return redirect()->back()->with('message', ''.$offer .' offer has successfully sent to dealer.');

            } */


            // OFFER SEND
            $offers->dealer_id = $request->get('dealer_id');
            $offers->car_id = $request->get('car_id');
            $offers->client_id = Auth::id();
            $offers->offer = $request->get('offer');
            $offers->save();

            return redirect()->back()->with('message', '' . $offer . ' offer has successfully sent to dealer.');

        } else {
            return redirect()->back()->with('error', 'Please login before send offer.');
        }

    }

    // End dealer Save by client.

    public function sendEmail()
    {

        $email_crons = DealerCron::where('is_registered', 1)->get();

        foreach ($email_crons as $email_cron) {

            if ($email_cron->where('date', '=', date("Y-m-d"))->pluck('total_in_search')->count() > 0) {

                $to_email = $email_cron->dealer_email;
                $name = $email_cron->dealer_name;
                $search_count = $email_cron->where('date', '=', date("Y-m-d"))->pluck('total_in_search')->count();
                $detail_count = $email_cron->where('date', '=', date("Y-m-d"))->pluck('total_in_detail')->count();

                $data = [
                    'name' => $name,
                    'search_count' => $search_count,
                    'detail_count' => $detail_count
                ];
                Mail::to($to_email)->send(new myemail($data));
            }
        }
    }

    public function load_param(Request $request)
    {
        $s_type = "";
        if ($request->input('load_model')) {
            $make = $request->input('make');
            $s_type = "model";
        } else {
            $make = "";
            $s_type = lcfirst($request->input('type'));
        }
        $res = "";

        $params = ApiCaller::get_facets($s_type, $make);

        if (isset($params->facets->$s_type)) {
            $aarr = $params->facets->$s_type;
            $res .= '<option value="">Select ' . $s_type . '</option>';
            if ($s_type == "model")
                $res .= '<option value="">All Models</option>';

            foreach ($aarr as $key => $value) {
                $res .= '<option value="' . $value->item . '">' . $value->item . '</option>';
            }
        } else
            $res = '<option value="">No data available</option>';

        echo $res;
        exit;
    }

    public function load_param_for_search(Request $request)
    {

        $car_type = $request->input('car_type');

        Session::put('car_type', $request->input('car_type'));
        $s_type = "";
        $make = "";

        $options = [
            'year' => $request->input('myear'),
            'price' => $request->input('mprice'),
            'miles' => $request->input('mmiles'),
            'body_type' => $request->input('mbody_type'),
            'fuel_type' => $request->input('mfuel_type'),
            'door' => $request->input('mdoor'),
            'transmission' => $request->input('mtransmission'),
            'drivetrain' => $request->input('mdrivetrain'),
            'dom' => $request->input('mdom'),
            'Interior_color' => $request->input('mInterior_color'),
            'Exterior_color' => $request->input('mExterior_color'),
            'make' => $request->input('mmake'),
            'model' => $request->input('mmodel'),
        ];


        $options = array_filter($options, 'strlen');


        if ($request->input('load_model')) {
            $make = lcfirst($request->input('make'));
            $s_type = "model";
            $params = ApiCaller::get_facets_for_search($car_type, $s_type, $make, '', $options);

        } else if ($request->input('load_trim')) {
            $make = lcfirst($request->input('make'));
            $model = $request->input('model');
            $s_type = "trim";
            $params = ApiCaller::get_facets_for_search($car_type, $s_type, $make, $model, $options);

        } else {
            $make = "";
            $s_type = lcfirst($request->input('type'));

//            if ($s_type == "year") {
//                $options['year'] = "";
//            }

            if ($s_type == 'price' || $s_type == 'miles' || $s_type == 'dom') {
                $params_range = ApiCaller::get_facets_range_for_search($car_type, $s_type, $options);
            } else if ($s_type != '') {

                $params = ApiCaller::get_facets_for_search($car_type, $s_type, "", "", $options);

            }
        }

        $res = "";
        if (isset($params->facets->$s_type)) {
            $aarr = $params->facets->$s_type;

            if (count($aarr) > 0) {

                if($s_type == 'drivetrain'){
                    $s_type = 'drive_train';
                }

                $type = str_replace('_', ' ', $s_type);
                $type = ucwords($type);
                $res .= '<option value="">Select ' . $type . '</option>';


                if ($s_type == "model") {
                    $res .= '<option value="">All Models</option>';
                }
                if ($s_type == "make") {
                    $res .= '<option value="">All Make</option>';
                }
                if ($s_type == "trim") {
                    $res .= '<option value="">All Trim</option>';
                }

                if ($s_type == 'year') {
                    foreach (array_reverse($aarr) as $key => $value) {
                        $res .= '<option value="' . $value->item . '">' . $value->item . '</option>';
                    }
                } else {
                    foreach ($aarr as $key => $value) {
                        $res .= '<option value="' . $value->item . '">' . $value->item . '</option>';
                    }
                }


            } else {
                $res = '<option value="">No data available</option>';
            }


        } else if (isset($params_range->range_facets->$s_type)) {
            $aarr = $params_range->range_facets->$s_type;
            $aarr = $aarr->counts;

            if (count($aarr) > 0) {
                $type = str_replace('_', ' ', $s_type);
                $type = ucwords($type);
                $res .= '<option value="">Select ' . $type . '</option>';
                foreach ($aarr as $ar) {
                    $lower_bound = '';
                    $upper_bound = '';
                    $count = '';
                    foreach ($ar as $key => $value) {
                        if ($key == 'lower_bound') {
                            $lower_bound = $value;
                        } else if ($key == 'upper_bound') {
                            $upper_bound = $value;
                        } else if ($key == 'count') {
                            $count = $value;
                        }

                    }
                    $res .= '<option value="' . $lower_bound . '-' . $upper_bound . '">' . $lower_bound . '-' . $upper_bound . '</option>';
                }
            } else {
                $res = '<option value="">No data available</option>';
            }


        } else {
            $res = '<option value="">No data available</option>';
        }

        echo $res;
        exit;
    }

    public function load_param_for_search_range(Request $request)
    {

        $car_type = $request->input('car_type');
        Session::put('car_type', $request->input('car_type'));
        $s_type = lcfirst($request->input('type'));
        if ($s_type == 'price' || $s_type == 'miles' || $s_type == 'dom') {
            $params_range = ApiCaller::get_facets_range_for_search($car_type, $s_type);
        }

        if (isset($params_range->range_facets->$s_type)) {
            $aarr = $params_range->range_facets->$s_type;
            $aarr = $aarr->counts;
            if (count($aarr) > 0) {

                return json_encode(array('status' => true, 'data' => $aarr));
            }

        }

        return json_encode(array('status' => false, 'data' => ''));
    }

    public function load_facets_values(Request $request)
    {
        $s_type = lcfirst($request->input('type'));
        $res = "";
        if($s_type == 'price' || $s_type == 'miles' || $s_type == 'dom'){

            $params_range = ApiCaller::get_facets_range($s_type, '');

            $aarr = $params_range->range_facets->$s_type;
            $aarr = $aarr->counts;

            if (count($aarr) > 0) {
                $type = str_replace('_', ' ', $s_type);
                $type = ucwords($type);
                $res .= '<option value="">Select ' . $type . '</option>';
                foreach ($aarr as $ar) {
                    $lower_bound = '';
                    $upper_bound = '';
                    $count = '';
                    foreach ($ar as $key => $value) {
                        if ($key == 'lower_bound') {
                            $lower_bound = $value;
                        } else if ($key == 'upper_bound') {
                            $upper_bound = $value;
                        } else if ($key == 'count') {
                            $count = $value;
                        }

                    }
                    $res .= '<option value="' . $lower_bound . '-' . $upper_bound . '">' . $lower_bound . '-' . $upper_bound . '</option>';
                }
            } else {
                $res = '<option value="">No data available</option>';
            }

            echo $res;

        }
        else{

            $params = ApiCaller::get_facets($s_type, '');

            if (isset($params->facets->$s_type)) {
                $aarr = $params->facets->$s_type;
                $res .= '<option value="">Select ' .  str_replace('_', ' ', ucwords($s_type)) . '</option>';
                foreach ($aarr as $key => $value) {
                    $res .= '<option value="' . $value->item . '">' . $value->item . '</option>';
                }

            } else {
                $res = 'No data available';
            }
            echo $res;
        }




        exit;
    }

    public function load_param_checkbox(Request $request)
    {
        $s_type = "";
        $question_key = $request->input('question_key');
        if ($request->input('load_model')) {
            $make = $request->input('make');
            $s_type = "model";
        } else {
            $make = "";
            $s_type = lcfirst($request->input('type'));
        }
        $res = "";

        $params = ApiCaller::get_facets($s_type, $make);

        if (isset($params->facets->$s_type)) {
            $aarr = $params->facets->$s_type;

            if ($s_type == "model") {
                $res .= '<br>';
            }

            $count = 1;
            foreach ($aarr as $key => $value) {
                $res .= '<div class="checkboxItem' . $count . '-' . $question_key . '"><input type="checkbox" id="checkboxItem' . $count . '-' . $question_key . '" value="' . $value->item . '" name="checkboxItem' . $question_key . '"><label> ' . $value->item . ' </label><span id="close_checkbox" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;"><i class="fa fa-close"></i></span></div>';
                $count++;
            }
        } else {
            $res = 'No data available';
        }
        echo $res;
        exit;
    }

    public function load_param_radiobox(Request $request)
    {
        $s_type = "";
        $question_key = $request->input('question_key');
        if ($request->input('load_model')) {
            $make = $request->input('make');
            $s_type = "model";
        } else {
            $make = "";
            $s_type = lcfirst($request->input('type'));
        }
        $res = "";

        $params = ApiCaller::get_facets($s_type, $make);

        if (isset($params->facets->$s_type)) {
            $aarr = $params->facets->$s_type;

            if ($s_type == "model") {
                $res .= '<br>';
            }

            $count = 1;
            foreach ($aarr as $key => $value) {
                $res .= '<div class="radioboxItem' . $count . '-' . $question_key . '"><input type="radio" id="radioboxItem' . $count . '-' . $question_key . '" value="' . $value->item . '" name="radioboxItem' . $question_key . '"><label> ' . $value->item . ' </label><span id="close_radiobox" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;"><i class="fa fa-close"></i></span></div>';
                $count++;
            }
        } else {
            $res = 'No data available';
        }
        echo $res;
        exit;
    }

    public function load_param_dropdown(Request $request)
    {
        if ($request->input('load_model')) {
            $make = $request->input('make');
            $s_type = "model";
        } else {
            $make = "";
            $s_type = lcfirst($request->input('type'));
        }
        $res = "";

        $params = ApiCaller::get_facets($s_type, $make);

        if (isset($params->facets->$s_type)) {
            $aarr = $params->facets->$s_type;

            if ($s_type == "model") {
                $res .= '<br>';
            }

            $count = 1;
            foreach ($aarr as $key => $value) {
                $res .= '<option value="' . $value->item . '">' . $value->item . '</option>';
                $count++;
            }
        } else {
            $res = 'No data available';
        }
        echo $res;
        exit;
    }

    public function dealer_info(Request $request)
    {

        $dealer = Dealer::where('dealer_id', $request->dealerid)->get();
        return $dealer;
    }
}