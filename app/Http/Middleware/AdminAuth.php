<?php


namespace App\Http\Middleware;

use Closure;
use DB;
use Session;
use View;

class AdminAuth {
	public function handle($request, Closure $next) {

		if(Session::has('adminSession')){
			return $next($request);
		} else{
			return redirect(route('login_page'));
		}
	}
}