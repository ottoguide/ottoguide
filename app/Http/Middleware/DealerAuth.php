<?php


namespace App\Http\Middleware;

use App\Models\Dealer;
use Closure;

use View;

class DealerAuth {

	public function handle($request, Closure $next) {

		if (!$request->session()->get('is_dealer_login') || $request->session()->get('is_dealer_login') != 1) {
			return redirect('dealer/login')->with('status', 'Please login to your account');;
		}

		$dealer_data = Dealer::where('dealer_id', $request->session()->get('dealer_id'))->first();

		View::share ('dealer_data', $dealer_data);


		return $next($request);
	}

}