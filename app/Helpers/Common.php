<?php

namespace App\Helpers;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class Common {

	public static function my_id($id) {
		/*$key = config('app.key');
		if (Str::startsWith($key, 'base64:')) {
			//$key = base64_decode(substr($key, 7));
			$key = substr($key, 7);
		}*/

		return md5($id);
	}

	public static function my_column ($col = 'id') {

		return DB::raw("md5({$col})");
	}

	public static function is_addon_subscribed($dealer_id,$addon){
        $dealer_data = Dealer::where('dealer_id', $dealer_id)->where($addon,1)->first();

        if(!empty($dealer_data)){
            return array("status" => true, "msg" => "success");
        }
        else{
            return array("status" => false, "msg" => "fail");
        }

    }

	public static function addon_inc($dealer_id,$addon,$units){

	    if($dealer_id > 0 && $units > 0 && ($addon == 'market_analysis_addon' || $addon == 'rbo_lead_addon' || $addon == 'dealer_pricing_tool_addon' || $addon == 'voice_lead_addon' || $addon == 'email_lead_addon' || $addon == 'activity_notify_addon' || $addon == 'chat_lead_addon')){
            DB::table('dealer_addon_logs')->insert(
                [
                    'dealer_id' => $dealer_id,
                    'addon' => $addon,
                    'units' => $units,
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> date('Y-m-d H:i:s')
                ]
            );

            return array("status" => true, "msg" => "success");
        }
        else{
            return array("status" => false, "msg" => "fail");
        }

    }



	
	
}