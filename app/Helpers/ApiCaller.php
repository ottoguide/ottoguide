<?php

namespace App\Helpers;

use Session;

//define("API_URL","http://api.marketcheck.com/v1/");
//define("API_KEY","pDqi68qUcoVkXh9kjmEzsDzVD7Ta0w0P");

class ApiCaller
{

    public static function run_api($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Host: marketcheck-prod.apigee.net"
            ),
        ));

        $response = @curl_exec($curl);
        $err = @curl_error($curl);

        curl_close($curl);

        if ($err)
            return 'fail';

        return $response;
    }

    public static function search_car()
    {

        $limit = (isset($_GET['per_page']) ? $_GET['per_page'] : 10);
        $start = (isset($_GET['page']) ? (($_GET['page']-1)*$limit+1) : 0 );
        //$start = (isset($_GET['page']) ? $_GET['page'] : 0);
        $sort_by = (isset($_GET['sort_by']) ? $_GET['sort_by'] : 'price');
        $sort_order = (isset($_GET['sort_order']) ? $_GET['sort_order'] : 'desc');
        $url = "";
        if (isset($_GET['filter_search'])) {
            $url = API_URL . "search/car/active?api_key=" . API_KEY;

            if (isset($_GET['car_type'])) {
                Session::put('car_type', $_GET['car_type']);
            }

            if (!isset($_GET['car_type'])) {
                Session::put('car_type', 'all');
            }

            if (isset($_GET['car_type']) && $_GET['car_type'] != "" && $_GET['car_type'] != "all")
                $url .= "&car_type=" . $_GET['car_type'];

            if (isset($_GET['vin']) && $_GET['vin'] != "")
             $url .= "&vins=" . $_GET['vin'];

            if (isset($_GET['price']) && $_GET['price'] != "")
                $url .= "&price_range=" . str_replace(array(" ", "$"), array("", ""), $_GET['price']);

            if (isset($_GET['year']) && $_GET['year'] != "")
                $url .= "&year=" . str_replace(" ", "", ltrim($_GET['year'], ','));

            if (isset($_GET['make']) && $_GET['make'] != "")
                $url .= "&make=" . lcfirst($_GET['make']);

            if (isset($_GET['model']) && $_GET['model'] != "")
                $url .= "&model=" . lcfirst($_GET['model']);

            if (isset($_GET['door']) && $_GET['door'] != "")
                $url .= "&doors=" . lcfirst($_GET['door']);

            if (isset($_GET['trim']) && $_GET['trim'] != "")
                $url .= "&trim=" . lcfirst($_GET['trim']);

            if (isset($_GET['drivetrain']) && $_GET['drivetrain'] != "")
                $url .= "&drivetrain=" . lcfirst($_GET['drivetrain']);

            if (isset($_GET['engine']) && $_GET['engine'] != "")
                $url .= "&engine=" . lcfirst($_GET['engine']);

            if (isset($_GET['body_type']) && $_GET['body_type'] != "")
                $url .= "&body_type=" . lcfirst($_GET['body_type']);

            if (isset($_GET['fuel_type']) && $_GET['fuel_type'] != "")
                $url .= "&fuel_type=" . lcfirst($_GET['fuel_type']);

            if (isset($_GET['Interior_color']) && $_GET['Interior_color'] != "")
                $url .= "&interior_color=" . lcfirst($_GET['Interior_color']);

            if (isset($_GET['exterior_color']) && $_GET['exterior_color'] != "")
                $url .= "&exterior_color=" . lcfirst($_GET['exterior_color']);

            if (isset($_GET['transmission']) && $_GET['transmission'] != "")
                $url .= "&transmission=" . $_GET['transmission'];

            if (isset($_GET['dom']) && $_GET['dom'] != "")
                $url .= "&dom_range=" . lcfirst($_GET['dom']);


            if (isset($_GET['latitude']) && isset($_GET['longitude'])) {
                $url .= "&latitude=" . str_replace(" ", "", $_GET['latitude']);
                $url .= "&longitude=" . str_replace(" ", "", $_GET['longitude']);
            }

            if (isset($_GET['radius']) && $_GET['radius'] != "") {
                $url .= "&radius=" . str_replace(" ", "", $_GET['radius']);
            }

            if (isset($_GET['miles']) && $_GET['miles'] != "")
                $url .= "&miles_range=" . str_replace(" ", "", $_GET['miles']);

            $url = $url . "&start=" . $start . "&rows=" . $limit;

        } else
            $url = API_URL . "search/car/active?api_key=" . API_KEY . "&latitude=40.759212&longitude=-73.984634&radius=30&car_type=used&make=ford&start=" . $start . "&rows=" . $limit;

        $url = $url . "&sort_by=" . $sort_by . "&sort_order=" . $sort_order;
		
        return self::run_api($url);
    }


    public static function get_car_detail($id)
    {

        $url = API_URL . "listing/car/" . $id . "?api_key=" . API_KEY;

        //echo $url; die;
        $response = self::run_api($url);
        return json_decode($response);
    }

    public static function getImageURL($imageSrc, $width = 300, $height = 300)
    {

        $imageurl = url('/') . "/timthumb/timthumb.php?src=" . $imageSrc . "&w=" . $width . "&h=" . $height . "&zc=3";
        return $imageurl;
    }

    public static function getDealerList()
    {

        $limit = (isset($_GET['per_page']) ? $_GET['per_page'] : 9);
        $start = (isset($_GET['page']) ? $_GET['page'] : 0);

        $url = API_URL . "dealers/car/?api_key=" . API_KEY . '&latitude=43.1856307&longitude=-77.7565881&radius=100&start=' . $start . '&rows=' . $limit;
					  
	    $response = self::run_api($url);
      
        return json_decode($response);
    }

    public static function get_facets($options, $other = "")
    {

        $url = API_URL . "search/car/active?api_key=" . API_KEY;
        if ($other != "")
            $url .= "&make=" . $other;

        $url .= "&facets=" . $options . "|0|1000&rows=0&facet_sort=index";

        $response = self::run_api($url);
        return json_decode($response);
    }

    public static function get_facets_range($options, $other = "")
    {

        $url = API_URL . "search/car/active?api_key=" . API_KEY;

        $limit = "|0|1000|50";
        if ($options == 'price') {
            $limit = "|0|300000|15000";
        } else if ($options == 'miles') {
            $limit = "|0|200000|10000";
        } else if ($options == 'dom') {
            $limit = "|0|1000|50";
        }

        $url .= "&start=0";
        $url .= "&rows=0";
        $url .= "&range_facets=" . $options . $limit;
        $url .= "&facet_sort=index";

        $response = self::run_api($url);
        return json_decode($response);
    }

    public static function get_facets_by_type($type, $parent = "")
    {


        $url = API_URL . "search/car/active?api_key=" . API_KEY;
        if ($parent != "")
            $url .= "&" . $parent;

        $url .= "&facets=" . $type . "|0|1000&rows=0&facet_sort=index";

        $response = self::run_api($url);
        return json_decode($response);
    }

    public static function get_facets_for_search($car_type = '', $facet_type = '', $make = '', $model = '', $options = '')
    {

        $lat = '';
        $long = '';
        $radius = '';
        if (Session::has('geo_location_session')) {
            $lat = Session::get('geo_location_session')['latitude'];
            $long = Session::get('geo_location_session')['longitude'];
            $radius = Session::get('search_radius');
        }


        $url = API_URL . "search/car/active?api_key=" . API_KEY;

        if ($lat != '') {
            $url .= "&latitude=" . $lat;
        }
        if ($long != '') {
            $url .= "&longitude=" . $long;
        }
        if ($radius != '') {
            $url .= "&radius=" . $radius;
        }
        if ($car_type == 'new' || $car_type == 'used' || $car_type == 'certified') {
            $url .= "&car_type=" . $car_type;
        }
        if ($make != '') {
            $url .= "&make=" . $make;
        }
        if ($model != '') {
            $url .= "&model=" . $model;
        }

        if ($facet_type == 'interior_color') {
        } else if ($facet_type == 'exterior_color') {
        } else {
            $url .= "&facet_sort=index";
        }

        $opt = '';
        if ($options != '') {

            foreach ($options as $key => $values) {
                $opt .= '&' . $key . '=' . $values;
            }

        }

        $url .= "&start=0";
        $url .= "&rows=0";
        $url .= "&facets=" . $facet_type . "|0|1000";
        $url .= $opt;




        $response = self::run_api($url);

        return json_decode($response);

    }

    public static function get_facets_range_for_search($car_type = '', $facet_type = '', $options = '')
    {

        $lat = '';
        $long = '';
        $radius = '';
        if (Session::has('geo_location_session')) {
            $lat = Session::get('geo_location_session')['latitude'];
            $long = Session::get('geo_location_session')['longitude'];
            $radius = 100;//Session::get('search_radius');
        }


        $url = API_URL . "search/car/active?api_key=" . API_KEY;

        if ($lat != '') {
            $url .= "&latitude=" . $lat;
        }
        if ($long != '') {
            $url .= "&longitude=" . $long;
        }
        if ($radius != '') {
            $url .= "&radius=" . $radius;
        }
        if ($car_type == 'new' || $car_type == 'used' || $car_type == 'certified') {
            $url .= "&car_type=" . $car_type;
        }

        $opt = '';
        if ($options != '') {

            foreach ($options as $key => $values) {
                $opt .= '&' . $key . '=' . $values;
            }

        }

        $limit = "|0|1000|50";
        if ($facet_type == 'price') {
            $limit = "|500|100000|5000";
        } else if ($facet_type == 'miles') {
            $limit = "|0|200000|10000";
        } else if ($facet_type == 'dom') {
            $limit = "|0|1000|50";
        }

        $url .= "&start=0";
        $url .= "&rows=0";
        $url .= "&range_facets=" . $facet_type . $limit;
        $url .= "&facet_sort=index";
        $url .= $opt;


        $response = self::run_api($url);

        return json_decode($response);

    }


}