@extends('layouts.web_pages')
@section('content')
  <style>
    p {
      margin-bottom: 0.3rem;
    }
    select :disabled.red-option{
      color: #d8d3d3;
    }
    .img-fixed img {
      height: 250px;
      width: 100%;
    }

    .img-fixed2 img {
      height: 178px;
      width: 100%;
    }
    .cxm-detail-border{
      border-top: 3px #158CBA solid;
      padding: 10px;
      margin-bottom: 35px;
      box-shadow: 0px 0px 5px #999;
      border-bottom-left-radius: 5px;
      border-bottom-right-radius: 5px;
      margin:8px;
    }
  </style>
  <?php $view =(isset($_GET['view']) ? $_GET['view'] : 1 );?>
  <div class="header-margin">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="card refine-search">
            <div class="card-header font-weight-bold"><a data-toggle="collapse" href="#refineSearch" role="button" aria-expanded="false" aria-controls="refineSearch">Select Features that you would like in your car</a></div>
            <div class="collapse show" id="refineSearch">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6 mb-3">
                    Vehicle Condition:
                    <div class="custom-control custom-checkbox mt-2 fs14 d-inline-block mx-1">
                      <input type="checkbox" name="car_type" value="new" class="custom-control-input chkBox" id="customCheckvc1">
                      <label class="custom-control-label" for="customCheckvc1">New</label>
                    </div>
                    <div class="custom-control custom-checkbox mt-2 fs14 d-inline-block mx-1">
                      <input type="checkbox" name="car_type" value="used" class="custom-control-input chkBox" id="customCheckvc2">
                      <label class="custom-control-label" for="customCheckvc2">Used</label>
                    </div>
                    <div class="custom-control custom-checkbox mt-2 fs14 d-inline-block mx-1">
                      <input type="checkbox" name="car_type" value="certified" class="custom-control-input chkBox" id="customCheckvc3">
                      <label class="custom-control-label" for="customCheckvc3">Certified</label>
                    </div>
                  </div>
                  <div class="col-md-6 mb-3 text-left text-md-right">
                    Find me a Car within
                    <select class="form-control form-control-sm w-auto d-inline-block mb-1" id="radius_select" onchange="$('#radius').val($(this).val());">
                      <option value="">Any distance</option>
                      <option value="10">10 miles</option>
                      <option value="20">20 miles</option>
                      <option value="30">30 miles</option>
                      <option value="40">40 miles</option>
                      <option value="50">50 miles</option>
                    </select>
                    of
                    <a data-toggle="collapse" href="#location-menu" id="geo_ip"></a>
                  </div>
                </div>
                <div class="form-row">
                  <div class="col-sm-3 mb-3">
                    <select onchange="load_param($(this).val());" id="main_select" class="form-control form-control-sm">
                      <option value="main_category">-Select Feature Category-</option>
                      <option value="Country">Country</option>
                      <option value="Price">Price</option>
                      <option value="Miles">Miles</option>
                      <option value="Year">Year</option>
                      <option value="Make">Make</option>
                      <option value="Body_type">Body Type</option>
                      <option value="Body_subtype">Body Sub Type</option>
                      <option value="Transmission">Transmission</option>
                      <option value="Seller_type">Seller Type</option>
                      <option value="Trim">Trim</option>
                      <option value="Drive_train">Drive Train</option>
                      <option value="Fuel">Fuel</option>
                      <option value="Doors">Doors</option>
                      <option value="Color">Color</option>
                    </select>
                  </div>
                  <div class="col-sm-3">
                    <select id="sub_select" disabled="disabled" class="form-control form-control-sm">
                      <option option_for="main_category">-Select Feature Category First-</option>
                      <option option_for="Country">US</option>
                      <option option_for="Country">CA</option>
                      <option option_for="Country">All</option>
                      <option option_for="Body_type">Suv</option>
                      <option option_for="Body_type">Sedan</option>
                      <option option_for="Body_type">Pickup</option>
                      <option option_for="Body_type">Hatchback</option>
                      <option option_for="Body_type">Coupe</option>
                      <option option_for="Body_type">Wagon</option>
                      <option option_for="Body_subtype">Crew Cab</option>
                      <option option_for="Transmission">Automatic</option>
                      <option option_for="Color">Any Color</option>
                      <option option_for="Color">Beige</option>
                      <option option_for="Color">Black</option>
                      <option option_for="Color">Blue</option>
                      <option option_for="Color">Brown</option>
                      <option option_for="Color">Gold</option>
                      <option option_for="Color">Gray</option>
                      <option option_for="Color">Green</option>
                      <option option_for="Color">Orange</option>
                      <option option_for="Color">Pink</option>
                      <option option_for="Color">Purple</option>
                      <option option_for="Color">Red</option>
                      <option option_for="Color">Silver</option>
                      <option option_for="Color">White</option>
                      <option option_for="Color">Yellow</option>
                      <option option_for="Miles">1000 - 5000</option>
                      <option option_for="Miles">5000 - 10000</option>
                      <option option_for="Miles">10000 - 15000</option>
                      <option option_for="Miles">15000 - 20000</option>
                      <option option_for="Miles">20000 - 25000</option>
                      <option option_for="Miles">20000 - 30000</option>
                      <option option_for="Miles">30000 - 40000</option>
                      <option option_for="Miles">40000 - 50000</option>
                      <option option_for="Miles">50000 - 60000</option>
                      <option option_for="Miles">60000 - 70000</option>
                      <option option_for="Miles">70000 - 80000</option>
                      <option option_for="Miles">80000 - 90000</option>
                      <option option_for="Miles">90000 - 100000</option>
                      <option option_for="Miles">100000 - 150000</option>
                      <option option_for="Miles">150000 - 200000</option>
                      <option option_for="Price">$1000 - $5000</option>
                      <option option_for="Price">$5000 - $10000</option>
                      <option option_for="Price">$10000 - $15000</option>
                      <option option_for="Price">$15000 - $20000</option>
                      <option option_for="Price">$20000 - $25000</option>
                      <option option_for="Price">$20000 - $30000</option>
                      <option option_for="Price">$30000 - $40000</option>
                      <option option_for="Price">$40000 - $50000</option>
                      <option option_for="Price">$50000 - $60000</option>
                      <option option_for="Price">$60000 - $70000</option>
                      <option option_for="Price">$70000 - $80000</option>
                      <option option_for="Price">$80000 - $90000</option>
                      <option option_for="Price">$90000 - $100000</option>
                      <option option_for="Price">$100000 - $150000</option>
                      <option option_for="Price">$150000 - $200000</option>
                      <option option_for="Make">Acura</option>
                      @for ($i = date("Y"); $i >= 2006; $i--)
                        <option option_for="Year" value="{{ $i }}">{{ $i }}</option>
                      @endfor
                    </select>
                  </div>
                  <div class="col-sm-3 mb-3 text-center">
                    <div class="custom-control custom-checkbox mt-2 fs14 d-inline-block">
                      <input type="checkbox" class="custom-control-input" id="customCheckmh1">
                      <label class="custom-control-label" for="customCheckmh1"><span class="d-sm-none d-md-inline-block">Must Have</span> Feature</label>
                    </div>
                  </div>
                  <div class="col-sm-3 mb-3">
                    <button onclick="update_tag($('#main_select').val(),$('#sub_select').val());" class="btn btn-primary btn-block"><span class="fa fa-plus-circle"></span> Add <span class="d-sm-none d-md-inline-block">this</span> feature</button>
                  </div>
                </div>
                <div class="tag-div form-row">
                  <div class="col-md-2 fs18 font-weight-bold lh18 d-flex align-items-center">Selected Features:</div>
                  <div class="col-md-10">
                    <input type="text" id="search_tage" data-role="tagsinput">
                  </div>
                </div>
              </div>
              <div class="card-footer bg-white">
                <div class="row">
                  <div class="col-sm-6 text-center text-sm-left mb-3 mb-sm-0">
                    <a href="javascript:;" data-toggle="modal" data-target="#save_search_model" class="btn btn-primary">Save This Search</a>
                  </div>
                  <div class="col-sm-6 text-center text-sm-right">
                    <a href="<?=url('/search-car.html')?>" class="btn btn-primary">Clear</a>
                    <form method="GET" action="" style="display: none;" id="searh-form">
                      <input type="hidden" class="form-field" name="price" id="search_Price">
                      <input type="hidden" class="form-field" name="country" id="search_Country">
                      <input type="hidden" class="form-field" name="miles" id="search_Miles">
                      <input type="hidden" class="form-field" name="year" id="search_Year">
                      <input type="hidden" class="form-field" name="make" id="search_Make">
                      <input type="hidden" class="form-field" name="color" id="search_Color">
                      <input type="hidden" class="form-field" name="body_type" id="search_Body_type">
                      <input type="hidden" name="filter_search" value="filter">
                        <?php
                        if(isset($_GET['filter_search'])){
                        foreach ($_GET as $key => $input) {
                        if(stripos($key, '-fea') !== false){ ?>
                      <input type="hidden" name="<?=$key?>" value="1" id="<?=$key?>">
                        <?php } } } ?>
                      <input type="hidden" name="radius" id="radius">
                      <input type="hidden" name="car_type" id="car_type">
                      <input type="hidden" name="latitude" id="latitude">
                      <input type="hidden" name="longitude" id="longitude">
                      <input type="hidden" name="zip" id="zip">
                      <input type="hidden" name="per_page" id="per_page_field">
                      <input type="hidden" name="view" id="view_field">
                    </form>
                    <button type="submit" onclick="$('#searh-form').submit();" class="btn btn-primary">Search</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="py-5">
    <div class="container">
      @if(isset($_GET['filter_search']))
        <div class="message"width="50%" align="center">
          @if (session('message'))
            <div class="alert alert-success" width="50%">
              {{ session('message') }}
            </div>
          @endif
        </div>

        <div class="message"width="50%" align="center">
          @if (session('error'))
            <div class="alert alert-danger" width="50%">
              {{ session('error') }}
            </div>
          @endif
        </div>
            <?php $pg_url = str_replace(array('?view=1','?view=2','?view=3','&view=1','&view=2','&view=3'),array("","","","","",""), $_SERVER['REQUEST_URI']);?>
        <div class="row">
          <div class="col-sm-4">
            <div class="cxm-view-icon text-center text-sm-left mb-2 mb-sm-0">
              <a onclick="$('#view_field').val(1);" class="{{(($view == 1) ? 'active' : '')}}" href="<?=$pg_url;?><?=((stripos($pg_url,'?') === false) ? '?' : '&');?>view=1"><span class="fa fa-th-large"></span>
              </a>
              <a onclick="$('#view_field').val(2);" class="{{(($view == 2) ? 'active' : '')}}" href="<?=$pg_url;?><?=((stripos($pg_url,'?') === false) ? '?' : '&');?>view=2"><span class="fa fa-list"></span>
              </a>
              <a onclick="$('#view_field').val(3);" class="{{(($view == 3) ? 'active' : '')}}" href="<?=$pg_url;?><?=((stripos($pg_url,'?') === false) ? '?' : '&');?>view=3"><span class="fa fa-th"></span>
              </a>
              <!--<a href="#"><span class="fa fa-list-ul"></span></a>-->
            </div>
          </div>
          <div class="col-sm-8">
            <div class="cxm-sort text-center text-sm-right">
              Sort By:
              <select onchange="updateQueryStringParameter(document.URL,'sort_by',this.value)" class="form-control form-control-sm w-auto d-inline-block rounded-0" id="sort_select">
                <option value="price-desc">Price - Highest</option>
                <option value="price-asc">Price - Lowest</option>
                <option value="miles-desc">Miles - Highest</option>
                <option value="miles-asc">Miles - Lowest</option>
                <option value="year-desc">Year - Highest</option>
                <option value="year-asc">Year - Lowest</option>
              </select>
              Per Page:
              <select id="per_page" onchange="updateQueryStringParameter(document.URL ,'per_page',this.value);" class="form-control form-control-sm w-auto d-inline-block rounded-0">
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="40">40</option>
                <option value="50">50</option>
              </select>
              Page {{ isset($_GET['page']) ? $_GET['page'] : 1 }}  of {{$all_cars->lastPage()}}
            </div>
          </div>
        </div>
        <hr>
        <div class="card bg-light">
          <div class="card-body p-2 text-center ">
            @if(number_format($all_cars->total()) == 0)
              <div style="width:30%;margin: 0 auto;" class="fs18"><span class="fa24">Result not found:</span> Sorry, but nothing matched your search term please try again with different keywords</div>
            @else
              <span class="font-weight-bold fs18">Found:</span> {{number_format($all_cars->total())}} records
            @endif
          </div>
        </div>
        <hr>
        <!-- TEST WORK -->
            <?php
            if($all_cars){
            $email_address = "syedshaharif@gmail.com";
            $numOfCols = 2;
            $rowCount = 0;
            $bootstrapColWidth = 12 / $numOfCols;
            if($view ==1){
                echo '<div class="cxm-pro-list">';
                echo'<div class="row no-gutters">';
            }
            if($view ==3){
                echo '<div class="cxm-pro-grid">';
                echo'<div class="row">';
            }
            foreach ($all_cars as $i => $car) { ?>

            <?php if($view ==3){?>

        <div class="col-md-6 col-lg-4">
          <div id="cxm-slider-pro-<?php echo $i; ?>" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <?php
                $car_images = $car->media->photo_links;
                if($i >= 4){$active = 1;}else {$active = $i;}
                if($car_images){
                foreach($car_images as $pimg => $car_image ) {
                if($pimg == 4)
                    break;
                ?>
              <div class="carousel-item<?php echo (($pimg == 0)? ' active' :''); ?>">
                <div class="cxm-img img-fixed">
                  <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                  <a href="{{url('car/detail').'/'.$car->id}}"><img class="img-fluid" onerror="this.onerror=null;this.src='{{asset('public/images/no-image.jpeg')}}';" src="{{$car_image}}"></a>
                </div>
              </div>
                <?php } } else { ?>
              <div class="carousel-item active">
                <div class="cxm-img img-fixed">
                  <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                  <a href="{{url('car/detail').'/'.$car->id}}"><img class="img-fluid" src="{{asset('public/images/no-image.jpeg')}}"></a>
                </div>
              </div>
                <?php } ?>
            </div>
            <a class="carousel-control-prev" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon fa fa-angle-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="next">
              <span class="carousel-control-next-icon fa fa-angle-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
          <div class="cxm-content">
            <div class="form-row text-muted">
              <div class="col-6">{{isset($car->build->year)?$car->build->year:''}}   {{(isset($car->build->make) ? $car->build->make : '' )}}</div>
              <div class="col-6 text-right">{{isset($car->miles) ? $car->miles:'--'}} MILES</div>
            </div>
            <div class="form-row text-muted">
              <div class="col-6 lh14"><span class="text-dark">{{isset($car->build->model)?$car->build->model:''}}</span><br>{{isset($car->inventory_type)?$car->inventory_type:''}}</div>

              <div class="col-6 text-right"><div class="price">$ {{isset($car->price) ? number_format((float)$car->price) : '--'}}</div></div>
            </div>
            <hr class="hr1">
            <div class="text-muted fs12"><span class="text-dark fs14">EXT.COLOR</span> &nbsp; {{isset($car->exterior_color)?$car->exterior_color:''}}</div>
            <div class="text-muted fs12"><span class="text-dark fs14">INT.COLOR</span> &nbsp; {{isset($car->interior_color)?$car->interior_color:''}}</div>
            <hr class="hr1">
            <div class="text-dark fs14">More Features</div>
            <div class="text-muted fs12">{{isset($car->build->year)?$car->build->year:''}} {{(isset($car->build->model) ? $car->build->model : '' )}} {{isset($car->build->engine)?$car->build->engine :''}}</div>
          <!--<div class="row mt-2">
			  <div class="col-12 mb-1"><a class="btn btn-primary btn-block" href="{{url('car/detail').'/'.$car->id}}"><span class="fa fa-eye"></span> SEE MORE DETAIL</a></div>
                </div>-->

            <div class="row mt-2">
              <div class="col-12 mb-1"><a class="btn btn-primary btn-block" href="{{route('CarCompareInsert',['car_id'=> $car->id])}}"><span class="fa fa-clone"></span> Compare</a></div>
            </div>
            <div class="row mt-2">
              <div class="col-12 mb-1"><a class="btn btn-primary btn-block" href="{{route('CarRating',['car_id'=> $car->id])}}"><span class="fa fa-line-chart"></span> Rating</a></div>
            </div>
            <div class="row mt-2">
              <div class="col-12 mb-1"><a class="btn btn-primary btn-block" href="#"><span class="fa fa-expand"></span> Dealer</a></div>
            </div>
            <div class="row mt-2">
              <div class="col-12 mb-1"><a class="btn btn-primary btn-block" href="{{route('CarHistorySave',['car_id'=> $car->id])}}"><span class="fa fa-history"></span> History</a></div>
            </div>




          </div>
        </div>

            <?php } ?>


            <?php if($view ==1){?>

        <div class="col-md-<?php echo $bootstrapColWidth; ?> box pull-right">
          <div class="cxm-detail p-3 cxm-detail-border">
            <div class="row no-gutters">
              <div class="col-sm-5">
                <div class="cxm-img">
                  <div id="cxm-slider-pro-<?php echo $i; ?>" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php
                        $car_images = $car->media->photo_links;
                        if($i >= 4){$active = 1;}else {$active = $i;}
                        if($car_images){
                        foreach($car_images as $pimg => $car_image ) {
                        if($pimg == 4)
                            break;
                        ?>
                      <div class="carousel-item<?php echo (($pimg == 0)? ' active' :''); ?>">
                        <div class="cxm-img">
                          <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                          <a href="{{url('car/detail').'/'.$car->id}}"><img class="img-fluid" onerror="this.onerror=null;this.src='{{asset('public/images/no-image.jpeg')}}';" src="{{$car_image}}"></a>
                        </div>
                      </div>
                        <?php } } else { ?>
                      <div class="carousel-item active">
                        <div class="cxm-img">
                          <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                          <a href="{{url('car/detail').'/'.$car->id}}"><img class="img-fluid" src="{{asset('public/images/no-image.jpeg')}}"></a>
                        </div>
                      </div>
                        <?php } ?>
                    </div>

                    <a class="carousel-control-prev" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon fa fa-angle-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="next">
                      <span class="carousel-control-next-icon fa fa-angle-right" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-sm-7">
                <div class="cxm-content ">
                  <h2><a href="{{url('car/detail').'/'.$car->id}}">@if(!empty($car->heading) > 0){{mb_strimwidth($car->heading,0,25,"...")}}@endif</a></h2>
                  @if(!empty($car->price) > 0)
                    <div class="fs24 fc1">$ {{isset($car->price) ? number_format((float)$car->price) : '--'}}</div>
                  @endif
                  <div class="cxm-features">{{isset($car->build->year)?$car->build->year:''}} {{(isset($car->build->model) ? $car->build->model : '' )}} {{isset($car->build->engine)?$car->build->engine :''}}</div>
                  <p>Make: {{(isset($car->build->make) ? $car->build->make : '-') }}</p>
                  <p>Model: {{(isset($car->build->model) ? $car->build->model : '' )}}</p>
                  <p>Trim: {{isset($car->build->trim) ? $car->build->trim : ' --' }}</p>
                </div>
              </div>

              <div class="col-sm-12 col-md-12 col-xs-6">
                @auth
                  <?php $tot = DB::table('client_favourite')->where('client_id',Auth::id())->where('car_id',$car->id)->count();?>
                @if($tot==0)
                      <?php
                      $save_car= array(
                          'id' => $car->id,
                          'title' => $car->heading,
                          'car_type' => $car->inventory_type,
                          'price' => (isset($car->price) ? $car->price : '--'),
                          'miles' => (isset($car->miles) ? $car->miles : '--'),
                          'build' => $car->build,
                          'images' => $car_images
                      );
                      ?>
                  <hr>
                  <input type="hidden" id="save_car_id_{{$i}}" value="{{$car->id}}">
                  <input type="hidden" id="save_car_{{$i}}" value="{{base64_encode(json_encode($save_car))}}">
                  <a class="btn btn-primary btn-sm" href="javascript:save_car({{$i}});"><span id="save_car_span_{{$i}}" class="fa fa-save"></span> <span id="jq_save_tag_{{$i}}">Save</span></a>
                @else
                  <a class="btn btn-primary btn-sm" href="javascript:void(0);">
                    <span class="fa fa-check"></span><span>Saved</span></a>
                @endif
                @endauth
                @guest
                <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#save_search_model"  href="javascript:void(0);"><span class="fa fa-save"></span> Save</a>
                @endguest

                <a href="{{route('CarCompareInsert',['car_id'=> $car->id])}}"><button type="button" class="btn btn-primary btn-sm"><span
                            class="fa fa-clone"></span> Compare</button></a>
                <a href="{{route('CarRating',['car_id'=> $car->id])}}">
                  <button type="button" class="btn btn-primary btn-sm"><span class="fa fa-line-chart"></span> Rating</button>
                </a>
                <a href="{{route('CarHistorySave',['car_id'=> $car->id])}}"><button type="button" class="btn btn-primary btn-sm"><span class="fa fa-history"></span> History</button></a>
              </div>
            </div>
            <br>
          </div>
        </div>

            <?php	} ?>
            <?php if($view ==2){?>
        <div class="cxm-pro-list cxm-detail-border">
          <div class="row no-gutters">
            <div class="col-sm-9">
              <div class="cxm-detail p-3">
                <div class="row no-gutters">
                  <div class="col-sm-4">
                    <div id="cxm-slider-pro-<?php echo $i; ?>" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner">
                          <?php
                          $car_images = $car->media->photo_links;
                          if($i >= 4){$active = 1;}else {$active = $i;}
                          if($car_images){
                          foreach($car_images as $pimg => $car_image ) {
                          if($pimg == 4)
                              break;
                          ?>
                        <div class="carousel-item<?php echo (($pimg == 0)? ' active' :''); ?>">
                          <div class="cxm-img img-fixed2">
                            <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                            <a href="{{url('car/detail').'/'.$car->id}}"><img onerror="this.onerror=null;this.src='{{asset('public/images/no-image.jpeg')}}';" class="img-fluid" src="{{$car_image}}"></a>
                          </div>
                        </div>
                          <?php } } else { ?>
                        <div class="carousel-item active">
                          <div class="cxm-img img-fixed2">
                            <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                            <a href="{{url('car/detail').'/'.$car->id}}"><img class="img-fluid" src="{{asset('public/images/no-image.jpeg')}}"></a>
                          </div>
                        </div>
                          <?php } ?>
                      </div>

                      <a class="carousel-control-prev" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon fa fa-angle-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="next">
                        <span class="carousel-control-next-icon fa fa-angle-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
                  </div>
                  <div class="col-sm-8">
                    <div class="cxm-content">
                      <h2><a href="{{url('car/detail').'/'.$car->id}}">@if(!empty($car->heading) > 0){{$car->heading}}@endif</a></h2>
                      <div class="form-row">
                        <div class="col-md-4">
                          @if(!empty($car->price) > 0)
                            <div class="fs24 fc1">$ {{isset($car->price) ? number_format((float)$car->price) : '--'}}</div>
                          @endif
                          <div class="text-muted">{{isset($car->miles) ? $car->miles:'--'}} MILES</div>
                          <div><span class="fa fa-square fs18" {{isset($car->exterior_color) ? 'style="color:'.$car->exterior_color.' ;"' :''}}></span> Color</div>
                          <div class="cxm-features">{{isset($car->build->year)?$car->build->year:''}} {{(isset($car->build->model) ? $car->build->model : '' )}} {{isset($car->build->engine)?$car->build->engine :''}}
                          </div>
                          <div class="text-muted">{{ucfirst($car->inventory_type)}}</div>
                        </div>
                        <div class="col-md-8">
                          <div class="fc1">Description:</div>
                          <p>Make: {{(isset($car->build->make) ? $car->build->make : '-') }}</p>
                          <p>Model: {{(isset($car->build->model) ? $car->build->model : '' )}}</p>
                          <p>Trim: {{isset($car->build->trim) ? $car->build->trim : ' --' }}</p>
                          <p>Body Type: {{isset($car->build->body_type) ? $car->build->body_type : ' --'}}</p>
                          <p>Vehicle Type: {{isset($car->build->vehicle_type) ? $car->build->vehicle_type : '--'}}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="pt-3">
                  <span class="fa fa-video-camera"></span> 0 <span class="fa fa-camera ml-4"></span> {{count($car_images)}} <span class="ml-4">VIN: {{$car->vin}}</span>
                </div>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="cxm-actions">
                <div class="row">
                  <div class="col-12 mb-2">
                    @auth
                      <?php $tot = DB::table('client_favourite')->where('client_id',Auth::id())->where('car_id',$car->id)->count();?>
                    @if($tot==0)
                          <?php
                          $save_car= array(
                              'id' => $car->id,
                              'title' => $car->heading,
                              'car_type' => $car->inventory_type,
                              'price' => (isset($car->price) ? $car->price : '--'),
                              'miles' => (isset($car->miles) ? $car->miles : '--'),
                              'build' => $car->build,
                              'images' => $car_images
                          );?>
                      <input type="hidden" id="save_car_id_{{$i}}" value="{{$car->id}}">
                      <input type="hidden" id="save_car_{{$i}}" value="{{base64_encode(json_encode($save_car))}}">
                      <a class="btn btn-primary btn-block" href="javascript:save_car({{$i}});"><span id="save_car_span_{{$i}}" class="fa fa-save"></span> <span id="jq_save_tag_{{$i}}">Save</span></a>
                    @else
                      <a class="btn btn-primary btn-block" href="javascript:void(0);">
                        <span class="fa fa-check"></span>
                        <span>Saved</span>
                      </a>
                    @endif
                    @endauth
                    @guest
                    <a class="btn btn-primary btn-block" data-toggle="modal" data-target="#save_search_model"  href="javascript:void(0);"><span class="fa fa-save"></span> Save</a>
                    @endguest
                  </div>
                </div>
                <div class="row">
                  <div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="{{route('CarCompareInsert',['car_id'=> $car->id])}}"><span class="fa fa-clone"></span> Compare</a></div>
                </div>
                <div class="row">
                  <div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="{{route('CarRating',['car_id'=> $car->id])}}"><span class="fa fa-line-chart"></span> Rating</a></div>
                </div>
                <div class="row">
                  <div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="#"><span class="fa fa-expand"></span> Dealer</a></div>
                </div>
                <div class="row">
                  <div class="col-12"><a class="btn btn-primary btn-block" href="{{route('CarHistorySave',['car_id'=> $car->id])}}"><span class="fa fa-history"></span> History</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>

            <?php } ?>


            <?php
            $dealerRecord = $dealer_model::findOrNew($car->dealer->id);
            $dealerRecord->dealer_id = $car->dealer->id;
            $dealerRecord->dealer_name = $car->dealer->name;
            $dealerRecord->dealer_latitude = (isset($car->dealer->latitude) ? $car->dealer->latitude : '');
            $dealerRecord->dealer_longitude = (isset($car->dealer->longitude) ? $car->dealer->longitude : '');
            $dealerRecord->dealer_phone = (isset($car->dealer->phone) ? $car->dealer->phone : '');
            $dealerRecord->dealer_country = (isset($car->dealer->country) ? $car->dealer->country : '');
            $dealerRecord->dealer_website = (isset($car->dealer->website) ? $car->dealer->website : '');
            $dealerRecord->dealer_email = $email_address;
            $dealerRecord->dealer_street = (isset($car->dealer->street) ? $car->dealer->street : '');
            $dealerRecord->dealer_city = (isset($car->dealer->city) ? $car->dealer->city : '');
            $dealerRecord->dealer_zip = (isset($car->dealer->zip) ? $car->dealer->zip : '');
            $dealerRecord->save();

            // inserting to cron email //
            $dealerCron = $dealer_cron::firstOrCreate([
                    'dealer_id' => $car->dealer->id,
                    'dealer_name' => $car->dealer->name,
                    'dealer_email' => $email_address,
                    'date' => date("Y-m-d")
                ]
            );

            $dealer_cron->where('id',$dealerCron->id)
                ->update(['total_in_search' => $dealerCron->total_in_search+1]);
            }
            if($view ==1 || $view ==3 ){
                echo '</div></div>';
            } } ?>
      <!-- TEST WORK -->

        <div class="container">
          <div class="row">
            <div class="col-9">
              <ul class="pagination justify-content-center">
                  <?php
                  $links = $all_cars->render();
                  $links = str_replace("<a", "<a class='page-link ' ", $links);
                  $links = str_replace("<li", "<li class='page-item' ", $links);
                  $links = str_replace("<span", "<span class='page-link'",$links);
                  echo $links;
                  ?>
              </ul>
            </div>
            <div class="col-3">
              <a class="btn btn-primary btn-block" href="{{route('CarComparePage')}}">Compare Car</a>
            </div>
          </div>
        </div>
      @else
        <div>
          <div class="card bg-light">
            <div class="card-body p-2 text-center ">
              <div style="width:30%;margin: 0 auto;" class="fs18">
                Search your desired car by entering the parameters.
              </div>
            </div>
          </div>
        </div>
      @endif
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="save_search_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Save Search</h5>
        </div>
        <div class="modal-body">
          @auth
          <form action="{{route('save-my-search')}}" method="post" id="js-saveQuery_form">
            <div class="row">
              {{csrf_field()}}
              <div class="col-sm-12 gutter-bottom">
                <div class="input-group">
                  <input type="text" class="form-control" name="search_name" id="search_name" placeholder="Search Name">
                </div>
              </div>
            </div>
            <input name="search_query" type="hidden" value="{{$_SERVER['QUERY_STRING']}}" />
          </form>
          @endauth
          @guest
          <p>Please login to the system in order to save this.</p>
          @endguest
        </div>
        <div class="modal-footer">
          @auth
          <button type="button" class="btn btn-primary" onclick="submitQueryStr ();">Save</button>
          @endauth
          &nbsp;
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">

      function load_param(type){
          var data = "_token={{csrf_token()}}";
          data += "&type="+type;
          alert(data);

          $.ajax({
              url: "{{url('/load/param')}}",
              type:'POST',
              data: data,
              beforeSend: function(){

              },
              success: function() {
              },
          });
      }

      function save_car(numb){
          var data = "_token={{csrf_token()}}";
          data += "&car_post_data="+$("#save_car_"+numb).val();
          data += "&car_post_id="+$("#save_car_id_"+numb).val();

          $.ajax({
              url: "{{url('/save/car')}}",
              type:'POST',
              data: data,
              beforeSend: function(){
                  $("#save_car_span_"+numb).removeClass("fa-save");
                  $("#jq_save_tag_"+numb).html(" Saving...");
                  $("#save_car_span_"+numb).addClass("fa-refresh fa-spin");
              },
              success: function() {
                  $("#save_car_span_"+numb).removeClass("fa-refresh fa-spin");
                  $("#save_car_span_"+numb).addClass("fa-check");
                  $("#jq_save_tag_"+numb).html("Saved");
                  $("#save_car_span_"+numb).parent().attr("href","javascript:void(0)");
              },
          });
      }
      function submitQueryStr (){
          if ($.trim($('#search_name').val ()).length == 0)
              alert ('Please write the search query name first');

          else
              $('#js-saveQuery_form').submit();
      }
  </script>
@endsection
@section('searchPageScript')
  <script src="{{asset('js/bootstrap-tagsinput.js')}}"></script>
  <script type="text/javascript">
      function updateQueryStringParameter(uri,key,value){

          if(key == 'sort_by'){

              uri = window.location.href.split('?')[0];

              var pairs = window.location.search.substring(1).split("&"),
                  obj="";
              pair="";
              i="";
              for ( i in pairs ) {
                  console.log(pairs[i]);
                  pair = pairs[i].split("=");
                  if ( pair[0] != "sort_by" && pair[0] != "sort_order" && pair[0] != "" ){
                      obj += "&"+pairs[i];
                  }
              }
              obj = obj.substr(1);
              sortArr = value.split('-');
              sortBy=sortArr[0];
              sortOrder=sortArr[1];

              uri =uri+"?"+obj+'&sort_by='+sortBy+'&sort_order='+sortOrder;
          }
          else{
              $("#per_page_field").val(value);

              return false;

              var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
              var separator = uri.indexOf('?') !== -1 ? "&" : "?";
              if (uri.match(re)) {
                  uri = uri.replace(re, '$1' + key + "=" + value + '$2');
              }
              else {
                  uri = uri + separator + key + "=" + value;
              }
          }
          window.location =uri;
      }

      function update_tag(main_select ,sub_select,pre_value=""){
          tag_val1="";

          if(pre_value == ""){
              if(main_select == "Year")
                  $("#search_"+main_select).val($("#search_"+main_select).val()+","+sub_select);

              else
                  $("#search_"+main_select).val(sub_select);

              $("#search_"+main_select).attr('data-featured', $('#customCheckmh1').is(':checked'));

              $("#"+main_select+"-fea").remove();

              if($('#customCheckmh1').is(':checked'))
                  $("#searh-form").append('<input type="hidden" value="1" id="'+main_select+"-fea"+'" name="'+main_select+"-fea"+'">');

              $(".form-field").each(function(){
                  if($(this).val() != ""){

                      field = $(this).attr('name')[0].toUpperCase()+$(this).attr('name').slice(1);
                      field =field.replace("_"," ");

                      if($(this).attr('name') == 'year') {

                          y_tag= "";
                          ye_arr = $(this).val().split(",");

                          for (index = 0; index < ye_arr.length; ++index) {

                              if(ye_arr[index] != "")
                                  y_tag += ',Year : '+ye_arr[index];
                          }
                          tag_val1 += ','+y_tag;
                      }

                      else
                          tag_val1 += ','+(field+': ' + $(this).val());
                  }
              });
              tag_val1 = tag_val1.substr(1);
              $("#customCheckmh1").prop('checked',false);
              $("#main_select").val("main_category");
              $("#main_select").change();
          }
          else{
              $(".form-field").each(function(){
                  if($(this).val() != ""){

                      field = $(this).attr('name')[0].toUpperCase()+$(this).attr('name').slice(1);
                      field =field.replace("_"," ");

                      if($(this).attr('name') == 'year') {
                          y_tag= "";
                          ye_arr = $(this).val().split(",");

                          for (index = 0; index < ye_arr.length; ++index) {

                              if(ye_arr[index] != "")
                                  y_tag += ',Year : '+ye_arr[index];
                          }

                          console.log(y_tag);
                          tag_val1 += ','+y_tag;
                      }

                      else
                          tag_val1 += ','+(field+': ' + $(this).val());
                  }
              });
              tag_val1 = tag_val1.substr(1);
          }

          $("#search_tage").tagsinput('removeAll');
          $("#search_tage").tagsinput('add', tag_val1);

          setTimeout(function(){
              $('.bootstrap-tagsinput .tag').each(function(){
                  t_val = $(this).text().split(':')[0];
                  if($("#search_"+t_val).attr('data-featured') == true || $("#search_"+t_val).attr('data-featured') == 'true'){
                      $(this).css('background-color','#158cba');
                      $(this).css('color','#fff');
                  }
                  else if($('#'+t_val+'-fea').val()){
                      $(this).css('background-color','#158cba');
                      $(this).css('color','#fff');
                  }
              });
          },1000)
      }

      $(function(){



          $('#geo_ip').text($("#z_c").val());

          $("#sub_select").children('option').hide();
          $("#sub_select").children("option[option_for^="+$('#main_select').val()+"]").show();
          $("#main_select").change(function() {

              if($(this).val() == 'main_category')
                  $('#sub_select').attr('disabled',true);

              else
                  $('#sub_select').attr('disabled',false);

              $("#sub_select").children("option[option_for^="+$(this).val()+"]:first").attr('selected',false)
              $("#sub_select").children('option').hide();
              $("#sub_select").children("option[option_for^="+$(this).val()+"]").show();
              $("#sub_select").children("option[option_for^="+$(this).val()+"]:first").attr('selected', true)
          })

          $('#search_tage').on('beforeItemRemove', function(event) {
              tagVal = event.item;
              tagVal = tagVal.split(":");
              f_name = tagVal[0];
              f_name = f_name.replace(" ","_");
              $("#search_"+f_name).val("");
          });

          setTimeout(function(){ getLatLong($("#z_c").val());},3000);
          $('.chkBox').click(function() {

              if($(this).val() == "new"){
                  $("#main_select option[value='Miles']").attr('disabled',true);
                  $("#main_select option[value='Miles']").addClass('red-option');
                  $("#main_select option[value='Year']").attr('disabled',true);
                  $("#main_select option[value='Year']").addClass('red-option');
              }

              else{
                  $("#main_select option[value='Miles']").attr('disabled',false);
                  $("#main_select option[value='Miles']").removeClass('red-option');
                  $("#main_select option[value='Year']").attr('disabled',false);
                  $("#main_select option[value='Year']").removeClass('red-option');
              }

              $(".chkBox").not(this).prop('checked', false);

              if($(this).is(':checked'))
                  $("#car_type").val($(this).val());

              else
                  $(this).prop('checked', true);
          });

          $("#searh-form").submit(function() {
              $(this).find(":input").filter(function(){
                  return !this.value;
              }).attr("disabled", "disabled");
              $("#searh-form").submit;
              return true; // ensure form still submits
          });
          <?php
              if(isset($_GET['car_type'])){ ?>
              car_type="{{$_GET['car_type']}}";
          $(":checkbox[value="+car_type+"]").prop("checked","true");
          <?php } else { ?>
            $(":checkbox[value='new']").click();
          <?php } ?>
      });
      <?php if(isset($_GET['filter_search'])) { ?>
          <?php unset($_GET['filter_search']) ?>

          price = "<?=(isset($_GET['price']) ? $_GET['price'] : '');?>";
      body_type = "<?=(isset($_GET['body_type']) ? $_GET['body_type'] : '');?>";
      country = "<?=(isset($_GET['country']) ? $_GET['country'] : '');?>";
      color = "<?=(isset($_GET['color']) ? $_GET['color'] : '');?>";
      miles = "<?=(isset($_GET['miles']) ? $_GET['miles'] : '');?>";
      year = "<?=(isset($_GET['year']) ? $_GET['year'] : '');?>";
      make = "<?=(isset($_GET['make']) ? $_GET['make'] : '');?>";
      car_type = "<?=(isset($_GET['car_type']) ? $_GET['car_type'] : '');?>";
      sort_by = "<?=(isset($_GET['sort_by']) ? $_GET['sort_by'] : '');?>";
      sort_order = "<?=(isset($_GET['sort_order']) ? $_GET['sort_order'] : '');?>";
      radius = "<?=(isset($_GET['radius']) ? $_GET['radius'] : '');?>";

      $("#search_Body_type").val(body_type);
      if(radius != "")
          $("#radius_select").val(radius);

      $("#search_Price").val(price);
      $("#search_Color").val(color);
      $("#search_Country").val(country);
      $("#search_Miles").val(miles);
      $("#search_Year").val(year);
      $("#search_Make").val(make);
      $("#car_type").val(car_type);

      if(sort_by != "" && sort_order != ""){
          selectedOpt = sort_by+'-'+sort_order;
          $("#sort_select").val(selectedOpt);
      }

      update_tag(null,null,'q_string');

      <?php } ?>
          <?php if(isset($_GET['per_page'])) { ?>
          per_page = "<?=$_GET['per_page'];?>";
      $("#per_page").val(per_page);
      $("#per_page_field").val(per_page);
      <?php } ?>
          <?php if(isset($_GET['page'])) { ?>
          page = "<?=$_GET['page'];?>";
      $(".page-item").each(function(){
          if($(this).text() == page)
              $(this).addClass("active");
      });
      <?php } ?>
      <?php if(isset($_GET['view'])) { ?>
        $("#view_field").val("<?=$_GET['view'];?>");
      <?php } ?>
  </script>
@endsection