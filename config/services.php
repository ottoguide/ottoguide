<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
	
	
	
	
   'facebook' => [ //change it to any provider
        'client_id' => '392972224536732',
        'client_secret' => 'f6539b9d17e9ad4c64e6248a5ab3e7f3',
        'redirect' => 'http://dev.ottoguide.com/auth/facebook/callback',
    ],

    'twitter' => [ //change it to any provider
        'client_id' => '9N2mj0QGiqAksD8LiVRVFpQHN',
        'client_secret' => 'slH3yoKuLvQJHBcTgrm0aDaHvNW2ZoWpMOTOkuDSylqBMcCsWI',
        'redirect' => 'http://dev.ottoguide.com/auth/twitter/callback',
    ],

    'google' => [ //change it to any provider
        'client_id' => '301856430027-jvkv3qjne34v2qsoi36uvtt5suvd84q6.apps.googleusercontent.com',
        'client_secret' => 'OUalkmhEX5wrh-8kmd9KpI9-',
        'redirect' => 'http://dev.ottoguide.com/auth/google/callback',
    ],
	
	
   'twilio' => [
        'accountSid' => env('TWILIO_ACCOUNT_SID'),
        'apiKey' => env('TWILIO_API_KEY'),
        'apiSecret' => env('TWILIO_API_SECRET'),
        'ipmServiceSid' => env('TWILIO_IPM_SERVICE_SID'),
    ],



];
