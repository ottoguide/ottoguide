<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TwilioChat extends Model{	
	protected $table = '`og_twilio_chat';
	public $timestamps = true;
	protected $fillable = ['dealer_id','user_id','chat_channel','notify'];   
}
