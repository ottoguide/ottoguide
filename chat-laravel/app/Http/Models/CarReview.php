<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CarReview extends Model{	

	protected $table = 'client_reviews';
	public $timestamps = true;
	protected $fillable = ['review_id','client_id','dealer_id','message','message_by'];   
}
