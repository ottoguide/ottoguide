<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SubscriberActivity extends Model{	
	protected $table = 'subscriber_activity';
	public $timestamps = true;
	protected $fillable = ['dealer_id','package_id','featured_ad','chats','lease_purchase'];   
}
