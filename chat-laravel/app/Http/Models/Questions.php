<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Questions extends Model{	
	protected $table = 'quiz';
	//public $timestamps = false;
	protected $fillable = ['title','description','is_active'];
}
