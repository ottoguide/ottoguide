<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class DealerCron extends Model{	

	protected $table = 'dealer_email_cron';
	public $timestamps = true;
	protected $fillable = ['dealer_id','dealer_name', 'dealer_email','car_id', 'search_type' ,'total_visitor','date'];   
}
