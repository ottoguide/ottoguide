<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CarRating extends Model{	

	protected $table = 'car_rating';
	public $timestamps = true;
	protected $fillable = ['car_id','client_id','rating','review_tile','review_description'];   
}
