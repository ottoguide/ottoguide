<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Save_search extends Model
{
	protected $table = 'client_save_search';
	
	public $timestamps = true;
	
	protected $fillable = ['json_data','save_search_name'];
}
