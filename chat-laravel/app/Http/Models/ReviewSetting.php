<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ReviewSetting extends Model{	

	protected $table = 'settings';
	public $timestamps = true;
	protected $fillable = ['car_id','client_id','dealer_id','reviews_off','client_block','dealer_block'];   
}
