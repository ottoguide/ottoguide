<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SearchCarViews extends Model{	

	protected $table = 'dealer_car_search_views';
	public $timestamps = true;
	protected $fillable = ['car_id','dealer_id'];   
}
