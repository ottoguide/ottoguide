<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CarHistory extends Model{	

	protected $table = 'car_history';
	public $timestamps = true;
	protected $fillable = ['car_id','client_id'];   
}
