@extends('layouts.login')

@section('content')

  <div class="login-box">
      <div class="login-logo">
       <a href="{{route('home')}}"><b>RO</b>PLANT</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p>Enter your email to reset password</p>
        <form method="POST" action="{{ route('password.email') }}" id="login-form">     
        {{ csrf_field() }}   
          <div class="form-group has-feedback">
             <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>  
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif          
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>         
            
          <div class="row">                        
            <div class="text-center col-md-12">
               <button type="submit" class="btn btn-primary">Send Password Reset Link</button>
            </div><!-- /.col -->
          </div>
        </form>      
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
@endsection
