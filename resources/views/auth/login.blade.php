@extends('layouts.web_pages')
@section('content')
<script type="text/javascript">
  var register_url = "{{ route('register') }}";
  //var login_url = "{{ route('login') }}";
</script>

<!--{{ route('login') }}-->

  <div class="header-margin py-5">
  
    <div class="container">
      <div class="row">                    
        <div class="col-lg-8 offset-lg-2">
		
		<br>
					   <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
		
          <div class="card">
		  
            <div class="card-header font-weight-bold fs18">Sign up or Sign in to Otto Guide Web Portal</div>
              <div class="card-body">
                <form method="POST" id="register-login-form">     
                {{ csrf_field() }}  
                <div class="row">
                  <div class="col-sm-12 border-right">
                   <label class="radio-inline">
                      &nbsp;<input type="radio" onclick="$('.login_fields').show(); $('.log_out_fields').hide(); $('#register-login-form').attr('action',login_url);" name="sinin" checked>Sign in
                    </label>&nbsp;&nbsp;&nbsp;
                    <label class="radio-inline">
                      &nbsp;<input type="radio" onclick="$('.log_out_fields').show(); $('.login_fields').hide();  $('#register-login-form').attr('action',register_url); " name="sinin">Sign Up
                    </label><br>   

					<div class="col-md-9" style="padding-left:0px;">
                    <label class="login_fields" for="email">Email</label>
                    <div class="login_fields input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><span class="fa fa-envelope-o"></span></span>
                      </div>
                      <input type="text" class="form-control check_email"  name="email" placeholder="Email">
                    </div>  
					</div>
					
					<div class="col-md-9" style="padding-left:0px;">
                    <label class="login_fields" for="password">Password</label>
                    <div class="login_fields input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><span class="fa fa-lock"></span></span>
                      </div>
                      <input type="password" name="password" class="form-control" autocomplete="off" placeholder="Password">
                    </div>
					</div>
					
					
                    <label style="display: none;" for="password">Mobile No</label>
                    <div style="display: none;" class=" input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><span class="fa fa-phone"></span></span>
                      </div>
                      <input type="number" name="phone" class="form-control" placeholder="12345678">
                    </div>    
                    <div style="display: none;" class="row mb-3">
                      <div class="text-center col-sm-12">                        
                        <button type="submit" class="btn btn-primary"><span class="fa fa-sign-in"></span> Sign Up</button>
                      </div>                      
                    </div>

                    <div class="row mb-3 login_fields">
                      <div class="col-sm-6">  
                        <a class="btn btn-primary manually-login" style="color:#fff;"><span class="fa fa-sign-in"></span> Login</a>
						<button type="reset" form="register-login-form" class="btn btn-secondary"> Reset </button>
                      </div>
                      <div class="col-sm-6">
                        <div class="custom-control custom-checkbox mt-2 fs14">
                          <input type="checkbox" class="custom-control-input" id="customChecklogin">
                          <label class="custom-control-label" for="customChecklogin">Remember me</label>
                        </div>
                      </div>
                    </div>
                    @if ($errors->has('email'))
                      <span class="help-block alert-danger">
                        <center><strong>{{ $errors->first('email') }}</strong></center>
                      </span>
                    @endif
                    @if ($errors->has('phone'))
                      <span class="help-block alert-danger">
                        <center><strong>{{ $errors->first('phone') }}</strong></center>
                      </span>
                    @endif
                  </form>

                  <form method="POST" style="display: none;" action="{{route('register')}}" class="log_out_fields" id="main-register-form">
                    {{ csrf_field() }}
					
					<div class="row">
					
					<div class="col-md-6 col-xs-12">
                      <label for="email">First Name</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                          <span class="fa fa-user-o"></span></span>
                        </div>
                        <input type="text" name="name" class="form-control" placeholder="First Name">
                      </div>
					  </div>
					  
					  
					  <div class="col-md-6 col-xs-12">
                      <label for="email">Last Name</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                          <span class="fa fa-user-o"></span></span>
                        </div>
                        <input type="text" name="lastname" class="form-control" placeholder="Last Name">
                      </div>
					  </div>
					  
					  
					<div class="col-md-6 col-xs-12">
                      <label for="email">Email</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                          <span class="fa fa-envelope-o"></span></span>
                        </div>
                        <input type="text" name="email" class="form-control" placeholder="Email">
                      </div>
					  </div>
					  
					 <div class="col-md-6 col-xs-12">
                      <label for="mobile_no">Mobile No</label>
                      <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                            <span class="fa fa-phone"></span></span>
                          </div>
                          <input type="number" name="phone" class="form-control" placeholder="###-###-####">
                      </div>
					  </div>
					  
					  
					  <div class="col-md-6 col-xs-12">
                      <label for="password">Password</label>
                      <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                            <span class="fa fa-lock"></span></span>
                          </div>
                         <input type="password" class="form-control" name="password" placeholder="Password">
                      </div>
					  </div>
					  
					  <div class="col-md-6 col-xs-12">
                      <label for="password-confirm">Confirm Password</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <span class="fa fa-lock"></span>
                          </span>
                        </div>
                        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
                      </div>
					  </div>
					  
					
					  
					  </div>
                  </form>
                  <div class="alert alert-danger validate-error-msg" style="font-size:12px; display:none">
                    <ul></ul>
                  </div>
                  <div class="row mb-12" style="display: none;" id="main-register-validate-loader">
                    <span style="margin:0 auto; color: #bf2f2f;" class="fa fa-spinner fa-spin"></span>
                  </div>
                  <div class="row mb-3 log_out_fields" style="display: none;" >
				  
				    <div class="col-sm-6" >
                      <input name="agreement" class="agreement form_controll" type="checkbox">  I accept the <a href="#">terms & conditions </a>. 
                      </div>
				  
                      <div class="col-sm-6">
                        <button data-form-id="main-register-form"
                          data-validate-url="/register/validate" data-validate-loader="main-register-validate-loader" type="button"  class="btn-form-submit btn btn-primary rounded-0">
                      <span class="fa fa-sign-in"></span>
                        Sign Up
                      </button>
					  
					  <button type="reset" class="btn btn-secondary" form="main-register-form">Reset</button>
					  
					  
                    </div>
                  </div>
                  <a class="fs12 login_fields" href="{{url('/user/forgot/password/')}}">Forgotten your password?</a>
                  </div> 

                </div>  
				
                <div class="border-top pt-3 mt-4 text-center text-dark fs14"><span class="response"style="color:#cc0000; font-weight:bold; font-size:18px;"></span><br><span class="fa fa-lock fc1 fs24"></span> We'll always protect your OTTO Guide account and keep your details safe.</div>
							
						<br>
					  <div class="message" width="50%" align="center">
                    @if (session('status'))
                        <div class="alert alert-success" width="50%">
                            {{ session('status') }}
                        </div>
                    @endif
		      </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	
	
	
	
	
	
	
	<script>
		$(document).ready(function () {
			
		$('.btn-form-submit').attr('disabled',true);
		
		var ckbox = $('.agreement ');
		
		$('.agreement').on('click',function () {
			if (ckbox.is(':checked')) {
				  $('.btn-form-submit').attr('disabled',false);
			} else {
				  event.stopPropagation();
				  $('.btn-form-submit').attr('disabled',true);
			}
		});
	});
	</script>
	
	
	
	
<script>

	$( document ).ready(function() {

	 $('.manually-login').click(function(){

	 var email = $('.check_email').val();
	
			$.ajax({
				type:'POST',
				data: {
				"_token": "{{ csrf_token() }}",	
				 check_email: email
				},
				url:'{{url('user-check-activation.html')}}',
			
				success:  function (response) {
				
				if(response == 'Not-Activated'){
				$('.response').html('Your account is not activated');
				exit;
				}
				
				else{
				
				$("#register-login-form").submit();
			  
				 }
				
				}
				
				}); 
		


			});	
				
		});		





</script>	
	
	
	
	
@endsection