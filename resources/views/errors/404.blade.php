@extends('layouts.web_pages')
@section('content')
    <style>
@import url(https://fonts.googleapis.com/css?family=Raleway:300,700);
#not-found{
text-align:center;	
}
.header-marginbg-secondary{
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#eeeeee+0,ffffff+100 */
background: rgb(238,238,238); /* Old browsers */
background: -moz-linear-gradient(top, rgba(238,238,238,1) 0%, rgba(255,255,255,1) 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, rgba(238,238,238,1) 0%,rgba(255,255,255,1) 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, rgba(238,238,238,1) 0%,rgba(255,255,255,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */	
}

#title{
color:#117298;
font-size: 150px;
font-weight: bold;
text-align:center;
}
.circles p{
font-size: 85px;	
font-weight: bold;	
color:#222;
}
.circles p small{
font-size: 20px;		
color:#222;
font-family: Raleway;
font-weight: 400;
}
</style>
  <div class="header-marginbg-secondary">
 	<div class="header-margin py-5">
        <div class="container">
       <div class="row">
       <div class="col-sm-12">
		
  <section id="not-found">
    <div id="title">404</div>
    <div class="circles">
      <p>
       <small>Sorry! The page you're looking <br> for cannot be found.</small>
      </p>
	
   
    </div>
	<br>
	<!--<img align ="center" width="500" src="{{asset('images/404car.png')}}">-->
  </section>
  <center><a href="{{url('/')}}" class="btn btn-primary btn-lg "><span class="fa fa-reply"></span> Back to Home</a></center> 
      </div>      
    </div>
     </div>
	</div>
</div>