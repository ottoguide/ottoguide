@extends('layouts.web_pages')
@section('header')
    @parent

    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
@endsection
@section('content')
    <div class="header-margin py-5">
        <div class="container">
            @include('dealer.includes.nav', array(
                'tab' => 'market-analysis',
				'page_title' => 'Market Analysis'
            ))

            <div class="row bg-secondary">
                <div class="col-md-12">

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link tab_in_demand_inventory_btn active" data-toggle="tab"
                               href="#tab_in_demand_inventory" role="tab" aria-selected="true">In Demand Inventory</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link tab_trending_local_searches_btn" data-toggle="tab"
                               href="#tab_trending_local_searches" role="tab" aria-selected="false">Trending Local Searches</a>
                        </li>
                    </ul>
                    <div class="tab-content card" id="myTabContent">
                        <div class="card-body tab-pane fade show active" id="tab_in_demand_inventory" role="tabpanel" aria-labelledby="tab_in_demand_inventory">
                            {{--<table class="table">
                                <tr>
                                    <th colspan="2">In-Demand Models in Your Area</th>
                                    <th>Searches per inventory Piece</th>
                                    <th>Local Searches</th>
                                    <th>Local Inventory</th>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="" class="form-control">
                                            <option value="">- Select make -</option>

                                        </select>
                                    </td>
                                    <td>
                                        <select name="" class="form-control">
                                            <option value="">- Select model -</option>

                                        </select>
                                    </td>
                                    <td>17</td>
                                    <td>939</td>
                                    <td>53</td>
                                </tr>
                                <tr>
                                    <td colspan="5"><button type="button" class="btn btn-primary">Search</button></td>
                                </tr>
                            </table>--}}

                            <div class="alert alert-info">
                                Searches per Inventory Piece represents the ration of local searches to available inventory, A higher number represents a high demand, low inventory model. Think about grabbing one of these at auction or on trade-in to take advantage of heavy demand!
                            </div>

                            <h4>Top In-Demand Models in Your Area</h4>

                            <table width="100%" id="data-table-in-demand" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="4%">#</th>
                                    <th>Model</th>
                                    <th>Searches per inventory Piece (last month)</th>
                                    <th>Local Searches</th>
                                    <th>Local Inventory</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                        <div class="card-body tab-pane fade" id="tab_trending_local_searches" role="tabpanel" aria-labelledby="tab_trending_local_searches">

                            {{--<table class="table">
                                <tr>
                                    <th colspan="2">Search by Model in Your Area</th>
                                    <th>Searches per inventory Piece</th>
                                    <th>Local Searches</th>
                                    <th>Local Inventory</th>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="" class="form-control">
                                            <option value="">- Select make -</option>
                                            
                                        </select>
                                    </td>
                                    <td>
                                        <select name="" class="form-control">
                                            <option value="">- Select model -</option>
                                            
                                        </select>
                                    </td>
                                    <td>17</td>
                                    <td>939</td>
                                    <td>53</td>
                                </tr>
                                <tr>
                                    <td colspan="5"><button type="button" class="btn btn-primary">Search</button></td>
                                </tr>
                            </table>--}}

                            <h4>Top Searches by Model In Your Area</h4>

                            <table width="100%" id="data-table" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="4%">#</th>
                                    <th>Model</th>
                                    <th>Searches (last month)</th>
                                    <th>Change</th>
                                    <th>Searches per inventory Piece</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>
    @endsection

@section('web-footer')
    @parent
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    <script>

		var oTable = $('#data-table').DataTable({
			processing: true,
			serverSide: true,
			aLengthMenu: [20, 50, 100, 500],
			deferRender:    true,
			"scrollX": true,
			"iDisplayLength": 20,
			"order": [[ 2, "desc" ]],
			ajax: '{{route('market-analysis-top-searches-table')}}',
			columns: [
				{ data: 'DT_Row_Index', name: 'id' },
				{ data: 'car_name', name: 'car_name' },
				{ data: 'search', name: 'search' },
				{ data: 'change', name: 'change' },
				{ data: 'inventory', name: 'inventory' },
			]
		});


		var oTable_in_demand = $('#data-table-in-demand').DataTable({
			processing: true,
			serverSide: true,
			aLengthMenu: [20, 50, 100, 500],
			deferRender:    true,
			"scrollX": true,
			"iDisplayLength": 20,
			"order": [[ 2, "desc" ]],
			ajax: '{{route('market-analysis-in-demand-table')}}',
			columns: [
				{ data: 'DT_Row_Index', name: 'id' },
				{ data: 'car_name', name: 'car_name' },
				{ data: 'search_inventory', name: 'search_inventory' },
				{ data: 'search', name: 'search' },
				{ data: 'inventory', name: 'inventory' },
			]
		});


		$(oTable.table().container()).removeClass('form-inline');
		$(oTable_in_demand.table().container()).removeClass('form-inline');

		$('.tab_in_demand_inventory_btn').on('shown.bs.tab', function (e) {
			//e.target // newly activated tab
			//e.relatedTarget // previous active tab
			console.log('in demand');
			oTable_in_demand.columns.adjust().draw();
		})
		$('.tab_trending_local_searches_btn').on('shown.bs.tab', function (e) {

			oTable.columns.adjust().draw();
		});
    </script>

@endsection