	@extends('layouts.web_pages')
	@section('header')
		@parent

		<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
	@endsection
	@section('content')<style>
	.padding{padding:10px;
	margin:10px;background-color:#eaf4fd;
		}
	td{
	vertical-align: middle;	
	}	
		
	.dataTables_scrollHead{
	display:none;
	}
	
	.badge {
    display: inline-block;
    padding: 0.50em 0.75em;
    font-size: 80%;
    font-weight: 500;
	}

	.table th, .table td {
    padding: 0.50rem;
    border-top: 0px solid #dee2e6; 
	}

	input[type=checkbox] {
    display: inline-block;
    *display: inline;
    vertical-align: middle;
    margin: 0;
    padding: 0;
    width: 16px;
    height: 16px;
    border: none;
    cursor: pointer;
    color: #fff;
    -webkit-background-size: 240px 24px;
    background-size: 240px 24px;
}

 #data-table_paginate{
	margin-top: 3%;
	align: right;	
 }
	
	</style>
<div class="header-margin py-5">
    <div class="container">
        @include('dealer.includes.nav', array(
            'tab' => 'Mail-Box',
            'page_title' => 'Mail Compose'
        ))

        <div class="row">
		
		 <div class="col-md-2">
		 <br>
		 <a href="#" class="btn btn-primary btn-block margin-bottom">Compose Message</a>
		 <br>
		 
	
	 	<div class="card border-info mb-3">
        <ul class="list-group list-group-flush cxm-list-group">               
      <li class="list-group-item">
	  
        <a href="http://dev.ottoguide.com/save-adverts.html" data-original-title="" title=""><h5><b>Inbox</b></h5></a>
        <div class="info-txt">view all messages </div> 
        <span class="badge badge-primary">
		<?php 
		$count_emails = DB::table('email_box')->where('dealer_id',Session()->get('dealer_id'))
	    ->where('message_by','client')->count('id');
		if(count($count_emails)){
		 echo $count_emails; 	
		}
		?> 
		</span>
       </li>
		
		<li class="list-group-item ">
        <a href="http://dev.ottoguide.com/save_searches.html" data-original-title="" title=""><h5><b>Sent</b></h5></a>
        <div class="info-txt">sent messages</div> 
		</li>  
	
	
    </ul>
  </div>
		
		 </div>
		
            <div class="col-md-10">
       
              <h1>Compose Message</h1>   					
              

            </div>
        </div>
    </div>

</div>
@endsection

@section('web-footer')
    @parent
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    
	

@endsection