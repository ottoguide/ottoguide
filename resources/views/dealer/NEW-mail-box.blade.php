
	@extends('layouts.web_pages')
	@section('header')
		@parent

	<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
	<link href="http://l-lin.github.io/font-awesome-animation/dist/font-awesome-animation.min.css" rel="stylesheet"/>
	@endsection
	@section('content')<style>
	.padding{padding:10px;
	margin:10px;background-color:#eaf4fd;
		}
	td{
	vertical-align: middle;	
	}	
		
	.dataTables_scrollHead{
	display:none;
	}

   .table th, .table td {
    padding: 0.50rem;
    border-top: 0px solid #dee2e6; 
	}

	input[type=checkbox] {
    display: inline-block;
    *display: inline;
    vertical-align: middle;
    margin: 0;
    padding: 0;
    width: 16px;
    height: 16px;
    border: none;
    cursor: pointer;
    color: #fff;
    -webkit-background-size: 240px 24px;
    background-size: 240px 24px;
}

 #data-table_paginate{
	margin-top: 3%;
	align: right;	
 }
	
  .bg-white{
	margin-bottom: 2%;
    padding:1%;
    border-top: 3px solid #127ba3;
	border-radius: 5px;	
  }
   .empty{
	margin-right: 5px;	
	}
	
  .fa-envelope{
	color:#28B62C;	
	}
	
	</style>
<div class="header-margin py-5">

    <div class="container bg-secondary">
        @include('dealer.includes.nav', array(
            'tab' => 'Mail-Box',
            'page_title' => ucfirst($_GET['email'])
        ))

	 <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>	
        <div class="row">
		
	
		 <div class="col-md-2">
		 <a href="{{url('dealer-compose-email.html')}}" class="btn btn-primary btn-block margin-bottom">Compose Message</a>
		 <br>
		 
	
	 	<div class="card border-info mb-3">
        <ul class="list-group list-group-flush cxm-list-group">               
		<li class="list-group-item">

        <a href="{{url('dealer-mail-box.html?email=inbox')}}" data-original-title="" title=""><h5><b>Inbox</b></h5></a>
       
	   <div class="info-txt">view inbox emails</div> 
        <?php 
		$count_emails = DB::table('email_box')->where('dealer_id',Session()->get('dealer_id'))
	    ->where('message_by','client')->where('is_read',0)->where('is_delete_by_dealer',0)->where('temp_delete_by_dealer',0)->count('id');
		
		if($count_emails > 0){
		echo "<span class='badge badge-success'>";
		echo $count_emails; 
		echo"</span>";		
		 }
		else { }
		?> 
       </li>
		
		<li class="list-group-item">
        <a href="{{url('dealer-mail-box.html?email=sent')}}" data-original-title="" title=""><h5><b>Sent</b></h5></a>
        <div class="info-txt">Sent emails</div> 
		 <span class="badge badge-primary">
		<?php 
		 $count_sent_emails = DB::table('email_box')->where('dealer_id',Session()->get('dealer_id'))
	    ->where('message_by','dealer')->where('is_delete_by_dealer',0)->where('temp_delete_by_dealer',0)->count('id');
		 if(count($count_sent_emails)){
		 echo $count_sent_emails; 	
		 }
		?> 
		</span>
		
		</li>  
		
		<li class="list-group-item">
        <a href="{{url('dealer-mail-box.html?email=trash')}}" data-original-title="" title=""><h5><b>Trash</b></h5></a>
        <div class="info-txt">deleted emails</div> 
		 <span class="badge badge-primary">
		<?php 
		 $count_delete_emails = DB::table('email_box')->where('dealer_id',Session()->get('dealer_id'))
	    ->where('temp_delete_by_dealer',1)->where('is_delete_by_dealer',0)->count('id');
		 if(count($count_delete_emails)){
		 echo $count_delete_emails; 	
		 }
		?> 
		</span>
		</li>  
	
		</ul>
	  </div>
			
		 </div>
		
        <div class="col-md-10">
			<br>
			 <div class="mailbox-controls pull-right">  
             
   			  <button class="btn btn-primary delete_button" form="delete_email" type="submit" disabled="">Delete</button>
			 <?php if($_GET['email']=='Trash'){?>		
			 <a href="{{url('user-empty-trash.html')}}" class="btn btn-danger btn-xs pull-right empty">Empty</a>
			 <?php } ?>
              </div>
			
			<br><br>
			<form method="post" action="{{url('dealer-temp-delete.html')}}" >
			 {!! Form::hidden('_token', csrf_token()) !!}
                       
			
				<div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                 
		<?php 
	
	
		if($_GET['email']=='inbox'){
					
		$emails_list = DB::table('email_box')->where('dealer_id',Session()->get('dealer_id'))->where('message_by','client')->where('temp_delete_by_dealer',0)->where('is_delete_by_dealer',0)->where('reply_to',null)	
		->join('users','email_box.client_id','=','users.id')
		
		->select('email_box.id as email_id', 'users.id as user_id','dealer_id','client_id',
		 'image','subject','name','message','message_by','reply_to','is_read',
		 'email_box.created_at as email_date','users.created_at as user_date')    	
		->get(); 
				
		}	
	
		if($_GET['email']=='sent'){
		
		$emails_list = DB::table('email_box')->where('dealer_id',Session()->get('dealer_id'))->where('message_by','dealer')->where('temp_delete_by_dealer',0)->where('is_delete_by_dealer',0)->where('dealer_first_reply',1)->orderBy('email_box.id','DESC')
		->join('users','email_box.client_id','=','users.id')
		
		->select('email_box.id as email_id', 'users.id as user_id','dealer_id','client_id',
		 'image','subject','name','message','message_by','reply_to','is_read',
		 'email_box.created_at as email_date','users.created_at as user_date')    	
		->get(); 
		
		}	
	
		if($_GET['email']=='trash'){
		
		$emails_list = DB::table('email_box')->where('dealer_id',Session()->get('dealer_id'))->where('temp_delete_by_dealer',1)->where('is_delete_by_dealer',0)
		->join('users','email_box.client_id','=','users.id')
		
		->select('email_box.id as email_id', 'users.id as user_id','dealer_id','client_id',
		 'image','subject','name','message','message_by','reply_to','is_read',
		 'email_box.created_at as email_date','users.created_at as user_date')    	
		->get(); 
		
		}	

	
		?>
				 
		
		@if(count($emails_list))

		<?php 
		
		 $message_reply = DB::table('email_box')->where('reply_to',$emails_list[0]->email_id)->where('message_by','client')->orderBy('id','desc')->get()->first();

		 if(count($message_reply)){
		 
		 $message = $message_reply->message;
		 
		 }
		 
		 else{
		 
		 $message = $emails_list[0]->message; 
		  
		 }
	
		 $message_date = DB::table('email_box')->where('reply_to',$emails_list[0]->email_id)->where('is_read',0)->where('message_by','client')->orderBy('id','desc')->get()->first();
		 
		 if(count($message_date)){
			
		 $message_date = date("d-m-Y", strtotime($message_date->created_at));
		 
		 }
		 
		 else{
		 $message_date = date("d-m-Y", strtotime($emails_list[0]->email_date));	 
		 }

		?>
			
				
		  @foreach($emails_list as $email_list)
				
				    <tr>
					<td class="mailbox-star"><a href="#"><i class="fa fa-star is_read{{$email_list->is_read}}"></i></a></td>
                    <td class="mailbox-name"><a href="{{route('message/read')}}?email_id=<?= encrypt($email_list->email_id) ?>">{{$email_list->name}}</a></td>
                    <td class="mailbox-subject"><b>{{substr($email_list->subject,0,25)}}</b> - {{strip_tags(substr($message,0,35))}}	
				    </td>                                  
                    <td class="mailbox-date"><?= $message_date ?></td>
					<td>
					<input type="checkbox" value="{{$email_list->email_id}}" class="delete_email" name=""></td>
                    </tr>
				
				  @endforeach
							

			
		
		@else		
				  <br>  
				  <h5 align="center"><b> No Email Found </b></h5>			
		@endif			
		
                 </tbody>
                </table>
				
				
			<!-- DELETE EMAIL -->	
				
		 	
				
                <!-- /.table -->
              </div>


				
		     </form>	
           	
            </div>
        </div>
    </div>

</div>
@endsection

@section('web-footer')
    @parent
       
	<script>
	
	   $(document).on('change','.delete_email',function(){   
	
			$(".delete_button").attr("disabled", true); 
		
			$('.delete_ids').val('');
		
			var selectedvalue = [];
		
			$(":checkbox:checked").each(function(e){
		
			$(".delete_button").attr("disabled", false); 
		
			selectedvalue.push($(this).val());
		
			$('.delete_ids').val(selectedvalue.join(","));

			});

		});
	
    </script>

@endsection