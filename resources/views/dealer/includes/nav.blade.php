<?php
if (!isset($tab)) {
	$tab = 'dashboard';
}
if (!isset($page_title)) {
	$page_title = 'Dashboard';
}
?>
<style>
    #dealer_main_nav {
        background-color: #eaf4fd !important;
        margin: 0 -15px 0;
        border: 0;
        width: calc(100% + 30px);
        display: block;
        color: #217C9D;
    }
    #dealer_main_nav .nav-link {
        padding: 13px !important;
        line-height: 20px;
        color: #217C9D !important;
    }
    #dealer_main_nav .nav-item.active .nav-link,
    #dealer_main_nav .nav-item:hover .nav-link {
        color: #fff !important;
        background-color: #217C9D !important;
        border-radius: 5px;
        text-decoration: none;
    }



    .top-head {
        background-color: #eaf4fd;
        padding: 12px;
        border-bottom: 1px solid #ccc;

    }
    .top-head h4 {
        font-weight: bold;
        color: #222;

    }
    .bg-secondary {
        background-color: #fefefe;
    }
    .grey {
        background-color: #f2f2f2;
    }
    .tag {
        padding: 10px;
        background-color: #FFF6CA;
        font-size: 12px;
        color: #333;
        border-radius: 4px;
        text-align: center;
        font-weight: bold;
    }
	
	.dropdown-menu {	
		background-color: #eaf4fd;
	}		
		
	.dropdown-menu li{	
		min-width: 100%;
		border-bottom: 1px dotted #ccc;
		padding: 5px;
	}
	
	.dropdown-menu li a:hover{	
		color: #217C9D;
	}	

	.margin-right{
	 margin-right: 18px;
	}	

	.margin-top{
	 margin-top: 10px;
	}
		

</style>


 <div class="row top-head">
    <div class="col-md-4"><h5>
	Welcome: <br><b>
	<?php
	
	if(Session::get('subdealer')){
	
	$get_subdealer = DB::table('dealer_subaccount')
	->where('email',Session::get('subdealer'))->first();	
	
	echo $get_subdealer->name;
	}
	
	else{
	echo $dealer_data->dealer_name; 	
		
	}

	?>
	
	
	</b>
	</h5>
	</div>
	
	<div class="col-md-3"></div>
	
    <div class="col-md-5 margin-top">

	<a href="#topmenu" class="btn btn-info pull-right" data-toggle="collapse">Main Menu</a>
    
	<div id="topmenu" class="collapse pull-right margin-right">

	@if(Session::get('subdealer'))
   
    @else	

     <a href="{{route('subdealer-form')}}" class="btn btn-primary btn-sm">Create Sub Dealer</a>
   
     <a href="{{route('subdealer-list')}}" class="btn btn-success btn-sm">Sub Dealer List</a>
   
	 @endif
     
	 <a href="{{route('dealer_logout')}}" class="btn btn-danger btn-sm">Logout</a>
   
    </div>
    </div>
	
</div>

<div class="row">

    <div id="navbar" class="col-lg-12">

        <nav class="navbar navbar-expand-lg navbar-light" id="dealer_main_nav">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item <?= ($tab == 'dashboard') ? 'active' : '' ?>">
                        <a class="nav-link" href="{{route('dealer_portal')}}">Dashboard</a>
                    </li>

					<li class="nav-item <?= ($tab == 'Mail-Box') ? 'active' : ''  ?>">
                        <a class="nav-link" href="{{url('dealer-mail-box.html?email=inbox')}}">Email Panel</a>
                    </li>

					<li class="nav-item <?= ($tab == 'rbo') ? 'active' : '' ?>">
                        <a class="nav-link" href="{{route('request-best-offer-list')}}">Request Best Offer</a>
                    </li>
					
					 <li class="nav-item <?= ($tab == 'chat') ? 'active' : ''  ?>">
                        <a class="nav-link" href="{{route('dealer-chat')}}">Chat Panel</a>
                    </li>

					 <li class="nav-item <?= ($tab == 'craiglist') ? 'active' : '' ?>">
                        <a class="nav-link" href="{{route('criagslist')}}">Social Media Assistant</a>
                      </li> 
					
					<li class="nav-item <?= ($tab == 'dealer-pricing') ? 'active' : '' ?>">
                        <a class="nav-link" href="{{route('dealer-pricing')}}">Dealer Pricing Tool</a>
                    </li>
					
					   <li class="nav-item <?= ($tab == 'sales-review') ? 'active' : '' ?>">
                        <a class="nav-link" href="{{route('dealer-reviews')}}">Manage User Reviews</a>
                    </li>
					
					 <li class="nav-item <?= ($tab == 'market-analysis') ? 'active' : '' ?>">
                        <a class="nav-link" href="{{route('market-analysis')}}">Market Analysis</a>
                    </li>
					
                    <li class="nav-item <?= ($tab == 'billing') ? 'active' : '' ?>">
                        <a class="nav-link" href="{{route('dealer-billing')}}">Billing</a>
                    </li>
					
					<li class="nav-item <?= ($tab == 'settings') ? 'active' : '' ?>">
                        <a class="nav-link" href="{{route('dealer-settings')}}">Settings</a>
                       </li>

                    <!--<li class="nav-item <?//= ($tab == 'leads') ? 'active' : '' ?>">
                        <a class="nav-link" href="{{route('leeds-stats-page')}}">Leads</a>
                    </li>-->

					

                  

				
                </ul>
            </div>
        </nav>
    </div>

</div>

<div class="row grey">
    <div class="col-md-4"><br>
        <h2><?= $page_title;?></h2>
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-2"><br>
        {{--<b>Account Manager</b>--}}
        {{--<p>Key File Wanger</p>--}}
    </div>
    <div class="col-md-2"><br>
        {{--<b>General Support</b>--}}
        {{--<p>(866) 411 6886</p>--}}
    </div>
</div>

<script>
    $(document).ready(function () {
		$('.cxm-menu li:nth-child(4)').hide();
	})
</script>