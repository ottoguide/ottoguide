@extends('layouts.web_pages')
@section('content')

    <style>
	
        .card-inner {
            margin-left: 2rem;
        }

        .p-0 {
            padding-left: 0px;
        }

        .b-0 {
            margin-bottom: 0px;
        }

        .fa-star {
            color: #ffc107 !important;
        }

        .border-top {
            border-top: 1px solid #ccc;
            margin-bottom: 5px;

        }

        .twitter-share-button {
            margin-bottom: -5px;
        }

        .card-body {
            padding: 0.025rem;
        }

        #myModal {
            margin-top: 20%;
        }
 
		.nice-ul {
			position: relative;
			padding-left: 32px;
			list-style-type: none;
		}
		.nice-ul li {
			margin-bottom: 8px;
		}
		.nice-ul li:last-child {
			margin-bottom: 0;
		}
		.nice-ul li::before {
			content: "\2713";
			position: absolute;
			left: 0;
			padding: 2px 8px;
			font-size: 1em;
			color: #1C90F3;
		}
		 
		.nice-ol {
			position: relative;
			padding-left: 32px;
			list-style-type: none;
			margin-left: 5%;
		}
		.nice-ol li {
			counter-increment: step-counter;
			margin-bottom: 6px;
			padding: 8px;
		}
		.nice-ol li:last-child {
			margin-bottom: 0;
		}
		.nice-ol li::before {
			content: counter(step-counter);
			position: absolute;
			left: 0;
			padding: 3px 8px;
			font-size: 0.9em;
			color: white;
			font-weight: bold;
			background-color: #217C9D;
			border-radius: 50%;
		}
		.square-box{
		border:1px solid #999;		
		}
		.description-box{
		margin-left: 10px;
		padding: 5px;	
			
		}
		small{
		margin-left: 10px;	
		color:#222;
		}
		.border{
		border:1px solid #ccc;
		}
		.items{
		padding-left:0px;
		padding-right:0px;		
		}
		
		.heading{
		 background-color: #217C9D;
		/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1f6591+1,2492a5+100 */
		background: #1f6591; /* Old browsers */
		background: -moz-linear-gradient(top, #1f6591 1%, #2492a5 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #1f6591 1%,#2492a5 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #1f6591 1%,#2492a5 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1f6591', endColorstr='#2492a5',GradientType=0 ); /* IE6-9 */
		padding:6px;
		color:#fff;
		font-size: 16px;
		font-weight: bold;
		}
		
		.block-info{
		padding: 10px;
		border-bottom: 1px solid #ccc;
		padding-bottom: 30px;	
		}
		
		.width{
		width: 95%;
		margin-left:5%;		
		}
		
		h5{
		font-weight: bold;
		color:#555;	
		}
		
		.cxm-img IMG{
			border-radius: 5px;
		}
		
		hr{
		border-bottom: 0.5px dotted #ccc;	
		}
		
		#addMore{
		margin-top:8px;	
		}
		.quote_form{
			margin-right: 10px;
		}
		</style>

	
<script
    src="https://code.jquery.com/jquery-2.1.4.min.js"
    integrity="sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
    crossorigin="anonymous">
</script>	
		
		
		
    <div class="header-margin py-5">
        
		<div class="container">
            @include('dealer.includes.nav', array(
                'tab' => 'rbo',
                'page_title' => 'RBO Quote Edit Form'
            ))
            <br>

            <div class="message" width="50%" align="center">
                @if (session('message'))
                    <div class="alert alert-success" width="50%">
                        {{ session('message') }}
                    </div>
                @endif
            </div>

			<br>
			
		<div class="row">
	
	
	<div class="col-sm-12 col-md-12 col-lg-12">
		  
            <div class="card-body">
			 
              <div class="cxm-advert-item mb-3" >            
                  <div class="form-row">
                    <div class="col-sm-3">
                      <div class="cxm-img">
                        <a href="#"><img class="img-fluid" src="@if(!empty($car_datas->media->photo_links[0]) > 0){{$car_datas->media->photo_links[0]}}@endif"></a>
                      </div>
                    </div>
                    <div class="col-sm-9">
                    <div class="cxm-content">
			   
                    <div>
					
					
				    <div class="text-primary font-weight-bold fs18">@if(!empty($car_datas->heading) > 0){{$car_datas->heading}}@endif</div>
                   
				   <span class="fc1 fs24">MSRP: $@if(!empty($car_datas->ref_price) > 0){{$car_datas->ref_price}}@endif</span></div>
				   
				   <ul class="cxm-facts fs12 bg-secondary p-2 rounded">
                        <li><span class="fa fa-modx text-primary"></span> 
				       @if(!empty($car_datas->build->year) > 0){{$car_datas->build->year}}@endif
												</li>
                        <li><span class="fa fa-barcode text-primary"></span> 
					    @if(!empty($car_datas->build->make) > 0){{$car_datas->build->make}}@endif 
						</li>
                        
						<li><span class="fa fa-road text-primary"></span> 
						@if(!empty($car_datas->miles) > 0){{$car_datas->miles}}@endif  miles
						</li>
                        <li><span class="fa fa-toggle-on text-primary"></span>
												Automatic
                       						</li>
											<br><br>
                        <li><span class="fa fa-spinner text-primary"></span> 
						
							@if(!empty($car_datas->build->engine) > 0){{$car_datas->build->engine}}@endif
                       						</li>
                        <li><span class="fa fa-fire text-primary"></span> 
							@if(!empty($car_datas->build->fuel_type) > 0){{$car_datas->build->fuel_type}}@endif
                       	</li>
                      </ul>
					
					</div>
					
					
                    </div>
                  </div>          
                </div>
          
              </div>
		 
		
		   </div>

	
		
		<div class="col-sm-12 col-md-12 col-lg-12">
		
		 <form action="{{route('rbo-quote-form-update')}}" method="post" >
                                       
       {!! Form::hidden('_token', csrf_token()) !!}
	   
	   
		  <h3>Dealer Price Options <button id="addMore" class="btn btn-primary pull-right" style="color:#fff;">Add Options</button> </h3>
		  <hr>
           			
			<input type="hidden" name="rbo_id" value="<?= $edit_quote_form->rbo_id ?>">
			<input type="hidden" name="car_price" value="<?= $car_datas->ref_price ?>">

			<div class="quote">
					
					<?php 
					
					 $get_dealer_option_description = explode(',',$edit_quote_form->dealer_price_description);
					 $get_dealer_option_value = explode(',',$edit_quote_form->dealer_price_values);
						
						foreach($get_dealer_option_description as $key => $description){ ?>
				
							  <div class="row remove_field">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label><b>Descriptions</b></label>
                                        <input type="text" class="form-control " name="dealer_options_description[]" value="<?= $get_dealer_option_description[$key] ?>" id="dealer_options_description ">
                                    </div>
                                </div>
                          

                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label><b>Value</b></label>
                                        <!--<input type="file" class="form-control" id="pdf_report" name="pdf_report">-->
								   
								  <input type="text" class="form-control " name="dealer_options_value[]" value="<?=$get_dealer_option_value[$key];?>" placeholder="$" id="dealer_options_value">
                                    </div>
                                </div>
								
								
								<div class="col-sm-2 ">
                                    <div class="form-group"><br>
						        <a type="button" class="btn btn-danger btn-sm remove_old_field"><i class="fa fa-times"></i></a> </div>
                                </div>
							
							</div>	
				
				
						<?php } ?>	
				
				
						
							</div>                 
										
					     
		
							<br><br>
		
						  <h3>Document Fees  <button id="addMore_df" class="btn btn-primary pull-right"style="color:#fff;"> Add Options</button></h3>
						  <hr>
           			  <div class="document_fees">
					  
					 <?php 
					 
					 $get_dealer_doc_fees_description = explode(',',$edit_quote_form->doc_fees_description);
					 $get_dealer_doc_fees_values = explode(',',$edit_quote_form->doc_fees_values);
						
						foreach($get_dealer_doc_fees_description as $key => $doc_fee_description){ ?>
					  
					  
					  
                            <div class="row remove_field2">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label><b>Descriptions</b></label>
                                        <input type="text" class="form-control" name="doc_fees_description[]" value="<?=$doc_fee_description;?>" id="doc_fees_description[]">
                                    </div>
                                </div>
                          

                          
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label><b>Value</b></label>
                                        <!--<input type="file" class="form-control" id="pdf_report" name="pdf_report">-->
								 <input type="text" class="form-control" name="doc_fees_value[]" value="<?= $get_dealer_doc_fees_values[$key] ?>" placeholder="$" id="doc_fees_value[]">
                                    </div>
                                </div>
								
									
							<div class="col-sm-2">
                            <div class="form-group"><br>		
						   <a type="button" class="btn btn-danger btn-sm remove_old_field2"><i class="fa fa-times"></i></a>                                 </div>

							</div>
							
							</div>	
							
							
						 <?php } ?>
							
							
							</div>

						
							
					<br><br>
					
							
				<h3>Rebates Discounts & Incentives  <button id="addMore_rd" class="btn btn-primary pull-right" style="color:#fff;">Add Options</button></h3>
		           <hr>
           			  <div class="rebate_discount">
					  
					 <?php 

					 $get_dealer_rebate_discount_description = explode(',',$edit_quote_form->rebate_discount_description);
					 $get_dealer_rebate_discount_values = explode(',',$edit_quote_form->rebate_discount_values);
						
						foreach($get_dealer_rebate_discount_description as $key => $rebate_discount_description){ ?>
					  
					  
                            <div class="row remove_field3">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label><b>Descriptions</b></label>
                                     <input type="text" class="form-control" name="rebate_discount_description[]" value="<?= $rebate_discount_description ?>" id="rebate_discount_description">
                                    </div>
                                </div>
                          

                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label><b>Value</b></label>
								 <input type="text" class="form-control" name="rebate_discount_value[]" value="<?= $get_dealer_rebate_discount_values[$key]?>" placeholder="$" id="rebate_discount_value">
                                </div>
                                </div>
								
								<div class="col-sm-2">
                                <div class="form-group"><br>
								<a type="button" class="btn btn-danger btn-sm remove_old_field3"><i class="fa fa-times"></i></a>
                                </div>
                                </div>
								
                          
							</div>

							<?php } ?>
							
							</div>  
			
							
							<br><br>
							
							<h3>Quote Expiry Date</h3>
		                    <hr>
						      <div class="row">
							  
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <input type="date" class="form-control" value="<?= $edit_quote_form->expiry_date ?>" name="quote_expiry_date" id="quote_expiry_date">
                                    </div>
                                </div>
								
								<div class="col-sm-6">
								 <button type="button" class="btn btn-danger  pull-right">Cancel Form</button> &nbsp; &nbsp;
                                    <button type="submit" class="btn btn-success quote_form pull-right">Update Quote Form </button>
									 
                                </div>
								
                          
							</div>		
		
		
					</div>
	 
		 
		 
						 <script>
								$(function() {
									$("#addMore").click(function(e) {
										e.preventDefault();
										var count = $('.js-from_add_field').length;
									
										var final_html = "<div class='row dark js-from_add_field'>";
										final_html += ("<div class='col-sm-5'><div class='form-group'><input type='text' class='form-control' name='dealer_options_description[]'></div></div>");
										final_html += ("<div class='col-sm-5'><div class='form-group'><input type='text'  class='form-control child_report_files' name='dealer_options_value[]'></div></div>");
										final_html += '<br><div class="col-sm-2 minus"><a type="button" class="btn btn-danger btn-sm remove_more_fld"><i class="fa fa-times"></i></a></div>';
										final_html += '</div>';
										$(".quote").append(final_html);
									});

									$(document).on('click', '.remove_more_fld', function (e) {
										e.stopImmediatePropagation();
										$(this).parent().parent().remove();
									});			
									
									$(document).on('click', '.remove_old_field', function (e) {
									
												$('.remove_field').remove();
									});
									
									$(document).on('change', '.child_report_files', function() {
										
										if ($(this)[0].files.length > 0) {
											$(this).next('.check_file').val('1');
										}
									
									})
								});
                                </script>
		 
		 
							<script>
								$(function() {
									$("#addMore_df").click(function(e) {
										e.preventDefault();
										var count = $('.js-from_add_field').length;
									
										var final_html = "<div class='row dark js-from_add_field'>";
										final_html += ("<div class='col-sm-5'><div class='form-group'><input type='text' class='form-control' name='doc_fees_description[]'></div></div>");
										final_html += ("<div class='col-sm-5'><div class='form-group'><input type='text'  class='form-control child_report_files' name='doc_fees_value[]'></div></div>");
										final_html += '<br><div class="col-sm-2 minus"><a type="button" class="btn btn-danger btn-sm remove_more_fld"><i class="fa fa-times"></i></a></div>';
										final_html += '</div>';
										$(".document_fees").append(final_html);
									});

									$(document).on('click', '.remove_more_fld', function (e) {
										e.stopImmediatePropagation();

										$(this).parent().parent().remove();
									});
									
									
										$(document).on('click', '.remove_old_field2', function (e) {
									
												$('.remove_field2').remove();
											});
									
									
									
									
									$(document).on('change', '.child_report_files', function() {
										
										if ($(this)[0].files.length > 0) {
											$(this).next('.check_file').val('1');
										}
									
									})
								});
                                </script>
		 
		 
		 
							<script>
								$(function() {
									$("#addMore_rd").click(function(e) {
										e.preventDefault();
										var count = $('.js-from_add_field').length;
									
										var final_html = "<div class='row dark js-from_add_field'>";
										final_html += ("<div class='col-sm-5'><div class='form-group'><input type='text' class='form-control' name='rebate_discount_description[]'></div></div>");
										final_html += ("<div class='col-sm-5'><div class='form-group'><input type='text'  class='form-control child_report_files' name='rebate_discount_value[]'></div></div>");
										final_html += '<br><div class="col-sm-2 minus"><a type="button" class="btn btn-danger btn-sm remove_more_fld"><i class="fa fa-times"></i></a></div>';
										final_html += '</div>';
										$(".rebate_discount").append(final_html);
									});

									$(document).on('click', '.remove_more_fld', function (e) {
										e.stopImmediatePropagation();

										$(this).parent().parent().remove();
									});
									
									$(document).on('click', '.remove_old_field3', function (e) {
									   $('.remove_field3').remove();
									});
									
									
									$(document).on('change', '.child_report_files', function() {
										
										if ($(this)[0].files.length > 0) {
											$(this).next('.check_file').val('1');
										}
									
									})
								});
                                </script>
								
	   
					<div class="col-sm-1 col-md-1 col-lg-1">
					
					</div>
					

	
		  </form>	
		</div>
		</div>
		<br><br>	
	  
	
		</div>

</div>

    
    <!-- Message Reply-->
   

 

@endsection