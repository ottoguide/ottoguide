@extends('layouts.web_pages')
@section('header')
    @parent

    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
@endsection
@section('content')
    <div class="header-margin py-5">
        <div class="container">
            <div class="flash-message subscription_added" style="display: none">
                <p class="alert alert-success">Subscription Added Successfully</p>
            </div>
            <div class="flash-message subscription_removed" style="display: none">
                <p class="alert alert-danger">Subscription Removed Successfully</p>
            </div>
            @include('dealer.includes.nav', array(
                'tab' => 'billing',
				'page_title' => 'Billing'
            ))

            <div class="row bg-secondary">
                <div class="col-md-12">
                    <table class="table table-striped table-hover dataTable no-footer" style="width: 100%;">
                        <thead>
                        <tr>
                            <th style="width: 10%;">#</th>
                            <th style="width: 25%;">Name</th>
                            <th style="width: 25%;">Price</th>
                            <th style="width: 20%;">Unit</th>
                            <th style="width: 20%;" class="text-center">Action</th>
                        </tr>

                        {{--Market Analysis--}}
                        <tr>
                            <td>1</td>
                            <td><h5>Market Analysis</h5></td>
                            <td>USD {{ $addons['market_analysis_addon_price'] }}</td>
                            <td>Per Month</td>
                            <td>
                                @if($addons['market_analysis_addon'] == 1)
                                    <input type="button" value="Remove From Subscribtion" class="btn btn-danger" style="width: 177px;" id="remove_market_analysis_addon"  onclick='addon_function(this)' />
                                @else
                                    <input type="button" value="Add To Subscribtion" class="btn btn-success" style="width: 177px;" id="add_market_analysis_addon"  onclick='addon_function(this)' />
                                @endif
                            </td>
                        </tr>

                        {{--RBO Lead--}}
                        <tr>
                            <td>2</td>
                            <td><h5>RBO Lead</h5></td>
                            <td>USD {{ $addons['rbo_lead_addon_price'] }}</td>
                            <td>Per Unit</td>
                            <td>
                                @if($addons['rbo_lead_addon'] == 1)
                                    <input type="button" value="Remove From Subscribtion" class="btn btn-danger" style="width: 177px;" id="remove_rbo_lead_addon"  onclick='addon_function(this)' />
                                @else
                                    <input type="button" value="Add To Subscribtion" class="btn btn-success" style="width: 177px;" id="add_rbo_lead_addon"  onclick='addon_function(this)' />
                                @endif
                            </td>
                        </tr>

                        {{--Dealer Pricing Tool--}}
                        <tr>
                            <td>3</td>
                            <td><h5>Dealer Pricing Tool</h5></td>
                            <td>USD {{ $addons['dealer_pricing_tool_addon_price'] }}</td>
                            <td>Per Unit</td>
                            <td>
                                @if($addons['dealer_pricing_tool_addon'] == 1)
                                    <input type="button" value="Remove From Subscribtion" class="btn btn-danger" style="width: 177px;" id="remove_dealer_pricing_tool_addon"  onclick='addon_function(this)' />
                                @else
                                    <input type="button" value="Add To Subscribtion" class="btn btn-success" style="width: 177px;" id="add_dealer_pricing_tool_addon"  onclick='addon_function(this)' />
                                @endif
                            </td>
                        </tr>

                        {{--Voice Lead--}}
                        <tr>
                            <td>4</td>
                            <td><h5>Voice Lead</h5></td>
                            <td>USD {{ $addons['voice_lead_addon_price'] }}</td>
                            <td>Per Unit</td>
                            <td>
                                @if($addons['voice_lead_addon'] == 1)
                                    <input type="button" value="Remove From Subscribtion" class="btn btn-danger" style="width: 177px;" id="remove_voice_lead_addon"  onclick='addon_function(this)' />
                                @else
                                    <input type="button" value="Add To Subscribtion" class="btn btn-success" style="width: 177px;" id="add_voice_lead_addon"  onclick='addon_function(this)' />
                                @endif
                            </td>
                        </tr>

                        {{--Email Lead--}}
                        <tr>
                            <td>5</td>
                            <td><h5>Email Lead</h5></td>
                            <td>USD {{ $addons['email_lead_addon_price'] }}</td>
                            <td>Per Unit</td>
                            <td>
                                @if($addons['email_lead_addon'] == 1)
                                    <input type="button" value="Remove From Subscribtion" class="btn btn-danger" style="width: 177px;" id="remove_email_lead_addon"  onclick='addon_function(this)' />
                                @else
                                    <input type="button" value="Add To Subscribtion" class="btn btn-success" style="width: 177px;" id="add_email_lead_addon"  onclick='addon_function(this)' />
                                @endif
                            </td>
                        </tr>

                        {{--Activity Notify--}}
                        <tr>
                            <td>6</td>
                            <td><h5>Activity Notify</h5></td>
                            <td>USD {{ $addons['activity_notify_addon_price'] }}</td>
                            <td>Per Unit</td>
                            <td>
                                @if($addons['activity_notify_addon'] == 1)
                                    <input type="button" value="Remove From Subscribtion" class="btn btn-danger" style="width: 177px;" id="remove_activity_notify_addon"  onclick='addon_function(this)' />
                                @else
                                    <input type="button" value="Add To Subscribtion" class="btn btn-success" style="width: 177px;" id="add_activity_notify_addon"  onclick='addon_function(this)' />
                                @endif
                            </td>
                        </tr>

                        {{--Chat Lead--}}
                        <tr>
                            <td>7</td>
                            <td><h5>Chat Lead</h5></td>
                            <td>USD {{ $addons['chat_lead_addon_price'] }}</td>
                            <td>Per Unit</td>
                            <td>
                                @if($addons['chat_lead_addon'] == 1)
                                    <input type="button" value="Remove From Subscribtion" class="btn btn-danger" style="width: 177px;" id="remove_chat_lead_addon"  onclick='addon_function(this)' />
                                @else
                                    <input type="button" value="Add To Subscribtion" class="btn btn-success" style="width: 177px;" id="add_chat_lead_addon"  onclick='addon_function(this)' />
                                @endif
                            </td>
                        </tr>
                        </thead>
                    </table>

                </div>
            </div>
			<div class="invoice">
			<table class="table table-striped" style="width: 100%;  margin-top: 26px;">
			<h3>Invoice Details</h3>

					<tr>
					<th style="width: 25%;">Invoice #</th>
					<th style="width: 25%;">Date</th>
					<th style="width: 25%;">Amount</th>
                        <th style="width: 25%;">Status</th>
					</tr>
                        @if(!empty($invoices))
                            @foreach($invoices as $invoice)
                                @if($invoice['invoice']['status'] != "paid")
                                    <tr>
                                    <td>{{ $invoice['invoice']['id'] }}</td>
                                    <td>{{ date("m/d/Y H:i:s", $invoice['invoice']['date'])  }}</td>
                                    <td>USD {{ number_format($invoice['invoice']['amount_due'] / 100, 2) }}</td>
                                    <td><button class="btn {{ ($invoice['invoice']['status'] == "paid") ? "btn-success": "btn-warning" }}" id="{{ $invoice['invoice']['customer_id'] }}" style="height: 28px;" onclick='pay_invoice_function(this)'>{{ strtoupper($invoice['invoice']['status']) }}</button></td>
                                    </tr>
                                @endif
                             @endforeach
                         @endif
					</table>
					</div>
        </div>

    </div>
@endsection

@section('web-footer')
    @parent

    {{--<script src="https://js.chargebee.com/v2/chargebee.js" data-cb-site="ottoguide" ></script>--}}

    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    <script>

        function addon_function(addon) {
//            alert(addon.id);

            if(addon.id == 'add_chat_lead_addon'){
                document.getElementById(addon.id).classList.add('btn-danger');
                document.getElementById(addon.id).classList.remove('btn-success');
                document.getElementById(addon.id).value="Remove From Subscribtion";
                document.getElementById(addon.id).id="remove_chat_lead_addon";

                add_addon('chat_lead_addon');

            }
            else if(addon.id == 'remove_chat_lead_addon'){
                document.getElementById(addon.id).classList.add('btn-success');
                document.getElementById(addon.id).classList.remove('btn-danger');
                document.getElementById(addon.id).value="Add To Subscribtion";
                document.getElementById(addon.id).id="add_chat_lead_addon";

                remove_addon('chat_lead_addon');

            }
            else if(addon.id == 'add_activity_notify_addon'){
                document.getElementById(addon.id).classList.add('btn-danger');
                document.getElementById(addon.id).classList.remove('btn-success');
                document.getElementById(addon.id).value="Remove From Subscribtion";
                document.getElementById(addon.id).id="remove_activity_notify_addon";

                add_addon('activity_notify_addon');

            }
            else if(addon.id == 'remove_activity_notify_addon'){
                document.getElementById(addon.id).classList.add('btn-success');
                document.getElementById(addon.id).classList.remove('btn-danger');
                document.getElementById(addon.id).value="Add To Subscribtion";
                document.getElementById(addon.id).id="add_activity_notify_addon";

                remove_addon('activity_notify_addon');

            }
            else if(addon.id == 'add_email_lead_addon'){
                document.getElementById(addon.id).classList.add('btn-danger');
                document.getElementById(addon.id).classList.remove('btn-success');
                document.getElementById(addon.id).value="Remove From Subscribtion";
                document.getElementById(addon.id).id="remove_email_lead_addon";

                add_addon('email_lead_addon');

            }
            else if(addon.id == 'remove_email_lead_addon'){
                document.getElementById(addon.id).classList.add('btn-success');
                document.getElementById(addon.id).classList.remove('btn-danger');
                document.getElementById(addon.id).value="Add To Subscribtion";
                document.getElementById(addon.id).id="add_email_lead_addon";

                remove_addon('email_lead_addon');

            }
            else if(addon.id == 'add_voice_lead_addon'){
                document.getElementById(addon.id).classList.add('btn-danger');
                document.getElementById(addon.id).classList.remove('btn-success');
                document.getElementById(addon.id).value="Remove From Subscribtion";
                document.getElementById(addon.id).id="remove_voice_lead_addon";

                add_addon('voice_lead_addon');

            }
            else if(addon.id == 'remove_voice_lead_addon'){
                document.getElementById(addon.id).classList.add('btn-success');
                document.getElementById(addon.id).classList.remove('btn-danger');
                document.getElementById(addon.id).value="Add To Subscribtion";
                document.getElementById(addon.id).id="add_voice_lead_addon";

                remove_addon('voice_lead_addon');

            }
            else if(addon.id == 'add_dealer_pricing_tool_addon'){
                document.getElementById(addon.id).classList.add('btn-danger');
                document.getElementById(addon.id).classList.remove('btn-success');
                document.getElementById(addon.id).value="Remove From Subscribtion";
                document.getElementById(addon.id).id="remove_dealer_pricing_tool_addon";

                add_addon('dealer_pricing_tool_addon');

            }
            else if(addon.id == 'remove_dealer_pricing_tool_addon'){
                document.getElementById(addon.id).classList.add('btn-success');
                document.getElementById(addon.id).classList.remove('btn-danger');
                document.getElementById(addon.id).value="Add To Subscribtion";
                document.getElementById(addon.id).id="add_dealer_pricing_tool_addon";

                remove_addon('dealer_pricing_tool_addon');

            }
            else if(addon.id == 'add_rbo_lead_addon'){
                document.getElementById(addon.id).classList.add('btn-danger');
                document.getElementById(addon.id).classList.remove('btn-success');
                document.getElementById(addon.id).value="Remove From Subscribtion";
                document.getElementById(addon.id).id="remove_rbo_lead_addon";

                add_addon('rbo_lead_addon');

            }
            else if(addon.id == 'remove_rbo_lead_addon'){
                document.getElementById(addon.id).classList.add('btn-success');
                document.getElementById(addon.id).classList.remove('btn-danger');
                document.getElementById(addon.id).value="Add To Subscribtion";
                document.getElementById(addon.id).id="add_rbo_lead_addon";

                remove_addon('rbo_lead_addon');

            }
            else if(addon.id == 'add_market_analysis_addon'){
                document.getElementById(addon.id).classList.add('btn-danger');
                document.getElementById(addon.id).classList.remove('btn-success');
                document.getElementById(addon.id).value="Remove From Subscribtion";
                document.getElementById(addon.id).id="remove_market_analysis_addon";

                add_addon('market_analysis_addon');

            }
            else if(addon.id == 'remove_market_analysis_addon'){
                document.getElementById(addon.id).classList.add('btn-success');
                document.getElementById(addon.id).classList.remove('btn-danger');
                document.getElementById(addon.id).value="Add To Subscribtion";
                document.getElementById(addon.id).id="add_market_analysis_addon";

                remove_addon('market_analysis_addon');

            }

        }

        function add_addon(id) {
            $.ajax({
                url: '/add_addon_subscriptions',
                type: 'POST',
                data: {_token: "{{ csrf_token() }}",addon_id:id},
                dataType: 'JSON',
                success: function (data) {
//                    alert('Added ' + data.msg);
                    $(".subscription_added").css("display", "block");

                    setTimeout(function() {
                        $(".subscription_added").css("display", "none");
                    }, 3000);
                }
            });
        }

        function remove_addon(id) {

            $.ajax({
                url: '/remove_addon_subscriptions',
                type: 'POST',
                data: {_token: "{{ csrf_token() }}",addon_id:id},
                dataType: 'JSON',
                success: function (data) {
//                    alert('Removed ' + data.msg);
                    $(".subscription_removed").css("display", "block");

                    setTimeout(function() {
                        $(".subscription_removed").css("display", "none");
                    }, 3000);
                }
            });
        }

        function pay_invoice_function(data) {
            $.ajax({
                url: '/invoice_url',
                type: 'POST',
                data: {_token: "{{ csrf_token() }}",customer_id:data.id},
                dataType: 'JSON',
                success: function (data) {
                    console.log(data.hosted_page.url);
                    window.open(data.hosted_page.url, '_blank');
                }
            });
        }

    </script>

@endsection