@extends('layouts.web_pages')
@section('content')

    <style>

        .card-inner {
            margin-left: 2rem;
        }

        .p-0 {
            padding-left: 0px;
        }

        .b-0 {
            margin-bottom: 0px;
        }

        .fa-star {
            color: #ffc107 !important;
        }

        .border-top {
            border-top: 1px solid #ccc;
            margin-bottom: 5px;

        }

        .twitter-share-button {
            margin-bottom: -5px;
        }

        .card-body {
            padding: 0.025rem;
        }

        #myModal {
            margin-top: 20%;
        }

        .nice-ul {
            position: relative;
            padding-left: 32px;
            list-style-type: none;
        }

        .nice-ul li {
            margin-bottom: 8px;
        }

        .nice-ul li:last-child {
            margin-bottom: 0;
        }

        .nice-ul li::before {
            content: "\2713";
            position: absolute;
            left: 0;
            padding: 2px 8px;
            font-size: 1em;
            color: #1C90F3;
        }

        .nice-ol {
            position: relative;
            padding-left: 32px;
            list-style-type: none;
            margin-left: 5%;
        }

        .nice-ol li {
            counter-increment: step-counter;
            margin-bottom: 6px;
            padding: 8px;
        }

        .nice-ol li:last-child {
            margin-bottom: 0;
        }

        .nice-ol li::before {
            content: counter(step-counter);
            position: absolute;
            left: 0;
            padding: 3px 8px;
            font-size: 0.9em;
            color: white;
            font-weight: bold;
            background-color: #217C9D;
            border-radius: 50%;
        }

        .square-box {
            border: 1px solid #999;
        }

        .description-box {
            margin-left: 10px;
            padding: 5px;

        }

        small {
            margin-left: 10px;
            color: #222;
        }

        .border {
            border: 1px solid #ccc;
        }

        .items {
            padding-left: 0px;
            padding-right: 0px;
        }

        .heading {
            background-color: #217C9D;
            /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1f6591+1,2492a5+100 */
            background: #1f6591; /* Old browsers */
            background: -moz-linear-gradient(top, #1f6591 1%, #2492a5 100%); /* FF3.6-15 */
            background: -webkit-linear-gradient(top, #1f6591 1%, #2492a5 100%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, #1f6591 1%, #2492a5 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#1f6591', endColorstr='#2492a5', GradientType=0); /* IE6-9 */
            padding: 6px;
            color: #fff;
            font-size: 16px;
            font-weight: bold;
        }

        .block-info {
            padding: 10px;
            border-bottom: 1px solid #ccc;
            padding-bottom: 30px;
        }

        .width {
            width: 95%;
            margin-left: 5%;
        }

        h5 {
            font-weight: bold;
            color: #555;
        }

        .fb-share-button a {
            color: #fff;
            padding: 6px;
            background-color: #4267b2;
            margin: 5px;
            border-radius: 5px;
            color: #fff;
        }


    </style>




    <div class="header-margin py-5">
        <div class="container">
            @include('dealer.includes.nav', array(
                'tab' => 'craiglist',
                'page_title' => 'Social Media Assistant'
            ))

            <div class="row bg-secondary">
                <div class="col-md-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item"><a class="nav-link active tab_phone_leeds_btn phone" data-toggle="tab" href="#tab_phone_leeds" role="tab" aria-selected="false">Facebook Assistant</a></li>
                        <li class="nav-item"><a class="nav-link tab_email_leeds_btn" data-toggle="tab" href="#Criagslist" role="tab" aria-selected="false">Listings to Criagslist </a></li>
                    </ul>
                    <div class="tab-content card" id="myTabContent">
                        <div class="card-body tab-pane fade" id="Criagslist" role="tabpanel"
                             aria-labelledby="tab_trending_local_searches">

                            <br>

                            <div>

                                <ol class="nice-ol">
                                    <li>Go to <a href="#"><b>Criaglist</b></a> and navigate to a city closest to you.
                                    </li>
                                    <li>Select the <b>post to classified </b> link in the upper left comer of the page.
                                    </li>
                                    <li>Select <b>for sale by dealer</b> and then select <b>cars &amp; trucks.</b></li>
                                    <li>Find the listings you want to post from the list below. Download the cover and
                                        use the copy-paste text to help you save time typing. <br> Click
                                        <b>continue </b> after filling out the form.
                                    </li>
                                    <li> Upload the cover photo then upload your listing pictures.</li>
                                    <li> Follow the rest of the on screen direction to preview and publish your
                                        listing.
                                    </li>
                                </ol>
                            </div>

                            <div class="items col-xs-12 col-sm-12 col-md-12 col-lg-12 border clearfix">

                                <div class="heading">Listings to Criaglist</div>

                                @if(!empty($response))

                                    @foreach($response->listings as $get_stats)

                                        <div class="info-block block-info clearfix">
                                            <div class="square-box pull-left">
                                                <span><img src="{{$get_stats->media->photo_links[0]}}" width="100"
                                                           height="70"></span>
                                            </div>

                                            <div class="description-box pull-left">
                                                <h5>{{$get_stats->build->year}}  {{$get_stats->build->make}}
                                                    - {{$get_stats->build->model}}
                                                    <small> Stock Number:
                                                    </small> {{(isset($get_stats->stock_no)) ? $get_stats->stock_no : 'N/A'}}
                                                </h5>
                                                <p><i class="fa fa-download" aria-hidden="true"></i> <a
                                                            href="{{$get_stats->media->photo_links[0]}}">Download cover
                                                        Photo </a> &nbsp; <i class="fa fa-copy" aria-hidden="true"></i>
                                                    <a href="#"> Get the copy paste text</a></p>
                                                </p>
                                            </div>

                                            <br><br>

                                        </div>

                                    @endforeach

                                @else
                                    <h3 align="center"> No Listings Found </h3>
                                @endif


                            </div>


                        </div>
                        <div class="card-body tab-pane fade show active" id="tab_phone_leeds" role="tabpanel"
                             aria-labelledby="tab_trending_local_searches">

                            <script>
                                (function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0';
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));
                            </script>


                            <div class="items col-xs-12 col-sm-12 col-md-12 col-lg-12 border clearfix">


                                <div class="heading">Listings to Facebook Assistant</div>

                                @if(!empty($response))

                                    @foreach($response->listings as $get_stats)

                                        <div class="row">

                                            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 clearfix">

                                                <div class="info-block block-info clearfix">
                                                    <div class="square-box pull-left">
                                                        <span><img src="{{$get_stats->media->photo_links[0]}}"
                                                                   width="100" height="70"></span>
                                                    </div>

                                                    <div class="description-box pull-left">
                                                        <h5>{{$get_stats->build->year}}  {{$get_stats->build->make}}
                                                            - {{$get_stats->build->model}}
                                                            <small> Stock Number:
                                                            </small> {{(isset($get_stats->stock_no)) ? $get_stats->stock_no : 'N/A'}}
                                                        </h5>
                                                        <p><i class="fa fa-download" aria-hidden="true"></i> <a
                                                                    href="{{$get_stats->media->photo_links[0]}}">Download
                                                                cover Photo </a> &nbsp; <i class="fa fa-copy"
                                                                                           aria-hidden="true"></i> <a
                                                                    href="#"> Get the copy paste text</a></p>
                                                        </p>
                                                    </div>


                                                    <br><br>

                                                </div>
                                            </div>

                                            <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2  clearfix">
                                                <br><br>
                                                <?php
                                                $title = urlencode('' . $get_stats->heading . '');
                                                $url = urlencode('https://dev.ottoguide.com/car/detail/' . $get_stats->id . '');
                                                $image = urlencode('' . $get_stats->media->photo_links[0] . '');
                                                ?>

                                                <div class="fb-share-button ">
                                                    <a onClick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?=$url?>&picture=<?=$image?>&title=<?=$title?>&quote=<?=$title?>&description=<?=$title?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');"
                                                       target="_parent" href="javascript: void(0)">
                                                        Share On Facebook !
                                                    </a>
                                                </div>

                                            </div>


                                        </div>





                                    @endforeach

                                @else
                                    <h3 align="center"> No Listings Found </h3>
                                @endif


                            </div>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>




    <!-- Message Reply-->




@endsection