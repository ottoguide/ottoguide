@extends('layouts.web_pages')
@section('header')
    @parent
<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
@endsection
	@section('content')<style>
	.padding{padding:10px;
	margin:10px;background-color:#eaf4fd;
		}
	td{
	vertical-align: middle;	
		
	}	
	small{
	font-size: 85%;
	}

 input[type=checkbox]{
	display: inline-block;
    *display: inline;
    vertical-align: middle;
    margin: 0;
    padding: 0;
    width: 16px;
    height: 16px;
    border: none;
    cursor: pointer;
	color:#fff;
    -webkit-background-size: 240px 24px;
    background-size: 240px 24px;
}
.table td {
    padding: 0.75rem;
    vertical-align: middle;
    border-top: 1px solid #dee2e6;
}
.card{
background-color: rgba(95, 193, 232, 0.25);
}
.card-header{
background-color: #5fc1e8;
color:#fff;	
}
</style>

<div class="header-margin py-5">
    <div class="container">
        @include('dealer.includes.nav', array(
            'tab' => '',
            'page_title' => 'Sub Dealer Signup'
        ))

		<br>
		
		<div class="message" width="50%" align="center">
                @if (session('message'))
                    <div class="alert alert-success" width="50%">
                        {{ session('message') }}
                    </div>
                @endif
				
				
				 @if (session('error'))
                    <div class="alert alert-danger" width="50%">
                        {{ session('error') }}
                    </div>
                @endif
				
				
            </div>

		<br>
		
		
        <div class="row">
            <div class="col-md-12 cover">
              
		
    <div class="container">    
      <div class="row">                    
        <div class="col-lg-8 offset-lg-2">
          @if($errors->any())
            <p class="alert alert-success">You have been sign up successfully please check your email for further process</p>
          @endif 
          <div class="card">
            <div class="card-header font-weight-bold fs18">Sign up form for sub dealer</div>
              <div class="card-body">
                 <div class="row">
                  <div class="col-sm-12">    
                   <form method="POST" action="{{route('create_subdealer')}}" id="signup-form">     
                    {{ csrf_field() }}                                
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="subdealer_name">Sub Dealer Name</label>
                         <input type="text" name="subdealer_name" class="form-control" placeholder="Cheverlet Vasila">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                         <label for="email">Email</label>
                         <input type="text" class="form-control" name="email" placeholder="Email">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                         <label for="dealer_phone">Phone number</label>
                         <input type="text" class="form-control" name="subdealer_phone" placeholder="125487">
                        </div>
                      </div>
                     
                     
                      
                      <div class="col-sm-6">
                        <div class="form-group">
                         <label for="password">Password</label>
                         <input type="password" class="form-control" name="password" placeholder="***">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                         <label for="confirm_password">Confirm Password</label>
                         <input type="password" class="form-control" name="confirm_password" placeholder="***">
                        </div>
                      </div>                      
                      <div class="col-sm-6" style="display: none;" id="signup-validate-loader">
                        <span style="margin:0 auto; color: #bf2f2f;" class="fa fa-spinner fa-spin"></span>
                      </div>

                      <div class="col-sm-12">
                        <div class="text-right form-group">
                         <label>&nbsp;</label>
                        <button data-form-id="signup-form" data-validate-url="/subdealer/register/validate" data-validate-loader="signup-validate-loader" type="button" class="btn-form-submit btn btn-primary rounded-0"><span class="fa fa-sign-in"></span> Sign Up</button>
                        </div>
                      </div>
                    </div>                   
                    </form>
                    <div class="alert alert-danger validate-error-msg" style="font-size:12px; display:none">
                      <ul></ul>
                    </div>
                  </div>
                </div>  
                <div class="border-top pt-3 mt-4 text-center text-dark fs14"><span class="fa fa-lock fc1 fs24"></span> We'll always protect your Auto Trader account and keep your details safe.</div>             
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>								
       
            </div>
        </div>
    </div>

</div>
@endsection

@section('web-footer')
    @parent
    
    
    

@endsection