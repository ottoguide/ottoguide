@extends('layouts.web_pages')
@section('content')
<script type="text/javascript">
  var register_url = "{{ route('register') }}";
  var login_url = "{{ route('login') }}";
</script>
  <div class="header-margin py-5">
    <div class="container">    
      <div class="row">                    
        <div class="col-lg-8 offset-lg-2">
          @if($errors->any())
            <p class="alert alert-success">You have been sign up successfully please check your email for further process</p>
          @endif 
          <div class="card">
            <div class="card-header font-weight-bold fs18">Sign up for dealer module</div>
              <div class="card-body">
                 <div class="row">
                  <div class="col-sm-12">    
                   <form method="POST" action="{{route('create_dealer')}}" id="signup-form">     
                    {{ csrf_field() }}                                
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="dealer_name">Dealer Name</label>
                         <input type="text" name="dealer_name" class="form-control" placeholder="Northtown Auto">
                        </div>
                      </div>
					  
					   <div class="col-sm-6">
                        <div class="form-group">
                          <label for="dealer_name">Dealer Contact Name</label>
                         <input type="text" name="dealer_contact_name" class="form-control" placeholder="John Doe">
                        </div>
                      </div>
					  
					  
                      <div class="col-sm-6">
                        <div class="form-group">
                         <label for="email">Email</label>
                         <input type="text" class="form-control" name="email" placeholder="Email">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                         <label for="dealer_phone">Phone number</label>
                         <input type="text" class="form-control" name="dealer_phone" placeholder="125487">
                        </div>
                      </div>
                     
                     
                      
                      <div class="col-sm-6">
                        <div class="form-group">
                         <label for="password">Password</label>
                         <input type="password" class="form-control" name="password" placeholder="***">
                        </div>
                      </div>
					  
					  
					 
					  
					  
					  
                      <div class="col-sm-6">
                        <div class="form-group">
                         <label for="confirm_password">Confirm Password</label>
                         <input type="password" class="form-control" name="confirm_password" placeholder="***">
                        </div>
                      </div>                      
                      <div class="col-sm-6" style="display: none;" id="signup-validate-loader">
                        <span style="margin:0 auto; color: #bf2f2f;" class="fa fa-spinner fa-spin"></span>
                      </div>
					  
					   <div class="col-sm-6" >
                      <input name="agreement" class="agreement form_controll" type="checkbox">  I accept the <a href="{{ url('dealer/terms') }}" target="_blank">terms & conditions </a>. 
                      </div>

                      <div class="col-sm-6">
                        <div class="text-right form-group">
                         <label>&nbsp;</label>
                          <button data-form-id="signup-form" data-validate-url="/dealer/register/validate" data-validate-loader="signup-validate-loader" type="button" class="btn-form-submit btn btn-primary rounded-0"><span class="fa fa-sign-in"></span> Sign Up</button>
                        </div>
                      </div>
                    </div>                   
                    </form>
                    <div class="alert alert-danger validate-error-msg" style="font-size:12px; display:none">
                      <ul></ul>
                    </div>
                  </div>
                </div>  
                <div class="border-top pt-3 mt-4 text-center text-dark fs14"><span class="fa fa-lock fc1 fs24"></span> We'll always protect your Otto Guide account and keep your details safe.</div>             
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	
	
	<script>
		$(document).ready(function () {
			
		$('.btn-form-submit').attr('disabled',true);
		
		var ckbox = $('.agreement ');
		
		$('.agreement').on('click',function () {
			if (ckbox.is(':checked')) {
				  $('.btn-form-submit').attr('disabled',false);
			} else {
				  event.stopPropagation();
				  $('.btn-form-submit').attr('disabled',true);
			}
		});
	});
	</script>
	
	
@endsection