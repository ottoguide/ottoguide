@extends('layouts.web_pages')
@section('content')
    <script type="text/javascript">
		var register_url = "{{ route('register') }}";
		var login_url = "{{ route('login') }}";
    </script>
    <div class="header-margin py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 offset-lg-4">
                    @if(session()->get('status'))
                        <p class="alert alert-info">{{session()->get('status')}}</p>
                    @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                     <div class="card">
						@if(Session::has('user_forget_password'))
                        <div class="card-header font-weight-bold fs18">Forgot Password for User</div>
                        @else
					   <div class="card-header font-weight-bold fs18">Forgot Password for dealer module</div>
						@endif
						<div class="card-body">
                            
							@if(Session::has('user_forget_password'))
							<form method="POST" action="{{url('/user/reset/password')}}" id="dealer-login-form">
                             @else
							<form method="POST" action="{{url('/dealer/reset/password')}}" id="dealer-login-form">
							 @endif	
							   {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                </div>

                                <div class="" style="display: none;" id="dealer-login-validate-loader">
                                    <span style="margin:0 auto; color: #bf2f2f;" class="fa fa-spinner fa-spin"></span>
                                </div>

                                <div class="text-right form-group">
                                    <label>&nbsp;</label>
                                    <button type="submit" class=" btn btn-primary rounded-0">
                                        <span class="fa fa-sign-in"></span> Forgot Password
                                    </button>
										
                                </div>
                            </form>
                            <div class="alert alert-danger validate-error-msg" style="font-size:12px; display:none">
                                <ul style="margin: 0; padding: 0;"></ul>
                            </div>
							
		
                            <div class="border-top pt-3 mt-4 text-center text-dark fs14"><span class="fa fa-lock fc1 fs24"></span> We'll always protect your OTTO Guide account and keep your details safe.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection