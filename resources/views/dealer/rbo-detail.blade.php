@extends('layouts.web_pages')
@section('content')

    <style>
	
        .card-inner {
            margin-left: 2rem;
        }

        .p-0 {
            padding-left: 0px;
        }

        .b-0 {
            margin-bottom: 0px;
        }

        .fa-star {
            color: #ffc107 !important;
        }

        .border-top {
            border-top: 1px solid #ccc;
            margin-bottom: 5px;

        }

        .twitter-share-button {
            margin-bottom: -5px;
        }

        .card-body {
            padding: 0.025rem;
        }

        #myModal {
            margin-top: 20%;
        }
 
		.nice-ul {
			position: relative;
			padding-left: 32px;
			list-style-type: none;
		}
		.nice-ul li {
			margin-bottom: 8px;
		}
		.nice-ul li:last-child {
			margin-bottom: 0;
		}
		.nice-ul li::before {
			content: "\2713";
			position: absolute;
			left: 0;
			padding: 2px 8px;
			font-size: 1em;
			color: #1C90F3;
		}
		 
		.nice-ol {
			position: relative;
			padding-left: 32px;
			list-style-type: none;
			margin-left: 5%;
		}
		.nice-ol li {
			counter-increment: step-counter;
			margin-bottom: 6px;
			padding: 8px;
		}
		.nice-ol li:last-child {
			margin-bottom: 0;
		}
		.nice-ol li::before {
			content: counter(step-counter);
			position: absolute;
			left: 0;
			padding: 3px 8px;
			font-size: 0.9em;
			color: white;
			font-weight: bold;
			background-color: #217C9D;
			border-radius: 50%;
		}
		.square-box{
		border:1px solid #999;		
		}
		.description-box{
		margin-left: 10px;
		padding: 5px;	
			
		}
		small{
		margin-left: 10px;	
		color:#222;
		}
		.border{
		border:1px solid #ccc;
		}
		.items{
		padding-left:0px;
		padding-right:0px;		
		}
		
		.heading{
		 background-color: #217C9D;
		/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1f6591+1,2492a5+100 */
		background: #1f6591; /* Old browsers */
		background: -moz-linear-gradient(top, #1f6591 1%, #2492a5 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #1f6591 1%,#2492a5 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #1f6591 1%,#2492a5 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1f6591', endColorstr='#2492a5',GradientType=0 ); /* IE6-9 */
		padding:6px;
		color:#fff;
		font-size: 16px;
		font-weight: bold;
		}
		
		.block-info{
		padding: 10px;
		border-bottom: 1px solid #ccc;
		padding-bottom: 30px;	
		}
		
		.width{
		width: 95%;
		margin-left:5%;		
		}
		
		h5{
		font-weight: bold;
		color:#555;	
		}
		
		.cxm-img IMG{
			border-radius: 5px;
		}
		
		hr{
		border-bottom: 1px solid #ccc;	
			
		}

		</style>

		
    <div class="header-margin py-5">
        
		<div class="container">
            @include('dealer.includes.nav', array(
                'tab' => 'rbo',
                'page_title' => 'RBO - Legal Policy '
            ))
            <br>

            <div class="message" width="50%" align="center">
                @if (session('message'))
                    <div class="alert alert-success" width="50%">
                        {{ session('message') }}
                    </div>
                @endif
            </div>

		<div class="row">
	
		
		<div class="col-sm-12 col-md-12 col-lg-12">
		  
            <div class="card-body">
			   <br>
              <div class="cxm-advert-item mb-3 pull-right" >            
                  <div class="form-row">
                    <div class="col-sm-4">
                      <div class="cxm-img">
                        <a href="#"><img class="img-fluid" src="@if(!empty($car_datas->media->photo_links[0]) > 0){{$car_datas->media->photo_links[0]}}@endif"></a>
                      </div>
                    </div>
                    <div class="col-sm-8">
                    <div class="cxm-content">
			   
                    <div>
					
					
			<div class="text-primary font-weight-bold fs18">@if(!empty($car_datas->heading) > 0){{$car_datas->heading}}@endif</div>
                   
				   <span class="fc1 fs24">Price: $ @if(!empty($car_datas->ref_price) > 0){{$car_datas->ref_price}}@endif</span></div>
				   
				   <ul class="cxm-facts fs12 bg-secondary p-2 rounded">
                        <li><span class="fa fa-modx text-primary"></span> 
				       @if(!empty($car_datas->build->year) > 0){{$car_datas->build->year}}@endif
												</li>
                        <li><span class="fa fa-barcode text-primary"></span> 
					    @if(!empty($car_datas->build->make) > 0){{$car_datas->build->make}}@endif 
						</li>
                        
						<li><span class="fa fa-road text-primary"></span> 
						@if(!empty($car_datas->miles) > 0){{$car_datas->miles}}@endif  miles

						</li>
                        <li><span class="fa fa-toggle-on text-primary"></span>
												Automatic
                       						</li>
											<br><br>
                        <li><span class="fa fa-spinner text-primary"></span> 
						
							@if(!empty($car_datas->build->engine) > 0){{$car_datas->build->engine}}@endif
                       						</li>
                        <li><span class="fa fa-fire text-primary"></span> 
							@if(!empty($car_datas->build->fuel_type) > 0){{$car_datas->build->fuel_type}}@endif
                       	</li>
                      </ul>
					  
					
					</div>
					
					<p>
					
					<h4>Privacy Policy</h4>
Protecting customer privacy is very important to TOYOTA Motor Corporation (hereafter referred to as TOYOTA). This page clearly outlines company policy to help customers understand how personal information is acquired, used, and protected on the TOYOTA Rent a Car website (hereafter referred to as "this website").
<br>
<b>Personal Information Provided by Our Customers</b> <br>

TOYOTA asks customers to give their name, date of birth, gender, address, phone number, email address, and driver’s license number in order for the company to provide information about our products and services. In addition, when providing information to customers, there are instances when we ask for personal details other than those listed above, but such non-essential information is only obtained after customer consent has been given.
<br>
<b>IP Address, Cookies, and Web Beacons </b> <br>

To manage this website, TOYOTA gathers the IP addresses used by customers’ computers when connected to the Internet. IP addresses collected by our Web server are used only for the appropriate and safe management and operation of services offered on this website, preventing unauthorized access and, in the event of a problem with our Web server, making it possible to quickly identify and remedy the cause. These IP addresses are never used or disclosed as personal information.


<br>
<b>SSL (Security)</b>
<br>
This website protects customer privacy by using SSL (Secure Sockets Layer, industry-standard security that encrypts information sent over the Internet) to ensure secure transmission of personal information via this website. Personal information is automatically encrypted before it is sent when customers use a browser compatible with SSL.
<br>
<b>Use of Personal Information</b><br>

The intended use of personal information is stated on every Web page where customers are expected to register it. TOYOTA never uses such information for purposes other than those stated.
					</p>
					
					
                    </div>
                  </div>          
                </div>
              <br>
              </div>
		 
             
		<br><br>
		
		   </div>

		 
	   <?php
	   $check_rbo_policy = DB::table('request_best_offer')->where('car_id',$car_datas->id)->where('user_id',$user_id)->where('legalpolicy',1)->first();
	   ?>		 
		
	  <div class="col-sm-7 col-md-7 col-lg-7">
	  </div>
	  @if(count($check_rbo_policy))	
	   <div class="col-sm-4 col-md-4 col-lg-4">
		<a href="{{route('rbo-quote-create', ['car_id' => $car_datas->id, 'user'=> encrypt($user_id),'rbo_id'=> $rbo_id])}}" class="btn btn-success btn-lg pull-right">Create Quote</a>
	   </div>
	   
	  @else
	 
      <div class="col-sm-2 col-md-2 col-lg-2">
	  <a href="{{route('rbo-accepted', ['car_id' => $car_datas->id, 'user'=> encrypt($user_id),'rbo_id'=> $rbo_id])}}" onclick="return confirm('Are you sure you would like Accept RBO Legal Policy?')" class="btn btn-success">Accept Privacy Policy</a>
	  </div>
	  
	  <div class="col-sm-2 col-md-2 col-lg-2">
	  <a href ="{{route('rbo-rejected', ['car_id' => $car_datas->id, 'user'=> encrypt($user_id)])}}"class="btn btn-danger"  onclick="return confirm('Are you sure you would like reject this Privacy Policy?')"> Reject Privacy Policy</a>
	  </div>
	  @endif	
	  
		
		</div>
		</div>
		<br><br>	
	  
	
		</div>

</div>

    
    <!-- Message Reply-->
   

 

@endsection