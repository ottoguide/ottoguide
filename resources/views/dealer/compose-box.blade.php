	@extends('layouts.web_pages')
	@section('header')
		@parent

	<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
	@endsection
	@section('content')<style>
	.padding{padding:10px;
	margin:10px;background-color:#eaf4fd;
		}
	td{
	vertical-align: middle;	
	}	
		
	.dataTables_scrollHead{
	display:none;
	}

   .table th, .table td {
    padding: 0.50rem;
    border-top: 0px solid #dee2e6; 
	}



 #data-table_paginate{
	margin-top: 3%;
	align: right;	
 }
	
  .bg-white{
	margin-bottom: 2%;
    padding:1%;
    border-top: 3px solid #127ba3;
	border-radius: 5px;	
  }
   .empty{
	margin-right: 5px;	
	}
	
	</style>
<div class="header-margin py-5">

    <div class="container bg-secondary">
        @include('dealer.includes.nav', array(
            'tab' => 'Mail-Box',
            'page_title' => 'Compose Email'
        ))

	 <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>	
        <div class="row">
		
	
		 <div class="col-md-2">
		 <a href="#" class="btn btn-primary btn-block margin-bottom">Compose Message</a>
		 <br>
		 
	
	 	<div class="card border-info mb-3">
        <ul class="list-group list-group-flush cxm-list-group">               
		<li class="list-group-item">

        <a href="{{url('dealer-mail-box.html?email=inbox')}}" data-original-title="" title=""><h5><b>Inbox</b></h5></a>
       
	   <div class="info-txt">view inbox emails</div> 
        <?php 
		$count_emails = DB::table('email_box_dealer')->where('dealer_id',Session()->get('dealer_id'))
	    ->where('message_by','client')->where('is_read',0)->where('is_deleted',0)->count('id');
		
		if($count_emails > 0){
		echo "<span class='badge badge-success'>";
		echo $count_emails; 
		echo"</span>";		
		 }
		else { }
		?> 
       </li>
		
		<!--<li class="list-group-item">
        <a href="{{url('dealer-mail-box.html?email=sent')}}" data-original-title="" title=""><h5><b>Sent</b></h5></a>
        <div class="info-txt">sent emails</div> 
		 <span class="badge badge-primary">
		<?php /* 
		 $count_sent_emails = DB::table('email_box_dealer')->where('dealer_id',Session()->get('dealer_id'))
	    ->where('message_by','dealer')->where('is_deleted',0)->count('id');
		 if(count($count_sent_emails)){
		 echo $count_sent_emails; 	
		 }*/
		?> 
		</span>
		
		</li> -->
		
		<li class="list-group-item">
        <a href="{{url('dealer-mail-box.html?email=trash')}}" data-original-title="" title=""><h5><b>Trash</b></h5></a>
        <div class="info-txt">deleted emails</div> 
		 <span class="badge badge-primary">
		<?php 
		 $count_delete_emails = DB::table('email_box_dealer')->where('dealer_id',Session()->get('dealer_id'))
	    ->where('is_deleted',1)->count('id');
		if(count($count_delete_emails)){
		 echo $count_delete_emails; 	
		}
		?> 
		</span>
		</li>  
	
		</ul>
	  </div>
			
		 </div>
		
        <div class="col-md-10">
			
		<form id="email" action="{{url('dealer-email-send.html')}}" method="post">
		 {!! Form::hidden('_token', csrf_token()) !!}
          <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
			  <label>To: </label>
			  <?php 
			   $users = DB::table('email_box_dealer')->where('dealer_id',Session()->get('dealer_id'))
			  ->join('users','email_box_dealer.client_id','=','users.id')
			  ->groupBy('users.name')
			  ->get();
			  ?>
               <select id="users" name="users" class="form-control">
				@if(count($users))
				@foreach($users as $user)	
				<option value="{{$user->id}}">{{$user->name}}</option>
				@endforeach
				@else
				<option disabled>User Not Found</option>	
				@endif
				</select>
              </div>
              <div class="form-group">
			   <label>Subject: </label>
                <input class="form-control"name="subject">
              </div>
              
			   <div class="form-group">
			   <label>Message: </label>
                <textarea class="form-control" name="message" style="height:300px; padding:15px;" placeholder=""></textarea>
              </div>
              <input type="hidden" name="dealer_id" value="{{$dealer_data['dealer_id']}}">
            
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i>Send</button>
              </div>
            </div>
			<br>
		<br>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
		
	   </form>  
           	
            </div>
        </div>
    </div>

</div>
@endsection

@section('web-footer')
    @parent
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
   
	
@endsection