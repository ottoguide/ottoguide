
  <table class="table table_strip">
   <form action="{{route('dealer-information-update')}}" method="post" id="edit_company_information_form">  
	{!! Form::hidden('_token', csrf_token()) !!}   
   <tbody>
   <h3>General Company Information</h3>
	<br>
	
	<tr>
    <td>
	<label><b>Dealer Name</b></label>
	<input type="text" class="form-control" value="{{$dealer_data->dealer_name}}" name="dealer_name"> 
	</td>
    
	<td>
	<label><b>Dealer Email</b></label>
	<input type="text" class="form-control" value="{{$dealer_data->dealer_email}}" name="dealer_email"> 
	</td>
	</tr>
	
	
	<tr>
	<?php                                                                        
	$dealer_emails = explode(',', $dealer_data->dealer_other_emails); 
	$i=2;
	?>
		
	@foreach($dealer_emails as  $dealer_email)
												
	@if(!empty($dealer_email))
	<td>
	<label><b>Dealer Email ({{$i++}})</b></label>
	<input type="text" class="form-control" value="{{$dealer_email}}" name="dealer_other_email[]"> 
	</td>
	
	@endif
												
	@endforeach
	
	</tr>
	
	
	
	<tr>
    <td>
	<label><b>Dealer Phone</b></label>
	<input type="text" class="form-control" value="{{$dealer_data->dealer_phone}}" name="dealer_phone"> 
	</td>
    
	<td>
	<label><b>Dealer Country </b></label>
	<input type="text" class="form-control" value="{{$dealer_data->dealer_country}}" name="dealer_country"> 
	</td>
	</tr>
	
	
	<tr>
    <td>
	<label><b>Dealer City</b></label>
	<input type="text" class="form-control" value="{{$dealer_data->dealer_city}}" name="dealer_city"> 
	</td>
    
	<td>
	<label><b>Dealer Street</b></label>
	<input type="text" class="form-control" value="{{$dealer_data->dealer_street}}" name="dealer_street"> 
	</td>
	</tr>
	
	
	<tr>
    <td>
	<label><b>Dealer Website</b></label>
	<input type="text" class="form-control" value="{{$dealer_data->dealer_website}}" name="dealer_website"> 
	</td>
    
	<td>
	<label><b>Dealer Zip Code</b></label>
	<input type="text" class="form-control" value="{{$dealer_data->dealer_zip}}" name="dealer_zip"> 
	</td>
	</tr>
	
	
	<tr>
    <td>
	<label><b>Dealer Latitude</b></label>
	<input type="text" class="form-control" value="{{$dealer_data->dealer_latitude}}" name="dealer_latitude"> 
	</td>
    
	<td>
	<label><b>Dealer Longitude</b></label>
	<input type="text" class="form-control" value="{{$dealer_data->dealer_longitude}}" name="dealer_longitude"> 
	</td>
	</tr>
	

  <tbody>
  </table>
      <button type="submit" class="btn btn-primary pull-right">Update Information</button>

    </form>