								<div class="row mt-5">
                                <div class="col-md-12">
                        

									<h4>Lead Email Addresses</h4>
									
                                    <p>When a customer fills out the <strong>Contact Dealer</strong> from on Otto Guide
                                        We will send the email on address.</p>
                                    
									<div class="emails_section">
                                        <form action="{{route('dealer-settings-update-email')}}" method="post" id="edit_email_form">
                                         <input type="hidden" name="dealer_email" class="email">
                                        {!! Form::hidden('_token', csrf_token()) !!}
                                        </form>
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Email Address</th>
                                                <th>Email Format</th>
                                                <th>For Inventory</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                            </thead>
                                        
										<tbody>
												<tr>
                                                <td class="edit_email_area">{{$dealer_data->dealer_email}}</td>
                                                <td>ADF</td>
                                                <td>New & Used</td>
                                                <th>No Known Issues</th>
                                                <th>
										<!--<button type="button" class="btn btn-success edit_email_btn">Edit Email</button>          
										<button type="button" class="btn btn-success edit_email_save_btn" style="display: none">Save Email</button>-->
											</th>
												
												</tr> 
												<?php                                                                        
												$dealer_emails = explode(',', $dealer_data->dealer_other_emails); 
												?>	
												
												<form action="{{route('dealer-email-update')}}" method="post" id="edit_email_form">
												
												{!! Form::hidden('_token', csrf_token()) !!}
												
												
												
												@foreach($dealer_emails as  $dealer_email)
												
												@if(!empty($dealer_email))
													
												<tr id="email">
                                                <td class="other_email"><input type="text" class="form-control" value="{{$dealer_email}}" name="other_email_address[]"></td>
												<input type="hidden" value="{{$dealer_email}}" class="get_other_email">
                                                <td>ADF</td>
                                                <td>New & Used</td>
                                                <th>No Known Issues</th>
                                                <th>
												<button type="button" class="btn btn-danger delete_email_btn pull-right">Delete</button>  &nbsp;&nbsp;                                        
								 
										   <!--<button type="button" class="btn btn-success edit_email_save_btn" style="display: none">Save Email</button>--> 
												
												</th>
												</tr>
												
												@endif
												
											    @endforeach


                                            </tbody>

											</table>
										
										<button type="button" class="btn btn-success add_email_btn">Add New Email</button>
											@if(!empty($dealer_email))
											<button type="submit" class="btn btn-primary edit_email_btn">Update Emails</button>
											@endif
										</div>
											</form>
                                    <div class="phone_section mt-4">
                                      
								<form action="{{route('dealer-settings-update-phone')}}" method="post">
						 
                                {!! Form::hidden('_token', csrf_token()) !!}
								
                             <div><strong>Primary Phone Number</strong></div>
                               <br> 
							 
							 
							 
							 <div class="row find_phone">
							
							<div class="col-md-11 col-sm-11 col-lg-11">	
							<tr>
							<input type="text" class="form-control get_primary_phone" name="dealer_primary_phone"  value="{{$dealer_data->dealer_phone}}"> 
							</tr>
							</div>
							
							<div class="col-md-1 col-sm-1 col-lg-1">	
							<button type="button" class="btn btn-danger  pull-left" disabled>Delete</button>   
							</div>
							
							</div>
							   <br> 
							 <div><strong>Other Phone Numbers</strong></div>
							   <br>
							
						     <?php $dealer_phone = explode(',', $dealer_data->dealer_other_phone); ?>
							
							@foreach($dealer_phone as $dealer_phones)
									
							@if(!empty($dealer_phones))
							
						    <div class="row find_phone">
							
							<div class="col-md-11 col-sm-11 col-lg-11">	
							<tr>
							<input type="text" class="form-control get_dealer_phone" name="dealer_phone[]"  value="{{$dealer_phones}}"> 
							</tr>
							</div>
							
							<div class="col-md-1 col-sm-1 col-lg-1">	
							<button type="button" class="btn btn-danger delete_phone_btn pull-left">Delete</button>   
							</div>
							
							</div>
							
							<br>
							
							@endif
							
							@endforeach
							
					   		<button type="button" class="btn btn-success add_phone_btn">Add New Phone</button>
							
							@if(empty($dealer_phones))@endif
                            <!--<button type="submit" class="btn btn-primary mt-2">Update Sales Phone Number </button><p></p>-->
							<button type="submit" class="btn btn-primary add_phone_btnn">Update Phone</button>
						
							

                                        </form>
                                    </div>
								

							
								
								

								  <div class="website_section mt-4">
								  
                                  <strong>Competition Radius </strong>
								  <br><br>
								  
		<select class="form-control" class="dealer_radius" name="dealer_radius">
         <option value="" disabled selected>Select Dealer Radius</option>
		<option value="10" <?php if(isset($dealer_data->radius)) { echo $dealer_data->radius == '10' ? 'selected' : ''; }?>>10 Miles</option>
        <option value="20" <?php if(isset($dealer_data->radius)) { echo $dealer_data->radius == '20' ? 'selected' : ''; }?>>20 Miles</option>
        <option value="30" <?php if(isset($dealer_data->radius)) { echo $dealer_data->radius == '30' ? 'selected' : ''; }?>>30 Miles</option>
        <option value="40" <?php if(isset($dealer_data->radius)) { echo $dealer_data->radius == '40' ? 'selected' : ''; }?>>40 Miles</option>
        <option value="50" <?php if(isset($$dealer_data->radius)) { echo $dealer_data->radius == '50' ? 'selected' : ''; }?>>50 Miles</option>

        </select>
			<input type="hidden" value="{{$dealer_data->dealer_id}}" class="dealer_id" name="dealer_id">						  
								  
                                   </div>	
							
					
                                  <div class="website_section mt-4">
                                        <form action="{{route('dealer-settings-update-website')}}"
                                              method="post"> 
											  {!! Form::hidden('_token', csrf_token()) !!}
                                            <strong>Website:</strong>
                                            <div>Website Url:</div>
                                            <input type="text" class="form-control" name="dealer_website"
                                                   value="{{$dealer_data->dealer_website}}">
                                           <br>
                                            <button type="submit" class="btn btn-primary">Save Settings</button>
                                        </form>
                                    </div>
                                </div>
                            </div>