	<?php 
	
	$subsription_packages = DB::table('subscriber_list')->where('subscriber_list.dealer_id',$dealer_data->dealer_id)
	
	->join('subscription_packages','subscriber_list.package_id','=','subscription_packages.id')
	
	->join('subscriber_activity','subscriber_list.dealer_id','=','subscriber_activity.dealer_id')
	
	->get();

	?>
	 

	 
<h3 align="center"> Your Subscribed Package </h3>
<table class="table table-striped">
           <thead>
            <tr>
            <th>Package Name</th>
            <th>Package Description</th>
            <th>Featured Ad</th>
            <th>Chats</th>
            <th>Lease/Purchase</th>
			<th>Days Limit</th>
			<th>Package Expiry</th>
			<th>Status</th>
            </tr>
           </thead>
                                        
			<tbody>
	
		@if(count($subsription_packages))
		
		@foreach($subsription_packages  as $package)
			
			<tr>
            <td><b>{{$package->title}}</b></td>
            <td>{{$package->description}}</td>
            <td>{{$package->featured_ad}}</</td>
            <td>{{$package->chats}}</</td>
			<td>{{$package->lease_purchase}}</</td>
			<td>{{$package->expire_days}}</td>
			<td>{{$package->package_expiry}}</td>
			<td>Subscribed</td>
            <tr>
		@endforeach	
		
		@else
		
		<h4 align="center"> Not Found Any Subscription </h4>
		
		@endif
	
	</tbody>	
												
	</table>
	
	<br>
	
	<h3 align="center"> Subscription Activity </h3>
	
	<table class="table">
           <thead>
            <tr>
            <th >Package Name</th>
            <th >Featured Ad Used</th>
            <th >Chats Used</th>
            <th >Lease/Purchase Used</th>
            </tr>
           </thead>
                                        
			<tbody>
	
		@if(count($subsription_packages))
		
		@foreach($subsription_packages  as $package)
			<tr>
            <td>{{$package->title}}</td>
            <td>{{$package->featured_ad_used}} Used</td>
            <td>{{$package->chats_used}} Used</td>
            <td>{{$package->lease_purchase_used}} Used</td>
            <tr>
		@endforeach	
		
		
			@else
		
	 <h4 align="center"> Not Found any activity detail</h4>
		
		@endif
	
	</tbody>	
												
	</table>
	
	
	