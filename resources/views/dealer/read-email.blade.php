@extends('layouts.web_pages')
@section('header')
    @parent
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
@endsection
@section('content')

    <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>

    <script type="text/javascript">
        //<![CDATA[
        bkLib.onDomLoaded(function () {
            nicEditors.allTextAreas()
        });
        //]]>
    </script>

    <style>
        .bg-white {
            margin-bottom: 2%;
            padding: 1%;
            border-top: 3px solid #127ba3;
            border-radius: 5px;
        }

        .modal-header-primary {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #ff7702;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        .modal-body {
            min-height: 300px;
            max-height: 400px;
            overflow: auto;
        }

        .table th, .table td {
            padding: 0.30rem;
            padding-left: 15px;

        }

        /* width */
        ::-webkit-scrollbar {
            width: 7px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #999;
            border-radius: 10px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #ff7702;
        }

        .card-header {
            padding: 0.40rem 0.75rem;
            background-color: #fff;
        }

        small {
            margin-right: 8px;
        }

        .padding {
            padding: 2%;
        }


	.client{
	  border-top:3px solid #7FBA5D;
	 }
	.dealer{
	border-top: 3px solid #127ba3;
	}


    </style>

    <div class="header-margin py-5">

        <div class="container">
            @include('dealer.includes.nav', array(
                'tab' => 'Mail-Box',
                'page_title' => 'Read Email'
            ))

		
		  <?php DB::table('email_box')->where('id',decrypt($_GET['email_id']))->update(array('read_by_dealer' => 1));	?>

            <div class="row bg-secondary">

                <div class="col-md-2">
                    <br>
                    <!--<a href="{{url('dealer-compose-email.html')}}" class="btn btn-primary btn-block margin-bottom">Compose
                        Message</a>-->
               
                    <div class="card border-info mb-3">
                        <ul class="list-group list-group-flush cxm-list-group">

                            <li class="list-group-item">

                                <a href="{{url('dealer-mail-box.html?email=inbox')}}" data-original-title="" title="">
                                    <h5><b>Inbox</b></h5></a>

                                <div class="info-txt">view inbox emails</div>
                                <?php
                                $count_emails = DB::table('email_box')->where('dealer_id', Session()->get('dealer_id'))
                                  ->where('message_by', 'client')->where('read_by_dealer', 0)->where('temp_delete_by_dealer', 0)->where('is_delete_by_dealer', 0)->count('id');
							
								//$total_emails_count = DB::table('email_box')->where('dealer_id', Session()->get('dealer_id'))
                                    //->where('message_by', 'client')->where('is_delete_by_dealer', 0)->where('temp_delete_by_dealer', 0)->count('id');
							
							
								 
								 DB::table('email_box')->where('reply_to',decrypt($_GET['email_id']))->update(array('read_by_dealer' => 1));	


                                 if ($count_emails > 0) {
                                    echo "<span class='badge badge-danger'>";
                                    echo $count_emails;
                                    echo "</span>";
                                } else{
                             
                                }
                                ?>
                            </li>

                           <!-- <li class="list-group-item ">
                                <a href="{{url('dealer-mail-box.html?email=sent')}}" data-original-title="" title="">
                                    <h5><b>Sent</b></h5></a>
                                <div class="info-txt">sent emails</div>
                                <?php /*
                                $count_sent_emails = DB::table('email_box')->where('dealer_id', Session()->get('dealer_id'))
                                    ->where('message_by', 'dealer')->where('is_read', 0)->where('temp_delete_by_dealer', 0)->where('is_delete_by_dealer', 0)->count('id');

                                if (count($count_sent_emails)) {
                                    echo "<span class='badge badge-info'>";
                                    echo $count_sent_emails;
                                    echo "</span>";
                                } */

                                ?>

                            </li>-->

                            <li class="list-group-item ">
                                <a href="{{url('dealer-mail-box.html?email=trash')}}" data-original-title="" title="">
                                    <h5><b>Trash</b></h5></a>
                                <div class="info-txt">deleted emails</div>
                              
		                            <?php
                                    $count_delete_emails = DB::table('email_box')->where('dealer_id', Session()->get('dealer_id'))
                                        ->where('temp_delete_by_dealer', 1)->count('id');
                                    if (count($count_delete_emails)) {
										 echo'<span class="badge badge-primary">';
										echo'</span>';
                                    }else{}
                                    ?>
		                        </span>
                            </li>
                        </ul>
                    </div>

                </div>

                <br>
                <br>

                <div class="col-md-9 bg-white {{$get_email_data->message_by}}">

                    <div class="box box-primary">
                        <div class="box-header with-border">

                        </div>
                        <br>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="mailbox-read-info">
                                <h6><b>Subject:</b> {{$get_email_data->subject}}</h6>
                                <h6><b>From:</b> <span style="background-color:#7FBA5D; border-radius:3px; padding-left:4px;padding-right:4px; color:#fff;">  {{$get_email_data->name}} </span>
                                    <span class="mailbox-read-time pull-right"><b>Date:</b> <?= date("d-m-Y", strtotime($get_email_data->email_date)) . "\n"; ?></span>
                                </h6>
                            </div>
                            <!-- /.mailbox-read-info -->
                            <br>
                            <!-- /.mailbox-controls -->
                            <div class="mailbox-read-message">
                                @if($get_email_data->message_by == "client")
                                    <p><b>Hello {{$get_email_data->dealer_name}},</b></p>
                                @else
                                    <p><b>Hello  {{$get_email_data->name}},</b></p>
                                @endif

                                <p>{!!$get_email_data->message!!}</p>

                            </div>
                            <hr>

                            <?php
                            $services_list = json_decode($get_email_data->services);
                            ?>

                            @if(count($services_list))
                                <div class="card refine-search">
                                    <div class="card-header font-weight-bold">
                                        <a data-toggle="collapse" href="#refineSearch" role="button"
                                           aria-expanded="false" aria-controls="refineSearch">
                                            <b>User Selected Services Details</b></a>
                                    </div>
                                    <div class="collapse show" id="refineSearch">
                                        <div class="">

                                            <table class="table services" style="margin: 0;">

                                                <?php
                                                foreach($services_list as $key=> $description) {
                                                ?>
                                                <tr>
                                                    <td colspan="2" width="40%">
                                                        <h6><i class="fa fa-caret-right"></i>&nbsp; <b>{{$key}}</b></h6>
                                                    </td>
                                                    <td colspan="3">
                                                        <h6>{{ucfirst($description)}}</h6>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </table>


                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <?php

                $get_replies = DB::table('email_box')->where('email_box.dealer_id', Session()->get('dealer_id'))
                    ->where('reply_to', $get_email_data->email_id)->orderBy('email_box.id', 'DESC')
                    ->join('users', 'client_id', '=', 'users.id')
                    ->join('dealer', 'email_box.dealer_id', '=', 'dealer.dealer_id')
                    ->select('email_box.id as email_id', 'users.id as user_id', 'email_box.dealer_id as dealer_id', 'dealer.dealer_id as dealer', 'client_id',
                        'image', 'name', 'subject', 'message', 'services', 'message_by', 'reply_to', 'is_read',
                        'email_box.created_at as email_date', 'users.created_at as user_date', 'dealer_name')
                    ->get();

                ?>

                @if(count($get_replies))

                    @foreach($get_replies as $get_reply)

                        <div class="col-md-2"></div>
                        <div class="col-md-9 col-xs-offset-1 bg-white {{$get_reply->message_by}} ">
                            <div class="card refine-search">
                                <div class="card-header">
                                    @if($get_reply->message_by == "client")
                                        <b>From:</b> <span style="background-color:#7FBA5D; border-radius:3px; padding-left:4px;padding-right:4px; color:#fff;"> {{$get_reply->name}} </span> <br>
                                        <b>To:</b> {{$get_reply->dealer_name}}  &nbsp; &nbsp;
                                    @else
                                        <b>From:</b> <span style="background-color:#127ba3; border-radius:3px; padding-left:4px;padding-right:4px; color:#fff;"> {{$get_reply->dealer_name}} </span><br>
                                        <b>To:</b> {{$get_reply->name}}  &nbsp; &nbsp;
                                    @endif
                                    <b>Subject:</b> {{$get_reply->subject}}</h6>
                                    <a data-toggle="collapse" href="#reply{{$get_reply->email_id}}" role="button"
                                       aria-expanded="false" class="pull-right" aria-controls="refineSearch">
                                        &nbsp; &nbsp;<small><?= date("F j, Y", strtotime($get_reply->email_date)) . "\n"; ?> </small>
                                    </a>
                                </div>


							<?php DB::table('email_box')->where('id',$get_reply->email_id)->update(array('read_by_dealer' => 1));	?>

                                <div class="collapse show" id="reply{{$get_reply->email_id}}">

                                    <div class="box-body padding">
                                        <!-- /.mailbox-controls -->
                                        <div class="mailbox-read-message">

                                            <p>{!!$get_reply->message!!}</p>

                                        </div>
                                        <hr>

                                        <!-- /.mailbox-read-message -->
                                    </div>


                                </div>
                            </div>
                        </div>

                @endforeach


            @endif
            <!-- /.mailbox-read-message -->

                <br>
                <div class="col-md-2"></div>
                <div class="col-md-9 col-xs-offset-1 bg-white message_reply">
                    <br>
                    <form class="form-horizontal" role="form" method="post" action="{{url('dealer-mail-reply.html')}}"
                          class="bg-white">
                        {!! Form::hidden('_token', csrf_token()) !!}
                        <div class="form-group">
                            <label for="message" class="col-sm-2 control-label"><b>Reply to email</b></label>
                            <div class="col-sm-12">
                                <textarea class="form-control" rows="5" name="message"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12 col-sm-offset-2">
                                <input id="submit" name="submit" type="submit" value="Send Message"
                                       class="btn btn-primary pull-right">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 col-sm-offset-2">
                            </div>
                        </div>

                        <input type="hidden" name="email_id" value="{{$get_email_data->email_id}}">
                        <input type="hidden" name="client_id" value="{{$get_email_data->client_id}}">
                        <input type="hidden" name="subject" value="{{$get_email_data->subject}}">
                        <input type="hidden" name="services" value="{{$get_email_data->services}}">
                    </form>
                </div>
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer -->
        </div>
        <!-- /. box -->
    </div>
    </div>

    <!-- CAR SERVICES MODAL -->

@endsection



@section('web-footer')
    @parent


    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>



@endsection