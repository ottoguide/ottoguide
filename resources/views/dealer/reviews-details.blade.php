@extends('layouts.web_pages')@section('content')

 <link href="/css/reviews.css" rel="stylesheet">

    <div class="header-margin py-5">
        <div class="container">
		
            @include('dealer.includes.nav', array(
            'tab' => 'sales-review',
            'page_title' => 'Reviews Details'
            ))
		
		<style>
		.btn-warning{
			margin-left:4px;
		}
		</style>
			
            <div class="row">
             
	
	
			
		<div class="col-md-12">
			
			
		 <?php 
		
		 $messages = DB::table('client_reviews')
		
		->where('car_id',$car_detail->id)
		->where('reply_to','=',null)
		->orderby('client_reviews.id','desc')
		
		->join('users','client_reviews.client_id','=','users.id')
		 ->select('client_reviews.id as msg_id', 'users.id as user_id','car_id','dealer_id','client_id',
		 'image','name','title','message','message_by','reply_to','client_reviews.created_at as msg_date',
		 'users.created_at as user_created_at')	
		 
		 ->get();		  
		
		?>
			
			
			
		<ul class="comment-section">

		@if(count($messages))		 
			  
		@foreach($messages as $get_message) 
	
			<?php 
		
			$messages_replies = DB::table('client_reviews')
			->where('car_id',$car_detail->id)
			->where('reply_to','=',$get_message->msg_id)
			->orderby('client_reviews.id','ASC')
			
			->join('users','client_reviews.client_id','=','users.id')
			 ->select('client_reviews.id as msg_id', 'users.id as user_id','car_id',
			 'image','name','title','message','message_by','reply_to','client_reviews.created_at as msg_date',
			 'users.created_at as user_created_at','dealer_id','client_id')	
			 ->get();		  
		 
			?>
		
			<li class="comment {{$get_message->message_by}}">

                <div class="info">
                    <a href="#">{{$get_message->name}}</a>
                    <span><?= date("d-m-Y", strtotime($get_message->msg_date))."\n"; ?></span>
                </div>

                <a class="avatar" href="#">
                    <img src="{{$get_message->image}}" width="35" alt="Profile Pic" title="" />
                </a>

                <p>
				<span class="pull-right">
              Rating: 
			 <?php 
				$avg_count = DB::table('client_reviews')->where('car_id',$get_message->car_id)->avg('rating');
				
				
				if(empty($avg_count)){
				$avg_count = 0;	
				}
				$rt=round($avg_count);
				$img="";
				$i=1;
				while($i<=$rt){
				$img=$img."<img src=/public/images/star.png>";
				$i=$i+1;
				}
				while($i<=5){
				$img=$img."<img src=/public/images/star2.png>";
				$i=$i+1;
				}
				echo $img;
			?>
			  </span>
			<br>
					<b>{{$get_message->title}}</b>
			<br>
			
			
				{{$get_message->message}}
				
				<br>
				<br>
				
				<span>
				<a href="javascript:message_report('<?=$get_message->msg_id ?>','<?=$get_message->client_id ?>','<?= $get_message->car_id?>','<?= $get_message->dealer_id ?>');">
				&nbsp; &nbsp; &nbsp;
				<button type="button" class="btn btn-warning btn-sm pull-right">Report </button>
				</a>
				</span>
				
				
				<span>
				<a href="javascript:message_post_reply('<?=$get_message->msg_id ?>','<?=$get_message->client_id ?>','<?= $get_message->car_id?>','<?= $get_message->dealer_id ?>');">
				<button type="button" class="btn btn-success btn-sm pull-right">Reply </button>
				</a>
				</span>
				
							
				<span>
				<a data-toggle="collapse" class="view_reply pull-right" href="#message_reply_{{$get_message->msg_id}}" aria-expanded="false" aria-controls="collapseExample">
				&nbsp; &nbsp; &nbsp;
				<button type="button" class="btn btn-primary btn-sm pull-right">View <b>{{$messages_replies->count('id')}}</b> replies  <i class="fa fa-arrow-down fa-1"></i></button>
				</a>
				</span>
				
				
				</p>
				
			</li>
		
		
		 <div class="collapse" id="message_reply_{{$get_message->msg_id}}">

	     @if(count($messages_replies))		 
			 
		 @foreach($messages_replies as $messages_reply) 

		 
		 <?php 
			
			if($messages_reply->message_by =="client"){
				
			$name = $messages_reply->name;	
			
			}
			
			else{
			$dealer_name = DB::table('dealer')->where('dealer_id',$messages_reply->dealer_id)->get()->first();		
			
			$name = $dealer_name->dealer_name; 
			
			}
		  ?>		
			
		 
			<li class="comment {{$messages_reply->message_by}}">

                <div class="info">
                    <a href="#">{{$name}}</a>
                    <span><?= date("d-m-Y", strtotime($messages_reply->msg_date))."\n"; ?></span>
                </div>

               <a class="avatar" href="#">
                    <img src="{{$messages_reply->image}}" width="35" alt="Profile Pic" title="" />
               </a>
				<b></b>
              <p>{{$messages_reply->message}}<br></p>
				
			</li>
	
		  @endforeach
					
		  @endif	
		
		</div>
			
		
		
		
		@endforeach
			
		
		@else
		
		<h5 align="center">No reviews found of this vehicle</h5>
			
		@endif			
				
			
	
	
			
		</ul>

				
            </div>
        </div>   

		
	<!-- modal for Reply message -->
		
	   <div class="modal fade" id="message_reply_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Reply to review</h5>
                </div>
                <div class="modal-body">
				<p class="success_message" style="color:green; font-weight:bold;"></p>
				<textarea id="message_reply" name="message_reply" class="form-control"placeholder="write your comment here" required></textarea>		
                </div>
               
			   <div class="modal-footer">
			   <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>               
			   <button type="button" class="btn btn-primary reply_message">Post</button>
                </div>
            </div>
        </div>
    </div>	
		
		
		
		
		
	<!-- modal for Report Client -->	
		
	<div class="modal fade" id="message_report_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Write your complain to OttoGuide Administration </h5>
                </div>
                <div class="modal-body">
				<p class="success_message" style="color:green; font-weight:bold;"></p>
				<textarea id="message_report" name="message_report" class="form-control"placeholder="write your complain here"></textarea>		
                </div>
               
			   <div class="modal-footer">
			   <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>               
			   <button type="button" class="btn btn-primary report_message">Report</button>
                </div>
            </div>
        </div>
    </div>	
		
		
		
		
		<script>	
			
	function message_post_reply(msg_ID,client_ID,car_ID,dealer_ID){
		
	  $('#message_reply_modal').modal('show');
	   
	  $(document).on('click','.reply_message',function(){ 
		
	     var message_reply = $('#message_reply').val();
		 var car_id =  car_ID;
		 var dealer_id = dealer_ID;
		 var client_id = client_ID;
		 var message_id = msg_ID;
		
		
		
		$.ajax({
			type:'POST',
			data: {
				"_token": "{{ csrf_token() }}",
				message: message_reply,
				message_id: message_id,
				car_id:car_id,
				client_id:client_id,
				dealer_id:dealer_id
			},

			
			
			url:'{{url('save/reply/dealer/ajax')}}',

			success:  function () {
			
		 	$('.success_message').html('Successfully Replied');

		 setTimeout(function(){
			$('#message_reply_modal').modal('hide');
			}, 2000); 
			
			location.reload(); 
				
			}
			
		});

	});
			 
  }	
	

	function message_report(msg_ID,client_ID,car_ID,dealer_ID){	
		
	 $('#message_report_modal').modal('show');
	
	 $(document).on('click','.report_message',function(){ 
	 
	     var message_report = $('#message_report').val();
		 var car_id =  car_ID;
		 var dealer_id = dealer_ID;
		 var client_id = client_ID;
		 var message_id = msg_ID;
	
	
		$.ajax({
			type:'POST',
			data: {
				"_token": "{{ csrf_token() }}",
				message: message_report,
				message_id: message_id,
				car_id:car_id,
				client_id:client_id,
				dealer_id:dealer_id
			},

			url:'{{url('report/message/ajax')}}',

			success:  function (message) {
			
		 	$('.success_message').html(message);

		 setTimeout(function(){
			$('#message_report_modal').modal('hide');
			}, 4000); 
			
			location.reload(); 
				
			}

		});

	});
		
	}	
		
		
	</script>
	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

 
    </div>
@endsection