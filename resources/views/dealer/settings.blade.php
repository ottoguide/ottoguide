@extends('layouts.web_pages')@section('content')
    <div class="header-margin py-5">
        <div class="container">

            @include('dealer.includes.nav', array(
            'tab' => 'settings',
            'page_title' => 'Dealership Settings'
            ))
		<style>
		.input{
			
		width: 85%;	
		}
		</style>
			
            <div class="row bg-secondary">
                
			<div class="col-md-12">
          
		   <ul class="nav nav-tabs" id="myTab" role="tablist">
           
		    <li class="nav-item">
			<a class="nav-link active tab_email_leeds_btn" data-toggle="tab" href="#tab_trending_local_searches" role="tab" aria-selected="false">General Setting</a>
			</li>
                        
			<li class="nav-item">
			<a class="nav-link tab_phone_leeds_btn phone" data-toggle="tab" href="#company-information" role="tab" aria-selected="false">Company General Information</a>
			</li>
            
			<!--<li class="nav-item">
			<a class="nav-link tab_sms_leeds_btn" data-toggle="tab" href="#Communication-Setting" role="tab" aria-selected="false">Communication Setting</a>
			</li>
            
			<li class="nav-item">
			<a class="nav-link tab_chat_leeds_btn " data-toggle="tab" href="#User-Setting" role="tab" aria-selected="false">User Setting</a>
            </li>
                      
			<li class="nav-item">
			<a class="nav-link tab_chat_leeds_btn " data-toggle="tab" href="#Manager-setting" role="tab" aria-selected="false">Manager Setting</a>
			</li>-->
			
            {{--<li class="nav-item">--}}
			{{--<a class="nav-link tab_chat_leeds_btn" data-toggle="tab" href="#subscription-setting" role="tab" aria-selected="false">Subscription Setting</a>--}}
			{{--</li>--}}
       
			</ul>
                    
					
			 @if ($errors->any())
              <div class="alert alert-danger">
               <ul>
              @foreach ($errors->all() as $error)

               <li>{{ $error }}</li>
                 @endforeach
               </ul>
               </div>
             
  			   @endif		           
					
					
					
					
					<div class="tab-content card" id="myTabContent">                   
				
						<div class="card-body tab-pane fade show active" id="tab_trending_local_searches"
                             role="tabpanel" aria-labelledby="tab_trending_local_searches">
                           
							 @include('dealer.settings.general-setting')
                        </div>
                       
					   <div class="card-body tab-pane fade" id="company-information" role="tabpanel"
                             aria-labelledby="tab_trending_local_searches">
							 
							 
							 @include('dealer.settings.company-information')
							 
							 
							 </div>
							 
                        <div class="card-body tab-pane fade" id="Communication-Setting" role="tabpanel"
                             aria-labelledby="tab_trending_local_searches">
							 <h3>Communication Setting</h3>
                                <p>Coming Soon...</p>
							 </div>
							 
                        <div class="card-body tab-pane fade" id="User-Setting" role="tabpanel"
                             aria-labelledby="tab_trending_local_searches">
							 <h3>User Setting</h3>
                                <p>Coming Soon...</p>
						</div>
						
						
								 
                        <div class="card-body tab-pane fade" id="Manager-setting" role="tabpanel"
                             aria-labelledby="tab_trending_local_searches">
							 <h3>Manager Setting</h3>
                                <p>Coming Soon...</p>
						</div>
						
						
								 
                        <div class="card-body tab-pane fade" id="subscription-setting" role="tabpanel"
                             aria-labelledby="tab_trending_local_searches">
							 
							<br>
					 @include('dealer.settings.subscription-setting')
							 
						</div>
					 
                    </div>
                </div>
            </div>
        </div>   

		

		<!-- Lightbox for add email -->
		
        <div class="modal fade" id="add_email_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">Add Email</h4></div>
                    <div class="modal-body">
                      
					  <form action="{{route('dealer-settings-add-email')}}" method="post" id="add_email_form">
						 {!! Form::hidden('_token', csrf_token()) !!}
                            <div class="form-group"><label>Email Address</label>
                                <input type="text" class="form-control" name="email_address">
							</div>
                           
						   <div class="form-group"><label>Email Format</label>
                                <input type="text" class="form-control" value="ADF" name="email_format">
							</div>
                            
							<div class="form-group">
                                <label>For Inventory</label>
                                <select name="" class="form-control">
                                    <option value="new_and_used">New & Used</option>
                                    <option value="new">New</option>
                                    <option value="used">Used</option>
                                </select>
								
								</div>
                        </form>
						
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success" form="add_email_form">Save</button>
                    </div>
                </div>
            </div>
			</div> <!-- /.modal -->
		
		
		
		<!-- Lightbox for edit emails -->
		
		  <div class="modal fade" id="edit_email_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">Update Email</h4></div>
                    <div class="modal-body">
					 
					 <form action="{{route('dealer-settings-add-email')}}" method="post" id="edit_email_form">
						 {!! Form::hidden('_token', csrf_token()) !!}
                           <div class="form-group">
						   <label><b>Email Address</b></label>
                           
						<?php                                                                        
						$dealer_emails = explode(',', $dealer_data->dealer_other_emails); 
						?>	
												
						@foreach($dealer_emails as  $dealer_email)
												
						@if(!empty($dealer_email))
						   
						<input type="text" class="form-control" value="{{$dealer_email}}" name="other_email_address[]">
						<br>
						
						@endif
						
						@endforeach
							
							
							</div>
                           
						 
                        </form>
						
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success" form="edit_email_form">Update</button>
                    </div>
                </div>
            </div>
			</div> <!-- /.modal -->
		
		
		
		
		
		
		<!-- Lightbox for add PHONE -->
		
        <div class="modal fade" id="add_phone_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">Add Phone</h4></div>
                    <div class="modal-body">
                        <form action="{{route('dealer-settings-add-phone')}}" method="post" id="add_phone_form">
						 {!! Form::hidden('_token', csrf_token()) !!}
                            
                            <div class="form-group"><label>Phone Number</label>
                                <input type="text" class="form-control" name="dealer_other_phone"></div>
                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success" form="add_phone_form">Save</button>
                    </div>
                </div>
            </div>
        </div> <!-- /.modal -->
	
	
	
	
	
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

  
	<script>
	$('select').on('change', function() {
		
	  var radius = this.value;
	  var dealer = $('.dealer_id').val();

			$.ajax({
				type:'POST',
				data: {
				"_token": "{{ csrf_token() }}",	
				 radius: radius,
				 dealer_id:dealer
				},
				
				url:'{{url('dealer_add_radius.html')}}',
				
				success:  function () {
				alert('Dealer Radius Added Successfully');
				}
				
		}); 
	 
	});
	</script> 
	
	
			<script>
           
		   $(document).on('click', '.add_email_btn', function (e) {
                e.stopImmediatePropagation();
                $('#add_email_modal').modal({backdrop: 'static', keyboard: false, show: true}); // open lightbox
            });
			
			 $(document).on('click', '.add_phone_btn', function (e) {
                e.stopImmediatePropagation();
                $('#add_phone_modal').modal({backdrop: 'static', keyboard: false, show: true}); // open lightbox
            });
			
			
			
			

			// DELETE Email
			
			$(document).on('click', '.delete_email_btn', function (e) {
			
			e.stopImmediatePropagation();
		
			var other_email  = $(this).parents('tr').find(".get_other_email").val();
		
			$.ajax({
			type:'POST',
			data: {
			"_token": "{{ csrf_token() }}",	
			 email: other_email
            },
			url:'{{url('delete_other_emails.html')}}',
			
			success:  function () {
			location.reload();
			}
			
			});
		
			});
	
			
			
			// delete phone
			
			$(document).on('click', '.delete_phone_btn', function (e) {
			
			e.stopImmediatePropagation();
		
			var phone = $(this).parents('.find_phone').find(".get_dealer_phone").val();
		  
			$.ajax({
			type:'POST',
			data: {
			"_token": "{{ csrf_token() }}",	
			 phone: phone
            },
			url:'{{url('delete_phone.html')}}',
			
			success:  function () {
			location.reload();
			}
			
			}); 
		
		    });
	
            // $(document).on('click', '.edit_email_btn', function (e) {
            // e.stopImmediatePropagation();
            // var email = '{{$dealer_data->dealer_email}}';
            // $('.edit_email_area').html('<input type="text" class="form-control edit_email" value="'+email+'">');
            // $('.edit_email_btn').hide();
            // $('.edit_email_save_btn').show();
            // });
            // $(document).on('click', '.edit_email_save_btn', function (e) {
            // e.stopImmediatePropagation();
            // var email = $(this).parents('tr').find('.edit_email').val();
            // $('#edit_email_form .email').val(email);
            // $('#edit_email_form').submit();
            // $('.edit_email_area').html(email);
            // $('.edit_email_btn').show();
            // $('.edit_email_save_btn').hide();
            // });
        </script>
    </div>
@endsection