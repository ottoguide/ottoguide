@extends('layouts.web_pages')
@section('header')
    @parent
<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
@endsection
@section('content')<style>
.padding{padding:10px;
margin:10px;background-color:#eaf4fd;
	}
td{
vertical-align: middle;	
	
}	
small{
font-size: 85%;
}

 input[type=checkbox]{
	display: inline-block;
    *display: inline;
    vertical-align: middle;
    margin: 0;
    padding: 0;
    width: 16px;
    height: 16px;
    border: none;
    cursor: pointer;
	color:#fff;
    -webkit-background-size: 240px 24px;
    background-size: 240px 24px;
}
.table td {
    padding: 0.75rem;
    vertical-align: middle;
    border-top: 1px solid #dee2e6;
}
</style>

<div class="header-margin py-5">
    <div class="container">
        @include('dealer.includes.nav', array(
            'tab' => 'rbo',
            'page_title' => 'Request Best Offer'
        ))

		<br>
		
		<div class="message" width="50%" align="center">
                @if (session('message'))
                    <div class="alert alert-success" width="50%">
                        {{ session('message') }}
                    </div>
                @endif
				
				
				 @if (session('error'))
                    <div class="alert alert-danger" width="50%">
                        {{ session('error') }}
                    </div>
                @endif
				
				
            </div>

		<br>
		
		
        <div class="row">
            <div class="col-md-12 cover">
               <br>
                    <div class="card-body tab-pane fade show active" id="tab_trending_local_searches" role="tabpanel" aria-labelledby="tab_trending_local_searches">
                        <table width="100%" id="data-table" class="table table-striped">
                            <thead>
                            <tr>
                                <th width="1%">#</th> 
								<th width="8%" class="text-center">RBO #</th>
                                <th>Car</th>
								<th>Car Title</th>
								<th>User Name</th>
                                <th>Date</th> 
								<th>Dealer Status</th>
								<th>Customer Status</th>
								<th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
           										
                

            </div>
        </div>
    </div>

</div>
@endsection

@section('web-footer')
    @parent
    
	<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    
	<script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    
		<script>
		
			$(document).on('change','.reviews_off',function(e){
		  
			 if ($('.reviews_off').is(':checked')) {	
			
			 if(confirm("User will not allow rate and review to this vehicle"))
			
			{
		
			var car_id =  $(this).parents('tr').find(".reviews_off").val();
			 
			
			//$(".reviews_off").attr("disabled", true); 
		
			$.ajax({
			type:'POST',
			data: {
				"_token": "{{ csrf_token() }}",
				car_id: car_id
		
			},

			url: '{{url('block/reviews/ajax')}}',
			
			success:  function() {			
				
			location.reload();
			
			alert("Reviews has been blocked successfully");
			
			
			}

		});
			
			 
			}
			  
			 }	
		  
	
	  });
		

	$(document).on('change','.reviews_on',function(e){
		  
			 if ($('.reviews_on').is(':checked')) {	
			
			 if(confirm("User will allow to rate and review to this vehicle"))
			
			{
		
			var car_id =  $(this).parents('tr').find(".reviews_on").val();
			 
		
			$.ajax({
			type:'POST',
			data: {
				"_token": "{{ csrf_token() }}",
				car_id: car_id
		
			},

			url: '{{url('unblock/reviews/ajax')}}',
			
			success:  function() {			
				
			location.reload();
			
			alert("Reviews has been unblocked successfully");
			
			
			}

		});
			
			 
			}
			  
			 }	
		  
	
	  });

		
		
		
		
		
	   </script>	
				
	
	<script>
        var oTable_state = $('#data-table').DataTable({
			processing:       true,
			serverSide:       true,
			aLengthMenu:      [20, 50, 100, 500],
			deferRender:      true,
			"scrollX":        true,
			"iDisplayLength": 20,
			"order":          [[0, "desc"]],
			
			ajax:'{{url('request-best-offer-table.html')}}',
			
			columns:[
				{data: 'DT_Row_Index', name: 'id'},
				{data: 'RBO#', name: 'RBO#'},
				{data: 'carimage', name: 'carimage'},
				{data: 'cartitle', name: 'cartitle'}, 
				{data: 'name', name: 'name'},
				//{data: 'name', name: 'name'},
				//{data: 'status', name: 'status'}, 
				{data: 'rbo_date', name: 'rbo_date'}, 
				{data: 'dealer_status', name: 'dealer_status'}, 
				{data: 'customer_status', name: 'customer_status'}, 
				{data: 'action', name: 'action'}, 
				 
				
			]
		});
		
	
	
		
		$(oTable_state.table().container()).removeClass('form-inline');
		$(oTable_phone.table().container()).removeClass('form-inline');
		$(oTable_sms.table().container()).removeClass('form-inline');
		$(oTable_chat.table().container()).removeClass('form-inline');



		$('.tab_email_leeds_btn').on('shown.bs.tab', function (e) {
			oTable_state.columns.adjust().draw();
		});
		$('.tab_phone_leeds_btn ').on('shown.bs.tab', function (e) {
			oTable_phone.columns.adjust().draw();
		});
		$('.tab_sms_leeds_btn').on('shown.bs.tab', function (e) {
			oTable_sms.columns.adjust().draw();
		});
		$('.tab_chat_leeds_btn ').on('shown.bs.tab', function (e) {
			oTable_chat.columns.adjust().draw();
		});
    </script>

@endsection