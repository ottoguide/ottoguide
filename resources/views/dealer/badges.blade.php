@extends('layouts.web_pages')
@section('content')

    <style>
	
        .card-inner {
            margin-left: 2rem;
        }

        .p-0 {
            padding-left: 0px;
        }

        .b-0 {
            margin-bottom: 0px;
        }

        .fa-star {
            color: #ffc107 !important;
        }

        .border-top {
            border-top: 1px solid #ccc;
            margin-bottom: 5px;

        }

        .twitter-share-button {
            margin-bottom: -5px;
        }

        .card-body {
            padding: 0.025rem;
        }

        #myModal {
            margin-top: 20%;
        }
 
		.nice-ul {
			position: relative;
			padding-left: 32px;
			list-style-type: none;
		}
		.nice-ul li {
			margin-bottom: 8px;
		}
		.nice-ul li:last-child {
			margin-bottom: 0;
		}
		.nice-ul li::before {
			content: "\2713";
			position: absolute;
			left: 0;
			padding: 2px 8px;
			font-size: 1em;
			color: #1C90F3;
		}
		 
		.nice-ol {
			position: relative;
			padding-left: 32px;
			list-style-type: none;
			margin-left: 5%;
		}
		.nice-ol li {
			counter-increment: step-counter;
			margin-bottom: 6px;
			padding: 8px;
		}
		.nice-ol li:last-child {
			margin-bottom: 0;
		}
		.nice-ol li::before {
			content: counter(step-counter);
			position: absolute;
			left: 0;
			padding: 3px 8px;
			font-size: 0.9em;
			color: white;
			font-weight: bold;
			background-color: #217C9D;
			border-radius: 50%;
		}
		.square-box{
		border:1px solid #999;		
		}
		.description-box{
		margin-left: 10px;
		padding: 5px;	
			
		}
		small{
		margin-left: 10px;	
		color:#222;
		}
		.border{
		border:1px solid #ccc;
		}
		.items{
		padding-left:0px;
		padding-right:0px;		
		}
		
		.heading{
		 background-color: #217C9D;
		/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1f6591+1,2492a5+100 */
		background: #1f6591; /* Old browsers */
		background: -moz-linear-gradient(top, #1f6591 1%, #2492a5 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #1f6591 1%,#2492a5 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #1f6591 1%,#2492a5 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1f6591', endColorstr='#2492a5',GradientType=0 ); /* IE6-9 */
		padding:6px;
		color:#fff;
		font-size: 16px;
		font-weight: bold;
		}
		
		.block-info{
		padding: 10px;
		border-bottom: 1px solid #ccc;
		padding-bottom: 30px;	
		}
		
		.width{
		width: 95%;
		margin-left:5%;		
		}
		
		h5{
		font-weight: bold;
		color:#555;	
		}
		</style>


		
    <div class="header-margin py-5">
        <div class="container">
            @include('dealer.includes.nav', array(
                'tab' => 'dealer-badges',
                'page_title' => 'Dealer Rating Badges'
            ))
            <br>

            <div class="message" width="50%" align="center">
                @if (session('message'))
                    <div class="alert alert-success" width="50%">
                        {{ session('message') }}
                    </div>
                @endif
            </div>

		<div class="row">
	
		<div class="col-md-12">
		<h3 align="center"><b>FREE for all dealers with inventory on OttoGuide</b></h3>
		<h4 align="center">You can now highlight Good/Great with our new Badges!</h4>
		</div>
		</div>
		<br><br>	
	    
		<div class="row">
		<div class="col-md-6">
		<img src="{{asset('images/ad.png')}}" class="img-responsive" width="500">
		</div>
		<div class="col-md-6">
		<br>
		<ol class="nice-ol">
		<li>FREE for all dealers with inventory.</li>
		<li>Enhanced trust with a valuable third-party endorsement.</li>
		<li>Only highlights Good/Great Deals.</li>
		<li>Close more effectively by showing customers that they getting a great deal.</li>
		<li>Badges are configurable to fit your website look and feel.</li>
	    </ol>
		</div>
		</div>
		
	
		
		<div class="row">
		<div class="col-md-6">
		<img src="{{asset('images/ad2.png')}}" class="img-responsive" width="500">
		</div>
		<div class="col-md-6">
		<br>
		<ol class="nice-ol">
		<li>FREE for all dealers with inventory.</li>
		<li>Enhanced trust with a valuable third-party endorsement.</li>
		<li>Only highlights Good/Great Deals.</li>
		<li>Close more effectively by showing customers that they getting a great deal.</li>
		<li>Badges are configurable to fit your website look and feel.</li>
	    </ol>
		</div>
		</div>
	
		</div>

</div>

    
    <!-- Message Reply-->
   

 

@endsection