@extends('layouts.web_pages')
@section('header')
	@parent
	<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
@endsection
@section('content')

	<div class="header-margin py-5">
		<div class="container">

			{{--show this alert when dealer are not subscribed for addon--}}

			@if(Session::has('message'))
				<div class="alert {{ Session::get('alert-class', 'alert-info') }} p-2 addon_subscription_alert" style="display: block">
					<a class="close" data-dismiss="alert" href="#">
						<span class="align-text-top small">&times;</span>
					</a>
					<p>{{ Session::get('message') }}</p>
				</div>

			@endif

			@include('dealer.includes.nav', array(
                'tab' => 'dashboard',
                'page_title' => 'Dashboard'
            ))

			<div class="row bg-secondary">
				<div class="col-md-12">

					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab"
							   href="#tab_interaction_summary" role="tab" aria-controls="home" aria-selected="true">Interaction Summary</a>
						</li>
						<li class="nav-item">
							<a class="nav-link tab_vehicle_details_btn" data-toggle="tab"
							   href="#tab_vehicle_details" role="tab" aria-controls="profile" aria-selected="false">Vehicle Details Page (VDP)</a>
						</li>
						<li class="nav-item">
							<a class="nav-link tab_search_result_btn" data-toggle="tab"
							   href="#tab_search_result" role="tab" aria-controls="contact" aria-selected="false">Search Result</a>
						</li>
						<li class="nav-item">
							<a class="nav-link tab_deals_btn" data-toggle="tab"
							   href="#tab_dealer_inside" role="tab" aria-controls="contact" aria-selected="false">Dealer Pricing</a>
						</li>
					</ul>
					<div class="tab-content card" id="myTabContent">
						<div class="card-body tab-pane fade show active" id="tab_interaction_summary" role="tabpanel" aria-labelledby="tab_interaction_summary">
							<h3>Interaction Summary</h3>

							<div class="row">
								<!--<div class="col-sm-2">
									<br>
									<h2>{{$total_interaction}}</h2>
									<p>Total interaction</p>

									<br>
									<h2>{{$total_interaction_past_30}}</h2>
									<p>Interaction (Past 30 Days)</p>

									<br>
									<h2>{{$total_interaction_past_7}}</h2>
									<p>Interaction (Past 7 Days)</p>

									<br>
									<h2>{{$total_interaction_past_6_months}}</h2>
									<p>Interaction (Past 6 Months)</p>
								</div>-->

								<div class="col-sm-12">
									<div id="interaction_chart"></div>
								</div>
							</div>
						</div>

						<div class="card-body tab-pane fade" id="tab_vehicle_details" role="tabpanel" aria-labelledby="tab_vehicle_details">

							<div class="row">
								<div class="col-sm-2">
									<br>
									<h2>
										<?php
										$sum_detail = 0;
										foreach($monthly_details as $detail_count){
											if(isset($detail_count))
												$sum_detail += $detail_count;
										}
										echo $sum_detail;
										?>
									</h2>
									<p>Total View Detail Result</p>
									<br>
									<h2>
										{{$current_month_details}}
									</h2>
									<p>View Detail This Month</p>
									<br>
									<h2>
										<?php
										$sum_detail = 0;
										foreach($monthly_details as $detail_count){

											if(isset($detail_count))
												$sum_detail += $detail_count;
										}
										echo $sum_detail;
										?>
									</h2>
									<p>Search Result 6 Months</p>
								</div>

								<div class="col-sm-10">
									<div id="monthly_details"></div>
								</div>
							</div>

							<table width="100%" id="data-table" class="table table-bordered table-striped table-hover">
								<thead>
								<tr class="info">
									<th>#</th>
									<th width="30%">Listing</th>
									<th>Model</th>
									<th>Stock</th>
									<th width="35%" align="center">Interaction</th>
									<th>On Site</th>
								</tr>
								</thead>
								<tbody></tbody>
							</table>

						</div>

						<div class="card-body tab-pane fade" id="tab_search_result" role="tabpanel" aria-labelledby="tab_search_result">

							<div class="row">
								<div class="col-sm-2">
									<br>
									<h2>
										<?php
										$sum = 0;
										foreach($monthly_views as $views_count){
											if(isset($views_count))
												$sum += $views_count;
										}
										echo $sum;
										?>
									</h2>
									<p>Total Search Result</p>
									<br>
									<h2>
										{{$current_month_views}}
									</h2>
									<p>Total Search This Month</p>
									<br>
									<h2>
										<?php
										$sum = 0;
										foreach($monthly_views as $views_count){
											if(isset($views_count))
												$sum += $views_count;
										}
										echo $sum;
										?>
									</h2>
									<p>Search Result 6 Months</p>
								</div>

								<div class="col-sm-10">
									<div id="monthly_views"></div>
								</div>
							</div>
						</div>

						<div class="card-body tab-pane fade" id="tab_dealer_inside" role="tabpanel" aria-labelledby="tab_dealer_inside">
							<div class="row">
								<div class="col-sm-2">
									<?php
									foreach ($deals_data as $deals) {
										if ($deals[0] != 'Deals') {
											echo '<br>';
											echo '<h2>'.$deals[1].'</h2>';
											echo '<p>'.$deals[0].'</p>';
										}
									}
									?>
								</div>
								<div class="col-sm-10">
									<div id="deals_chart"></div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
@endsection


@section('web-footer')
	@parent
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
	<script>

		var oTable = $('#data-table').DataTable({
			processing: true,
			serverSide: true,
			aLengthMenu: [20, 50, 100, 500],
			deferRender:    true,
			"scrollX": true,
			ajax: '{{url('get_dealer_stats.html')}}',
			columns: [
				{ data: 'DT_Row_Index', name: 'id' },
				{ data: 'title', name: 'title' },
				{ data: 'model', name: 'model' },
				{ data: 'stock', name: 'stock' },
				{ data: 'interaction', name: 'interaction' },
				{ data: 'days', name: 'days' },

			]
		});
		$(oTable.table().container()).removeClass('form-inline');

		$('.tab_search_result_btn').on('shown.bs.tab', function (e) {

			google.charts.load('current', {'packages':['bar']});
			google.charts.setOnLoadCallback(function () {
				var data = google.visualization.arrayToDataTable([
					['Months', 'Views'],
					<?php
					foreach($monthly_views as $key2=> $monthly_view){
						echo "['".date('F',strtotime($key2))."',".$monthly_view."],";
					}
					?>
				]);
				var options = {
					chart: {
						title: 'Car Search Result by Months',
						subtitle: 'Dealer Search Cars Monthly Views',
					},
					bars: 'vertical',
					vAxis: {format: 'decimal'},
					width: 840,
					height: 400,
					colors: ['#127ba3']
				};
				var chart = new google.charts.Bar(document.getElementById('monthly_views'));
				chart.draw(data, google.charts.Bar.convertOptions(options));
			});
		});

		$('.tab_vehicle_details_btn').on('shown.bs.tab', function (e) {
			google.charts.load('current', {'packages':['bar']});
			google.charts.setOnLoadCallback(function () {
				var data = google.visualization.arrayToDataTable([
					['Months', 'Details'],

					<?php foreach($monthly_details as $key=> $monthly_detail){
					echo "['".date('F',strtotime($key))."',".$monthly_detail."],";
				}
					?>
				]);
				var options = {
					chart: {
						title: 'Car Details Result by Months',
						subtitle: 'Cars View Details Monthly ',
					},
					bars: 'vertical',
					vAxis: {format: 'decimal'},
					width: 840,
					height: 400,
					colors: ['#127ba3']
				};
				var chart = new google.charts.Bar(document.getElementById('monthly_details'));
				chart.draw(data, google.charts.Bar.convertOptions(options));

				oTable.columns.adjust().draw();
			});
		});

		$('.tab_deals_btn').on('shown.bs.tab', function (e) {
			google.charts.load('current', {'packages':['corechart']});
			google.charts.setOnLoadCallback(function () {
				var data = google.visualization.arrayToDataTable(<?= json_encode($deals_data) ?>);

				var options = {
					title: 'Dealer Inside',
					height: 400,
					slices: {
						0: { color: '#4CAF50' },
						1: { color: '#8BC34A' },
						2: { color: '#FFC107' },
						3: { color: '#F44336' },
						4: { color: '#777' }
					}
				};
				var chart = new google.visualization.PieChart(document.getElementById('deals_chart'));
				chart.draw(data, options);
			});
		});

		google.charts.load('current', {'packages':['bar']});
		google.charts.setOnLoadCallback(function () {
			var data = google.visualization.arrayToDataTable([
				['Months', 'Phone Leads', 'SMS Leads', 'Email Leads', 'Web Clicks', { role: 'annotation' }],
				<?php
				foreach($interaction_chart as $key => $chart_data){
					echo "['". $key ."', ".$chart_data['phone_leads'].", ".$chart_data['sms_leads'].", ".$chart_data['email_leads'].", ".$chart_data['web_links'].", ''],";
				}
				?>
			]);
			var options = {
				chart: {
					title: 'Interactions by Months',
					//subtitle: 'Dealer Search Cars Monthly Views',
				},
				bars: 'vertical',
				vAxis: {format: 'decimal'},
				height: 400,
				isStacked: true,
				series: {
            0: { color: '#cc0000' },
            1: { color: '#ff6600' },
            2: { color: '#8bc34a' },
            3: { color: '#217C9D' },
          }
			};
			var chart = new google.charts.Bar(document.getElementById('interaction_chart'));
			chart.draw(data, google.charts.Bar.convertOptions(options));
		});

	</script>


@endsection