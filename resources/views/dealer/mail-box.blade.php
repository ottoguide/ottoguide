@extends('layouts.web_pages')
@section('header')
    @parent

 
@endsection
@section('content')
    <style>
        .padding {
            padding: 10px;
            margin: 10px;
            background-color: #eaf4fd;
        }

        td {
            vertical-align: middle;
        }

        .dataTables_scrollHead {
            display: none;
        }

        .table th, .table td {
            padding: 0.50rem;
            border-top: 0px solid #dee2e6;
        }

        input[type=checkbox] {
            display: inline-block;
            *display: inline;
            vertical-align: middle;
            margin: 0;
            padding: 0;
            width: 16px;
            height: 16px;
            border: none;`
            cursor: pointer;
            color: #fff;
            -webkit-background-size: 240px 24px;
            background-size: 240px 24px;
        }

        #data-table_paginate {
            margin-top: 3%;
            align: right;
        }

        .bg-white {
            margin-bottom: 2%;
            padding: 1%;
            border-top: 3px solid #127ba3;
            border-radius: 5px;
        }

        .empty {
            margin-right: 5px;
        }

        .fa-envelope {
            color: #28B62C;
        }

        .input-sm {
            display: none;
            font-size: 0px;
        }
		
	.is_read1 {
	  color: #999;
	}	
	.is_read0 {
	  color: #23a127;
	}	
	
		

    </style>
	
	
	<link rel="stylesheet" href="https://l-lin.github.io/font-awesome-animation/dist/font-awesome-animation.min.css">
	
	
    <div class="header-margin py-5">

        <div class="container bg-secondary">
            @include('dealer.includes.nav', array(
                'tab' => 'Mail-Box',
                'page_title' => ucfirst($_GET['email'])
            ))

            <div class="message" width="50%" align="center">
                @if (session('message'))
                    <div class="alert alert-success" width="50%">
                        {{ session('message') }}
                    </div>
                @endif
            </div>
            <div class="row">


                <div class="col-md-2">
                   <!-- <a href="{{url('dealer-compose-email.html')}}" class="btn btn-primary btn-block margin-bottom">Compose Message</a>-->
                    <br>
                    <div class="card border-info mb-3">
                        <ul class="list-group list-group-flush cxm-list-group">
                            <li class="list-group-item">

                                <a href="{{url('dealer-mail-box.html?email=inbox')}}" data-original-title="" title="">
                                    <h5><b>Inbox</b></h5></a>

                                <div class="info-txt">view inbox emails</div>
                                <?php
                                $count_emails = DB::table('email_box')->where('dealer_id', Session()->get('dealer_id'))
                                    ->where('message_by', 'client')->where('is_delete_by_dealer', 0)->where('temp_delete_by_dealer', 0)->where('read_by_dealer',0)->count('id');
						
								$total_emails_count = DB::table('email_box')->where('dealer_id', Session()->get('dealer_id'))
                                    ->where('message_by', 'client')->where('is_delete_by_dealer', 0)->where('temp_delete_by_dealer', 0)->count('id');
	

                                if ($count_emails > 0) {
                                    echo "<span class='badge badge-danger'>";
                                    echo $count_emails;
                                    echo "</span>";
                                } else{
                              
                                }
                                ?>
                            </li>

                            <!--<li class="list-group-item">
                                <a href="{{url('dealer-mail-box.html?email=sent')}}" data-original-title="" title="">
                                    <h5><b>Sent</b></h5></a>
                                <div class="info-txt">sent emails</div>
								
                                <span class="badge badge-info">
		                            <?php /*
                                    $count_sent_emails = DB::table('email_box')->where('dealer_id', Session()->get('dealer_id'))
                                        ->where('message_by', 'dealer')->where('is_delete_by_dealer', 0)->where('temp_delete_by_dealer', 0)->count('id');
                                    if (!empty($count_sent_emails)) {
                                        echo $count_sent_emails;
                                    }else{
                                        echo "0";
                                    }*/
                                    ?>
		                        </span>

                            </li>-->

                            <li class="list-group-item">
                                <a href="{{url('dealer-mail-box.html?email=trash')}}" data-original-title="" title="">
                                    <h5><b>Trash</b></h5></a>
                                <div class="info-txt">deleted emails</div>
                             
		                            <?php
                                    $count_delete_emails = DB::table('email_box')->where('dealer_id', Session()->get('dealer_id'))
                                        ->where('temp_delete_by_dealer', 1)->where('is_delete_by_dealer', 0)->count('id');
                                    if (!empty($count_delete_emails)) {
										
										   echo '<span class="badge badge-info">';
                                        echo $count_delete_emails;
										 echo'</span>';
                                    }
                                    else{
                                      
                                    }
                                    ?>
		                       
                            </li>

                        </ul>
                    </div>
                </div>

                <div class="col-md-10">
                    <form method="post" action="{{url('dealer-temp-delete.html')}}">
                    <input type="submit" name="submit" class="btn btn-primary btn-xs pull-right delete_button"
                           value="Delete" disabled="">
                    <?php
                    if($_GET['email'] == 'trash'){
                    if($count_delete_emails > 0){?>
                    <a href="{{url('dealer-empty-trash.html')}}"
                       class="btn btn-danger btn-xs pull-right empty">Empty</a>
                    <?php }} ?>

                    <input type="hidden" value="<?= $_GET['email'];?>" name="email_status" class="email_status">
                    <input type="hidden" id="delete_ids" name="delete_ids" class="delete_ids">

                        {!! Form::hidden('_token', csrf_token()) !!}
                        <div class="card-body tab-pane fade show active bg-white" id="tab_trending_local_searches"
                             role="tabpanel" aria-labelledby="tab_trending_local_searches">
                            <table width="100%" id="data-table" class="table table-striped table-hover">
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    </div>
@endsection

@section('web-footer')
    @parent
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>

    <script>

        $(document).on('change', '.delete_email', function () {

            $(".delete_button").attr("disabled", true);

            $('.delete_ids').val('');

            var selectedvalue = [];

            $(":checkbox:checked").each(function (e) {

                $(".delete_button").attr("disabled", false);

                selectedvalue.push($(this).val());

                $('.delete_ids').val(selectedvalue.join(","));

            });

        });

        var oTable_state = $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            aLengthMenu: [20, 50, 100, 500],
            deferRender: true,
            "bFilter": false,
            "bLengthChange": true,
            "scrollX": true,
            "iDisplayLength": 20,
            "order": [[0, "desc"]],


            ajax: '{{url('dealer-mail-list.html')}}' + location.search,

            columns: [
                //{data: 'DT_Row_Index', name: 'email_id'},
                {data: 'action', name: 'action'},
                {data: 'username', name: 'username'},
                {data: 'email', name: 'email'},
                {data: 'date', name: 'date'},
                {data: 'delete', name: 'delete'},

            ]
        });

        $(oTable_state.table().container()).removeClass('form-inline');
        $(oTable_phone.table().container()).removeClass('form-inline');
        $(oTable_sms.table().container()).removeClass('form-inline');
        $(oTable_chat.table().container()).removeClass('form-inline');


        $('.tab_email_leeds_btn').on('shown.bs.tab', function (e) {
            oTable_state.columns.adjust().draw();
        });
        $('.tab_phone_leeds_btn ').on('shown.bs.tab', function (e) {
            oTable_phone.columns.adjust().draw();
        });
        $('.tab_sms_leeds_btn').on('shown.bs.tab', function (e) {
            oTable_sms.columns.adjust().draw();
        });
        $('.tab_chat_leeds_btn ').on('shown.bs.tab', function (e) {
            oTable_chat.columns.adjust().draw();
        });
    </script>

@endsection