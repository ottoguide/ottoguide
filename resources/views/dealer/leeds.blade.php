@extends('layouts.web_pages')
@section('header')
    @parent

    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
@endsection
@section('content')<style>.padding{padding:10px;margin:10px;background-color:#eaf4fd;	}</style>
<div class="header-margin py-5">
    <div class="container">
        @include('dealer.includes.nav', array(
            'tab' => 'leads',
            'page_title' => 'Leads Statistics'
        ))

        <div class="row bg-secondary">
            <div class="col-md-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active tab_email_leeds_btn" data-toggle="tab"
                           href="#tab_trending_local_searches" role="tab" aria-selected="false">Email Leads</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab_phone_leeds_btn phone" data-toggle="tab"
                           href="#tab_phone_leeds" role="tab" aria-selected="false">Phone Leads</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab_sms_leeds_btn" data-toggle="tab"
                           href="#tab_sms_leeds" role="tab" aria-selected="false">SMS Leads</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab_chat_leeds_btn " data-toggle="tab"
                           href="#tab_chat_leeds" role="tab" aria-selected="false">Chat Leads</a>
                    </li>
                </ul>
                <div class="tab-content card" id="myTabContent">				 				   <!-- Search by filter -->					<!--					<div class="row padding">					<div class="col-md-5 col-lg-5"><label><b>From</b></label><input type="date" class="form-control"></div>								<div class="col-md-5 col-lg-5"><label><b>To</b></label><input type="date" class="form-control"></div>										<div class="col-md-2 col-lg-2"><br><button class="btn btn-small btn-primary">Filter</button>										&nbsp;					<button class="btn btn-small btn-primary">Export</button>					</div>					</div>-->									<br><br>
                    <div class="card-body tab-pane fade show active" id="tab_trending_local_searches" role="tabpanel" aria-labelledby="tab_trending_local_searches">
                        <table width="100%" id="data-table" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th width="4%">#</th>
                                <th>Date</th>
                                <th>Name</th>
                                <th>Email</th>																		<th>Phone</th>																		<th>Make</th>																		<th>Model</th>																		<th>Year</th>																		<th>VIN</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="card-body tab-pane fade" id="tab_phone_leeds" role="tabpanel" aria-labelledby="tab_trending_local_searches">                            <table width="100%" id="data-table2" class="table table-striped table-hover">                                <thead>                                <tr>                                    <th width="4%">#</th>                                    <th>Date</th>                                    <th>Name</th>                                    <th>Email</th>																		<th>Phone</th>																		<th>Make</th>																		<th>Model</th>																		<th>Year</th>																		<th>VIN</th>                                </tr>                                </thead>                                <tbody></tbody>                            </table>                        </div>											<div class="card-body tab-pane fade" id="tab_sms_leeds" role="tabpanel" aria-labelledby="tab_trending_local_searches">                            <table width="100%" id="data-table3" class="table table-striped table-hover">                                <thead>                                <tr>                                    <th width="4%">#</th>                                    <th>Date</th>                                    <th>Name</th>                                    <th>Email</th>																		<th>Phone</th>																		<th>Make</th>																		<th>Model</th>																		<th>Year</th>																		<th>VIN</th>                                </tr>                                </thead>                                <tbody></tbody>                            </table>                        </div>																								<div class="card-body tab-pane fade" id="tab_chat_leeds" role="tabpanel" aria-labelledby="tab_trending_local_searches">                            <table width="100%" id="data-table4" class="table table-striped table-hover">                                <thead>                                <tr>                                    <th width="4%">#</th>                                    <th>Date</th>                                    <th>Name</th>                                    <th>Email</th>																		<th>Phone</th>																		<th>Make</th>																		<th>Model</th>																		<th>Year</th>																		<th>VIN</th>                                </tr>                                </thead>                                <tbody></tbody>                            </table>                        </div>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection

@section('web-footer')
    @parent
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    <script>
        var oTable_state = $('#data-table').DataTable({
			processing:       true,
			serverSide:       true,
			aLengthMenu:      [20, 50, 100, 500],
			deferRender:      true,
			"scrollX":        true,
			"iDisplayLength": 20,
			"order":          [[1, "desc"]],
			ajax:             '{{url('leeds-stats_table.html')}}',
			columns:          [
				{data: 'DT_Row_Index', name: 'id'},
				{data: 'created_at', name: 'created_at'},
				{data: 'name', name: 'name'},
				{data: 'email', name: 'email'}, {data: 'phone', name: 'phone'}, {
					data: 'car_make',
					name: 'car_make'
				}, {data: 'car_model', name: 'car_model'}, {data: 'car_year', name: 'car_year'}, {
					data: 'car_vin',
					name: 'car_vin'
				},
			]
		});
		var oTable_phone = $('#data-table2').DataTable({
			processing:       true,
			serverSide:       true,
			aLengthMenu:      [20, 50, 100, 500],
			deferRender:      true,
			"scrollX":        true,
			"iDisplayLength": 20,
			"order":          [[1, "desc"]],
			ajax:             '{{url('leeds-phone-stats-table.html')}}',
			columns:          [{data: 'DT_Row_Index', name: 'id'}, {
				data: 'created_at',
				name: 'created_at'
			}, {data: 'name', name: 'name'}, {data: 'email', name: 'email'}, {
				data: 'phone',
				name: 'phone'
			}, {data: 'car_make', name: 'car_make'}, {data: 'car_model', name: 'car_model'}, {
				data: 'car_year',
				name: 'car_year'
			}, {data: 'car_vin', name: 'car_vin'},]
		});
		var oTable_sms = $('#data-table3').DataTable({
			processing:       true,
			serverSide:       true,
			aLengthMenu:      [20, 50, 100, 500],
			deferRender:      true,
			"scrollX":        true,
			"iDisplayLength": 20,
			"order":          [[1, "desc"]],
			ajax:             '{{url('leeds-sms-stats-table.html')}}',
			columns:          [{data: 'DT_Row_Index', name: 'id'}, {
				data: 'created_at',
				name: 'created_at'
			}, {data: 'name', name: 'name'}, {data: 'email', name: 'email'}, {
				data: 'phone',
				name: 'phone'
			}, {data: 'car_make', name: 'car_make'}, {data: 'car_model', name: 'car_model'}, {
				data: 'car_year',
				name: 'car_year'
			}, {data: 'car_vin', name: 'car_vin'},]
		});
		var oTable_chat = $('#data-table4').DataTable({
			processing:       true,
			serverSide:       true,
			aLengthMenu:      [20, 50, 100, 500],
			deferRender:      true,
			"scrollX":        true,
			"iDisplayLength": 20,
			"order":          [[1, "desc"]],
			ajax:             '{{url('leeds-chat-stats-table.html')}}',
			columns:          [{data: 'DT_Row_Index', name: 'id'}, {
				data: 'created_at',
				name: 'created_at'
			}, {data: 'name', name: 'name'}, {data: 'email', name: 'email'}, {
				data: 'phone',
				name: 'phone'
			}, {data: 'car_make', name: 'car_make'}, {data: 'car_model', name: 'car_model'}, {
				data: 'car_year',
				name: 'car_year'
			}, {data: 'car_vin', name: 'car_vin'},]
		});
		$(oTable_state.table().container()).removeClass('form-inline');
		$(oTable_phone.table().container()).removeClass('form-inline');
		$(oTable_sms.table().container()).removeClass('form-inline');
		$(oTable_chat.table().container()).removeClass('form-inline');



		$('.tab_email_leeds_btn').on('shown.bs.tab', function (e) {
			oTable_state.columns.adjust().draw();
		});
		$('.tab_phone_leeds_btn ').on('shown.bs.tab', function (e) {
			oTable_phone.columns.adjust().draw();
		});
		$('.tab_sms_leeds_btn').on('shown.bs.tab', function (e) {
			oTable_sms.columns.adjust().draw();
		});
		$('.tab_chat_leeds_btn ').on('shown.bs.tab', function (e) {
			oTable_chat.columns.adjust().draw();
		});
    </script>

@endsection