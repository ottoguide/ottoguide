@extends('layouts.web_pages')
@section('header')
    @parent
<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
@endsection
@section('content')<style>
.padding{padding:10px;
margin:10px;background-color:#eaf4fd;
	}
td{
vertical-align: middle;	
	
}	
small{
font-size: 85%;
}

 input[type=checkbox]{
	display: inline-block;
    *display: inline;
    vertical-align: middle;
    margin: 0;
    padding: 0;
    width: 16px;
    height: 16px;
    border: none;
    cursor: pointer;
	color:#fff;
    -webkit-background-size: 240px 24px;
    background-size: 240px 24px;
}
.table td {
    padding: 0.75rem;
    vertical-align: middle;
    border-top: 1px solid #dee2e6;
}
</style>

<div class="header-margin py-5">
    <div class="container">
        @include('dealer.includes.nav', array(
            'tab' => '',
            'page_title' => 'Sub Dealer List'
        ))

		<br>
		
		<div class="message" width="50%" align="center">
                @if (session('message'))
                    <div class="alert alert-success" width="50%">
                        {{ session('message') }}
                    </div>
                @endif
				
				
				 @if (session('error'))
                    <div class="alert alert-danger" width="50%">
                        {{ session('error') }}
                    </div>
                @endif
				
				
            </div>

		<br>
		
		
        <div class="row">
            <div class="col-md-12 cover">
              
                    <div class="card-body tab-pane fade show active" id="tab_trending_local_searches" role="tabpanel" aria-labelledby="tab_trending_local_searches">
                        <table width="100%" id="data-table" class="table table-striped">
                            <thead>
                            <tr>
                                <th width="1%">#</th>
                                <th width="25%">Name</th>
								<th width="25%">Email</th>
								<th width="15%">Phone</th>
                                <th width="15%">Created on</th> 
								<th class="text-center" width="20%">Action</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
           										
                

            </div>
        </div>
    </div>

</div>
@endsection

@section('web-footer')
    @parent
    
	<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    
	<script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    

	<script>
        var oTable_state = $('#data-table').DataTable({
			processing:       true,
			serverSide:       true,
			aLengthMenu:      [20, 50, 100, 500],
			deferRender:      true,
			"scrollX":        true,
			"iDisplayLength": 20,
			"order":          [[0, "desc"]],
			
			ajax:'{{url('subdealer-accounts-table.html')}}',
			
			columns:[
				{data: 'DT_Row_Index', name: 'id'},
				{data: 'name', name: 'name'},
				{data: 'email', name: 'email'}, 
				{data: 'phone', name: 'phone'},
				{data: 'created_at', name: 'created_at'}, 
				{data: 'action', name: 'action'}, 
				 
				
			]
		});
		
	
	
		
		$(oTable_state.table().container()).removeClass('form-inline');
		$(oTable_phone.table().container()).removeClass('form-inline');
		$(oTable_sms.table().container()).removeClass('form-inline');
		$(oTable_chat.table().container()).removeClass('form-inline');



		$('.tab_email_leeds_btn').on('shown.bs.tab', function (e) {
			oTable_state.columns.adjust().draw();
		});
		$('.tab_phone_leeds_btn ').on('shown.bs.tab', function (e) {
			oTable_phone.columns.adjust().draw();
		});
		$('.tab_sms_leeds_btn').on('shown.bs.tab', function (e) {
			oTable_sms.columns.adjust().draw();
		});
		$('.tab_chat_leeds_btn ').on('shown.bs.tab', function (e) {
			oTable_chat.columns.adjust().draw();
		});
    </script>

@endsection