@extends('layouts.web_pages')
@section('content')
<div class="header-margin py-4 bg-secondary">
    <div class="container">
      <div class="row">                    
        <div class="col-md-12 col-sm-12">
          <h1>Terms and Conditions</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="py-4">
    <div class="container">
      <div class="row">
        <div class="heading">Standard Terms and Conditions for Dealership</div>
        <div class="col-md-12 col-sm-12">
            <h6 style="font-weight: bold;">THESE STANDARD TERMS AND CONDITIONS FOR DEALERS ARE INCORPORATED BY REFERENCE INTO EACH DEALER AGREEMENT, AND CONSTITUTE A PART OF EACH DEALER AGREEMENT.  THE COMPANY (OTTOPGUIDE.COM)WILL PROVIDE DEALERS WITH NOT LESS THAN THIRTY (30) DAYS NOTICE OF THE EFFECTIVE DATE OF ANY SUCH MODIFICATIONS OR AMENDMENTS THAT WOULD CHANG THE TERMS AND CONDITIONS OF DEALERS PARTICIPATION IN ANY PROGRAM IN WHICH DEALER PARTICIPATES AT THE TIME A MODIFICATION OR AMENDMENTS IS POSTED.</h6>
                <p><h4 style="font-weight: bold;">DEFINITIONS</h4></p>
                    <ul>
                        <li>For purposes of this dealer agreement, the following terms shall have the meaning set forth below.</li>
                        <li>“Advertising Content” means all text, copy, graphics, and photographs included in an advertisement.</li>
                        <li>“Ottoguide Traffic” Means program offered to dealers that provides communication with a perspective prospect using the tools from Ottoguide.com</li>
                        <li>“Bundle” means lead packages in increments of 100</li>
                        <li>“Company” Means ottoguide.com</li>
                        <li>“Company’s Advertising guidelines and Requirements” means Company’s guidelines and requirements for the publishing of advertisements under local Dealer Advertising Program as announced, published or provided, and modified or revised from time to time, by Company.</li>
                        <li>“Company Website” means any website owned and operated by Company.</li>
                        <li>“Confidential Information” means the existence of this terms and conditions of this agreement. Each party’s trade secrets, business plan, strategies, methods and/or practices; software, technology, computer systems architecture and network configurations: Any and all information which is governed by now-existing or future non-disclosure agreement between the parties her to; any other information relating to either party that is not generally known to the public, including information about either part’s personnel, products, customers, financial information, marketing and pricing strategies, services or future business plans; and any and all analyses, compilations, studies, notes, emails, or any other materials prepared which contain or are based on Confidential information received from a disclosing party.</li>
                        <li>“Consumer” means an individual Consumer using internet-based automobile research. Information or communication services.</li>
                        <li>“Consumer Request” means a request made or submitted by a consumer pursuant to which a consumer indicates interest in communicating with dealer in any way.</li>
                        <li>“Dealer” means a retailer of vehicles and related automotive products and services that is a party to a dealer agreement.</li>
                        <li>“Dealer Agreement” means an agreement to terms and conditions set forth.</li>
                        <li>“Dealer Backend” means the backend maintained for dealers with respect to dealer participation with the Company.</li>
                        <li>“Duplicate Electronic Transmission Request” means and Electronic Transmission Consumer Request delivered to dealer by company within 30-day period immediately following the date of an earlier valid electronic transmission consumer request, as identified by the same phone number or email address; and for the same vehicle year make model.</li>
                        <li>“Intellectual Property Rights” mean patent, copyright, trademark, service mark, trade secrets or other intellectual property rights.</li>
                        <li>“Invalid consumer request” means any consumer request that is not a valid consumer request.</li>
                    </ul>

            <p><h4 style="font-weight: bold;">PROGRAM</h4></p></br>

            <h5 style="font-weight: bold;">GENERAL PROGRAM PROVISIONS</h5>
            <p>‘Applicable Provisions” The provisions of this article are applicable to the dealer backend a dealer has elected to participate the ottoguide program pursuant to the terms and conditions.</p></br>

            <p><h5 style="font-weight: bold;">COMPLIANCE WITH LAWS AND PROGRAM RULES</h5></p>

            <h6 style="font-weight: bold;">Compliance with Laws and Privacy Policies.</h6>
            <p>Each of the parties shall comply in all material respects with all laws, rules, regulations and orders applicable to such party performance obligation under Dealer Agreement.  Without limiting the generality of the foregoing, each of the parties shall comply with their applicable privacy policies and all applicable laws, rules, regulation, and orders governing the collection, confidentiality, use, and safeguarding of non-public individually identifiable personal information of consumers included in consumer request.  Dealer shall comply with the Company’s privacy policy in Dealer's use and disclosure of information provided by consumers that may be included in Consumer Request.</p>
            <p>Target number of consumer Request selected by dealer on a monthly basis as set forth in the Terms and Conditions.  Consumer request delivered under consumer request program in which a dealer participates are aggregated for purposes of determining monthly targets, and the Company will determine the quality and Mix of Consumer request delivered to Dealer.  The parties agree that the Monthly Targets are not minimum guarantees.</p>

            <h6 style="font-weight: bold;">General.</h6>
            <p>If dealer has elected to participate in the Pay-Per-Call- Minute and has approved of participation in dealer backend.  Company will host calls made to the dealer for the purpose of the request not for other marketing purposes.</p>

            <h6 style="font-weight: bold;">Dealer’s acknowledgment of call recording.</h6>
            <p>Dealer acknowledges that connected calls are recorded and allow for monitoring of service.  Dealer acknowledges and consents to all applicable laws, rules, and regulations when using the Companies phone connect program.</p>

            <h6 style="font-weight: bold;">Dealers Responsibility for All Advertising Content.</h6>
            <p>Dealer shall be responsible for reviewing and approving all advertising content prior to publishing.  Because the company only displays data directly from dealers website regular website audits are recommended by the Company. Dealer will honor all prices to be confirmed with RBO.</p>

            <h6 style="font-weight: bold;">Advertising Placements.</h6>
            <p>Company reserves the right to make placements of Dealer advertisement solely on Company Websites in the Company Advertising network (including Company Websites) as the company may determine from time to time in its sole discretion.</p>

            <h6 style="font-weight: bold;">BECAUSE THE TEXT PROGRAM IS ONLY A TOOL THAT FACILITATES TEXT COMMUNICATIONS BETWEEN DEALERS AND CONSUMERS, IN THE EVENT OF ANY TYPE OF DISPUTE OVER A PRODUCT OR SERVICE OFFERED BY A DEALER, DEALER AGREES THAT ANY RELIANCE ON TEXT CONTENT SUBMITTED IS AT THE DEALERS OWN RISK, AND THAT DEALER UNCONDITIONALLY RELEASES THE COMPANY, AND EACH OF THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES, AGENTS, SERVICE PROVIDERS, AND LICENSERS FOR ANY AND ALL CLAIMS, DEMANDS AND DAMAGES (ACTUAL AND CONSEQUENTIAL) OR EVERY KIND OF NATURE, KNOWN AND UNKNOWN, SUSPECTED AND UNSUSPECTED, DISCLOSED AND UNDISCLOSED, ARISING OUT OF OR IN ANY WAY CONNECTED WITH SUCH DISPUTE OR USE.</h6>

            <h6 style="font-weight: bold;">Use and Disclosure.</h6>
            <p>Each party agrees that it will use Confidential Information of the other party in accordance of Terms and Conditions and it will not disclose such information to any third party without the other parties written consent, dealer agrees to fore pay $1795 to be exhausted with each interaction by consumer with the sum considered confidential, except as otherwise permitted hereunder.  Each party may disclose Confidential information of the other party, in whole or in part to its employees, representatives, actual or potential investors and subcontractors who have a need to know and are legally bound to keep such information confidential and consistent with the terms of this section.</p>

            <h6 style="font-weight: bold;">Taxes and other fees excluded.</h6>
            <p>All, fees, charges and cost are exclusive of Federal, state and local excise, sales, use and other or assessments now and here after levied or imposed for the provision of the Program services.  Except for taxes on Company’s net income, Dealer shall be liable for and pay all taxes, assessments and levies, regardless of whether included on any invoice.

            <h6 style="font-weight: bold;">Operating cost and Expenses.</h6>
            <p>Other than program cost invoiced to Dealer, each party is responsible for all of its internal cost, it any, associated with implementation and operation of the Program.

            <h6 style="font-weight: bold;">Term and Termination.</h6>
            <p style="font-weight: bold;">Otto Guide can be canceled at any time with no waiting period using dealer backend tool.
        </div>
      </div>
    </div>      
  </div>
@endsection