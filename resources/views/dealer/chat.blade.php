	@extends('layouts.web_pages')
	@section('header')
		@parent

		<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
	@endsection
	@section('content')<style>
	.padding{padding:10px;
	margin:10px;background-color:#eaf4fd;
		}
	td{
	vertical-align: middle;	
		
	}	
	li.channel_list{
	width: 100%;
	background:#337ab7;
	font-size: 12px;
	list-style:none;
	padding: 6px;
	border-radius: 5px 0px 0px 5px;
	margin-top: 10px;
	margin-bottom:10px;
	color:#333;
	}
	li.channel_list a:hover{
		text-decoration: none;
		color:#000;
	}
	li.channel_list a{
		color:#fff;
	}
	
	
	
	li.channel_list_active{
	width: 100%;
	background:#7FBA5D;
	font-size: 12px;
	list-style:none;
	padding: 6px;
	border-radius: 5px 0px 0px 5px;
	margin-top: 10px;
	margin-bottom:10px;
	color:#333;
	}
	
	
	
	
	iframe{
		min-height: 200px;
		max-height: 810px;	
			
		}
		</style>
<div class="header-margin py-5">

    <div class="container">
        @include('dealer.includes.nav', array(
            'tab' => 'chat',
            'page_title' => 'Chat Communication'
        ))

		
		<h4 align="center" style="padding-top:10px; font-weight:bold;">
		
		{{$car_title}}
		
		</h4>
	
        <div class="row bg-secondary" style="border-top:1px solid #ccc;">
					
		
		
	 <?php $get_dealer_channels =  DB::table('twilio_chat')->where('dealer_id',session()->get('dealer_id'))->get();?>
	
	   @if(count($get_dealer_channels))
		
	
		<div class="col-md-2">
		
		<?php 		  

		 foreach($get_dealer_channels as $get_dealer_channel){
		  echo "<li class='channel_list'><a href='".url('chat-communication.html/?car_id='.$get_dealer_channel->car_id.'&channel_name='.$get_dealer_channel->chat_channel.'&dealer_id='.$get_dealer_channel->dealer_id.'')."'>".$get_dealer_channel->channel_display_name. "</a></li>";  
		 }
		
		 ?>
		</div>
		
		<?php 
		$get_dealer = DB::table('dealer')->where('dealer_id',session()->get('dealer_id'))->get()->first(); 
		?>
		
		
        <div class="col-md-10">
            
        <iframe src="conversation.html?id={{$get_dealer->dealer_name}}&channel_name=<?=(isset($_GET['channel_name'])?$_GET['channel_name']:'blank')?>" width="100%" scrolling="no" height="820" onload="resizeIframe(this)" frameborder="0"></iframe>
  
        </div>
	
		<script>
		  function resizeIframe(obj) {
			obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
		  }
		</script>
		
			@else
			<br><br>	
			<h5 align="center" style="margin:0px auto; margin-top: 10%; margin-bottom:10%; color:#999;"> Chat communication record not found </h5>  	
			<br><br>
			@endif		
			
        </div>
		
		
			
    </div>

</div>

<script>
$(function() {                       //run when the DOM is ready
  $(".channel_list").click(function() {  //use a class, since your ID gets mangled
    $(this).addClass("channel_list_active");      //add the class to the clicked element
  });
});
</script>


@endsection

@section('web-footer')
    @parent
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    


@endsection