@extends('layouts.web_pages')
@section('content')
    <script type="text/javascript">
	
		$(document).ready(function () {
			
		$('.btn-form-submit').attr('disabled',false);
		
			});
	
		var register_url = "{{ route('register') }}";
		var login_url = "{{ route('login') }}";
    </script>
    <div class="header-margin py-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 offset-lg-4">
                    @if(session()->get('status'))
                        <p class="alert alert-info">{{session()->get('status')}}</p>
                    @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <div class="card">
                        <div class="card-header font-weight-bold fs18">Login for dealer module</div>
                        <div class="card-body">
                            <form method="POST" action="{{route('login_submit')}}" id="dealer-login-form">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                </div>

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" placeholder="***">
                                </div>

                                <div class="" style="display: none;" id="dealer-login-validate-loader">
                                    <span style="margin:0 auto; color: #bf2f2f;" class="fa fa-spinner fa-spin"></span>
                                </div>

                                <div class="text-right form-group">
                                    <label>&nbsp;</label>
                                    <button data-form-id="dealer-login-form" data-validate-url="/dealer/login/validate"
                                            data-validate-loader="dealer-login-validate-loader"
                                            type="button" class="btn-form-submit btn btn-primary rounded-0">
                                        <span class="fa fa-sign-in"></span> Login
                                    </button>
                                </div>
                            </form>
                            <div class="alert alert-danger validate-error-msg" style="font-size:12px; display:none">
                                <ul style="margin: 0; padding: 0;"></ul>
                            </div>
							
							 <a href="{{url('dealer/forgot/password/')}}">Forgot Password</a>
							
                            <div class="border-top pt-3 mt-4 text-center text-dark fs14"><span class="fa fa-lock fc1 fs24"></span> We'll always protect your OTTO Guide account and keep your details safe.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection