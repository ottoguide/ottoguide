@extends('layouts.web_pages')
@section('header')
    @parent

    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"/>
@endsection
@section('content')<style>
.padding{padding:10px;
margin:10px;background-color:#eaf4fd;
	}
td{
vertical-align: middle;	
	
}	
	
	</style>
<div class="header-margin py-5">
    <div class="container">
        @include('dealer.includes.nav', array(
            'tab' => 'lease-purchase',
            'page_title' => 'Lease Purchase Offer'
        ))

        <div class="row bg-secondary">
            <div class="col-md-12">
               <br><br>
                    <div class="card-body tab-pane fade show active" id="tab_trending_local_searches" role="tabpanel" aria-labelledby="tab_trending_local_searches">
                        <table width="100%" id="data-table" class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th width="4%">#</th>
                                <th>Car</th>
								<th>Title</th>
								<th>Model</th>
								<th>Stock</th>
                                <th width="20%">Lease / Purchase Offers</th>
															
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
           										
                

            </div>
        </div>
    </div>

</div>
@endsection

@section('web-footer')
    @parent
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    
	<script>
        var oTable_state = $('#data-table').DataTable({
			processing:       true,
			serverSide:       true,
			aLengthMenu:      [20, 50, 100, 500],
			deferRender:      true,
			"scrollX":        true,
			"iDisplayLength": 20,
			"order":          [[0, "desc"]],
			
			ajax:'{{url('lease-purchase-table.html')}}',
			
			columns:[
				{data: 'DT_Row_Index', name: 'id'},
				{data: 'image', name: 'image'}, 
				{data: 'title', name: 'title'},
				{data: 'model', name: 'model'},
				{data: 'stock', name: 'stock'}, 
				{data: 'Offers', name: 'Offers'}, 
				
			]
		});
		
	
	
		
		$(oTable_state.table().container()).removeClass('form-inline');
		$(oTable_phone.table().container()).removeClass('form-inline');
		$(oTable_sms.table().container()).removeClass('form-inline');
		$(oTable_chat.table().container()).removeClass('form-inline');



		$('.tab_email_leeds_btn').on('shown.bs.tab', function (e) {
			oTable_state.columns.adjust().draw();
		});
		$('.tab_phone_leeds_btn ').on('shown.bs.tab', function (e) {
			oTable_phone.columns.adjust().draw();
		});
		$('.tab_sms_leeds_btn').on('shown.bs.tab', function (e) {
			oTable_sms.columns.adjust().draw();
		});
		$('.tab_chat_leeds_btn ').on('shown.bs.tab', function (e) {
			oTable_chat.columns.adjust().draw();
		});
    </script>

@endsection