
<!DOCTYPE html>

<html lang="en">

<head>
  <!-- Google Tag Manager Head -->
  @include('googletagmanager::head')
  <!-- Google Tag Manager Head -->
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="CXM">
  
  <title>OTTO Guide</title>
  
  <link rel="icon" href="{{asset('/images/favicon.ico')}}">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/slider-pro.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/bootstrap-tagsinput.css')}}" rel="stylesheet">
  <link href="{{asset('css/common.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
  <style>
    .badge {
      display: inline-block;
      padding: 0.50em 0.75em;
      font-size: 80%;
      font-weight: 500;
    }
    .is_read1 {
      color: #999;
    }
    .is_read0 {
      color: #23a127;
    }
    .header-margin {
      background-image: url('<?=url('images/search-car.png')?>');
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-position: center; 
    }
    .header .cxm-menu .social-media {
      border-top: 2px dotted #158CBA;
      padding-top: 2px; 
      margin-top: 2px;
    }
    .header .cxm-ss-icons a img {
      margin-top: -8px;
    }
    .header .logo {
      padding-top: 16px;
    }
    .quiz-icons {
      margin-left: 25px;
    }
    .home-page .header .top-icons  {
      opacity: 0;
      -webkit-transition: opacity 0.5s ease-out;
      -moz-transition: opacity 0.5s ease-out;
      -o-transition: opacity 0.5s ease-out;
      transition: opacity 0.5s ease-out;
    }
    .home-page .header.bg-header .top-icons {
      opacity: 1;
    }

    @media only screen and (min-width: 480px) and (max-width: 1024px) {
      .header .cxm-ss-icons a:first-child {
        left: 42%;
        background-color: rgba(23, 162, 184, 0);
      }
      .header .cxm-ss-icons a:last-child {
        right: 42%;
        background-color: rgba(255, 193, 7, 0);
      }
      .header .cxm-ss-icons {
        margin-top: 0px;
      }
      .header .cxm-ss-icons a img {
        width: 62px;
        margin-top: 2px;
      }
    }
    
    @media only screen and (min-width: 320px) and (max-width: 480px) {
        .top-icons img {
        width:0px;
      }
    }
  </style>

  <script>
    function drag_drop() {
      $(".sortable1").sortable({
        connectWith: ".connectedSortable"
      }).disableSelection();
      $(".sortable3").sortable({
        connectWith: ".connectedSortable"
      }).disableSelection();
      $(".sortable4").sortable({
        connectWith: ".connectedSortable"
      }).disableSelection();
      $(".sortable2").sortable({
        connectWith: ".connectedSortable",
        update: function (event, ui) {
          console.log(ui)
          if ($(ui.item).parent().attr('id') != 'main_section') return;
          if ($(ui.item).find('.addmore').length > 0) return;
          $(ui.item).append(' <button type="button" class="btn btn-primary addmore">  +</button>');
        }
      }).disableSelection();
    }
    
    $(document).on('click', '.addmore', function (e) {
      e.preventDefault();
      var person = prompt("Please enter name");
      if (person != null) {
        var final_html = "<div class='panel panel-default'><div class='panel-heading'>" + person + "</div><div class='panel-body sortable3 connectedSortable'></div></div>";
        $(this).parents('li').append(final_html);
      }
    })
    $(window).on('load', function () {
    })
  </script>

</head>

<body class="{{isset($home)? 'home-page' : ''}}">
  <!-- Google Tag Manager Body -->
  @include('googletagmanager::body')
  <!-- End Google Tag Manager Body -->

  <div class="header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-1 col-lg-1"></div>
        <div class="col-md-2 col-lg-2">
          <div class="logo">
            <a href="{{url('/')}}"><img class="img-fluid" src="{{asset('images/og_logo_hrz.svg')}}"></a>
          </div>
        </div>
        <div class="col-md-4 col-lg-4 top-icons">
          <div class="text-right ml-3 ">
            <a class="mr-3" href="{{url('search-car.html')}}">
              <img width="18%" class="img-fluid" src="{{asset('/images/search-car.svg')}}">
            </a> 
            <a class="mr-3" href="{{url('faq-search.html')}}">
              <img width="18%" class="img-fluid" src="{{asset('/images/quiz-car.svg')}}">
            </a>
            <a class="mr-3" href="{{url('/car/services')}}">
              <img width="18%" class="img-fluid" src="{{asset('/images/service-car.svg')}}">
            </a>
          </div>
        </div>
        <div class="col-md-4 col-lg-4">
          <div class="cxm-menu d-none d-md-block">
            <ul>
              <li>
                @if(Session::has('geo_location_session'))
                <a data-toggle="collapse" class="location" href="#location-menu" style="font-size: 20px; text-decoration:none;">
                  {{Session::get('geo_location_session')['zip_code']}}
                  <!-- {{Session::get('geo_location_session')['city']}}, {{Session::get('geo_location_session')['state']}} -->
                  <span class="fa fa-map-marker pl-2" style="font-size: 20px;></span>
                </a>
                @else
                <a data-toggle="collapse" href="#location-menu" style="font-size: 18px; color:#375e97; text-decoration:none;">
                  Location?<span class="fa fa-map-marker pl-3" style="font-size: 20px;></span>
                </a>  
                @endif  
                &nbsp;
                <span class="fa fa-map-marker"></span>
                <div class="collapse menu-box text-left" id="location-menu">
                  <div><a class="btn-close" id="l_close" href="#"><span class="fa fa-remove"></span></a></div>
                  <div class="heading">Your Zip Code</div>
                  <div class="fs14">
                    <div class="input-group mb-3">
                      <input type="text" class="form-control" value="<?=$addr?>" id="z_c" placeholder="Enter Zip Code" autofocus>
                      <div class="input-group-append">
                        <button class="btn btn-primary" onclick="$('#ref').show(); getLatLong($('#z_c').val());" type="button">Enter</button>
                      </div>
                    </div>
                    <small class="user-address form-text text-muted" id="g_n"><?=$addr?></small>
                    <span style="float: right;display: none" id="ref"><i class="pull-right fa fa-refresh fa-spin"></i> Loading...</span>
                  </div>
                </div>
              </li>
              <li>
                @auth
                  <a href="{{route('user-dashboard')}}"><span class="fa fa-user"></span></a>
                  <a href="{{route('logout')}}"><span class="fa fa-sign-out"></span></a>
                @endauth
                @guest
                  <a data-toggle="collapse" href="#login-menu"><span class="fa fa-lock"></span></a>
                  <div class="collapse menu-box" id="login-menu">
                    <div class="text-left">
                      <div><a class="btn-close" href="#"><span class="fa fa-remove"></span></a></div>
                      <div class="heading">Account Sign in or Sign up</div>
                      <div class="row">
                        <label class="radio-inline" style="font-size: 20px; font-weigh:bold; padding-left:10px; padding-bottom:5px;">
                          &nbsp;<input type="radio"
                          onclick="$('#register_div').hide(); $('#login_div').show();"
                          name="sinin" checked><span style="padding-left: 10px;">Sign in</span>
                        </label>&nbsp;&nbsp;&nbsp;
                        <label class="radio-inline" style="font-size: 20px; font-weigh:bold; padding-left:10px; padding-bottom:5px;">
                          &nbsp;<input type="radio"
                          onclick="$('#register_div').show(); $('#login_div').hide();"
                          name="sinin"><span style="padding-left: 10px;">Sign up</span>
                        </label>
                        <br>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div id="login_div">
                            <form method="POST" action="{{ route('login') }}" id="login-form">
                              {{ csrf_field() }}
                              <label for="email">Email Address</label>
                              <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">
                                    <span class="fa fa-envelope-o"></span>
                                  </span>
                                </div>
                                <input type="text" name="email" class="form-control" placeholder="Email" autofocus>
                              </div>
                              <label for="password">Password</label>
                              <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><span class="fa fa-lock"></span></span>
                                </div>
                                <input type="password" class="form-control" name="password" placeholder="Password">
                              </div>
                            </form>
                            <div class="alert alert-danger validate-error-msg" style="font-size:12px; display:none">
                              <ul></ul>
                            </div>
                            <div class="row mb-12" style="display: none;" id="login-validate-loader">
                              <span style="margin:0 auto; color: #bf2f2f;" class="fa fa-spinner fa-spin"></span>
                            </div>
                            <div class="row mb-3">
                              <div class="col-sm-5">
                                <button data-form-id="login-form"
                                  data-validate-url="/login/validate"
                                  data-validate-loader="login-validate-loader"
                                  type="button"
                                  class="btn-form-submit login btn btn-primary rounded-0">
                                  <span class="fa fa-sign-in"></span>
                                  Login
                                </button>
                                <button type="reset" form="login-form" value="Reset"class="btn btn-secondary">Reset</button>
                              </div>
                              <div class="col-sm-7">
                                <div class="custom-control custom-checkbox mt-2 fs14">
                                  <input type="checkbox" class="custom-control-input" id="customCheck1">
                                  <label class="custom-control-label" for="customCheck1">Remember me</label>
                                </div>
                              </div>
                            </div>
                            <a class="fs12" href="{{url('/user/forgot/password/')}}">Forgotten your password?</a>
                          </div>
                          <div id="register_div" style="display: none;">
                            <form method="POST" action="{{route('register')}}" id="register-form">
                              {{ csrf_field() }}
                              <div class="row">
                                <div class="col-sm-6">
                                  <label for="email">First Name</label>
                                  <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text">
                                        <span class="fa fa-user-o"></span>
                                      </span>
                                    </div>
                                    <input type="text" name="name" class="form-control" placeholder="First Name">
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <label for="email">Last Name</label>
                                  <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text">
                                        <span class="fa fa-user-o"></span>
                                      </span>
                                    </div>
                                    <input type="text" name="lastname" class="form-control" placeholder="Last Name">
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <label for="email">Email Address</label>
                                  <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text">
                                        <span class="fa fa-envelope-o"></span>
                                      </span>
                                    </div>
                                    <input type="text" name="email" class="form-control" placeholder="Email">
                                  </div>
                                </div>	
                                <div class="col-sm-6">
                                  <label for="mobile_no">Mobile No</label>
                                  <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text">
                                        <span class="fa fa-phone"></span>
                                      </span>
                                    </div>
                                    <input type="text" name="phone" class="form-control" placeholder="###-###-####">
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <label for="password">Password</label>
                                  <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text">
                                        <span class="fa fa-lock"></span>
                                      </span>
                                    </div>
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <label for="password-confirm">Confirm Password</label>
                                  <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text">
                                        <span class="fa fa-lock"></span>
                                      </span>
                                    </div>
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password">
                                  </div>
                                </div>
                              </div>
                            </form>
                            <div class="alert alert-danger validate-error-msg" style="font-size:12px; display:none">
                              <ul></ul>
                            </div>
                            <div class="row mb-12" style="display: none;"
                              id="register-validate-loader">
                              <span style="margin:0 auto; color: #bf2f2f;"
                              class="fa fa-spinner fa-spin"></span>
                            </div>
                            <div class="col-sm-6" >
                              <input name="agreement" class="agreement form_controll" type="checkbox">
                              <small>I accept the <a href="#">terms & conditions</small></a>
                            </div>
                            <div class="col-sm-6">
                              <button data-form-id="register-form"
                                data-validate-url="/register/validate"
                                data-validate-loader="register-validate-loader"
                                type="button"
                                class="btn-form-submit btn btn-primary rounded-0">
                                <span class="fa fa-sign-in"></span>
                                Sign Up
                              </button>
                              <button type="reset" form="register-form" value="Reset" class="btn btn-secondary">Reset</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="border-top pt-3 mt-4 text-center text-dark fs14">
                      <span class="fa fa-lock fc1 fs24"></span>
                      We'll always protect your OTTO Guide account and keep your details safe.
                    </div>
                  </div>
                @endguest
              </li>
              <li>
                <a data-toggle="collapse" href="#main-menu">
                  <span class="fa fa-bars"></span>
                </a>
                <div class="collapse menu-box text-left" id="main-menu">
                  <div><a class="btn-close" href="#"><span class="fa fa-remove"></span></a></div>
                  <div class="heading">Main Menu</div>
                  <ul class="cxm-main-menu">
                    <li><a href="{{route('whoweare')}}">Who We Are</a></li>
                    <li><a href="{{route('howitwork')}}">How Its Works</a></li>
                    <li><a href="{{route('affiliate')}}">Affiliate</a></li>
                    <li><a href="{{route('faq')}}">FAQ's</a></li>
                    <li><a href="{{route('contactus')}}">Contact Us</a></li>
                  </ul>
                  <div class="social-media">
                    <a href="#" target="_blank"><span class="fa fa-facebook"></span></a>
                    <a href="#" target="_blank"><span class="fa fa-twitter"></span></a>
                    <a href="#" target="_blank"><span class="fa fa-linkedin"></span></a>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <div class="d-block d-md-none">
            <nav class="navbar navbar-expand-md navbar-light cxm-navbar">
              <span class="navbar-brand mb-0 h1 d-md-none d-lg-none">
                <a href="{{url('search-car.html')}}"><img class="img-fluid" src="{{asset('/images/search-car.svg')}}"></a> 
                <a href="{{url('faq-search.html')}}"><img class="img-fluid" src="{{asset('/images/quiz-car.svg')}}"></a>
                <a href="{{url('/car/services')}}"><img class="img-fluid" src="{{asset('/images/service-car.svg')}}"></a>
                <a href="{{ route('login') }}"><span class="fa fa-sign-in"></span></a>
                @if(Session::has('geo_location_session'))
                  <a data-toggle="collapse" class="location" href="#location-menu2">
                    {{Session::get('geo_location_session')['city']}} , {{Session::get('geo_location_session')['state']}}
                  </a>
                  <span class="fa fa-map-marker"></span>
                @else
                  <a data-toggle="collapse" href="#location-menu2" style="font-size: 16px; color:#cc0000; text-decoration:underline;">
                    location
                  </a> 
                @endif  
                &nbsp;
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                  data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                  aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon fc1"></span>
                </button>
              </span>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav">
                  <li class="nav-item active"><a class="nav-link" href="{{route('whoweare')}}">Who We Are</a></li>
                  <li class="nav-item"><a class="nav-link" href="{{route('howitwork')}}">How Its Works</a></li>
                  <li class="nav-item"><a class="nav-link" href="{{route('affiliate')}}">Affiliate</a></li>
                  <li class="nav-item"><a class="nav-link" href="{{route('faq')}}">FAQ's</a></li>
                  <li class="nav-item"><a class="nav-link" href="{{route('contactus')}}">Contact Us</a></li>
                </ul>
              </div>
            </nav>
            <center style="border-top: 1px dotted #ccc; margin-top:3px; padding-top:1px;"></center>
          </div>
                            
        </div>
        <div class="col-md-1 col-lg-1"></div>
      </div>
    </div>
  </div>

  <script>
    $(document).ready(function () {
      $('.btn-form-submit').attr('disabled',true);
      $('.login').attr('disabled',false);
      var ckbox = $('.agreement ');
      $('.agreement').on('click',function () {
        if (ckbox.is(':checked')) {
          $('.btn-form-submit').attr('disabled',false);
        } else {
          event.stopPropagation();
          $('.btn-form-submit').attr('disabled',true);
        }
      });
    });
  </script>
  
  @if(Session::has('geo_location_session'))
  
  @else
    <script type="text/javascript">
      $( document ).ready(function() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition((loc) => {
            var lat = loc.coords.latitude;
            var long = loc.coords.longitude;
            $('.lat').val(lat); 
            $('.long').val(long);
            console.log('##### Lat Long #####');
            console.log('lat: '+ lat);
            console.log('long: '+ long);
            $.ajax({
              type:'POST',
              data: {
                "_token": "{{ csrf_token() }}",	
                get_lat: lat,
                get_long: long
              },
              url:'{{url('get/auto/location')}}',
              success:  function () {
                location.reload(true);
              }
            }); 
          });
        } 
        else {
          return false;
        }
      }); 
    </script>
  @endif

  <script>
    $('html').click(function() {
      $('.btn-close').trigger('click');
      $('.menu-overlay').removeClass('show');
    });
    $('.menu-box').click(function(event){
      event.stopPropagation();
    });
  </script>
<div class="menu-overlay collapse"></div>