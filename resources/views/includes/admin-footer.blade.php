<style>
    #data-table_filter {
        float: right;
    }
</style>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('admin-assets2/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('admin-assets2/js/jquery-ui.min.js')}}"></script>
<!-- Bootstrap 3.3.5 -->
<script src="{{asset('admin-assets2/js/bootstrap.min.js')}}"></script>
<!-- datatable charts -->
<script src="{{asset('admin-assets2/js/datatables.js')}}"></script>
<script>
	var datatable_options = {
		processing:  true,
		serverSide:  true,
		columns:     [],
		aLengthMenu: [50, 100, 500],
	};
</script>

<!-- Morris.js charts
<script src="{{asset('admin-assets2/js/jquery.min.js')}}"></script>-->
<script src="{{asset('admin-assets2/js/raphael-min.js')}}"></script>
<script src="{{asset('admin-assets2/css/morris/morris.min.js')}}"></script>

<!-- Sparkline -->
<script src="{{asset('admin-assets2/css/sparkline/jquery.sparkline.min.js')}}"></script>

<!-- jvectormap -->
<script src="{{asset('admin-assets2/css/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('admin-assets2/css/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

<!-- jQuery Knob Chart -->
<script src="{{asset('admin-assets2/css/knob/jquery.knob.js')}}"></script>

<!-- daterangepicker -->
<script src="{{asset('admin-assets2/js/moment.min.js')}}"></script>
<script src="{{asset('admin-assets2/css/daterangepicker/daterangepicker.js')}}"></script>


<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('admin-assets2/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>

<!-- Slimscroll -->
<script src="{{asset('admin-assets2/css/slimScroll/jquery.slimscroll.min.js')}}"></script>

<!-- FastClick -->
<script src="{{asset('admin-assets2/css/fastclick/fastclick.min.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('admin-assets2/js/app.min.js')}}"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->

<script src="{{asset('admin-assets2/js/demo.js')}}"></script>

@section('page_script')


@show

<script src="{{asset('admin-assets2/js/custom.js')}}"></script>

<script>
	jQuery(function ($) {
		$(".jq-date-time").datepicker({
			dateFormat:  'dd/mm/yy',
			altField:    '#dateStrat',
			altFormat:   'yy-mm-dd',
			changeMonth: true,
			changeYear:  true,
			showWeek:    false,
			firstDay:    1
		});
		$('label').next('input').attr("autocomplete", "off");
		$('label').next('textarea').attr("autocomplete", "off");
		$(".js-menu_sign_out").click(function (e) {
			if ($(this).parent().hasClass('open'))
				$(this).parent().removeClass('open');
			else
				$(this).parent().addClass('open');
		});


		$(document).on('click', '.btn-form-submit', function (e) {
			e.stopImmediatePropagation();
			e.preventDefault();

			var $this = $(this);
			var btn_text = $(this).html();
			var form_id=$(this).attr('data-form-id');
			var validate_loader=$(this).attr('data-validate-loader');
			var validate_url = $(this).attr('data-validate-url');

			var data = $("#"+form_id).serialize();
			$.ajax({
				url: "{{url('/')}}"+validate_url,
				type:'POST',
				data: data,
				beforeSend: function(){
					$("#"+form_id).find("div.validate-error-msg").hide();
					$("#"+validate_loader).show();
					$this.attr('disabled', true).html('Loading...');
				},
				complete: function(){
					$("#"+validate_loader).hide();
					$this.attr('disabled', false).html(btn_text);
				},
				success: function(data) {
					$this.attr('disabled', false).html(btn_text);
					if($.isEmptyObject(data.error)){
						$("#"+form_id).submit();
					}else{
						error_messages=data.error;
						$("#"+form_id).find("div.validate-error-msg").html('<ul></ul>');
						$("#"+form_id).find("div.validate-error-msg").css('display','block');
						$.each( error_messages, function( key, value ) {
							$("#"+form_id).find("div.validate-error-msg").find("ul").append('<li>'+value+'</li>');
						});
						$("#"+form_id).find("div.validate-error-msg").show();
						$(window).scrollTop($("#"+form_id).find("div.validate-error-msg").offset().top)
					}
				},
				error:   function () {
					$this.attr('disabled', false).html(btn_text);
				}
			});
		});
	});

</script>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
</footer>

<!-- Control Sidebar -->
<div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->

</body>
</html>
