
<div class="footer">
  <div class="footer-cols bgc1 pt-4 pb-4 text-white">
    <div class="container">
      <!--<div class="row logos">
        <div class="col-3 mb-3"><img class="img-fluid" src="{{asset('images/car-and-driver-logo.png')}}"></div>
        <div class="col-3 mb-3"><img class="img-fluid" src="{{asset('images/motor-trend-logo.png')}}"></div>
        <div class="col-3 mb-3"><img class="img-fluid" src="{{asset('images/j-d-power-logo.png')}}"></div>
        <div class="col-3 mb-3"><img class="img-fluid" src="{{asset('images/edmunds-logo.png')}}"></div>
      </div>-->
      <div class="row">
        <div class="col-sm-6 col-md-3">
          <h5>Media</h5>
          <div class="footer-nav">
            <ul>
              <li><a href="#">News</a></li>
              <li><a href="#">Investors</a></li>
              <li><a href="#">Blog</a></li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <h5>Dealers</h5>
          <div class="footer-nav">
            <ul>
              <li><a href="{{url('dealership-overview.html')}}">Dealership Overview</a></li>
              <li><a href="{{route('dealer_login')}}">Dealer Login</a></li>
              <li><a href="{{route('dealer_signup')}}">Dealer Signup</a></li>
              <li><a href="{{route('dealer_portal')}}">Dealer Portal</a></li>
            </ul>
          </div>
        </div>
        
        <div class="col-sm-6 col-md-3">
          <h5>About Us</h5>
          <div class="footer-nav">
            <ul>
               <li><a href="{{route('whoweare')}}">Who We Are</a></li>
                <li><a href="{{route('howitwork')}}">How Its Works</a></li>
                <li><a href="{{route('career')}}">Affiliate</a></li>
                <li><a href="{{route('faq')}}">FAQ's</a></li>
                <li><a href="{{route('contactus')}}">Contact Us</a></li>
            </ul>
          </div>
          </div>
                  
     

        <div class="col-sm-6 col-md-3">
<!-- REMOVED BY Amin
	  <h5>Subscribe to Newsletter</h5>
          <div class="input-group input-group-sm mb-3">
            <input type="text" class="form-control" placeholder="Enter Your Email" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="btn btn-primary" type="button">Submit</button>
            </div>
          </div>
          
          <h5>Follow Us</h5>
          <div class="social-media">
          <a href="#" target="_blank"><span class="fa fa-facebook"></span></a>
          <a href="#" target="_blank"><span class="fa fa-twitter"></span></a>
          <a href="#" target="_blank"><span class="fa fa-google-plus"></span></a>
          <a href="#" target="_blank"><span class="fa fa-linkedin"></span></a>
          <a href="#" target="_blank"><span class="fa fa-rss"></span></a>
	  </div>
-->	  
	</div>
      </div>
    </div>
  </div>  
  <div class="copyright">
    <div class="container">
<!-- REMOVED BY Amin
      <div class="row">
        <div class="col-8 offset-2 col-sm-2 offset-sm-4 mb-2"><img class="img-fluid" src="{{asset('images/apple-store-logo.png')}}"></div>
        <div class="col-8 offset-2 col-sm-2 offset-sm-0 mb-2"><img class="img-fluid" src="{{asset('images/google-play-logo.png')}}"></div>
      </div>
-->
      <div class="row">
        <div class="col-sm-12">
          <div class="font-weight-bold">Copyright &copy; and TM 2019 OTTO Guide, Inc. All rights reserved.</div>
          <div class="fs10"><a class="text-muted" href="#">Terms Of Services</a> | <a class="text-muted" href="#">Privacy Policy</a></div>
<!-- REMOVED BY Amin
	  <img width="100" class="img-fluid" src="{{asset('images/norton-logo.png')}}">
-->
	</div>
      </div>
    </div>    
  </div>
</div>
<a href="#" class="back-to-top"><span class="fa fa-hand-o-up"></span></a>
@include('includes.footer_scripts') 


<script>
setTimeout(function(){
  $('.message').remove();
}, 5000);
</script>

  </body>
</html>
