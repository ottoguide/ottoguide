<!DOCTYPE HTML><html lang="en"><head>	
<meta charset="utf-8"><meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="max-age=604800" />	
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<style>		
	body{			
	background: url("{{asset('dealer_assets/images/background.jpg')}}");			
	background-repeat: no-repeat;			
	background-attachment: top-fixed;			
	background-position: top-center;			
	padding-top: 8%;		
	}	
	

	
	</style>		
	<title>OttoGuide Dealer</title>		
	<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">		
	<!-- Font awesome 5 -->		
	<link href="{{asset('dealer_assets/fonts/fontawesome/css/fontawesome-all.min.css')}}" type="text/css" rel="stylesheet">
	<link href="{{asset('dealer_assets/css/price-calculator.css')}}" type="text/css" rel="stylesheet">	<!-- custom style -->	
	<link href="{{asset('dealer_assets/css/uikit.css')}}" rel="stylesheet" type="text/css"/>		
	<link href="{{asset('dealer_assets/css/responsive.css')}}" rel="stylesheet" media="only screen and (max-width: 900)" />	<!-- custom javascript -->	
	<script src="{{asset('dealer_assets/js/script.js')}}" type="text/javascript"></script>	
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" media="all" href="{{asset('dealer_assets/og/tabs.css')}}">	
	<link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon" >
	<script type="text/javascript" src="{{asset('dealer_assets/js/librarary.js')}}"></script>	
	<style>		
	.container {
		max-width: 85%;		}	
		</style>		
		</head>	
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('dealer_assets/og/filter.css')}}">	
		<link rel="stylesheet" type="text/css" media="all" href="{{asset('dealer_assets/og/filter2.css')}}">	
		<script type="text/javascript" src="{{asset('dealer_assets/js/external_jq_ui.entry.js')}}"></script>	
		<script type="text/javascript" src="{{asset('dealer_assets/js/listings.entry.en_US.js')}}"></script>	
		<script type="text/javascript" src="{{asset('dealer_assets/js/listings.entry.js')}}"></script>	
		<style>		
		.navbar{			
		background-color: #eaf4fd;			
		margin-bottom: 0px;		
		}		
		.top-head{			
		background-color: #eaf4fd;			
		padding:12px;			
		border-bottom:1px solid #ccc;		
		}		
		.top-head h4{			
		font-weight: bold;			
		color:#222;		
		}		
		.navbar-nav > li {			
		border-right: 0px solid #fff;			
		text-decoration: none;			
		margin:3px;		}		
		.navbar-default .navbar-nav > li > a, .navbar-default .navbar-text {			
		color: #217C9D;			margin:3px;			
		text-decoration: none;		}		
		.navbar-default .navbar-nav > li > a:hover {			
		color: #fff;			
		background-color: #217C9D;			
		border-radius: 5px;		}	
		.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover {		
		color: #fff;			
		background-color: #217C9D;			
		border-radius: 5px;		
		margin:3px;			
		text-decoration: none;		
		}		
		.bg-secondary{			
		background-color: #fefefe;		
		}		
		.grey{			
		
		background-color: #f2f2f2;
		}		
		.graph-tab-active{			
		background-color: #217C9D;		
		}		
		.tag {			
		padding: 10px;			
		background-color: #FFF6CA;			
		font-size: 12px;			
		color: #333;			
		border-radius: 4px;			
		text-align: center;			
		font-weight: bold;		
		}		
		.pie-active{			
		padding:10px;			
		background-color :#217C9D;			
		color: #fff;		
		}		
		input[type="search"] {			
		text-align: left;		
		}	
			
		
		</style>
		
		<body>	<br>	<br>	<br><div class="container">	
		<div class="row top-head">		
		<div class="col-md-4"><h4>Classic Nissan for Statesville</h4></div>	
		<div class="col-md-3"><h5><b>View/Edit Inventory</b></h5></div>		
		<div class="col-md-5"><div class="tag">Give us your feedback to help to improve our product offering.</div>
		</div>	</div>	<div class="row">		
		<div id="navbar">		
		<nav class="navbar navbar-default navbar-static-top" role="navigation">			
		<div class="navbar-header">					
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
		<span class="sr-only">Toggle navigation</span>		
		<span class="icon-bar"></span>						
		<span class="icon-bar"></span>						
		<span class="icon-bar"></span>				
		</button>			
		</div>				
		<div class="collapse navbar-collapse" id="navbar-collapse-1">			
		<ul class="nav navbar-nav">         
		<li class="#"><a href="dashboard.php">Dashboard</a></li>         
		<li class="#">         
		<a class="nav-link" href="billing.php">Billing</a>						
		</li>			
		<li class="#">              
		<a class="nav-link" href="dealer-pricing.php">Dealer Pricing Tool</a>						
		</li>			
		<li class="#">              
		<a class="nav-link" href="market-analysis.php">Market Analysis</a>						
		</li>			<li class="#">              
		<a class="nav-link" href="criaglist.php">Craiglist Assistant</a>						
		</li>			
		<li class="#">              
		<a class="nav-link" href="sales-review.php">Manager Review</a>						
		</li>			
		<li class="#">              
		<a class="nav-link" href="settings.php">Settings</a>						
		</li>			
		<li class="#">           
		<a class="nav-link" href="leads.php">Leads</a>						
		</li>			
		<li class="#">            
		<a class="nav-link" href="dealer-badges.php">Badges</a>						
		</li>					
		</ul>				
		</div><!-- /.navbar-collapse -->			
		</nav>		
		</div>	
		</div>		