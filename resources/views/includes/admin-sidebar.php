<!-- Left side column. contains the logo and sidebar -->
<style>
.sidebar-menu li.active > a > .pull-right-container > .fa-angle-right {
    transform: rotate(90deg);
	transition: all 0.5s ease;
}
.sidebar-menu li > a > .pull-right-container > .fa-angle-right {
    transition: all 0.5s ease;
}

.chat_block_action {
    background-color: #ecf0f5;
    padding: 5px;
    border-radius: 5px;
    color: #555b63;
}
</style>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php if (!empty(Auth::user()->image) > 0) {
					echo Auth::user()->image;
				} ?>" class="img-circle" alt="User Image" width="75" height="75">
            </div>
            <div class="pull-left info">
                <p>Administrator</p>
                <a href="#"><i class="fa fa-circle text-success"></i>Online</a>
            </div>
        </div>
        <!-- search form -->
        <!--<form action="#" method="get" class="sidebar-form">
		  <div class="input-group">
			<input type="text" name="q" class="form-control" placeholder="Search...">
			<span class="input-group-btn">
				  <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
				  </button>
				</span>
		  </div>
		</form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
                <a href="/dashboard.html">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
          
           <!-- <li class="">
                <a href="/roles/list">
                    <i class="fa fa-users"></i>
                    <span>Admin group</span>
                    <span class="pull-right-container">
              
            </span>
                </a>
            </li>-->
			
			
			
			
			<ul class="sidebar-menu" data-widget="tree">
			<li class="header">Admin</li>
				<li class="treeview menu-open">
					<a href="">
						<i class="fa fa-users"></i><span>Admin</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-right pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu menu-open treeview-menu-visible">
						<li><a href="/roles/list"><i class="fa fa-users"></i> Admin Group List</a></li>
						 <li><a href="/user/list"><i class="fa fa-users"></i> Admin Users </a></li>
						
						 
					</ul>
				</li>
			</li>
		</ul>
			
			
			
			
			
			
           <!-- <li>
                <a href="/user/list">
                    <i class="fa fa-user"></i>
                    <span>Admin Users</span>
                    <span class="pull-right-container">
             <!--<small class="label pull-right bg-green">new</small>
            </span>
                </a>
            </li>-->

            <li class="header">Dealer</li>
           	<li>
			  <a href="/dealer/activity">
				<i class="fa fa-user"></i> <span>Dealer List & Activity</span>

			  </a>
			</li> 
			
			   <li class="">
                <a href="/dealer-list-api.html">
                    <i class="fa fa-users"></i>
                    <span>Dealers List From Api</span>

                </a>
            </li>
			
		
			
			
			<ul class="sidebar-menu" data-widget="tree">
			<li class="header">Users</li>
				<li class="treeview menu-open">
					<a href="#">
						<i class="fa fa-users"></i><span>Users</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-right pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu menu-open treeview-menu-visible">
						<li><a href="/client/list"><i class="fa fa-users"></i> User list</a></li>
						<li><a href="/request/unblock/list"><i class="fa fa-line-chart"></i> User Unblock Request </a></li>
					</ul>
				</li>
			</li>
		</ul>
			
			
			
			
            <!--<li>
                <a href="/client/list">
                    <i class="fa fa-user"></i>
                    <span>Users</span>
                </a>
            </li> 
			<!--
            <li class="">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Registered Dealer</span>
                </a>
            </li>-->
            <!--<li>
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Registered Users </span>
                </a>
            </li>-->
          
			 <!--
            <li class="header">System Subscription</li>
            <li>
                <a href="/subscription/list">
                    <i class="fa fa-tv"></i>
                    <span>System Subscription</span>
                </a>
            </li>

			-->
			 <li class="header">Rating & Reviews</li>
        <li class="">
          <a href="/admin/reviews/report">
            <i class="fa fa-line-chart"></i>
            <span>Reviews Report</span>
          </a>
          </li>
			
			
		<li class="header">Chat Communication</li>
        <li class="">
          <a href="/admin/chat/block">
            <i class="fa fa-line-chart"></i>
            <span>Chat Block List</span>
          </a>
          </li>
			
			
		  <!--<li class="header">User Unblock Request</li>
          <li class="">
          <a href="/request/unblock/list">
          <i class="fa fa-line-chart"></i>
          <span>User Unblock Request List</span>
          </a>
          </li>	-->
			
			
			
            <!--<li class="header">Email Template</li>-->


            <li class="header">Quiz</li>
            <li>
                <a href="/quiz/list">
                    <i class="fa fa-book"></i> <span>Quiz List</span>
                </a>
            </li>
            <!--
			<li>
			  <a href="pages/widgets.html">
				<i class="fa fa-user"></i> <span>On Chat</span>

			  </a>
			</li> -->

            <!--<li class="header">Content Management</li>-->

            </li>

            <!-- <li class="header">Report</li>-->

            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>