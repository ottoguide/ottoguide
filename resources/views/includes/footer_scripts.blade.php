<style type="text/css">
    .validate-error-msg ul li{
        display:block !important;
    }
</style>
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{asset('js/jquery.min.js')}}"></script>
    <script>
	$('.btn-toggle').click(function() {
        $(this).find('.btn').toggleClass('active');  

        if ($(this).find('.btn-primary').length>0) {
        	$(this).find('.btn').toggleClass('btn-primary');
        }
        if ($(this).find('.btn-danger').length>0) {
        	$(this).find('.btn').toggleClass('btn-danger');
        }
        if ($(this).find('.btn-success').length>0) {
        	$(this).find('.btn').toggleClass('btn-success');
        }
        if ($(this).find('.btn-info').length>0) {
        	$(this).find('.btn').toggleClass('btn-info');
        }    
        $(this).find('.btn').toggleClass('btn-default');       
    });
</script>
<script>
	$('.rating input').change(function () {
      var $radio = $(this);
      $('.rating .selected').removeClass('selected');
      $radio.closest('label').addClass('selected');
	});
</script>
<script>
    $('.uk-button').on('click', function (){
	   $('.uk-section').toggleClass('uk-dark uk-light');
       $('.uk-container > .uk-card').toggleClass('uk-card-default uk-card-secondary');
	   $('html').toggleClass('uk-background-muted uk-background-secondary');
	});
    
    $(document).ready(function($) {

      $("#z_c").keypress(function(e) {
        if(e.which == 13) {
          $('#ref').show();
          getLatLong($('#z_c').val());
        }
      });
        /*$('#cxm-slider-trands').carousel({
          autoPlay: false
        });*/
        $(".btn-form-submit").click(function(e){
            var form_id=$(this).attr('data-form-id');
            var validate_loader=$(this).attr('data-validate-loader');
            var validate_url = $(this).attr('data-validate-url');
            e.preventDefault();
            var data = $("#"+form_id).serialize();
            $.ajax({
                url: "{{url('/')}}"+validate_url,
                type:'POST',
                data: data,
                beforeSend: function(){
                    $("#"+form_id).next("div.validate-error-msg").hide();
                    $("#"+validate_loader).show();
                },
                complete: function(){
                    $("#"+validate_loader).hide();
                },
                success: function(data) {
                  if($.isEmptyObject(data.error)){
                    $("#"+form_id).submit();
                  }else{
                   error_messages=data.error;
                    $("#"+form_id).next("div.validate-error-msg").find("ul").html('');
                    $("#"+form_id).next("div.validate-error-msg").css('display','block');
                    $.each( error_messages, function( key, value ) {
                       $("#"+form_id).next("div.validate-error-msg").find("ul").append('<li>'+value+'</li>');
                    });
                    $("#"+form_id).next("div.validate-error-msg").show();
                  }
                },
            });
        });        
    });   

    function getLatLong(zip){       
        var data = "_token={{csrf_token()}}";
        data += "&zip_code="+zip;

        $("#zip").val(zip);

        $.ajax({
          url: "{{url('/get/lat/long')}}",
          type:'POST',
          dataType: 'json',
          data: data,
          beforeSend: function(){         
          },        
          success: function(response) {
            $('#latitude').val(response.lat);
            $('#longitude').val(response.long);
            $("#geo_ip").text(zip);
            $("#l_close").click();
            $('#ref').hide();
			location.reload();
          },
        });
      }
</script>
<!-- Placed at the end of the document so the pages load faster -->

 {{--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--}}

<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/ie10-viewport-bug-workaround.js')}}"></script>
<script src="{{asset('public/js/common.js')}}"></script>