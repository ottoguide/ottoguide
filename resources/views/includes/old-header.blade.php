
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="CXM">
    <link rel="icon" href="{{asset('public/images/favicon.ico')}}">
    <title>OTTO Guide</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/slider-pro.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/bootstrap-tagsinput.css')}}" rel="stylesheet">
    <link href="{{asset('css/common.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<style>
	.badge{
    display: inline-block;
    padding: 0.50em 0.75em;
    font-size: 80%;
    font-weight: 500;
	}
	.is_read1 {
	  color: #999;
	}	
	.is_read0 {
	  color: #23a127;
	}	
	
  .header-margin{
  background-image: url('https://dev.ottoguide.com/images/search-car.png');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center; 
  }

.header .cxm-menu .social-media {
    border-top: 2px dotted #158CBA;
    padding-top: 2px; 
	margin-top: 2px;
}
 

 
 
@media only screen and (min-width: 480px) and (max-width: 1024px) {

 .header .cxm-ss-icons a:first-child {
    left: 42%;
    background-color: rgba(23, 162, 184, 0);
}
.header .cxm-ss-icons a:last-child {
    right: 42%;
    background-color: rgba(255, 193, 7, 0);
}
.header .cxm-ss-icons a img {
    width: 62px;
    margin-top: 25px;
}


  }
	</style>
	
    <script>
		function drag_drop() {
			$(".sortable1").sortable({
				connectWith: ".connectedSortable"
			}).disableSelection();
			$(".sortable3").sortable({
				connectWith: ".connectedSortable"
			}).disableSelection();
			$(".sortable4").sortable({
				connectWith: ".connectedSortable"
			}).disableSelection();
			$(".sortable2").sortable({
				connectWith: ".connectedSortable",
				update:      function (event, ui) {
					//alert('hfghg');
					console.log(ui)
					if ($(ui.item).parent().attr('id') != 'main_section') return;
					if ($(ui.item).find('.addmore').length > 0) return;
					$(ui.item).append(' <button type="button" class="btn btn-primary addmore">  +</button>');
				}
			}).disableSelection();
		}

		$(document).on('click', '.addmore', function (e) {
			e.preventDefault();
			var person = prompt("Please enter name");
			if (person != null) {
				var final_html = "<div class='panel panel-default'><div class='panel-heading'>" + person + "</div><div class='panel-body sortable3 connectedSortable'></div></div>";
				//var input_filed = "<input type'text' class='search' name='search'>";
				$(this).parents('li').append(final_html);
				drag_drop();
			}
		})
		$(window).on('load', function () {
			drag_drop();
		})

    </script>
	
</head>

<body class="{{isset($home)? 'home-page' : ''}}">
<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-6 col-md-2 col-lg-3">
                <div class="logo"><a href="{{url('/')}}"><img class="img-fluid" src="{{asset('images/logo.png')}}"></a>
                </div>
            </div>
            <div class="col-6 d-md-none">
                <div class="ph fs18 mt-3 text-right">888-411OTTO</div>
            </div>
            <div class="col-md-3 col-lg-3">
        <div class="text-center cxm-ss-icons d-none d-md-block"><a href="{{url('search-car.html')}}"><img class="img-fluid" src="{{asset('public/images/search-car.svg')}}"></a> <a href="/car/services"><img class="img-fluid" src="{{asset('public/images/service-car.svg')}}"></a></div>
                </div>




            <div class="col-md-7 col-lg-6">
                <div class="cxm-menu d-none d-md-block">
			
                    <ul>
		         		  
						<!-- <a href="{{url('remove/tracking/location')}}"><i class="fa fa-times fa-2" title="Unset Your current Location" style="color:#cc0000; padding-left:10px; font-size:18px;" aria-hidden="true"></i></a>-->	
						
						   <li>
						  
							@if(Session::has('geo_location_session'))
							<a data-toggle="collapse" class="location" href="#location-menu">	
							{{Session::get('geo_location_session')['city']}} , {{Session::get('geo_location_session')['state']}}
							</a>

							@else
							<a data-toggle="collapse" href="#location-menu" style="font-size: 16px; color:#cc0000; text-decoration:underline;">
							Set Location  
							</a>  
							@endif  
							&nbsp;
                           <span class="fa fa-map-marker"></span>
						 
                          
                            <div class="collapse menu-box text-left" id="location-menu">
                                <a class="btn-close" id="l_close" href="#">
                                    <span class="fa fa-remove"></span>
                                </a>
                                <div class="heading">Your Location</div>
                                <div class="fs14">
                                    <label for="zip">Enter a location to Search</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="<?=$addr?>" id="z_c"
                                               placeholder="Enter Zip Code or City, ST">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary"
                                                    onclick="$('#ref').show(); getLatLong($('#z_c').val());"
                                                    type="button">OK
                                            </button>
                                        </div>
                                    </div>
                                    <small class="user-address form-text text-muted" id="g_n"><?=$addr?></small>
                                    <span style="float: right;display: none" id="ref"><i
                                                class="pull-right fa fa-refresh fa-spin"></i> Loading...</span>
                                </div>
                            </div>
                        </li>
						
						
						
                        <li><a data-toggle="collapse" href="#contact-menu">
                                <span class="fa fa-phone"></span>
                            </a>
                            <div class="collapse menu-box text-left" id="contact-menu">
                                <a class="btn-close" href="#">
                                    <span class="fa fa-remove"></span>
                                </a>
                                <div class="heading">Contact Us</div>
                                <div class="text-left fs14">
                                    <div class="row">
                                        <div class="col-sm-6 mb-3">
                                            <div class="fs24">Call Us Tollfree</div>
                                            <p>7 days a week from 8am-9pm CST.<br/>
                                                <span class="fs24">1.888.411.OTTO</span>
                                            </p>
                                        </div>
										
                                         <div class="col-sm-3">
                                                    <button class="btn btn-primary btn-block">
                                                        <span class="fa fa-phone"></span>
                                                        Call Me Now
                                                    </button>
                                                </div>
                                                <div class="col-sm-3">
                                                    <button class="btn btn-primary btn-block">
                                                        <span class="fa fa-comments"></span>
                                                        Live Chat
                                                    </button>
                                                </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="name">Full Name
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><span
                                                                class="fa fa-user"></span></span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Your Name">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="email">Email Address
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><span class="fa fa-envelope"></span></span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Your Email">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="phn">Phone Number
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><span
                                                                class="fa fa-phone"></span></span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Telephone No">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="pcode">Your Zip Code
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><span
                                                                class="fa fa-map-marker"></span></span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Postal Code">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="msg">Comments</label>
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><span
                                                                class="fa fa-list"></span></span>
                                                </div>
                                                <textarea class="form-control" placeholder="Your Message"></textarea>
                                            </div>

                                            <div class="form-row">
                                                <div class="col-sm-4">
                                                   
												<div class="social-media border-0">
                                                <a href="#" target="_blank">
                                                    <span class="fa fa-facebook"></span>
                                                </a>
                                                <a href="#" target="_blank">
                                                    <span class="fa fa-twitter"></span>
                                                </a>
                                              
                                                <a href="#" target="_blank">
                                                    <span class="fa fa-linkedin"></span>
                                                </a>
                                                
                                            </div> 
												   
                                                </div>
												
												
                                                <div class="col-sm-3 offset-sm-3">
                                                    <button class="btn btn-primary btn-block">
                                                        <span class="fa fa-send-o"></span>
                                                          Submit
                                                    </button>
                                                </div>
                                                <div class="col-sm-2">
                                                    <button class="btn btn-primary ">
                                                        <span class="fa fa-recycle"></span>
                                                       Clear
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                     
                        <li>
                            @auth
                                <a href="{{route('user-dashboard')}}">
                                    <span class="fa fa-dashboard"></span>
                                </a>
                                <a href="{{route('logout')}}">
                                    <span class="fa fa-sign-out"></span>
                                </a>
                            @endauth
                            @guest
                                <a data-toggle="collapse" href="#login-menu">
                                    <span class="fa fa-lock"></span>
                                </a>
                                <div class="collapse menu-box" id="login-menu">
                                    <div class="text-left">
                                        <a class="btn-close" href="#">
                                            <span class="fa fa-remove"></span>
                                        </a>
                                        <div class="heading">
                                            <label class="radio-inline">
                                                &nbsp;<input type="radio"
                                                             onclick="$('#register_div').hide(); $('#login_div').show();"
                                                             name="sinin" checked>Sign in
                                            </label>&nbsp;&nbsp;&nbsp;
                                            <label class="radio-inline">
                                                &nbsp;<input type="radio"
                                                             onclick="$('#register_div').show(); $('#login_div').hide();"
                                                             name="sinin">Sign Up
                                            </label>
                                            <br></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="login_div">
                                                    <form method="POST" action="{{ route('login') }}" id="login-form">
                                                        {{ csrf_field() }}
                                                        <label for="email">Email Address</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><span
                                                                            class="fa fa-envelope-o"></span></span>
                                                            </div>
                                                            <input type="text" name="email" class="form-control"
                                                                   placeholder="Email">
                                                        </div>
                                                        <label for="password">Password</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><span
                                                                            class="fa fa-lock"></span></span>
                                                            </div>
                                                            <input type="password" class="form-control" name="password"
                                                                   placeholder="Password">
                                                        </div>
                                                    </form>
                                                    <div class="alert alert-danger validate-error-msg"
                                                         style="font-size:12px; display:none">
                                                        <ul></ul>
                                                    </div>
                                                    <div class="row mb-12" style="display: none;"
                                                         id="login-validate-loader">
                                                        <span style="margin:0 auto; color: #bf2f2f;"
                                                              class="fa fa-spinner fa-spin"></span>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <div class="col-sm-5">
                                                            <button data-form-id="login-form"
                                                                    data-validate-url="/login/validate"
                                                                    data-validate-loader="login-validate-loader"
                                                                    type="button"
                                                                    class="btn-form-submit btn btn-primary rounded-0">
                                                                <span class="fa fa-sign-in"></span>
                                                                Login
                                                            </button>
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <div class="custom-control custom-checkbox mt-2 fs14">
                                                                <input type="checkbox" class="custom-control-input"
                                                                       id="customCheck1">
                                                                <label class="custom-control-label" for="customCheck1">
                                                                    Remember me
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="fs12" href="#">Forgotten your password?</a>
                                                </div>
												
												
												<!-- REGISTRATION -->
												
												<div id="register_div" style="display: none;">
												
										   <form method="POST" action="{{route('register')}}"
                                                          id="register-form">
                                                        {{ csrf_field() }}		
									<div class="row">
                                       
									   <div class="col-sm-6">
                                             <label for="email">First Name</label>
                                                  <div class="input-group mb-3">
                                                       <div class="input-group-prepend">
                                                          <span class="input-group-text">
														  <span class="fa fa-user-o"></span></span>
                                                            </div>
                                                            <input type="text" name="name" class="form-control"
                                                                   placeholder="First Name">
                                                        </div>
                                        </div>
										
                                        <div class="col-sm-6">
                                            
											 <label for="email">Last Name</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><span
                                                                            class="fa fa-user-o"></span></span>
                                                            </div>
                                                            <input type="text" name="lastname" class="form-control"
                                                                   placeholder="First Name">
                                                        </div>
                                        </div>
										
										
										
									   <div class="col-sm-6">
										
										<label for="email">Email Address</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><span
                                                                            class="fa fa-envelope-o"></span></span>
                                                            </div>
                                                            <input type="text" name="email" class="form-control"
                                                                   placeholder="Email">
                                                        </div>
										
										</div>	
										
										
									
										<div class="col-sm-6">
									
												<label for="mobile_no">Mobile No</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">
																<span class="fa fa-phone"></span></span>
                                                            </div>
                                                            <input type="text" name="phone" class="form-control"
                                                                   placeholder="12345678">
                                                        </div>	
											</div>
									
									
										<div class="col-sm-6">
										<label for="password">Password</label>
                                                        
														<div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><span
                                                                            class="fa fa-lock"></span></span>
                                                            </div>
                                                            <input type="password" class="form-control" name="password"
                                                                   placeholder="Password">
                                                        </div>
										</div>

									
										<div class="col-sm-6">
										
										 <label for="password-confirm">Confirm Password</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><span
                                                                            class="fa fa-lock"></span></span>
                                                            </div>
                                                            <input type="password" class="form-control"
                                                                   name="password_confirmation"
                                                                   placeholder="Confirm Password">
                                                        </div>
										
										</div>
									  </form>
									
									
									<div class="alert alert-danger validate-error-msg"
                                                         style="font-size:12px; display:none">
                                                        <ul></ul>
                                                    </div>
                                                    <div class="row mb-12" style="display: none;"
                                                         id="register-validate-loader">
                                                        <span style="margin:0 auto; color: #bf2f2f;"
                                                              class="fa fa-spinner fa-spin"></span>
                                                    </div>
                                                    <div class="row mb-3">
                                                        <div class="text-center col-sm-12">
                                                            <button data-form-id="register-form"
                                                                    data-validate-url="/register/validate"
                                                                    data-validate-loader="register-validate-loader"
                                                                    type="button"
                                                                    class="btn-form-submit btn btn-primary rounded-0">
                                                                <span class="fa fa-sign-in"></span>
                                                                Sign Up
                                                            </button>
                                                        </div>
                                                    </div>
									
									       
                                                </div>
                                            </div>
									
									
									  </div>
                                        <div class="border-top pt-3 mt-4 text-center text-dark fs14">
                                            <span class="fa fa-lock fc1 fs24"></span>
                                            We'll always protect your OTTO Guide account and keep your details safe.
                                        </div>
									
                                    </div>
												
										
                                    </div>
                                </div>
                            @endguest
                        </li>
                        <li>
                            <a data-toggle="collapse" href="#main-menu">
                                <span class="fa fa-bars"></span>
                            </a>
                            <div class="collapse menu-box text-left" id="main-menu">
                                <a class="btn-close" href="#">
                                    <span class="fa fa-remove"></span>
                                </a>
                                <div class="heading">Main Menu</div>
                                <ul class="cxm-main-menu">
                                    <li><a href="{{route('whoweare')}}">Who We Are</a></li>
                                    <li><a href="{{route('howitwork')}}">How Its Works</a></li>
                                    <li><a href="{{route('career')}}">Careers</a></li>
                                    <li><a href="{{route('faq')}}">FAQ's</a></li>
                                    <li><a href="{{route('contactus')}}">Contact Us</a></li>
                                </ul>
                                <div class="social-media">
                                    <a href="#" target="_blank">
                                        <span class="fa fa-facebook"></span>
                                    </a>
                                    <a href="#" target="_blank">
                                        <span class="fa fa-twitter"></span>
                                    </a>
                                    <a href="#" target="_blank">
                                        <span class="fa fa-linkedin"></span>
                                    </a>
                                   
                                </div>
                            </div>

                        </li>
                    </ul>
                </div>
                <div class="d-block d-md-none">
                    <nav class="navbar navbar-expand-md navbar-light cxm-navbar">
            <span class="navbar-brand mb-0 h1 d-md-none d-lg-none">
              <a href="#"><img class="img-fluid" src="{{asset('public/images/search-car.svg')}}"></a> <a href="#"><img
                            class="img-fluid" src="{{asset('public/images/service-car.svg')}}"></a>
              
              <a href="#"><span class="fa fa-comments"></span></a> <a href="#"><span
                            class="fa fa-map-marker"></span></a> <a href="login.php"><span class="fa fa-sign-in"></span></a>
            </span>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon fc1"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Who We Are</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">How Its Works</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Careers</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">FAQ's</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>

        </div>
    </div>
</div>


@if(Session::has('geo_location_session'))
		
 @else
		
 <script type="text/javascript">

  $( document ).ready(function() {
	  
	 
	  
	  
	if (navigator.geolocation) {
	

 	navigator.geolocation.getCurrentPosition((loc) => {
		
		 var lat = loc.coords.latitude;
		 var long = loc.coords.longitude;

		 console.log('##### Lat Long #####');
		 console.log('lat: '+ lat);
		 console.log('long: '+ long);

		$.ajax({
			type:'POST',
			data: {
			"_token": "{{ csrf_token() }}",	
			 get_lat: lat,
			 get_long: long
            },
			url:'{{url('get/auto/location')}}',
			
			success:  function () {
			
			location.reload(true);
			
			
			}
			
			}); 
	
	});
	

	
  } 
  else {
  return false;
  }



  
}); 
 
 
 
</script>
@endif


<script>

	$('html').click(function() {
	   $('.btn-close').trigger('click');
	   $('.menu-overlay').removeClass('show');
	   
	});

	$('.menu-box').click(function(event){
		event.stopPropagation();
	});
	
	
	
	
	
	
	
	/*
    $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $(".navbar-collapse").hasClass("navbar-collapse in");
        if (_opened === true && !clickover.hasClass("navbar-toggle")) {
            $(".btn-close").click();
        }
    });
});
 */

// $(document).click(function(e) {
	// if (!$(e.target).is('.navbar-collapse')) {
    	// $('.navbar-toggle').collapse('hide');	    
    // }
// });
/* $(document).on('click','body',function(){
	$('.collapse').collapse('hide');
})
 */
</script>

 
<div class="menu-overlay collapse"></div>
