<br>
<h3>Request for Best Offer Sent!</h3> 

<p><b>Thanks {{$data['name']}} .</b> Request Best Offer for this car has been sent to the Dealership without disclosing your contact details. <br> 

</p>

<p>The Dealership will now begin the process of preparing the Best Offer for your requested car.</p>
<br>
<p>A notification will be sent to your email when the Dealership submits an Offer and it will appear in your User Profile. The submitted Offer can be accepted or countered.
</p>
<br>
<p>
It is also possible to explore similar cars by visiting the link below. Send up to 4 more more Rest Best Offers on similar cars to make it easy to compare terms from up to 5 Dealerships before you sign the dotted line for that Perfect Car!.
</p>

<br>

<a href="{{url('search-car.html?filter_search=filter&vin=')}}{{$data['similar_car']}}"> Similar Cars Link </a>

<br>

<b>Thank You Choosing <a href="https://ottoguide.com">ottoguide.com</a></b><br>

<b>OTTO Guide Team</b><br>





