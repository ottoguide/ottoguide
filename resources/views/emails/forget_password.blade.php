<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Forgotten Password</title>
<style>
.link>a{
	color:#E14D57 !important;
}
</style>
</head>
<body style="background:#F2F2F2;">
<table align="center" width="600" border="0" cellpadding="5" bgcolor="#ddd">
  <tr>
    <td align="center" bgcolor="#eaa54b"><p style="font-family:Verdana, Geneva, sans-serif; font-size:24px; color:#FFF;"><b>OTTO</b> GUIDE</p></td>
  </tr>
  <tr>
    <td bgcolor="#4a7fc3" style="color:#fff">
    <p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF;">Dear , {{$data['dealer_name']}}</p>
    <p style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#fff;">You can change your password by entering the following pin:</p>
    <p style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:color:#fff;">{{$data['pin']}}</p>  
    </td>
  </tr>
  <tr>
    <td bgcolor="#4a7fc3">
    	<p style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#fff">Regards,<br/>Otto Guide.</p>
    </td>
  </tr>
  <tr>  
  </tr>
  <tr>
    <td align="center" bgcolor="#eaa54b"><p style="font-family:Verdana, Geneva, sans-serif; font-size:10px; color:#FFF;">{{date("Y")}} Otto Guide</p>
    </td>
  </tr>
</table>
</body>
</html>