<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>User Registration</title>
<style>
.link>a{
	color:#E14D57 !important;
}
a{
	color:#ccc;
}
</style>
</head>
<body style="background:#F2F2F2;">
<table align="center" width="600" border="0" cellpadding="2" bgcolor="#ddd">
  <tr>
   <td align="center" bgcolor="#eaa54b" colspan="2"><p style="font-family:Verdana, Geneva, sans-serif; font-size:24px; color:#FFF !important;"><b>OTTO</b> GUIDE</p></td>
  </tr>
  <tr>
    <td bgcolor="#4a7fc3" style="color:#FFF !important" colspan="2">
    <p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Dear , {{$data['name']}}</p>
    <p style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#FFF !important;">Thank you for signing up on ottoguide.com</p>
    <p style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:color:#FFF !important;">To activate your account please click the link below.</p>  
    <p style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:color:#FFF !important;"><a href="{{url('/confirm/registration/'.$data['confirmation_code'])}}" target="_blank">Activation Link</a></p> 
	</td>
	 
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> Registration Purpose</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{str_replace('_',' ',$data['status'])}}</p></td>
  </tr>
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> From Which Car</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['car_title']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> Car Link</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">
    <a href="{{url('car/detail')}}/{{$data['car_id'].'?filter_search=filter&radius='.$data['radius'].'&latitude='.$data['latitude'].'&longitude='.$data['longitude']}}" target="_blank">
    Click Here To View</a>             
  </p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> Similar Car </p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">
  <a href="{{url('search-car.html')}}?filter_search=filter&vin=<?=$data['car_vin']?>&radius=99999&latitude=<?=$data['latitude']?>&longitude=<?=$data['longitude']?>" target="_blank">
   Click Here To View Similar Cars</a>             
  </p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> From Which Car</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['car_title']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> Make</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['make']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> Modal</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['modal']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> Trim</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['trim']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> Body Type</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['body']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> Vehicle Type</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['vehicle_type']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> Miles</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['miles']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;"> Inventory Type</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['inventory_type']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Color</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['color']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Dealer ID</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['dealer_id']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Dealer Name</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['dealer_name']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Dealer Phone</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['dealer_phone']}}</p></td>
  </tr>
  
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Dealer City</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['dealer_city']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Dealer Street</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['dealer_street']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Dealer State</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['dealer_state']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Dealer Latitude</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['dealer_lat']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Dealer Longitude</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['dealer_long']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Dealer Zipcode</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['dealer_zip']}}</p></td>
  </tr>
  
  <tr bgcolor="#4a7fc3" style="color:#FFF !important">
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">Dealer Website</p></td>
  <td><p style="font-family:Verdana, Geneva, sans-serif; font-size:16px; color:#FFF !important;">{{$data['dealer_website']}}</p></td>
  </tr>
    
  <tr>
    <td bgcolor="#4a7fc3" colspan="2">
    	<p style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#FFF !important">Regards,<br/>Otto Guide.</p>
    </td>
  </tr>
  <tr>  
  </tr>
  <tr>
    <td align="center" bgcolor="#eaa54b" colspan="2"><p style="font-family:Verdana, Geneva, sans-serif; font-size:10px; color:#FFF !important;">{{date("Y")}} Otto Guide</p>
    </td>
  </tr>
</table>
</body>
</html>