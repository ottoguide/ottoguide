<br>
<b>We apologize! this Dealership is currently not accepting Request Best Offer.</b>

<p>
Thanks {{$data['name']}}! Unfortunately this Dealership is currently not accepting Request Best Offer but we have notified them of your interest in this car without disclosing your contact details.
</p>
<p>A notification will be sent to your email when the Dealership submits an Offer and it will appear in your User Profile. The submitted Offer can be accepted or countered.</p>

<p> 
It is also possible to explore similar cars by visiting the link below. Send up to 5 Request Best Offers on similar cars to make it easy to compare terms from 5 Dealerships before you sign the dotted line for that Perfect Car!
</p>

<br>

<a href="{{url('search-car.html?filter_search=filter&vin=')}}{{$data['similar_car']}}"> Similar Cars Link </a>

<br>

<p><a href="{{url('/car/detail/')}}/{{$data['car_link']}}"target="_blank"> Concern Car Link </a></p>

<br>

<b>Thank you for choosing ottoguide.com to Find you Perfect Car!</b><br>
<b>OTTO Guide Team</b><br>


