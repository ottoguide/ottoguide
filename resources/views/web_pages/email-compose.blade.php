@extends('layouts.web_pages')
@section('content')

<style>
.bg-white{
	margin-bottom: 2%;
    padding:1%;
    border-top: 3px solid #127ba3;
	border-right: 1px solid #ccc;
	border-left: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	border-radius: 5px;	
  }
  input[type=checkbox] {
    display: inline-block;
    *display: inline;
    vertical-align: middle;
    margin: 0;
    padding: 0;
    width: 16px;
    height: 16px;
    border: none;
    cursor: pointer;
    color: #fff;
    -webkit-background-size: 240px 24px;
    background-size: 240px 24px;
}
 .badge {
    display: inline-block;
    padding: 0.50em 0.75em;
    font-size: 80%;
    font-weight: 500;
	}
</style>

  <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
  <script type="text/javascript">
//<![CDATA[
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
  //]]>
  </script>

<?php if(Auth::user()){ ?>

 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Compose New Message</h1>            
          </div>
        </div>
      </div>      
    </div>
    

	
    <div class="py-5">
		<div class="container">	 
	   
	    <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
	   
		</div>
    <div class="container">
	
      <div class="row">   
    		 <!-- client sidebar -->
    		  @include('client.emailbox_sidebar')
			  
		 <div class="col-md-9 bg-white">
	
		 
		 <br>
          <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
                <input class="form-control" placeholder="To:" value="">
              </div>
              <div class="form-group">
                <input class="form-control" placeholder="Subject:">
              </div>
              
			   <div class="form-group">
                <textarea class="form-control" style="height:300px; padding:15px;" placeholder=""></textarea>
              </div>
              
            
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary"  onclick="submitForm(this);"><i class="fa fa-envelope-o"></i> Send</button>
              </div>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
			  
			  
			  
			  
			  
			  
			  <ul class="pagination justify-content-center">      
            <?php
            /*   $links = $car_datas->render(); 
              $links = str_replace("<a", "<a class='page-link ' ", $links);
              $links = str_replace("<li", "<li class='page-item' ", $links);
              $links = str_replace("<span", "<span class='page-link'",$links);
              echo $links; */
            ?>
          </ul>  
				
			
			
		
            </div>
          </div>  
	
		    
        </div>
      </div>      
    </div>
	
	
<?php } 
else{
	echo "CLient ARea";	
}
?>

<script>
    function submitForm(btn) {
        // disable the button
        btn.disabled = true;
        // submit the form    
        btn.form.submit();
    }
</script>
	@endsection