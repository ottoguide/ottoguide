  <style>
  .btn-circle {
    width: 75px;
    height: 75px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 50%;
}

  hr{
      margin-bottom: 4px !important;
  }
.cxm-img-badge-cls a {
	color: #fff;
}

  </style>
  @if(!empty($all_car_datas))
      <div class="row">
          <div class="col-sm-12">
            <div class="cxm-tabs">             
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="overview" role="tabpanel">
              <div class="form-row">
                <!-- START FOREACH -->          
                    @foreach($all_car_datas as $no => $data)  
                    <div class="col-sm-3 col-md-3 col-lg-3">            
                      <div class="cxm-vihecle-box card border-secondary mb-4">
                      <div class="cxm-img">
					  <div class="cxm-img-badge-cls">
                        <a class="<?php echo $data->id;?>-car" href="javascript:rbo_car_compare_delete('<?php echo $data->id;?>');">
                        <span class="fa fa-remove"></span></a>
					  </div>
                            <img style="width: 100% !important; " class="img-fluid" src="@if(!empty($data->media->photo_links[0]) > 0)
                            {{$data->media->photo_links[0]}}
                            @else
                            {{asset('public/images/no-image.jpeg')}}
                            @endif">                          
                      </div> 
                      <div class="text-center lh16">
                        <b> @if(!empty($data->heading) > 0)
                          {{$data->heading}}
                          @endif
                        </b>
                        </div>    
                        <div class="card-body p-2">                    
                          <hr class="hr1">
                          <div class="form-row">
                            <div class="col-12 mb-2">
                              <b>Price:</b><span class="price">
                                 @if(!empty($data->price) > 0)
                                  $ {{number_format((float)$data->price)}}
                                  @else
								  N/A
								  @endif
								</span>
							     <hr>
                                <b>Miles:</b> <span class="miles"> @if(!empty($data->miles))
                                        {{$data->miles}}
                                    @endif
								 </span>
                                <hr>
								<b>Year:</b> <span class="year"> @if(!empty($data->build->year) > 0)
								 {{$data->build->year}}
								 @endif
								 </span>
                                <hr>
                                <b>Type:</b> <span class="type"> @if(!empty($data->inventory_type))
                                        {{ucfirst($data->inventory_type)}}
                                    @endif
								 </span>
                                <hr>
                                <b>Exterior Color:</b> <span class="type"> @if(!empty($data->exterior_color))
                                        {{ucfirst($data->exterior_color)}}
                                    @endif
								 </span>
                                <hr>
                                <b>Interior Color:</b> <span class="type"> @if(!empty($data->interior_color))
                                        {{ucfirst($data->interior_color)}}
                                    @endif
								 </span>
                              <div>
						  </div>
                        </div>
                           
                          </div>                        
                        </div>
                      </div>          
                    </div>
                  <!-- END FOREACH -->                                         
                  @endforeach
               
			      <div class="col-sm-3 col-md-3 col-lg-3">
					<?php 
					$count_compare = count($all_car_datas);
					
					if($count_compare < 4 ){?>
					<button type="button" class="btn btn-info btn-circle btn-xl"data-dismiss="modal">Add More<br>Cars</button>
					<?php } ?>

				    </div>
                   </div>

                    <div class="col-md-12 text-center">

                        @endif
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>

	<script>
	 $(document).on("click",".btn-circle",function() {
        location:reload();
    });
	</script>
