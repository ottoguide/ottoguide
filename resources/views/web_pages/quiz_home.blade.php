@extends('layouts.web_pages')
@section('content')


<script  src="js/Questioner.js"></script>
<link rel='stylesheet' href='css/questioner2.css'>
<style>
.questioner li {
 position: relative;
 opacity: 1;
}
.questioner {
	  font-family: 'Montserrat', sans-serif; 
	  background-image: url('../images/questioner-back.jpg');
	  background-repeat: no-repeat;
	  background-attachment: fixed;
	  background-position: center; 
	}
	#ads {
    margin: 30px 0 30px 0;
   
}

#ads .card-notify-badge {
        position: absolute;
        left: -10px;
        top: -20px;
        background: #f2d900;
        text-align: center;
        border-radius: 30px 30px 30px 30px; 
        color: #000;
        padding: 5px 10px;
        font-size: 14px;

    }

#ads .card-notify-year {
        position: absolute;
        right: -10px;
        top: -20px;
        background: #ff4444;
        border-radius: 50%;
        text-align: center;
        color: #fff;      
        font-size: 14px;      
        width: 50px;
        height: 50px;    
        padding: 15px 0 0 0;
}


#ads .card-detail-badge {
        text-transform: uppercase;	
        text-align: center;
        border-radius: 30px 30px 30px 30px;
        color: #127ba3;
        padding: 5px 10px;
        font-size: 18px;
		font-weight:bold;	
    }

   

#ads .card:hover {
            background: #fff;
            box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
            border-radius: 4px;
            transition: all 0.3s ease;
        }

#ads .card-image-overlay {
       font-size: 20px; 
    }


#ads .card-image-overlay span {
            display: inline-block;              
        }


#ads .ad-btn {
        width: 150px;
        height: 40px;
        border-radius: 80px;
        font-size: 14px;
        line-height: 35px;
        text-align: center;
        border: 3px solid #e6de08;
        display: block;
        text-decoration: none;
        margin: 20px auto 1px auto;
        color: #000;
        overflow: hidden;        
        position: relative;
        background-color: #e6de08;
    }

#ads .ad-btn:hover {
            background-color: #e6de08;
            color: #1e1717;
            border: 2px solid #e6de08;
            background: transparent;
            transition: all 0.3s ease;
            box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
        }

#ads .ad-title h5 {
   font-size: 16px;
    }
.rounded{
border-radius: 8px;	
}



	
</style>	
   <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	
    <div class="header-margin py-4 bg-secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1> Questioner Search</h1>
                </div>
            </div>
        </div>
    </div>

	
    <div class="container-fluid questioner">
      
	<div class="container">
    <br>
	<br>
	
	
 @if(count($get_quizes))	
	
  <div class="row" id="ads">
    <!-- Category Card -->

  @foreach($get_quizes as $get_quiz)
   
   <div class="col-md-4">
        <div class="card rounded">
            <div class="card-image">
                <span class="card-notify-badge">Quiz: <b>{{$get_quiz->id}}</b></span>
               
                <img class="img-fluid" src="/images/quiz-home.png" alt="Alternate Text" />
            </div>
            <div class="card-image-overlay m-auto"> <br>
                <span class="card-detail-badge">@if(!empty($get_quiz->title) > 0) {{$get_quiz->title}} @endif</span>
            </div>
            <div class="card-body text-center">
                <div class="ad-title m-auto">
                    <h5>@if(!empty($get_quiz->description) > 0) {{$get_quiz->description}} @endif</h5>
                </div>				
				<a class="ad-btn" href="{{route('questioner-search', ['quiz_id' => encrypt($get_quiz->id)])}}">Let's Go</a>
	
				
            </div>
        </div>
    </div>
    
	@endforeach	

	</div>
	
	
	@else

		<h3 align="center">Coming Soon</h3>

	@endif
	
	
	 
	</div>

	<br>
	<br>	
    </div>      
 
	
	
@endsection 
