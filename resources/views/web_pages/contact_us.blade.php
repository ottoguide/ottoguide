@extends('layouts.web_pages')
@section('content')
<div class="header-margin py-4 bg-secondary">
  <div class="header-margin py-5">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 offset-lg-2">
          <div class="card">
             <div class="card-header font-weight-bold fs18">Contact Us</div>
               <form method="post" action="{{ action('HomeController@contact_us_submit') }}" accept-charset="UTF-8">
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="name">Your Name <span class="text-danger">*</span></label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><span class="fa fa-user"></span></span>
                                </div>
                                <input type="text" name="user_name" class="form-control" placeholder="Your Name">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="email">Your Email <span class="text-danger">*</span></label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><span class="fa fa-envelope"></span></span>
                                    </div>
                                    <input type="text" name="user_email" class="form-control" placeholder="Your Email">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <label for="phn">Your Telephone <span class="text-danger">*</span></label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><span class="fa fa-phone"></span></span>
                                    </div>
                                    <input type="text" name="user_phone" class="form-control" placeholder="Telephone No">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label for="pcode">Your Zip Code <span class="text-danger">*</span></label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><span class="fa fa-map-marker"></span></span>
                                    </div>
                                    <input type="text" name="user_zip" class="form-control" placeholder="Postal Code">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <!-- <label for="att">Attachment</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><span class="fa fa-link"></span></span>
                                    </div>
                                    <input type="file" class="form-control" placeholder="Postal Code">
                                </div> -->

                                <label for="msg">Your Message</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><span class="fa fa-list"></span></span>
                                    </div>
                                    <textarea name="user_message" class="form-control" rows="8" placeholder="Your Message"></textarea>
                                </div>

                                <button type="submit" class="btn btn-primary"><span class="fa fa-send-o"></span> Send</button>
                            </div>
                        </div>
                    </div>
                 </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection