@extends('layouts.web_pages')@section('content')
    <div class="header-margin py-5">
        <div class="container">

            @include('dealer.includes.nav', array(
            'tab' => 'settings',
            'page_title' => 'Dealership Settings'
            ))

            <div class="row bg-secondary">
                <div class="col-md-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item"><a class="nav-link active tab_email_leeds_btn" data-toggle="tab"
                                                href="#tab_trending_local_searches" role="tab" aria-selected="false">General
                                Setting</a></li>
                        <li class="nav-item"><a class="nav-link tab_phone_leeds_btn phone" data-toggle="tab"
                                                href="#tab_phone_leeds" role="tab" aria-selected="false">Company General
                                Information</a></li>
                        <li class="nav-item"><a class="nav-link tab_sms_leeds_btn" data-toggle="tab"
                                                href="#tab_sms_leeds" role="tab" aria-selected="false">Communication
                                Setting</a></li>
                        <li class="nav-item"><a class="nav-link tab_chat_leeds_btn " data-toggle="tab"
                                                href="#tab_chat_leeds" role="tab" aria-selected="false">User Setting</a>
                        </li>
                        <li class="nav-item"><a class="nav-link tab_chat_leeds_btn " data-toggle="tab"
                                                href="#tab_chat_leeds" role="tab" aria-selected="false">Manager
                                Setting</a></li>
                        <li class="nav-item"><a class="nav-link tab_chat_leeds_btn " data-toggle="tab"
                                                href="#tab_chat_leeds" role="tab" aria-selected="false">Subscription
                                Setting</a></li>
                    </ul>
                    <div class="tab-content card" id="myTabContent">                    <!-- Search by filter -->
                        <!--                    <div class="row padding">                    <div class="col-md-5 col-lg-5"><label><b>From</b></label><input type="date" class="form-control"></div>								<div class="col-md-5 col-lg-5"><label><b>To</b></label><input type="date" class="form-control"></div>										<div class="col-md-2 col-lg-2"><br><button class="btn btn-small btn-primary">Filter</button>										&nbsp;					<button class="btn btn-small btn-primary">Export</button>					</div>					</div>-->
                        <div class="card-body tab-pane fade show active" id="tab_trending_local_searches"
                             role="tabpanel" aria-labelledby="tab_trending_local_searches">
                            <div class="row mt-5">
                                <div class="col-md-12">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)

                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <h4>Lead Email Addresses</h4>
                                    <p>When a customer fills out the <strong>Contact Dealer</strong> from on Otto Guide
                                        We will send the email on address.</p>
                                    <div class="emails_section">
                                        <form action="{{route('dealer-settings-update-email')}}" method="post" id="edit_email_form">
                                            <input type="hidden" name="dealer_email" class="email">
                                            {!! Form::hidden('_token', csrf_token()) !!}
                                        </form>
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Email Address</th>
                                                <th>Email Format</th>
                                                <th>For Inventory</th>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="edit_email_area">{{$dealer_data->dealer_email}}</td>
                                                <td>ADF</td>
                                                <td>New & Used</td>
                                                <th>No Known Issues</th>
                                                <th>
                                                    <!--<button type="button" class="btn btn-success edit_email_btn">Edit Email</button>                                            <button type="button" class="btn btn-success edit_email_save_btn" style="display: none">Save Email</button>-->                                        </th>
                                            </tr> <?php                                                                        $dealer_email = explode(',', $dealer_data->dealer_other_emails);                                                                                    foreach($dealer_email as  $dealer_email){                                                                                    ?>
                                            <tr>
                                                <td class="edit_email_area">{{$dealer_email}}</td>
                                                <td>ADF</td>
                                                <td>New & Used</td>
                                                <th>No Known Issues</th>
                                                <th>
                                                    <!--<button type="button" class="btn btn-success edit_email_btn">Edit Email</button>                                            <button type="button" class="btn btn-success edit_email_save_btn" style="display: none">Save Email</button>-->                                        </th>
                                            </tr> <?php } ?>


                                            </tbody>

                                        </table>
										
							<button type="button" class="btn btn-success add_email_btn">Add New Email</button>
	
										
                                    </div>
                                    <div class="phone_section mt-4">
                                      
						 <form action="{{route('dealer-settings-update-phone')}}" method="post">
						 
                                {!! Form::hidden('_token', csrf_token()) !!}
								
                                            <div><strong>Phone Numbers</strong></div>
                                            <br> <?php
                                            $dealer_phone = explode(',', $dealer_data->dealer_phone);
                                            foreach($dealer_phone as $dealer_phones){
                                            ?>
                       <input type="text" class="form-control" name="dealer_phone"value="{{$dealer_phones}}"> <br>
					  
							<?php } ?>
					   
                            <button type="submit" class="btn btn-primary mt-2">Update Sales Phone Number </button><p></p>
								<button type="button" class="btn btn-success add_phone_btn">Add New Phone</button>
                                        </form>
                                    </div>
                                    <div class="website_section mt-4">
                                        <form action="{{route('dealer-settings-update-website')}}"
                                              method="post"> 
											  {!! Form::hidden('_token', csrf_token()) !!}
                                            <strong>Website:</strong>
                                            <div>Website Url:</div>
                                            <input type="text" class="form-control" name="dealer_website"
                                                   value="{{$dealer_data->dealer_website}}">
                                            <div class="checkbox mt-2"><label> <input type="checkbox" name="has_vdp">
                                                    Enable deep linking to your VDP pages </label></div>
                                            <p class="small">(if we are able to deep to dealer website.It will take
                                                effect in 24 hours checking the box.)</p>
                                            <div class="checkbox mt-2"><label> <input type="checkbox"
                                                                                      name="override_url"> Over ride
                                                    automatically generated tracking URL </label></div>
                                            <button type="submit" class="btn btn-primary">Save Settings</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body tab-pane fade" id="tab_phone_leeds" role="tabpanel"
                             aria-labelledby="tab_trending_local_searches">
							 <h2>Company General Information</h2>
							 </div>
							 
                        <div class="card-body tab-pane fade" id="tab_sms_leeds" role="tabpanel"
                             aria-labelledby="tab_trending_local_searches">
							 <h3>Communication Setting</h3>
							 </div>
							 
                        <div class="card-body tab-pane fade" id="tab_chat_leeds" role="tabpanel"
                             aria-labelledby="tab_trending_local_searches">
							 <h3>User Setting</h3>
							 
							 </div>
                    </div>
                </div>
            </div>
        </div>   


		<!-- Lightbox for add email -->
		
        <div class="modal fade" id="add_email_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">Add Email</h4></div>
                    <div class="modal-body">
                        <form action="{{route('dealer-settings-add-email')}}" method="post" id="add_email_form">
						 {!! Form::hidden('_token', csrf_token()) !!}
                            <div class="form-group"><label>Email Address</label>
                                <input type="text" class="form-control" name="email_address"></div>
                            <div class="form-group"><label>Email Format</label>
                                <input type="text" class="form-control" name="email_format"></div>
                            <div class="form-group">
                                <label>For Inventory</label>
                                <select name="" class="form-control">
                                    <option value="new_and_used">New & Used</option>
                                    <option value="new">New</option>
                                    <option value="used">Used</option>
                                </select></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success" form="add_email_form">Save</button>
                    </div>
                </div>
            </div>
        </div> <!-- /.modal -->
		
		
		<!-- Lightbox for add PHONE -->
		
        <div class="modal fade" id="add_phone_modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header"><h4 class="modal-title">Add Phone</h4></div>
                    <div class="modal-body">
                        <form action="{{route('dealer-settings-add-phone')}}" method="post" id="add_phone_form">
						 {!! Form::hidden('_token', csrf_token()) !!}
                            
                            <div class="form-group"><label>Phone Number</label>
                                <input type="text" class="form-control" name="dealer_other_phone"></div>
                            
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success" form="add_phone_form">Save</button>
                    </div>
                </div>
            </div>
        </div> <!-- /.modal -->
		
		
		
		
		
		
        <script>
            $(document).on('click', '.add_email_btn', function (e) {
                e.stopImmediatePropagation();
                $('#add_email_modal').modal({backdrop: 'static', keyboard: false, show: true}); // open lightbox
            });
			
			 $(document).on('click', '.add_phone_btn', function (e) {
                e.stopImmediatePropagation();
                $('#add_phone_modal').modal({backdrop: 'static', keyboard: false, show: true}); // open lightbox
            });
			
			
            // $(document).on('click', '.edit_email_btn', function (e) {
            // e.stopImmediatePropagation();
            // var email = '{{$dealer_data->dealer_email}}';
            // $('.edit_email_area').html('<input type="text" class="form-control edit_email" value="'+email+'">');
            // 			$('.edit_email_btn').hide();
            // $('.edit_email_save_btn').show();
            // });
            // $(document).on('click', '.edit_email_save_btn', function (e) {
            // e.stopImmediatePropagation();
            // var email = $(this).parents('tr').find('.edit_email').val();
            // $('#edit_email_form .email').val(email);
            // $('#edit_email_form').submit();
            // $('.edit_email_area').html(email);
            // $('.edit_email_btn').show();
            // $('.edit_email_save_btn').hide();
            // });
        </script>
    </div>
@endsection