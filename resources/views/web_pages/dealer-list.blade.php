
@extends('layouts.web_pages')
@section('content')
	<style>
		.table tbody > tr > td {
			font-size:13px;
			background: #f5f5f5;
			background-color: rgb(245, 245, 245);
			border-top: 2px solid #fff;
			border-right: 2px solid #fff;
			vertical-align: middle;
			padding: 9px 7px;
			overflow: hidden;
		}

		.card-header{
			font-family: 'Century Gothic',CenturyGothic,AppleGothic,sans-serif;
			background: #127ba3;
			color:#fff !important;
		}
		.card-header b{
			color:#fff;
		}

		.services{
			padding:10px;
		}

		.refine-search .card-header a {
			color: #fff;
		}
		
		.map-border{
		  border-top: 3px #158CBA solid;		
		}
		
		
		.cxm-content p{
			margin-bottom: 0.3px;
			margin-top: 8px;
		}
	</style>
	
	
	<style>
		
		.checkbox{
			font-size: 15px;
			color:#444;
			font-weight:bold;
		}
		.collapse{
			margin-bottom: 5px;
		}

		.form-field{
			line-height: 30px;
			margin-bottom: 10px;

		}

		.multi-select{
			padding: 10px;
		}

		
		
		.collapsed{
		 
		 width:100%;	
			
		}
		
		 .checkbox,.cxm-content p {
			
			font-family: 'Century Gothic',CenturyGothic,AppleGothic,sans-serif;
			padding:5px;
			border:1px solid #e2e2e2;
			border-radius: 5px;
			background: rgb(255,255,255);
			background: -moz-linear-gradient(left, rgba(255,255,255,1) 0%, rgba(243,243,243,1) 50%, rgba(237,237,237,1) 51%, rgba(255,255,255,1) 100%);
			background: -webkit-linear-gradient(left, rgba(255,255,255,1) 0%,rgba(243,243,243,1) 50%,rgba(237,237,237,1) 51%,rgba(255,255,255,1) 100%);
			background: linear-gradient(to right, rgba(255,255,255,1) 0%,rgba(243,243,243,1) 50%,rgba(237,237,237,1) 51%,rgba(255,255,255,1) 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff',GradientType=1 );
					
		 }

	 .tab-width{
		 width:100%;
	 }
	 .fa-check{
	  color: #2E88C9;
	   padding-top: 5px;
     display:none;	   
	 }
	 
	 .margin{
	  margin: 5px;
	  border-right: 1px dotted #e2e2e2;	  
	 }
	
	.padding-left{
		padding-right: 20px;
	}
  
  .services-height{
	height:180px;
	overflow-y: auto;
	overflow-x: hidden;	
   }
   
::-webkit-scrollbar {
    width: 7px;
	margin-top:3px;
}
::-webkit-scrollbar-track {
    background: #f1f1f1; 
}
 	

::-webkit-scrollbar-thumb {
    background: #999; 
    border-radius: 10px;
}


::-webkit-scrollbar-thumb:hover {
    background: #2a89ca; 
}	

.form-control:disabled, .form-control[readonly] {
    background-color: #fff;
    color: #222;
    opacity: 1;
}
.img-fluid {

    border-radius: 5px;
}
	</style>
	
	
	<br>
	
	<div class="header-margin">
		<div class="container">


			@if($errors->any())
				<p class="alert alert-success">Search Saved successfully </p>
			@endif


			<div class="row">
				<div class="col-sm-12">
					<div class="card refine-search">
						<div class="card-header font-weight-bold">
							<a data-toggle="collapse" href="#refineSearch" role="button" aria-expanded="false" aria-controls="refineSearch"><b>Your Selected Services Details</b></a>
						</div>
						<div class="collapse show" id="refineSearch">
							<div class="">
								@if(!empty(Session::get('services')))
							
				<div class="row">
				<div class="col-sm-6">
						
					<div class="row margin">
					<div class="col-sm-11">
						
					<div class="cxm-img">
                      
					  @foreach($all_dealers as $all_dealer) @endforeach

					  <img class="img-fluid" src=" @if(!empty($car_image)){{$car_image}}@endif"></a>
					  
                    </div>
						
						</div>
						
						<div class="col-sm-11">
						 
						 
						 <div class="cxm-content">
						 <div class="row">
                             <p class="col-sm-6"><i class="fa fa-arrow-circle-right"></i> <b>Year:</b>    @if(!empty(Session::get('services')['Year']) > 0){{Session::get('services')['Year']}}@endif</p>
							 <p class="col-sm-6"><i class="fa fa-arrow-circle-right"></i> <b>Make:</b>    @if(!empty(Session::get('services')['Make']) > 0){{Session::get('services')['Make']}}@endif</p>
                             <p class="col-sm-6"><i class="fa fa-arrow-circle-right"></i> <b>Model:</b>   @if(!empty(Session::get('services')['Model']) > 0){{Session::get('services')['Model']}}@endif</p>
                             <p class="col-sm-6"><i class="fa fa-arrow-circle-right"></i> <b>Trim:</b>    @if(!empty(Session::get('services')['Trim']) > 0){{Session::get('services')['Trim']}}@endif</p>
						     <p class="col-sm-6"><i class="fa fa-arrow-circle-right"></i> <b>Mileage:</b> @if(!empty(Session::get('services')['Mileage']) > 0){{Session::get('services')['Mileage']}}@endif</p>
							 <p class="col-sm-6"><i class="fa fa-arrow-circle-right"></i> <b>Radius:</b> 
							
							<select class="form-control form-control-sm w-auto d-inline-block mb-1" name="radius" id="radius_select">
							<option value="">Any distance</option>
							<option value="10" {{!empty(Session::get('services')['Radius']) && Session::get('services')['Radius'] == '10' ? 'selected' : '' }}> 10 miles</option>
							<option value="20" {{!empty(Session::get('services')['Radius']) && Session::get('services')['Radius'] == '20' ? 'selected' : '' }} >20 miles</option>
							<option value="30" {{!empty(Session::get('services')['Radius']) && Session::get('services')['Radius'] == '30' ? 'selected' : '' }}>30 miles</option>
							<option value="40" {{!empty(Session::get('services')['Radius']) && Session::get('services')['Radius'] == '40' ? 'selected' : '' }}>40 miles</option>
							<option value="50" {{!empty(Session::get('services')['Radius']) && Session::get('services')['Radius'] == '50' ? 'selected' : '' }}>50 miles</option>
							</select>
												
							 </p>
                               
						 </div>	
						 </div>
						</div>
						
						</div>
						
						</div>
						
						
				<div class="col-sm-5 padding-left services-height">
				
				<!-- selected services -->	

				@include('web_pages.includes.selected-services')

		    	</div>

						
						
				</div>
						
					@endif
							</div>
							<br>
							<a href="{{url('car/services')}}" class="btn btn-primary pull-right">Edit Services </a>
						</div>


					</div>


				</div>
			</div>

			
		</div>
	</div>


	<div class="py-3">
		<div class="container">

			<div class="message" width="50%" align="center">
				@if (session('message'))
					<div class="alert alert-success" width="50%">
						{{ session('message') }}
					</div>
				@endif
			</div>

			<div class="message" width="50%" align="center">
				@if (session('error'))
					<div class="alert alert-danger" width="50%">
						{{ session('error') }}
					</div>
				@endif
			</div>


			<div class="row">

				<div class="col-lg-12">

					<div class="card bg-light">
						<div class="card-body p-2">
							<h3>Dealers List</h3>
						</div>
					</div>

					<div class="main-box clearfix">
						
						
						<div class="row custom-padding">


						
						
						
						
						@if(!empty($all_dealers) > 0)							
							@foreach($all_dealers as $dealer_detail)
						
								<?php
								
									$email_address="syedshaharif@gmail.com";
									
//									$dealerRecord = $dealer_model::findOrNew($dealer_detail->dealer->id);
//									$dealerRecord->dealer_id = $dealer_detail->dealer->id;
//									$dealerRecord->dealer_latitude = (isset($dealer_detail->dealer->latitude) ? $dealer_detail->dealer->latitude : '');
//									$dealerRecord->dealer_longitude = (isset($dealer_detail->dealer->longitude) ? $dealer_detail->dealer->longitude : '');
//									$dealerRecord->dealer_country = (isset($dealer_detail->dealer->country) ? $dealer_detail->dealer->country : '');
//									$dealerRecord->dealer_website = (isset($dealer_detail->dealer->website) ? $dealer_detail->dealer->website : '');
//									if (!$dealerRecord->exists) {
//										$dealerRecord->dealer_name = $dealer_detail->dealer->name;
//										$dealerRecord->dealer_email = $email_address;
//										$dealerRecord->dealer_phone = (isset($dealer_detail->dealer->city) ? $dealer_detail->dealer->city : '');
//									}
//									$dealerRecord->dealer_street = (isset($dealer_detail->dealer->street) ? $dealer_detail->dealer->street : '');
//									$dealerRecord->dealer_city = (isset($dealer_detail->dealer->city) ? $dealer_detail->dealer->city : '');
//									$dealerRecord->dealer_zip = (isset($dealer_detail->dealer->zip) ? $dealer_detail->dealer->zip : '');
//									$dealerRecord->save();


                                    $dealerRecord = $dealer_model::findOrNew($dealer_detail->id);
                                    $dealerRecord->dealer_id = $dealer_detail->id;
                                    $dealerRecord->dealer_latitude = (isset($dealer_detail->latitude) ? $dealer_detail->latitude : '');
                                    $dealerRecord->dealer_longitude = (isset($dealer_detail->longitude) ? $dealer_detail->longitude : '');
                                    $dealerRecord->dealer_country = (isset($dealer_detail->country) ? $dealer_detail->country : '');
                                    $dealerRecord->dealer_website = (isset($dealer_detail->website) ? $dealer_detail->website : '');
                                    if (!$dealerRecord->exists) {
                                        $dealerRecord->dealer_name = $dealer_detail->seller_name;
                                        $dealerRecord->dealer_email = $email_address;
                                        $dealerRecord->dealer_phone = (isset($dealer_detail->city) ? $dealer_detail->city : '');
                                    }
                                    $dealerRecord->dealer_street = (isset($dealer_detail->street) ? $dealer_detail->street : '');
                                    $dealerRecord->dealer_city = (isset($dealer_detail->city) ? $dealer_detail->city : '');
                                    $dealerRecord->dealer_zip = (isset($dealer_detail->zip) ? $dealer_detail->zip : '');
                                    $dealerRecord->save();
								
								
								?>
			
					<div class="col-md-4 col-lg-4">
					
                    <div id="cxm-slider-pro" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                          
                   {{--<iframe src="https://maps.google.com/maps?q={{$dealer_detail->dealer->latitude}},{{$dealer_detail->dealer->longitude}}&hl=es;z=8&amp;output=embed" frameBorder="0" height="300" width="100%" class="map-border" target="_self"></iframe></a>--}}
                   <iframe src="https://maps.google.com/maps?q={{$dealer_detail->latitude}},{{$dealer_detail->longitude}}&hl=es;z=8&amp;output=embed" frameBorder="0" height="300" width="100%" class="map-border" target="_self"></iframe></a>
					</div>
                    
                       
                    </div>
                    <div class="cxm-content">
                       
                        {{--<hr class="hr1">--}}
                        {{--<div class="text-muted fs12"><span class="text-dark fs14">DEALER NAME :</span> &nbsp; @if(!empty($dealer_detail->dealer->name) > 0){{$dealer_detail->dealer->name}}@endif </div>--}}
                        {{--<div class="text-muted fs12"><span class="text-dark fs14">DEALER CITY :</span> &nbsp; @if(!empty($dealer_detail->dealer->city) > 0){{$dealer_detail->dealer->city}}@endif </div>--}}
                        {{--<div class="text-muted fs12"><span class="text-dark fs14">DEALER STREET :</span> &nbsp; @if(!empty($dealer_detail->dealer->street) > 0){{$dealer_detail->dealer->street}}@endif </div>--}}
                        {{--<div class="text-muted fs12"><span class="text-dark fs14">DEALER MAKE :</span> &nbsp;  @if(!empty($dealer_detail->build->make) > 0){{$dealer_detail->build->make}}@endif </div>--}}
						{{--<hr class="hr1">--}}

						<hr class="hr1">
						<div class="text-muted fs12"><span class="text-dark fs14">DEALER NAME :</span> &nbsp; @if(!empty($dealer_detail->seller_name) > 0){{$dealer_detail->seller_name}}@endif </div>
						<div class="text-muted fs12"><span class="text-dark fs14">DEALER CITY :</span> &nbsp; @if(!empty($dealer_detail->city) > 0){{$dealer_detail->city}}@endif </div>
						<div class="text-muted fs12"><span class="text-dark fs14">DEALER STREET :</span> &nbsp; @if(!empty($dealer_detail->street) > 0){{$dealer_detail->street}}@endif </div>
						<div class="text-muted fs12"><span class="text-dark fs14">DEALER MAKE :</span> &nbsp;  @if(!empty($services_details['Make'])){{$services_details['Make']}}@endif </div>
						<hr class="hr1">

						<form id="services_email" method="post" action="{{url('user-services-email.html')}}?email_subject=Car Services">
							{!! Form::hidden('_token', csrf_token()) !!}
							<input type="hidden" name="dealer_id"    class="dealer_id">
							<input type="hidden" name="dealer_name" class="dealer_name" >
						</form>

						<form id="services_chat" method="get" action="{{route('mychat')}}">
							{!! Form::hidden('_token', csrf_token()) !!}
							<input type="hidden" name="car_id"    value="">
							<input type="hidden" name="subject" value="Car Service" >
							<input type="hidden" name="dealer_id" value="{{$dealer_detail->id}}" >
							<input type="hidden" name="channel_name" value="" class="channel_name">
						</form>


						<input type="hidden" id="user-phone" value="{{Auth::user()->phone}}">
						<input type="hidden" class="dealer_phone" value="">
						
						
						<a href="javascript:services_email_modal('<?=$dealer_detail->id?>','<?=$dealer_detail->seller_name ?>');" class="btn btn-primary btn-lg btn-block">
						<span class="fa fa-comment"></span> Anonymous Contact </a>
						
                    <br><br>	
					</div>
							
					 </div>		
					
					
					
					
							<!-- Modal -->
<div class="modal fade" id="dealer-contact-modal" data-backdrop="static"  data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="contact-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contact-modal">Dealer Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <center>
                    @auth
					

                        <a href="javascript:void(0);" style="width: 50%;" class="contact-btn btn btn-block btn-primary" onclick="$('#num-div').show() , $('.contact-btn').hide();">
                            <i class="fa fa-phone "></i> Call Dealer</a><!--$car_detail->dealer->id-->
					<button type="submit" style="width: 50%;" form="services_chat" class="contact-btn btn btn-block btn-primary"><i class="fa fa-comments"></i> Text with Dealer</button>
					<button type="submit" style="width: 50%;" form="services_email" class="contact-btn btn btn-block btn-primary"><i class="fa fa-envelope"></i> Email to Dealer</button>
                         <div id="num-div" style="width: 50%;display: none;border: 1px dashed #ddd; margin: 10px;padding: 10px;">
                             <h5>Enter the number to call</h5>
                             <input type="text" id="user-phone" class="form-control" value="{{Auth::user()->phone}}" /><br>
                             <input type="hidden" id="dealer_phone" class="form-control" value="" />
							 <input type="hidden" class="dealer_id" class="form-control"  />
							 <input type="hidden" id="dealer_other_phone" class="form-control"/>
							 <br>
                            <a href="javascript:void(0);" style="width: 50%;" class="call-button btn btn-block btn-primary">
                             <i class="fa fa-arrow-right "></i> Go</a>
                         </div>
                        <div id="div1" style="display: none">
                            <span>This dealer is not in contact please see the similar cars</span>
                                <br>
                                <br>
                            <a href="{{url('/search-car.html?filter_search=filter&vin='.$car_detail->vin)}}" style="width: 50%;" class="btn btn-block btn-primary">
                                <i class="fa fa-arrow-right "></i> Show Similar Cars</a>
                        </div>
                    @endauth

                    @guest
                        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary"><i class="fa fa-phone"></i> Call Dealer</a>
                        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary"><i class="fa fa-comments"></i> Chat Dealer</a>
                        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary"><i class="fa fa-envelope"></i> Email</a>
                    @endguest
                </center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

	
	
	<!-- Modal -->
<div class="modal fade" id="user-contact" data-backdrop="static"  data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="contact-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contact-modal">Dealer Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <center>
                    @auth
                        <input type="text" class="form-control" value="{{Auth::user()->mobile}}" /><br>
                        <a href="javascript:void(0);" style="width: 50%;" class="call-button btn btn-block btn-primary">
                            <i class="fa fa-arrow-right "></i> Go</a>
                        <div id="div1"></div>
                    @endauth
                </center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
	
					
					
					
					
					
							<!-- SERVICES EMAIL TO DEALER -->
	
							@endforeach

							<div>
							
							<div>
							</div>	
							</div>
							</div>
							
								<ul class="pagination justify-content-center">
									<?php
									$links = $all_dealers->render();
									$links = str_replace("<a", "<a class='page-link ' ", $links);
									$links = str_replace("<li", "<li class='page-item' ", $links);
									$links = str_replace("<span", "<span class='page-link'",$links);
									echo $links;
									?>
								</ul>
								@endif
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	

	
	<script>

	 function services_email(dealerid){
        $.ajax({
            type:'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                dealer_id:dealerid
            },
            url:'{{url('check/dealer/register')}}',
            success: function (res) {
                if(res == 1){
                    $("#div1").hide();
                    $(".contact-btn").show();
                }

                else{
                    $(".contact-btn").hide();
                    $("#div1").show();
                }
            }
        });
        $('#num-div').css("display", "none");
        $('#dealer-contact-modal').modal('show');
    }

	
	
	
    $(document).ready(function(){

        var callStatus = '';
        var callSid = '';
    $(".call-button").click(function(){
        $(this).html('Connecting...');
        var user_phone = $('#user-phone').val();
        var dealer_phone = $('#dealer_phone').val();
		var dealer_id = $('.dealer_id').val();
		var other_phone = $('#dealer_other_phone').val();
        $.ajax({
            url: "{{url('twilio-call-chat/call.php')}}",
            type: "POST",
            data: {id: 1, user_phone:user_phone,dealer_phone:dealer_phone,dealer_id:dealer_id,other_phone:other_phone},
            dataType: 'json',
            error: function(xhr, status, error) {
                console.log(xhr.status + status+error);
            },
            success: function(result){
                console.log(result.call_sid);
                callSid = result.call_sid;
                callStatus = setInterval(function () {
                    checkCallStatus(result.call_sid)
                }, 2000);
            }
        });
    });


        function checkCallStatus(sid){

            $.ajax({
                url: "{{url('twilio-call-chat/call_status.php')}}",
                type: "POST",
                data: {call_sid: sid},
                dataType: 'json',
                success: function(result){
                    console.log(result.call_status);

                    if(result.call_status == 'queued' || result.call_status == 'initiated' || result.call_status == 'ringing'){
                        $(".call-button").html('Connecting...');
                    }
                    else if(result.call_status == 'in-progress'){
                        $(".call-button").html('Connected!');
                    }
                    else if(result.call_status == 'busy' || result.call_status == 'failed' || result.call_status == 'no-answer'){
                        $(".call-button").html('Call Failed');
                        stopFunction();
                    }
                    else if(result.call_status == 'completed'){
                        $(".call-button").html('Call Again');
                        callSid = '';
                        stopFunction();
                    }
                }
            });
        }


        function stopFunction(){
            clearInterval(callStatus);
        }

	});
  
	</script>



<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>


<script type="text/javascript">

$( document ).ready(function() {
	
	$('select').on('change', function() {
		
	  var radius = this.value;
	  
			$.ajax({
				type:'POST',
				data: {
				"_token": "{{ csrf_token() }}",	
				 radius: radius
				},
				
				url:'{{url('services_edit_radius.html')}}',
				
				success:  function () {
				alert('Radius Edited Successfully');
				}
				
		}); 
	 
	});
	
	});	
	</script> 


	

<script type="text/javascript">


   function services_email_modal(dealerid,dealername){

       var user_email = "<?php echo Auth::user()->email ?>";

       $.ajax({
           type:'POST',
           data: {"_token": "{{ csrf_token() }}",dealerid: dealerid},
           url:'{{url('/dealer_info')}}',
           success:  function (data) {
               console.log(data[0]);

               $('#dealer-contact-modal').modal('show');

               $(".dealer_id").val(data[0].dealer_id);
               $(".dealer_name").val(data[0].dealer_name);
               $("#dealer_phone").val(data[0].dealer_phone);
               $(".dealer_phone").val(data[0].dealer_phone);  
			   $("#dealer_other_phone").val(data[0].dealer_other_phone);
               $(".channel_name").val(data[0].dealer_email+'-'+user_email);

           }

       });
   }



/* 	$(function(){

		$(".call-button").click(function(){
			$(this).html('connecting to call...');
    		$.ajax({
    			url: "http://ethnicjob.ca/twilio-call-chat/call.php",
    			jsonp: 'jsonp_callback',     			
    			type: "POST",
		        crossDomain: true,
    			data: {id: 1},
    			dataType: 'jsonp',
    			headers: {
			        "Access-Control-Allow-Origin": "*",
			        "Access-Control-Allow-Headers": "origin, content-type, accept"
			    },
			    error: function(xhr, status, error) {

                     alert(xhr.status+status+error);

                 },
    			success: function(result){    				
        			$("#div1").html(result);
    			}
    		});
		});
	}) */
</script>




@endsection
@section('searchPageScript')
@endsection 
