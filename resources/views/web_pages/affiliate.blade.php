@extends('layouts.web_pages')
@section('content')
    <div class="header-margin py-4 bg-secondary">
        <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
            <h1>Affiliate Application</h1>
            </div>
        </div>
        </div>
    </div>

    <div class="py-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="card">
                        <div class="card-header font-weight-bold fs18">Affiliate Application Form</div>
                        <form method="post" action="{{ action('HomeController@affiliate_submit') }}" accept-charset="UTF-8">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="name">Full Name <span class="text-danger">*</span></label>
                                        <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><span class="fa fa-user"></span></span>
                                        </div>
                                        <input type="text" name="affiliate_name" class="form-control" placeholder="Enter Full Name">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="email">Email <span class="text-danger">*</span></label>
                                        <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><span class="fa fa-envelope"></span></span>
                                        </div>
                                        <input type="text" name="affiliate_email" class="form-control" placeholder="Enter Email Address">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="phn">Phone Number <span class="text-danger">*</span></label>
                                        <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><span class="fa fa-phone"></span></span>
                                        </div>
                                        <input type="text" name="affiliate_phone" class="form-control" placeholder="1-888-888-8888">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="pcode">Zip Code <span class="text-danger">*</span></label>
                                        <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><span class="fa fa-map-marker"></span></span>
                                        </div>
                                        <input type="text" name="affiliate_zipcode" class="form-control" placeholder="99999">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="sales">Sales Experience (Optional) <span class="text-danger"></span></label>
                                        <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><span class="fa fa-suitcase"></span></span>
                                        </div>
                                        <input type="text" name="affiliate_sales" class="form-control" placeholder="Enter Sales Experience if any">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="industry">Industry Experience (Optional) <span class="text-danger"></span></label>
                                        <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><span class="fa fa-car"></span></span>
                                        </div>
                                        <input type="text" name="affiliate_industry" class="form-control" placeholder="Enter Automotive Industry Experience if any">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="msg">Introduce yourself!</label>
                                        <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><span class="fa fa-list"></span></span>
                                        </div>
                                        <textarea name="affiliate_message" class="form-control" rows="8" placeholder="Write a few paragraphs to introduce yourself"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-primary"><span class="fa fa-paper-plane" style="padding-right: 10px;"></span>Submit Application</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection