
 @extends('layouts.web_pages')
@section('content') 
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel='stylesheet prefetch' href='http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css'>

<link href="{{asset('css/rating.css')}}" rel="stylesheet"> 
 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Rating</h1>            
          </div>
        </div>
      </div>      
    </div>
				<div class="container width-75">
				<br>
					   <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
			  
		    <div class="message"width="50%" align="center">
                    @if (session('error'))
                        <div class="alert alert-danger" width="50%">
                            {{ session('error') }}
                        </div>
                    @endif
		      </div>	  
			  
				
				<div class="row">
				 <div class="col-sm-12 col-md-12">
							<div class="card border-none rounded-0">
							  <div class="card-header rounded-0">
								<div class="row no-gutters">
						<div class="col-md-8 text-center text-md-left font-weight-bold fs18">Rating:</div>								  
								</div>
							  </div>
							  <div class="card-body">
							   <br>
							@foreach($car_rating as $key => $data)
						
							
								<div class="cxm-advert-item mb-3">            
								  <div class="form-row">
									<div class="col-sm-3">
									     <div class="cxm-img">
                      <!--<div class="cxm-img-badge position-absolute"><span class="fa fa-camera"></span> </div>-->
									  <a href="{{url('car/detail').'/'.$data->id}}"><img class="img-fluid" src="@if(!empty($data->media->photo_links[0]) > 0)
													{{$data->media->photo_links[0]}}
													@else
													{{asset('public/images/no-image.jpeg')}}
									  @endif"></a>
									</div>	
									</div>
									<div class="col-sm-8">
									  <div class="cxm-content">
					
										<div><span class="fc1 fs24">$14,000</span></div>
										<a class="text-primary font-weight-bold fs18" href="#">	@if(!empty($data->heading) > 0){{$data->heading}}@endif </a>
										<div class="fs12 lh16 mb-2">
										<ul class="cxm-facts fs12  p-2 rounded">
										<li><span class="fa fa-modx text-primary"></span> 
										@if(!empty($data->build->year) > 0)
										{{$data->build->year}}
										@endif
										</li>
										<li><span class="fa fa-barcode text-primary"></span> 
										@if(!empty($data->build->body_type) > 0)
										{{$data->build->body_type}}
										@endif
										</li>
										<li><span class="fa fa-road text-primary"></span> 
										@if(!empty($data->miles) > 0)
										{{$data->miles}}
										@endif  miles
										</li><br>
										<li><span class="fa fa-toggle-on text-primary"></span>
										@if(!empty($data->build->transmission) > 0)
										{{$data->build->transmission}}
									   @endif
										</li>
										<li><span class="fa fa-spinner text-primary"></span> 
										
										@if(!empty($data->build->engine) > 0)
										{{$data->build->engine}}
									   @endif
										</li>
										<li><span class="fa fa-fire text-primary"></span> 
										@if(!empty($data->build->fuel_type) > 0)
										{{$data->build->fuel_type}}
									   @endif
										</li>
									  </ul>
										</div>
										
									</div>
									</div>
								  </div>          
								</div>
								
								@endforeach
								
								<br>
							  </div>
							</div>

						</div>
						</div>
						</div>
						
				  <div class="container width-75">
				  
				  <div class="row abb4">
					<div class="col-md-4 white">
				  <div class='rating-stars text-center'>
					<ul id='stars'>
					  <li class='star' title='Poor' data-value='1'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='Fair' data-value='2'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='Good' data-value='3'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='Excellent' data-value='4'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='WOW!!!' data-value='5'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					</ul>
			</div>
				</div>
			<div class="col-md-7 white">
		     <div class='success-box'>
			<div class='clearfix'></div>
			<div class="text-message2"></div>
			<div class='text-message'style='font-weight:bold;'></div>
			<div class="text-message3"></div>
			<div class='clearfix'></div>
			
		    </div>
			</div>
			</div>
				  
			
		  <div class="container margin-top">
		  <div class="row">
		
		  <div class="col-md-11 white">
		  
		  {!! Form::open(array('url' => '/save/rating', 'enctype' => 'multipart/form-data')) !!}
         {!! Form::hidden('_token', csrf_token()) !!}
		 
		  <div class="form-group">
			<label>Review Title*</label>
			<input type="text" class="form-control" name="review_title" id="exampleInputEmail1" aria-describedby="emailHelp" >
			<input type="hidden" class="form-control" id="rating" name="rating" >
			<input type="hidden" class="form-control" value="<?php echo $data->id;?>" id="car_id" name="car_id" aria-describedby="emailHelp" >
			<input type="hidden" class="form-control" value="<?php echo $car_rating[0]->dealer->id;?>" id="dealer_id" name="dealer_id" aria-describedby="emailHelp" >

		  </div>
		  <div class="form-group">
			<label>Review Description*</label>
		   <textarea class="form-control" name="review_description"></textarea>
		  </div>
		  <br>
		  </div>
		  </div>
		   <button type="submit" class="btn btn-primary" align="right">Submit</button>

		  </div>

	     </form>
	
      </div>      

    <br><br>
 <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script>
$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
    if (ratingValue > 1) {
        message = "Thanks! You rated this ";
		msg = "" + ratingValue + "";
		message2 = " Stars ";
		
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + "stars";
    }
    responseMessage(msg);
	responseMessage2(message);
	responseMessage3(message2);
    
  });
  
  
});

function responseMessage(msg) {
  $('.success-box').fadeIn(200); 
  $('.success-box div.text-message').html("<span>" + msg + "</span>");
  var rating = msg; 
  document.getElementById("rating").value = rating; 
}

function responseMessage2(message) {
  $('.success-box').fadeIn(200); 
  $('.success-box div.text-message2').html("<span>" + message + "</span>");
}

function responseMessage3(message3) {
  $('.success-box').fadeIn(200); 
  $('.success-box div.text-message3').html("<span>" + message2 + "</span>");
}
	</script>
@endsection 	