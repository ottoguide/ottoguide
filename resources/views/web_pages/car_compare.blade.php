<?php
$user_id= Auth::id();

if( strpos(url()->previous() , "search") !== false)
Session::put('bck_url',url()->previous());
?>
<style>
.cxm-img-badge-cls a{
color:#fff;

td:nth-child(1) {
background-color: #ccc;
}​
.border{
border:1px solid #158CBA;
box-shadow: 5px;
-webkit-box-shadow: 0px 0px 10px -1px rgba(0,0,0,0.53);
-moz-box-shadow: 0px 0px 10px -1px rgba(0,0,0,0.53);
box-shadow: 0px 0px 10px -1px rgba(0,0,0,0.53);
}
h3{
font-size: 16px;

}

}

table {
    font-size: 12.5px !important;
}

.message {
  animation: cssAnimation 0s 5s forwards;
  opacity: 1; 
}

@keyframes cssAnimation {
  to   { opacity: 0; }
}
.pd-2{padding: 2px;}
</style>
@extends('layouts.web_pages')
@section('content') 

 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <h1>Compare Vehicles</h1>
          </div>
          <div class="col-sm-6">
            <div class="text-center text-sm-right mt-2">
                <a class="btn btn-primary" href="{{Session::get('bck_url')}}"><span class="fa fa-plus-circle"></span> Add Vehicle</a>
			<a class="btn btn-primary" href="#"><span class="fa fa-save"></span> Save Comparision</a>            </div>
          </div>
        </div>
      </div>      
    </div>
	@if(!empty($all_car_datas) > 0)
	<div style="margin-top: 2px;">
      <div class="container">

	   <div class="message" width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>

		    <div class="message" width="50%" align="center">
                    @if (session('error'))
                        <div class="alert alert-danger" width="50%">
                            {{ session('error') }}
                        </div>
                    @endif
		      </div>


        <div class="row">
          <div class="col-sm-12">
            <div style="border: 1px solid #ccc;margin: 5px 2px;">
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="overview" role="tabpanel">
				  <div class="form-row">
				  <!-- START FOREACH -->
                    @foreach($all_car_datas as $no=> $data)
                    <div class="col-sm-6 col-md-3 col-lg-3">
                      <div class="cxm-vihecle-box card border-secondary mb-3">
                      <div class="cxm-img">
					  <div class="cxm-img-badge-cls">
					  <a href="javascript:void(0);" class="{{$data->id}}-car" onclick="rbo_car_compare_delete('<?php echo $data->id;?>');"><span class="fa fa-remove"></span></a>
					  </div>

					 <div class="cxm-img-badge position-absolute"></div>

					  <a href="{{url('car/detail').'/'.$data->id}}"><img class="img-fluid" src="@if(!empty($data->media->photo_links[0]) > 0)
            			{{$data->media->photo_links[0]}}
            			@else
            			{{asset('public/images/no-image.jpeg')}}
						@endif">
					  </a>

                     </div>
                        <div class="text-center lh16" style="height:32px;">
						<b>	@if(!empty($data->heading) > 0)
							{{$data->heading}}
							@endif
						</b>
						</div>
                        <div class="card-body p-2">
                          <hr class="hr1">
                          <div class="form-row">
                            <div class="col-6 mb-2 border-right">
                              <span class="badge badge-secondary">Price</span>
                              <div><span class="fc1 fs24">
								$ @if(!empty($data->price) > 0)
								{{number_format((float)$data->price)}}
								@endif
							  </span></div>
                            </div>
                            <div class="col-6 mb-2 text-right">
                             <span class="badge badge-secondary">MILES</span>
                             <div><span class="fc1 fs24">
							 @if(!empty($data->miles) > 0)
							 {{$data->miles}}
							 @endif
							 </span></div>
                            </div>
                          </div>
                          <div style="display: none;" class="text-center"><a class="fs14 font-weight-bold" href="#"><span class="fa fa-money"></span> See Dealer Offer</a></div>
                          <hr class="hr1">
                          <div class="card mb-1">
                            <div class="card-header p-0"><a class="collapsed" data-toggle="collapse" href="#warranty<?php echo $no;?>" role="button" aria-expanded="false" aria-controls="warranty<?php echo $no;?>">Build</a></div>
                            <div class="collapse" id="warranty<?php echo $no;?>">
                            <?php foreach($data->build as $desc => $values){ ?>
							    <div class="pd-2"><?php echo str_replace("_","  ","<b>".ucwords($desc)." : </b>");  echo  $values;?> </div>
							<?php } ?>
                                <div class="pd-2"><b>Interior Color:</b> {{$data->interior_color}}</div>
                                <div class="pd-2"><b>Exterior Color:</b> {{$data->exterior_color}}</div>
                            </div>
                          </div>
                          <div class="card mb-1">
                            <div class="card-header p-0"><a class="collapsed" data-toggle="collapse" href="#features<?php echo $no;?>" role="button" aria-expanded="false" aria-controls="features<?php echo $no;?>">Features</a></div>
                            <div class="collapse" id="features<?php echo $no;?>">
                                @if(!empty($data->extra->features) > 0)
                                    <?php foreach($data->extra->features as  $value){ ?>
                                        <li style="padding-left: 4px;">{{$value}}</li>
                                     <?php } ?>
                                @endif
                            </div>
                          </div>
                          <div class="card mb-1">
                            <div class="card-header p-0"><a class="collapsed" data-toggle="collapse" href="#safety<?php echo $no;?>" role="button" aria-expanded="false" aria-controls="safety<?php echo $no;?>">Safety</a></div>
                            <div class="collapse" id="safety<?php echo $no;?>">
                                @if(!empty($data->extra->safety_f) > 0)
                                    <?php foreach($data->extra->safety_f as $value){ ?>
                                        <li style="padding-left: 4px;">{{$value}}</li>
                                    <?php } ?>
                                @endif
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
				 @endforeach





		<!-- END FOREACH -->

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

   @else

				   <br> <br>
				   <h2 align="center">Plase add vehicle for comparison</h2>
				    <br> <br>
  @endif

<script>
    function rbo_car_compare_delete(carid){
			var car_id = carid;

			$.ajax({
			type:'POST',

			data: {
				"_token": "{{ csrf_token() }}",
				car_id: car_id
			 },

			url:'{{route('car_compare_delete')}}',

                beforeSend: function(){
                    $('.'+carid+'-car').html('<span class="fa fa-spin fa-refresh"></span> Deleting...');
                },
                success: function(html){
                    location.reload();
                },
			});
		}

</script>


@endsection