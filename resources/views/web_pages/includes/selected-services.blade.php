
						<br>
						
							@if(!empty(Session::get('services')['Describe Problem']) > 0)
								<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" class="tab-width" data-target="#tab1" aria-expanded="true" aria-controls="tab1">
												<img src="/images/icons/own-problem.png" width="24px"> Describe problem in your own words
												<i class="fa fa-check dp pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab1" class="collapse in">
													<div class="well"><textarea  name="describe-problem" id="describe-problem" class="form-field form-control" disabled="disabled" >@if(!empty(Session::get('services')['Describe Problem']) > 0){{Session::get('services')['Describe Problem']}}@endif</textarea></div>
												</div>

											</div>

										</div>
									@endif

									@if(!empty(Session::get('services')['General Maintenance']) > 0)
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab2" class="tab-width" aria-expanded="true" aria-controls="tab2">
												 <img src="/images/icons/general-maintenace.png" width="24px"> General Maintenance
												<i class="fa fa-check gm pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab2" aria-expanded="true" class="collapse in">
													<div class="well"><textarea class="form-field form-control" name="general-maintenance" id="general-maintenance" disabled="disabled">@if(!empty(Session::get('services')['General Maintenance']) > 0){{Session::get('services')['General Maintenance']}}@endif</textarea></div>
												</div>

											</div>
										</div>
										@endif

									  @if(!empty(Session::get('services')['Scheduled Maintenance']) > 0)
											<div class="form-group">
												<div class="checkbox">
													<label data-toggle="collapse" data-target="#tab3" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
													 <img src="/images/icons/schedule.png" width="28px">Scheduled Maintenance
														<i class="fa fa-check sm pull-right" aria-hidden="true"></i>
													</label>

													<div id="tab3" aria-expanded="true" class="collapse in">
														<div class="well"><textarea   class="form-field form-control" name="scheduled-maintenance"  id="scheduled-maintenance" disabled="disabled">@if(!empty(Session::get('services')['Scheduled Maintenance']) > 0){{Session::get('services')['Scheduled Maintenance']}}@endif</textarea></div>
													</div>

												</div>
											</div>
										@endif	
											
											
										@if(!empty(Session::get('services')['Inspection Diagnostic']) > 0)
											<div class="form-group">
												<div class="checkbox">
													<label data-toggle="collapse" data-target="#tab4" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
													<img src="/images/icons/inspection.png" width="28px"> Inspection and/or Diagnostic
													<i class="fa fa-check id pull-right" aria-hidden="true"></i>
													</label>

													<div id="tab4" aria-expanded="true" class="collapse in">
														<div class="well"><textarea   class="form-field form-control" name="inspection-diagnostic"  id="inspection-diagnostic" disabled="disabled">@if(!empty(Session::get('services')['Inspection Diagnostic']) > 0){{Session::get('services')['Inspection Diagnostic']}}@endif</textarea></div>
													</div>
												</div>
											</div>
										@endif

									 @if(!empty(Session::get('services')['Vehicle Start']) > 0)
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab5" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/not-start.png" width="28px">	 Vehicle doesn’t start
												<i class="fa fa-check vstart pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab5" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="vehicle-start"  id="vehicle-start" disabled="disabled">@if(!empty(Session::get('services')['Vehicle Start']) > 0){{Session::get('services')['Vehicle Start']}}@endif</textarea></div>
												</div>

											</div>
										</div>
									  @endif

									 @if(!empty(Session::get('services')['Vibrations Noises']) > 0)
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab6" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
											<img src="/images/icons/noises.png" width="28px"> Vibrations and/or Noises
												<i class="fa fa-check vnoises pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab6" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="vibrations-noises"  id="vibrations-noises" disabled="disabled">@if(!empty(Session::get('services')['Vibrations Noises']) > 0){{Session::get('services')['Vibrations Noises']}}@endif</textarea></div>
												</div>

											</div>
										</div>
									  @endif

								@if(!empty(Session::get('services')['Visual Problem']) > 0)
										
									<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab7" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/visual-problem.png" width="24px">	 Visual problem (Unusual smoke, Fluid leak etc.)
												<i class="fa fa-check vp pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab7" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="visual-problem"  id="visual-problem" disabled="disabled">@if(!empty(Session::get('services')['Visual Problem']) > 0){{Session::get('services')['Visual Problem']}}@endif</textarea></div>
												</div>


											</div>
										</div>
								 @endif
										
								@if(!empty(Session::get('services')['Odor/Smell']) > 0)
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab8" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/odor.png" width="24px">	 Odor / Smell
												<i class="fa fa-check os pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab8" aria-expanded="true" class="collapse in">
													<div class="well">
														<textarea class="form-field form-control" name="odor-smell"  id="odor-smell" disabled="disabled">@if(!empty(Session::get('services')['Odor/Smell']) > 0){{Session::get('services')['Odor/Smell']}}@endif</textarea>
													</div>
												</div>

											</div>
										</div>
								 @endif
								 
								 @if(!empty(Session::get('services')['Air Conditioning and Heating']) > 0)
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab10" class="tab-width" aria-expanded="false" aria-controls="collapseOne">
												 <img src="/images/icons/ac.png" width="28px">   Air-conditioning and Heating
												<i class="fa fa-check ach pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab10" aria-expanded="false" class="collapse">
													<div class="well"><textarea   class="form-field form-control"  name="ach" id="ach" disabled="disabled">@if(!empty(Session::get('services')['Air Conditioning and Heating']) > 0){{Session::get('services')['Air Conditioning and Heating']}}@endif</textarea></div>
												</div>

											</div>
										</div>
									@endif
									
									@if(!empty(Session::get('services')['Brakes']) > 0)
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab11" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/brakes.png" width="28px"> Brakes
												<i class="fa fa-check brakes pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab11" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="brakes" id="brakes" disabled="disabled">@if(!empty(Session::get('services')['Brakes']) > 0){{Session::get('services')['Brakes']}}@endif</textarea></div>
												</div>

											</div>
										</div>
									@endif

									@if(!empty(Session::get('services')['Electrical']) > 0)
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab12" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/electric.png" width="28px">	 Electrical
												<i class="fa fa-check elec pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab12" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="electrical"  id="electrical" disabled="disabled">@if(!empty(Session::get('services')['Electrical']) > 0){{Session::get('services')['Electrical']}}@endif</textarea></div>
												</div>

											</div>
										</div>
								    @endif

								@if(!empty(Session::get('services')['Exhaust']) > 0)
										
									<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab13" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/exaust.png" width="28px"> Exhaust
												<i class="fa fa-check exhaust pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab13" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control"  name="exhaust" id="exhaust" disabled="disabled">@if(!empty(Session::get('services')['Exhaust']) > 0){{Session::get('services')['Exhaust']}}@endif</textarea></div>
												</div>

											</div>
										</div>
								@endif
										

							@if(!empty(Session::get('services')['Fluids']) > 0)
								
										 <div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab14" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/fluids.png" width="28px">Fluids
												<i class="fa fa-check fluids pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab14" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control"  name="fluids" id="fluids" disabled="disabled">@if(!empty(Session::get('services')['Fluids']) > 0){{Session::get('services')['Fluids']}}@endif</textarea></div>
												</div>

											</div>
										  </div>
								@endif  

								@if(!empty(Session::get('services')['Steering Suspension']) > 0)
								
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab15" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/suspension.png" width="28px">	 Steering & Suspension
													<i class="fa fa-check ss pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab15" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control"  name="steering-suspension" id="steering-suspension" disabled="disabled">@if(!empty(Session::get('services')['Steering Suspension']) > 0){{Session::get('services')['Steering Suspension']}}@endif</textarea></div>
												</div>

											</div>
										</div>

								@endif  

								
								@if(!empty(Session::get('services')['Wheel Alignment']) > 0)
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab16" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/wheel-alignment.png" width="28px">	 Wheel Alignment
												<i class="fa fa-check wa pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab16" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="wheel-alignment"  id="wheel-alignment" disabled="disabled">@if(!empty(Session::get('services')['Wheel Alignment']) > 0){{Session::get('services')['Wheel Alignment']}}@endif</textarea></div>
												</div>

											</div>
										</div>
								@endif

								@if(!empty(Session::get('services')['Tire Replace']) > 0)

										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab17" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
													<img src="/images/icons/tyre.png" width="18px">	 Tires Replacement
													<i class="fa fa-check tire-replace pull-right" aria-hidden="true"></i>
												</label>

											
												<div id="tab17" aria-expanded="true" class="collapse in">
													
												<div class="well"><textarea class="form-field form-control" name="wheel-alignment"  id="wheel-alignment" disabled="disabled">@if(!empty(Session::get('services')['Tire Replace']) > 0){{Session::get('services')['Tire Replace']}}@endif</textarea></div>

												</div>

											</div>
										</div>

								 @endif	
										
								@if(!empty(Session::get('services')['Tire Repair']) > 0)		
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab18" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/tire-replacement.png" width="28px">	 Tire Repair
												<i class="fa fa-check tire-repair pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab18" aria-expanded="true" class="collapse in">
													<div class="well"><textarea class="form-field form-control" name="wheel-alignment"  id="wheel-alignment" disabled="disabled">@if(!empty(Session::get('services')['Tire Repair']) > 0){{Session::get('services')['Tire Repair']}}@endif</textarea></div>

											</div>
										</div>
									
									   </div>
	
									@endif	
