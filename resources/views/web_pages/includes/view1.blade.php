<?php
$param="";
foreach ($_GET as $k => $p){
    $param.=$k.'='.$p.'&';
}
?>
<div class="col-md-<?php echo $bootstrapColWidth; ?>  box pull-right">
                    <div class="cxm-detail p-3 cxm-detail-border">
                        <div class="row no-gutters">
                            <div class="col-sm-5">
                                <div class="cxm-img">
                                    <div id="cxm-slider-pro-<?php echo $i; ?>" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
											<?php
											if($i >= 4){$active = 1;}else {$active = $i;}
											//if($car_images){
											foreach($car_images as $pimg => $car_image ) {
											if($pimg == 4)
												break;
											?>
                                            <div class="carousel-item<?php echo (($pimg == 0)? ' active' :''); ?>">
                                                <div class="cxm-img">
                                                    <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                                                    <a href="{{url('car/detail').'/'.$car->id.'?'.$param}}"><img class="img-fluid" onerror="this.onerror=null;this.src='{{asset('images/no-image.jpeg')}}';" src="{{$car_image}}"></a>
                                                </div>
                                            </div>
											<?php } /*} else { ?>
                                            <div class="carousel-item active">
                                                <div class="cxm-img">
                                                    <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                                                    <a href="{{url('car/detail').'/'.$car->id}}"><img class="img-fluid" src="{{asset('images/no-image.jpeg')}}"></a>
                                                </div>
                                            </div>
											<?php }*/ ?>
                                        </div>

                                        <a class="carousel-control-prev" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon fa fa-angle-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon fa fa-angle-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="cxm-content ">
                                    <h2><a href="{{url('car/detail').'/'.$car->id.'?'.$param}}">@if(!empty($car->heading) > 0){{mb_strimwidth($car->heading,0,25,"...")}}@endif</a></h2>
                                
									   @if(!empty($car->price) > 0)
													@if(Auth::user()->id)	
                                                     <div class="fs24 fc1">$ {{isset($car->price) ? number_format((float)$car->price) : '--'}}</div>
													@else
													<a href="{{route('srp_session',['session_car_id'=> $car->id, 'session_status'=> 'SRP','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}">
													<div class="btn-login-for-price">
													<div class="lbl-login-for-price">Login for Price<div class="lbl-free">It's FREE!</div></div>
													</div>
													</a>		
												
													@endif
                                                    @endif
									
									
                                    <div class="cxm-features">{{isset($car->build->year)?$car->build->year:''}} {{(isset($car->build->model) ? $car->build->model : '' )}} {{isset($car->build->engine)?$car->build->engine :''}}</div>
                                    <p>Make: {{(isset($car->build->make) ? $car->build->make : '') }}</p>
                                    <p>Model: {{(isset($car->build->model) ? $car->build->model : '' )}}</p>
                                    <p>Trim: {{isset($car->build->trim) ? $car->build->trim : ' ' }}</p>
                                </div>
                            </div>

							
							
                            <div class="col-sm-12 col-md-12 col-xs-6">
							  @if(!isset($_GET['rbo']))	
                             
                                        <hr>
										
										
										
										@auth 
								<?php
							    $check_user_rbo_status = DB::table('request_best_offer')->where('user_id',(isset(Auth::user()->id) ? Auth::user()->id : '0'))->where('car_id','=',$car->id)->where('is_rbo_active',1)->count();
							    ?>
								
						
								@if($check_user_rbo_status)
							    <a class="btn btn-danger btn-sm" disabled>
								<span class="fa fa-check"></span> <span class="fa fa-money"></span> Requested </a>	
							    @else
								<a href="{{route('RequestBestOffer',['car_id'=> $car->id, 'dealer_id'=> $car->dealer->id , 'radius'=>$_GET['radius'], 'latitude'=> $_GET['latitude'],'longitude'=>$_GET['longitude']])}}" class="btn btn-danger btn-sm">
								<span class="fa fa-money"></span> RBO </a>								
								@endif
								
								@endauth
								
								
								@guest
								<a href="{{route('rbo_session',['session_car_id'=> $car->id,'session_car_vin'=> $car->vin,'dealer_id'=> $car->dealer->id,'session_car_title'=> $car->heading, 'session_status'=> 'Request_Best_Offer','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}" class="btn btn-danger btn-sm">
								<span class="fa fa-money"></span> RBO
								</a>
								@endguest
									
								
								
								
							    @else
									
							   <?php
							   $check_user_rbo_status_temp = DB::table('car_compare_temp')->where('user_id',Auth::user()->id)->where('session_carid','=',$car->id)->where('status','rbo')->count();
							   $check_user_rbo_status_permenent = DB::table('request_best_offer')->where('user_id',Auth::user()->id)->where('car_id','=',$car->id)->where('is_rbo_active',1)->count();
							   ?>
							
								@if($check_user_rbo_status_temp != 0)
								<button class="btn btn-secondary btn-sm" disabled><span class="fa fa-lock"> </span> Already requested best offer</button>
								
								@elseif($check_user_rbo_status_permenent != 0)
								<button class="btn btn-secondary btn-sm" disabled><span class="fa fa-lock"> </span> Already requested best offer</button>
								
								@else
							 
								<a class="btn btn-success btn-sm" href="javascript:rbo_car_compare('<?=$car->id?>',<?=$car->dealer->id?>);">
								<span class="fa fa-check"></span>  Add For Request Best Offer </a>
							  	
								@endif	
										
								@endif		
					
                                  <input type="hidden" id="save_car_id_{{$i}}" value="{{$car->id}}">
                                   <input type="hidden" id="save_car_{{$i}}" value="{{base64_encode(json_encode($save_car))}}">
								   
								  @guest
								  <a href="{{route('search_buttons_session',['session_car_id'=> $car->id,'session_car_vin'=> $car->vin,'session_car_title'=> $car->heading, 'session_status'=> 'just_question','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}"><button type="button" class="btn btn-primary btn-sm"><span class="fa fa-question-circle"></span> Question</button></a>
								  @endguest		
								   
								  @auth
                                 <a href="javascript:services_email('<?=$car->id?>', '<?=$car->dealer->id ?>','<?=$get_dealer_email->dealer_email ?>','<?=$get_dealer_email->dealer_phone ?>','<?=$get_dealer_email->dealer_name ?>','<?=$get_dealer_email->dealer_other_phone ?>');">
								 <button type="button" class="btn btn-primary btn-sm"><span class="fa fa-question-circle"></span>Question</button></a>
								 @endauth
								 
								 <!-- CHECK RBO -->
								 
				
                                <a href="javascript:save_car_compare('<?=$car->id?>');">
								<button type="button" class="btn btn-primary btn-sm"><span class="fa fa-clone"></span> Compare</button></a>
						
								@guest
								<a href="{{route('search_buttons_session',['session_car_id'=> $car->id,'session_car_vin'=> $car->vin,'session_car_title'=> $car->heading, 'session_status'=> 'just_question','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}"><button type="button" class="btn btn-primary btn-sm"><span class="fa fa-comment"></span> Contact</button></a>
								@endguest
						
								@auth	
								<a href="javascript:services_email('<?=$car->id?>', '<?=$car->dealer->id ?>','<?=$get_dealer_email->dealer_email ?>','<?=$get_dealer_email->dealer_phone ?>','<?=$get_dealer_email->dealer_name ?>','<?=$get_dealer_email->dealer_other_phone ?>');">
								<button type="button" class="btn btn-primary btn-sm"><span class="fa fa-comment"></span> Contact</button></a>
								@endauth	
								
						
							   @if(count(Auth::user()))
	
							   <?php
							   
							   $rating_status = DB::table('client_reviews')->where('client_id',Auth::user()->id)->where('car_id','=',$car->id)->where('reply_to','=',null)->get()->first();
							   $check_block_status = DB::table('settings')->where('car_id',$car->id)->where('reviews_off',1)->get()->first();
							   ?>
							
							  @if(count($check_block_status))
							  <!-- <a class="btn btn-primary btn-sm" href="javascript:rating_blocked();"><span class="fa fa-line-chart"></span> Reviews </a>-->
							   @else
								   
							   @if(count($rating_status))
							   <!--<a class="btn btn-success btn-sm" href="car/detail/<?//=$car->id?>"><span class="fa fa-line-chart" style="color:#fff;"></span> Reviewed</a>-->
							 
 							   @else
							   <!--<a class="btn btn-primary btn-sm" href="javascript:car_rating('<?//=$car->id?>', <?//=$car->dealer->id ?>);"><span class="fa fa-line-chart"></span> Reviews </a>-->
							   
							   @endif
							   
							   @endif
							 
							    @if(!count($check_block_status))
								@include('web_pages.includes.rating-save-modal')
								@endif
							 
							   @endif
							  
							
							  @guest
							  <!-- <a class="btn btn-primary btn-sm" href="javascript:car_rating_guest();"><span class="fa fa-line-chart"></span> Reviews </a>-->
                               @endguest
							 
								
                               <!--<a href="{{route('CarHistorySave',['car_id'=> $car->id])}}"><button type="button" class="btn btn-primary btn-sm"><span class="fa fa-history"></span> History</button></a>-->
							
								   @auth
									<?php $tot = DB::table('client_favourite')->where('client_id',Auth::id())->where('car_id',$car->id)->count();?>
                                    @if($tot==0)
										
									<?php
										$save_car= array(
											'id' => (isset($car->id)) ? $car->id: '', 
											'title' => (isset($car->heading)) ? $car->heading: '',
											'car_type' => (isset($car->inventory_type) ? $car->inventory_type : '--'),
											'price' => (isset($car->price) ? $car->price : '--'),
											'miles' => (isset($car->miles) ? $car->miles : '--'),
											'build' => (isset($car->build)) ? $car->build: '',
											'images' =>(isset($car_images)) ? $car_images: '',
										);
										?>
								 
								<a class="btn btn-primary btn-sm" href="javascript:save_car({{$i}});"><span id="save_car_span_{{$i}}" class="fa fa-save"></span> <span id="jq_save_tag_{{$i}}">Save</span></a>
                                  @else
							    <!-- CHECK RBO -->
                                        <a class="btn btn-primary btn-sm" href="javascript:void(0);">
                                            <span class="fa fa-check"></span><span>Saved</span></a>
                                @endif
                                @endauth
                                @guest
														
									<a href="{{route('search_buttons_session',['session_car_id'=> $car->id,'session_car_vin'=> $car->vin,'session_car_title'=> $car->heading, 'session_status'=> 'Save_Car','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}" class="btn btn-primary btn-sm">	<span class="fa fa-save"></span> Save </a>		

                                @endguest
								
							
								 <!-- end check RBO -->
								
						       </div>
							
							<!-- Rating post -->
							<tr>
							<input type="hidden" id="car_ID" name="car_id" value="{{$car->id}}">
							<input type="hidden" id="dealer_ID" name="dealer_id" value="{{$car->dealer->id}}">							
							<input type="hidden" id="rating" name="rating">
							</tr>
							
                        </div>
                        <br>
                    </div>
                </div>