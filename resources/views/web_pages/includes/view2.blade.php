<style>
.row-custom-margin{
	margin-left:0px;
	margin-right:0px;
}

</style>
<?php $param="";
foreach ($_GET as $k => $p){
    $param.=$k.'='.$p.'&';
}
?>

<div class="cxm-pro-list cxm-detail-border">
                    <div class="row no-gutters">
                        <div class="col-sm-9">
                            <div class="cxm-detail p-3">
                                <div class="row no-gutters">
                                    <div class="col-sm-4">
                                        <div id="cxm-slider-pro-<?php echo $i; ?>" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner">
												<?php
												$car_images = $car->media->photo_links;
												if($i >= 4){$active = 1;}else {$active = $i;}
												if($car_images){
												foreach($car_images as $pimg => $car_image ) {
												if($pimg == 4)
													break;
												?>
                                                <div class="carousel-item<?php echo (($pimg == 0)? ' active' :''); ?>">
                                                    <div class="cxm-img img-fixed2">
                                                        <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                                                        <a href="{{url('car/detail').'/'.$car->id.'?'.$param}}"><img onerror="this.onerror=null;this.src='{{asset('public/images/no-image.jpeg')}}';" class="img-fluid" src="{{$car_image}}"></a>
                                                    </div>
                                                </div>
												<?php } } else { ?>
                                                <div class="carousel-item active">
                                                    <div class="cxm-img img-fixed2">
                                                        <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                                                        <a href="{{url('car/detail').'/'.$car->id.'?'.$param}}"><img class="img-fluid" src="{{asset('public/images/no-image.jpeg')}}"></a>
                                                    </div>
                                                </div>
												<?php } ?>
                                            </div>

                                            <a class="carousel-control-prev" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon fa fa-angle-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon fa fa-angle-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="cxm-content">
                                            <h2><a href="{{url('car/detail').'/'.$car->id.'?'.$param}}">@if(!empty($car->heading) > 0){{$car->heading}}@endif </a></h2>
                                            <div class="form-row">
                                                <div class="col-md-4">
                                                    @if(!empty($car->price) > 0)
													@if(Auth::user()->id)	
                                                    <div class="fs24 fc1">$ {{isset($car->price) ? number_format((float)$car->price) : '--'}}</div>
													@else
														
													<a href="{{route('srp_session',['session_car_id'=> $car->id, 'session_status'=> 'SRP','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}">
													<div class="btn-login-for-price">
													<div class="lbl-login-for-price">Login for Price<div class="lbl-free">It's FREE!</div></div>
													</div>
													</a>		
													
													@endif
                                                    @endif
                                                    <div class="text-muted">{{isset($car->miles) ? $car->miles:'--'}} MILES</div>
                                                    <div><span class="fa fa-square fs18" {{isset($car->exterior_color) ? 'style="color:'.$car->exterior_color.' ;"' :''}}></span> Color</div>
                                                    <div class="cxm-features">{{isset($car->build->year)?$car->build->year:''}} {{(isset($car->build->model) ? $car->build->model : '' )}} {{isset($car->build->engine)?$car->build->engine :''}}
                                                    </div>
                                                    <div class="text-muted">{{ucfirst($car->inventory_type)}}</div>
                                                </div>
                                                 <div class="col-md-8">
                                                    <div class="fc1">Description:</div>
                                                    <p>Make: {{(isset($car->build->make) ? $car->build->make : '') }}</p>
                                                    <p>Model: {{(isset($car->build->model) ? $car->build->model : '' )}}</p>
                                                    <p>Trim: {{isset($car->build->trim) ? $car->build->trim : ' ' }}</p>
                                                    <p>Body Type: {{isset($car->build->body_type) ? $car->build->body_type : ' '}}</p>
                                                    <p>Vehicle Type: {{isset($car->build->vehicle_type) ? $car->build->vehicle_type : ''}}</p>
													<!--<p>Dealer ID: <b>{{$car->dealer->id}}</b></p>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pt-3">
                                    <span class="fa fa-video-camera"></span> 0 <span class="fa fa-camera ml-4"></span> {{count($car_images)}} <!-- <span class="ml-4">VIN: {{$car->vin}}</span>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="cxm-actions">
                               <div class="row">
                                <div class="col-12 mb-2">
								
								
								
								@auth 
								<?php
							    $check_user_rbo_status = DB::table('request_best_offer')->where('user_id',(isset(Auth::user()->id) ? Auth::user()->id : '0'))->where('car_id','=',$car->id)->where('is_rbo_active',1)->count();
							    ?>
								
								@if($check_user_rbo_status)
							    <a class="btn btn-danger btn-sm btn-block" disabled>
								<span class="fa fa-money"></span> Requested </a>	
							    @else
								<a href="{{route('RequestBestOffer',['car_id'=> $car->id, 'dealer_id'=> $car->dealer->id , 'radius'=>$_GET['radius'], 'latitude'=> $_GET['latitude'],'longitude'=>$_GET['longitude']])}}" class="btn btn-danger btn-block">
								<span class="fa fa-money"></span> Request Best Offer </a>								
								@endif
								
								@endauth
								
								
								@guest
								
								<a href="{{route('rbo_session',['session_car_id'=> $car->id,'session_car_vin'=> $car->vin,'dealer_id'=> $car->dealer->id,'session_car_title'=> $car->heading, 'session_status'=> 'Request_Best_Offer','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}" class="btn btn-danger btn-block">
								<span class="fa fa-money"></span> Request Best Offer
								</a>
								@endguest
									
							   @auth
							   <!-- <?php
							   $check_user_rbo_status_temp = DB::table('car_compare_temp')->where('user_id',Auth::user()->id)->where('session_carid','=',$car->id)->where('status','rbo')->count();
							   $check_user_rbo_status_permenent = DB::table('request_best_offer')->where('user_id',Auth::user()->id)->where('car_id','=',$car->id)->where('is_rbo_active',1)->count();
							   ?>
							
								@if($check_user_rbo_status_temp != 0)
								<button class="btn btn-secondary btn-sm" disabled><span class="fa fa-lock"> </span> Already requested best offer</button>
								
								@elseif($check_user_rbo_status_permenent != 0)
								<button class="btn btn-secondary btn-sm" disabled><span class="fa fa-lock"> </span> Already requested best offer</button>
								
								@else
							 
								<a class="btn btn-success btn-sm" href="javascript:rbo_car_compare('<?=$car->id?>',<?=$car->dealer->id?>);">
								<span class="fa fa-check"></span>  Add For Request Best Offer </a>
							  	
								@endif -->
								 @endauth
 				
								 <!-- end check RBO -->
								 
								 
								 
								 
								 @auth
								 
								 <div class="row">
                                    <div class="col-12 mt-2">
								 <a href="javascript:services_email('<?=$car->id?>', '<?=$car->dealer->id ?>','<?=$get_dealer_email->dealer_email ?>','<?=$get_dealer_email->dealer_phone ?>','<?=$get_dealer_email->dealer_name ?>','<?=$get_dealer_email->dealer_other_phone ?>','<?=$car->heading?>');" class="btn btn-primary btn-block "><span class="fa fa-question-circle"></span>  Just a Question </a>
								
									</div>
                                </div>
								  @endauth
								

								@guest
							
                                <div class="row">
                                    <div class="col-12 mt-2">
									
								<a href="{{route('search_buttons_session',['session_car_id'=> $car->id,'session_car_vin'=> $car->vin,'session_car_title'=> $car->heading, 'session_status'=> 'just_question','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}" class="btn btn-primary btn-block"><span class="fa fa-question-circle"></span>  Just a Question  </a>

									</div>
                                </div>
								@endguest
								 
									
									
                                       
                                    </div>
                                </div>
							
							  <!-- START CHECK CAR COMPARE -->	
							  
								@auth	
								<?php 
								$check_car_compare_status =  DB::table('car_campare')->where('car_id',$car->id)->where('client_id', Auth::id())->get();
								?>
								@endauth
								
								@guest
								<?php 
								$check_car_compare_status =  DB::table('car_compare_temp')->where('session_carid',$car->id)->where('sessionID', Session::getId())->get();
								?>
								@endguest
								
								@if(count($check_car_compare_status))
								
								<a class="btn btn-primary btn-block mb-2" style="color:#ccc;" disabled><span class="fa fa-check fa-lg"></span> already Compared</a>
								
								@else
								
								@guest
                                <div class="row">
                                    <div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="javascript:save_car_compare('<?=$car->id?>');"><span class="fa fa-clone"></span> Compare</a></div>
                                </div>
								@endguest
								
								@auth	
                                <div class="mb-2"><a class="btn btn-primary btn-block" href="javascript:save_car_compare('<?=$car->id?>');"><span class="fa fa-clone"></span> Compare</a></div>
								@endauth
                                
								@endif
								
							<!-- END CHECK CAR COMPARE -->	
							
							   @if(count(Auth::user()))
	
							   <?php
							   $rating_status = DB::table('client_reviews')->where('client_id',Auth::user()->id)->where('car_id','=',$car->id)->where('reply_to','=',null)->get()->first();
							   $check_block_status = DB::table('settings')->where('car_id',$car->id)->where('reviews_off',1)->get()->first();
							   ?>
							
							   @if(count($check_block_status))
								 <div class="row">
                                    <div class="col-12 mb-2"> 
									<!--<a class="btn btn-primary btn-block" href="javascript:rating_blocked();"><span class="fa fa-line-chart"></span> Reviews </a>-->
									</div>
                                </div>			
	
							   @else
								   
							   @if(count($rating_status))
							   <div class="row">
                               <div class="col-12 mb-2"> 
							   <!--<a class="btn btn-success btn-block" href="car/detail/<?//=$car->id?>"><span class="fa fa-line-chart" style="color:#fff;"></span> Reviewed</a>-->
							   </div>
 							   @else
							   <div class="row row-custom-margin">
                               <div class="mb-2"> 
							   <!--<a class="btn btn-primary btn-block" href="javascript:car_rating('<?//=$car->id?>', <?//=$car->dealer->id ?>);"><span class="fa fa-line-chart"></span> Reviews </a>-->
							   </div>
							   
							   @endif
							   
							    @endif
							 
							    @if(!count($check_block_status))
								@include('web_pages.includes.rating-save-modal')
								@endif
							 
							   @endif
							  
							
							   @guest
							   
							   <div class="row">
                                 <!-- <div class="col-12 mb-2"> 
								<!--<a href="{{url('/login')}}" class="btn btn-primary btn-block"><span class="fa fa-line-chart"></span> Reviews </a>
								</div>-->
                                </div>	
                               @endguest
								
								
								  @auth
								  
                                   <a href="javascript:services_email('<?=$car->id?>', '<?=$car->dealer->id ?>','<?=$get_dealer_email->dealer_email ?>','<?=$get_dealer_email->dealer_phone ?>','<?=$get_dealer_email->dealer_name ?>','<?=$get_dealer_email->dealer_other_phone ?>','<?=$car->heading?>');" class="btn btn-primary btn-block mb-2"><span class="fa fa-comment"></span>  Anonymous Contact</a>
                                
								  @endauth
									

								@guest
                                <div class="row ro">
                               <div class="col-12 mb-2">
							   <a href="{{route('search_buttons_session',['session_car_id'=> $car->id,'session_car_vin'=> $car->vin,'session_car_title'=> $car->heading, 'session_status'=> 'Anonymous_Contact','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}" class="btn btn-primary btn-block"><span class="fa fa-comment"></span>  Anonymous Contact</a>
							   </div>
                                </div>
                            
								@endguest
								
								<!-- SAVE BUTTON START -->
								
								 @auth
											<?php $tot = DB::table('client_favourite')->where('client_id',Auth::id())->where('car_id',$car->id)->count();?>
                                            @if($tot==0)
												<?php
												$save_car= array(
													'id' => (isset($car->id) ? $car->id : ''),
													'title' => (isset($car->heading) ? $car->heading : '--'),
													'car_type' => (isset($car->inventory_type) ? $car->inventory_type : '--'),
													'price' => (isset($car->price) ? $car->price : '--'),
													'miles' => (isset($car->miles) ? $car->miles : '--'),
													'build' => (isset($car->build)) ? $car->build: '',
													'images' => $car_images
												);
												
												?>
                                                <input type="hidden" id="save_car_id_{{$i}}" value="{{$car->id}}">
                                                <input type="hidden" id="save_car_{{$i}}" value="{{base64_encode(json_encode($save_car))}}">
                                                
												<a class="btn btn-primary btn-block" href="javascript:save_car({{$i}});"><span id="save_car_span_{{$i}}" class="fa fa-save"></span> <span id="jq_save_tag_{{$i}}">Save</span></a>
                                                                                          
										   @else
                                                <a class="btn btn-primary btn-block" href="javascript:void(0);">
                                                    <span class="fa fa-check"></span>
                                                    <span>Saved</span>
                                                </a>
                                            @endif

                                        @endauth
																				
                                        @guest
                                        <a href="{{route('search_buttons_session',['session_car_id'=> $car->id,'session_car_vin'=> $car->vin,'session_car_title'=> $car->heading, 'session_status'=> 'Save_Car','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}" class="btn btn-primary btn-block"><span class="fa fa-save"></span> Save </a>
                                        @endguest
								
								
								<!-- END SAVE BBUTTON -->
								
                            </div>
                        </div>
                    </div>
                </div>
		@auth
		</div>
		
		
		@endauth
		
	
		<script>

		$(document).on("click",".btn-circle,.compare-close",function() {
			location.reload();
			exit;
		});

		

		</script>
			
	
	<!-- COMMUNICATION BUTTONS INCLUDED -->
	