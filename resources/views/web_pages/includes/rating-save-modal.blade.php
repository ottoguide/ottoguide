 @if(count(Auth::user()))
	
  <div class="modal" id="primary">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header modal-header-primary">
          <h4 class="modal-title rating-title">Rate and review <br>
		  <small>2018 Toyota Corolla XLi Automatic</small>
		
		  </h4> <br>
		  
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
		
		
        <div class="modal-body">
	
		 <div class="incoming_msg">
              <div class="incoming_msg_img"><img src="{{isset(Auth::user()->image)?Auth::user()->image:''}}" alt="user_image" width="40px"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
				  <b>{{isset(Auth::user()->name)?Auth::user()->name:''}}</b><br>
					<h6>Your review will be posted on the web</h6>
              </div>
            </div>
		</div>
	   
	   <br><br>
			<div class="row abb4">
				<div class="col-md-5 white">
					<div class='rating-stars'>
					  <ul id='stars'>
					  <li class='star' title='Poor' data-value='1'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='Fair' data-value='2'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='Good' data-value='3'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='Excellent' data-value='4'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='WOW!!!' data-value='5'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					</ul>
				</div>
			
			</div>
			
				<div class="col-md-7 white">
				<div class='success-box'>
				<div class='clearfix'></div>
				<div class="text-message2"></div>
				<div class='text-message'style='font-weight:bold;'></div>
				<div class="text-message3"></div>
				<div class='clearfix'></div>
				
				</div>
				</div>
			</div>
		

		
		   <div class="type_msg">
		 
			<div class="input_msg_write">
            <input type="text" class="title_msg"  name="title" style="border-bottom:1px solid #e2e2e2; border-radius: 5px;"placeholder="Write the title of the message" required />
            </div>
			<br>
			<div class="input_msg_write">
            <input type="text" class="write_msg"  name="message" style="border-bottom:1px solid #e2e2e2; border-radius: 5px;" placeholder="Share detail of your own experince on this car" required />
            </div>
			
			<input type="hidden" name="dealer_id" value="">
			<input type="hidden" name="car_id" value="">

		   </div>
		
	
		
	
		   </div>
        
		
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
		  <button type="button" class="btn btn-primary post_message" >Post</button>
        </div>
        
			<input type="hidden" class="form-control" id="rating" name="rating">
	
      </div>
    </div>
  </div>
  
  	@endif
  