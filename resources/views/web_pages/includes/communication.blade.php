<?php
use App\Helpers\Common;
$get_dealer_email = DB::table('dealer')->where('dealer_id',$car->dealer->id)->get()->first();
$dealer_id = $car->dealer->id;
?>
<form id="services_email" method="post" action="{{url('user-services-email.html')}}?email_subject=Car Deal of ">
    {!! Form::hidden('_token', csrf_token()) !!}
    <input type="hidden" name="dealer_id" class="dealer_id" >
    <input type="hidden" name="dealer_name" class="dealer_name"> 
	<input type="hidden" name="car_heading" class="car_heading">
</form>

		<!-- Modal -->
<div class="modal fade" id="dealer-contact-modal" data-backdrop="static"  data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="contact-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contact-modal">Dealer Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
			
                <center>
                    @auth
					  <!-- Get Dealer Car Id  dealer email -->
						<input type="hidden" class="car_id" >
						<input type="hidden" class="dealer_email">
						<input type="hidden" class="dealer_other_phone">
					
                        <a href="javascript:void(0);" style="width: 50%;" class="contact-btn btn btn-block btn-primary" onclick="$('#num-div').show() , $('.contact-btn').hide();">
                            <i class="fa fa-phone "></i> Call Dealer</a><!--$car_detail->dealer->id-->
                        <button style="width: 50%;" class="contact-btn btn btn-block btn-primary chat-button"><i class="fa fa-comments"></i> Text with Dealer</button>
						<button type="submit" style="width: 50%;" form="services_email" class="contact-btn btn btn-block btn-primary"><i class="fa fa-envelope"></i> Email</button>
                         
						 <div id="num-div" style="width: 85%;display: none;border: 1px dashed #ddd; margin: 10px;padding: 10px;">
						  <p style="color:#cc0000; margin-bottom: 2%;line-height:1.4; font-size:11.5px; font-family: 'Century Gothic',CenturyGothic,AppleGothic,sans-serif;"><b>Connect with this Dealership Anonymously via Phone Call.</b><br>
We have picked up the following number from your profile , If you want to use a different number for this call then enter it in the text box below!</p>
                            
							<br><h5><b style="font-family: 'Century Gothic',CenturyGothic,AppleGothic,sans-serif;">Where you want to receive this call?</b></h5>
                             <input type="text" id="user-phone" class="form-control" value="{{Auth::user()->phone}}" /><br>
                             <input type="hidden" id="dealer-phone" class="form-control"/>
							 
						  <a href="javascript:void(0);" style="width: 50%;" class="call-button btn btn-block btn-primary">
                             <i class="fa fa-arrow-right "></i> Go</a>
							 <br>
							<p style="color:#4; margin-bottom: 2%;line-height:1.4; font-size:11.5px; font-family: 'Century Gothic',CenturyGothic,AppleGothic,sans-serif;"><b>At OTTO Guide we are furiously commited to the Privacy of our Customers and Dealerships.</b><br>
Any number that you share will never be exposed to the Dealership or any 3rd Party! When you click the "Connect" button, our Intellegent Comminication Platform will use an intermediate number to join both parties in the phone call.
</p>                         </div>
                        <div id="div1" style="display: none">
                            <span>This dealer is not in contact please see the similar cars</span>
                                <br>
                                <br>
                            <?php
                                $param="";
                                foreach ($_GET as $k => $p){
                                    $param.=$k.'='.$p.'&';
                                }
                            ?>
                            <a href="{{url('/search-car.html?'.$param.'vin='.$car->vin)}}" style="width: 50%;" class="btn btn-block btn-primary">
                                <i class="fa fa-arrow-right "></i> Show Similar Cars</a>
                        </div>
                    @endauth

                    @guest
					
						<input type="hidden" class="car_id" >
						<input type="hidden" class="dealer_email">
					
						    <div id="div1" style="display: none">
                            <span>This dealer is not in contact please see the similar cars</span>
                                <br>
                                <br>
                            <?php
                                $param="";
                                foreach ($_GET as $k => $p){
                                    $param.=$k.'='.$p.'&';
                                }
                            ?>
                            <a href="{{url('/search-car.html?'.$param.'vin='.$car->vin)}}" style="width: 50%;" class="btn btn-block btn-primary">
                                <i class="fa fa-arrow-right "></i> Show Similar Cars </a>
                        </div>
					
					
                        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary contact-btn"><i class="fa fa-phone"></i> Call Dealer</a>
                        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary contact-btn"><i class="fa fa-comments"></i> Chat Dealer</a>
                        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary contact-btn"><i class="fa fa-envelope"></i> Email</a>
                    
					@endguest
                </center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

	
	
	<!-- Modal -->
<div class="modal fade" id="user-contact" data-backdrop="static"  data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="contact-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contact-modal">Dealer Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <center>
                    @auth
                        <input type="text" class="form-control" value="{{Auth::user()->mobile}}" /><br>
                        <a href="javascript:void(0);" style="width: 50%;" class="call-button btn btn-block btn-primary">
                            <i class="fa fa-arrow-right "></i> Go</a>
                        <div id="div1"></div>
                    @endauth
                </center>
            </div>
			
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
	
	
 <script>
	
	function services_email(carid,dealerid,dealer_email,dealer_phones,dealer_name,dealer_other_phone,car_heading){


        $.ajax({
            type:'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                dealer_id:dealerid
            },
            url:'{{url('check/dealer/register')}}',
            success: function (res) {
                //if(res == 1){
                    $("#div1").hide();
                    $(".contact-btn").show();
					$('#dealer-phone').val(dealer_phones);
					$('.dealer_id').val(dealerid);
					$('.dealer_name').val(dealer_name);
					$('.car_id').val(carid);
					$('.dealer_email').val(dealer_email);
					$('.dealer_other_phone').val(dealer_other_phone);
					$('.car_heading').val(car_heading);
                //}

                //else{
                //    $(".contact-btn").hide();
                //    $("#div1").show();
                //}
            }
        });
         $('#num-div').css("display", "none");
        $('#dealer-contact-modal').modal('show');
    }



    $(document).ready(function(){
		
	$(".chat-button").click(function() {
   
	var	carid = $('.car_id').val();
	var	dealerid = $('.dealer_id').val();
	var	dealer_email = $('.dealer_email').val();

	var chat_url ="{{url('mychat.html?')}}"+'car_id='+carid+'&dealer_id='+dealerid+'&channel_name='+dealer_email+'-{{Auth::user()->email}}';

	window.location.href = chat_url;
	
	});	
	
	
	});


    $(document).ready(function(){

        var callStatus = '';
        var callSid = '';
        $(".call-button").click(function(){	
        $(this).html('Connecting...');
        var user_phone = $('#user-phone').val();
        var dealer_phone = $('#dealer-phone').val(); 
		var dealer_id = $('.dealer_id').val();
		var other_phone = $('.dealer_other_phone').val();
	
        $.ajax({
            url: "{{url('twilio-call-chat/call.php')}}",
            type: "POST",
            data: {id: 1, user_phone:user_phone,dealer_phone:dealer_phone,dealer_id:dealer_id,other_phone:other_phone},
            dataType: 'json',
            error: function(xhr, status, error) {
                console.log(xhr.status + status+error);
            },
            success: function(result){
                <?php Common::addon_inc($dealer_id,'voice_lead_addon',1); ?>
                console.log(result.call_sid);
                callSid = result.call_sid;
                callStatus = setInterval(function () {
                    checkCallStatus(result.call_sid)
                }, 2000);
            }
        });
    });

        function checkCallStatus(sid){

            $.ajax({
                url: "{{url('twilio-call-chat/call_status.php')}}",
                type: "POST",
                data: {call_sid: sid},
                dataType: 'json',
                success: function(result){
                    console.log(result.call_status);
                    if(result.call_status == 'queued' || result.call_status == 'initiated'){
                        $(".call-button").html('Connecting...');
                    }else if(result.call_status == 'ringing'){
                        $(".call-button").html('Ringing...');
                    }
                    else if(result.call_status == 'in-progress'){
                        $(".call-button").html('Connected!');
                    }
                    else if(result.call_status == 'busy' || result.call_status == 'failed' || result.call_status == 'no-answer'){
                        $(".call-button").html('Call Failed');
                        stopFunction();
                    }
                    else if(result.call_status == 'completed'){
                        $(".call-button").html('Call Again');
                        callSid = '';
                        stopFunction();
                    }
                }
            });
        }

        function stopFunction(){
            clearInterval(callStatus);
        }

	 });


    </script>