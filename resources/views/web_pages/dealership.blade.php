@extends('layouts.web_pages')
@section('content')
<style>
video {
	/* video border */
	border: 1px solid #ccc;
	padding: 20px;
	margin: 10px;
	border-radius: 20px;
	/* tranzitionstransitions applied to the vodeovideo element */
	-moz-transition: all 1s ease-in-out;
	-webkit-transition: all 1s ease-in-out;
	-o-transition: all 1s ease-in-out;
	-ms-transition: all 1s ease-in-out;
	transition: all 1s ease-in-out;
}


/* background color and gradient */

video,
#start,
#stop,
#pause,
#plus,
#minus,
#mute {
	/* background color */
	background-color: #ffcccc;
	/* background gradient */
	background-image: linear-gradient(top, #fff, #e2e2e2);
	background-image: -moz-linear-gradient(top, #fff, #e2e2e2);
	background-image: -webkit-linear-gradient(top, #fff, #e2e2e2);
	background-image: -o-linear-gradient(top, #fff, #e2e2e2);
	background-image: -ms-linear-gradient(top, #fff, #e2e2e2);
}


/* shadows */

video,
#start,
#stop,
#pause,
#plus,
#minus,
#mute {
	box-shadow: 0 0 10px #ccc;
}

video:hover,
video:focus,
#start:hover,
#stop:hover,
#pause:hover,
#plus:hover,
#minus:hover,
#mute:hover {
	/* glow */
	box-shadow: 0 0 20px #e2e2e2;
}

#controls {
	display: none;
	margin: 10px 30px;
}


/* style for buttons */

#start,
#stop,
#pause,
#plus,
#minus,
#mute {
	border: 1px solid #ccc;
	padding: 5px;
	border-radius: 10px;
	width: 60px;
	margin: 0 10px 0 0;
}


#butterfly {
	position: absolute;
	left: 0;
	top: -6px;
	background-image: url(butterfly.png);
	width: 40px;
	height: 40px;
}

#progressbar {
	display: none;
	/* size */
	width: 500px;
	height: 20px;
	/* position and border */
	position: relative;
	border: 1px solid #ccc;
	margin: 10px;
	border-radius: 20px;
	/* background color */
	background-color: #cccccc;
	/* background gradient */
	background-image: linear-gradient(top, #fff, #ccc);
	background-image: -moz-linear-gradient(top, #fff, #ccc);
	background-image: -webkit-linear-gradient(top, #fff, #e2e2e2);
	background-image: -o-linear-gradient(top, #fff, #ccc);
	background-image: -ms-linear-gradient(top, #fff, #ccc);
	/* shadow */
	box-shadow: 0 0 10px #ccc;
}

#loadingprogress {
	/* border */
	border-radius: 20px;
	/* initial size */
	height: 20px;
	width: 0;
	/* background color */
	background-color: #9acd00;
	/* background gradient */
	background-image: linear-gradient(top, #ffffff, #9acd00);
	background-image: -moz-linear-gradient(top, #ffffff, #9acd00);
	background-image: -webkit-linear-gradient(top, #ffffff, #9acd00);
	background-image: -o-linear-gradient(top, #ffffff, #9acd00);
	background-image: -ms-linear-gradient(top, #ffffff, #9acd00);
}

</style>

<div class="header-margin py-4 bg-secondary">
    <div class="container">
      <div class="row">                    
        <div class="col-md-12 col-sm-12">
          <h1>Dealership Overview</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="py-4">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12">
		
          <p>
					  <video width="100%"  preload="none"  controls autoplay>
			  <source src="<?=url('videos/Dealership Overview.webm')?>" type="video/webm">
			  
			</video>

          </p>
         
        <br>  
        </div>
      </div>
    </div>      
  </div>
@endsection
