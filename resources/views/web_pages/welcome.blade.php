@extends('layouts.web_pages')
@section('content')

  <style>
    .img-box img{
      border-radius: 50%;
      border:3px solid #ccc;
      width:150px;
    }
    .fa-angle-left,.fa-angle-right{
      color: #333;
      font-size:25px;
      font-weight:bold;
      padding-right:25px;
    }
    .thumb-content h4{
      font-weight: bold;
      font-size: 16px;
    }
    #video-background{
      width: 100%;
    }
    .bg-trans0 {
      background-color: #E77800;
      opacity: 0.9;
    }
    .box-eq {
      padding-top: 10%;
      padding-bottom: 10%;
      display: block;
      color: #FFF;
      font-size: 30px;
      line-height: 30px;
    }
    .box-eq img {
      padding-bottom: 2%;
      width: 65%;
    }
  </style>

  <div id="cxm-home-slider" class="carousel slide" data-ride="carousel">
    <!--   
    <ol class="carousel-indicators">
      <li data-target="#cxm-home-slider" data-slide-to="0" class="active"></li>
      <li data-target="#cxm-home-slider" data-slide-to="1"></li>
      <li data-target="#cxm-home-slider" data-slide-to="2"></li>
    </ol>
    -->
    <div class="cxm-slider-content">
      <div class="container py-5">
        <div class="row justify-content-center">
          <div class="col-sm-4 col-xs-4 col-lg-4 text-center">
            <a class="box-eq bg-trans2" href="{{url('/search-car.html')}}">Car Shopping<br><br><img class="img-fluid" src="{{asset('images/search-car-whi.svg')}}" ><br>Find a Perfect Car</a>
          </div>
          <div class="col-sm-4 col-xs-4 col-lg-4 text-center">
            <a class="box-eq bg-trans0" href="{{url('faq-search.html')}}">Car Quiz<br><br><img class="img-fluid" src="{{asset('images/quiz-car-whi.svg')}}"><br> OTTO Intelligence</a>
          </div>
          <div class="col-sm-4 col-xs-4 col-lg-4 text-center">
            <a class="box-eq bg-trans3" href="{{route('CarService')}}">Car Service<br><br><img class="img-fluid" src="{{asset('images/service-car-whi.svg')}}"><br> Service a Car I own</a>
          </div>
        </div>
      </div>
    </div>
          
    <div class="carousel-inner">
      <video autoplay loop id="video-background" muted plays-inline>
        <source src="{{url('/video/Cover-Lulu.webm')}}" style="background-size:cover; width:100%; height:100%;" type="video/webm">
      </video>
    </div>
    <!--
    <a class="carousel-control-prev" href="#cxm-home-slider" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"><span class="fa fa-chevron-left"></span></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#cxm-home-slider" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"><span class="fa fa-chevron-right"></span></span>
      <span class="sr-only">Next</span>
    </a>
    -->
  </div>
      
  <div class="py-5 bg-light">    
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="heading heading-lg fc1 text-center mb-5">Car Shopping the OTTO Guide way</div>
          <div class="timeline">
            <div class="timeline-container timeline-left">
              <div class="num">1</div>
              <div class="timeline-content text-center">
                <div class="heading mb-3">Search and Find that Perfect Car</div>
                <p class="mb-4 text-primary font-weight-bold">Search more than 7 million New and Used Vehicles from over 52,000 Dealerships across the United States.</p>
                <p class="mb-4 text-primary font-weight-bold">Search and Shop Dealerships to service a Car you Own</p>
                <div class="text-center mt-3 mb-4">
                  <a class="mr-4" href="{{url('search-car.html')}}"><img width="18%" class="img-fluid" src="{{asset('/images/search-car-mid.svg')}}"></a>
                  <a class="mr-4" href="{{url('faq-search.html')}}"><img width="18%" class="img-fluid" src="{{asset('/images/quiz-car-mid.svg')}}"></a>
                  <a href="{{url('services')}}"><img width="18%" class="img-fluid" src="{{asset('/images/service-car-mid.svg')}}"></a>
                </div>
              </div>
            </div>
            <div class="timeline-container timeline-right">
              <div class="num">2</div>
              <div class="timeline-content text-center">
                <div class="heading mb-3">Shop Anonymously at your own Pace</div>
                <p class="mb-4 text-primary font-weight-bold">Avoid the Pressure Sales Process, communicate on the OTTO Guide Platform and only proceed when you are ready to Buy!</p>
                <div class="text-center mb-3">
                  <img class="img-fluid" src="{{asset('/images/how-it-works-anon-con.svg')}}">
                </div>
              </div>
            </div>
            <div class="timeline-container timeline-left">
              <div class="num">3</div>
              <div class="timeline-content text-center">
                <div class="heading mb-3">Craft Car Deals with Dealerships</div>
                <p class="mb-4 text-primary font-weight-bold">Request Best Offers and Send Counter Offers Anonymously and from the comfort of your Couch.</p>
                <div class="text-center"><img class="img-fluid" src="{{asset('images/13.png')}}"></div>
              </div>
            </div>
            <div class="timeline-container timeline-right">
              <div class="num">4</div>
              <div class="timeline-content text-center">
                <div class="heading mb-3">Review and Rate your Experience</div>
                <p class="mb-4 text-primary font-weight-bold">A good deal is one you feel good about, please share your feel good experience and help other shoppers.</p>
                <div class="mb-4 text-center"><img width="87px" class="img-fluid" src="{{asset('images/14.png')}}"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>      
  </div>
      
  <div class="py-5 border-top">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="text-center mt-3">
            <a class="" href="/search-car.html" style="margin: 0 50px;"><img width="9%" class="img-fluid" src="{{asset('/images/search-car-drk.svg')}}"></a>
            <a href="{{url('faq-search.html')}}" style=" margin: 0 50px;"><img width="9%" class="img-fluid" src="{{asset('/images/quiz-car-drk.svg')}}"></a>
            <a href="/car/services" style=" margin: 0 50px;"><img width="9%" class="img-fluid" src="{{asset('/images/service-car-drk.svg')}}"></a>
          </div>
        </div>
      </div>
    </div>    
  </div>

  <!-- REMOVED BY Amin   
      <div class="py-5" style="background-color:#ccc;">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="heading heading-lg mb-4" style="color:#333;">Latest Blog Articles</div>

              <div id="cxm-slider" class="carousel slide" data-ride="carousel">  
                <div class="carousel-inner">
                <h5 align="left" style="color:#333">"Blog Content here"</h5>
                </div>
              </div>                       
            </div>
          </div>
        </div>
      </div>
  -->

  <div class="py-5 bg-secondary">
    <div class="container">
      <div class="row">
        <h2 class="fc1 mb-3">What our satisfied customers are saying...</h2>
        <div class="col-md-12 col-center m-auto">
          <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
            <!-- Carousel indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>   
            <!-- Wrapper for carousel items -->
            <br>
            <div class="carousel-inner">
              <div class="item carousel-item active">
                <div class="row">
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumb-wrapper">
                      <center>
                        <div class="img-box">
                          <img src="{{asset('images/1.jpg')}}" align="center" class="img-responsive img-fluid " alt="">
                        </div>
                      </center>
                      <div class="thumb-content"><br>
                        <h4 align="center">ANTONIO MORENO </h4>
                        <p align="center">When I wanted to buy a Toyota I used Ottoguide and it was easy. I requested best offer from 3 dealerships all the pricing was up front and their were no surprises. I could have even had the car and paperwork brought to my home. I recommend ottoguide 100%.</p>
                      </div>						
                    </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumb-wrapper">
                      <center>
                        <div class="img-box">
                          <img src="{{asset('images/2.jpg')}}" align="center" class="img-responsive img-fluid " alt="">
                        </div>
                      </center>
                      <div class="thumb-content"><br>
                        <h4 align="center">PAULA WILSON</h4>
                        <p align="center">Easy way to find a used car.</p>
                      </div>						
                    </div>
                  </div>	
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumb-wrapper">
                      <center>
                        <div class="img-box">
                          <img src="{{asset('images/3.jpg')}}" align="center" class="img-responsive img-fluid " alt="">
                        </div>
                      </center>
                      <div class="thumb-content"><br>
                        <h4 align="center">MICHAEL HOLZ</h4>
                        <p align="center">I used all websites Truecar, autotrader, cargurus and got a 1000 emails and 100s of unwanted calls it was like I was buying a timeshare. Ottoguide was easy dealerships had to contact me thru the website and I had the choice to pick from them!.</p>
                      </div>						
                    </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumb-wrapper">
                      <center>
                        <div class="img-box">
                          <img src="{{asset('images/4.jpg')}}" align="center" class="img-responsive img-fluid " alt="">
                        </div>
                      </center>
                      <div class="thumb-content"><br>
                        <h4 align="center">PAUL ADOMS</h4>
                        <p align="center">It was easy to shop for a car and easy to set a service appointment pretty cool site.</p>
                      </div>						
                    </div>
                  </div>
                </div>
              </div>
              
              
              <div class="item carousel-item ">
                <div class="row">
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumb-wrapper">
                      <center><div class="img-box">
                        <img src="{{asset('images/5.jpg')}}" align="center" class="img-responsive img-fluid " alt="">
                      </div></center>
                      <div class="thumb-content"><br>
                        <h4 align="center">CARL PERAZZO</h4>
                        <p align="center">When I wanted to buy a car I did not know where to start. Thank you ottoguide for making it easy two thumbs up.</p>
                        
                      </div>						
                    </div>
                  </div>
                    <div class="col-sm-3 col-xs-12">
                    <div class="thumb-wrapper">
                      <center><div class="img-box">
                        <img src="{{asset('images/6.jpg')}}" align="center" class="img-responsive img-fluid " alt="">
                      </div></center>
                      <div class="thumb-content"><br>
                        <h4 align="center">DOUGLAS MACARTHUR</h4>
                        <p align="center">Super easy!</p>
                        
                      </div>						
                    </div>
                  </div>	
                  
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumb-wrapper">
                      <center><div class="img-box">
                        <img src="{{asset('images/77.jpg')}}" align="center" class="img-responsive img-fluid " alt="">
                      </div></center>
                      <div class="thumb-content"><br>
                        <h4 align="center">JANE ALICE</h4>
                        <p align="center">I know how car dealerships work. I found cars that have been sitting on the lot ottoguide.com made it easy and I got a great deal.</p>
                        
                      </div>						
                    </div>
                  </div>
                  
                  <div class="col-sm-3 col-xs-12">
                    <div class="thumb-wrapper">
                      <center><div class="img-box">
                        <img src="{{asset('images/8.jpg')}}" align="center" class="img-responsive img-fluid " alt="">
                      </div></center>
                      
                      <div class="thumb-content"><br>
                        <h4 align="center">EDWARD ABBEY</h4>
                        <p align="center">Use ottoguide to buy a Toyota! It took 5 minutes.</p>
                      </div>						
                    </div>
                  </div>
                  
                </div>
              </div> 
              
            
            </div>
            <!-- Carousel controls -->
            <a class="carousel-control left carousel-control-prev" href="#myCarousel" data-slide="prev" style="width:1%; font-weight:bold; color:#222;">
              <i class="fa fa-angle-left"></i>
            </a>
            <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next" style="width:1%; font-weight:bold;color:#222;">
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div> 
    
  @if(Session::has('rbo_session'))
    <div id="modalOnLoad" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body text-center mt-4">
            <div class="text-center"><img class="img-fluid" src="{{asset('images/13.png')}}"></div>
              <div>
                <p class="heading font-weight-bold">Your "Request Best Offer" is pending!</p>
                <p class="">Please continue to the Detail Page of that car to submit it.</p>
                <a href="/car/detail/{{ Session::get('rbo_session')['session_car_id'] }}" class="btn btn-success mb-4">Proceed to Car Details</a>
              </div>
          </div>
        </div>
      </div>
    </div>
    <script>
      $(document).ready(function () {
          $("#modalOnLoad").modal('show');
        });
    </script>
  @endif

  @if(Session::has('quiz_session'))

    <div id="modalOnLoad" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-body text-center mt-4">
            <div class="text-center mb-4"><img width="18%" class="img-fluid" src="{{asset('/images/quiz-car-mid.svg')}}"></div>
            <div>
              <p class="heading font-weight-bold">Your "Car Quiz" is pending.</p>
              <p>Please continue to see the results and save the Car Quiz in your Profile</p>
              <a href="{{url('/save/quiz/session')}}" class="btn btn-success">See Quiz Results</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
      $(document).ready(function () {
          $("#modalOnLoad").modal('show');
        });
    </script>
  @endif

@endsection