<p>
<h4 align="center"><b>LEGAL TERMS & CONDITIONS</b></h4><br>

We value the trust you place in Address Home. That's why we insist upon the highest standards for secure transactions and customer information privacy. Please read thefollowing statement to learn about our privacy policy. Our privacy policy is subject to change at any time without notice. To make sure you are aware of any changes, please review this policy periodically.<br>

<br>
We collect personally identifiable information (email address, name, phone number, etc.) from you when you set up a free account with Address Home. While you can browse some sections of our Website without being a registered member, certain activities (such as placing an order) do require registration. <br>
<br>
We do use your contact information to send you offers based on your previous orders and your interests. We use personal information to provide the services you request. To the extent we use your personal information to market to you, we will provide you the ability to opt-out of such uses.<br>
<br>
We use your personal information to resolve disputes, trouble shoot problems, help promote a safe service, collect monies owed, measure consumer interest in our productsand services, inform you about online and offline offers, products, services, and updates,to customize your experience, detect and protect us against error, fraud and other criminal activity, to enforce our terms and conditions and as otherwise described to you at the time of collection.<br>


</p>