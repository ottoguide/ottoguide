@extends('layouts.web_pages')
@section('content')


	<style>
		.table tbody > tr > td {
			font-size:13px;
			background: #f5f5f5;
			background-color: rgb(245, 245, 245);
			border-top: 2px solid #fff;
			border-right: 2px solid #fff;
			vertical-align: middle;
			padding: 9px 7px;
			overflow: hidden;
		}
		.checkbox{
			font-size: 15px;
			color:#444;
			font-weight:bold;
		}
		.collapse{
			margin-bottom: 5px;
		}

		.form-field{
			line-height: 30px;
			margin-bottom: 10px;

		}

		.multi-select{
			padding: 10px;
		}

		span{
			padding: 12px;
			font-size: 14px;
			color:#0F346C;
			font-weight:bold;

		}
		.collapsed{
		width:100%;	
			
		}
		
		 .checkbox{
			
			font-family: 'Century Gothic',CenturyGothic,AppleGothic,sans-serif;
			padding:5px;
			border:1px solid #e2e2e2;
			border-radius: 5px;
			background: rgb(255,255,255);
			background: -moz-linear-gradient(left, rgba(255,255,255,1) 0%, rgba(243,243,243,1) 50%, rgba(237,237,237,1) 51%, rgba(255,255,255,1) 100%);
			background: -webkit-linear-gradient(left, rgba(255,255,255,1) 0%,rgba(243,243,243,1) 50%,rgba(237,237,237,1) 51%,rgba(255,255,255,1) 100%);
			background: linear-gradient(to right, rgba(255,255,255,1) 0%,rgba(243,243,243,1) 50%,rgba(237,237,237,1) 51%,rgba(255,255,255,1) 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff',GradientType=1 );
					
		 }

	 .tab-width{
		 width:100%;
	 }
	 .fa-check{
	  color: #2E88C9;
	   padding-top: 5px;
     display:none;	   
	 }
	 
	</style>
	<div class="header-margin">
		<div class="container">

			@if($errors->any())
				<p class="alert alert-success">Search Saved successfully </p>
			@endif
			<div class="row">
				<div class="col-sm-12">
					<div class="card refine-search">
						<div class="card-header font-weight-bold"><a data-toggle="collapse" href="#refineSearch" role="button" aria-expanded="false" aria-controls="refineSearch">Put your car details</a></div>
						<div class="collapse show" id="refineSearch">
							<div class="card-body">

								{!! Form::open(array('url' => '/save/services', 'enctype' => 'multipart/form-data')) !!}
								{!! Form::hidden('_token', csrf_token()) !!}

								<div class="row">

									<div class="col-sm-10 mb-3">

										<div class="row">

											<div class="col-sm-5 mb-3">

												<select id="year" name="year" class="form-control form-control-sm">
													<option value="main_category" disabled selected>YEAR</option>
												 @if(!empty(Session::get('services')['Year']))
												<option value="@if(!empty(Session::get('services')['Year']) > 0){{Session::get('services')['Year']}}@endif"selected>@if(!empty(Session::get('services')['Year']) > 0){{Session::get('services')['Year']}}@endif</option>
												@endif
													<?php
													for($i=20; $i >= 0 ;$i--) {
														$year = date('Y') - 20 + $i;
														echo '<option value='.$year.'>'.$year.'</option>';
													}
													?>

												</select>
											</div>

											<div class="col-sm-5 mb-3">
												<select  id="make" name="make" class="form-control form-control-sm">
													<option value="main_category" disabled selected>MAKE</option>
												@if(!empty(Session::get('services')['Make']))
												<option value="@if(!empty(Session::get('services')['Make']) > 0){{Session::get('services')['Make']}}@endif"selected>@if(!empty(Session::get('services')['Make']) > 0){{Session::get('services')['Make']}}@endif</option>
												@endif
													@foreach($make->facets->make as $m)
														<option value="{{$m->item}}">{{$m->item}}</option>
													@endforeach
												</select>
											</div>

										</div>


										<div class="row">
											<div class="col-sm-5 model-div">
												<select  id="model" name="model" class="form-control form-control-sm">
												 @if(!empty(Session::get('services')['Model']))
												<option value="@if(!empty(Session::get('services')['Model']) > 0){{Session::get('services')['Model']}}@endif"selected>@if(!empty(Session::get('services')['Model']) > 0){{Session::get('services')['Model']}}@endif</option>
												 @endif
												 <option value="main_category" disabled >MODEL</option>
												</select>
											</div>

											<div class="col-sm-5 mb-3">
												<select id="trim" name="trim" class="form-control form-control-sm">
											    @if(!empty(Session::get('services')['Trim']))
												<option value="@if(!empty(Session::get('services')['Trim']) > 0){{Session::get('services')['Trim']}}@endif"selected>@if(!empty(Session::get('services')['Trim']) > 0){{Session::get('services')['Trim']}}@endif</option>
												@endif
													<option value="main_category" disabled >TRIM</option>
												
												</select>
											</div>

										</div>


										<div class="row">
											<div class="col-sm-5 mb-3">
												<input type="text" id="mileage" name="mileage" value="@if(!empty(Session::get('services')['Mileage']) > 0){{Session::get('services')['Mileage']}}@endif" placeholder="Put Your Mileage" class="form-control">
											</div>

											<div class="col-sm-6 mb-3">
												Find me a Car within
												<select class="form-control form-control-sm w-auto d-inline-block mb-1" name="radius" id="radius_select" onchange="$('#radius').val($(this).val());">
                                                    <?php
                                                    use Illuminate\Support\Facades\Session;


                                                    $one = "";
                                                    $two = "";
                                                    $three = "";
                                                    $four = "";
                                                    $five = "";
                                                    $any = "";

                                                    if(Session::has('search_radius')){
                                                        $session_radius = Session::get('search_radius');

                                                        if($session_radius == 10){
                                                            $one = 'selected';
                                                        }
                                                        else if($session_radius == 20){
                                                            $two = 'selected';
                                                        }
                                                        else if($session_radius == 30){
                                                            $three = 'selected';
                                                        }
                                                        else if($session_radius == 40){
                                                            $four = 'selected';
                                                        }
                                                        else if($session_radius == 50){
                                                            $five = 'selected';
                                                        }
                                                        else if($session_radius == 9999999999){
                                                            $any = 'selected';
                                                        }
                                                    }
                                                    else{
                                                        $any = 'selected';
													}

                                                    ?>


													<option value="9999999999" <?php echo $any ?> >Any distance</option>
													<option value="10" <?php echo $one ?> >10 miles</option>
													<option value="20" <?php echo $two ?> >20 miles</option>
													<option value="30" <?php echo $three ?> >30 miles</option>
													<option value="40" <?php echo $four ?> >40 miles</option>
													<option value="50" <?php echo $five ?> >50 miles</option>

												</select>
												of

									@if(Session::has('geo_location_session'))
									<a data-toggle="collapse" href="#location-menu" style="font-size: 16px; color:#333;">
									{{Session::get('geo_location_session')['city']}} , {{Session::get('geo_location_session')['state']}}
									</a>
									@else
									<a data-toggle="collapse" href="#location-menu" style="font-size: 16px; color:#cc0000; text-decoration:underline;">
									Set Location  
									</a>  
									@endif
											</div>


										</div>

									</div>


								</div>
								<div class="form-row">
									<div class="col-sm-3 mb-3">
										<button type="button" class="btn btn-primary btn-large next"  disabled> Next Step </button>
									</div>



								</div>


								<!-- NEXT SERVICES -->

								<div class="row services">
									<div class="col-md-12 col-sm-12">
										<h3>Select the problem in the following List: </h3>
									</div>

									<div class="col-md-6 col-sm-6 mb-3">
										<br/>
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" class="tab-width" data-target="#tab1" aria-expanded="true" aria-controls="tab1">
												<img src="/images/icons/own-problem.png" width="24px"> Describe problem in your own words
												<i class="fa fa-check dp pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab1" class="collapse in">
													<div class="well"><textarea  name="describe-problem" id="describe-problem" class="form-field form-control" >@if(!empty(Session::get('services')['Describe Problem']) > 0){{Session::get('services')['Describe Problem']}}@endif</textarea></div>
												</div>

											</div>

										</div>


										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab2" class="tab-width" aria-expanded="true" aria-controls="tab2">
												 <img src="/images/icons/general-maintenace.png" width="24px"> General Maintenance
												<i class="fa fa-check gm pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab2" aria-expanded="true" class="collapse in">
													<div class="well"><textarea class="form-field form-control" name="general-maintenance" id="general-maintenance">@if(!empty(Session::get('services')['General Maintenance']) > 0){{Session::get('services')['General Maintenance']}}@endif</textarea></div>
												</div>

											</div>
										</div>



										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab3" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												 <img src="/images/icons/schedule.png" width="28px">Scheduled Maintenance
													<i class="fa fa-check sm pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab3" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="scheduled-maintenance"  id="scheduled-maintenance">@if(!empty(Session::get('services')['Scheduled Maintenance']) > 0){{Session::get('services')['Scheduled Maintenance']}}@endif</textarea></div>
												</div>

											</div>
										</div>


										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab4" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/inspection.png" width="28px"> Inspection and/or Diagnostic
												<i class="fa fa-check id pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab4" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="inspection-diagnostic"  id="inspection-diagnostic">@if(!empty(Session::get('services')['Inspection Diagnostic']) > 0){{Session::get('services')['Inspection Diagnostic']}}@endif</textarea></div>
												</div>
											</div>
										</div>


										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab5" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/not-start.png" width="28px">	 Vehicle doesn’t start
												<i class="fa fa-check vstart pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab5" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="vehicle-start"  id="vehicle-start">@if(!empty(Session::get('services')['Vehicle Start']) > 0){{Session::get('services')['Vehicle Start']}}@endif</textarea></div>
												</div>

											</div>
										</div>


										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab6" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
											<img src="/images/icons/noises.png" width="28px"> Vibrations and/or Noises
												<i class="fa fa-check vnoises pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab6" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="vibrations-noises"  id="vibrations-noises">@if(!empty(Session::get('services')['Vibrations Noises']) > 0){{Session::get('services')['Vibrations Noises']}}@endif</textarea></div>
												</div>

											</div>
										</div>



										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab7" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/visual-problem.png" width="24px">	 Visual problem (Unusual smoke, Fluid leak etc.)
												<i class="fa fa-check vp pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab7" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="visual-problem"  id="visual-problem">@if(!empty(Session::get('services')['Visual Problem']) > 0){{Session::get('services')['Visual Problem']}}@endif</textarea></div>
												</div>


											</div>
										</div>

										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab8" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/odor.png" width="24px">	 Odor / Smell
												<i class="fa fa-check os pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab8" aria-expanded="true" class="collapse in">
													<div class="well">
														<textarea class="form-field form-control" name="odor-smell"  id="odor-smell">@if(!empty(Session::get('services')['Odor/Smell']) > 0){{Session::get('services')['Odor/Smell']}}@endif</textarea>
													</div>
												</div>

											</div>
										</div>


										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab9" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												 <img src="/images/icons/dashboard.png" width="28px"> 	Dashboard service light <small>(Diagnostic Code Error, Check Engine Light etc)</small>
												<i class="fa fa-check ds pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab9" aria-expanded="true" class="collapse in">
													<div class="well">
														<textarea   class="form-field form-control" name="dashboard-service" id="dashboard-service">@if(!empty(Session::get('services')['Dashboard Service']) > 0){{Session::get('services')['Dashboard Service']}}@endif</textarea>
													</div>
												</div>


											</div>
										</div>


									</div>


									<div class="col-md-6 col-sm-6 mb-3">

										<br/>
										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab10" class="tab-width" aria-expanded="false" aria-controls="collapseOne">
												 <img src="/images/icons/ac.png" width="28px">   Air-conditioning and Heating
												<i class="fa fa-check ach pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab10" aria-expanded="false" class="collapse">
													<div class="well"><textarea   class="form-field form-control"  name="ach" id="ach">@if(!empty(Session::get('services')['Air Conditioning and Heating']) > 0){{Session::get('services')['Air Conditioning and Heating']}}@endif</textarea></div>
												</div>

											</div>
										</div>


										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab11" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/brakes.png" width="28px"> Brakes
												<i class="fa fa-check brakes pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab11" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="brakes" id="brakes">@if(!empty(Session::get('services')['Brakes']) > 0){{Session::get('services')['Brakes']}}@endif</textarea></div>
												</div>

											</div>
										</div>



										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab12" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/electric.png" width="28px">	 Electrical
												<i class="fa fa-check elec pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab12" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="electrical"  id="electrical">@if(!empty(Session::get('services')['Electrical']) > 0){{Session::get('services')['Electrical']}}@endif</textarea></div>
												</div>

											</div>
										</div>
   

										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab13" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/exaust.png" width="28px"> Exhaust
												<i class="fa fa-check exhaust pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab13" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control"  name="exhaust" id="exhaust">@if(!empty(Session::get('services')['Exhaust']) > 0){{Session::get('services')['Exhaust']}}@endif</textarea></div>
												</div>

											</div>
										</div>


										 <div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab14" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/fluids.png" width="28px">Fluids
												<i class="fa fa-check fluids pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab14" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control"  name="fluids" id="fluids">@if(!empty(Session::get('services')['Fluids']) > 0){{Session::get('services')['Fluids']}}@endif</textarea></div>
												</div>

											</div>
										  </div>


										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab15" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/suspension.png" width="28px">	 Steering & Suspension
													<i class="fa fa-check ss pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab15" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control"  name="steering-suspension" id="steering-suspension">@if(!empty(Session::get('services')['Steering Suspension']) > 0){{Session::get('services')['Steering Suspension']}}@endif</textarea></div>
												</div>

											</div>
										</div>



										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab16" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/wheel-alignment.png" width="28px">	 Wheel Alignment
												<i class="fa fa-check wa pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab16" aria-expanded="true" class="collapse in">
													<div class="well"><textarea   class="form-field form-control" name="wheel-alignment"  id="wheel-alignment">@if(!empty(Session::get('services')['Wheel Alignment']) > 0){{Session::get('services')['Wheel Alignment']}}@endif</textarea></div>
												</div>

											</div>
										</div>



										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab17" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
													<img src="/images/icons/tyre.png" width="18px">	 Tires Replacement
													<i class="fa fa-check tire-replace pull-right" aria-hidden="true"></i>
												</label>

											
												<div id="tab17" aria-expanded="true" class="collapse in">
													
													<div class="well multi-select">
													<span><input type="checkbox" id="tire-replace" name="tire-replace[]" value="Front Driver Side"{{!empty(Session::get('services')['Tire Replace']) && Session::get('services')['Tire Replace'] == 'Front Driver Side' ? 'checked' : '' }}>Front Drive Side</span>
													<span><input type="checkbox" id="tire-replace" name="tire-replace[]" value="Rear Driver Side"{{!empty(Session::get('services')['Tire Replace']) && Session::get('services')['Tire Replace'] == 'Rear Driver Side' ? 'checked' : '' }}>Rear Driver Side</span>
													<span><input type="checkbox" id="tire-replace" name="tire-replace[]" value="Front Passenger Side"{{!empty(Session::get('services')['Tire Replace']) && Session::get('services')['Tire Replace'] == 'Front Passenger Side' ? 'checked' : '' }}>Front Passenger Side</span><br>
													<span><input type="checkbox" id="tire-replace" name="tire-replace[]" value="Rear Passenger Side"{{!empty(Session::get('services')['Tire Replace']) && Session::get('services')['Tire Replace'] == 'Rear Passenger Side' ? 'checked' : '' }}>Rear Passenger Side</span>
													</div>
												</div>

											</div>
										</div>

										<div class="form-group">
											<div class="checkbox">
												<label data-toggle="collapse" data-target="#tab18" class="tab-width" aria-expanded="true" aria-controls="collapseTwo">
												<img src="/images/icons/tire-replacement.png" width="28px">	 Tire Repair
												<i class="fa fa-check tire-repair pull-right" aria-hidden="true"></i>
												</label>

												<div id="tab18" aria-expanded="true" class="collapse in">
													<div class="well multi-select">
													<span><input type="checkbox" id="tire-repair" name="tire-repair[]" value="Front Driver Side"{{!empty(Session::get('services')['Tire Repair']) && Session::get('services')['Tire Repair'] == 'Front Driver Side' ? 'checked' : '' }}>Front Drive Side</span>
													<span><input type="checkbox" id="tire-repair" name="tire-repair[]" value="Rear Driver Side"{{!empty(Session::get('services')['Tire Repair']) && Session::get('services')['Tire Repair'] == 'Rear Driver Side' ? 'checked' : '' }}>Rear Driver Side</span>
													<span><input type="checkbox" id="tire-repair" name="tire-repair[]" value="Front Passenger Side"{{!empty(Session::get('services')['Tire Repair']) && Session::get('services')['Tire Repair'] == 'Front Passenger Side' ? 'checked' : '' }}>Front Passenger Side</span><br>
													<span><input type="checkbox" id="tire-repair" name="tire-repair[]" value="Rear Passenger Side"{{!empty(Session::get('services')['Tire Repair']) && Session::get('services')['Tire Repair'] == 'Rear Passenger Side' ? 'checked' : '' }}>Rear Passenger Side</span>
												    </div>

											</div>
										</div>
										<br>
										<button type="submit"class="btn btn-primary pull-right">Search</button> &nbsp; &nbsp;
										<a href="{{url('/clear/services')}}" class="btn btn-secondary pull-right">Clear</a>

									</div>


								</div>


							</div>


							</form>

						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="py-3">
		<div class="container">

			<div class="message" width="50%" align="center">
				@if (session('message'))
					<div class="alert alert-success" width="50%">
						{{ session('message') }}
					</div>
				@endif
			</div>

			<div class="message" width="50%" align="center">
				@if (session('error'))
					<div class="alert alert-danger" width="50%">
						{{ session('error') }}
					</div>
				@endif
			</div>


			<!--  -->

		</div>
	</div>
	@endsection
	@section('searchPageScript')

	<script src="https://code.jquery.com/jquery-1.7.2.min.js"></script>

	<script src="/js/car_services.js"></script>

  @if(!empty(Session::get('services')))	
	<script type="text/javascript">
	
		$("#make").attr("disabled", false);
		$("#model").attr("disabled", false);
		$("#trim").attr("disabled", false);
		$("#mileage").attr("disabled", false);
		$(".services").show();
	
	</script>

	@else
	<script type="text/javascript">
	
		$("#make").attr("disabled", true);
		$("#model").attr("disabled", true);
		$("#trim").attr("disabled", true);
		$("#mileage").attr("disabled", true);
		$(".services").hide();
	
	</script>	
	
	@endif	
	
	
	<script type="text/javascript">

		$(document).on('change','#year',function(){

            var year = $('#year').val();

            $.ajax({
                type:'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    year: year
                },
                url:'{{url('car/services/get_facets')}}',
                success:  function (html) {

                    console.log(html);

                    $("#make").removeAttr("disabled");
                    $("#make").html(html);
                }
            });


		});

		$(document).on('change','#make',function(){
            var year = $('#year').val();
			var make = $('#make').val();

			$.ajax({
				type:'POST',
				data: {
					"_token": "{{ csrf_token() }}",
					year: year,
					make: make
				},
				url:'{{url('car/services/get_facets')}}',
				success:  function (html) {
					$("#model").removeAttr("disabled");
					$("#model").html(html);
				}
			});

		});

		$(document).on('change','#model',function(){
            var year = $('#year').val();
            var make = $('#make').val();
			var model = $('#model').val();

			$.ajax({
				type:'POST',
				data: {
					"_token": "{{ csrf_token() }}",
                    year: year,
                    make: make,
                    model: model
				},
				url:'{{url('car/services/get_facets')}}',
				success:  function (html) {
					$("#trim").removeAttr("disabled");
					$("#trim").html(html);
				}
			});

		});

		$(document).on('change','#trim',function(){

			$("#mileage").removeAttr("disabled");

		});


		$(document).on('change keyup','#mileage',function(){
			$(".next").removeAttr("disabled");
		});


		$(document).on('click','.next',function(){

			$(".services").show();

		});




	</script>



@endsection 