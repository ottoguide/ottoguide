@extends('layouts.web_pages')
@section('content')
    <?php

    $param="";
    foreach ($_GET as $k => $p){
        $param.=$k.'='.$p.'&';
    }

//    use App\Helpers\Common;
//    $dealer_id = $car_detail->dealer->id;
    ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{asset('css/rating.css')}}" rel="stylesheet"> 	
 <link href="/css/reviews.css" rel="stylesheet">
	
	<style type="text/css">

      .carousel-item img{
        height: 487px !important;
        border-radius: 10px;
      }
  
     .cxm-price .amount {
        font-size: 20px;
        font-weight: bold;
     }

    .container{
        max-width:1170px;
        margin:auto;
    }
     .review img{
       max-width:100%;
      }
    .recent_heading h4 {
      color: #05728f;
      font-size: 21px;
      margin: auto;
    }
    .srch_bar input{
        border:1px solid #cdcdcd;
        border-width:0 0 1px 0;
        width:80%;
        padding:2px 0 4px 6px;
        background:none;
    }

    .srch_bar .input-group-addon button {
      background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
      border: medium none;
      padding: 0;
      color: #707070;
      font-size: 18px;
    }
    .rating-title{
		font-size:15px;
		color:#fff;	
		font-weight: bold;
		padding-left:10px;		
	}
	
	.rating-stars ul > li.star > i.fa {
        font-size: 1.3em;
        color: #ccc;
    }
	
	.incoming_msg_img {
	  display: inline-block;
	  width: 6%;
	}
	.incoming_msg{	
	  margin-top:2%;	
	}
 .received_msg {
	  display: inline-block;
	  display: inline-block;
	  padding: 0 0 0 10px;
	  vertical-align: top;
	   margin-left: 1%;
	 }

	 
  .input_msg_write input {
  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
  border: medium none;
  color: #555;
  font-size: 15px;
  padding:5px;
  min-height: 40px;
  width: 75%;
}
	 
   .success-box {
    margin: 0px 0;
    padding: 0px 0px;
    border: 0px solid #eee;
    font-size: 16px;
} 

	input:-webkit-autofill, textarea:-webkit-autofill, select:-webkit-autofill {
    background-color: #fff !important;
    background-image: none !important;
    color: rgb(0, 0, 0) !important;
}
.type_msg {border-bottom: 0px solid #c4c4c4;  5px; padding:5px; position: relative;} 

.title_msg,.write_msg{
   font-size: 16px;	
} 	

.success-box {
    margin: 0px 0;
    padding: 0px 0px; 
    border: 0px solid #eee;
}

.msg_send_btn{
margin-bottom: 10px;
}	
.btn-sm, .btn-group-sm>.btn {
 padding: 0.2rem 0.2rem;

}
.comment p {
width: 65%;
}

.btn-danger{
	margin-left:5px;
}	

.modal-header-primary {
	color:#fff;
    padding:9px 15px;
    border-bottom:1px solid #eee;
    background-color: #428bca;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
	}


}
</style>

		@if(Session::has('rbo_session'))
		{{session()->forget('rbo_session')}}
		@endif

	<?php 
	$title = "DEALER TEST CAR";
	?>

<!--
 DB::table('dealer_email_cron')->where('dealer_id',$car_detail->dealer->id)->where('date','=',date('Y-m-d'))
->where('car_id',$car_detail->id)->update(array('total_in_detail' => DB::raw('total_in_detail + 1')));
-->  <?php 
$data['radius'] = 10 ;
$data['car_id'] = 'mycar';
$data['latitude'] = "lat";
$data['longitude'] = "long";
$filter_search = 'filter';
?>
	

 <div class="header-margin py-4 bg-secondary">
	<div class="container">
	 <div class="message"width="50%" align="center">
         @if (session('message'))
              <div class="alert alert-success" width="50%">
                   {{ session('message') }}
                </div>
         @endif
	 </div>
	 <div class="message"width="50%" align="center">
        @if (session('error'))
           <div class="alert alert-danger" width="50%">
              {{ session('error') }}
           </div>
        @endif
     </div>
     <div class="row">
        <div class="col-sm-12">
          <h1> @if(!empty($car_detail->heading) > 0){{$car_detail->heading}}@endif </h1>
        </div>
      </div>
    </div>      
  </div> 
    
  <div class="py-5">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div id="cxm-pro-slider" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <?php 
              $car_images = $car_detail->media->photo_links;
               if($car_images){
              foreach ($car_images as $i => $car) { ?>
              <div class="carousel-item<?php echo ($i === 0)?' active' :''; ?>">
                <img class="car-images img-fluid" src="{{$car}}" alt="IMG">
              </div>
              <?php } } else { ?>
                <div class="carousel-item active">
                  <img class="car-images img-fluid" src="{{asset('images/no-image.jpg')}}" alt="IMG">
                </div>
              <?php } ?>
            </div>

            <a class="carousel-control-prev" href="#cxm-pro-slider" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
		
            <a class="carousel-control-next" href="#cxm-pro-slider" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
            
          </div>
        </div>
        <div class="col-md-4">
          <div class=" bg-secondary p-3">
            <div class="cxm-rating">
              Rating: 
			 <?php 
				$avg_count = DB::table('client_reviews')->where('car_id',$car_detail->id)->avg('rating');
				
				
				if(empty($avg_count)){
				$avg_count = 0;	
				}
				$rt=round($avg_count);
				$img="";
				$i=1;
				while($i<=$rt){
				$img=$img."<img src=/images/star.png>";
				$i=$i+1;
				}
				while($i<=5){
				$img=$img."<img src=/images/star2.png>";
				$i=$i+1;
				}
				echo $img;
			?>
			
			
            </div>
            <div class="cxm-distance font-weight-bold"><span class="fa fa-road"></span> {{isset($car_detail->miles)? $car_detail->miles :'--' }} miles From You</div>
            <div class="cxm-contacts">
			@auth
                <a href="javascript:services_email('<?=$car_detail->dealer->id?>','<?=$car_detail->dealer->dealer_name ?>');" class="btn btn-primary btn-lg btn-block">
                    <span class="fa fa-comment"></span> Anonymous Contact
                    <?php //preg_match_all( "/[0-9]/", (isset($dealer_detail->seller_phone) ? $dealer_detail->seller_phone : '--')) ?>
                </a>
				
			@endauth

			@guest
			
			<a href="{{route('search_buttons_session',['session_car_id'=> $car_detail->id,'session_car_vin'=> $car_detail->vin,'session_car_title'=> $car_detail->heading, 'session_status'=> 'Anonymous_Contact','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}" class="btn btn-primary btn-lg btn-block">
                    <span class="fa fa-comment"></span> Anonymous Contact
                    <?php //preg_match_all( "/[0-9]/", (isset($dealer_detail->seller_phone) ? $dealer_detail->seller_phone : '--')) ?>
                </a>
			@endguest	
			
			
				
				
				<!-- best -->
			   @auth
			   
			   <?php			
			   $check_user_rbo_status = DB::table('request_best_offer')->where('user_id',Auth::user()->id)->where('car_id','=',$car_detail->id)->where('is_rbo_active',1)->get();
			   ?>
			 
				@if(count($check_user_rbo_status))
				<button class="btn btn-success btn-lg btn-block" disabled>
                <span class="fa fa-lock"></span> Car is already Requested Best Offer
                </button>
				
				<a href="{{url('/search-car.html?filter_search=filter&vin='.$car_detail->vin)}}&rbo=yes&view=1" class="btn btn-primary btn-lg btn-block">
                <span class="fa fa-car"></span> View similar car for more (RBO) </a>
					
			    @else	
			    <a href="{{route('RequestBestOffer',['car_id'=> $car_detail->id, 'dealer_id'=> $car_detail->dealer->id , 'radius'=>$_GET['radius'], 'latitude'=> $_GET['latitude'],'longitude'=>$_GET['longitude']])}}" class="btn btn-success btn-lg btn-block">
                 <span class="fa fa-money"></span> Request Best Offer</a> 
			    @endif
			
				@endauth
				
				@guest
				
				<a href="{{route('rbo_session',['session_car_id'=> $car_detail->id,'session_car_vin'=> $car_detail->vin,'dealer_id'=> $car->dealer->id,'session_car_title'=> $car->heading, 'session_status'=> 'Request_Best_Offer','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}" class="btn btn-success btn-lg btn-block">
                <span class="fa fa-money"></span> Request Best Offer
                </a>
				@endguest
				
				
				@auth
                <a href="javascript:services_email('<?=$car_detail->dealer->id?>','<?=$car_detail->dealer->dealer_name ?>');" class="btn btn-primary btn-lg btn-block">
                    <span class="fa fa-comment"></span> Just A Question
                    <?php //preg_match_all( "/[0-9]/", (isset($dealer_detail->seller_phone) ? $dealer_detail->seller_phone : '--')) ?>
                </a>
				
			  @endauth
			  
			  
			@guest
			
			<a href="{{route('search_buttons_session',['session_car_id'=> $car_detail->id,'session_car_vin'=> $car_detail->vin,'session_car_title'=> $car_detail->heading, 'session_status'=> 'just_question','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}" class="btn btn-primary btn-lg btn-block">
                    <span class="fa fa-comment"></span> Just A Question
                    <?php //preg_match_all( "/[0-9]/", (isset($dealer_detail->seller_phone) ? $dealer_detail->seller_phone : '--')) ?>
                </a>
			@endguest	
			          
	
			@auth 
		    <?php 
			 $check_saved_car = DB::table('client_favourite')
			->where('car_id',$car_detail->id)
			->where('client_id',Auth::user()->id)->first(); 
			?>
		   @if(count($check_saved_car))
		   <button class="btn btn-primary btn-block"><span id="save_car_span_1" class="fa fa-check"></span> Saved Car </button>
		   @else	
           <button type="submit" form="save_car" class="btn btn-primary btn-block"><span id="save_car_span_1" class="fa fa-save"></span> Save Car </button>
		   @endif
		   @endauth
		   
		    @guest
			<a href="{{route('search_buttons_session',['session_car_id'=> $car_detail->id,'session_car_vin'=> $car_detail->vin,'session_car_title'=> $car_detail->heading, 'session_status'=> 'save_car','session_price'=> $_GET['price'],'session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}" class="btn btn-primary btn-lg btn-block">
            <span class="fa fa-save"></span> Save Car
            </a>
			@endguest
				
				
            </div>
			
			
			<div class="cxm-price fc1">
			<span class="amount">
    		
			      @if(!empty($car_detail->price) > 0)
				  
			      @if(Auth::user()->id)	
                   $ {{number_format((float)$car_detail->price)}}
				  @else
					<center>  
					<a href="{{route('vdp_session',['session_car_id'=> $car_detail->id, 'session_status'=> 'VDP','session_radius'=> $_GET['radius'], 'session_latitude'=> $_GET['latitude'],'session_longitude'=> $_GET['longitude']])}}">
					<div class="btn-login-for-price">
					<div class="lbl-login-for-price" style="font-size:16px; padding-top:6px; padding-left:20px;">Login for Price<div class="lbl-free">It's FREE!</div></div>
					</div>
					</a>
					</center>	

				  @endif
				  @else
				  Price Not Available
                  @endif
			
			
                           	

    			</span>
			</div>    
			<!--
            <div class="form-row">
              <div class="col-sm-6 col-md-12 col-lg-6 mb-2 mb-sm-0 mb-md-2 mb-lg-0">
                <a class="btn btn-primary btn-block" href="{{route('offer', ['blank' => 'value', 'car_id' => $car_detail->id, 'offer'=> 'lease', 'dealer_id'=> $car_detail->dealer->id])}}">Lease Offer</a>
              </div>
              <div class="col-sm-6 col-md-12 col-lg-6">
                <a class="btn btn-primary btn-block" href="{{route('offer', ['blank' => 'value', 'car_id' => $car_detail->id, 'offer'=> 'purchase', 'dealer_id'=> $car_detail->dealer->id])}}">Purchase Offer</a>
              </div>
            </div> -->           
          </div>
        </div>
		
	<!-- Car Review -->	
		
		 <div class="col-md-8">
	
		 <div class="p-3">
		
		
		@if(count(Auth::user()))
		
		<?php
		$check_block_status = DB::table('settings')->where('car_id',$car_detail->id)->where('reviews_off',1)->get()->first();
		?>
		
		@if(!count($check_block_status))
	
		<?php
		$rating_status = DB::table('client_reviews')->where('client_id',Auth::user()->id)->where('car_id','=',$car_detail->id)->where('reply_to','=',null)->get()->first();
		?>	
	
	   @if(count($rating_status))
		
		
		@else

	   <div class="incoming_msg">
              <div class="incoming_msg_img"><img src="{{Auth::user()->image}}" alt="user_image" width="50"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
				  <b>{{Auth::user()->name}}</b><br>
				  <h6>Your review will be posted on the web </h6>
                </div>
				
				<div class='rating-stars '>
					  <ul id='stars'>
					  <li class='star' title='Poor' data-value='1'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='Fair' data-value='2'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='Good' data-value='3'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='Excellent' data-value='4'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					  <li class='star' title='WOW!!!' data-value='5'>
						<i class='fa fa-star fa-fw'></i>
					  </li>
					</ul>
			</div>
			
			
            </div>
		</div>			
			
	

		 <div class="mesgs">
          <div class="msg_history">

		 {!! Form::open(array('url' => '/save/reviews', 'enctype' => 'multipart/form-data')) !!}
		 {!! Form::hidden('_token', csrf_token()) !!}
		
		  <div class="type_msg">
		  
		  	<div class="input_msg_write">
            <input type="text" class="title_msg"  name="title" style="border-bottom:1px solid #e2e2e2; border-radius: 5px;"placeholder="Write the title of the message" required />
            </div>
			
            <div class="input_msg_write">
			<input type="text" class="write_msg" name="message" style="border-bottom:1px solid #e2e2e2; border-radius: 5px;" placeholder="Share detail of your own experince on this car"  required>
              <button type ="submit" class="msg_send_btn btn-small btn-primary" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            </div>
			<input type="hidden" name="dealer_id" value="@if(!empty($car_detail->dealer->id) > 0){{$car_detail->dealer->id}}@endif">
			<input type="hidden" name="car_id" value="@if(!empty($car_detail->id) > 0){{$car_detail->id}}@endif">
			<input type="hidden" class="form-control" id="rating" name="rating">
		  </div>
				
			@endif
		@endif


		  @if ($errors->any())
                        
			@foreach ($errors->all() as $error)
           
				{{ $error }}
            
				@endforeach
                       
            @endif
		
		</form>	
		@endif
		
		
	
	
           
           <br>
           
          </div>
	
        </div>
			</div>
		 
	
		 </div>
		
      </div>
    </div>      
  </div>
  
  <div class="py-5 bg-secondary">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="cxm-tabs">
            <nav>
              <ul class="nav nav-tabs d-md-none" id="nav-tab" role="tablist">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Option Menu</a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item active"  data-toggle="tab" href="#overview" role="tab">Vehicle Overview</a>
                    <a class="dropdown-item" data-toggle="tab" href="#dealer" role="tab">About The Dealer</a>
                    <a class="dropdown-item" data-toggle="tab" href="#rating" role="tab">Model Rating</a>                    
                  </div>
                </li>
				      </ul>
              <div class="d-none d-md-block">
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" data-toggle="tab" href="#overview" role="tab">Vehicle Overview</a>
                  <a class="nav-item nav-link" data-toggle="tab" href="#about-model" role="tab">About This Model</a>
                  <a class="nav-item nav-link" data-toggle="tab" href="#colors" role="tab">Colors & Stock</a>
                  <a class="nav-item nav-link" data-toggle="tab" href="#reviews" role="tab">User Reviews</a>
                  <!--<a class="nav-item nav-link" data-toggle="tab" href="#recalls" role="tab">Model Recalls</a>-->
                </div>
              </div>
            </nav>
			
			
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade show active" id="overview" role="tabpanel">
                <h3>Key Facts</h3>
                <ul class="cxm-facts">
                  <li><span class="fa fa-modx text-primary"></span>@if(!empty($car_detail->build->year)>0){{$car_detail->build->year}}@endif</li>
                  <li><span class="fa fa-barcode text-primary"></span> SUV</li>
                  <li><span class="fa fa-road text-primary"></span> {{isset($car_detail->miles)? $car_detail->miles :'--' }} miles</li>
                  <li><span class="fa fa-toggle-on text-primary"></span>{{isset($car_detail->build->tank_size)?$car_detail->build->tank_size:''}}
                  <li><span class="fa fa-spinner text-primary"></span> </li>
                  <li><span class="fa fa-fire text-primary"></span> Diesel</li>
                </ul>                
                <p>{{isset($car_detail->extra->seller_comments)?$car_detail->extra->seller_comments:''}}</p>
              </div>
              <div class="tab-pane fade" id="model" role="tabpanel">                
                <table class="table">
                  
                </table>
              </div>
			  
			  
			<div class="tab-pane fade" id="about-model" role="tabpanel">
	
			<?php
			echo "<table width='75%'><tr>";
			$i = 0;
			foreach($car_detail->build as $desc => $values){
				echo "<td>".str_replace("_","  ","<b>".ucwords($desc)." :</b>")."</td>";
				echo "<td>". $values."</td>";
				$i++;
				if($i%3==0){
					echo "</tr><tr>";
				}
			}	echo "</tr></table>"; ?>
			 
			  </div>
			  
              <div class="tab-pane fade" id="colors" role="tabpanel">
        			  <table width="70%" border="0">
         			  <tr>
        			  <td><b>Exterior Color:</b></td>
        			  <td>{{isset($car_detail->exterior_color)?$car_detail->exterior_color:''}}</td>
        			  <td><b>Interior Color:</b></td>
        			  <td>{{isset($car_detail->interior_color)?$car_detail->interior_color:''}}
        			  </td>
        			  </tr>
        			  <tr>
        			  <td><b>Inventory Type</b></td>
        			  <td>{{isset($car_detail->inventory_type)?$car_detail->inventory_type:''}} </td>
        			  <td><b>Stock</b></td>
        			  <td><b>{{isset($car_detail->stock_no)?$car_detail->stock_no:''}}</b></td>
        			  <td> </td>
        			  </tr>
        			  </table>			  
			     </div>  




		<div class="tab-pane fade" id="reviews" role="tabpanel">
	
		<!-- Get main reviews -->	
		
		<?php 
		
		 $messages = DB::table('client_reviews')
		->where('car_id',$car_detail->id)
		->where('reply_to','=',null)
		->orderby('client_reviews.id','desc')
		
		->join('users','client_reviews.client_id','=','users.id')
		
		->select('client_reviews.id as msg_id', 'users.id as user_id','car_id','client_id','dealer_id',
		 'image','name','title','message','message_by','reply_to','rating','client_reviews.created_at as msg_date',
		 'users.created_at as user_created_at')	
		 
		 ->get();		  
		
		?>
			
			
		<ul class="comment-section">

		@if(count($messages))		 
			  
		@foreach($messages as $get_message) 
	
			<?php 
		
			$messages_replies = DB::table('client_reviews')
			->where('car_id',$car_detail->id)
			->where('reply_to','=',$get_message->msg_id)
			->orderby('client_reviews.id','ASC')
			
			->join('users','client_reviews.client_id','=','users.id')
			 ->select('client_reviews.id as msg_id', 'users.id as user_id','car_id','client_id',
			 'image','name','message','message_by','reply_to','title','rating','client_reviews.created_at as msg_date',
			 'users.created_at as user_created_at','dealer_id')	
			 ->get();		  
		 
			?>
		
			<li class="comment {{$get_message->message_by}}">

                <div class="info">
                    <a href="#">{{$get_message->name}}</a>
                    <span><?= date("d-m-Y", strtotime($get_message->msg_date))."\n"; ?></span>
                </div>

                <a class="avatar" href="#">
                    <img src="{{$get_message->image}}" width="35" alt="Profile Pic" title="" />
                </a>

                <p>
			
			  <span class="pull-right">
				
				Rating: 
			
			<?php 
			 
				$avg_count = DB::table('client_reviews')->where('car_id',$car_detail->id)->avg('rating');
				
				if(empty($avg_count)){
				$avg_count = 0;	
				}
				$rt=round($avg_count);
				$img="";
				$i=1;
				while($i<=$rt){
				$img=$img."<img src=/images/star.png>";
				$i=$i+1;
				}
				while($i<=5){
				$img=$img."<img src=/images/star2.png>";
				$i=$i+1;
				}
				echo $img;
			?>
				
			</span>	
				<br>
				<b style="color:#444;">{{(isset($get_message->title) ? $get_message->title : '' )}}.</b> <br>
				{{(isset($get_message->message) ? $get_message->message : '' )}}
				<br>
				<br>
				@auth
				
				<?php 
				if($get_message->client_id == Auth::user()->id){?>
				
				<span>
				<a href="javascript:car_rating_update('{{(isset($get_message->car_id) ? $get_message->car_id : '' )}}', '{{(isset($get_message->dealer_id) ? $get_message->dealer_id : '' )}}','{{(isset($get_message->msg_id) ? $get_message->msg_id : '' )}}', '{{(isset($get_message->title) ? $get_message->title : '' )}}', '{{(isset($get_message->message) ? $get_message->message : '' )}}', '{{(isset($get_message->rating) ? $get_message->rating : '' )}}');">
				&nbsp; &nbsp; &nbsp;
				<button type="button" class="btn btn-info btn-sm pull-right"> <i class="fa fa-pencil fa-1"></i> Edit review</button>
				</a>
				</span>
			  <?php } ?>
			   @endauth
				
				&nbsp; &nbsp; &nbsp;
						
				<span>
				<a data-toggle="collapse" class="view_reply pull-right" href="#message_reply_{{$get_message->msg_id}}" aria-expanded="false" aria-controls="collapseExample">
				&nbsp; &nbsp; &nbsp;
				<button type="button" class="btn btn-primary btn-sm pull-right"> <i class="fa fa-arrow-down fa-1"></i> View <b>{{$messages_replies->count('id')}}</b> replies </button>
				</a>
				</span>
				
				
				</p>
				
			</li>
		
		
		 <div class="collapse" id="message_reply_{{$get_message->msg_id}}">

			@if(count($messages_replies))		 
			 
			@foreach($messages_replies as $messages_reply) 

		 
		   <?php 
			
			if($messages_reply->message_by =="client"){
				
			$name = $messages_reply->name;	
			
			}
			
			else{
			
			$dealer_name = DB::table('dealer')->where('dealer_id',$messages_reply->dealer_id)
			->select('dealer_name')->get()->first();		
			
			$name = $dealer_name->dealer_name; 
			
			}
		  
		  ?>		
			
		 
			<li class="comment {{$messages_reply->message_by}}">

                <div class="info">
                    <a href="#">{{$name}}</a>
                    <span><?= date("d-m-Y", strtotime($messages_reply->msg_date))."\n"; ?></span>
                </div>

               <a class="avatar" href="#">
                    <img src="{{$messages_reply->image}}" width="35" alt="Profile Pic" title="" />
               </a>

              <p>{{$messages_reply->message}}<br></p>
				
				
			</li>
	
		  @endforeach
		   <?php 
			if($get_message->client_id == Auth::user()->id){?>
		
			<li>
				
			<a href="javascript:message_post_reply('<?=$get_message->msg_id ?>','<?= $get_message->car_id?>', '<?= $get_message->dealer_id ?>');">
				<center><button type="button" class="btn btn-success pull-right">Reply</button></center>
				</a>
			</li>
			<?php } ?>
			
			<br>			
		  @endif	
		
		</div>
	
		
		@endforeach
						
		@endif			
				
		
			
		</ul>
			 
			  </div>	
			  
				 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  


  
    <div class="modal fade" id="message_reply_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Reply to message</h5>
                </div>
                <div class="modal-body">
				<p class="success_message" style="color:green; font-weight:bold;"></p>
				<textarea id="message_reply" name="message_reply" class="form-control"placeholder="write your comment here"></textarea>		
                </div>
               
			   <div class="modal-footer">
			   <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>               
			   <button type="button" class="btn btn-primary reply_message">Reply</button>
                </div>
            </div>
        </div>
    </div>

  
   @if(count(Auth::user()))
	   
  <!-- Rating modal update -->
 
    <div class="modal" id="primary_udate">
	
    <div class="modal-dialog">
      <div class="modal-content">
     
        <!-- Modal Header -->
        <div class="modal-header modal-header-primary">
          <h4 class="modal-title rating-title">Rate and review (Update) <br>
		  
		  <small>2018 Toyota Corolla XLi Automatic</small>
		
		  </h4> <br>
		  
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
		
	
	
        <div class="modal-body">
	
		 <div class="incoming_msg">
              <div class="incoming_msg_img"><img src="{{isset(Auth::user()->image)?Auth::user()->image:''}}" alt="user_image" width="40px"> </div>
              <div class="received_msg">
                <div class="received_withd_msg">
				  <b>{{isset(Auth::user()->name)?Auth::user()->name:''}}</b><br>
					<h6>Your review will be posted on the web </h6>
              </div>
            </div>
		</div>
	   
	   <br>
	   
			<div class="row abb4">
				<div class="col-md-5 white">
					<div class='rating-stars'>
					  <ul id='stars'>
						
						<?php 
						
						$rating_status = DB::table('client_reviews')->where('car_id',$car_detail->id)->where('client_id',Auth::user()->id)->where('reply_to','=',null)->get()->first();
						
						$avg_count = (isset($rating_status->rating) ? $rating_status->rating : '' );
							
						if(empty($avg_count)){
						$avg_count = 0;	
						}
						$rt=round($avg_count);
					
						$img="";
						$i=1;
						$count=1;
						while($i<=$rt){
						$img=$img."<li class='star selected' data-value='".$count++."'>
								<i class='fa fa-star fa-fw'></i>
							  </li>";
						$i+=1;
						}
						while($i<=5){	
						$img=$img."<li class='star' data-value='".$count++."'>
								<i class='fa fa-star fa-fw'></i>
							  </li>";
						$i=$i+1;
						}
						
						echo $img;
						
						?>
					</ul>
				</div>
			
			</div>
			
				<div class="col-md-7 white">
				<div class='success-box'>
				<div class='clearfix'></div>
				<div class="text-message2"></div>
				<div class='text-message'style='font-weight:bold;'></div>
				<div class="text-message3"></div>
				<div class='clearfix'></div>
				
				</div>
				</div>
			</div>
		

		
		   <div class="type_msg">
		 
			<div class="input_msg_write">
            <input type="text" class="title_msg2"  name="title" style="border-bottom:1px solid #e2e2e2; border-radius: 5px;"placeholder="Write the title of the message" required />
            </div>
			<br>
			<div class="input_msg_write">
            <input type="text" class="write_msg2"  name="message" style="border-bottom:1px solid #e2e2e2; border-radius: 5px;" placeholder="Share detail of your own experince on this car" autofocus required />
            </div>
			
			<input type="hidden" name="dealer_id" value="">
			<input type="hidden" name="car_id" value="">
		
		   </div>
		
	
		   </div>
        
		
			<!-- Modal footer -->
			<div class="modal-footer">
			  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
			   <button type="button" class="btn btn-danger delete_message" >Delete</button>
			  <button type="button" class="btn btn-primary update_message">Update</button>
			</div>
			
			<input type="hidden" class="form-control" id="rating" name="rating">

		
			</div>
			
			</div>
		
		  </div>
	  @endif
<?php $get_dealer_email = DB::table('dealer')->where('dealer_id',$car_detail->dealer->id)->get()->first(); ?>
<form id="services_email" method="post" action="{{url('user-services-email.html')}}?email_subject=Car Deal">
    {!! Form::hidden('_token', csrf_token()) !!}
    <input type="hidden" name="dealer_id" class="dealer_id" value="{{$get_dealer_email->dealer_id}}">
    <input type="hidden" name="dealer_name" class="dealer_name" value="{{$get_dealer_email->dealer_name}}">
	<input type="hidden" name="car_heading" class="car_heading" value="{{$car_detail->heading}}">
</form>

<!-- Modal -->
<div class="modal fade" id="dealer-contact-modal" data-backdrop="static"  data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="contact-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contact-modal">Dealer Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <center>
                    @auth
                        <a href="javascript:void(0);" style="width: 50%;" class="contact-btn btn btn-block btn-primary" onclick="$('#num-div').show() , $('.contact-btn').hide();">
                            <i class="fa fa-phone "></i> Call to Dealer</a><!--$car_detail->dealer->id-->
                        <a href="{{route('mychat', ['car_id' => $car_detail->id, 'dealer_id'=> $car_detail->dealer->id,  'channel_name'=> $get_dealer_email->dealer_email])}}-<?= Auth::user()->email ?>"><button style="width: 50%;" class="contact-btn btn btn-block btn-primary"><i class="fa fa-comments"></i> Text with Dealer</button></a>
                        <button type="submit" style="width: 50%;" form="services_email" class="contact-btn btn btn-block btn-primary"><i class="fa fa-envelope"></i> Email to dealer</button>
                        <div id="num-div" style="width: 50%;display: none;border: 1px dashed #ddd; margin: 10px;padding: 10px;">
                             <h5>Enter the number to call</h5>
                             <input type="text" id="user-phone" class="form-control" value="{{Auth::user()->phone}}" /><!--misbah as user 16502629320--><br>
                             <input type="hidden" id="dealer-phone" class="form-control" value="{{$get_dealer_email->dealer_phone}}" />
							 <input type="hidden" id="dealer-id" class="form-control" value="{{$get_dealer_email->dealer_id}}" />
							  <input type="hidden"id="dealer-other-phone" class="form-control" value="{{$get_dealer_email->dealer_other_phone}}" />
							 <br>
                            <a href="javascript:void(0);" style="width: 50%;" class="call-button btn btn-block btn-primary">
                             <i class="fa fa-arrow-right "></i> Go</a>
                        </div>
                        <div id="div1" style="display: none">
                            <span>This dealer is not in contact please see the similar cars</span>
                                <br>
                                <br>
                                    <?php
                                $param="";
                                foreach ($_GET as $k => $p){
                                    $param.=$k.'='.$p.'&';
                                }
                            ?>
                            <a href="{{url('/search-car.html?'.$param.'vin='.$car_detail->vin)}}" style="width: 50%;" class="btn btn-block btn-primary">
                                <i class="fa fa-arrow-right "></i> Show Similar Cars</a>
                        </div>
                    @endauth

                    @guest
					
					  <div id="div1" style="display: none">
                            <span>This dealer is not in contact please see the similar cars</span>
                                <br>
                                <br>
                                  <?php
                                $param="";
                                foreach ($_GET as $k => $p){
                                    $param.=$k.'='.$p.'&';
                                }
                            ?>
                            <a href="{{url('/search-car.html?'.$param.'vin='.$car_detail->vin)}}" style="width: 50%;" class="btn btn-block btn-primary">
                                <i class="fa fa-arrow-right "></i> Show Similar Cars</a>
								
                        </div>
					
                        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary contact-btn"><i class="fa fa-phone"></i> Call Dealer</a>
                        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary contact-btn"><i class="fa fa-comments"></i> Chat Dealer</a>
                        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary contact-btn"><i class="fa fa-envelope"></i> Email</a>
                    @endguest
                </center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="user-contact" data-backdrop="static"  data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="contact-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contact-modal">Dealer Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <center>
                    @auth
                        <input type="text" class="form-control" value="{{Auth::user()->mobile}}" /><br>
                        <input type="hidden" id="dealer-phone" class="form-control" value="{{$get_dealer_email->dealer_phone}}" /><br>
                        <a href="javascript:void(0);" style="width: 50%;" class="call-button btn btn-block btn-primary">
                            <i class="fa fa-arrow-right "></i> Go</a>
                        <div id="div1"></div>
                    @endauth
                </center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


	<!-- SAVE CAR FROM DETAIL PAGE -->

			<form  id="save_car" action="{{url('save/car')}}" method="post">	
				{!! Form::hidden('_token', csrf_token()) !!}
				<?php
				$save_car= array(
				'id' => (isset($car_detail->id) ? $car_detail->id : ''),
				'title' => (isset($car_detail->heading) ? $car_detail->heading : '--'),
				'car_type' => (isset($car_detail->inventory_type) ? $car_detail->inventory_type : '--'),
				'price' => (isset($car_detail->price) ? $car_detail->price : '--'),
				'miles' => (isset($car_detail->miles) ? $car_detail->miles : '--'),
				'build' => (isset($car_detail->build)) ? $car_detail->build: '',
				'images' => $car_detail->media->photo_links
				);
				?>
				<input type="hidden" name="car_post_id" value="{{$car_detail->id}}">
				<input type="hidden" name="car_post_data" value="{{base64_encode(json_encode($save_car))}}">
				</form>	



<script>
    function services_email(dealerid){
        $.ajax({
            type:'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                dealer_id:dealerid
            },
            url:'{{url('check/dealer/register')}}',
            success: function (res) {
                if(res == 1){
                    $("#div1").hide();
                    $(".contact-btn").show();
                }

                else{
                    $(".contact-btn").hide();
                    $("#div1").show();
                }
            }
        });
        $('#num-div').css("display", "none");
        $('#dealer-contact-modal').modal('show');
    }

    $(document).ready(function(){


		function services_email(dealerid,dealername){

		$('#dealer-contact-modal').modal('show');

		$(".dealer_id").val(dealerid);

		$(".dealer_name").val(dealername);

   }



    var callStatus = '';
    var callSid = '';
    $(".call-button").click(function(){
        $(this).html('Connecting...');
        $(".call-button").attr('disabled',true);
        var user_phone = $('#user-phone').val();
        var dealer_phone = $('#dealer-phone').val();
		var dealer_id = $('#dealer-id').val();
		var other_phone = $('#dealer-other-phone').val();
		
		
        $.ajax({
            url: "{{url('twilio-call-chat/call.php')}}",
            type: "POST",
            data: {id: 1, user_phone:user_phone,dealer_phone:dealer_phone,dealer_id:dealer_id,other_phone:other_phone},
            dataType: 'json',
            error: function(xhr, status, error) {
                console.log(xhr.status + status+error);
            },
            success: function(result){
//                <?php //Common::addon_inc($dealer_id,'voice_lead_addon',1); ?>
                console.log(result.call_sid);
                callSid = result.call_sid;
                callStatus = setInterval(function () {
                    checkCallStatus(result.call_sid)

                }, 2000);
            }
        });
    });

    function checkCallStatus(sid){

        $.ajax({
            url: "{{url('twilio-call-chat/call_status.php')}}",
            type: "POST",
            data: {call_sid: sid},
            dataType: 'json',
            success: function(result){
                console.log(result.call_status);

                if(result.call_status == 'queued' || result.call_status == 'initiated' || result.call_status == 'ringing'){
                    $(".call-button").html('Connecting...');
                }
                else if(result.call_status == 'in-progress'){
                    $(".call-button").html('Connected!');
                }
                else if(result.call_status == 'busy' || result.call_status == 'failed' || result.call_status == 'no-answer'){
                    $(".call-button").html('Call Failed');
                    stopFunction();
                }
                else if(result.call_status == 'completed'){
                    $(".call-button").html('Call Again');
                    callSid = '';
                    stopFunction();
                }
            }
        });
    }


    function stopFunction(){
        clearInterval(callStatus);
    }


  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
    if (ratingValue > 1) {
        message = "Thanks! You rated this";
		msg = "" + ratingValue + "";
		message2 = " stars ";
		
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + "stars";
    }
    responseMessage(msg);
	responseMessage2(message);
	responseMessage3(message2);
    
  });
  
  
	});


	function responseMessage(msg) {
	  $('.success-box').fadeIn(200); 
	  $('.success-box div.text-message').html("<span>" + msg + "</span>");
	  var rating = msg; 
	  document.getElementById("rating").value = rating; 
	}

	function responseMessage2(message) {
	  $('.success-box').fadeIn(200); 
	  $('.success-box div.text-message2').html("<span>" + message + "</span>");
	}

	function responseMessage3(message3) {
	  $('.success-box').fadeIn(200); 
	  $('.success-box div.text-message3').html("<span>" + message2 + "</span>");
	}
	

	
	
    function message_post_reply(msg_ID,car_ID,dealer_ID){
		
	
	  $('#message_reply_modal').modal('show');
	   
	  $(document).on('click','.reply_message',function(){ 
		
	     var message_reply = $('#message_reply').val();
		 var car_id =  car_ID;
		 var dealer_id = dealer_ID;
		 var message_id = msg_ID;

		$.ajax({
			type:'POST',
			data: {
				"_token": "{{ csrf_token() }}",
				message: message_reply,
				message_id: message_id,
				car_id:car_id,
				dealer_id:dealer_id
			},

			url:'{{url('save/reply/ajax')}}',

			success: function () {
			
			$('.success_message').html('Successfully replied');

			setTimeout(function(){
			$('#message_reply_modal').modal('hide');
			}, 2000);
				 
			}
			

		});
		
		
	 location.reload();  
		

	});

	}	

	
	
   function car_rating_update(carid,dealerid,id,title,message,rating){

		$('#primary_udate').modal('show');
		
		$(".title_msg2").val(title);
		
		$(".write_msg2").val(message);


   // UPDATE RATING & REVIEW		
	
	$(document).on('click','.update_message',function(e){
	
		e.stopImmediatePropagation();
		
		var title_update = $('.input_msg_write').find(".title_msg2").val();
		var message_update = $('.input_msg_write').find(".write_msg2").val();	
		var car_id = carid;
		var dealer_id = dealerid;
		var Orating = rating;
		var Nrating = $("#rating").val();
		var review_id = id;
	
	
		$.ajax({
			type:'POST',
			data: {
				"_token": "{{ csrf_token() }}",
				title: title_update,
				message: message_update,
				car_id: car_id,
				dealer_id:dealer_id,
				Orating:Orating,
				Nrating:Nrating,
				review_id:review_id
			
			},

			url: '{{url('update/reviews/ajax')}}',
			
			success:  function() {			
				
			$('.modal-body').fadeIn().html('Rating and review updated successfully').css({"font-size": "18px", "color": "green", "text-align": "center"});

			setTimeout(function(){
			$('.modal').modal('hide');
			}, 3000);
				
			location.reload();	 
		
		
			}

		});
	

	});

	
	// DELETE RATING & REVIEW	
	
	$(document).on('click','.delete_message',function(e){
			
			e.stopImmediatePropagation();
			
				var review_id = id;
		
				$.ajax({
					type:'POST',
					data: {
						"_token": "{{ csrf_token() }}",
						
						 review_id:review_id
					
					},

					url: '{{url('delete/reviews/ajax')}}',
					
					success:  function() {			
						
					$('.modal-body').fadeIn().html('Rating and review deleted successfully').css({"font-size": "18px", "color": "green", "text-align": "center"});

					setTimeout(function(){
					$('.modal').modal('hide');
					}, 3000);
						
					location.reload();	
				
				
					}

	 });	

	});


	}	
	
	
	</script>
@endsection