@extends('layouts.web_pages')
@section('content')
<div class="header-margin py-2 bg-secondary">
  <div class="header-margin py-2">
    <div class="container">
      <div class="row" style="padding-bottom: 6em;">
        <div class="col-lg-8 offset-lg-2">
          <div class="card">
             <div class="card-header font-weight-bold fs18">Thank you!</div>
                 <div class="card-body">
                    <div class="row">
                        Thank you for contacting us!
                    </div>
                 </div>
            </div>
        </div>
    </div>
</div>
@endsection