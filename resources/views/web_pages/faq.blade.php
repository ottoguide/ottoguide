@extends('layouts.web_pages')
@section('content')
<style>
.heading {
    font-size: 17px;
    color: #000000;
    font-family: 'Century Gothic',CenturyGothic,AppleGothic,sans-serif;
    font-weight: bold;
}
</style>
  
  <div class="header-margin py-4 bg-secondary">
    <div class="container">
      <div class="row">                    
        <div class="col-md-12 col-sm-12">

          <h1>FAQ'S
    </h1>
        </div>
      </div>
    </div>
  </div>
  <div class="py-4">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <p>
			<h5 class="heading">1. How does Ottoguide.com work?</h5> </p>
			<p><h5>Ottoguide.com is a communication tool that puts you in direct contact with decision makers at dealerships.</h5></p>
	        
			<video width="65%"  preload="none"  controls autoplay>
			  <source src="<?=url('videos/How OTTO Guide Works.webm')?>" type="video/webm">
			  
			</video>
			<hr>
          <p>
			<h5 class="heading">2. Is there a fre for ottoguide.com services to car shoppers?</h5> </p>
			<p><h5>NO.</h5></p>          </p>
			
				<hr>
          <p>
			<h5 class="heading">3. What if a dealership is trying to cheat me?</h5> </p>
			<p><h5> If you ever feel uncomfortable with the dealer walk away? You can always email <a href="mailto:admin@ottoguide.com" target="_top">admin@ottoguide.com</a> for help.</h5></p></p>
				<hr>
		     <p>
			<h5 class="heading">4.  Does ottoguide.com have luxury cars? </h5> </p>
			<p><h5>Yes we display luxury cars but they belong to the dealership.</h5></p>
			</p>
			
	<hr>
			 <p>
			<h5 class="heading">5. How can I be an ottoguide.com affiliate.</h5> </p>
			<p><h5>Email admin@ottoguide.com.</h5></p>
			</p>
			
			
			
			
        </div>
      </div>
    </div>      
  </div>
@endsection



