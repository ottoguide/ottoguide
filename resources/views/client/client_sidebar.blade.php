<div class="col-sm-3 col-md-3 col-lg-3">
  <div class="card border-primary mb-3">
    <h4 class="card-header bg-primary font-weight-bold text-light"></h4>
    <ul class="list-group list-group-flush cxm-list-group">               
      <li class="list-group-item">
        <a href="{{route('save_adverts')}}">My Saved Cars</a>
        <div class="info-txt">My all saved cars list</div> 
        <span class="badge badge-primary">
		<?php 
		 $count_search =  DB::table('client_favourite')
		 ->where('client_id',Auth::id())->count();
		echo $count_search;
		?>
		</span>
      </li>
      <li class="list-group-item ">
        <a href="{{route('save_searches')}}">My Saved Searches</a>
        <div class="info-txt">List of all saved searches</div> 
        <span class="badge badge-primary">
		<?php 
		 $count_search =  DB::table('client_save_search')
		 ->where('client_id',Auth::id())->count();
		echo $count_search;
		?>
		</span>
		</li>  
		<li class="list-group-item ">
        <a href="{{route('saved_reviews')}}">My Reviews & Rating </a>
        <div class="info-txt">List of all saved reviews & rating. </div> 
        <span class="badge badge-primary">
		<?php 
		 $count_reviews =  DB::table('client_reviews')
		 ->where('client_id',Auth::id())->where('reply_to',null)->count();
		  echo  $count_reviews;
		?>
		</span>	
		</li>  
		

		
      <li class="list-group-item">
        <a href="{{route('chat_list')}}">MY Chat</a>
        <div class="info-txt">Detail of my chat</div> 
        <span class="badge badge-primary">
		<?php 
		 $count_chat_channel =  DB::table('twilio_chat')
		 ->where('user_id',Auth::id())->count();
		  echo  $count_chat_channel;
		?>
		</span>
		</li>
		
		<?php 
		 $count_rbo = DB::table('request_best_offer')
		 ->where('user_id',Auth::id())->get();
		?>
		<li class="list-group-item">
		@if(count($count_rbo))
		<a href="{{ route('rbo-quote-form-list') }}">Request Best Offers</a>
		@else
           	<a href="#">Request Best Offers</a>  
		@endif
        <div class="info-txt">Request Best Offer</div> 
		<span class="badge badge-primary">
		<?php echo $count_rbo->count(); ?>
		</span>
         </li>
		
		

      
	  <li class="list-group-item">
        <a href="{{route('my_account')}}">My Account</a>
        <div class="info-txt">view my account status</div> 
      </li>
	   
	   <li class="list-group-item">
        <a href="{{route('user_email_box')}}?email='.<?= encrypt('Inbox')?>.'">Email Box</a>
        <div class="info-txt">view my email box</div> 
      </li>
	  
	  

	  <li class="list-group-item">
        <a href="{{route('ublock_request')}}">Request For Unblock</a>
        <div class="info-txt">Request to admin for unblock</div> 
        <span class="badge badge-primary">0</span>
		</li>
	  
	  
	    <li class="list-group-item">
        <a href="{{route('user_questioner_list')}}">Questioner List</a>
        <div class="info-txt">View all attempt quiz search </div> 
        <span class="badge badge-primary">
		
		<?php 
		 $count_quiz =  DB::table('quiz_query')
		 ->where('user_id',Auth::id())->count();
		  echo  $count_quiz;
		?>
		
		
		</span>
		</li>
	 
	  
    </ul>
  </div>
</div>

<div class="modal fade" id="policy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" align="center" id="exampleModalLongTitle"><b>Legal Agreement & Policy</b></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
	  <div class="modal-body">
  
		@include('web_pages.policy')
  
      </div>
      
	  <div class="modal-footer">
		<a href="{{route('rbo-quote-form-list')}}" class="btn btn-success btn-lg">
                 <span class="fa fa-money"></span> Accept Policy</a> 
				 
            <a href="{{route('RequestBestOfferReject')}}" class="btn btn-danger btn-lg">
                 <span class="fa fa-money"></span> Reject Policy</a> 
			

      </div>
    </div>
  </div>
</div>
