<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- CSS -->
    <link rel="stylesheet"
     href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	 <link rel="stylesheet"
     href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/twiliochat2.css') }}">
	
    </head>

<style>
#leave-span{
margin-right: 2%;	
}
#leave-span b{
padding:5px;
background-color: #cc0000;
color:#fff;
border-radius: 4px;
font-size:11px;
}

#report b{
padding:5px;
background-color: #52075d;
color:#fff;
border-radius: 4px;
font-size:11px;
}


#reported b{
padding:5px;
background-color: #999;
color:#fff;
border-radius: 4px;
font-size:11px;
}


#report {
margin-right: 5%;
}

#reported {
margin-right: 5%;
}

#status-span b{
padding:5px;
background-color: #158CBA;
color:#fff;
border-radius: 4px;
font-size:11px;
}
.border-left{
border-left: 1px solid #e2e2e2;
}

#input-text{
width: 90%;	
}

small{
color: #999;
padding-left: 15px;	
}
#send{
margin-top:15px;	
}
.report{
margin-left: 20px;
}

		.no-margin {
			margin-top: 3px;
			margin-left: 0px;
			margin-right: 0px;
			margin-bottom: 0px;
		}

		.message-info-row {
			padding-top: 0.2vh;
			 padding-bottom: 0.1vh;
		}


		#message-list.connected {
			/* height: 92%; */
			max-height: 550px;
			min-height: 300px;
		}


</style>
   <br>
   <div class="border-left">
   
      <div class="row">
         <div class="col-md-2 left-align col-md-offset2">
         <div id="status-row" class="disconnected">
		
		  </div> 
		 
		  </div>
          
		  <!--<span id="delete-channel-span"><b>Delete current channel</b></span>-->
          
            <div class="col-md-6 right-align">
		
			<span id="status-span"><i class="fa fa-user"></i> Connected as <b><span id="username-span"></span></b></span>
             <span id="leave-span"><b> Leave Channel </b></span>
			 @if(Session::get('dealer_id'))
			 <?php $check_block_status = DB::table('twilio_chat')->where('chat_channel',$_GET['channel_name'])->where('is_block_request',1)->get(); ?>	 
			 @if(count($check_block_status))
			 <span id="reported"><b>Reported</b></span> 	 
			 @else
			 <span id="report"><b>Report Channel</b></span>
		     @endif
			 @endif
            </div>
          </div>
     <br>
	
	 <div id="container" class="row">
	 
		<div id="chat-window" class="col-md-12 margin-left">
		   <div class="col-md-12">
                <input id="username-input" type="text" placeholder="username"/>
                <input id="channel_name" value="<?=(isset($_GET['channel_name'])?$_GET['channel_name']:'blank')?>" type="hidden"  />

			  </div>
    <!-- HIDE AFTER LOAD CHANNEL -->	
		<div id="connect-panel" class="disconnected row ">
            <div class="row">
              <div class="col-md-12">
                <img id="connect-image" src=""/>
              </div>
            </div>
			
          </div>
		  
	<!-- end hide -->	
		<div class="col-md-10">
          <div id="message-list" class="row disconnected"></div>
          <div id="typing-row" class="row disconnected">
          <p id="typing-placeholder"></p>
          </div>
		  <br>
        <textarea id="input-text" disabled="false" required placeholder="write your message"></textarea>
        <button type="button" id="send" class="btn btn-info pull-right"> <i class="fa fa-paper-plane" aria-hidden="true"></i>  Send </button>
     
		<small> Note: (1) Click send button to send message. (2) Press enter key for break line.</small>
        
		</div>
	
      </div>
		
    <!-- HTML Templates -->
	
	 <script type="text/html" id="channel-template">
      <div class="col-md-12">
      <p class="channel-element" data-content="channelName">
	  </p>
      </div>
	 
    </script>
	
	
    <!-- HTML Templates -->
	
    <script type="text/html" id="message-template">
	
      <div class="no-margin ">
		  <div class="col-md-6 right-align">
		  <span data-content="date" class="message-date">
		  </span>
		  </div>
        <div class="no-margin message-info-row">
          <div class="col-md-6 left-align">
		  <p data-content="username" class="message-username"></p></div>
		  </div>

        <div class="no-margin message-content-row">
          <div style="" class="col-md-12">
		  <p data-content="body" class="message-body">
		  </p>
        </div>
		
		</div>
        
	  <script type="text/html" id="channel-template">
      <div class="col-md-12">
        <p class="channel-element" data-content="channelName"></p>

      </div>
	  

    </script>
   
      </div>

    </script>
	
	</div>
	
    <!-- JavaScript -->
    <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="{{ asset('js/vendor/jquery-throttle.min.js') }}"></script>
    <script src="{{ asset('js/vendor/jquery.loadTemplate-1.4.4.min.js') }}"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js"></script>
    <!-- Twilio Common helpers and Twilio Chat JavaScript libs from CDN. -->
    <script src="//media.twiliocdn.com/sdk/js/common/v0.1/twilio-common.min.js"></script>
    <script src="//media.twiliocdn.com/sdk/js/chat/v3.0/twilio-chat.min.js"></script>

	<script src="{{url('js/twiliochatty.js')}}"></script>
    <script src="{{ asset('js/dateformatter.js') }}"></script>

	<script>
	$( document).ready(function() {
    $('#username-input').val('<?= $_GET['id'] ?>');  
	$('#connect-image').trigger('click');
	});
	</script>

	<script>
	 
	 $(document).on('click', '#report', function (e) {
		
		e.stopImmediatePropagation();
		
	 var report_channels = $('#channel_name').val();
	
		 	$.ajax({
			type:'POST',
			data: {
			"_token": "{{ csrf_token() }}",	
			 report_channel: report_channels
            },
			url:'{{url('user-check-activation.html')}}',
			
			success:  function () {
			alert('Channal successfully reported');
			}
			
			}); 
		
			});
	 
	  
	</script>
	
	
</html>
