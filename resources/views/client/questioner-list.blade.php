@extends('layouts.web_pages')
@section('content')
<style>
	#ads {
    margin: 30px 0 30px 0;
   
}

#ads .card-notify-badge {
        position: absolute;
        left: -10px;
        top: -20px;
        background: #f2d900;
        text-align: center;
        border-radius: 30px 30px 30px 30px; 
        color: #000;
        padding: 5px 10px;
        font-size: 14px;

    }

#ads .card-notify-year {
        position: absolute;
        right: -10px;
        top: -20px;
        background: #ff4444;
        border-radius: 50%;
        text-align: center;
        color: #fff;      
        font-size: 14px;      
        width: 50px;
        height: 50px;    
        padding: 15px 0 0 0;
}


#ads .card-detail-badge {
        text-transform: uppercase;	
        text-align: center;
        border-radius: 30px 30px 30px 30px;
        color: #127ba3;
        padding: 5px 10px;
        font-size: 18px;
		font-weight:bold;	
    }

   

#ads .card:hover {
            background: #fff;
            box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
            border-radius: 4px;
            transition: all 0.3s ease;
        }

#ads .card-image-overlay {
       font-size: 20px; 
    }


#ads .card-image-overlay span {
            display: inline-block;              
        }


#ads .ad-btn {
        width: 60px;
        height: 25px;
        border-radius: 10px;
        font-size: 14px;

        text-align: center;
        border: 3px solid #e6de08;
        display: block;
        text-decoration: none;
        margin: 20px auto 1px auto;
        color: #000;
        overflow: hidden;        
        position: relative;
        background-color: #e6de08;
    }

	#ads .ad-btn-delete {
        width: 60px;
        height: 25px;
        border-radius: 10px;
        font-size: 14px;
        text-align: center;
        border: 3px solid #cc0002;
        display: block;
        text-decoration: none;
        margin: 20px auto 1px auto;
        color: #fff;
        overflow: hidden;        
        position: relative;
        background-color: #cc0000;
    }
	
	
	
#ads .ad-btn:hover {
            background-color: #e6de08;
            color: #1e1717;
            border: 2px solid #e6de08;
            background: transparent;
            transition: all 0.3s ease;
            box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
        }
		
#ads .ad-btn-delete:hover {
            background-color: #fff;
            color: #1e1717;
            border: 2px solid #cc0000;
            background: transparent;
            transition: all 0.3s ease;
            box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
        }

#ads .ad-title h5 {
   font-size: 16px;
    }
.rounded{
border-radius: 8px;	
}

</style>

<div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Questioner Search List</h1>            
          </div>
        </div>
      </div>      
    </div>
    <br>
	 <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
					
					  @if (session('error'))
                        <div class="alert alert-danger" width="50%">
                            {{ session('error') }}
                        </div>
                    @endif
		      </div>	
    <div class="py-5">
      <div class="container">
        <div class="form-row">
         <!-- client sidebar -->
		 @include('client.client_sidebar')
		 <!-- client sidebar --> 
		 
          <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="card">
              <div class="card-header font-weight-bold fs18">Questioner List</div>
              <div class="card-body">
                <div class="form-row">
                  
				  <div class="col-sm-10 col-md-10 col-lg-10">
				  
	               <div class="row" id="ads">
    <!-- Category Card -->
   
   <?php 
   $get_quizes = DB::table('quiz_query')->join('quiz','quiz_id','=','quiz.id')
   ->where('user_id',Auth::id())
   ->paginate(3);
   
   ?>
   
   
   @if(count($get_quizes))
   
	@foreach($get_quizes as $get_quiz)
   
   <div class="col-md-4">
        <div class="card rounded">
            <div class="card-image">
                <span class="card-notify-badge">Quiz: <b>{{$get_quiz->id}}</b></span>
               
                <img class="img-fluid" src="/images/quiz-home.png" alt="Alternate Text" />
            </div>
			
            <div class="card-image-overlay m-auto"> <br>
                <span class="card-detail-badge">@if(!empty($get_quiz->title) > 0) {{$get_quiz->title}} @endif</span>
            </div>
			
            <div class="card-body text-center">
                <div class="ad-title m-auto">
                <h5> @if(!empty($get_quiz->description) > 0) {{$get_quiz->description}} @endif </h5>
                </div>				
				<a class="ad-btn pull-left" href="{{route('user_questioner_view', ['quiz_id' => encrypt($get_quiz->id)])}}">View</a>
			    <a class="ad-btn-delete pull-right" href="{{route('user_questioner_delete', ['quiz_id' => encrypt($get_quiz->id)])}}">Delete</a>
            </div>
			
        </div>
    </div>
 
	@endforeach	
	
	@else
    <h6 align="center">No Questioner Found </h6>	

	@endif

	</div>
	
	<center> <ul class="pagination justify-content-center">      
          <?php
            $links = $get_quizes->links(); 
        		$links = str_replace("<a", "<a class='page-link ' ", $links);
        		$links = str_replace("<li", "<li class='page-item' ", $links);
        		$links = str_replace("<span", "<span class='page-link'",$links);
        		echo $links;?>
          </ul></center>
				  
				  </div>
                 
                </div>
              </div>
            </div>
          </div>          
                    
        </div>
      </div>      
    </div>
    @endsection