@extends('layouts.web_pages')
@section('content')
<style>

</style>

<?php if(Auth::user()){ ?>

 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Chat Communication </h1>            
          </div>
        </div>
      </div>      
    </div>
    
	@if(!empty($car_datas) > 0)
	
    <div class="py-5">
		<div class="container">	 
	   
	    <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
	   
		</div>
		
    <div class="container">
    
    	<div class="form-row">   
    		 <!-- client sidebar -->
    		  @include('client.client_sidebar')
 		 
    		<!-- client sidebar -->
          <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="card">
              <div class="card-header">
                <div class="row no-gutters">
                  <div class="col-md-4 text-center text-md-left font-weight-bold fs18">My Chat Communication List</div>
                  <div class="col-md-8">
                    <div class="text-center text-md-right">
                   
                    </div>
                  </div>
                </div>
              </div>
			  
			  
              <div class="card-body">
                <div class="row">				        				
				@foreach($car_datas as $key => $data)
                <div class="col-md-4">                  
                  <div class="cxm-advert-item mb-4">   
				  
                 	   <div class="cxm-img">
                      <!--<div class="cxm-img-badge position-absolute"><span class="fa fa-camera"></span> </div>-->
                      <a href="{{url('car/detail').'/'.$car_datas[0]->id}}"><img class="img-fluid" src="@if(!empty($data->media->photo_links[0]) > 0)
            						{{$data->media->photo_links[0]}}
            						@else
            						{{asset('public/images/no-image.jpeg')}}
                      @endif"></a>
					  
                    </div>	


                    <div class="cxm-content">
                      <a class="fs18 lh18 text-dark" href="#">
            			@if(!empty($data->heading) > 0)
            			{{$data->heading}}
                        @endif
          				    </a>
                      <hr class="hr1">
					 <center> <a href="<?= url('mychat.html/?car_id=')?><?=$show_chat_list->car_id?>&channel_name=<?=$show_chat_list->chat_channel?>&dealer_id=<?=$show_chat_list->dealer_id?>" target="_blank"><button class="btn btn-primary btn-large">Join Chat Channel </button> </a></center>
					  
                    </div>
                    <hr class="hr1">
                  </div>                
                </div>
			   @endforeach			   
                </div>
               
              </div>
		  <ul class="pagination justify-content-center">      
            <?php
              $links = $car_datas->render(); 
              $links = str_replace("<a", "<a class='page-link ' ", $links);
              $links = str_replace("<li", "<li class='page-item' ", $links);
              $links = str_replace("<span", "<span class='page-link'",$links);
              echo $links;
            ?>
           </ul>  
				
			@else
			<br><br>
			<h3 align="center"> Reviews & Rating Not Found. </h3>
			<br><br>
			@endif
	
            </div>
          </div>  
	
	
        </div>
      </div>      
    </div>
	
	
	<?php } 
	else{
		echo "Please login to the system for access this area";	
	}
	?>

	<script>
	$(document).ready(function () {
	  $("a").tooltip({
		'selector': '',
		'placement': 'top',
		'container':'body'
	  });
	});

	$('.auto-tooltip').tooltip();
	</script>


	@endsection