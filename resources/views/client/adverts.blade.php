@extends('layouts.web_pages')
@section('content')

<?php if(Auth::user()){ ?>

 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>My Saved Cars</h1>            
          </div>
        </div>
      </div>      
    </div>
    
    <div class="py-5">
		<div class="container">	 
	    @if($errors->any())
        <p class="alert alert-success">Saved Advert Deleted successfully </p>
      @endif 
		</div>
    <div class="container">
      <div class="form-row">   
    		 <!-- client sidebar -->
    		  @include('client.client_sidebar')
    		 
    		 <!-- client sidebar --> 
		 
			   <?php foreach($show_fav as $show_data);
				if(!empty($show_data->client_id) > 0){
				$client_id = $show_data->client_id;
				}
				else{$client_id=""; } ?>	
		
          <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="card">
              <div class="card-header">
                <div class="row no-gutters">
                  <div class="col-md-4 text-center text-md-left font-weight-bold fs18">My Saved Cars</div>
                  <div class="col-md-8">
                    <div class="text-center text-md-right">
                      @if(!$show_fav->isEmpty())
                      <a href="#" class="btn btn-primary btn-sm fs10"><span class="fa fa-history"></span> Remove Expired</a>
                     <a href="{{route('delete_all_save_advert',['client_id'=> encrypt($client_id)])}}" class="btn btn-primary btn-sm fs10" onclick="return confirm('Are you sure you want to delete all items?');"><span class="fa fa-trash-o"></span>Remove All</a>
                     @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                 @if(!$show_fav->isEmpty())
                <div class="row">				
        				@foreach($show_fav as $show_data)
        				<?php 
        				$json = base64_decode($show_data->json_data);
        				$data = json_decode($json, true) ?>
				
                <div class="col-md-4">                  
                  <div class="cxm-advert-item mb-4">            
                    <div class="cxm-img">
                      <!--<div class="cxm-img-badge position-absolute"><span class="fa fa-camera"></span> {{count($data['images'])}}</div>-->
                      <a href="{{url('car/detail').'/'.$show_data->car_id}}"><img class="img-fluid" src="@if(!empty($data['images'][0]) > 0)
            						{{$data['images'][0]}}
            						@else
            						{{asset('public/images/no-image.jpeg')}}
                      @endif"></a>
                    </div>						
                    <div class="cxm-content">
                      <a class="fs18 lh18 text-dark" href="{{url('car/detail').'/'.$show_data->car_id}}">
            					  @if(!empty($data['title']) > 0)
            				     {{$data['title']}}
                        @endif
          				    </a>
                     
                      <div><span class="fs24 fc1">$ @if(!empty($data['price']) > 0)
						{{number_format((float)$data['price'])}}
                 @endif</span></div>
                  <ul class="cxm-facts fs12 bg-secondary p-2 rounded">
                        <li><span class="fa fa-modx text-primary"></span> 
						@if(!empty($data['build']['year']) > 0)
						{{$data['build']['year']}}
                 @endif</li>
                        <li><span class="fa fa-barcode text-primary"></span> 
						@if(!empty($data['build']['body_type']) > 0)
						{{$data['build']['body_type']}}
                        @endif
						</li>
                        <li><span class="fa fa-road text-primary"></span> 
						@if(!empty($data['miles']) > 0)
						{{$data['miles']}}
                        @endif  miles
						</li>
                        <li><span class="fa fa-toggle-on text-primary"></span>
						@if(!empty($data['build']['transmission']) > 0)
						{{$data['build']['transmission']}}
                       @endif
						</li>
                        <li><span class="fa fa-spinner text-primary"></span> 
						
						@if(!empty($data['build']['engine']) > 0)
						{{$data['build']['engine']}}
                       @endif
						</li>
                        <li><span class="fa fa-fire text-primary"></span> 
						@if(!empty($data['build']['fuel_type']) > 0)
						{{$data['build']['fuel_type']}}
                       @endif
						</li>
                      </ul>
                      <a class="btn btn-primary btn-sm" href="{{route('delete_save_advert',['favourite_id'=> encrypt($show_data['favourite_id'])])}}" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash"></span>Delete</a>
                    
                    </div>
                  
                  </div>                
                </div>
			   @endforeach			   
                </div>
                @else
                 <p align="center" width="100%">No data found</p>
                @endif
              </div>
            </div>
			
			   <ul class="pagination justify-content-center">      
          <?php
            $links = $show_fav->links(); 
        		$links = str_replace("<a", "<a class='page-link ' ", $links);
        		$links = str_replace("<li", "<li class='page-item' ", $links);
        		$links = str_replace("<span", "<span class='page-link'",$links);
        		echo $links;?>
          </ul>
			
			
          </div>  
	
		  
                    
        </div>
      </div>      
    </div>
<?php } 
else{
	echo "<br>"; echo "<br>"; echo "<br>";	
	echo "<p align='center'>"."Client Area"."</p>";	
}
?>


	@endsection