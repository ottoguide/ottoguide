@extends('layouts.web_pages')
@section('content')

<style>
.bg-white{
	margin-bottom: 2%;
    padding:1%;
    border-top: 3px solid #127ba3;
	border-right: 1px solid #ccc;
	border-left: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	border-radius: 5px;	
  }
  input[type=checkbox] {
    display: inline-block;
    *display: inline;
    vertical-align: middle;
    margin: 0;
    padding: 0;
    width: 16px;
    height: 16px;
    border: none;
    cursor: pointer;
    color: #fff;
    -webkit-background-size: 240px 24px;
    background-size: 240px 24px;
}
 .badge {
    display: inline-block;
    padding: 0.50em 0.75em;
    font-size: 80%;
    font-weight: 500;
	}

.card-header {
		padding: 0.40rem 0.75rem;
		background-color:#fff; 
	}

.table th, .table td {
		   padding: 0.30rem;
		   padding-left:15px;
		}	
</style>

  <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
  <script type="text/javascript">
//<![CDATA[
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
  //]]>
  </script>

<?php if(Auth::user()){ ?>

 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Compose New Message</h1>            
          </div>
        </div>
      </div>      
    </div>
    

	
    <div class="py-5">
		<div class="container">	 
	   
	    <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
	   
		</div>
    <div class="container">
	
      <div class="row">   
    		 <!-- client sidebar -->
    		  @include('client.emailbox_sidebar')

		 <div class="col-md-9 bg-white">

		 <br>
		 
		 <form id="email" action="{{url('user-services-email-send.html')}}" method="post">
		 {!! Form::hidden('_token', csrf_token()) !!}
          <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
			  <label>To: </label>
                <input class="form-control" value="{{$dealer_data['dealer_name']}}" readonly name="dealer_name">
              </div>
              <div class="form-group">
			   <label>Subject: </label>
                <input class="form-control"  value="<?php echo $_GET['email_subject'];?> ({{$dealer_data['car_heading']}})" readonly name="subject">
              </div>
              
			  
			  <div class="form-group">
			   <label>Message: </label>
                <textarea class="form-control" name="message" style="height:200px; padding:15px;" placeholder=""></textarea>
              </div>
			  
			  @if(!empty(Session::get('services')))
		
		 <div class="col-md-12 bg-white">
		<div class="card refine-search">
						<div class="card-header font-weight-bold">
						
							<a data-toggle="collapse" href="#refineSearch" role="button" aria-expanded="false" aria-controls="refineSearch">			
							<img class="img-fluid" src="http://dev.ottoguide.com/public/images/service-car.svg" width="40"> 
						<b>Your Selected Services Details</b></a>
						</div>
						<div class="collapse show" id="refineSearch">
							<div class="">
								
								
							<table class="table services" style="margin: 0;">
										  <?php
										  
											foreach(Session::get('services') as $key=> $description) {
												//if ($key == 'Year' || $key == 'Model' || $key == 'Make' || $key == 'Trim' || $key == 'Mileage') {
													//continue;
												//}
										   ?>
											<tr>
												<td colspan="2" width="40%">
													<h6><i class="fa fa-caret-right"></i>&nbsp; <b>{{$key}}</b></h6>
												</td>
												<td colspan="3">
											  
													<h6>{{ucfirst($description)}}</h6>
												</td>
											</tr>
										<?php } ?>
									</table>
							
							</div>
						</div>

					</div>
					</div>
	
						@endif
			  
			  
			  
			  
			   
              <input type="hidden" name="dealer_id" value="{{$dealer_data['dealer_id']}}">
            
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i>  Send Message  </button>
              </div>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
		
		
	   </form>  
			 
			 
			 
			  <ul class="pagination justify-content-center">      
            <?php
            /*   $links = $car_datas->render(); 
              $links = str_replace("<a", "<a class='page-link ' ", $links);
              $links = str_replace("<li", "<li class='page-item' ", $links);
              $links = str_replace("<span", "<span class='page-link'",$links);
              echo $links; */
            ?>
          </ul>  
				
			
			
		
            </div>
          </div>  
	
		    
        </div>
      </div>      
    </div>
	
	
<?php } 
else{
	echo "CLient ARea";	
}
?>


	@endsection