@extends('layouts.web_pages')
@section('content')


<div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>My Account</h1>            
          </div>
        </div>
      </div>      
    </div>
    
    <div class="py-5">
      <div class="container">
        <div class="form-row">
         <!-- client sidebar -->
		 @include('client.client_sidebar')
		 <!-- client sidebar --> 
		 
          <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="card">
              <div class="card-header font-weight-bold fs18">My Account</div>
              <div class="card-body">
                <div class="form-row">
                  <div class="col-md-7">
                    <div class="px-2 py-3 border rounded fs12 lh18 bg-secondary mb-3">
                      <div class="row no-gutters mb-2">
                        <div class="col-sm-3"><strong>Name:</strong></div>                      
                        <div class="col-sm-9">{{Auth::user()->name}}</div>
                      </div>
                      
                      <div class="row no-gutters mb-2">
                        <div class="col-sm-3"><strong>Display Name:</strong></div>                      
                        <div class="col-sm-9">{{Auth::user()->name}}</div>
                      </div>
                      
                      <div class="row no-gutters mb-2">
                        <div class="col-sm-3"><strong>Address:</strong></div>                      
                        <div class="col-sm-9">Donec justo odio, ultricies eu orci et, varius fermentum dui.</div>
                      </div>
                      
                      <div class="row no-gutters mb-2">
                        <div class="col-sm-3"><strong>Town/City:</strong></div>                      
                        <div class="col-sm-9">City.</div>
                      </div>
                      
                      <div class="row no-gutters mb-2">
                        <div class="col-sm-3"><strong>Mobile:</strong></div>                      
                        <div class="col-sm-9">{{Auth::user()->mobile}}</div>
                      </div>
                      
                      <div class="row no-gutters mb-2">
                        <div class="col-sm-3"><strong>Telephone:</strong></div>                      
                        <div class="col-sm-9">{{Auth::user()->phone}}.</div>
                      </div>
                      <div class="text-center text-sm-right">
                        <a class="btn btn-primary btn-sm fs10" href="#"><span class="fa fa-edit"></span> Edit</a>
                      </div>  
                    </div>
                    
                    <div class="px-2 py-3 border rounded fs12 lh18 bg-secondary">
                      <div class="fs14 font-weight-bold mb-2">Account Connections:</div>
                      <p>Social - Sign in faster by connecting your social media account</p>
                      
                      <div class="text-center w-25">
                        <span class="fa fa-facebook-square fc1 fs48"></span><br>
                        <a class="text-success" href="#"><span class="fa fa-link"></span> Connected</a><br>
                        <a class="text-dark" href="#"><span class="fa fa-unlink"></span> Disconnect</a>
                      </div>
                      
                    </div>
                    
                  </div>
                  <div class="col-md-5">
                    <div class="bg-secondary p-2 border rounded fs12">
                    <div class="font-weight-bold mb-2 fs14">Sign in Detail</div>
                      <div class="row no-gutters mb-2">
                        <div class="col-sm-3"><strong>Email:</strong></div>                      
                        <div class="col-sm-9">{{Auth::user()->email}}</div>
                      </div>
                      <div class="row no-gutters mb-2">
                        <div class="col-sm-3"><strong>Password:</strong></div>                      
                        <div class="col-sm-9">********</div>
                      </div>
                      <div class="text-center text-sm-right">
                        <a class="btn btn-primary btn-sm fs10" href="#"><span class="fa fa-edit"></span> Edit</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>          
                    
        </div>
      </div>      
    </div>
    @endsection