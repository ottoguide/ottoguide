@extends('layouts.web_pages')
@section('content')
<style>
.bg-white{
	margin-bottom: 2%;
    padding:1%;
    border-top: 3px solid #127ba3;
	border-bottom: 1px solid #ccc;
	border-left: 1px solid #ccc;
	border-right: 1px solid #ccc;
	border-radius: 5px;	
  }
  input[type=checkbox] {
    display: inline-block;
    *display: inline;
    vertical-align: middle;
    margin: 0;
    padding: 0;
    width: 16px;
    height: 16px;
    border: none;
    cursor: pointer;
    color: #fff;
    -webkit-background-size: 240px 24px;
    background-size: 240px 24px;
	}
	
 .table th, .table td {
		padding: 0.40rem;
	}	
 
 .mailbox-subject {
  text-transform: Capitalize;	
  }

.empty{
  margin-right: 5px;	
 }
.fa-envelope{
	color:#28B62C;
}

</style>

<link rel="stylesheet" href="https://l-lin.github.io/font-awesome-animation/dist/font-awesome-animation.min.css">

<?php if(Auth::user()){ ?>

 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Client Email Panel</h1>            
          </div>
        </div>
      </div>      
    </div>
    

	
    <div class="py-5">
		<div class="container">	 
	   
	    <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
	   
		</div>
    <div class="container">
	
      <div class="row">   
    		 <!-- client sidebar -->
    		  @include('client.emailbox_sidebar')

		 <div class="col-md-9 bg-white">
			  
			
          <div class="box box-primary">
          
            <!-- /.box-header -->
            <div class="box-body no-padding">
			
               <h3 class="pull-left">
			    <?php 
			       $email="Inbox";
				   if(isset($_GET['email'])){
				   $email= decrypt($_GET['email']);   
				   }
				   echo $email;
				 ?>
			   </h3>
			  <div class="mailbox-controls pull-right">  
             
   			  <button class="btn btn-primary delete_button" form="delete_email" type="submit" disabled="">Delete</button>
			 <?php if($email=='Trash'){?>		
			 <a href="{{url('user-empty-trash.html')}}" class="btn btn-danger btn-xs pull-right empty">Empty</a>
			 <?php } ?>
              </div>
			 
			  <br><br>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                 
				  <?php 
				   $email="Inbox";
				   if(isset($_GET['email'])){
				   $email= decrypt($_GET['email']);   
				   }
				   
				   if($email =='Inbox'){ 
				   $get_dealer_email = DB::table('email_box')->where('client_id',Auth::user()->id)
				  //->where('message_by','dealer')
				  ->where('is_delete_by_user',0)
				  ->where('temp_delete_by_user',0)
				  //->where('dealer_first_reply',1)
				  ->where('reply_to',null)
				  ->orderBy('id','DESC')
				  ->join('dealer','email_box.dealer_id','=','dealer.dealer_id')
				  ->paginate(10); 
				   }
				   
				   if($email =='Sent'){ 
				   $get_dealer_email = DB::table('email_box')->where('client_id',Auth::user()->id)
				  ->where('message_by','client')->where('is_delete_by_user',0)->where('temp_delete_by_user',0)->join('dealer','email_box.dealer_id','=','dealer.dealer_id')
				  ->paginate(10);  
				   }
				   
				   if($email =='Trash'){ 
				   $get_dealer_email = DB::table('email_box')->where('client_id',Auth::user()->id)
				   ->where('temp_delete_by_user',1)->where('is_delete_by_user',0)->orderBy('id','DESC')->join('dealer','email_box.dealer_id','=','dealer.dealer_id')
				   ->paginate(10);  
				   }
				  ?>
				 
				   @if(count($get_dealer_email))
				 
					<?php /*
					
					 $message_reply = DB::table('email_box')->where('reply_to',$get_dealer_email[0]->reply_to)->where('message_by','dealer')->orderBy('id','desc')->get()->first();
		 
					 if(count($message_reply)){
					 
					 $message = "<b>".$get_dealer_email[0]->message."</b>";
					 
					 }
					 
					 else{
					 
					 $message = $get_dealer_email[0]->message; 
					 
					 }
					 
					 	 // if read reply envelope show
						 if($message_reply->read_by_client =='0'){
							 $read = '<i class="fa fa-envelope" aria-hidden="true"></i>';
						 }
						 else{
							  $read = '';
						 }
						*/
					?>	
			     
				  @foreach($get_dealer_email as $dealer_email)
				  
				  <?php
					$check_new_email = DB::table('email_box')->where('reply_to',$dealer_email->id)->where('read_by_client',0)->get();
					if(count($check_new_email)){
				    $read = '<i class="fa fa-envelope faa-flash animated" aria-hidden="true"></i>';
					}else{
					$read = '';	
					}	
				   ?>
				  
				    <tr>
					<td class="mailbox-star"><a href="#"><i class="fa fa-star is_read{{$dealer_email->read_by_client}}"></i></a></td>
                    <td class="mailbox-name"><a href="{{url('user-mail-read.html')}}?email_id=<?= encrypt($dealer_email->id) ?>">{{$dealer_email->dealer_name}}</a></td>
                    <td class="mailbox-subject"><b>{{$dealer_email->subject}}</b> &nbsp;&nbsp; <?=$read?>	
				    </td>                                  
                    <td class="mailbox-date"><?= date("d-m-Y", strtotime($dealer_email->created_at))."\n"; ?></td>
					<td><input type="checkbox" value="{{$dealer_email->id}}" class="delete_email" name=""></td>
                    </tr>
				
				  @endforeach
					
				  @else
				  <br>  
				  <h5 align="center"><b>No Email Found </b></h5>			
					
				  @endif
		
                 </tbody>
                </table>
				
				
			<!-- DELETE EMAIL -->	
				
		   <form method="post" id="delete_email" action="{{url('user-temp-delete.html')}}" >
		   {!! Form::hidden('_token', csrf_token()) !!}	
	       <input type="hidden" value="<?= $email ?>" name="email_status" class="email_status">
	       <input type="hidden" id="delete_ids" name="delete_ids" class="delete_ids">	
		   </form>		
				
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
               
               
                <!-- /.pull-right -->
              </div>
            </div>
          </div>
          <!-- /. box -->
        </div>
		
		<div class="col-md-3"></div>
		 <div class="col-md-9">	  
			<ul class="pagination justify-content-center">      
            <?php
              $links = $get_dealer_email->render(); 
              $links = str_replace("<a", "<a class='page-link ' ", $links);
              $links = str_replace("<li", "<li class='page-item' ", $links);
              $links = str_replace("<span", "<span class='page-link'",$links);
			  echo $links; 
			 ?>
            </ul>  
		  </div>		
		
            </div>
          </div>  
	
		    
        </div>
      </div>      
    </div>
	
	<?php } 
	else{
		echo "Client Area";	
	}
	?>
	<script>
	
	$(document).on('change','.delete_email',function(){   
	
			$(".delete_button").attr("disabled", true); 
		
			$('.delete_ids').val('');
		
			var selectedvalue = [];
		
			$(":checkbox:checked").each(function(e){
		
			$(".delete_button").attr("disabled", false); 
			
			selectedvalue.push($(this).val());
		
			$('.delete_ids').val(selectedvalue.join(","));

			});

		});
		
	</script>	



	@endsection