@extends('layouts.web_pages')
@section('content')
<style>
.fs24 {
    font-size: 18px !important;
	font-weight: bold;
}
</style>
<?php if(Auth::user()){ ?>

 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>My Contact Requests</h1>            
          </div>
        </div>
      </div>      
    </div>
    
@if(!empty($car_datas) > 0)
	
    <div class="py-5">
		<div class="container">	 
	   
	    <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
	   
		</div>
    <div class="container">
      <div class="form-row">   
    		 <!-- client sidebar -->
    		  @include('client.client_sidebar')
 		 
    		<!-- client sidebar -->
          <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="card">
              <div class="card-header">
                <div class="row no-gutters">
                  <div class="col-md-4 text-center text-md-left font-weight-bold fs18">My Contact Requests </div>
                  <div class="col-md-8">
                    <div class="text-center text-md-right">
                   
                    </div>
                  </div>
                </div>
              </div>
			  
			  
              <div class="card-body">
                <div class="row">				        				
				
				@foreach($car_datas as $data)
				<?php
				
				if($data->id==""){
				$id="0000";	
				echo"car id not get from api";	
					
				}else{
				$id = $data->id;		
				}
				
				//{{url('car/detail').'/'.$data->id}}
				?>
				
                <div class="col-md-4">                  
                  <div class="cxm-advert-item mb-4">   
				  
                 	<div class="cxm-img">
                      <!--<div class="cxm-img-badge position-absolute"><span class="fa fa-camera"></span> </div>-->
                     <a href="#"><img class="img-fluid" src="@if(!empty($data->media->photo_links[0]) > 0)
            						{{$data->media->photo_links[0]}}
            						@else
            						{{asset('public/images/no-image.jpeg')}}
                      @endif"></a>
                    </div>	

                    <div class="cxm-content">
                      <a class="fs18 lh18 text-dark" href="#">
            			@if(!empty($data->heading) > 0)
            			{{$data->heading}}
                        @endif
          			 </a>
                      <hr class="hr1">
                      <div><span class="fs24 fc1 price">
					   @if(!empty($data->price) > 0)
						  $
						{{number_format((float)$data->price)}}
						@else
							Price Not Available
						@endif
						</span>
						</div>
						<ul class="cxm-facts fs12 bg-secondary p-2 rounded">
                        <li><span class="fa fa-modx text-primary"></span> 
						@if(!empty($data->build->year) > 0)
						{{$data->build->year}}
						@endif
						</li>
                        <li><span class="fa fa-barcode text-primary"></span> 
						@if(!empty($data->build->body_type) > 0)
						{{$data->build->body_type}}
                        @endif
						</li>
                        <li><span class="fa fa-road text-primary"></span> 
						@if(!empty($data->miles) > 0)
						{{$data->miles}}
                        @endif  miles
						</li>
                        <li><span class="fa fa-toggle-on text-primary"></span>
						@if(!empty($data->build->transmission) > 0)
						{{$data->build->transmission}}
                       @endif
						</li>
                        <li><span class="fa fa-spinner text-primary"></span> 
						
						@if(!empty($data->build->engine) > 0)
						{{$data->build->engine}}
                        @endif
						</li>
                        <li><span class="fa fa-fire text-primary"></span> 
						@if(!empty($data->build->fuel_type) > 0)
						{{$data->build->fuel_type}}
                       @endif
						</li>
                      </ul>
                      <a class="btn btn-primary btn-sm" href="{{route('ContactReuqestDetail',['car_id'=> $id])}}"><span class="fa fa-trash"></span>Requests</a>
                    
                    </div>
                    <hr class="hr1">
                  </div>                
                </div>
			
			   
			   @endforeach			   
               	</div>
               
              </div>
			  
			  <ul class="pagination justify-content-center">      
            <?php
              $links = $car_datas->render(); 
              $links = str_replace("<a", "<a class='page-link ' ", $links);
              $links = str_replace("<li", "<li class='page-item' ", $links);
              $links = str_replace("<span", "<span class='page-link'",$links);
              echo $links;
            ?>
          </ul>  
				
			@else
			<br><br>
			<h3 align="center"> No Request Found </h3>
			<br><br>
			@endif
			
		
            </div>
          </div>  
	
		    
        </div>
      </div>      
    </div>
	
	
<?php } 
else{
	echo "<br>";
	echo "<h1 align='center'>"."Registered user only to access this area"."</h1>";	
}
?>


	@endsection