@extends('layouts.web_pages')
@section('content')
<style>
.qback{
background-color:#777;
color:#fff;	
}

.qvalues{
background-color:#f2f2f2;
color:#333;	
font-weight: bold;
 border-right: 0.5px solid #e2e2e2;
}

.qvalues a{
color:#333;	
}
.qvalues a:hover{
color:#407488;
text-decoration: none;	
}

.table th, .table td {
    padding: 0.50rem;
    vertical-align: top;
    border-bottom: 0.5px dotted #e2e2e2;
	border-top: 0px solid #dee2e6; 
}
.fa-1x{
font-size: 18px;	
}

.green{
color:green;	
	
}

	.send_email{
		background: none;
		padding: 0px;
		border: none;
		}	
.send_email:hover{
		cursor: pointer;
		color:#158CBA;
		}
.height{
  height: 130px;	
}		
</style>

 
<div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>RBO QUOTE LIST</h1>            
          </div>
        </div>
      </div>      
    </div>

    <br>
	 <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
					
					  @if (session('error'))
                        <div class="alert alert-danger" width="50%">
                            {{ session('error') }}
                        </div>
                    @endif
		      </div>	
    
      <div class="container">
        <div class="form-row">
         <!-- client sidebar -->
		 
		 <div class="col-sm-2 col-md-2 col-lg-2 pull-left qback"> 
		 
		<table class="table">
				 <tr>
				 <td class="data">
				  Details
				 </td>
				 </tr>
				  <tr>
				 <td class="data">
				  Dealer
				 </td>
				 </tr>
				 
				 <tr>
				 <td class="data">
				  Expires
				 </td>
				 </tr>
				 
				 
				  <tr>
				 <td class="data height">

				 </td>
				 </tr>
				 
				 
				 
				 <tr>
				 <td class="data">
				  MSRP
				 </td>
				 </tr>
				 
				 
				 
				 <tr>
				 <td class="data">
				  Dealer Option.
				 </td>
				 </tr>
				 
				 
				 <tr>
				 <td class="data">
				  Document & Fees
				 </td>
				 </tr>
				 
				 
				 <tr>
				 <td class="data">
				  Rebates & Disccount
				 </td>
				 </tr>
				 
				 
				 
				 <tr>
				 <td class="data">
				  Sale Price
				 </td>
				 </tr>
				 
				 <tr>
				 <td class="data">
				  Action
				 </td>
				 </tr>
				 
				  <tr>
				 <td class="data">
				  Contact
				 </td>
				 </tr>
				 
				  <tr>
				 <td class="data">
				  Quote Status
				 </td>
				 </tr>
				 
				 </table>
		 <!-- client sidebar --> 
		 </div>
		 
		@if(count($get_customer_quote_list) || count($get_rbo_list) ) 
		 
			<?php
			$get_dealer_phone = DB::table('dealer')->where('dealer_id',$get_customer_quote_list[0]->dealer_id)->get()->first();
			DB::table('rbo_dealer_quote')->where('rbo_id',$get_customer_quote_list[0]->rbo_id)->update(array('viewed_by_customer' => '1'));
			?>
		
			<input type="hidden" id="dealer-phone" value="{{$get_dealer_phone->dealer_phone}}">
			<input type="hidden" id="dealer-other-phone" value="{{$get_dealer_phone->dealer_other_phone}}">
			<input type="hidden" id="dealer-id" value="{{$get_dealer_phone->dealer_id}}">
			<input type="hidden" id="user-phone" value="{{Auth::user()->phone}}">
	 
		@foreach($get_customer_quote_list as $key => $get_customer_quote) 
	
          <div class="col-sm-2 col-md-2 col-lg-2 qvalues">
              
				 <table class="table">
				 <tr>
				 <td class="data" align="center">
				 <a href="{{url('car/detail').'/'.$get_customer_quote->car_id}}" target="_blank">
				 @if(!empty($car_datas[$key]->heading) > 0){{str_limit($car_datas[$key]->heading, 20)}}@endif
				 </a>
				 </td>
				 </tr>
				  <tr>
				 <td class="data" align="center">
				 @if(!empty($get_customer_quote->dealer_name) > 0){{$get_customer_quote->dealer_name}}@endif
				 </td>
				 </tr>
				
				 <tr>
				 <td class="data" align="center" style="color:#cc0000; font-weight:bold;">
				  @if(!empty($get_customer_quote->expiry_date) > 0){{$get_customer_quote->expiry_date}}@endif
				  
				 </td>
				 </tr>
				 
				<tr>
				<td class="data" align="center">
				<div style="height: 118px; overflow:hidden;"><a href="{{url('car/detail').'/'.$get_customer_quote->car_id}}" target="_blank">
				<img src="@if(!empty($car_datas[$key]->media->photo_links[0]) > 0){{$car_datas[$key]->media->photo_links[0]}}@endif" onerror="this.src='https://stg.ottoguide.com/public/images/no-image.jpeg'" width="150px" height="115px" align="center">
				</a>
				</div>		
				</td>
				</tr>
				 
				 <tr>
				 <td class="data" align="center" style="color:#127ba3; font-weight: bold;">
				  $ @if(!empty($get_customer_quote->car_price) > 0){{$get_customer_quote->car_price}}@endif
				 </td>
				 </tr>
				 
				 
				 <?php 
				 
				 $p = 0;  $d = 0;  $ds = 0; $sum = 0; $sum2 = 0; $sum3 = 0;
				 
				// get dealer options total 
				 
				 if(!empty($get_customer_quote->dealer_price_values)){
					 
				 $dealer_option = explode(',',$get_customer_quote->dealer_price_values);
				 
				 foreach ($dealer_option as $row)
				 {
					$p++;
					$dealer_options = $sum += $row;
				 }
				 	
				
				 }
				 
				 else{
				 $dealer_options = ''; 
				 }
				 
				 

				 // get document fees total
				 
				 if(!empty($get_customer_quote->doc_fees_values)){
					 
				 $doc_fees = explode(',',$get_customer_quote->doc_fees_values);
				 
				 foreach ($doc_fees as $row2)
				 {
					$d++;
					$doc_feess = $sum2 += $row2;
				 }
				 	
				
				 }
				 
				 else{
				 $doc_feess = ''; 
				 }
				 
				// get rebate and diccount total 
				 
				 if(!empty($get_customer_quote->rebate_discount_values)){
					 
				 $rabates_discount = explode(',',$get_customer_quote->rebate_discount_values);
				 
				 foreach ($rabates_discount as $row3)
				 {
					$ds++;
					$rabates_discounts = $sum3 += $row3;
				 }
				 	
				
				 }
				 
				 else{
				 $rabates_discounts = ''; 
				 }
				 
				 
				 ?>
				 
				 
				 <tr>
				 <td class="data" align="center" style="color:#333; font-weight: normal;">
				 <b> $ <?= $dealer_options ?></b>
				 </td>
				 </tr>
				 
				 
				 <tr>
				 <td class="data" align="center" style="color:#333; font-weight: normal;">
				  <b>$ <?= $doc_feess  ?></b>
				 </td>
				 </tr>
				 
				  <tr>
				 <td class="data" align="center" style="color:#333; font-weight: normal;">
				 <b style="color:#cc0000;">  $ <?= $rabates_discounts ?></b>
				 </td>
				 </tr>
				 
				 
				 <tr>
				 <td class="data" align="center" style="color:green; font-weight: bold;">
				   $ @if(!empty($get_customer_quote->sale_price) > 0){{$get_customer_quote->sale_price}}@endif
				 </td>
				 </tr>
				 
			
				
			  @if($get_customer_quote->dealer_status == 'Accepted' OR $get_customer_quote->dealer_status == 'Rejected') 
				<tr>
				 <td class="data" align="center">
				   <i class="fa fa-check fa-1x" data-toggle="tooltip" title="Accept" disabled></i> &nbsp; &nbsp;  
				   <i class="fa fa-times fa-1x" data-toggle="tooltip" title="Reject" disabled></i>&nbsp; &nbsp;  
				   <i class="fa fa-repeat fa-1x" data-toggle="tooltip" title="Modify" disabled></i>
				 </td>
				</tr>
			  @else
				<tr>
				 <td class="data" align="center">
				 <a href="{{route('rbo-quote-form-customer-status',['quote_status'=> 'Accepted', 'rbo'=> encrypt($get_customer_quote->rbo_id)])}}"> <i class="fa fa-check fa-1x" data-toggle="tooltip" title="Accept"></i> </a> &nbsp; &nbsp;  
				 <a href="{{route('rbo-quote-form-customer-status',['quote_status'=> 'Rejected', 'rbo'=> encrypt($get_customer_quote->rbo_id)])}}"><i class="fa fa-times fa-1x" data-toggle="tooltip" title="Reject"></i> </a>&nbsp; &nbsp;  
				 <a href="{{route('rbo-quote-form-customer-view',['car'=> $get_customer_quote->car_id, 'rbo'=> encrypt($get_customer_quote->rbo_id)])}}"><i class="fa fa-repeat fa-1x" data-toggle="tooltip" title="Modify"></i> </a>
				 </td>
				</tr>   
			
			  @endif 		
				 					
									
				<form id="services_email" method="post" action="{{url('user-services-email.html')}}?email_subject= RBO - {{$get_customer_quote->rbo_id}}">
				{!! Form::hidden('_token', csrf_token()) !!}
				<input type="hidden" name="dealer_id" value="{{$get_customer_quote->dealer_id}}" class="dealer_id">
				<input type="hidden" name="dealer_name" value="{{$get_customer_quote->dealer_name}}" class="dealer_name">
				</form>		
				
			   <tr>
				 <td class="data" align="center">
				  <button type="submit" class="send_email" form="services_email"><i class="fa fa-envelope fa-1x" data-toggle="tooltip" title="Email"></i> </button> &nbsp; &nbsp;<a href="{{route('mychat', ['car_id' => $get_customer_quote->car_id, 'dealer_id'=> $get_dealer_phone->dealer_id,  'channel_name'=> $get_dealer_phone->dealer_email])}}-<?php echo Auth::user()->email; ?>"> <i class="fa fa-comments fa-1x" data-toggle="tooltip" title="Chat"></i></a> &nbsp; &nbsp; 
				  
				  <a href="javascript:services_email({{'1010101'}});" data-toggle="tooltip" title="Call" class="contact-btn" onclick="$('#num-div').show()"><i class="fa fa-phone fa-1x" data-toggle="tooltip" title="Call"></i></a>

				 </td>
				</tr>
				
				<tr>
				<td>
				<center><span style="color:#000;"><b>{{$get_customer_quote->dealer_status}}</b></span></center>
				</td>
				</tr>
				<tr>
				<td>
				<center><a href="{{route('rbo-quote-form-details',['car'=> $get_customer_quote->car_id, 'rbo'=> encrypt($get_customer_quote->rbo_id)])}}"><button class="btn btn-primary btn-small">View Quote Form</button></a> </center> </td>
				
				</tr>
				
				
				 
			<!--@if($get_customer_quote->dealer_status == 'Accepted' OR $get_customer_quote->dealer_status == 'Rejected') 
	 
				<tr>
				<td class="data" align="center">
				<i class="fa fa-hourglass-end fa-1x" data-toggle="tooltip" title="Pending" disabled></i>  &nbsp; &nbsp;
				<i class="fa fa-power-off fa-1x" data-toggle="tooltip" title="Close" disabled></i>  &nbsp; &nbsp; 
				<i class="fa fa-thumbs-up fa-1x" data-toggle="tooltip" title="Complete" disabled></i>
				</td>
				</tr>
				 
			@else	 
				 
			 <!--<tr>
				 <td class="data" align="center">
				 <a href="{{route('rbo-quote-form-customer-status',['quote_status'=> 'Pending', 'rbo'=> encrypt($get_customer_quote->rbo_id)])}}"><i class="fa fa-hourglass-end fa-1x" data-toggle="tooltip" title="Pending"></i> </a>  &nbsp; &nbsp;
				 <a href="{{route('rbo-quote-form-customer-status',['quote_status'=> 'Closed', 'rbo'=> encrypt($get_customer_quote->rbo_id)])}}"><i class="fa fa-power-off fa-1x" data-toggle="tooltip" title="Close"></i> </a> &nbsp; &nbsp; 
				 <a href="{{route('rbo-quote-form-customer-status',['quote_status'=> 'Completed', 'rbo'=> encrypt($get_customer_quote->rbo_id)])}}"><i class="fa fa-thumbs-up fa-1x" data-toggle="tooltip" title="Complete" aria-hidden="true"></i></a>
				 </td>
				 </tr>
			
			@endif  
			
			
			<!--@if($get_customer_quote->dealer_status == 'Accepted') -->

			
			<!--@else

			 <tr>
				 <td class="data" align="center">
				  <i class="fa fa-envelope fa-1x" data-toggle="tooltip" title="Email"></i>  &nbsp; &nbsp;
				  <i class="fa fa-comments fa-1x" data-toggle="tooltip" title="Chat"></i> &nbsp; &nbsp; 
				  <i class="fa fa-phone fa-1x" data-toggle="tooltip" title="Call"></i>
				 </td>
				 </tr>	
				
			@endif --> 		
				 
				 </table>
				 
			 </div>
         
			 @endforeach
			 
			 
			 
		   @foreach($get_rbo_list as $key => $get_customer_quote) 
	
            <div class="col-sm-2 col-md-2 col-lg-2 qvalues">
              
				 <table class="table">
				 <tr>
				 <td class="data" align="center">
				 <a href="{{url('car/detail').'/'.$get_customer_quote->car_id}}" target="_blank">
				 @if(!empty($rbo_count_data[$key]->heading) > 0){{str_limit($rbo_count_data[$key]->heading, 20)}}@endif
				 </a>
				 </td>
				 </tr>
				 <tr>
				 <td class="data" align="center">
				 @if(!empty($get_customer_quote->dealer_name) > 0){{$get_customer_quote->dealer_name}}@endif
				 </td>
				 </tr>
			
				 
				 <tr>
				<td class="data" align="center" >
				<div style="height: 127px; overflow:hidden;">
				<a href="{{url('car/detail').'/'.$get_customer_quote->car_id}}" target="_blank">
				  <img src="@if(!empty($rbo_count_data[$key]->media->photo_links[0]) > 0){{$rbo_count_data[$key]->media->photo_links[0]}}@endif" width="150px" align="center">
				</a>
				</div>		
				</td>
				 </tr>
				 
				 
				 <tr>
				 <td class="data" align="center" style="color:#127ba3; font-weight: bold;">
				  $ @if(!empty($rbo_count_data[$key]->price) > 0){{$rbo_count_data[$key]->price}}@endif
				 </td>
				 </tr>
				 
				 
				 <tr>
				 <td>
				 <h6 align="center"style="color:#ff6600;"><b> (Pending) </b></h6>
				 </td>
				 </tr>
			 
				 </table>
				 
				  
			 </div>
			 
			 @endforeach
			  
			 @endif
                    
					
					
					
					
        </div>
      </div>    
	  
	  
	  
	<!-- Modal -->
<div class="modal fade" id="user-contact" data-backdrop="static"  data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="contact-modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contact-modal">Dealer Contact</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <center>
                    @auth
                        <input type="text" class="form-control" value="{{Auth::user()->mobile}}" /><br>
                        <a href="javascript:void(0);" style="width: 50%;" class="call-button btn btn-block btn-primary">
                            <i class="fa fa-arrow-right "></i> Go</a>
                        <div id="div1"></div>
                    @endauth
                </center>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
	
	
	<script>
	
	 function services_email(dealerid){

      $('#user-contact').modal('show');
    }

    $(document).ready(function(){

        var callStatus = '';
        var callSid = '';
    $(".call-button").click(function(){
        $(this).html('Connecting...');
        var user_phone = $('#user-phone').val();
        var dealer_phone = $('#dealer-phone').val(); 
		var dealer_id = $('#dealer-id').val();
		var other_phone = $('#dealer-other-phone').val();
        $.ajax({
            url: "{{url('twilio-call-chat/call.php')}}",
            type: "POST",
            data: {id: 1, user_phone:user_phone,dealer_phone:dealer_phone,dealer_id:dealer_id,other_phone:other_phone},
            dataType: 'json',
            error: function(xhr, status, error) {
                console.log(xhr.status + status+error);
            },
            success: function(result){
                console.log(result.call_sid);
                callSid = result.call_sid;
                callStatus = setInterval(function () {
                    checkCallStatus(result.call_sid)
                }, 2000);
            }
        });
    });

        function checkCallStatus(sid){

            $.ajax({
                url: "{{url('twilio-call-chat/call_status.php')}}",
                type: "POST",
                data: {call_sid: sid},
                dataType: 'json',
                success: function(result){

                    console.log(result.call_status);
                    if(result.call_status == 'queued' || result.call_status == 'initiated'){
                        $(".call-button").html('Connecting...');
                    }else if(result.call_status == 'ringing'){
                        $(".call-button").html('Ringing...');
                    }
                    else if(result.call_status == 'in-progress'){
                        $(".call-button").html('Connected!');
                    }
                    else if(result.call_status == 'busy' || result.call_status == 'failed' || result.call_status == 'no-answer'){
                        $(".call-button").html('Call Failed');
                        stopFunction();
                    }
                    else if(result.call_status == 'completed'){
                        $(".call-button").html('Call Again');
                        callSid = '';
                        stopFunction();
                    }
                }
            });
        }


        function stopFunction(){
            clearInterval(callStatus);
        }

	 });



    </script>

	  
  <br>
  
  
  <script type="text/javascript">
$(document).ready(function(){    
    //Check if the current URL contains '#'
    if(document.URL.indexOf("#")==-1){
        // Set the URL to whatever it was plus "#".
        url = document.URL+"#";
        location = "#";

        //Reload the page
        location.reload(true);
    }
});
</script>
  
  
	 <script>
	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip(); 
	});
	</script>
  
  
    @endsection