@extends('layouts.web_pages')
@section('content')


<div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Dashboard</h1>            
          </div>
        </div>
      </div>      
    </div>
    <br>
	 <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
					
					  @if (session('error'))
                        <div class="alert alert-danger" width="50%">
                            {{ session('error') }}
                        </div>
                    @endif
		      </div>	
    <div class="py-5">
      <div class="container">
        <div class="form-row">
         <!-- client sidebar -->
		 @include('client.client_sidebar')
		 <!-- client sidebar --> 
		 
          <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="card">
              <div class="card-header font-weight-bold fs18">Request for unblock</div>
              <div class="card-body">
                <div class="form-row">
                  
				  <div class="col-sm-10 col-md-10 col-lg-10">
				  
	     <form id="email" action="{{url('unblock_request_send.html')}}" method="post">
		 {!! Form::hidden('_token', csrf_token()) !!}
          <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
			  <label>To: </label>
			  <input class="form-control" name="admin" value="OttoGuide Administration">

              </div>
              <div class="form-group">
			   <label>Subject: </label>
				<select id="unblock-subject" name="unblock-subject" class="form-control">
					
				<option disabled selected>Select Subject</option>	
				<option value="chat_unblock">Unblock Chat </option>
				<option value="call_unblock">Unblock Call </option>	
			    <option value="reviews_unblock">Unblock Reviews </option>  

				</select>
              </div>
              
			   <div class="form-group">
			   <label>Message: </label>
                <textarea class="form-control" name="message" style="height:300px; padding:15px;" placeholder="" required></textarea>
              </div>
              <input type="hidden" name="dealer_id" >
            
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i>Send</button>
              </div>
            </div>
			<br>
		<br>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
		
	   </form>  
				  
				  </div>
                 
                </div>
              </div>
            </div>
          </div>          
                    
        </div>
      </div>      
    </div>
    @endsection