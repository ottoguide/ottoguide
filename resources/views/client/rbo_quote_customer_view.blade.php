@extends('layouts.web_pages')
@section('content')
<style>

		.table thead tr {
		background-color: #555;
		color: #fff;
		font-weight: bold;
		text-align: center;
		border: 1px solid #ccc;
		}
		
		.data{
		background-color: #666;
	    color: #fff;
        font-weight: 600;		
		}
		
		tr{
		background-color: #f2f2f2;
	    color: #111;
        font-weight: 400;		
		}
		
		
		.table th, .table td {
		padding: 0.45rem;
		vertical-align: top;
		}
		
		.padding-left{
		padding-left: 0px;
		padding-right: 0px;
		border:1px solid #ccc;
		 margin-bottom: 0rem;
		}
		.height{
			line-height:3;
		}
		.height-fix{
			height: 130px;
		}
		.table {
     margin-bottom: 0rem;
		}
		
		.cxm-img IMG{
			border-radius: 5px;
		}
		
		hr{
		border-bottom: 0.5px dotted #ccc;	
		}
		
		#addMore{
		margin-top:3px;	
		}
		.quote_form{
			margin-right: 10px;
		}
		
		.table th, .table td {
		padding: 0.20rem;
		vertical-align: top;
		border: 1px solid #d4d9de;
		padding-left: 10px;
		}

		
		input[class='form-controll'] {
		  background: #fff;
		  border:1px solid #ccc;
		  font-size: 16px;
		  color: #0c7096;
		  font-weight: bold;
		  text-align:center;
		  border-radius: 5px;
		}
		
	  
	  .table thead th {
		background-color: rgb(156, 156, 156);
		color: #fff;
		font-weight: bold;
		text-align: center;
		}
	
	   .separator{
		   border-top:1px solid #ccc;
		   padding-top:1px;
		   padding-bottom:1px;
		   background-color:#fff;
	   }
		#data-table{
		background: #ffffff; /* Old browsers */
		background: -moz-linear-gradient(top, #ffffff 0%, #f3f3f3 50%, #ededed 51%, #ffffff 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #ffffff 0%,#f3f3f3 50%,#ededed 51%,#ffffff 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #ffffff 0%,#f3f3f3 50%,#ededed 51%,#ffffff 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */	
		}
		.fa-2x{
		padding-left: 20px;		
		}
		.send_form{
		background: none;
		padding: 0px;
		border: none;	
		}
		
	.fa-1x{
	-webkit- transition: width 2s linear 1s;
    transition:  transition: width 2s linear 1s;	
	font-size: 17px;
	color:#555;
	}
	.fa-1x:hover{
	-webkit- transition: width 2s linear 1s;
    transition:  transition: width 2s linear 1s;	
	font-size: 19px;
	color:#222;
	cursor: pointer;	
	}
	
	.active{
	color:green;
	font-size: 19px;	
	}	
	
   .dactive{
	color:red;
	font-size: 19px;	
	}		
   
   .modify{
	color:orange;
	font-size: 19px;	
	}

  .fa{
	  color:#555;
  }	
		
		
	@keyframes page-load {
    from {
        background-color: #ffc422;
    }
    to {
        background-color: #c0392b;
    }
}
.page-loading::before {
    content:" ";
    display:block;
    position:fixed;
    z-index:10;
    height:2px;
    width:100%;
    top:0;
    left:0;
    background-color:#06D;
    animation: page-load infinite ease-out 2s;
    box-shadow:0 2px 2px rgba(0,0,0,.2);
}	

a:hover{
	text-decoration: none;
}
</style>
<?php if(Auth::user()){ ?>

 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>RBO Quote Customer View (RBO # <?=$view_quote_form->rbo_id?>)</h1>            
          </div>
        </div>
      </div>      
    </div>
    
	
    <div>
		<div class="container">	 
	   
	    <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
	   
		</div>
    <div class="container">
      <div class="form-row">   
    	
		
		<div class="col-sm-12 col-md-12 col-lg-12">
		  
            <div class="card-body">
			 
              <div class="cxm-advert-item mb-3" >            
                  <div class="form-row">
                    <div class="col-sm-3">
                      <div class="cxm-img">
                        <a href="#"><img class="img-fluid" width="100%" src="@if(!empty($car_datas->media->photo_links[0]) > 0){{$car_datas->media->photo_links[0]}}@endif"></a>
                      </div>
                    </div>
                    <div class="col-sm-9">
                    <div class="cxm-content">
			   
                    <div>
					
					
				    <div class="text-primary font-weight-bold fs18">@if(!empty($car_datas->heading) > 0){{$car_datas->heading}}@endif</div>
                   
				   <span class="fc1 fs24">MSRP: $@if(!empty($car_datas->ref_price) > 0){{$car_datas->ref_price}}@endif</span></div>
				   
				   <ul class="cxm-facts fs12 bg-secondary p-2 rounded">
                        <li><span class="fa fa-modx text-primary"></span> 
				       @if(!empty($car_datas->build->year) > 0){{$car_datas->build->year}}@endif
												</li>
                        <li><span class="fa fa-barcode text-primary"></span> 
					    @if(!empty($car_datas->build->make) > 0){{$car_datas->build->make}}@endif 
						</li>
                        
						<li><span class="fa fa-road text-primary"></span> 
						@if(!empty($car_datas->miles) > 0){{$car_datas->miles}}@endif  miles

						</li>
                        <li><span class="fa fa-toggle-on text-primary"></span>
												Automatic
                       						</li>
											<br><br>
                        <li><span class="fa fa-spinner text-primary"></span> 
						
							@if(!empty($car_datas->build->engine) > 0){{$car_datas->build->engine}}@endif
                       						</li>
                        <li><span class="fa fa-fire text-primary"></span> 
							@if(!empty($car_datas->build->fuel_type) > 0){{$car_datas->build->fuel_type}}@endif
                       	</li>
                      </ul>
					
					</div>
					
					
                    </div>
                  </div>          
                </div>
          
              </div>
		 
		
		   </div>
		
		
		
		
		
		  <div class="col-sm-12 col-md-12 col-lg-12">
		  
		  <form action="{{route('rbo-quote-form-customer-submit')}}" method="post" >
                                       
       {!! Form::hidden('_token', csrf_token()) !!}
			
		    <table width="80%" id="data-table" class="table  table-border">
                            <thead>
                            
							<th>Details</th>
							<th>Dealer Quote</th>
							<th align="center">Action</th>
							<th>Your Offer</th>
							
                            </thead>
                            <tbody>
							
							<tr>
							<td><b>MSRP</b></td>
							<td align="center" style="font-size:16px;"><b>$@if(!empty($car_datas->ref_price) > 0){{$car_datas->ref_price}}@endif</b></td>
							<td></td>
							<td align="center" style="font-size:16px;"><b>$@if(!empty($car_datas->ref_price) > 0){{$car_datas->ref_price}}@endif</b></td>
						
							</tr>
							
								
					        <tr>
							<td><b>Expire</b></td>
							<td align="center" style="font-size:16px;"><b></b></td>
							<td align="center"><b><i class="fa fa-clock-o" aria-hidden="true"></i>  {{$view_quote_form->expiry_date}}</b></td>
							<td align="center" style="font-size:16px;"><b></b></td>
						
							</tr>
							
							
							<tr>

							<td><b>Dealer Option</b></td>
							<td align="center" class="dealer_option"><b>
						<?php
						$get_dealer_option_description = explode(',',$view_quote_form->dealer_price_description);
						$get_dealer_option_value = explode(',',$view_quote_form->dealer_price_values);
						  
					  $i = 0;
					  $sum = 0;  
					 
					 foreach ( $get_dealer_option_value as $row)
					 { 
						$i++; 
						$dealer_option = $sum += $row; 	
					 } 
					 
					 echo "$". $dealer_option;
				     ?>
							</b>
							
							</td>
							<td></td>
							<td align="center"><b></b></td>
							</tr>
					
				
					
					
				<?php 
				
				$get_customer_price_values = explode(',',$view_quote_form->customer_price_values);
				
				foreach($get_dealer_option_description as $key => $description){ ?>
							<tr>
							<td><?= $description ?></td>
							<td align="center">$<?= $get_dealer_option_value[$key] ?></td>
							<td align="center"> <i class="fa fa-check fa-1x" aria-hidden="true" data-toggle="tooltip" title="Accept"></i>&nbsp; &nbsp; <i class="fa fa-times fa-1x" aria-hidden="true" data-toggle="tooltip" title="Reject"></i>&nbsp; &nbsp; <i class="fa fa-repeat fa-1x" aria-hidden="true" data-toggle="tooltip" title="Modify"></i></td>
							<td align="center">$ <input type="number" name="dealer_options_value[]" value="@if(!empty($get_customer_price_values[$key]) > 0){{$get_customer_price_values[$key]}}@endif" class="form-controll" ></td>
							</tr>
				
				<?php } ?>			
							
				<tr class="separator"><td></td><td></td><td></td><td></td></tr>
				
					<tr class="doc_fee_total">
					<td><b>Document Fees</b></td>
					<td align="center" ><b>
					 <?php
					 $get_doc_fees_description = explode(',',$view_quote_form->doc_fees_description);
				     $get_doc_fees_value = explode(',',$view_quote_form->doc_fees_values);	
					 
					 $i2 = 0;
					 $sum2 = 0;  
					 
					 foreach ($get_doc_fees_value as $row2)
					 { 
						$i2++; 
						$doc_fees_value = $sum2 += $row2; 	
					 } 
					 
					 echo "$". $doc_fees_value;
				  ?>
					</b></td>
					<td></td>
					<td align="center"><b></b></td>
					</tr>
				
				<?php 
				$get_customer_doc_fees = explode(',',$view_quote_form->customer_doc_fees_values);
				foreach($get_doc_fees_description as $key => $description){ ?>
				
						    <tr>
							<td><?= $description ?></td>
							<td align="center">$<?= $get_doc_fees_value[$key] ?></td>
							<td align="center"> <i class="fa fa-check fa-1x" aria-hidden="true" data-toggle="tooltip" title="Accept"></i>&nbsp; &nbsp; <i class="fa fa-times fa-1x" aria-hidden="true" data-toggle="tooltip" title="Reject"></i>&nbsp; &nbsp; <i class="fa fa-repeat fa-1x" aria-hidden="true" data-toggle="tooltip" title="Modify"></i></td>
							<td align="center"> $ <input type="number" name="doc_fees_value[]" value="@if(!empty($get_customer_doc_fees[$key]) > 0){{$get_customer_doc_fees[$key]}}@endif" class="form-controll" required></td>
							</tr>
				
				<?php } ?>						
					
				<tr class="separator"><td></td><td></td><td></td><td></td></tr>
					
				    <tr>
					<td><b>Rebates & Discounts</b></td>
					<td align="center"><b>
					
					<?php 
					$get_rebate_discount_description = explode(',',$view_quote_form->rebate_discount_description);
					$get_rebate_discount_values = explode(',',$view_quote_form->rebate_discount_values);
					
					 $i3 = 0;
					 $sum3 = 0;  
					 
					 foreach ($get_rebate_discount_values as $row3)
					 { 
						$i3++; 
						$rebate_discount_values = $sum3 += $row3; 	
					 } 
					 
					 echo "$". $rebate_discount_values;
					
					?>
					</b></td>
					<td></td>
					<td align="center"><b></b></td>
					</tr>
								

					
				<?php 
				
				$get_cutomer_rebate_discount_value = explode(',',$view_quote_form->customer_rebate_discount_values);
				
				foreach($get_rebate_discount_description as $key => $description){ ?>
				
						    <tr>
							<td><?= $description ?></td>
							<td align="center">$<?= $get_rebate_discount_values[$key] ?></td>
							<td align="center"> <i class="fa fa-check fa-1x" aria-hidden="true" data-toggle="tooltip" title="Accept"></i>&nbsp; &nbsp; <i class="fa fa-times fa-1x" aria-hidden="true" data-toggle="tooltip" title="Reject"></i>&nbsp; &nbsp; <i class="fa fa-repeat fa-1x" aria-hidden="true" data-toggle="tooltip" title="Modify"></i></td>
							<td align="center"> $ <input type="number" name="rebate_discount_value[]" value="@if(!empty($get_cutomer_rebate_discount_value[$key]) > 0){{$get_cutomer_rebate_discount_value[$key]}}@endif" class="form-controll" required></td>
							</tr>
				
				<?php } ?>	

			     <tr class="separator"><td></td><td></td><td></td><td></td></tr>

				<!--<tr>
					<td><b style="font-size:16px; color:green;">Sales Price</b></td>
					<td align="center"><b style="font-size:16px; color:green;">${{$view_quote_form->sale_price}} + TTTI</b></td>
					<td></td>
					<td align="center"></td>
					</tr>-->
					
					<!--<tr>
					<td><b style="font-size:16px; color:#cc0000;">Total Saving</b></td>
					<td align="center"><b style="font-size:16px; color:#cc0000;">
					
					<?php//  echo "$". $rebate_discount_values; ?>
					
					</b></td>
					<td></td>
					<td align="center"></td>
					</tr>-->
					
					
					<tr style="background-color:#666;">
					<td><b style="font-size:16px; color:#fff;">Sales Price </b></td>
					<td align="center" style="font-size:16px; color:#fff;"><b>
					<?php  $net_selling_price = $view_quote_form->sale_price ;
							echo "$". $net_selling_price;
					?>
					</b></td>
					<td></td>
					<td align="center"></td>
					</tr>
					
					
				  </tbody>
              </table>
			
           <br>
		   
		    <div class="row test">
                                <div class="col-sm-9">
                                   <h3>Customer Options </h3>
                                </div>
                          

                                <div class="col-sm-3">
                                  
									<button id="addMore" class="btn btn-primary pull-right "style="color:#fff;">Add Options</button>
                                </div>
								
								
							</div>	
							<hr>
							
							
						<div class="row">
							<div class="col-sm-5"><b>Customer Option Description</b></div>
							<div class="col-sm-5"><b>Customer Option Values</b></div>
							</div>	
							<hr>	
							
		
		  <div class="quote">
		  
		  <?php  
		    $get_cutomer_option_description = explode(',',$view_quote_form->customer_options_description);
			$get_cutomer_option_values = explode(',',$view_quote_form->customer_options_value);

				foreach($get_cutomer_option_description as $key => $description){ ?>
		  
				@if($description !=='')
			
				
					 <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                       
                                        <input type="text" class="form-control" name="customer_options_description[]" value="@if(!empty($description) > 0){{$description}}@endif" id="dealer_options_description">
                                    </div>
                                </div>
                          

                                <div class="col-sm-5">
                                    <div class="form-group">
                                    
                                        <!--<input type="file" class="form-control" id="pdf_report" name="pdf_report">-->
								   
								  <input type="text" class="form-control" name="customer_options_value[]" value="@if(!empty($get_cutomer_option_values[$key]) > 0){{$get_cutomer_option_values[$key]}}@endif" id="dealer_options_value">
                                    </div>
                                </div>
								
								<div class="col-sm-1 minus pull-right"><a type="button" class="btn btn-danger btn-sm remove_more_fld"><i class="fa fa-times"></i></a></div>
							
							</div>	
					
					@endif		
							
				<?php }?>
		  
							<script>
								$(function() {
									$("#addMore").click(function(e) {
										e.preventDefault();
										var count = $('.js-from_add_field').length;
									
										var final_html = "<div class='row dark js-from_add_field'>";
										final_html += ("<div class='col-sm-6'><div class='form-group'><input type='text' class='form-control' placeholder='Customer Option Description' name='customer_options_description[]'></div></div>");
										final_html += ("<div class='col-sm-5'><div class='form-group'><input type='number'  class='form-control' placeholder='Customer Option Values' child_report_files' name='customer_options_value[]'></div></div>");
										final_html += '<br><div class="col-sm-1 minus"><a type="button" class="btn btn-danger btn-sm remove_more_fld"><i class="fa fa-times"></i></a></div>';
										final_html += '</div>';
										$(".quote").append(final_html);
									});

									$(document).on('click', '.remove_more_fld', function (e) {
										e.stopImmediatePropagation();

										$(this).parent().parent().remove();
									});
									
									$(document).on('change', '.child_report_files', function() {
										
										if ($(this)[0].files.length > 0) {
											$(this).next('.check_file').val('1');
										}
									
									})
								});
                                </script>
							
							
						
							</div>  
							
							
						   <table width="80%" id="data-table" class="table  table-border">
							<tr>
							<td width="30%"><h6 style="padding-top:5px;">(Tax, Title, Tag, Insurance)*</h6></td>
							<td align="center">
							<!--<a href="{{route('rbo-quote-form-customer-status', ['rbo' => encrypt($view_quote_form->rbo_id),'quote_status' => 'Pending'])}}"><i class="fa fa-hourglass-end fa-2x" data-toggle="tooltip" title="" data-original-title="Pending"></i></a> &nbsp; &nbsp;--> 
							<!--<a href="{{route('rbo-quote-form-customer-status', ['rbo' => encrypt($view_quote_form->rbo_id),'quote_status' => 'Closed'])}}"><i class="fa fa-power-off fa-2x" data-toggle="tooltip" title="" data-original-title="Closed"></i></a> &nbsp; &nbsp; -->
							<a href="{{route('rbo-quote-form-customer-status', ['rbo' => encrypt($view_quote_form->rbo_id),'quote_status' => 'Accepted'])}}">
							<i class="fa fa-thumbs-up fa-2x" aria-hidden="true"data-toggle="tooltip" title="" data-original-title="Accept"></i>&nbsp; &nbsp;</a>       
							<button type="submit" class="send_form"><a href=""><i class="fa fa-save fa-2x" data-toggle="tooltip" title="" data-original-title="Save"></i></a></button>&nbsp; &nbsp;
							<button type="submit" class="send_form"><a href=""><i class="fa fa-paper-plane fa-2x" data-toggle="tooltip" title="" data-original-title="Send"></i></a></button> &nbsp; &nbsp;
							<a href="{{route('rbo-quote-form-customer-status', ['rbo' => encrypt($view_quote_form->rbo_id),'quote_status' => 'Delete'])}}"> <i class="fa fa-recycle fa-2x" data-toggle="tooltip" title="" data-original-title="Delete"></i></a>
							
							<a href="{{url('request-best-offer-quote-list.html')}}"class="pull-right btn btn-primary"> Back To RBO List</a></td>

							
							</tr>
							
						   </table>	
						   	  <br>
						   
			        <br>				
							
					<!--<button type="submit" value="Submit Quote" class="btn btn-primary">Submit Quote</button>-->
					
			
						<br><br>
							
					  </div>		
							
		  
        </div>
      </div>      
    </div>
	
						
		<!-- Hidden Fields -->
		<input type="hidden" class="form-control" value="{{$view_quote_form->car_price}}" name="car_price" >
		<input type="hidden" class="form-control" value="{{$view_quote_form->dealer_id}}" name="dealer_id" >
		<input type="hidden" class="form-control" value="{{$view_quote_form->rbo_id}}" name="rbo_id" >
		<input type="hidden" class="form-control" value="{{$view_quote_form->car_id}}" name="car_id" >	
		<input type="hidden" name="submit_status" class="submit_status">
	</form>						

	<script>
	 $(document).on("click",".fa-save",function() {
        $('.submit_status').val('Saved')
     });
	 
	
	$(document).on("click",".fa-check",function() {
		
	var dealer_option = $(this).parents('tr').find('td:nth-child(2)').text();
	
	$(this).parents('tr').find('.form-controll').val(dealer_option.split('$')[1]);
	
	
	});
	
	
	$(document).on("click",".fa-times",function() {
	
	var doc_fees = '0';
	
	$(this).parents('tr').find('.form-controll').val(doc_fees);
	
	
	});
	
	
	$(document).on("click",".fa-repeat",function() {
	
	$(this).parents('tr').find('.form-controll').focus();
	
	$(this).parents('tr').find('.form-controll').val('');
	
	
	});	
	 
	 
	</script>

	
	 <script>
	$(document).ready(function(){
		
		window.addEventListener("beforeunload",function(e){
    document.body.className = "page-loading";
},false);
		
	  $('[data-toggle="tooltip"]').tooltip(); 
	});
	</script>
	
	

	
								
<?php } 
else{
	echo "CLient ARea";	
}
?>





	@endsection