
@extends('layouts.web_pages')
@section('content')

<style>
.table-responsive{
overflow-x: hidden;	
border-bottom: 1px solid #ccc;
background-color: #f2f2f2;
margin-bottom:15px;
}
.table-responsive td{
padding: 0.50rem;	
}

</style>

<?php if(Auth::user()){ ?>

 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>My Saved Dealers</h1>            
          </div>
        </div>
      </div>      
    </div>
  
  
@if(!empty($dealer_datas) > 0)


  
    <div class="py-5">
		<div class="container">	 
	    <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
		</div>
    <div class="container">
      <div class="form-row">   
    		 <!-- client sidebar -->
    		  @include('client.client_sidebar')
    		 
    		 <!-- client sidebar --> 
		 
		
          <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="card">
              <div class="card-header">
                <div class="row no-gutters">
                  <div class="col-md-4 text-center text-md-left font-weight-bold fs18">My Saved Dealers</div>
                  <div class="col-md-8">
                    <div class="text-center text-md-right">
                   
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
               
                <div class="row">				
        		
				
			

				
				
			@foreach($dealer_datas as  $dealer_detail)
			

				<div class="table-responsive">
				<table width="75%" border="0" class="table">
				<tr> 
				<td width="15%">Dealer Name :</td>
				<td width="25%"><b>{{isset($dealer_detail[0]->dealer_name) ? $dealer_detail[0]->dealer_name : '' }}</b></td>
				<td rowspan="6" width="30%">
				<iframe src = "https://maps.google.com/maps?q={{isset($dealer_detail[0]->latitude) ? $dealer_detail[0]->latitude : '' }},{{isset($dealer_detail[0]->longitude) ? $dealer_detail[0]->longitude : '' }}&hl=es;z=8&amp;output=embed" frameBorder="0" height="100%" width="100%"></iframe>
				</td>
				<td rowspan="6" width="20%">
				<div class="row">
				<div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="{{route('delete_my_dealer',['dealer_id'=> isset($dealer_detail[0]->dealer_name) ? $dealer_detail[0]->dealer_name : ''])}}"><span class="fa fa-clone"></span> Delete </a></div>
				</div>
				<div class="row">
				<div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="#"><span class="fa fa-comment"></span> Chat</a></div>
				</div>            
				</td>	
				</tr>
				<tr> 
				<td width="15%">Inventory Url</td>
				<td width="25%"><b>{{isset($dealer_detail[0]->dealer_website) ? $dealer_detail[0]->dealer_website : '' }}
				<tr> 
				<td width="15%">Dealer Contact:</td>
				<td width="25%"><b>{{isset($dealer_detail[0]->dealer_phone) ? $dealer_detail[0]->dealer_phone : '' }}</b></td>
				</tr>	
				<tr> 
				<td width="15%">City</td>
				<td width="25%"><b>{{isset($dealer_detail[0]->dealer_city) ? $dealer_detail[0]->dealer_city : '' }}</b> </td>
				</tr>
			  <tr> 
				<td width="15%">Street</td>
				<td width="25%"><b>{{isset($dealer_detail[0]->dealer_street) ? $dealer_detail[0]->dealer_street : '' }}</b></td>
			  </tr>
			  <tr> 
				<td width="15%">Zip</td>
				<td width="25%"><b>{{isset($dealer_detail[0]->dealer_zip) ? $dealer_detail[0]->dealer_zip : '' }}</b></td>
			  </tr>
			</table>
				
			</div>
		   
			@endforeach
              </div>
			  
				<ul class="pagination justify-content-center">      
						<?php
						  $links = $dealer_datas->render(); 
						  $links = str_replace("<a", "<a class='page-link ' ", $links);
						  $links = str_replace("<li", "<li class='page-item' ", $links);
						  $links = str_replace("<span", "<span class='page-link'",$links);
						  echo $links;
						?>
				 </ul>  
				  
              
            
                @else
			<br><br>
			<h3 align="center"> No Dealer data Found </h3>
			<br><br>
			@endif
              
			  
			  
			  
            </div>
          </div>  
	
		  
                    
        </div>
      </div>      
    </div>
<?php } 
else{
	echo "<br>"; echo "<br>"; echo "<br>";	
	echo "<p align='center'>"."Client Area"."</p>";	
}
?>


	@endsection