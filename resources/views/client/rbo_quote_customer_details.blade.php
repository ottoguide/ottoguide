@extends('layouts.web_pages')
@section('content')

    <style>
	
        .card-inner {
            margin-left: 2rem;
        }

        .p-0 {
            padding-left: 0px;
        }

        .b-0 {
            margin-bottom: 0px;
        }

        .fa-star {
            color: #ffc107 !important;
        }

        .border-top {
            border-top: 1px solid #ccc;
            margin-bottom: 5px;

        }

        .twitter-share-button {
            margin-bottom: -5px;
        }

        .card-body {
            padding: 0.025rem;
        }

        #myModal {
            margin-top: 20%;
        }
 
		.nice-ul {
			position: relative;
			padding-left: 32px;
			list-style-type: none;
		}
		.nice-ul li {
			margin-bottom: 8px;
		}
		.nice-ul li:last-child {
			margin-bottom: 0;
		}
		.nice-ul li::before {
			content: "\2713";
			position: absolute;
			left: 0;
			padding: 2px 8px;
			font-size: 1em;
			color: #1C90F3;
		}
		 
		.nice-ol {
			position: relative;
			padding-left: 32px;
			list-style-type: none;
			margin-left: 5%;
		}
		.nice-ol li {
			counter-increment: step-counter;
			margin-bottom: 6px;
			padding: 8px;
		}
		.nice-ol li:last-child {
			margin-bottom: 0;
		}
		.nice-ol li::before {
			content: counter(step-counter);
			position: absolute;
			left: 0;
			padding: 3px 8px;
			font-size: 0.9em;
			color: white;
			font-weight: bold;
			background-color: #217C9D;
			border-radius: 50%;
		}
		.square-box{
		border:1px solid #999;		
		}
		.description-box{
		margin-left: 10px;
		padding: 5px;	
			
		}
		small{
		margin-left: 10px;	
		color:#222;
		}
		.border{
		border:1px solid #ccc;
		}
		.items{
		padding-left:0px;
		padding-right:0px;		
		}
		
		.heading{
		 background-color: #217C9D;
		/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1f6591+1,2492a5+100 */
		background: #1f6591; /* Old browsers */
		background: -moz-linear-gradient(top, #1f6591 1%, #2492a5 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #1f6591 1%,#2492a5 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #1f6591 1%,#2492a5 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1f6591', endColorstr='#2492a5',GradientType=0 ); /* IE6-9 */
		padding:6px;
		color:#fff;
		font-size: 16px;
		font-weight: bold;
		}
		
		.block-info{
		padding: 10px;
		border-bottom: 1px solid #ccc;
		padding-bottom: 30px;	
		}
		
		.width{
		width: 95%;
		margin-left:5%;		
		}
		
		h5{
		font-weight: bold;
		color:#555;	
		}
		
		.cxm-img IMG{
			border-radius: 5px;
		}
		
		hr{
		border-bottom: 0.5px dotted #ccc;	
		}
		
		#addMore{
		margin-top:8px;	
		}
		.quote_form{
			margin-right: 10px;
		}
		
		.table th, .table td {
		padding: 0.20rem;
		vertical-align: top;
		border: 1px solid #d4d9de;
		}
		
	  
	  .table thead th {
		background-color: rgb(156, 156, 156);
		color: #fff;
		font-weight: bold;
		text-align: center;
		}
	
	   .separator{
		   border-top:1px solid #ccc;
		   padding-top:1px;
		   padding-bottom:1px;
		   background-color:#fff;
	   }
		#data-table{
		background: #ffffff; /* Old browsers */
		background: -moz-linear-gradient(top, #ffffff 0%, #f3f3f3 50%, #ededed 51%, #ffffff 100%); /* FF3.6-15 */
		background: -webkit-linear-gradient(top, #ffffff 0%,#f3f3f3 50%,#ededed 51%,#ffffff 100%); /* Chrome10-25,Safari5.1-6 */
		background: linear-gradient(to bottom, #ffffff 0%,#f3f3f3 50%,#ededed 51%,#ffffff 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */	
		}
		</style>


		<?php  
		DB::table('rbo_dealer_quote')->where('rbo_id',$view_quote_form->rbo_id)->update(array('viewed_by_customer' => 1));
		?>

		
		
   <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>RBO QUOTE DETAILS (RBO # <?=$view_quote_form->rbo_id?>)</h1>            
          </div>
        </div>
      </div>      
    </div>
        
		<div class="container">
         
            <br>

            <div class="message" width="50%" align="center">
                @if (session('message'))
                    <div class="alert alert-success" width="50%">
                        {{ session('message') }}
                    </div>
                @endif
            </div>

			<br>
			
		<div class="row">
	
	
	<div class="col-sm-12 col-md-12 col-lg-12">
		  
            <div class="card-body">
			 
              <div class="cxm-advert-item mb-3" >            
                  <div class="form-row">
                    <div class="col-sm-3">
                      <div class="cxm-img">
                        <a href="#"><img class="img-fluid" width="100%" src="@if(!empty($car_datas->media->photo_links[0]) > 0){{$car_datas->media->photo_links[0]}}@endif"></a>
                      </div>
                    </div>
                    <div class="col-sm-9">
                    <div class="cxm-content">
			   
                    <div>
					
					
				    <div class="text-primary font-weight-bold fs18">@if(!empty($car_datas->heading) > 0){{$car_datas->heading}}@endif</div>
                   
				   <span class="fc1 fs24">MSRP: $@if(!empty($car_datas->ref_price) > 0){{$car_datas->ref_price}}@endif</span></div>
				   
				   <ul class="cxm-facts fs12 bg-secondary p-2 rounded">
                        <li><span class="fa fa-modx text-primary"></span> 
				       @if(!empty($car_datas->build->year) > 0){{$car_datas->build->year}}@endif
												</li>
                        <li><span class="fa fa-barcode text-primary"></span> 
					    @if(!empty($car_datas->build->make) > 0){{$car_datas->build->make}}@endif 
						</li>
                        
						<li><span class="fa fa-road text-primary"></span> 
						@if(!empty($car_datas->miles) > 0){{$car_datas->miles}}@endif  miles

						</li>
                        <li><span class="fa fa-toggle-on text-primary"></span>
												Automatic
                       						</li>
											<br><br>
                        <li><span class="fa fa-spinner text-primary"></span> 
						
							@if(!empty($car_datas->build->engine) > 0){{$car_datas->build->engine}}@endif
                       						</li>
                        <li><span class="fa fa-fire text-primary"></span> 
							@if(!empty($car_datas->build->fuel_type) > 0){{$car_datas->build->fuel_type}}@endif
                       	</li>
                      </ul>
					
					</div>
					
					
                    </div>
                  </div>          
                </div>
          
              </div>
		 
		
		   </div>

	
		  <div class="col-sm-12 col-md-12 col-lg-12">
			
		    <table width="80%" id="data-table" class="table  table-border">
                            <thead>
                            
							<th>Details</th>
							<th>Dealer Quote</th>
							<th align="center">Difference</th>
							<th>Customer Offer</th>
                            </thead>
                            <tbody>
							
							<tr>
							<td><b>MSRP</b></td>
							<td align="center" style="font-size:16px;"><b>$@if(!empty($car_datas->ref_price) > 0){{$car_datas->ref_price}}@endif</b></td>
							<td></td>
							<td align="center" style="font-size:16px;"><b>$@if(!empty($car_datas->ref_price) > 0){{$car_datas->ref_price}}@endif</b></td>
						
							</tr>
							
							
							<tr>

							<td><b>Dealer Option</b></td>
							<td align="center"><b>
						<?php
						$get_dealer_option_description = explode(',',$view_quote_form->dealer_price_description);
						$get_dealer_option_value = explode(',',$view_quote_form->dealer_price_values);
						  
						  $i = 0;
						  $sum = 0;  
					 
					 foreach ( $get_dealer_option_value as $row)
						{ 
						$i++; 
						$dealer_option = $sum += $row; 	
						} 
					 
					 echo "$". $dealer_option;
				     ?>
							</b></td>
							<td align="center">
					
						<?php
						
						 $customer_get_dealer_option_value = explode(',',$view_quote_form->customer_price_values);
						  
						  $ui = 0;
						  $usum = 0;  
					 
						 foreach ( $customer_get_dealer_option_value as $user_row)
						 { 
							$i++; 
							$customer_price_option = $usum += $user_row; 	
						 } 
						 
						 $dp_total_diff = $dealer_option - $customer_price_option;
						 
						 echo " <b style='font-weight:bold; color:#cc0000; font-size:18px;'>" .'$'. $dp_total_diff;
						
						 ?>
							
						</td>
							<td align="center"><b> 
							<!-- CUSTOMER -->
						 <?php	echo "$". $customer_price_option; ?>
					
							</b></td>
							</tr>
							
				<?php 
				
				foreach($get_dealer_option_description as $key => $description){ ?>
				
							<tr>
							<td><?= $description ?></td>
							<td align="center">$<?= $get_dealer_option_value[$key] ?></td>
							<td align="center" style="color:#cc0000;">$<?= $get_dealer_option_value[$key] - $customer_get_dealer_option_value[$key] ?> </td>
							<td align="center">$ <?=$customer_get_dealer_option_value[$key];?></td>
			
							</tr>
				
				
				<?php } ?>			
							
				<tr class="separator"><td></td><td></td><td></td><td></td></tr>
				
					<tr class="doc_fee_total">
					<td><b>Document Fees</b></td>
					<td align="center"><b>
					 <?php
					 $get_doc_fees_description = explode(',',$view_quote_form->doc_fees_description);
				     $get_doc_fees_value = explode(',',$view_quote_form->doc_fees_values);	
					 
					 $i2 = 0;
					 $sum2 = 0;  
					 
					 foreach ($get_doc_fees_value as $row2)
					 { 
						$i2++; 
						$doc_fees_value = $sum2 += $row2; 	
					 } 
					 
					 echo "$". $doc_fees_value;
				     ?>
					</b></td>
					
					<td align="center"><b>
					
				
					<?php
				     $customer_get_doc_fees_values = explode(',',$view_quote_form->customer_doc_fees_values);	
					 
					 $ui2 = 0;
					 $usum2 = 0;  
					 
					 foreach ($customer_get_doc_fees_values as $user_row2)
					 { 
						$ui2++; 
						$customer_get_doc_fees_value = $usum2 += $user_row2; 	
					 } 
					 
					 $total_doc_diff = $doc_fees_value - $customer_get_doc_fees_value;
					 
				    echo " <b style='font-weight:bold; color:#cc0000; font-size:18px;'>" .'$'. $total_doc_diff;
				     ?>
				
					<td align="center">
					
					<b><?php  echo "$". $customer_get_doc_fees_value; ?></b>
					
					</td>
					
					
					
					</b></td>
					</tr>
				
				<?php 
				foreach($get_doc_fees_description as $key => $description){ ?>
				
						<tr>
							<td><?= $description ?></td>
							<td align="center">$<?= $get_doc_fees_value[$key] ?></td>
							<td align="center" style="color:#cc0000;">$<?= $get_doc_fees_value[$key] - $customer_get_doc_fees_values[$key] ?> </td>
							<td align="center">$ <?=$customer_get_doc_fees_values[$key];?></td>
							</tr>
				
				<?php } ?>						
					
				<tr class="separator"><td></td><td></td><td></td><td></td></tr>
					
				    <tr>
					<td><b>Rebates & Discounts</b></td>
					<td align="center" style="color:#cc0000;"><b>
					
					<?php 
					$get_rebate_discount_description = explode(',',$view_quote_form->rebate_discount_description);
					$get_rebate_discount_values = explode(',',$view_quote_form->rebate_discount_values);
					
					 $i3 = 0;
					 $sum3 = 0;  
					 
					 foreach ($get_rebate_discount_values as $row3)
					 { 
						$i3++; 
						$rebate_discount_values_total = $sum3 += $row3; 	
					 } 
					 
					 echo "$". $rebate_discount_values_total;
					
					?>
					</b></td>
					
					<td align="center"><b>
				  

					<?php 
					$customer_get_rebate_discount_values = explode(',',$view_quote_form->customer_rebate_discount_values);
					
					 $ui3 = 0;
					 $usum3 = 0;  
					 
					 foreach ($customer_get_rebate_discount_values as $user_row3)
					 { 
						$ui3++; 
						$customer_rebate_discount_values_total = $usum3 += $user_row3; 	
					 } 
					 
					 $rebate_discount_values_diff = $customer_rebate_discount_values_total - $rebate_discount_values_total;
					 
					 echo "<b style='font-weight:bold; color:#cc0000; font-size:18px;'>" .'$'. $rebate_discount_values_diff;

					?>
					
					</b></td>
					
					<td align="center"style="color:#cc0000;"><b> $<?php echo $customer_rebate_discount_values_total ; ?></b></td>
					
					</tr>
								

					
				<?php 
				foreach($get_rebate_discount_description as $key => $description){ ?>
				
						<tr>
							<td><?= $description ?></td>
							<td align="center"style="color:#cc0000;">$<?= $get_rebate_discount_values[$key] ?></td>
							<td align="center" style="color:#cc0000;">$<?= $customer_get_rebate_discount_values[$key] - $get_rebate_discount_values[$key] ?> </td>
							<td align="center"style="color:#cc0000;">$ <?=$customer_get_rebate_discount_values[$key];?></td>
							</tr>
				
				<?php } ?>	


				<!--<tr>
					<td><b style="font-size:16px; color:#cc0000;">Total Saving</b></td>
					<td align="center"><b style="font-size:16px; color:#cc0000;"><?php  echo "$". $rebate_discount_values_total; ?></b></td>
					<td align="center">
					
					 <b style="font-size:18px; color:#cc0000; font-weight:bold;">

					<?php /*
					$customer_get_rebate_discount_values = explode(',',$view_quote_form->customer_rebate_discount_values);
					
					 $ui3 = 0;
					 $usum3 = 0;  
					 
					 foreach ($customer_get_rebate_discount_values as $user_row3)
					 { 
						$ui3++; 
						$customer_rebate_discount_values_total = $usum3 += $user_row3; 	
					 } 
					 
					 $rebate_discount_values_diff = $customer_rebate_discount_values_total - $rebate_discount_values_total;
					 
					 echo "<b style='font-weight:bold; color:#cc0000; font-size:18px;'>" .'$'. $rebate_discount_values_diff;

					*/?>


					 
					 </b>
				
					</td>
					<td align="center">

					<b style="font-size:16px; color:#cc0000;">	
					<?php  /*
					if(!empty($customer_rebate_discount_values_total)){
					echo "$". $customer_rebate_discount_values_total; 
					}
					else{
						"0";		
					}*/
					?></b>
	
					</td>
					</tr>-->
					
					
					
					<tr>
					<td><b style="font-size:16px; color:#cc0000;"></b></td>
					<td align="center"><b style="font-size:16px; color:#cc0000;">
					
					
					
					</b></td>
					<td></td>
					<td align="center">
					<b style="font-size:16px; color:#cc0000;">
				
					</b>
					</td>
					</tr>
					
					<tr>
					<td><b style="font-size:16px; color:green;">Sales Price</b></td>
					<td align="center" style="font-size:16px; color:green;"><b>
					
					<?php  
					  $net_selling_price = $view_quote_form->sale_price;
					  echo "$". $net_selling_price;
					 ?>
					
					</b></td>
					<td></td>
					<td align="center" style="font-size:16px; color:green;"><b>
					${{$view_quote_form->customer_sale_price}} 
					
					</b>
					</td>
					</tr>
					
					
					<tr class="grey">
					<td><b>Customer Options</b></td>
					<td><b></b></td>
					<td></td>
					<td><b></b></td>
					</tr>
						
				 <?php
					
					$customer_options_description = explode(',',$view_quote_form->customer_options_description);
					$customer_options_value = explode(',',$view_quote_form->customer_options_value);
  
					  foreach($customer_options_description as $key => $description){ ?>
					
								<tr>
								<td><?= $description ?></td>
								<td align="center"></td>
								<td align="center"></td>
								<td align="center"><b><?= $customer_options_value[$key] ?></b></td>
				
								</tr>
		
					<?php } ?>				
						
				
				  </tbody>
              </table>
		
			  <a href="{{url('request-best-offer-quote-list.html')}}" class="btn btn-primary pull-right"> Back to RBO list </a>
			
		  </div>
	

		<div class="col-sm-1 col-md-1 col-lg-1">
		
		</div>
		
		</div>
		</div>
		<br><br>	
	  
	
		</div>

		</div>

    
    <!-- Message Reply-->
   

 

@endsection