<?php 
require_once 'twilio-call-chat/vendor/autoload.php';
use Twilio\Rest\Client;
use App\Helpers\Common;
?>
@extends('layouts.web_pages')
@section('content')
<style>
.img-fluid{
border-radius: 5px;	
}
</style>

	<style>
	.leave-span{
	border:1px solid #ccc;
	border-radius: 5px;	
	}
	.channel-row,#new-channel-input-row{
	display: none;	
	}

	#channel-panel{
	border-right: 1px solid #333;
	padding-right: 8px;	
	}

	li.channel_list{
	width: 100%;
	background:#337ab7;
	font-size: 12px;
	list-style:none;
	padding: 6px;
	border-radius: 5px 0px 0px 5px;
	margin-top: 10px;
	margin-bottom:10px;
	color:#333;
	}
	
	li.channel_list a:hover{
		text-decoration: none;
		color:#000;
	}
	
	li.channel_list a{
		color:#fff;
	}

	</style>


<?php if(Auth::user()){?>


   <?php
  
   $channel_count = DB::table('twilio_chat')->where('twilio_chat.dealer_id',$_GET['dealer_id'])
  ->where('twilio_chat.user_id',Auth::user()->id)->where('notify',0)->count(); 
  
   $notify_count = DB::table('twilio_chat')->where('twilio_chat.dealer_id',$_GET['dealer_id'])
  ->where('twilio_chat.user_id',Auth::user()->id)->where('notify',0)->count(); 
   
   $get_dealer_phone = DB::table('dealer')->where('dealer_id',$_GET['dealer_id'])->get()->first();

   	$sid = "AC85a8fe913314e6049feff220c4095dcb";
	$token ="4f08d3307d1e8812cb07dbf70c45d808";
	
	$twilio = new Client($sid, $token);
   
  
   if($chat_count!=0){
	 
	 
	  $channel = $twilio->chat->v2->services("IS81c79deb3e9b46e09e874082bc454607")
                ->channels
                ->create(array("friendlyName" => "".$check_chat_status_inner[0]->chat_channel."", "uniqueName" => "".$check_chat_status_inner[0]->chat_channel."", "CreatedBy" => "".Auth::user()->name.""));  
	 
	 
	try {
		$message = $twilio->messages
							  ->create("+".$get_dealer_phone->dealer_phone,//to +15129993171
									   array(
										   "body" => ''.Auth::user()->name.' want live chat with you. He sent chat message. Kindly login ottoguide.com dealer panel '.url('dealer/portal').'',
										   "from" => "+15104803765"
										  
									   )
							  );
		}
	
		 catch (Exception $e) {
		 
		}
	   
	 DB::table('twilio_chat')->where('dealer_id', $_GET['dealer_id'])
	 ->where('user_id',Auth::user()->id)->update(array('notify' => '1'));  
	 
	$dealer_id = $_GET['dealer_id'];
	
	Common::addon_inc($dealer_id,'chat_lead_addon',1);
	   
   }
   
   
   
	else {
		
	}

?>

	<!--<script>
	window.my_base_url = '<?//= url('') ?>';
	</script>;-->



<link rel="stylesheet" href="{{ asset('css/twiliochat.css') }}">

<div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Chat Communication</h1>            
          </div>
        </div>
      </div>      
    </div>
    
		<div class="container">	 
	    @if($errors->any())
        <p class="alert alert-success">Saved Advert Deleted successfully </p>
      @endif 
		</div>
    <div class="container">
      <div class="form-row"><br><br><br>
    		
			<!-- client sidebar -->

		  
		  <div class="col-sm-12 col-md-12 col-lg-12">
		  
            <div class="card-body">
			@if(!empty($_GET['car_id']))
			   <br>
              <div class="cxm-advert-item mb-3 pull-right" >            
                  <div class="form-row">
                    <div class="col-sm-3">
                      <div class="cxm-img">
                        <a href="#"><img class="img-fluid" src="@if(!empty($car_datas->media->photo_links[0]) > 0){{$car_datas->media->photo_links[0]}}@endif"></a>
                      </div>
                    </div>
                    <div class="col-sm-9">
                    <div class="cxm-content">
    
                    <div>
					
					<span class="fc1 fs24"> @if(!empty($car_datas->ref_price) > 0)$ {{$car_datas->ref_price}}@endif</span></div>
				    <a class="text-primary font-weight-bold fs18" href="#">@if(!empty($car_datas->heading) > 0){{$car_datas->heading}}@endif</a>
                   
				   <ul class="cxm-facts fs12 bg-secondary p-2 rounded">
                        <li><span class="fa fa-modx text-primary"></span> 
				       @if(!empty($car_datas->build->year) > 0){{$car_datas->build->year}}@endif
												</li>
                        <li><span class="fa fa-barcode text-primary"></span> 
					    @if(!empty($car_datas->build->make) > 0){{$car_datas->build->make}}@endif 
						</li>
                        
						<li><span class="fa fa-road text-primary"></span> 
						@if(!empty($car_datas->miles) > 0){{$car_datas->miles}}@endif  miles

						</li>
                        <li><span class="fa fa-toggle-on text-primary"></span>
												Automatic
                       						</li>
                        <li><span class="fa fa-spinner text-primary"></span> 
						
										@if(!empty($car_datas->build->engine) > 0){{$car_datas->build->engine}}@endif
                       						</li>
                        <li><span class="fa fa-fire text-primary"></span> 
											@if(!empty($car_datas->build->fuel_type) > 0){{$car_datas->build->fuel_type}}@endif
                       						</li>
                      </ul>
					  
					<div class="fs12 lh16 mb-2">@if(!empty($car_datas->extra->seller_comments) > 0){{ str_limit($car_datas->extra->seller_comments, 650)}}@endif</div>
						
						
					</div>
                    </div>
                  </div>          
                </div>
                	
						@else
						<h3 align="center">{{$_GET['subject']}}</h3>
						@endif
						

					<br>
              </div>
		 
             
		<br><br>
		
		   </div>
		    
			
          </div>
		  
		  
		
		<div class="row leave-span">
		
		<div class="col-md-2">
		<br>
		<span id="status-span"> <b> Communication List </b></span>
		
		<?php 
		  
	   $get_user_channels =  DB::table('twilio_chat')->where('user_id',Auth::id())->get();
		  
		  if(count($get_user_channels)){
			
		  foreach($get_user_channels as $get_user_channel){
			
		  echo "<li class='channel_list'><a href='".url('mychat.html/?car_id='.$get_user_channel->car_id.'&channel_name='.$get_user_channel->chat_channel.'&dealer_id='.$get_user_channel->dealer_id.'')."'>".$get_user_channel->channel_display_name. "</a></li>";  
			
		  }
			  
		 } 
		
		 ?>
		 
		</div>
		
	  <div class="col-md-10">
		
	   <iframe src="conversation.html?id=<?=Auth::user()->name?>&channel_name=<?=$_GET['channel_name']?>" width="100%" height="800" frameborder="0" scrolling="no"></iframe>

	  </div>
		</div>
	
	
	
  
		 
		  
      </div>
   
	<br><br>

 
<?php } 
else{
	echo "<br>"; echo "<br>"; echo "<br>";	
	echo "<p align='center'>"."Client Area"."</p>";	
}
?>


	@endsection