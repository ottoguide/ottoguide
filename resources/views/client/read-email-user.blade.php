@extends('layouts.web_pages')
@section('content')
<style>

.bg-white{
	margin-bottom: 2%;
    padding:1%;
    border-top: 3px solid #127ba3;
	border-bottom: 1px solid #ccc;
	border-left: 1px solid #ccc;
	border-right: 1px solid #ccc;
	border-radius: 5px;	
  }
  
  input[type=checkbox] {
    display: inline-block;
    *display: inline;
    vertical-align: middle;
    margin: 0;
    padding: 0;
    width: 16px;
    height: 16px;
    border: none;
    cursor: pointer;
    color: #fff;
    -webkit-background-size: 240px 24px;
    background-size: 240px 24px;
	}

  .badge{
    display: inline-block;
    padding: 0.50em 0.75em;
    font-size: 80%;
    font-weight: 500;
	}

.modal-header-primary {
	color:#fff;
    padding:9px 15px;
    border-bottom:1px solid #eee;
    background-color: #ff7702;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
	}	
	.modal-body{
	min-height: 300px;
	max-height: 400px;
	overflow: auto;
	}

	.card-header {
		padding: 0.40rem 0.75rem;
		background-color:#fff; 
	}
	
	.table th, .table td {
		   padding: 0.30rem;
		   padding-left:15px;
		}
	small{
	margin-right: 8px;		
	}
 .padding{
	padding: 2%;	 
 }
 
 .client{
	    border-top: 3px solid #127ba3;
 }
  .dealer{
	 border-top:3px solid #7FBA5D;
 }
 
 hr {
    margin-top: 0.3rem;
    margin-bottom: 0.3rem;
    border: 0;
    border-top: 1px dotted rgba(0,0,0,0.1);
}
 
</style>

 <script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
 
 <script type="text/javascript">
//<![CDATA[
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
  //]]>
  </script>


<?php if(Auth::user()){ ?>

<?php DB::table('email_box')->where('id',decrypt($_GET['email_id']))->update(array('read_by_client' => 1));	?>

 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Client Email Panel </h1>            
          </div>
        </div>
      </div>      
    </div>
    

	
    <div class="py-5">
		<div class="container">	 
	   
	    <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
	   
		</div>
    <div class="container">
	
      <div class="row">   
    		 <!-- client sidebar -->
      @include('client.emailbox_sidebar')



	   @if(count($get_email_data)) 
			  
        <div class="col-md-9 bg-white ">

          <div class="box box-primary">
            <div class="box-header with-border">

            </div>
            <!-- /.box-header -->
            <div class="box-body padding">
              <div class="mailbox-read-info">
                <h6><b>Subject:</b> {{$get_email_data[0]->subject}}</h6>
                <h6><b>From:</b> {{$get_email_data[0]->dealer_name}}
                <span class="mailbox-read-time pull-right"><b>Date:</b> <?= date("d-m-Y", strtotime($get_email_data[0]->email_date))."\n"; ?></span></h6>
              </div>
              <!-- /.mailbox-read-info -->
              <br>
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
				@if($get_email_data[0]->message_by == "client")
                <p> <b>Hello {{$get_email_data[0]->dealer_name}},</b></p>
				@else
				<p> <b>Hello {{$get_email_data[0]->name}},</b></p>	
				@endif
				
                <p>{!!$get_email_data[0]->message!!}</p>
			  </div>
			  
		  	  <hr>
              <!-- /.mailbox-read-message -->
           
			<?php 
			$services_list = json_decode($get_email_data[0]->services);
			?>	
			
			@if(count($services_list))	
		
			<div class="card refine-search">
						
						<div class="card-header font-weight-bold">
						
						<a data-toggle="collapse" href="#refineSearch" role="button" aria-expanded="false" aria-controls="refineSearch">			
						<img class="img-fluid" src="<?=url()?>public/images/service-car.svg" width="40"> 
						<b>Your Selected Services Details</b></a>
						</div>
						<div class="collapse show" id="refineSearch">
							<div class="">
								
								
							<table class="table services" style="margin: 0;">
										  <?php
										  
											foreach($services_list as $key=> $description) {
												//if ($key == 'Year' || $key == 'Model' || $key == 'Make' || $key == 'Trim' || $key == 'Mileage') {
													//continue;
												//}
										   ?>
											<tr>
												<td colspan="2" width="40%">
													<h6><i class="fa fa-caret-right"></i>&nbsp; <b>{{$key}}</b></h6>
												</td>
												<td colspan="3">
											  
													<h6>{{ucfirst($description)}}</h6>
												</td>
											</tr>
										<?php } ?>
									</table>
							
							</div>
						</div>

					</div>
					
			@else
			

			@endif
			
		</table>


		   </div>
            <!-- /.box-body -->
           
            <!-- /.box-footer -->
				
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
    

		</div>
		
		
		 <?php 

		  $get_replies = DB::table('email_box')->where('email_box.client_id',Auth::user()->id)
		 
		  ->where('reply_to',$get_email_data[0]->email_id)	
		  
		  ->orderBy('email_box.id','DESC')
		
		 ->join('users','client_id','=','users.id')
		
		 ->join('dealer','email_box.dealer_id','=','dealer.dealer_id')
		
		 ->select('email_box.id as email_id', 'users.id as user_id','email_box.dealer_id as dealer_id','dealer.dealer_id as dealer','client_id',
		  'image','name','subject','message','services','message_by','reply_to','is_read',
		  'email_box.created_at as email_date','users.created_at as user_date','dealer_name')		
		  
		 ->get();

			
		  ?>
		




		
		@if(count($get_replies))
			
		@foreach($get_replies as $get_reply)	
		
		
		<div class="col-md-3"></div>
		<div class="col-md-9 col-xs-offset-1 bg-white {{$get_reply->message_by}}">
		<div class="card refine-search">
				  		
						<div class="card-header">
						@if($get_reply->message_by == "client")
					    <b>From:</b> <span style="background-color:#127ba3; border-radius:3px; padding-left:4px;padding-right:4px; color:#fff;"> {{$get_reply->name}} </span> <br> 
						<b>To:</b> {{$get_reply->dealer_name}}  &nbsp; &nbsp;
						@else
						<b>From:</b> <span style="background-color:#7FBA5D; border-radius:3px; padding-left:4px;padding-right:4px; color:#fff;">  {{$get_reply->dealer_name}} </span><br> 
						<b>To:</b> {{$get_reply->name}}  &nbsp; &nbsp;					
					 	@endif
						<b>Subject:</b> {{$get_reply->subject}}</h6>
						<a data-toggle="collapse" href="#reply{{$get_reply->email_id}}" role="button" aria-expanded="false" class="pull-right" aria-controls="refineSearch">			
						&nbsp; &nbsp;<small><?= date("F j, Y", strtotime($get_reply->email_date))."\n"; ?> </small>
						</a>
						</div>
						
						
				<?php DB::table('email_box')->where('id',$get_reply->email_id)->update(array('read_by_client' => 1));?>

						
			   <div class="collapse show" id="reply{{$get_reply->email_id}}">
		   
				<div class="box-body padding">
              <!-- /.mailbox-controls -->
				<div class="mailbox-read-message">
				
                <p>{!!$get_reply->message!!}</p>
				
			   </div>
		  	   <hr>
			  
               <!-- /.mailbox-read-message -->
				</div>

							
				</div>
		   </div>

			
		</div>
		
		@endforeach
		
		@endif
		
		
		<!-- REPLY BOX -->
		
		<div class="col-md-3"></div>
		<div class="col-md-9  bg-white">
		
		<form class="form-horizontal" role="form" method="post" action="{{url('user-mail-reply.html')}}" class="bg-white">
	    {!! Form::hidden('_token', csrf_token()) !!}
		<div class="form-group">
			<label for="message" class="col-sm-2 control-label"><b>Reply to email</b></label>
			<div class="col-sm-12">
			<textarea class="form-control" rows="6" name="message"></textarea>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-12 col-sm-offset-2">
				<input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary pull-right">
			</div>
		</div>
		<div class="form-group">
		<div class="col-sm-12 col-sm-offset-2">
		</div>
		</div>
		
		<input type="hidden" name="email_id" value="{{$get_email_data[0]->email_id}}">
		<input type="hidden" name="dealer_id" value="{{$get_email_data[0]->dealer_id}}">
		<input type="hidden" name="subject" value="{{$get_email_data[0]->subject}}">
		
		</form>
        </div> 
		

	
	
		
	    @else
			
		<div class="col-md-9  bg-white">
		<br><br><br>
		 <h3 align="center">No Email Available</h3>	
		</div>
		
		@endif	
	
	      <ul class="pagination justify-content-center">      
            <?php
            /*   $links = $car_datas->render(); 
              $links = str_replace("<a", "<a class='page-link ' ", $links);
              $links = str_replace("<li", "<li class='page-item' ", $links);
              $links = str_replace("<span", "<span class='page-link'",$links);
              echo $links; */
            ?>
          </ul>  
				
			
			
		
            </div>
          </div>  
	
	
	
		    
        </div>
      </div>      
    </div>
	
	
<?php } 
else{
	echo "CLient ARea";	
}
?>


	@endsection