<div class="col-sm-3 col-md-3 col-lg-3 ">
  <div class="card border-primary mb-3">
    <ul class="list-group list-group-flush cxm-list-group">               
     
	 <li class="list-group-item">
       <a href="{{route('user_email_box')}}?email='.<?= encrypt('Inbox')?>.'"><h5><b>Inbox</b></h5></a>
        <div class="info-txt">List of all incoming email </div> 
		
		<?php 
		$count_emails = DB::table('email_box')->where('client_id',Auth::user()->id)
	    ->where('message_by','dealer')->where('read_by_client',0)->where('is_delete_by_user',0)->where('temp_delete_by_user',0)->count('id');
		
								if ($count_emails > 0) {
                                    echo "<span class='badge badge-danger'>";
                                    echo $count_emails;
                                    echo "</span>";
                                } else{
                               
                                }
	
					?> 
	
      </li>
     
	  <!-- <li class="list-group-item ">
       <a href="{{route('user_email_box')}}?email='.<?= encrypt('Sent')?>.'"><h5><b>Sent</b></h5></a>
        <div class="info-txt">List of all sent emails</div> 
        <?php /*
		$count_emails = DB::table('email_box')->where('client_id',Auth::user()->id)
	    ->where('message_by','client')->where('is_delete_by_user',0)->where('temp_delete_by_user',0)->count('id');
		
		if($count_emails > 0){
		echo "<span class='badge badge-primary'>";
		echo $count_emails; 
		echo"</span>";		
		 }
		else { } */
		?> 
		</li>  -->
		
		<li class="list-group-item ">
       
		<a href="{{route('user_email_box')}}?email='.<?= encrypt('Trash')?>.'"><h5><b>Trash</b></h5></a>
        <div class="info-txt">List of all deleted emails</div> 
         <?php 
		$count_deleted_emails = DB::table('email_box')->where('client_id',Auth::user()->id)
	    ->where('temp_delete_by_user',1)->where('is_delete_by_user',0)->count('id');
		
		if($count_deleted_emails > 0){
		echo "<span class='badge badge-primary'>";
		echo $count_deleted_emails;
		echo"</span>";		
		 }
		else {
		}
		?> 
		</li>  
	
    </ul>
  </div>
</div>