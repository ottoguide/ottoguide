@extends('layouts.web_pages')
@section('content')

  <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>My Contact Request:</h1>            
          </div>
        </div>
      </div>      
    </div>
 
    <div class="py-5">
      <div class="container">
        <div class="row">
           <!-- Client Sidebar  -->
		   @include('client.client_sidebar')
		    <!-- Client Sidebar  -->
          <div class="col-sm-9">
            <div class="card border-none rounded-0">
              <div class="card-header rounded-0">
                <div class="row no-gutters">
				
                  <div class="col-md-8 text-center text-md-left font-weight-bold fs18">My Contact Request: 
				</div>
                  
                </div>
              </div>
              <div class="card-body">
			   <br>
              
			 
			  
			  
			
			  
			  
			  
                <div class="cxm-advert-item mb-3">            
                  <div class="form-row">
                    <div class="col-sm-3">
                      	<div class="cxm-img">
                      <!--<div class="cxm-img-badge position-absolute"><span class="fa fa-camera"></span> </div>-->
                     <a href="{{url('car/detail').'/'.$car_datas->id}}"><img class="img-fluid" src="@if(!empty($car_datas->media->photo_links[0]) > 0)
            						{{$car_datas->media->photo_links[0]}}
            						@else
            						{{asset('public/images/no-image.jpeg')}}
                      @endif"></a>
                    </div>	
                    </div>
                    <div class="col-sm-6">
                      <div class="cxm-content">
	
                        <div><span class="fc1 fs24"> 
						 @if(!empty($car_datas->price) > 0)
						${{number_format((float)$car_datas->price)}}
						@endif</span></div>
					<a class="text-primary font-weight-bold fs18" href="#">
					@if(!empty($car_datas->heading) > 0)
            			{{$car_datas->heading}}
                        @endif </a>
					<div class="fs12 lh16 mb-2">
					<ul class="cxm-facts fs12  p-2 rounded">
                        <li><span class="fa fa-modx text-primary"></span> 
						@if(!empty($car_datas->build->year) > 0)
						{{$car_datas->build->year}}
						@endif
						</li>
                        <li><span class="fa fa-barcode text-primary"></span> 
						@if(!empty($car_datas->build->body_type) > 0)
						{{$car_datas->build->body_type}}
                        @endif
						</li>
                        <li><span class="fa fa-road text-primary"></span> 
						@if(!empty($car_datas->miles) > 0)
						{{$car_datas->miles}}
                        @endif  miles
						</li>
                        <li><span class="fa fa-toggle-on text-primary"></span>
						@if(!empty($car_datas->build->transmission) > 0)
						{{$car_datas->build->transmission}}
                       @endif
						</li>
                        <li><span class="fa fa-spinner text-primary"></span> 
						
						@if(!empty($car_datas->build->engine) > 0)
						{{$car_datas->build->engine}}
                        @endif
						</li>
                        <li><span class="fa fa-fire text-primary"></span> 
						@if(!empty($car_datas->build->fuel_type) > 0)
						{{$car_datas->build->fuel_type}}
                        @endif
						</li>
                      </ul>
					</div>
						
					</div>
					
                    </div>
			
					         
                </div>
				
                
				
				<br>
              </div>
			  
		
		
			 <div class="container">
			 @if($contact_data->email_request ==1)
			 <div class="col-md-9 width">
			 <div class="row">
			 <div class="col-md-1 col-xs-12"><span class="fa fa-envelope fa-2 abb2"></span></div>
			 <div class="col-md-6 col-xs-12 abb1" >Email contact already sent</div>
			 <div class="col-md-2 col-xs-12"> <button class="btn btn-primary"><span class="fa fa-send-o"></span> Resend</button></div>
			 </div>
			 </div>
			 @endif
			 <br>
			 @if($contact_data->sms_request ==1)
			 <div class="col-md-9 width">
			 <div class="row">
			 <div class="col-md-1 col-xs-12"><span class="fa fa-mobile abb3"></span></div>
			 <div class="col-md-6 col-xs-12 abb1">SMS contact already sent</div>
			 <div class="col-md-2 col-xs-12"> <button class="btn btn-primary"><span class="fa fa-send-o"></span> Resend</button></div>
			 </div>
			 </div>
			 @endif
			 <br>
			  @if($contact_data->call_request ==1)
			 <div class="col-md-9 width">
			 <div class="row">
			 <div class="col-md-1 col-xs-12"><span class="fa fa-phone fa-2 abb2"></span></div>
			 <div class="col-md-6 col-xs-12 abb1">Call contact already sent</div>
			 <div class="col-md-2 col-xs-12"> <button class="btn btn-primary"><span class="fa fa-send-o"></span> Resend</button></div>
			 </div>
			 </div>
			 @endif
			 </div>
			  
            </div>
			
          </div>  
    
        </div>
      </div>      
    </div>
   

@endsection   