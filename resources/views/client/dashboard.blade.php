@extends('layouts.web_pages')
@section('content')

<div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Dashboard</h1>            
          </div>
        </div>
      </div>      
    </div>
    <br>
	
    <div class="py-5">
	
	
	 <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
					
					  @if (session('error'))
                        <div class="alert alert-danger" width="50%">
                            {{ session('error') }}
                        </div>
                    @endif
		      </div>	
	
      <div class="container">
        <div class="form-row">
         <!-- client sidebar -->
		 @include('client.client_sidebar')
		 <!-- client sidebar --> 
		 
          <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="card">
              <div class="card-header font-weight-bold fs18"></div>
              <div class="card-body">
			  <br>
              <center><img class="img-fluid" src="http://dev.ottoguide.com/images/logo.png"></center>
			  <br>
			  <h2 align="center"> User Dashboard </h2>
            </div>
        </div>
    </div>

</div>

				  
				  </div>
                 
                </div>
              </div>
            </div>
          </div>          
                    
        </div>
      </div>      
    </div>
    @endsection