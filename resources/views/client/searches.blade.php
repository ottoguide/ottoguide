@extends('layouts.web_pages')
@section('content')
<div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>My Saved Searches</h1>            
          </div>
        </div>
      </div>      
    </div>    
    <div class="py-5">
	 <div class="container">
	   	@if($errors->any())
        <p class="alert alert-success">Saved Search Renamed successfully </p>
      @endif 
		</div>
    <div class="container">
     <div class="form-row">         		 
		 <!-- client sidebar -->
		 @include('client.client_sidebar')
		  <!-- client sidebar -->		    
      <div class="col-sm-7 col-md-8 col-lg-9">
        <div class="card mb-3">
          <div class="card-header">
            <div class="row no-gutters">
              <div class="col-md-4 text-center text-md-left font-weight-bold fs18">My Saved Searches</div>
              <div class="col-md-8">
                <div class="text-center text-md-right">
                 <!-- <a href="#" class="btn btn-primary btn-sm fs10"><span class="fa fa-bell-slash-o"></span> Trun Off Alerts</a>-->
                 @if(!$save_data->isEmpty())
                 <a href="{{route('delete_all_save_search',['client_id'=>encrypt(Auth::user()->id)])}}" class="btn btn-primary btn-sm fs10" onclick="return confirm('Are you sure you want to delete all items?');"><span class="fa fa-trash-o"></span>Remove All</a>
                 @endif
					     </div>
              </div>
            </div>                
          </div>
			    <div class="card-body">
			     <!-- Data Fetch From database -->	
           @if(!$save_data->isEmpty())
			   	 @foreach($save_data as $data)
              <div class="cxm-advert-item mb-3">            
                <div class="form-row">
                  <div class="col-md-12">				
                    <div class="cxm-content">
                      <a class="text-primary font-weight-bold fs18" href="#">{{$data['save_search_name']}}</a>
                      <hr class="hr1">
                        <div class="fs12 lh16 mb-2">{{str_replace(array("&","=","+","%","| filter_search : filter") ,array(" | "," : ","","","") , $data['save_search_url'])}}</div>
                        <div>{{$data['save_search_description']}}</div>
                        <hr class="hr1">							
                       <!-- <div class="font-weight-bold text-center text-md-left d-md-inline-block">Alerts:</div>-->
                        <div class="text-center text-md-left d-md-inline-block">
                        <!--<div class="btn-group btn-group-sm mb-2 mb-md-0" role="group">
                        <a href="#" class="btn btn-primary fs12"><span class="fa fa-bell-o"></span> ON</a>
                        <a href="#" class="btn btn-primary fs12"><span class="fa fa-bell-slash-o"></span> OFF</a>
						            </div>-->
                        <div class="btn-group btn-group-sm" role="group">			
						 <a href="{{route('delete_save_search',['save_searches_id'=> encrypt($data['save_search_id'])])}}" class="btn btn-primary fs12" onclick="return confirm('Are you sure you want to delete this item?');"><span class="fa fa-trash-o"></span>Delete</a>						
                        <a href="#" data-toggle="modal" data-target="#rename{{$data['save_search_id']}}" class="btn btn-primary fs12"><span class="fa fa-pencil-square-o"></span> Rename</a>
                          <!-- Rename PopUp -->
                          <div class="modal fade" id="rename{{$data['save_search_id']}}" tabindex="-1" role="dialog" aria-labelledby="rename" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header rounded-0">
                                  <h5 class="modal-title" id="rename">Rename Your Search</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>					
                								{!! Form::open(array('url' => '/save/search/edit', 'enctype' => 'multipart/form-data')) !!}
                								{!! Form::hidden('_token', csrf_token()) !!}								
                                <div class="modal-body">
                                 <!-- <div class="font-weight-bold">Search Details:</div>
                                  
                                  <div class="row no-gutters">
                                    <div class="col-sm-4 mb-3">
                                      <img class="img-fluid" src="{{asset('images/car2.jpg')}}">
                                    </div>                                  
                                    <div class="col-sm-8 mb-3">
                                      <p class="p-1 fs12 lh16">{{str_replace(array("&","=","+","%") ,array(" | "," : ","","") , $data['save_search_url'])}}</p>
                                    </div>
                                  </div> -->                              
                                  
                                  <label for="search-rename">Enter a new name for this search</label>
                                  <input type="text" class="form-control rounded-0" value="{{$data['save_search_name']}}" name="save_search_name"  placeholder="Save Search Title" required>

                                </div>
                                <div class="modal-footer">
								                  <input type="submit" name="submit" id="submit" class="btn btn-primary btn-sm" value="Save Search">
                                  <button type="button" class="btn btn-dark btn-sm" data-dismiss="modal"><span class="fa fa-remove"></span> Cancel</button>
                                </div>
                              </div>
                            </div>
                          </div>
						                <input type="hidden" class="form-control rounded-0" value="{{$data['save_search_id']}}" name="save_search_id">
                          </form>
                          <a href="{{route('search_car')}}?{{$data['save_search_url']}}" class="btn btn-primary fs12"><span class="fa fa-flash"></span> Run</a>
                        </div>
                        </div>
                      </div>					
                    </div>
                  </div>          
                </div>	
              		<!-- END Data Fetch From database -->  
              	@endforeach
                @else
                  <p align="center">No data found</p>
               @endif     
              </div>              
            </div>	
           
        <ul class="pagination justify-content-center">      
          <?php
            $links = $save_data->links(); 
        		$links = str_replace("<a", "<a class='page-link ' ", $links);
        		$links = str_replace("<li", "<li class='page-item' ", $links);
        		$links = str_replace("<span", "<span class='page-link'",$links);
        		echo $links;?>
          </ul>
        </div>
      </div>
    </div>
  </div>      
</div>
@endsection