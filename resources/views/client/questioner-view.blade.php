@extends('layouts.web_pages')
@section('content')
<style>
section {
    padding-top: 4rem;
    padding-bottom: 5rem;
    background-color: #f1f4fa;
}
.wrap {
    display: flex;
    background: white;
    padding-left:15px;
    border-radius: 0.5rem;
    box-shadow: 7px 7px 30px -5px rgba(0,0,0,0.1);
    margin-bottom: 0.3rem;
	color:#127ba3;
	border:1px dotted #ccc;
}

.wrap:hover {
    background-color: #e2e2e2;
    color: white;
}

.ico-wrap {
    margin: auto;
}

.mbr-iconfont {
    font-size: 1.2rem;
    color: #313131;
    margin: 0.2rem;
    padding-right: 0.2rem;
	font-family: 'Century Gothic',CenturyGothic,AppleGothic,sans-serif;
}
.vcenter {
    margin: auto;
}

.mbr-section-title3 {
    text-align: left;
}
h2 {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
}
.display-5 {
    font-family: 'Century Gothic',CenturyGothic,AppleGothic,sans-serif;
    font-size: 1.2rem;
}
.mbr-bold {
    font-weight: 600;
	
}

 p {
    padding-top: 0.5rem;
    padding-bottom: 0.1rem;
    line-height: 24px;
}
.display-6 {
    font-family: 'Century Gothic',CenturyGothic,AppleGothic,sans-serif;
    font-size: 1.2rem;
	font-weight: bold;
	color:green;
}

</style>

<div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Questioner View</h1>            
          </div>
        </div>
      </div>      
    </div>
    <br>
	 <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
					
					 @if (session('error'))
                        <div class="alert alert-danger" width="50%">
                            {{ session('error') }}
                        </div>
                    @endif
		      </div>	
    <div class="py-5">
      <div class="container">
        <div class="form-row">
         <!-- client sidebar -->
		 @include('client.client_sidebar')
		 <!-- client sidebar --> 
		 
          <div class="col-sm-7 col-md-8 col-lg-9">
            <div class="card">
              <div class="card-header font-weight-bold fs18">Questioner View</div>
              <div class="card-body">
                <div class="form-row">
                  
				 <div class="col-sm-12 col-md-12 col-lg-12">
				  
                
				<div class="panel panel-default">
					<div class="panel-body">
						
						<div class="table-container">
							<table class="table table-filter">
								<tbody>
		
					
					
		<div class="row">
	  @foreach($Qanswer_decode  as $key=> $answers)
	   
	  <?php $get_questions = DB::table('quiz_detail')->where('question_id', $key)->get()->toArray();  ?>		
	    
	   
	     <div class="col-lg-12 mbr-col-md-12">
                <div class="wrap">
                    
                    <div class="text-wrap ">
                        <h2 class="mbr-fonts-style mbr-bold mbr-section-title3 display-5">
                        <span>Q: {{$get_questions[0]->question_text}} ?</span>
                        </h2>
						@if($get_questions[0]->question_subtype == "image")
                        <p><img src="{{$answers}}" width="100"></p>
						@else
                        <p class="mbr-fonts-style text1 mbr-text display-6">{{$answers}}</p>
						@endif
						
						
                    </div>
                </div>
            </div>
	  
		@endforeach
			  
								
			</div>						
							
								</tbody>
							</table>
							
							<a href="{{url('/search-car.html?')}}{{$search_query}}filter_search=filter&car_type=all" class="btn btn-success pull-right">Go To Search Result</a>
						</div>
					</div>
				</div>	
				
				
	             
				 </div>
	
				  
				  </div>
                 
                </div>
              </div>
            </div>
          </div>          
                    
        </div>
      </div>      
    </div>
	

    @endsection