<?php include('header.php');
?>
  <!-- Left side column. contains the logo and sidebar -->
   <?php include('sidebar.php');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      User List
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="UserAdd.php" class="btn btn-primary test5"> Add User</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       <table class="table table-striped test2">
    <thead>
      <tr class="test3">
        
        <th class="">Name</th>
        <th class="">Email</th>
        <th class="">Mobile</th>
        <th class="">User group</th>
        <th>Action<i class="fa fa-angle-down test4"></i></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        
        <td>Abbas</td>
        <td>abbas@hotmail.com</td>
        <td>054554</td>
        <td>Super-admin</td>
        <td><a>Edit/</a><a>Delete</a></td>
      </tr>
      <tr>
        
        <td>Abbas</td>
        <td>abbas@hotmail.com</td>
        <td>054554</td>
        <td>Super-admin</td>
        <td><a>Edit/</a><a>Delete</a></td>
      </tr>
      <tr>
       
        <td>Abbas</td>
        <td>abbas@hotmail.com</td>
        <td>054554</td>
        <td>Super-admin</td>
        <td><a>Edit/</a><a>Delete</a></td>
      </tr>
      <tr>
      
        <td>Abbas</td>
        <td>abbas@hotmail.com</td>
        <td>054554</td>
        <td>Super-admin</td>
        <td><a>Edit/</a><a>Delete</a></td>
      </tr>
    </tbody>
  </table>

    </section>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php include('footer.php');
?>