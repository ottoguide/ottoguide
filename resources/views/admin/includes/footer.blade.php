  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="admin-assets/bower_components/jquery/admin-assets/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="admin-assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
  
</script>
<!-- Bootstrap 3.3.7 -->
<script src="admin-assets/bower_components/bootstrap/admin-assets/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="admin-assets/bower_components/raphael/raphael.min.js"></script>
<script src="admin-assets/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="admin-assets/bower_components/jquery-sparkline/admin-assets/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="admin-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="admin-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="admin-assets/bower_components/jquery-knob/admin-assets/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="admin-assets/bower_components/moment/min/moment.min.js"></script>
<script src="admin-assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="admin-assets/bower_components/bootstrap-datepicker/admin-assets/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="admin-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="admin-assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="admin-assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="admin-assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="admin-assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="admin-assets/dist/js/demo.js"></script>

<script src="admin-assets/bower_components/ckeditor/ckeditor.js"></script>
</body>
</html>