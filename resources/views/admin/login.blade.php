<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>OttoGuide | Log in</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="admin_assets/bower_components/bootstrap/css/bootstrap.min.css">

  <link rel="stylesheet" href="admin_assets/bower_components/font-awesome/css/font-awesome.min.css">

  <link rel="stylesheet" href="admin_assets/bower_components/Ionicons/css/ionicons.min.css">

	<link rel="stylesheet" href="{{asset('admin_assets/css/AdminLTE.min.css')}}">
 
  <link rel="stylesheet" href="{{asset('admin_assets/plugins/iCheck/square/blue.css')}}">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition login-page">
  @yield('content')
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>OTTO GUIDE</b><br>Admin Panel</a>
  </div>
  <!-- /.login-logo -->
   <div class="login-box-body">
         <p class="login-box-msg">Sign in to start your session for RO Plant</p>
        <form method="POST" action="{{ route('admin_login') }}" id="login-form">     
        {{ csrf_field() }}   
          <div class="form-group has-feedback">
             <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>            
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" required placeholder="Password" name="password">           
            <span class="fa fa-lock form-control-feedback"></span>
          </div>
            
          <div class="row">
            @if ($errors->has('email'))
                <span class="help-block alert-danger">
                    <center><strong>{{ $errors->first('email') }}</strong></center>
                </span>
            @endif
            <div class="col-xs-8">
             <div class="checkbox icheck">
                <label>
                   <input type="checkbox" name="rememberme" {{ old('remember') ? 'checked' : '' }}> Remember Me
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>
       <a href="{{ route('password.request') }}">Forgot my password</a><br>      
      </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
