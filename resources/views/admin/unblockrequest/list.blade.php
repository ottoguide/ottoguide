@extends('layouts.admin')
@section('content')
<style>
.chat_block_action {
    background-color: #1590d8;
    padding: 5px;
    border-radius: 5px;
    color: #fff;
}
</style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               User Unblock Request
                <small>User Unblock Request List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">User Unblock Request List</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-10"></div>
                              
                            </div>
                            <table width="100%" id="data-table" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr class="test3">
                                    <th class="text-center" width="4%">#</th>
                                    <th width="15%">User Name</th>
									 <th width="12%">Subject</th>
                                    <th width="32%">Message</th>
									<th width="12%">Date</th>
									<th width="7%">Status</th>
                                    
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
	
	

	
	
@endsection

@section('page_script')

    <script>
		
		$('#data-table').DataTable($.extend(datatable_options, {
		"scrollX": true,
			ajax: '{{route('request_unblock_table')}}',
			columns: [
				{ data: 'DT_Row_Index', name: 'id' },
				{ data: 'name', name: 'name' },
				{ data: 'subject', name: 'subject' },
				{ data: 'message', name: 'message' },
				{ data: 'created_at', name:'created_at'},
				{ data: 'status', name: 'status'},
				
				
			]
		}));
		
    </script>
	
	
	
@endsection