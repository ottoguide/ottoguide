@extends('layouts.admin')
@section('content')
<style>
.chat_block_action {
    background-color: #e2e2e2;
    padding: 5px;
    border-radius: 5px;
    color: #222;
}
</style>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               Chat Communication
                <small>User Block List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">User Block List</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-10"></div>
                              
                            </div>
                            <table width="100%" id="data-table" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr class="test3">
                                    <th class="text-center" width="4%">#</th>
                                    <th width="16%">Dealer Name</th>
									 <th width="15%">User Name</th>
                                    <th width="18%">Channel</th>
									<th width="27%">Reason For Report</th>
									<th width="10%">Date</th>
                                    <th class="text-center" width="8%">Block</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
	
	
	
@endsection

@section('page_script')

    <script>
		
		$('#data-table').DataTable($.extend(datatable_options, {
		"scrollX": true,
			ajax: '{{route('chat_block_table')}}',
			columns: [
				{ data: 'DT_Row_Index', name: 'id' },
				{ data: 'dealer_name', name: 'dealer_name' },
				{ data: 'name', name: 'name' },
				{ data: 'chat_channel', name: 'chat_channel' },
				{ data: 'reported_reason', name: 'reported_reason' },
				{ data: 'created_at', name: 'created_at'},
				{ data: 'action', name: 'action', orderable: false, searchable: false},
			]
		}));
		
    </script>
	
	<script>
	
	
	$(document).on('change','.chat_block_action',function (e){ 
	

		var chat_block_id = $(this).parents('tr').find(".chat_block_id").val();
			
		var user_id = $(this).parents('tr').find(".user_id").val();

		var dealer_id = $(this).parents('tr').find(".dealer_id").val(); 

		var status = $(this).parents('tr').find(".chat_block_action").val(); 

		$.ajax({
			type:'POST',
			data: {
				"_token": "{{ csrf_token() }}",
				
				chat_block_id: chat_block_id,
				user_id:user_id,
				dealer_id:dealer_id,
				status:status
			},

			url:'{{url('chat/user/block')}}',

			success: function () {
			
			location.reload();
			
			alert('User Blocked Sucessfully').css({"font-size": "17px", "color": "red","font-weight": "bold", "text-align": "center"});

			}
	
	
	 });

	});
	</script>	
	
	
	
	
@endsection