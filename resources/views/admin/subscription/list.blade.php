@extends('layouts.admin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Subscriptions
                <small>All subscriptions List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Subscriptions List</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-10"></div>
                                <div class="col-xs-2">
                                    <a href="{{route('create_subscription')}}" class="btn btn-primary btn-block margin-bottom">
                                        Add subscription
                                    </a>
                                </div>
                            </div>
                            <table width="100%" id="data-table" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr class="test3">
                                    <th class="text-center" width="4%">#</th>
                                    <th>Subscription Title</th>
									 <th>Description</th>
                                    <th>Total Featured Ad</th>
                                    <th>Total Chats</th>
                                    <th>Total Lease/Purchase</th>
									<th>Expiry Days</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('page_script')
    <script>
		$('#data-table').DataTable($.extend(datatable_options, {
		"scrollX": true,
			ajax: '{{route('subscription_list_table')}}',
			columns: [
				{ data: 'DT_Row_Index', name: 'title' },
				{ data: 'title', name: 'title' },
				{ data: 'description', name: 'description' },
				{ data: 'featured_ad', name: 'featured_ad' },
				{ data: 'chats', name: 'chats' },
				{ data: 'lease_purchase', name: 'lease_purchase' },
				{ data: 'expire_days', name: 'expire_days' },
				{ data: 'action', name: 'action', orderable: false, searchable: false},
			]
		}));
    </script>
@endsection