@extends('layouts.admin')
@section('content')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Subscription
                <small>Create</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Create</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Create Subscription</h3>
                </div>
                <div class="box-body form-horizontal">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    {!! Form::open(array('url' => 'subscription/store', 'enctype' => 'multipart/form-data')) !!}
                    {!! Form::hidden('_token', csrf_token()) !!}

                    @include('admin.subscription.partials.form')

                    <div class="row">
                        <div class="col-sm-2 col-sm-offset-10">
                            {{Form::submit('Create subscription', ['class' => 'btn btn-primary btn-block'])}}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

@endsection

<script type="text/javascript" >  
    
     function disableMyText(){  
          if(document.getElementById("ads_unlimited").checked == true){		  
              document.getElementById("featured_ad").disabled = true;
			  $('#featured_ad').val('');
		  
          }else{
            document.getElementById("featured_ad").disabled = false;
			
          } 
	
	  if(document.getElementById("chats_unlimited").checked == true){  
              document.getElementById("chats").disabled = true; 
				 $('#chats').val('');			  
          }else{
            document.getElementById("chats").disabled = false;
			
          }  
		  
		  if(document.getElementById("lease_purchase_unlimited").checked == true){  
              document.getElementById("lease_purchase").disabled = true;
			   $('#lease_purchase').val('');
				 			  
          }else{
            document.getElementById("lease_purchase").disabled = false;
			
          } 	  
     }  
     </script> 