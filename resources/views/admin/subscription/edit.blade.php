
@extends('layouts.admin')
@section('content')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Subscription
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Edit Subscription</h3>
                </div>
                <div class="box-body form-horizontal">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    {!! Form::open(array('url' => 'subscription/update', 'enctype' => 'multipart/form-data')) !!}
                    {!! Form::hidden('_token', csrf_token()) !!}

                    @include('admin.subscription.partials.form', $subscription)

                    <div class="row">
                        <div class="col-sm-2 col-sm-offset-10">
                           <input type="hidden" name="subscription_id" value="<?php echo $subscription->id;?>">
                            {!! Form::submit('Update subscription', ['class' => 'btn btn-primary btn-block']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

@endsection
