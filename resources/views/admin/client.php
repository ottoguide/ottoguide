<?php include('header.php');
?>
  <!-- Left side column. contains the logo and sidebar -->
   <?php include('sidebar.php');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Client list
        <small></small>
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">
       <table class="table table-striped test2">
    <thead>
      <tr class="test3">
        <th class="">Dealer</th>
        <th class="">Name</th>
        <th class="">Email</th>
        <th class="">Mobile</th>
        <th class="">User group</th>
        <th>Action<i class="fa fa-angle-down test4"></i></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>222</td>
        <td>Abbas</td>
        <td>abbas@hotmail.com</td>
        <td>054554</td>
        <td>Super-admin</td>
        <td><a href="clientin.php">Edit/</a><a>Delete/</a><a href="clientinblock.php">Block</a></td>
      </tr>
      <tr>
        <td>222</td>
        <td>Abbas</td>
        <td>abbas@hotmail.com</td>
        <td>054554</td>
        <td>Super-admin</td>
        <td><a href="clientin.php">Edit/</a><a>Delete/</a><a href="clientinblock.php">Block</a></td>
      </tr>
      <tr>
       <td>222</td>
        <td>Abbas</td>
        <td>abbas@hotmail.com</td>
        <td>054554</td>
        <td>Super-admin</td>
        <td><a href="clientin.php">Edit/</a><a>Delete/</a><a href="clientinblock.php">Block</a></td>
      </tr>
      <tr>
        <td>222</td>
        <td>Abbas</td>
        <td>abbas@hotmail.com</td>
        <td>054554</td>
        <td>Super-admin</td>
        <td><a href="clientin.php">Edit/</a><a>Delete/</a><a href="clientinblock.php">Block</a></td>
      </tr>
    </tbody>
  </table>

    </section>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php include('footer.php');
?>