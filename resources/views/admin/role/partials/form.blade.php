<div class="form-group">
    <div class="col-sm-6">
        {!! Form::label('display_name', 'Group Name') !!}
        {!! Form::text('display_name', isset($role) ? $role->display_name : null, ['class' => 'form-control']) !!}
        <br>
        {!! Form::label('description', 'Description') !!}
        {!! Form::textarea('description', isset($role) ? $role->description : null, ['class' => 'form-control', 'rows' => 5]) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('permissions', 'Permissions') !!}
        <div class="row">
        @foreach ($perm_list as $key =>  $perm)
            <div class="checkbox col-sm-6">
                <label>
                    {!! Form::checkbox('permissions[]', $perm->id, in_array($perm->id, $role_perms)) !!} {{$perm->display_name}}
                </label>
            </div>
        @endforeach
        </div>
    </div>
</div>
