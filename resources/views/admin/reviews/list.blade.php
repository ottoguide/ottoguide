@extends('layouts.admin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               Reviews Reports
                <small>All Reviews Reports List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Report List</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-10"></div>
                              
                            </div>
                            <table width="100%" id="data-table" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr class="test3">
                                    <th class="text-center" width="4%">#</th>
                                    <th>Username</th>
									 <th width="15%">Title</th>
                                    <th width="20%">Review</th>
									<th>Dealer</th>
                                    <th>Complain Description</th>
									<th width="10%">Date</th>
                                    <th class="text-center" width="10%">Block</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
	
	
	  <div class="modal fade" id="reason" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            
			<div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Block Reason</h5>
                </div>
				
                <div class="modal-body">
				<p class="success_message" style="color:green; font-weight:bold;"></p>
				<textarea id="block_reason" name="block_reason" class="form-control" placeholder="write your comment to user"></textarea>		
                </div>
               
			   <div class="modal-footer">
			   <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>               
			   <button type="button" class="btn btn-primary block_user"> Block </button>
               </div>
			   
            </div>
        </div>
    </div>
	
	
@endsection

@section('page_script')

    <script>
		
		$('#data-table').DataTable($.extend(datatable_options, {
		"scrollX": true,
			ajax: '{{route('reviews_report_table')}}',
			columns: [
				{ data: 'DT_Row_Index', name: 'title' },
				{ data: 'name', name: 'name' },
				{ data: 'title', name: 'title' },
				{ data: 'message', name: 'message' },
				{ data: 'dealer_name', name: 'dealer_name'},
				{ data: 'message_report', name: 'message_report'},
				{ data: 'created_at', name: 'created_at' },
				{ data: 'action', name: 'action', orderable: false, searchable: false},
			]
		}));
		
    </script>
	
	<script>
	
	
   $(document).on('change','.review_action',function (e){ 
	 
	 var message_id = $(this).parents('tr').find(".message_id").val(); 
	
	 var client_id = $(this).parents('tr').find(".client_id").val();
	
	 var car_id = $(this).parents('tr').find(".car_id").val(); 
	
	 var status = $(this).parents('tr').find(".review_action option:selected" ).val();
	 
	 $('#reason').modal('show');
	
	 
	$(document).on('click','.block_user', function(){ 
		
	  var reason = $("#block_reason").val(); 
	  
	  var status_post = status;
	  var message_id_post = message_id;
	  var client_id_post = client_id;
	  var car_id_post = car_id;
	  
	
		$.ajax({
			type:'POST',
			data: {
				"_token": "{{ csrf_token() }}",
				message_id_post: message_id_post,
				client_id_post:client_id_post,
				car_id_post:car_id_post,
				status_post:status_post,
				reason:reason
			},

			url:'{{url('reviews/user/block')}}',

			success: function () {
			
			  $('.modal-body').html('User Blocked Sucessfully').css({"font-size": "17px", "color": "red","font-weight": "bold", "text-align": "center"});;

			setTimeout(function(){
			$('#reason').modal('hide');
			}, 2000);    
				
				location.reload();
				
			}
	
	
	
	
	});
		
	 }); 
	});
	</script>	
	
	
	
	
@endsection