

@extends('layouts.admin')
@section('web-footer')
@parent

	<script>
		/*var fixmeTop = $('#question_list_panel').offset().top;
		$(window).on('scroll',function() {
			var currentScroll = $(window).scrollTop();
			console.log(currentScroll );
			if (currentScroll >= fixmeTop) {
				$('#question_list_panel').css({
					position: 'fixed',
					top: '0',
					left: '0'
				});
			} else {
				$('#question_list_panel').css({
					position: 'static'
				});
			}
		});*/

		$(document).on('click', '.new_question_btn', function (e) {
			e.stopImmediatePropagation();

			var v = $('.new_question_text').val();
			$('#new_question_area').removeClass('has-error');
			$('#new_question_area > p').remove();
			if (v.length == 0) {
				$('#new_question_area').addClass('has-error');
				$('#new_question_area').append('<p class="text-danger col-sm-12">Question field is required!</p>');
				return;
			}

			$('#question_list').append('<div class="question">'+v+'</div>');
			$('.new_question_text').val('');

			drag_drop();
		});

		$(document).on('change', '.new_question_text', function (e) {
			e.stopImmediatePropagation();
			e.preventDefault();
			if (e.keyCode == 13) {
				$('.new_question_btn').click();
			}
		});

		function drag_drop() {
			$('#question_list').sortable({
				connectWith: ".connected"
			}).disableSelection();


			$('.question_tree').sortable({
				connectWith: ".connected",
				placeholder: "question_placeholder",
				update: function(event, ui) {
					//alert('hfghg');
					console.log(ui)

					if($(ui.item).find('.panel').length > 0) return;

					if ($(ui.item).parent('#selected_questions').length > 0) {
						$(ui.item).parent('#selected_questions').removeClass('connected');
					}

					var q = $(ui.item).html();
					var h = '<div class="panel panel-default panel-body">\
                        <label class="question_text">'+q+'</label>\
                        <div class="pull-right">\
                        <button type="button" class="btn btn-xs btn-link question_collapse_btn">Collapse</button>\
                         | <button type="button" class="btn btn-xs btn-primary add_sub_search"><i class="fa fa-plus"></i> Add Search</button>\
                        <button type="button" class="btn btn-xs btn-primary add_sub_question"><i class="fa fa-plus"></i> Add Answer</button>\
                         | <button type="button" class="btn btn-xs btn-danger delete_question"><i class="fa fa-times"></i></button>\
                        </div>\
                        <div class="clearfix"></div>\
                        <div class="sub_question_area"></div>\
                        </div>';
					$(ui.item).html(h);

					drag_drop();
				}
			}).disableSelection();
		}

		$(document).ready(function () {
			drag_drop();
		})


		$(document).on('click', '.question_collapse_btn', function (e) {
			e.stopImmediatePropagation();

			if ($(this).parents('.question:first').find('.sub_question_area:first').is(':visible')) {
				$(this).parents('.question:first').find('.sub_question_area:first').slideUp();
			} else {
				$(this).parents('.question:first').find('.sub_question_area:first').slideDown();
			}
		});

		$(document).on('click', '.add_sub_search', function (e) {
			e.stopImmediatePropagation();

			var c = prompt('Please enter answer.');

			if (c && c != '') {
				var h = '<div class="sub_question_drop">\
                    <label class="question_category">'+c+'</label>\
                    <div class="pull-right"><button type="button" class="btn btn-xs btn-danger delete_answer"><i class="fa fa-times"></i></button></div>\
                    <input type="text" class="form-control search_url" placeholder="Search URL">\
                    </div>';

				$(this).parents('.panel:first').find('.sub_question_area:first').append(h);
				$(this).parents('.panel:first').find('.sub_question_area:first > .sub_question_drop:last').effect('highlight', {}, 500);
				drag_drop();
			}

		});

		$(document).on('click', '.add_sub_question', function (e) {
			e.stopImmediatePropagation();

			var c = prompt('Please enter answer.');
			//$(this).prev('.add_sub_search').attr('disabled', true);

			if (c && c != '') {
				var h = '<div class="sub_question_drop">\
                    <label class="question_category">'+c+'</label>\
                    <div class="pull-right"><button type="button" class="btn btn-xs btn-danger delete_answer"><i class="fa fa-times"></i></button></div>\
                    <div class="question_tree connected"></div>\
                    </div>';

				$(this).parents('.panel:first').find('.sub_question_area:first').append(h);
				$(this).parents('.panel:first').find('.sub_question_area:first > .sub_question_drop:last').effect('highlight', {}, 500);
				drag_drop();
			}
		});

		$(document).on('click', '.delete_question', function (e) {
			e.stopImmediatePropagation();

			var v = $(this).parents('.panel:first').find('.question_text:first').text();
			$('#question_list').append('<div class="question">'+v+'</div>');
			$(this).parents('.question:first').remove();

			if ($('#selected_questions > .question').length == 0) {
				$('#selected_questions').addClass('connected');
				drag_drop();
			}
		});


		$(document).on('click', '.delete_answer', function (e) {
			e.stopImmediatePropagation();

			$(this).parents('.sub_question_drop:first').remove();
		});

		function guid() {
			function s4() {
				return Math.floor((1 + Math.random()) * 0x10000)
					.toString(16)
					.substring(1);
			}
			return s4() + s4() + '-' + s4() + s4();
		}

		$(document).on('click', '.generate_questions', function (e) {
			e.stopImmediatePropagation();

			var valid = true;

			var final_obj = [];
			function _get_question($selector, obj_arr) {
				if ($selector.find('> .question').length == 0) {
					alert('Atleast one question is required');
					$selector.effect('highlight', {}, 500);
					valid = false;
					return;
				}


				$selector.find('> .question').each(function (e) {
					var obj = {};
					var q = $(this).find('.question_text:first').text();
					obj.question = q;
					obj.unique_id = guid();

					if ($(this).find('.sub_question_area:first > .sub_question_drop').length > 0) {
						obj.answer = [];
						$(this).find('.sub_question_area:first > .sub_question_drop').each(function (e) {
							obj.answer.push({
								answer_text: $(this).find('.question_category:first').text(),
								unique_id: guid()
							});

							if ($(this).find('> .question_tree').length > 0) {
								obj.answer[obj.answer.length - 1].answer_type = 'question';
								obj.answer[obj.answer.length - 1].questions = _get_question($(this).find('.question_tree:first'), []);
							} else {
								obj.answer[obj.answer.length - 1].answer_type = 'search';
								obj.answer[obj.answer.length - 1].search_url = $(this).find('.search_url:first').val();

								if ($(this).find('.search_url:first').val() == '') {
									alert('Please enter search url');
									$(this).find('.search_url:first').parents('.sub_question_drop:first').effect('highlight', {}, 500);
									valid = false;
								}
							}
						});
					} else {
						alert('Atleast one answer is required');
						$(this).find('> .panel').effect('highlight', {}, 500);
						valid = false;
					}

					obj_arr.push(obj);
				});

				return obj_arr;
			}

			var final_arr = _get_question($('#selected_questions'), final_obj);
			console.log(final_arr);

			$('.question_final_array').val(JSON.stringify(final_arr));

			if (valid) {
				$('#question_form').submit();
			}
		});

	</script>
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Question
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Questions</h3>
                </div>
                <div class="box-body form-horizontal">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

						<div class="form-group row" id="new_question_area">
							<div class="col-sm-4">
								<input type="text" class="form-control new_question_text" placeholder="Question">
							</div>
							<div class="col-sm-4">
								<button type="button" class="btn btn-success new_question_btn"><i class="fa fa-plus"></i> Add Question</button>
							</div>
						</div>

                    {!! Form::open(array('url' => 'question/update', 'enctype' => 'multipart/form-data', 'id' => 'question_form')) !!}
                    {!! Form::hidden('_token', csrf_token()) !!}

                    @include('admin.questions.partials.form', $questions)

                    <div class="row">
                        <div class="col-sm-2 col-sm-offset-10">
							<input type="hidden" name="question_id" value="<?php echo $question_id;?>">
							<button type="button" class="btn btn-primary btn-block generate_questions">Update Question</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

@endsection
