
<style>
    #question_list .question {
        background: #f3f3f3;
        padding: 10px;
        border-radius: 4px;
        border: 1px solid #ccc;
        cursor: move;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        margin-top: 10px;
		margin-bottom: 5px;
    }
    #question_list .question:last-child {
        margin-bottom: 0;
    }

    .question_tree {
        min-height: 60px;
        position: relative;
    }
    .question_tree:after {
        content: 'DROP QUESTIONS HERE';
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        opacity: 0.4;
        z-index: 1;
    }

    #selected_questions {
        min-height: 200px;
    }
    #selected_questions > .question > .panel {
        min-height: 200px;
    }
    .sub_question_area {
        margin-top: 10px;
    }

    .question_tree .question {
        background: #fff;
        margin-bottom: 20px;
        position: relative;
        z-index: 2;
    }
    .question_tree .question:last-child {
        margin-bottom: 0;
    }
    .question_tree .question .panel {
        margin-bottom: 0;
    }


    .sub_question_drop {
        background: #f9f9f9;
        padding: 10px;
        border: 1px solid #eee;
        margin-bottom: 20px;
    }
    .sub_question_drop:last-child {
        margin-bottom: 0;
    }

    .question_placeholder {
        background: #f9f9f9;
        margin-bottom: 10px;
        height: 40px;
        border: 1px dashed #eee;
    }

    #selected_questions > .question .panel {
        border-right: 0;
        padding-right: 5px;
        margin-right: -5px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }
    #selected_questions > .question > .panel {
        padding-right: 5px;
        border-right: 1px solid #ccc;
    }
    .sub_question_drop {
        padding-right: 5px;
        margin-right: -5px;
        border-right: 0 !important;
    }
</style>



<hr>

<div class="row">
    <div class="col-sm-4">
        <div class="panel panel-default" id="question_list_panel">
            <div class="panel-heading">Question List</div>
            <div class="panel-body connected" id="question_list">
                <?php
				if (isset($questions)) {
				    $options = json_decode($questions->option);

                    function _print_question_list($questions) {
						foreach ($questions as $question) {
							echo '<div class="question"> '.$question->question.'</div>';

							if (isset($question->answer)) {
								foreach ($question->answer as $answer) {
                                    if ($answer->answer_type == 'question') {
										_print_question_list($answer->questions);
                                    }
								}
                            }
                        }
                    }

					echo _print_question_list($options);
				}
                ?>
            </div>
        </div>
    </div>


    <div class="col-sm-8">
        {{--<pre>--}}
            <?php //if (isset($questions)) { print_r(json_decode($questions->option)); } ?>
        {{--</pre>--}}
        <div class="panel panel-default">
            <div class="panel-heading">Selected Questions</div>
            <div class="panel-body question_tree <?= (!isset($questions)) ? 'connected' : '' ?>" id="selected_questions">
                <?php
				if (isset($questions)) {
					$options = json_decode($questions->option);

					function _print_question($questions) {
						$html = '';
						foreach ($questions as $question) {
							$html .= '<div class="question ui-sortable-handle" style=""><div class="panel panel-default panel-body">
                                    <label class="question_text">'.$question->question.'</label>
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-xs btn-link question_collapse_btn">Collapse</button>
                                         | <button type="button" class="btn btn-xs btn-primary add_sub_search"><i class="fa fa-plus"></i> Add Search</button>
                                        <button type="button" class="btn btn-xs btn-primary add_sub_question"><i class="fa fa-plus"></i> Add Answer</button>
                                        | <button type="button" class="btn btn-xs btn-danger delete_question"><i class="fa fa-times"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="sub_question_area">';

							if (isset($question->answer)) {

								foreach ($question->answer as $answer) {

									if ($answer->answer_type == 'search') {
										$html .= '<div class="sub_question_drop">
                                                <label class="question_category">'.$answer->answer_text.'</label>
                                                <div class="pull-right"><button type="button" class="btn btn-xs btn-danger delete_answer"><i class="fa fa-times"></i></button></div>
                                                <input type="text" class="form-control search_url" placeholder="Search URL" value="'.$answer->search_url.'">
                                                </div>';
                                    } elseif ($answer->answer_type == 'question') {
										$html .= '<div class="sub_question_drop">
                                                <label class="question_category">'.$answer->answer_text.'</label>
                                                <div class="pull-right"><button type="button" class="btn btn-xs btn-danger delete_answer"><i class="fa fa-times"></i></button></div>
                                                <div class="question_tree connected ui-sortable">'._print_question($answer->questions).'</div>
                                                </div>';

                                    }

                                }

                            }

							$html .= '</div>
                                    </div>
                                 </div>';

                        }
						return $html;
                    }
					echo _print_question($options);
                }
                ?>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="question_final_array" name="question_final_array">
