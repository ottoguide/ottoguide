<?php include('header.php');
?>
  <!-- Left side column. contains the logo and sidebar -->
   <?php include('sidebar.php');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
      
    </section>

    <!-- Main content -->
    <section class="content">
      
   <h4>Dealer Detail:</h4>
   <div class="alert alert-warning">
    <strong>Warning!</strong> Dealer Unblocked successfully
  </div>
   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

   <sub>Last Activity on: 27-Feb-2018 4:50pm </sub>

   <h4>Dealer Subscription:</h4>

   <table class="table table-striped test2">
    <thead>
      <tr class="test3">
        <th class="">Subscription Name</th>
        <th class="">Expired On</th>
        <th class="">Current Status</th>
        <th class="">Amount</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Golden</td>
        <td>Next month</td>
        <td>active</td>
        <td>$200</td>
        
      </tr>
      <tr>
        <td>Golden</td>
        <td>Next month</td>
        <td>active</td>
        <td>$200</td>
      </tr>
      
      
    </tbody>
  </table>

  <h4 class="fz19"><b>Total Amount Paid:</b> $200</h4>
  
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php include('footer.php');
?>