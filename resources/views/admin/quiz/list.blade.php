@extends('layouts.admin')
@section('content')
    <style>
        .widget-user .widget-user-header {
            height: auto;
        }
        .widget-user .box-footer {
            padding-top: 10px;
        }


        /* The switch - the box around the slider */
        .switch {
            position: relative;
            display: inline-block;
            width: 40px;
            height: 20px;
            margin: 5px 0 0;
        }
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }
        .switch .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            border-radius: 34px;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .switch .slider:before {
            position: absolute;
            content: "";
            height: 15px;
            width: 15px;
            left: 4px;
            bottom: 3px;
            border-radius: 50%;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .switch input:checked + .slider {
            background-color: #2196F3;
        }
        .switch input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }
        .switch input:checked + .slider:before {
            -webkit-transform: translateX(18px);
            -ms-transform: translateX(18px);
            transform: translateX(18px);
        }
    </style>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Quiz
                <small>List</small>
            </h1>
        </section>
        <!-- Main content -->
        <section class="content">

            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <div class="row">
                <div class="col-sm-3">
                    <div class="box box-widget widget-user-2 open-add-modal" style="cursor: pointer">
                        <div class="widget-user-header" style="padding: 55px 20px">
                            <h3 class="widget-user-username">Add New Quiz</h3>
                        </div>
                    </div>
                </div>

                <?php foreach ($quiz as $q) { ?>
                <div class="col-sm-3">
                    <div class="box box-widget widget-user">
                        <div class="widget-user-header bg-aqua-active">
                            <h3 class="widget-user-username"><?= $q->title ?></h3>
                            <?php
                                if ($q->is_active == 1) {
                                	echo '<p class="label label-success published_quiz">Published</p>';
                                    echo '<p class="label label-warning unpublished_quiz" style="display: none">Unpublished</p>';
                                } else {
                                    echo '<p class="label label-success published_quiz" style="display: none">Published</p>';
                                    echo '<p class="label label-warning unpublished_quiz" >Unpublished</p>';
                                }
                            ?>
                        </div>

                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-4 border-right">
                                    <div class="switch-button" data-id="{{$q->id}}">
                                    <label class="switch">
                                        <input type="checkbox" {{ ($q->is_active == 1) ? 'checked' : '' }}>
                                        <span class="slider"></span>
                                    </label>
                                    </div>
                                </div>
                                <div class="col-sm-8 text-right">
                                    <a href="<?= url('/quiz/' . Common::my_id($q->id). '/delete') ?>" class="btn btn-danger btn-ms">Delete</a>
                                    <a href="<?= url('/quiz/' . Common::my_id($q->id). '/edit') ?>" class="btn btn-default btn-ms">Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>


            </div>


        </section>
    </div>

    <!-- Lightbox for  -->
    <div class="modal fade" id="add-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Quiz</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('/quiz/add')}}" method="post" id="add-form">
                        <div class="alert alert-danger validate-error-msg" style="display: none"></div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="form-group">
                            <label>Quiz Title</label>
                            <input type="text" class="form-control" name="quiz_title">
                        </div>
                        <div class="form-group">
                            <label>Quiz Description</label>
                            <textarea name="quiz_description" class="form-control" rows="3"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button"
                            data-form-id="add-form"
                            data-validate-loader=""
                            data-validate-url="/quiz/add"
                            class="btn btn-success btn-form-submit">Submit</button>
                </div>
            </div>
        </div>
    </div> <!-- /.modal -->
@endsection

@section('page_script')
    <script>

		$(document).on('click', '.open-add-modal', function (e) {
			e.stopImmediatePropagation();

			//$('#add-form')

			$('#add-modal').modal({backdrop: 'static', keyboard: false, show: true}); // open lightbox
		});

        $(".switch-button").find("input[type=checkbox]").on("change",function() {
            var $this = $(this);
            var q_id = $(this).parents('.switch-button').attr("data-id");
            var status = 0;
            if($(this).prop('checked') === true){
                status = 1
            }

            $.ajax({
                url: '/publish_quiz',
                type: 'POST',
                data: {_token: "{{ csrf_token() }}",id:q_id,status:status},
                dataType: 'JSON',
                success: function (data) {
                    if(data.is_active == 1){
                        $this.parents('.widget-user').find(".published_quiz").show();
                        $this.parents('.widget-user').find(".unpublished_quiz").hide();
                    }
                    else{
                        $this.parents('.widget-user').find(".unpublished_quiz").show();
                        $this.parents('.widget-user').find(".published_quiz").hide();
                    }
                }
            });
        });

    </script>
@endsection