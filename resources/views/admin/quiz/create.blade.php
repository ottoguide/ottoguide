@extends('layouts.admin')
@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />

    <style>
      #sortable2 .ui-state-default {
          display: block;
          width: 100% !important;
          padding: 0;
          margin: 0;
          position: relative;
          height: auto !important;
      }
      .question-box{
          background: #ecf0f5;
          padding: 20px 9px;
          border-radius: 5px;
          margin-bottom: 10px;
      }
      .ui-state-default{
          line-height: 1.2em;
          font-size: 18px;
          text-overflow: ellipsis;
      }
      .addition {
          margin-top: 8px;
          margin-bottom: 8px;
          border: 0;
          border-top: 1px solid #ecf0f5;
      }
      #close {
          float:right;
          display:inline-block;
          padding:2px 5px;
          background:#ef5959;
      }
      .copyCheckBox {
          float:right;
          display:inline-block;
          padding:2px 5px;
          margin-right: 5px;
      }
      .copyRadioBox {
          float:right;
          display:inline-block;
          padding:2px 5px;
          margin-right: 5px;
      }
      #close_checkbox {
          display:inline-block;
          color:#ef5959;
          padding-left: 15px;
      }
      #close_radiobox {
          display:inline-block;
          color:#ef5959;
          padding-left: 15px;
      }
      .sticky-question-box {
          position: sticky;
          top: 10px;
      }
      .switch-welcome-screen {
          position: relative;
          display: inline-block;
          width: 60px;
          height: 34px;
      }
      .switch-welcome-screen input {
          opacity: 0;
          width: 0;
          height: 0;
      }
      .slider-welcome-screen {
          position: absolute;
          cursor: pointer;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          background-color: #ccc;
          -webkit-transition: .4s;
          transition: .4s;
      }
      .slider-welcome-screen:before {
          position: absolute;
          content: "";
          height: 26px;
          width: 26px;
          left: 4px;
          bottom: 4px;
          background-color: white;
          -webkit-transition: .4s;
          transition: .4s;
      }
      input:checked + .slider-welcome-screen {
          background-color: #2196F3;
      }
      input:focus + .slider-welcome-screen {
          box-shadow: 0 0 1px #2196F3;
      }
      input:checked + .slider-welcome-screen:before {
          -webkit-transform: translateX(26px);
          -ms-transform: translateX(26px);
          transform: translateX(26px);
      }
      .slider-welcome-screen.round {
          border-radius: 34px;
      }
      .slider-welcome-screen.round:before {
          border-radius: 50%;
      }
      .fa-facebook {
          background: #3B5998;
          color: white;
          padding: 10px;
          font-size: 12px;
          width: 30px;
          text-align: center;
          text-decoration: none;
          border-radius: 50%;
      }
      .fa-twitter {
          background: #55ACEE;
          color: white;
          padding: 10px;
          font-size: 12px;
          width: 30px;
          text-align: center;
          text-decoration: none;
          border-radius: 50%;
      }
      .fa-google {
          background: #dd4b39;
          color: white;
          padding: 10px;
          font-size: 12px;
          width: 30px;
          text-align: center;
          text-decoration: none;
          border-radius: 50%;
      }
      .fa-linkedin {
          background: #007bb5;
          color: white;
          padding: 10px;
          font-size: 12px;
          width: 30px;
          text-align: center;
          text-decoration: none;
          border-radius: 50%;
      }
      .fa-youtube {
          background: #bb0000;
          color: white;
          padding: 10px;
          font-size: 12px;
          width: 30px;
          text-align: center;
          text-decoration: none;
          border-radius: 50%;
      }
      .fa-instagram {
          background: #125688;
          color: white;
          padding: 10px;
          font-size: 12px;
          width: 30px;
          text-align: center;
          text-decoration: none;
          border-radius: 50%;
      }
      .fa-pinterest {
          background: #cb2027;
          color: white;
          padding: 10px;
          font-size: 12px;
          width: 30px;
          text-align: center;
          text-decoration: none;
          border-radius: 50%;
      }
      .drag-question {
          background: #ecf0f5;
          padding: 10px 16px;
          border-radius: 13px;
          margin-bottom: 8px;
      }
  </style>
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Quiz <small><?=$quiz->title;?></small>
            </h1>
        </section>

        <section class="content">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="row" style="margin-bottom: 20px">
                <div class="col-sm-2">
                </div>
                <div class="col-sm-10 text-right">
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#welcomeScreen"><i class="fa fa-sign-in"></i> Entry and Exit Messaging </button>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#mediaLibrary" id="mediaLibraryButton"><i class="fa fa-picture-o"></i> Media Library </button>
                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#routingMap" id="routingMapButton"><i class="fa fa-sitemap"></i> Routing Map</button>
                    {{--url('/quiz/' . Common::my_id($quiz->id). '/preview')--}}
                    <a href="{{ url('faq-search.html') }}" class="btn btn-default" target="_blank"><i class="fa fa-external-link"></i> Quiz Preview</a>
                </div>
            </div>

                <!-- Modal Entry and Exit Messaging -->
                <div id="welcomeScreen" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Quiz Entry and Exit Messaging</h4>
                            </div>
                            <div class="modal-body">

                                <div>
                                    <div class="col-sm-10">
                                    </div>
                                        <div class="col-sm-2">
                                            <label class="switch-welcome-screen">
                                                <input type="checkbox" id="welcomeToggle">
                                                <span class="slider-welcome-screen round"></span>
                                            </label>
                                        </div>

                                </div>
                                <div class="row welcomeScreenContent" style="display: none;">

                                    <div class="col-sm-12 form-group">
                                        <div class="col-sm-4">
                                            <label for="welcomeMessage">Welcome Message*</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input value="" type="text" id="welcomeMessage" name="welcomeHeading" class="form-control" placeholder="Welcome to the Family Car Quiz">
                                        </div>
                                    </div>

                                    <div class="col-sm-12 form-group">
                                        <div class="col-sm-4">
                                            <label for="thankyouMessage">Thank You Message*</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input value="" type="text" id="thankyouMessage" name="thankyouMessage" class="form-control" placeholder="Thank You Message">
                                        </div>
                                    </div>


                                        <div class="col-sm-12 form-group">
                                            <div class="col-sm-4">
                                                <label for="welcomeDescription">Description</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <textarea id="welcomeDescription" name="welcomeDescription" class="form-control" placeholder="Description"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 form-group">
                                            <div class="col-sm-4">
                                                <label for="welcomeVariables">Variables</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input value="" type="text" id="welcomeVariables" name="welcomeVariables" class="form-control" placeholder="">
                                            </div>
                                        </div>

                                        <div class="col-sm-12 form-group">
                                            <div class="col-sm-4">
                                                <label for="welcomeMedia">Media</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input value="" type="text" id="welcomeMedia" name="welcomeMedia" class="form-control" placeholder="Image or Video URL">
                                            </div>
                                        </div>

                                        <div class="col-sm-12 form-group">
                                            <div class="col-sm-4">
                                                <label for="welcomeActionButtonLabel">Action Button Label</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input value="" type="text" id="welcomeActionButtonLabel" name="welcomeActionButtonLabel" class="form-control" placeholder="Start My Quiz">
                                            </div>
                                        </div>
                                    <div class="col-sm-12 form-group">
                                        <div class="col-sm-4">
                                            <label>Social Media Links</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <i class="fa fa-facebook"></i>&nbsp;&nbsp;<input value="" type="text" id="facebookLink" name="facebookLink" style="width: 314px;"><br>
                                            <i class="fa fa-twitter"></i>&nbsp;&nbsp;<input value="" type="text" id="twitterLink" name="twitterLink" style="width: 314px;"><br>
                                            <i class="fa fa-google"></i>&nbsp;&nbsp;<input value="" type="text" id="googleLink" name="googleLink" style="width: 314px;"><br>
                                            <i class="fa fa-linkedin"></i>&nbsp;&nbsp;<input value="" type="text" id="linkedinLink" name="linkedinLink" style="width: 314px;"><br>
                                            <i class="fa fa-youtube"></i>&nbsp;&nbsp;<input value="" type="text" id="youtubeLink" name="youtubeLink" style="width: 314px;"><br>
                                            <i class="fa fa-instagram"></i>&nbsp;&nbsp;<input value="" type="text" id="instagramLink" name="instagramLink" style="width: 314px;"><br>
                                            <i class="fa fa-pinterest"></i>&nbsp;&nbsp;<input value="" type="text" id="pinterestLink" name="pinterestLink" style="width: 314px;"><br>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <p style="color: #ff0000">* required</p>
                                    </div>

                                </div>
                                <br/>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-info" id="saveWelcomeScreen">Save</button>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Modal Media Library-->
                <div id="mediaLibrary" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Media Library</h4>
                            </div>
                            <div class="modal-body">

                                <div class="file-loading">
                                    <input id="image-file" type="file" name="file" accept="image/*" data-min-file-count="1" multiple>
                                </div>
                                <hr>
                                <h4><b>Image list</b></h4>
                                <div class="file-list">
                                    loading...
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Modal Routing Map -->
                <div id="routingMap" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Routing Map</h4>
                            </div>
                            <div class="modal-body">
                                <div class="routingData" style="overflow-x:auto;">
                                    <button class="btn btn-info btn-xs" onclick="new_route();"><i class="fa fa-plus"></i> Add New Route</button>
                                    <table style="width:800px;margin:0;" cellpadding="0" cellspacing="0" border="0" id="routeTable">
                                        <tr style="height:40px;margin:0;">
                                            <th>If Question</th>
                                            <th>Response is</th>
                                            <th>Next Question</th>
                                            <th>Delete</th>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" id="saveRouting" class="btn btn-info" >Save</button>
                            </div>
                        </div>

                    </div>
                </div>


            <div class="row">
                <div class="col-sm-3 sticky-question-box">
                    <div class=" box">
                        <div class="box-header">Drag question from here</div>
                        <div class="box-body">
                            <div id="sortable1" class="connect_fields droptrue" style="min-height: 182px;">
                                <div class="ui-state-default drag-question" data-type="checkbox" data-subtype="text">
                                    <i class="fa fa-check-square-o" style="margin-right: 9px;  margin-bottom: 10px;"></i>Multiple Text Choice
                                </div>
                                <hr class="addition">
                                <div class="ui-state-default drag-question" data-type="radio" data-subtype="text">
                                    <i class="fa fa-circle-o" style="margin-right: 9px;  margin-bottom: 10px;"></i>Single Text Choice
                                </div>
                                <hr class="addition">
                                <div class="ui-state-default drag-question" data-type="checkbox" data-subtype="image">
                                    <i class="fa fa-files-o" style="margin-right: 9px;  margin-bottom: 10px;"></i>Multiple Image Choice
                                </div>
                                <hr class="addition">
                                <div class="ui-state-default drag-question" data-type="radio" data-subtype="image">
                                    <i class="fa fa-picture-o" style="margin-right: 9px;  margin-bottom: 10px;"></i>Single Image Choice
                                </div>
                                <hr class="addition">
                                <div class="ui-state-default drag-question" data-type="dropdown">
                                    <i class="fa fa-caret-down" style="margin-right: 9px;  margin-bottom: 10px;"></i>Drop Down
                                </div>
                                <hr class="addition">
                                <div class="ui-state-default drag-question" data-type="radio" data-subtype="yes_no">
                                    <i class="fa fa-question-circle" style="margin-right: 9px;  margin-bottom: 10px;"></i>Yes/No
                                </div>
                                <hr class="addition">
                                <div class="ui-state-default drag-question" data-type="scale">
                                    <i class="fa fa-arrows-h" style="margin-right: 9px;  margin-bottom: 10px;"></i>Opinion Scale
                                </div>
                                <hr class="addition">
                                <div class="ui-state-default drag-question" data-type="rating">
                                    <i class="fa fa-star" style="margin-right: 9px;  margin-bottom: 10px;"></i>Rating
                                </div>
                                <hr class="addition">
                                <div class="ui-state-default drag-question" data-type="date">
                                    <i class="fa fa-calendar" style="margin-right: 9px;  margin-bottom: 10px;"></i>Date
                                </div>
                                <hr class="addition">
                                <div class="ui-state-default drag-question" data-type="number">
                                    <i class="fa fa-list-ol" style="margin-right: 9px;  margin-bottom: 10px;"></i>Number
                                </div>
                                <hr class="addition">
                                <div class="ui-state-default drag-question" data-type="hidden">
                                    <i class="fa fa-eye-slash" style="margin-right: 9px;  margin-bottom: 10px;"></i>Variable
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="col-sm-9">
                    <div class=" box">
                        <div class="box-header">Drop Questions Here</div>
                            <div class="box-body">
                                <div id="sortable2" class="connect_fields dropfalse" style="min-height: 700px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px">
                    <div class="col-sm-2">
                    </div>
                    <div class="col-sm-10 text-right">
                        <button type="button" class="btn btn-info" id="saveQuiz"><i class="fa fa-save"></i> Save Quiz</button>
                    </div>
                </div>
        </section>
    </div>
@endsection

@section('page_script')
    <script>

        $(function() {
            $( "#sortable1 > .ui-state-default" ).draggable({
                connectToSortable: "#sortable2",
                revert: "invalid", // bounce back when dropped
                helper: "clone", // create "copy" with original
        });

            $( "#sortable2" ).sortable({
                //connectWith: ".connect_fields",
                revert: true, // bounce back when dropped
                helper: "clone", // create "copy" with original
                update: function (event, ui) {
                    var count = $( "#sortable2 > .ui-state-default" ).length;
                    var uniqueKey = makeUniqueKey();
                    if ($(ui.item).attr('data-type') == 'checkbox' && $(ui.item).attr('data-subtype') == 'text') {
                        $(ui.item).html(
                            '<div style="border: dashed" class="question-box question-box-id-'+uniqueKey+'" data-type="checkbox" data-subtype="text"  id="'+uniqueKey+'"> <button class="btn btn-danger" id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"> <i class="fa fa-trash"></i></button> <button class="btn btn-info copyCheckBox" id="copyCheckBox'+uniqueKey+'" onclick="copyCheckBox('+uniqueKey+');"> <i class="fa fa-copy"></i></button>' +
                            '<p>Question Type: <strong>Multiple Text Choice</strong></p><br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="enableDisableDynamicData'+uniqueKey+'" id="'+uniqueKey+'" onchange="enable_disable_dynamic_api_data('+uniqueKey+',this)"> Dynamic API Data' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPI'+uniqueKey+'" style="display: none">' +
                            '<div class="mainCategory'+uniqueKey+'">' +
                            '<select onchange="load_param($(this).val(),'+uniqueKey+');" id="mainCategory'+uniqueKey+'">' +
                            '<option value="main_category">-Select Feature Category-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<input type="checkbox" class="resposneRequired_'+uniqueKey+'"> Required <br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="saveResposneForQuery'+uniqueKey+'" onchange="ResposneForQuery('+uniqueKey+',this)"> Save Response for Query' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPIforSave'+uniqueKey+'" style="display: none">' +
                            '<select  id="mainCategoryforSave'+uniqueKey+'" >' +
                            '<option value="main_category">-Select Query Param-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<h3><span class="label label-default click-to-edit-question" for="textBox'+uniqueKey+'">Double click to Edit Question</span></h3><input value="" type="text" id="textBox'+uniqueKey+'" name="textBox'+uniqueKey+'" class="click-to-save-question-name" hidden>' +
                            '<h4><span class="label label-default click-to-edit-question-description" for="textBoxDescription'+uniqueKey+'">Double click to Edit Question Description </span></h4><input value="" type="text" id="textBoxDescription'+uniqueKey+'" name="textBoxDescription'+uniqueKey+'" class="click-to-save-question-description" hidden>' +
                            '<input type="hidden" id="subtype'+uniqueKey+'" value="text">' +
                            '<p class="add_new_options'+uniqueKey+'" style="display: block">To add options type in input box and click add &nbsp;&nbsp; <input type="text" id="checkboxInput'+uniqueKey+'" autofocus placeholder="Enter Check Box Item" />&nbsp;<input type="button" id="addcheckbox'+uniqueKey+'" value="Add" onclick="createChk('+uniqueKey+',checkboxInput'+uniqueKey+',subtype'+uniqueKey+');" /></p>' +
                            '<br><br>' +
                            '<div id="checkboxContainer'+uniqueKey+'">' +
                            '</div>' +
                            '</div>');
                    }
                    if ($(ui.item).attr('data-type') == 'checkbox' && $(ui.item).attr('data-subtype') == 'image') {
                        $(ui.item).html(
                            '<div style="border: dashed" class="question-box question-box-id-'+uniqueKey+'" data-type="checkbox" data-subtype="image"  id="'+uniqueKey+'"> <button class="btn btn-danger" id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"> <i class="fa fa-trash"></i></button> <button class="btn btn-info copyCheckBox" id="copyCheckBox'+uniqueKey+'" onclick="copyCheckBox('+uniqueKey+');"> <i class="fa fa-copy"></i></button>' +
                            '<p>Question Type: <strong>Multiple Image Choice</strong></p><br>' +
                            '<div class="row" style="display: none">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="enableDisableDynamicData'+uniqueKey+'" id="'+uniqueKey+'" onchange="enable_disable_dynamic_api_data('+uniqueKey+',this)"> Dynamic API Data' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPI'+uniqueKey+'" style="display: none">' +
                            '<div class="mainCategory'+uniqueKey+'">' +
                            '<select onchange="load_param($(this).val(),'+uniqueKey+');" id="mainCategory'+uniqueKey+'">' +
                            '<option value="main_category">-Select Feature Category-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<input type="checkbox" class="resposneRequired_'+uniqueKey+'"> Required <br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="saveResposneForQuery'+uniqueKey+'" onchange="ResposneForQuery('+uniqueKey+',this)"> Save Response for Query' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPIforSave'+uniqueKey+'" style="display: none">' +
                            '<select  id="mainCategoryforSave'+uniqueKey+'" >' +
                            '<option value="main_category">-Select Query Param-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<h3><span class="label label-default click-to-edit-question" for="textBox'+uniqueKey+'">Double click to Edit Question</span></h3><input value="" type="text" id="textBox'+uniqueKey+'" name="textBox'+uniqueKey+'" class="click-to-save-question-name" hidden>' +
                            '<h4><span class="label label-default click-to-edit-question-description" for="textBoxDescription'+uniqueKey+'">Double click to Edit Question Description </span></h4><input value="" type="text" id="textBoxDescription'+uniqueKey+'" name="textBoxDescription'+uniqueKey+'" class="click-to-save-question-description" hidden>' +
                            '<input type="hidden" id="subtype'+uniqueKey+'" value="image">' +
                            '<p class="add_new_options'+uniqueKey+'" style="display: block">To add options type in input box and click add &nbsp;&nbsp; <input type="text" id="checkboxInput'+uniqueKey+'" autofocus placeholder="Enter Check Box Item" />&nbsp;<input type="button" id="addcheckbox'+uniqueKey+'" value="Add" onclick="createChk('+uniqueKey+',checkboxInput'+uniqueKey+',subtype'+uniqueKey+');" /></p>' +
                            '<br><br>' +
                            '<div id="checkboxContainer'+uniqueKey+'">' +
                            '</div>' +
                            '</div>');
                    }
                    if ($(ui.item).attr('data-type') == 'radio' && $(ui.item).attr('data-subtype') == 'text') {
                        $(ui.item).html(
                            '<div style="border: dashed" class="question-box question-box-id-'+uniqueKey+'" data-type="radio" data-subtype="text"  id="'+uniqueKey+'"> <button class="btn btn-danger" id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"> <i class="fa fa-trash"></i></button> <button class="btn btn-info copyRadioBox" id="copyRadioBox'+uniqueKey+'" onclick="copyRadioBox('+uniqueKey+');"> <i class="fa fa-copy"></i></button>' +
                            '<p>Question Type: <strong>Single Text Choice</strong></p><br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="enableDisableDynamicData'+uniqueKey+'" id="'+uniqueKey+'" onchange="enable_disable_dynamic_api_data_for_radio('+uniqueKey+',this)"> Dynamic API Data' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPI'+uniqueKey+'" style="display: none">' +
                            '<div class="mainCategory'+uniqueKey+'">' +
                            '<select onchange="load_param_radio($(this).val(),'+uniqueKey+');" id="mainCategory'+uniqueKey+'">' +
                            '<option value="main_category">-Select Feature Category-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<input type="checkbox" class="resposneRequired_'+uniqueKey+'"> Required <br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="saveResposneForQuery'+uniqueKey+'" onchange="ResposneForQuery('+uniqueKey+',this)"> Save Response for Query' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPIforSave'+uniqueKey+'" style="display: none">' +
                            '<select  id="mainCategoryforSave'+uniqueKey+'" >' +
                            '<option value="main_category">-Select Query Param-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<h3><span class="label label-default click-to-edit-question" for="textBox'+uniqueKey+'">Double click to Edit Question</span></h3><input value="" type="text" id="textBox'+uniqueKey+'" name="textBox'+uniqueKey+'" class="click-to-save-question-name" hidden>' +
                            '<h4><span class="label label-default click-to-edit-question-description" for="textBoxDescription'+uniqueKey+'">Double click to Edit Question Description </span></h4><input value="" type="text" id="textBoxDescription'+uniqueKey+'" name="textBoxDescription'+uniqueKey+'" class="click-to-save-question-description" hidden>' +
                            '<input type="hidden" id="subtype'+uniqueKey+'" value="text">' +
                            '<p class="add_new_options'+uniqueKey+'" style="display: block">To add options type in input box and click add &nbsp;&nbsp; <input type="text" id="radioboxInput'+uniqueKey+'" autofocus placeholder="Enter Item" />&nbsp;<input type="button" id="addradiobox'+uniqueKey+'" value="Add" onclick="createradio('+uniqueKey+',radioboxInput'+uniqueKey+',subtype'+uniqueKey+');" /></p>' +
                            '<br><br>' +
                            '<div id="radioboxContainer'+uniqueKey+'">' +
                            '</div>' +
                            '</div>');
                    }
                    if ($(ui.item).attr('data-type') == 'radio' && $(ui.item).attr('data-subtype') == 'image') {
                        $(ui.item).html(
                            '<div style="border: dashed" class="question-box question-box-id-'+uniqueKey+'" data-type="radio" data-subtype="image"  id="'+uniqueKey+'"> <button class="btn btn-danger" id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"> <i class="fa fa-trash"></i></button> <button class="btn btn-info copyRadioBox" id="copyRadioBox'+uniqueKey+'" onclick="copyRadioBox('+uniqueKey+');"> <i class="fa fa-copy"></i></button>' +
                            '<p>Question Type: <strong>Single Image Choice</strong></p><br>' +
                            '<div class="row" style="display: none">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="enableDisableDynamicData'+uniqueKey+'" id="'+uniqueKey+'" onchange="enable_disable_dynamic_api_data_for_radio('+uniqueKey+',this)"> Dynamic API Data' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPI'+uniqueKey+'" style="display: none">' +
                            '<div class="mainCategory'+uniqueKey+'">' +
                            '<select onchange="load_param_radio($(this).val(),'+uniqueKey+');" id="mainCategory'+uniqueKey+'">' +
                            '<option value="main_category">-Select Feature Category-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<input type="checkbox" class="resposneRequired_'+uniqueKey+'"> Required <br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="saveResposneForQuery'+uniqueKey+'" onchange="ResposneForQuery('+uniqueKey+',this)"> Save Response for Query' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPIforSave'+uniqueKey+'" style="display: none">' +
                            '<select  id="mainCategoryforSave'+uniqueKey+'" >' +
                            '<option value="main_category">-Select Query Param-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<h3><span class="label label-default click-to-edit-question" for="textBox'+uniqueKey+'">Double click to Edit Question</span></h3><input value="" type="text" id="textBox'+uniqueKey+'" name="textBox'+uniqueKey+'" class="click-to-save-question-name" hidden>' +
                            '<h4><span class="label label-default click-to-edit-question-description" for="textBoxDescription'+uniqueKey+'">Double click to Edit Question Description </span></h4><input value="" type="text" id="textBoxDescription'+uniqueKey+'" name="textBoxDescription'+uniqueKey+'" class="click-to-save-question-description" hidden>' +
                            '<input type="hidden" id="subtype'+uniqueKey+'" value="image">' +
                            '<p class="add_new_options'+uniqueKey+'" style="display: block">To add options type in input box and click add &nbsp;&nbsp; <input type="text" id="radioboxInput'+uniqueKey+'" autofocus placeholder="Enter Item" />&nbsp;<input type="button" id="addradiobox'+uniqueKey+'" value="Add" onclick="createradio('+uniqueKey+',radioboxInput'+uniqueKey+',subtype'+uniqueKey+');" /></p>' +
                            '<br><br>' +
                            '<div id="radioboxContainer'+uniqueKey+'">' +
                            '</div>' +
                            '</div>');
                    }
                    if ($(ui.item).attr('data-type') == 'radio' && $(ui.item).attr('data-subtype') == 'yes_no') {
                        $(ui.item).html(
                            '<div style="border: dashed" class="question-box question-box-id-'+uniqueKey+'" data-type="yes_no"  id="'+uniqueKey+'"> <button class="btn btn-danger" id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"> <i class="fa fa-trash"></i></button> <button class="btn btn-info copyRadioBox" id="copyYesNo'+uniqueKey+'" onclick="copyYesNo('+uniqueKey+');"> <i class="fa fa-copy"></i></button>' +
                            '<p>Question Type: <strong>Yes/No</strong></p><br>' +
                            '<input type="checkbox" class="resposneRequired_'+uniqueKey+'"> Required <br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="saveResposneForQuery'+uniqueKey+'" onchange="ResposneForQuery('+uniqueKey+',this)"> Save Response for Query' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPIforSave'+uniqueKey+'" style="display: none">' +
                            '<select  id="mainCategoryforSave'+uniqueKey+'" >' +
                            '<option value="main_category">-Select Query Param-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<h3><span class="label label-default click-to-edit-question" for="textBox'+uniqueKey+'">Double click to Edit Question</span></h3><input value="" type="text" id="textBox'+uniqueKey+'" name="textBox'+uniqueKey+'" class="click-to-save-question-name" hidden>' +
                            '<h4><span class="label label-default click-to-edit-question-description" for="textBoxDescription'+uniqueKey+'">Double click to Edit Question Description </span></h4><input value="" type="text" id="textBoxDescription'+uniqueKey+'" name="textBoxDescription'+uniqueKey+'" class="click-to-save-question-description" hidden>' +
                            '<br>' +
                            '<div style="display: none" id="radioboxContainer'+uniqueKey+'">' +
                            '<div style="padding: 5px 5px 5px 5px;" class="radioboxItem-'+uniqueKey+'-1"><input type="radio" id="radioboxItem-'+uniqueKey+'-1" value="Yes" name="radioboxItem'+uniqueKey+'"> &nbsp; <label> Yes </label></div>' +
                            '<div style="padding: 5px 5px 5px 5px;" class="radioboxItem-'+uniqueKey+'-2"><input type="radio" id="radioboxItem-'+uniqueKey+'-2" value="No" name="radioboxItem'+uniqueKey+'"> &nbsp; <label> No </label></div>' +
                            '</div>' +
                            '</div>');
                    }
                    if ($(ui.item).attr('data-type') == 'date') {
                        $(ui.item).html(
                            '<div style="border: dashed" class="question-box question-box-id-'+uniqueKey+'" data-type="date"  id="'+uniqueKey+'"> <button class="btn btn-danger" id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"> <i class="fa fa-trash"></i></button> <button class="btn btn-info copyRadioBox" id="copyDate'+uniqueKey+'" onclick="copyDate('+uniqueKey+');"> <i class="fa fa-copy"></i></button>' +
                            '<p>Question Type: <strong>Date</strong></p><br>' +
                            '<input type="checkbox" class="resposneRequired_'+uniqueKey+'"> Required <br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="saveResposneForQuery'+uniqueKey+'" onchange="ResposneForQuery('+uniqueKey+',this)"> Save Response for Query' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPIforSave'+uniqueKey+'" style="display: none">' +
                            '<select  id="mainCategoryforSave'+uniqueKey+'" >' +
                            '<option value="main_category">-Select Query Param-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<h3><span class="label label-default click-to-edit-question" for="textBox'+uniqueKey+'">Double click to Edit Question</span></h3><input value="" type="text" id="textBox'+uniqueKey+'" name="textBox'+uniqueKey+'" class="click-to-save-question-name" hidden>' +
                            '<h4><span class="label label-default click-to-edit-question-description" for="textBoxDescription'+uniqueKey+'">Double click to Edit Question Description </span></h4><input value="" type="text" id="textBoxDescription'+uniqueKey+'" name="textBoxDescription'+uniqueKey+'" class="click-to-save-question-description" hidden>' +
                            '<br>' +
                            '<div id="dateContainer'+uniqueKey+'" style="height: 45px;">' +
                            '<div class="col-sm-4">' +
                            '<div class="form-group">' +
                            '<div class="input-group date" id="datetimepicker'+uniqueKey+'">' +
                            '<input type="text" class="form-control" />' +
                            '<span class="input-group-addon" onclick="datetimepicker(datetimepicker'+uniqueKey+')"><span class="glyphicon glyphicon-calendar"></span></span>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                    }
                    if ($(ui.item).attr('data-type') == 'number') {
                        $(ui.item).html(
                            '<div style="border: dashed" class="question-box question-box-id-'+uniqueKey+'" data-type="number"  id="'+uniqueKey+'"> <button class="btn btn-danger" id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"> <i class="fa fa-trash"></i></button> <button class="btn btn-info copyRadioBox" id="copyNumber'+uniqueKey+'" onclick="copyNumber('+uniqueKey+');"> <i class="fa fa-copy"></i></button>' +
                            '<p>Question Type: <strong>Number</strong></p><br>' +
                            '<input type="checkbox" class="resposneRequired_'+uniqueKey+'"> Required <br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="saveResposneForQuery'+uniqueKey+'" onchange="ResposneForQuery('+uniqueKey+',this)"> Save Response for Query' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPIforSave'+uniqueKey+'" style="display: none">' +
                            '<select  id="mainCategoryforSave'+uniqueKey+'" >' +
                            '<option value="main_category">-Select Query Param-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<h3><span class="label label-default click-to-edit-question" for="textBox'+uniqueKey+'">Double click to Edit Question</span></h3><input value="" type="text" id="textBox'+uniqueKey+'" name="textBox'+uniqueKey+'" class="click-to-save-question-name" hidden>' +
                            '<h4><span class="label label-default click-to-edit-question-description" for="textBoxDescription'+uniqueKey+'">Double click to Edit Question Description </span></h4><input value="" type="text" id="textBoxDescription'+uniqueKey+'" name="textBoxDescription'+uniqueKey+'" class="click-to-save-question-description" hidden>' +
                            '<br>' +
                            '<div id="numberContainer'+uniqueKey+'" style="height: 160px;">' +
                            '<div class="col-sm-4">' +
                            '<div class="form-group">' +
                            '<div class="input-group date" id="number-'+uniqueKey+'">' +
                            '<input type="number" class="form-control" id="numberInput'+uniqueKey+'" onclick="numberFieldValidation(numberInput'+uniqueKey+','+uniqueKey+');" /><br><br>' +
                            '<input placeholder="minimum" style="width: 120px;" type="number" class="form-control" id="min'+uniqueKey+'" onclick="minimumNumberLength(min'+uniqueKey+');" /><br>' +
                            '<input placeholder="maximum" style="width: 120px;" type="number" class="form-control" id="max'+uniqueKey+'" onclick="maximumNumberLength(max'+uniqueKey+');" />' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                    }
                    if ($(ui.item).attr('data-type') == 'scale') {
                        $(ui.item).html(
                            '<div style="border: dashed" class="question-box question-box-id-'+uniqueKey+'" data-type="scale"  id="'+uniqueKey+'"> <button class="btn btn-danger" id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"> <i class="fa fa-trash"></i></button> <button class="btn btn-info copyRadioBox" id="copyScale'+uniqueKey+'" onclick="copyScale('+uniqueKey+');"> <i class="fa fa-copy"></i></button>' +
                            '<p>Question Type: <strong>Opinion Scale</strong></p><br>' +
                            '<input type="checkbox" class="resposneRequired_'+uniqueKey+'"> Required <br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="saveResposneForQuery'+uniqueKey+'" onchange="ResposneForQuery('+uniqueKey+',this)"> Save Response for Query' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPIforSave'+uniqueKey+'" style="display: none">' +
                            '<select  id="mainCategoryforSave'+uniqueKey+'" >' +
                            '<option value="main_category">-Select Query Param-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<h3><span class="label label-default click-to-edit-question" for="textBox'+uniqueKey+'">Double click to Edit Question</span></h3><input value="" type="text" id="textBox'+uniqueKey+'" name="textBox'+uniqueKey+'" class="click-to-save-question-name" hidden>' +
                            '<h4><span class="label label-default click-to-edit-question-description" for="textBoxDescription'+uniqueKey+'">Double click to Edit Question Description </span></h4><input value="" type="text" id="textBoxDescription'+uniqueKey+'" name="textBoxDescription'+uniqueKey+'" class="click-to-save-question-description" hidden>' +
                            '<input type="button" value="Add Opinion" id="addOpinion'+uniqueKey+'" onclick="addOpinion('+uniqueKey+');">' +
                            '<br>' +
                            '<div id="scaleContainer'+uniqueKey+'">' +
                            '</div>' +
                            '</div>');
                    }
                    if ($(ui.item).attr('data-type') == 'rating') {
                        $(ui.item).html(
                            '<div style="border: dashed" class="question-box question-box-id-'+uniqueKey+'" data-type="rating"  id="'+uniqueKey+'"> <button class="btn btn-danger" id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"> <i class="fa fa-trash"></i></button> <button class="btn btn-info copyRadioBox" id="copyRating'+uniqueKey+'" onclick="copyRating('+uniqueKey+');"> <i class="fa fa-copy"></i></button>' +
                            '<p>Question Type: <strong>Rating</strong></p><br>' +
                            '<input type="checkbox" class="resposneRequired_'+uniqueKey+'"> Required <br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="saveResposneForQuery'+uniqueKey+'" onchange="ResposneForQuery('+uniqueKey+',this)"> Save Response for Query' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPIforSave'+uniqueKey+'" style="display: none">' +
                            '<select  id="mainCategoryforSave'+uniqueKey+'" >' +
                            '<option value="main_category">-Select Query Param-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<h3><span class="label label-default click-to-edit-question" for="textBox'+uniqueKey+'">Double click to Edit Question</span></h3><input value="" type="text" id="textBox'+uniqueKey+'" name="textBox'+uniqueKey+'" class="click-to-save-question-name" hidden>' +
                            '<h4><span class="label label-default click-to-edit-question-description" for="textBoxDescription'+uniqueKey+'">Double click to Edit Question Description </span></h4><input value="" type="text" id="textBoxDescription'+uniqueKey+'" name="textBoxDescription'+uniqueKey+'" class="click-to-save-question-description" hidden>' +
                            '<br>' +
                            '<div id="ratingContainer'+uniqueKey+'">' +
                            '<div style="padding: 5px 5px 5px 5px;" class="ratingItem-'+uniqueKey+'-1"><input type="radio" id="ratingItem-'+uniqueKey+'-1" value="1-5" name="ratingItem'+uniqueKey+'"> &nbsp; <label> 1 - 5  </label></div>' +
                            '<div style="padding: 5px 5px 5px 5px;" class="ratingItem-'+uniqueKey+'-2"><input type="radio" id="ratingItem-'+uniqueKey+'-2" value="1-10" name="ratingItem'+uniqueKey+'"> &nbsp; <label> 1 - 10 </label></div>' +
                            '<input style="width: 450px;" type="text" id="ratingImg'+uniqueKey+'" placeholder="URL for Image of a Star, Heart, User, Thumbs Up, Car etc. ">' +
                            '</div>' +
                            '</div>');
                    }
                    if ($(ui.item).attr('data-type') == 'dropdown') {
                        $(ui.item).html(
                            '<div style="border: dashed" class="question-box question-box-id-'+uniqueKey+'" data-type="dropdown"  id="'+uniqueKey+'"> <button class="btn btn-danger" id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"> <i class="fa fa-trash"></i></button> <button class="btn btn-info copyRadioBox" id="copyDropDownBox'+uniqueKey+'" onclick="copyDropDownBox('+uniqueKey+');"> <i class="fa fa-copy"></i></button>' +
                            '<p>Question Type: <strong>Drop Down</strong></p><br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="enableDisableDynamicData'+uniqueKey+'" id="'+uniqueKey+'" onchange="enable_disable_dynamic_api_data_for_dropdown('+uniqueKey+',this)"> Dynamic API Data' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPI'+uniqueKey+'" style="display: none">' +
                            '<div class="mainCategory'+uniqueKey+'">' +
                            '<select onchange="load_param_dropdown($(this).val(),'+uniqueKey+');" id="mainCategory'+uniqueKey+'">' +
                            '<option value="main_category">-Select Feature Category-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<input type="checkbox" class="resposneRequired_'+uniqueKey+'"> Required <br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="saveResposneForQuery'+uniqueKey+'" onchange="ResposneForQuery('+uniqueKey+',this)"> Save Response for Query' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPIforSave'+uniqueKey+'" style="display: none">' +
                            '<select  id="mainCategoryforSave'+uniqueKey+'" >' +
                            '<option value="main_category">-Select Query Param-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<h3><span class="label label-default click-to-edit-question" for="textBox'+uniqueKey+'">Double click to Edit Question</span></h3><input value="" type="text" id="textBox'+uniqueKey+'" name="textBox'+uniqueKey+'" class="click-to-save-question-name" hidden>' +
                            '<h4><span class="label label-default click-to-edit-question-description" for="textBoxDescription'+uniqueKey+'">Double click to Edit Question Description </span></h4><input value="" type="text" id="textBoxDescription'+uniqueKey+'" name="textBoxDescription'+uniqueKey+'" class="click-to-save-question-description" hidden>' +
                            '<p class="add_new_options'+uniqueKey+'" style="display: block">To add options type in input box and click add &nbsp;&nbsp; <input type="text" id="dropdownboxInput'+uniqueKey+'" autofocus placeholder="Enter Item" />&nbsp;<input type="button" id="adddropdownbox'+uniqueKey+'" value="Add" onclick="createdropdown('+uniqueKey+',dropdownboxInput'+uniqueKey+');" /></p>' +
                            '<br><br>' +
                            '<div id="dropdownboxContainer'+uniqueKey+'">' +
                            '<select id="dropdownboxSelect'+uniqueKey+'"> ' +
                            '<option value="">--Select--</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>');
                    }
                    if ($(ui.item).attr('data-type') == 'hidden') {
                        $(ui.item).html(
                            '<div style="border: dashed" class="question-box question-box-id-'+uniqueKey+'" data-type="hidden"  id="'+uniqueKey+'"> <button class="btn btn-danger" id="close" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"> <i class="fa fa-trash"></i></button> <button class="btn btn-info copyRadioBox" id="copyHidden'+uniqueKey+'" onclick="copyHidden('+uniqueKey+');"> <i class="fa fa-copy"></i></button>' +
                            '<p>Question Type: <strong>Variable</strong></p><br>' +
                            '<input type="checkbox" class="resposneRequired_'+uniqueKey+'"> Required <br>' +
                            '<div class="row">' +
                            '<div class="col-sm-4">' +
                            '<input type="checkbox" class="saveResposneForQuery'+uniqueKey+'" onchange="ResposneForQuery('+uniqueKey+',this)"> Save Response for Query' +
                            '</div>' +
                            '<div class="col-sm-4">' +
                            '<div class="dynamicDataFromAPIforSave'+uniqueKey+'" style="display: none">' +
                            '<select  id="mainCategoryforSave'+uniqueKey+'" >' +
                            '<option value="main_category">-Select Query Param-</option>' +
                            '<option value="Country">Country</option>' +
                            '<option value="Price">Price</option>' +
                            '<option value="Miles">Miles</option>' +
                            '<option value="Year">Year</option>' +
                            '<option value="Make">Make</option>' +
                            '<option value="Engine">Engine</option>' +
                            '<option value="Body_type">Body Type</option>' +
                            '<option value="Body_subtype">Body Sub Type</option>' +
                            '<option value="Transmission">Transmission</option>' +
                            '<option value="Seller_type">Seller Type</option>' +
                            '<option value="Trim">Trim</option>' +
                            '<option value="Drivetrain">Drive Train</option>' +
                            '<option value="Fuel">Fuel</option>' +
                            '<option value="Doors">Doors</option>' +
                            '<option value="Exterior_color">Exterior Color</option>' +
                            '</select>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<br>' +
                            '<br>' +
                            '<div id="hiddenContainer'+uniqueKey+'" style="height: 120px;">' +
                            '<div class="col-sm-4">' +
                            '<div class="form-group">' +
                            '<label for="hiddenKey'+uniqueKey+'"> Key </label> <input type="text" id="hiddenKey'+uniqueKey+'" class="form-control" />' +
                            '<label for="hiddenVal'+uniqueKey+'"> Value </label> <input type="text" id="hiddenVal'+uniqueKey+'" class="form-control" />' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                    }
                }

            });

        
            $( "#sortable1, #sortable2, #sortable3" ).disableSelection();
        });

        jQuery(function ($) {
            // Edit Question
            $(document).on('click', '.click-to-edit-question', function (e) {
                e.stopImmediatePropagation();
                e.preventDefault();
                "use strict";
                $(this).hide();
                $('#' + $(this).attr('for'))
                    .val($(this).text())
                    .toggleClass("form-control")
                    .show()
                    .focus();
            });

            // Save Question
            $(document).on('blur', '.click-to-save-question-name', function (e) {
                e.stopImmediatePropagation();
                e.preventDefault();
                "use strict";
                $(this)
                    .hide()
                    .toggleClass("form-control");
                var myid = (this).id;
                $('span[for=' + myid + ']')
                    .text($(this).val())
                    .show();
            });

            // Edit Question Description
            $(document).on('click', '.click-to-edit-question-description', function (e) {
                e.stopImmediatePropagation();
                e.preventDefault();
                "use strict";
                $(this).hide();
                $('#' + $(this).attr('for'))
                    .val($(this).text())
                    .toggleClass("form-control")
                    .show()
                    .focus();
            });

            // Save Question Description
            $(document).on('blur', '.click-to-save-question-description', function (e) {
                e.stopImmediatePropagation();
                e.preventDefault();
                "use strict";
                $(this)
                    .hide()
                    .toggleClass("form-control");
                var myid = (this).id;
                $('span[for=' + myid + ']')
                    .text($(this).val())
                    .show();
            });

            // load images in media library
            $(document).on('click', '#mediaLibraryButton', function (e) {
                e.stopImmediatePropagation();
                e.preventDefault();
                load_images();
            });

            // Welcome toggle
            $(document).on('click', '#welcomeToggle', function () {

                var quiz_id = '{{ $quiz->id }}';
                var status = 0;
                if(document.getElementById('welcomeToggle').checked) {
                    $('.welcomeScreenContent').show();
                    status = 1;
                } else {
                    $('.welcomeScreenContent').hide();
                }

                $.ajax({
                    url: "{{url('/quiz/welcome')}}",
                    type: 'POST',
                    data: {_token: "{{ csrf_token() }}",status:status,id:quiz_id},
                    success: function() {
                    },
                });

            });

            // Save welcome Screen and Thank you screen
            $(document).on('click', '#saveWelcomeScreen', function () {

                var quiz_id = '{{ $quiz->id }}';
                var status = 0;
                if(document.getElementById('welcomeToggle').checked) {
                    status = 1;
                }

                var welcomeMessage = document.getElementById('welcomeMessage').value;
                var thankyouMessage = document.getElementById('thankyouMessage').value;
                var welcomeDescription = document.getElementById('welcomeDescription').value;
                var welcomeVariables = document.getElementById('welcomeVariables').value;
                var welcomeMedia = document.getElementById('welcomeMedia').value;
                var welcomeActionButtonLabel = document.getElementById('welcomeActionButtonLabel').value;

                var facebookLink = document.getElementById('facebookLink').value;
                var twitterLink = document.getElementById('twitterLink').value;
                var googleLink = document.getElementById('googleLink').value;
                var linkedinLink = document.getElementById('linkedinLink').value;
                var youtubeLink = document.getElementById('youtubeLink').value;
                var instagramLink = document.getElementById('instagramLink').value;
                var pinterestLink = document.getElementById('pinterestLink').value;


                $.ajax({
                    url: "{{url('/quiz/save_data')}}",
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        status:status,
                        id:quiz_id,
                        welcomeMessage:welcomeMessage,
                        thankyouMessage:thankyouMessage,
                        welcomeDescription:welcomeDescription,
                        welcomeVariables:welcomeVariables,
                        welcomeMedia:welcomeMedia,
                        welcomeActionButtonLabel:welcomeActionButtonLabel,
                        facebookLink:facebookLink,
                        twitterLink:twitterLink,
                        googleLink:googleLink,
                        linkedinLink:linkedinLink,
                        youtubeLink:youtubeLink,
                        instagramLink:instagramLink,
                        pinterestLink:pinterestLink
                    },
                    success: function() {
                        alert('data saved!!!');
                    },
                });


            });

            // Save Routing
            $(document).on('click', '#saveRouting', function (e) {
                e.stopImmediatePropagation();
                e.preventDefault();


                var routeArr = [];
                $(".routingRow").each(function() {
                var innerObj = {};
                innerObj['select_question'] = $(this).find('#selectTD'+this.id).find('#select_question'+this.id).val();
                innerObj['select_response'] = $(this).find('#answerTD'+this.id).find('#select_choice'+this.id).val();
                innerObj['next_question'] = $(this).find('#nextTD'+this.id).find('#select_next_question'+this.id).val();
                    routeArr.push(innerObj);
                });

                routes = JSON.stringify(routeArr);

                $.ajax({
                url: '/save_quiz_routes',
                type: 'POST',
                data: {_token: "{{ csrf_token() }}",id:"{{$quiz->id}}",routes:routes},
                dataType: 'JSON',
                    success: function (data) {
                        if(data.status == 'success'){
                            alert('Routes save successfully!!!');
                        }
                        else{
                            alert(data.msg);
                        }
                    }
                });

            });

            // Save Quiz
            $(document).on('click', '#saveQuiz', function (e) {
                e.stopImmediatePropagation();
                e.preventDefault();

                var questionArr = []
                $(".question-box").each(function() {
                    var innerObj = {};
                    innerObj['id'] = this.id;
                    innerObj['required'] = $(this).find('.resposneRequired_'+this.id+':checked').length;
                    innerObj['query'] = $(this).find('.saveResposneForQuery'+this.id+':checked').length;
                    innerObj['query_param'] = $(this).find('.dynamicDataFromAPIforSave'+this.id).children('select').val();
                    innerObj['question'] = $(this).children('h3').text();
                    innerObj['description'] = $(this).children('h4').text();

                    if($(this).attr('data-type') == 'checkbox' && $(this).attr('data-subtype') == 'text'){
                        var allItems = [];
                        $(this).find('#checkboxContainer'+this.id).find('[name="checkboxItem'+this.id+'"]').each(function () {
                            allItems.push($(this).val());
                        });
                        innerObj['type'] = 'checkbox';
                        innerObj['subtype'] = 'text';
                        innerObj['values'] = allItems;
                        innerObj['rating_img'] =  '';
                        innerObj['min_number'] =  '';
                        innerObj['max_number'] =  '';
                        innerObj['variable_key'] =  '';
                        innerObj['variable_value'] =  '';
                    }
                    else if($(this).attr('data-type') == 'radio' && $(this).attr('data-subtype') == 'text'){
                        var allItems = [];
                        $(this).find('#radioboxContainer'+this.id).find('[name="radioboxItem'+this.id+'"]').each(function () {
                            allItems.push($(this).val());
                        });
                        innerObj['type'] = 'radio';
                        innerObj['subtype'] = 'text';
                        innerObj['values'] = allItems;
                        innerObj['rating_img'] =  '';
                        innerObj['min_number'] =  '';
                        innerObj['max_number'] =  '';
                        innerObj['variable_key'] =  '';
                        innerObj['variable_value'] =  '';

                    }
                    else if($(this).attr('data-type') == 'checkbox' && $(this).attr('data-subtype') == 'image'){
                        var allItems = [];
                        $(this).find('#checkboxContainer'+this.id).find('[name="checkboxItem'+this.id+'"]').each(function () {
                            allItems.push($(this).val());
                        });
                        innerObj['type'] = 'checkbox';
                        innerObj['subtype'] = 'image';
                        innerObj['values'] = allItems;
                        innerObj['rating_img'] =  '';
                        innerObj['min_number'] =  '';
                        innerObj['max_number'] =  '';
                        innerObj['variable_key'] =  '';
                        innerObj['variable_value'] =  '';
                    }
                    else if($(this).attr('data-type') == 'radio' && $(this).attr('data-subtype') == 'image'){
                        var allItems = [];
                        $(this).find('#radioboxContainer'+this.id).find('[name="radioboxItem'+this.id+'"]').each(function () {
                            allItems.push($(this).val());
                        });
                        innerObj['type'] = 'radio';
                        innerObj['subtype'] = 'image';
                        innerObj['values'] = allItems;
                        innerObj['rating_img'] =  '';
                        innerObj['min_number'] =  '';
                        innerObj['max_number'] =  '';
                        innerObj['variable_key'] =  '';
                        innerObj['variable_value'] =  '';

                    }
                    else if($(this).attr('data-type') == 'dropdown'){
                        var allItems = [];
                        $(this).find('#dropdownboxSelect'+this.id+' option').each(function () {
                            allItems.push($(this).val());
                        });
                        innerObj['type'] = 'dropdown';
                        innerObj['subtype'] = '';
                        innerObj['values'] = allItems;
                        innerObj['rating_img'] =  '';
                        innerObj['min_number'] =  '';
                        innerObj['max_number'] =  '';
                        innerObj['variable_key'] =  '';
                        innerObj['variable_value'] =  '';

                    }
                    else if($(this).attr('data-type') == 'yes_no'){
                        var allItems = [];
                        $(this).find('#radioboxContainer'+this.id).find('[name="radioboxItem'+this.id+'"]').each(function () {
                            allItems.push($(this).val());
                        });
                        innerObj['type'] = 'yes_no';
                        innerObj['subtype'] = '';
                        innerObj['values'] = allItems;
                        innerObj['rating_img'] =  '';
                        innerObj['min_number'] =  '';
                        innerObj['max_number'] =  '';
                        innerObj['variable_key'] =  '';
                        innerObj['variable_value'] =  '';

                    }
                    else if($(this).attr('data-type') == 'scale'){
                        var allItems = [];
                        $(this).find('#scaleContainer'+this.id+' :input').each(function () {
                            allItems.push($(this).val());
                        });
                        innerObj['type'] = 'scale';
                        innerObj['subtype'] = '';
                        innerObj['values'] = allItems;
                        innerObj['rating_img'] =  '';
                        innerObj['min_number'] =  '';
                        innerObj['max_number'] =  '';
                        innerObj['variable_key'] =  '';
                        innerObj['variable_value'] =  '';

                    }
                    else if($(this).attr('data-type') == 'rating'){
                        var allItems = [];
                        $(this).find('#ratingContainer'+this.id).find('[name="ratingItem'+this.id+'"]').each(function () {
                            allItems.push($(this).val());
                        });
                        innerObj['type'] = 'rating';
                        innerObj['subtype'] = '';
                        innerObj['values'] = allItems;
                        innerObj['rating_img'] =  $(this).find('#ratingImg'+this.id).val();
                        innerObj['min_number'] =  '';
                        innerObj['max_number'] =  '';
                        innerObj['variable_key'] =  '';
                        innerObj['variable_value'] =  '';

                    }
                    else if($(this).attr('data-type') == 'date'){

                        innerObj['type'] = 'date';
                        innerObj['subtype'] = '';
                        innerObj['values'] = [];
                        innerObj['rating_img'] =  '';
                        innerObj['min_number'] =  '';
                        innerObj['max_number'] =  '';
                        innerObj['variable_key'] =  '';
                        innerObj['variable_value'] =  '';

                    }
                    else if($(this).attr('data-type') == 'number'){

                        innerObj['type'] = 'number';
                        innerObj['subtype'] = '';
                        innerObj['values'] = [];
                        innerObj['rating_img'] =  '';
                        innerObj['min_number'] =  $(this).find('#min'+this.id).val();
                        innerObj['max_number'] =  $(this).find('#max'+this.id).val();
                        innerObj['variable_key'] =  '';
                        innerObj['variable_value'] =  '';

                    }
                    else if($(this).attr('data-type') == 'hidden'){

                        innerObj['type'] = 'hidden';
                        innerObj['subtype'] = '';
                        innerObj['values'] = [];
                        innerObj['rating_img'] =  '';
                        innerObj['min_number'] =  '';
                        innerObj['max_number'] =  '';
                        innerObj['variable_key'] =  $(this).find('#hiddenKey'+this.id).val();
                        innerObj['variable_value'] =  $(this).find('#hiddenVal'+this.id).val();

                    }

                    questionArr.push(innerObj);
                });

                quizQuestions = JSON.stringify(questionArr);
                console.log(quizQuestions)

                $.ajax({
                    url: '/save_quiz',
                    type: 'POST',
                    data: {_token: "{{ csrf_token() }}",id:"{{$quiz->id}}",quizQuestions:quizQuestions},
                    dataType: 'JSON',
                    success: function (res) {
                        if(res.status == 'success'){
                            alert('Quiz saved!!!');

                        }
                    }
                });

            });

        });

        $(document).ready(function(){
            $("#image-file").fileinput({
                theme: 'fa',
                uploadUrl: "{{url('/image/store')}}",
                uploadExtraData: function() {
                    return {
                        _token: "{{ csrf_token() }}",
                    };
                },
                allowedFileExtensions: ['jpg', 'png', 'gif','jpeg'],
                overwriteInitial: false,
                maxFileSize:2048,
                maxFilesNum: 10,

            }).on('fileuploaded', function() {
                load_images()
            });
        });

        function createChk(question_key,obj,type) {
            var checkboxCount = makeUniqueKey();
            if (obj.value !== '') {
                var options = '';
                if(type.value == 'text'){
                    options = '<div style="padding: 5px 5px 5px 5px;" class="checkboxItem-'+question_key+'-'+checkboxCount+'"><input type="checkbox" id="checkboxItem-'+question_key+'-'+checkboxCount+'" value="'+obj.value+'" name="checkboxItem'+question_key+'"> &nbsp; <label> '+obj.value+' </label><span id="close_checkbox" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;"><i class="fa fa-close"></i></span></div>';
                }
                else{
                    options = '<div style="padding: 5px 5px 5px 5px;" class="checkboxItem-'+question_key+'-'+checkboxCount+'"><input type="checkbox" id="checkboxItem-'+question_key+'-'+checkboxCount+'" value="'+obj.value+'" name="checkboxItem'+question_key+'"> &nbsp;&nbsp; <img src="'+obj.value+'" height="50" width="52"><span id="close_checkbox" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;"><i class="fa fa-close"></i></span></div>';
                }
                $("#checkboxContainer"+question_key).append(options);
                obj.value = '';
                document.getElementById(obj.id).focus();
            }
        }

        function createradio(question_key,obj,type) {
            var checkboxCount = makeUniqueKey();

            if (obj.value !== '') {
                var options = '';
                if(type.value == 'text'){
                    options = '<div style="padding: 5px 5px 5px 5px;" class="radioboxItem-'+question_key+'-'+checkboxCount+'"><input type="radio" id="radioboxItem-'+question_key+'-'+checkboxCount+'" value="'+obj.value+'" name="radioboxItem'+question_key+'"> &nbsp; <label> '+obj.value+' </label><span id="close_radiobox" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;"><i class="fa fa-close"></i></span></div>';
                }
                else{
                    options = '<div style="padding: 5px 5px 5px 5px;" class="radioboxItem-'+question_key+'-'+checkboxCount+'"><input type="radio" id="radioboxItem-'+question_key+'-'+checkboxCount+'" value="'+obj.value+'" name="radioboxItem'+question_key+'"> &nbsp;&nbsp; <img src="'+obj.value+'" height="50" width="52"><span id="close_radiobox" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;"><i class="fa fa-close"></i></span></div>';
                }
                $("#radioboxContainer"+question_key).append(options);
                obj.value = '';
                document.getElementById(obj.id).focus();
            }
        }

        function createdropdown(question_key,obj) {

            if (obj.value !== '') {
                var options = '';
                options = '<option value="'+obj.value+'" >'+obj.value+'</option>';
                $("#dropdownboxSelect"+question_key).append(options);
                obj.value = '';
                document.getElementById(obj.id).focus();
            }
        }

        function enable_disable_dynamic_api_data(q_id,obj) {
            $("#checkboxContainer"+q_id).html('');
            if(obj.checked){
                $('.dynamicDataFromAPI'+q_id).show();
                $(".add_new_options"+q_id).hide();
            }
            else{
                $('.dynamicDataFromAPI'+q_id).hide();
                $(".add_new_options"+q_id).show();
            }

        }

        function ResposneForQuery(q_id,obj) {
            if(obj.checked){
                $('.dynamicDataFromAPIforSave'+q_id).show();
            }
            else{
                $('.dynamicDataFromAPIforSave'+q_id).hide();
            }

        }

        function enable_disable_dynamic_api_data_for_radio(q_id,obj) {
            $("#radioboxContainer"+q_id).html('');
            if(obj.checked){
                $('.dynamicDataFromAPI'+q_id).show();
                $(".add_new_options"+q_id).hide();
            }
            else{
                $('.dynamicDataFromAPI'+q_id).hide();
                $(".add_new_options"+q_id).show();
            }

        }

        function enable_disable_dynamic_api_data_for_dropdown(q_id,obj) {
            $("#dropdownboxContainer"+q_id).html('');
            if(obj.checked){
                $('.dynamicDataFromAPI'+q_id).show();
                $(".add_new_options"+q_id).hide();
            }
            else{
                $('.dynamicDataFromAPI'+q_id).hide();
                $(".add_new_options"+q_id).show();
                $("#dropdownboxContainer"+q_id).html('<select id="dropdownboxSelect'+q_id+'"><option>--Select--</option></select>');
            }

        }

        function load_param(type,select_id){
            var options="";
            var dataArr = [];
            var radioUnique = '';
            if ( type == "Miles" || type == "Price" || type== "Country" ){
                if(type == "Country"){
                    dataArr = ["US","CA","ALL"];

                }
                else if (type == "Miles"){
                    dataArr = [
                        "1000 - 5000",
                        "5000 - 10000",
                        "10000 - 15000",
                        "15000 - 20000",
                        "20000 - 25000",
                        "25000 - 30000",
                        "30000 - 40000",
                        "40000 - 50000",
                        "50000 - 60000",
                        "60000 - 70000",
                        "70000 - 80000",
                        "80000 - 90000",
                        "90000 - 100000",
                        "100000 - 150000",
                        "150000 - 200000"
                    ];

                }
                else{
                    dataArr = [
                        "$1000 - $5000",
                        "$5000 - $10000",
                        "$10000 - $15000",
                        "$15000 - $20000",
                        "$20000 - $25000",
                        "$25000 - $30000",
                        "$30000 - $40000",
                        "$40000 - $50000",
                        "$50000 - $60000",
                        "$60000 - $70000",
                        "$70000 - $80000",
                        "$80000 - $90000",
                        "$90000 - $100000",
                        "$100000 - $150000",
                        "$150000 - $200000"
                    ];

                }

                options = '';
                dataArr.forEach(function (element) {
                    radioUnique = makeUniqueKey();
                    options += '<div class="checkboxItem-'+radioUnique+'-'+select_id+'"><input type="checkbox" id="checkboxItem-'+radioUnique+'-'+select_id+'" value="'+element+'" name="checkboxItem'+select_id+'"><label> '+element+' </label><span id="close_checkbox" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;"><i class="fa fa-close"></i></span></div>';
                })

                $("#checkboxContainer"+select_id).attr('disabled',false);
                $("#checkboxContainer"+select_id).html(options);
                return false;
            }

            $("#model_select"+select_id).hide();

            $('#checkboxContainer'+select_id).attr('disabled',false);

            var data = "_token={{csrf_token()}}";
            data += "&type="+type;
            data += "&question_key="+select_id;

            $.ajax({
                url: "{{url('/load/param_checkbox')}}",
                type:'POST',
                data: data,
                beforeSend: function(){
                        $("#checkboxContainer"+select_id).html('');
                },
                success: function(html) {
                    $("#checkboxContainer"+select_id).html(html);
                },
            });


        }

        function load_param_radio(type,select_id){
            var options="";
            var dataArr = [];
            var radioUnique = '';

            if ( type == "Miles" || type == "Price" || type== "Country" ){

                if(type == "Country"){
                    dataArr = ["US","CA","ALL"];
                }
                else if (type == "Miles"){
                    dataArr = [
                        "1000 - 5000",
                        "5000 - 10000",
                        "10000 - 15000",
                        "15000 - 20000",
                        "20000 - 25000",
                        "25000 - 30000",
                        "30000 - 40000",
                        "40000 - 50000",
                        "50000 - 60000",
                        "60000 - 70000",
                        "70000 - 80000",
                        "80000 - 90000",
                        "90000 - 100000",
                        "100000 - 150000",
                        "150000 - 200000"
                    ];
                }
                else{
                    dataArr = [
                        "$1000 - $5000",
                        "$5000 - $10000",
                        "$10000 - $15000",
                        "$15000 - $20000",
                        "$20000 - $25000",
                        "$25000 - $30000",
                        "$30000 - $40000",
                        "$40000 - $50000",
                        "$50000 - $60000",
                        "$60000 - $70000",
                        "$70000 - $80000",
                        "$80000 - $90000",
                        "$90000 - $100000",
                        "$100000 - $150000",
                        "$150000 - $200000"
                    ];
                }
                options = '';
                dataArr.forEach(function (element) {
                    radioUnique = makeUniqueKey();
                    options += '<div class="radioboxItem-'+radioUnique+'-'+select_id+'"><input type="radio" id="radioboxItem-'+radioUnique+'-'+select_id+'" value="'+element+'" name="radioboxItem'+select_id+'"><label> '+element+' </label><span id="close_radiobox" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;"><i class="fa fa-close"></i></span></div>';
                })

                $("#radioboxContainer"+select_id).attr('disabled',false);
                $("#radioboxContainer"+select_id).html(options);
                return false;
            }

            $("#model_select"+select_id).hide();

            $('#radioboxContainer'+select_id).attr('disabled',false);

            var data = "_token={{csrf_token()}}";
            data += "&type="+type;
            data += "&question_key="+select_id;

            $.ajax({
                url: "{{url('/load/param_radiobox')}}",
                type:'POST',
                data: data,
                beforeSend: function(){
                    $("#radioboxContainer"+select_id).html('');
                },
                success: function(html) {
                    $("#radioboxContainer"+select_id).html(html);
                },
            });


        }

        function load_param_dropdown(type,select_id){
            var options="";
            var dataArr = [];
            var radioUnique = '';

            if ( type == "Miles" || type == "Price" || type== "Country" ){

                if(type == "Country"){
                    dataArr = ["US","CA","ALL"];
                }
                else if (type == "Miles"){
                    dataArr = [
                        "1000 - 5000",
                        "5000 - 10000",
                        "10000 - 15000",
                        "15000 - 20000",
                        "20000 - 25000",
                        "25000 - 30000",
                        "30000 - 40000",
                        "40000 - 50000",
                        "50000 - 60000",
                        "60000 - 70000",
                        "70000 - 80000",
                        "80000 - 90000",
                        "90000 - 100000",
                        "100000 - 150000",
                        "150000 - 200000"
                    ];
                }
                else{
                    dataArr = [
                        "$1000 - $5000",
                        "$5000 - $10000",
                        "$10000 - $15000",
                        "$15000 - $20000",
                        "$20000 - $25000",
                        "$25000 - $30000",
                        "$30000 - $40000",
                        "$40000 - $50000",
                        "$50000 - $60000",
                        "$60000 - $70000",
                        "$70000 - $80000",
                        "$80000 - $90000",
                        "$90000 - $100000",
                        "$100000 - $150000",
                        "$150000 - $200000"
                    ];
                }
                options = '';
                dataArr.forEach(function (element) {
                    radioUnique = makeUniqueKey();
                    options += '<option value="'+element+'">'+element+'</option>';
                })

                $("#dropdownboxContainer"+select_id).attr('disabled',false);
                $("#dropdownboxContainer"+select_id).html('<select id="dropdownboxSelect'+select_id+'"><option value="">--Select--</option>'+options+'</select>');
                return false;
            }

            $("#model_select"+select_id).hide();

            $('#dropdownboxContainer'+select_id).attr('disabled',false);

            var data = "_token={{csrf_token()}}";
            data += "&type="+type;
            data += "&question_key="+select_id;

            $.ajax({
                url: "{{url('/load/param_dropdown')}}",
                type:'POST',
                data: data,
                beforeSend: function(){
                    $("#dropdownboxContainer"+select_id).html('');
                },
                success: function(html) {
                    $("#dropdownboxContainer"+select_id).html('<select id="dropdownboxSelect'+select_id+'"><option value="">--Select--</option>'+html+'</select>');
                },
            });


        }

        function load_images() {
            $.ajax({
                url: "{{url('/image/list')}}",
                type:'GET',
                success: function(response) {

                    var html = '';
                    html += '<table><tr style="height: 25px;margin:0;"><th>Thumbnail</th><th>&nbsp;&nbsp;&nbsp;Label</th><th>&nbsp;</th><th>&nbsp;&nbsp;Link</th><th>&nbsp;</th></tr>';
                    response = JSON.parse(response);
                    for(var i=0;i<response.length;i++){
                        html += '' +
                            '<tr style="height: 65px;margin:0;">' +
                            '<td>' +
                            '<img id="'+response[i].id+'" src="'+response[i].image_url+'" height="50" width="52">' +
                            '</td>' +
                            '<td>' +
                            '<input id="lb_'+response[i].id+'" style="width: 200px;height: 20px;" type="text" value="'+response[i].image_title+'" />' +
                            '</td>' +
                            '<td>' +
                            '<button style="margin-right: 5px;height: 20px;width: 40px;background: #3c8dbc;color: white;border: 0;-webkit-appearance: none;" onclick="save_label(lb_'+response[i].id+','+response[i].id+')">Save</button>' +
                            '</td>' +
                            '<td>' +
                            '<input id="cp_'+response[i].id+'" style="width: 200px;height: 20px;" type="text" value="'+response[i].image_url+'" />' +
                            '</td>' +
                            '<td>' +
                            '<button style="margin-right: 5px;height: 20px;width: 40px;background: #3c8dbc;color: white;border: 0;-webkit-appearance: none;" onclick="cp_link(cp_'+response[i].id+')">Copy</button>' +
                            '</td>' +
                            '</tr>';
                    }
                    $(".file-list").html(html);

                },
            });
        }

        function new_route() {
            var route_count = makeUniqueKey();
            var new_route_tr = '' +
                '<tr class="routingRow" id="'+route_count+'" style="height:40px;margin:0;">' +
                '<td id="selectTD'+route_count+'">' +
                '<select class="form-control" onchange="load_question_responses($(this).val(),'+route_count+');" id="select_question'+route_count+'" style="max-width:150px">' +
                '<option value="">Select question...</option>' +
                '</select>' +
                '</td>' +
                '<td id="answerTD'+route_count+'">' +
                '<select class="form-control"  id="select_choice'+route_count+'" style="max-width:150px" disabled>' +
                '<option value="">Select Answer...</option>' +
                '</select>' +
                '</td>' +
                '<td id="nextTD'+route_count+'">' +
                '<select class="form-control"  id="select_next_question'+route_count+'" style="max-width:150px" disabled>' +
                '<option value="">Next question...</option>' +
                '</select>' +
                '</td>' +
                '<td>' +
                '<button class="btn btn-danger btn-xs" onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode); return false;"><i class="fa fa-trash"></i></button>' +
                '</td>' +
                '</tr>';

            $("#routeTable").append(new_route_tr);

            var all_questions = '<option value="">Select question...</option>';
            var next_question = '<option value="">Next question...</option>';

            $(".question-box").each(function() {

                all_questions += '<option value="'+this.id+'">'+$(this).children('h3').text()+'</option>';
                next_question += '<option value="'+this.id+'">'+$(this).children('h3').text()+'</option>';

            });

            $("#select_question"+route_count).html(all_questions);
            $("#select_next_question"+route_count).html(next_question);

        }

        function load_question_responses(q_id,row_id) {
            var all_questions_response = '<option value="">Select Answer...</option>';
            $(".question-box").each(function() {

                if (this.id == q_id) {

                    if ($(this).attr('data-type') == 'checkbox') {
                        $(this).find('#checkboxContainer' + this.id).find('[name="checkboxItem' + this.id + '"]').each(function () {
                            all_questions_response += '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';

                        });
                    }
                    else if ($(this).attr('data-type') == 'radio' || $(this).attr('data-type') == 'yes_no') {
                        $(this).find('#radioboxContainer' + this.id).find('[name="radioboxItem' + this.id + '"]').each(function () {
                            all_questions_response += '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';

                        });
                    }
                    else if ($(this).attr('data-type') == 'date') {
                        $(this).find('#dateContainer' + this.id+' :input').each(function () {
                            all_questions_response += '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';

                        });
                    }
                    else if ($(this).attr('data-type') == 'number') {
                        $(this).find('#numberContainer' + this.id+' :input').each(function () {
                            all_questions_response += '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';

                        });
                    }
                    else if ($(this).attr('data-type') == 'scale') {
                        $(this).find('#scaleContainer' + this.id+' :input').each(function () {
                            all_questions_response += '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';

                        });
                    }
                    else if ($(this).attr('data-type') == 'rating') {
                        $(this).find('#ratingContainer' + this.id).find('[name="ratingItem' + this.id + '"]').each(function () {
                            all_questions_response += '<option value="' + $(this).val() + '">' + $(this).val() + '</option>';

                        });
                    }
                    else if ($(this).attr('data-type') == 'dropdown') {
                        $(this).find('#dropdownboxContainer' + this.id).find('#dropdownboxSelect' + this.id + ' option').each(function () {
                            all_questions_response += '<option value="' + $(this).attr('value') + '">' + $(this).attr('value') + '</option>';

                        });
                    }

                }

            });
            $("#select_choice"+row_id).html(all_questions_response);
            $("#select_choice"+row_id).removeAttr("disabled");
            $("#select_next_question"+row_id).removeAttr("disabled");

        }

        function cp_link(obj) {
            var copyText = document.getElementById(obj.id);
            copyText.select();
            document.execCommand("copy");
        }
        
        function save_label(obj,id) {

            $.ajax({
                url: "{{url('/image/label')}}",
                type: 'POST',
                data: {_token: "{{ csrf_token() }}",label:obj.value,id:id},
                success: function(response) {
                    load_images()
                },
            });
        }

        function copyCheckBox(obj) {

            var newObj = makeUniqueKey();
            var newDiv = $(".question-box-id-"+obj).clone();
            var re = new RegExp(obj,"g");
            newDiv = newDiv.html().replace(re, newObj);

            $('.dropfalse').append('<div class="ui-state-default ui-draggable ui-draggable-handle" data-type="checkbox" style="position: relative; width: 138px; right: auto; height: 30px; bottom: auto; left: 0px; top: 0px;"><div style="border: dashed" class="question-box question-box-id-'+newObj+'" data-type="checkbox"  id="'+newObj+'">'+newDiv+'</div></div>');

        }

        function copyRadioBox(obj) {

            var newObj = makeUniqueKey();
            var newDiv = $(".question-box-id-"+obj).clone();
            var re = new RegExp(obj,"g");
            newDiv = newDiv.html().replace(re, newObj);

            $('.dropfalse').append('<div class="ui-state-default ui-draggable ui-draggable-handle" data-type="radio" style="position: relative; width: 138px; right: auto; height: 30px; bottom: auto; left: 0px; top: 0px;"><div style="border: dashed" class="question-box question-box-id-'+newObj+'" data-type="radio"  id="'+newObj+'">'+newDiv+'</div></div>');

        }

        function copyDropDownBox(obj) {

            var newObj = makeUniqueKey();
            var newDiv = $(".question-box-id-"+obj).clone();
            var re = new RegExp(obj,"g");
            newDiv = newDiv.html().replace(re, newObj);

            $('.dropfalse').append('<div class="ui-state-default ui-draggable ui-draggable-handle" data-type="dropdown" style="position: relative; width: 138px; right: auto; height: 30px; bottom: auto; left: 0px; top: 0px;"><div style="border: dashed" class="question-box question-box-id-'+newObj+'" data-type="dropdown"  id="'+newObj+'">'+newDiv+'</div></div>');

        }

        function copyYesNo(obj) {

            var newObj = makeUniqueKey();
            var newDiv = $(".question-box-id-"+obj).clone();
            var re = new RegExp(obj,"g");
            newDiv = newDiv.html().replace(re, newObj);

            $('.dropfalse').append('<div class="ui-state-default ui-draggable ui-draggable-handle" data-type="yes_no" style="position: relative; width: 138px; right: auto; height: 30px; bottom: auto; left: 0px; top: 0px;"><div style="border: dashed" class="question-box question-box-id-'+newObj+'" data-type="yes_no"  id="'+newObj+'">'+newDiv+'</div></div>');

        }

        function copyDate(obj) {

            var newObj = makeUniqueKey();
            var newDiv = $(".question-box-id-"+obj).clone();
            var re = new RegExp(obj,"g");
            newDiv = newDiv.html().replace(re, newObj);

            $('.dropfalse').append('<div class="ui-state-default ui-draggable ui-draggable-handle" data-type="date" style="position: relative; width: 138px; right: auto; height: 30px; bottom: auto; left: 0px; top: 0px;"><div style="border: dashed" class="question-box question-box-id-'+newObj+'" data-type="date"  id="'+newObj+'">'+newDiv+'</div></div>');

        }

        function copyHidden(obj) {

            var newObj = makeUniqueKey();
            var newDiv = $(".question-box-id-"+obj).clone();
            var re = new RegExp(obj,"g");
            newDiv = newDiv.html().replace(re, newObj);

            $('.dropfalse').append('<div class="ui-state-default ui-draggable ui-draggable-handle" data-type="hidden" style="position: relative; width: 138px; right: auto; height: 30px; bottom: auto; left: 0px; top: 0px;"><div style="border: dashed" class="question-box question-box-id-'+newObj+'" data-type="date"  id="'+newObj+'">'+newDiv+'</div></div>');

        }

        function copyNumber(obj) {

            var newObj = makeUniqueKey();
            var newDiv = $(".question-box-id-"+obj).clone();
            var re = new RegExp(obj,"g");
            newDiv = newDiv.html().replace(re, newObj);

            $('.dropfalse').append('<div class="ui-state-default ui-draggable ui-draggable-handle" data-type="number" style="position: relative; width: 138px; right: auto; height: 30px; bottom: auto; left: 0px; top: 0px;"><div style="border: dashed" class="question-box question-box-id-'+newObj+'" data-type="number"  id="'+newObj+'">'+newDiv+'</div></div>');

        }

        function copyScale(obj) {

            var newObj = makeUniqueKey();
            var newDiv = $(".question-box-id-"+obj).clone();
            var re = new RegExp(obj,"g");
            newDiv = newDiv.html().replace(re, newObj);

            $('.dropfalse').append('<div class="ui-state-default ui-draggable ui-draggable-handle" data-type="scale" style="position: relative; width: 138px; right: auto; height: 30px; bottom: auto; left: 0px; top: 0px;"><div style="border: dashed" class="question-box question-box-id-'+newObj+'" data-type="scale"  id="'+newObj+'">'+newDiv+'</div></div>');

        }

        function copyRating(obj) {

            var newObj = makeUniqueKey();
            var newDiv = $(".question-box-id-"+obj).clone();
            var re = new RegExp(obj,"g");
            newDiv = newDiv.html().replace(re, newObj);

            $('.dropfalse').append('<div class="ui-state-default ui-draggable ui-draggable-handle" data-type="rating" style="position: relative; width: 138px; right: auto; height: 30px; bottom: auto; left: 0px; top: 0px;"><div style="border: dashed" class="question-box question-box-id-'+newObj+'" data-type="rating"  id="'+newObj+'">'+newDiv+'</div></div>');

        }

        function datetimepicker(obj) {
            $('#'+obj.id).datetimepicker();
        }

        function setInputFilter(textbox, inputFilter) {
            ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                textbox.addEventListener(event, function() {
                    if (inputFilter(this.value)) {
                        this.oldValue = this.value;
                        this.oldSelectionStart = this.selectionStart;
                        this.oldSelectionEnd = this.selectionEnd;
                    } else if (this.hasOwnProperty("oldValue")) {
                        this.value = this.oldValue;
                        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                    }
                });
            });
        }

        function maximumNumberLength(obj) {
            setInputFilter(document.getElementById(obj.id), function(value) {
                return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 99999999);
            });
        }

        function minimumNumberLength(obj) {
            setInputFilter(document.getElementById(obj.id), function(value) {
                return /^\d*$/.test(value) && (value === "" || parseInt(value) <= 9);
            });
        }

        function numberFieldValidation(obj,id) {
            var min = document.getElementById('min'+id).value;
            var max = document.getElementById('max'+id).value;
            setInputFilter(document.getElementById(obj.id), function(value) {
                return /^\d*$/.test(value) && (parseInt(value) > max && parseInt(value) < min);
            });
        }

        function addOpinion(q_id) {
            var opinionCount = makeUniqueKey();
             var options = '<div style="padding: 5px 5px 5px 5px;" class="openionItem-'+q_id+'-'+opinionCount+'"><input type="text" id="openionItem-'+q_id+'-'+opinionCount+'" name="openionItem-'+q_id+'-'+opinionCount+'"><span id="close_checkbox" onclick="this.parentNode.parentNode.removeChild(this.parentNode); return false;"><i class="fa fa-close"></i></span></div>';
            $("#scaleContainer"+q_id).append(options);
        }

        function makeUniqueKey() {
            var text = "";
            var possible = "0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }

        // Remove Questions
        window.onload = function(){
            document.getElementById('close').onclick = function(){
                this.parentNode.parentNode.parentNode
                    .removeChild(this.parentNode.parentNode);
                return false;
            };

            document.getElementById('close_checkbox').onclick = function(){
                this.parentNode.parentNode.removeChild(this.parentNode);
                return false;
            };

            document.getElementById('close_radiobox').onclick = function(){
                this.parentNode.parentNode.removeChild(this.parentNode);
                return false;
            };

        };

    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>


@endsection