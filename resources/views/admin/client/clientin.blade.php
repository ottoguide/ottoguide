
@extends('layouts.admin')
@section('content')

<style>
body::-webkit-scrollbar {
    width: 0.5em;
}
 
body::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
}
 
body::-webkit-scrollbar-thumb {
  background-color: #cc0000;
  outline: 1px solid slategrey;
}
</style>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if (session('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                            @endif
      
    </section>

    <!-- Main content -->
    <section class="content">
      
   <h4>User Detail:</h4>

<table width="100%" class="table">
   <tr>
   <td><b>User Name:</b> {{$user[0]['name']}} </td>
   <td><b>User Last Name:</b> {{$user[0]['lastname']}} 
       </td>
   <td><b></b></td>
   </tr>
   
   <tr>
   <td><b>User Phone:</b> {{$user[0]['phone']}} </td>
   <td><b>User Mobile:</b> {{$user[0]['email']}}
   </tr>
   
   <tr>
   
  <td>
 
	
		<b>Status:</b> <?php if($user[0]['is_block']=='1'){
			echo "Blocked";
		}else{
			echo "Active";
		}
	?>
  </td>
   </tr>
   </table>
   </p>




		
		@if($user[0]['is_block']==0)
	
 
		<a href="{{route('client/block/', ['user_id' => $user[0]['id']])}}"
				   class="btn btn-primary test5" onclick="return confirm(\'Are you sure you want to block?\')">
					<i class="fa fa-times"></i> Block this account
				</a>
	
		@endif
		
	     @if($user[0]['is_block']==1)

		<a href="{{route('client/unblock/', ['user_id' => $user[0]['id']])}}"
				   class="btn btn-success test5" onclick="return confirm(\'Are you sure you want to unblock?\')">
					<i class="fa fa-right"></i> Unblock this account
				</a>
	
		
		@endif
			
		


  
		<hr>

			<div id="exTab1">	
			<ul  class="nav nav-pills">
						<li class="active"><a href="#2a" data-toggle="tab">RBO Logs</a>
						</li>
						<li><a href="#3a" data-toggle="tab">Chat Logs</a>
						</li>
					<li><a href="#4a" data-toggle="tab">Email Logs</a>
						</li>
						
							<li><a href="#5a" data-toggle="tab">Reviews Logs</a>
						</li>
						
						  <li><a href="#6a" data-toggle="tab">Quiz Logs</a></li>
						  
						  <li><a href="#7a" data-toggle="tab">Saved Car</a></li>
						  
						  <li><a href="#8a" data-toggle="tab">Saved Search</a></li>
					</ul>

				<div class="tab-content clearfix">
				
		

				<div class="tab-pane active" id="2a">
					 <br>
				 <table class="table table-striped test2" width="90%">
   
					   <th>Dealer Id</th>
					   <th>Dealer Name</th>
					   <th>RBO Link</th>
					   <th>RBO Date</th>	
			        	<?php 
					   $get_rbo_logs = DB::table('request_best_offer')->where('user_id',$user[0]['id'])
					   ->join('dealer','request_best_offer.dealer_id','=','dealer.dealer_id')
					   ->get();
					   ?>
					   @if(count($get_rbo_logs))
					   @foreach($get_rbo_logs as $get_rbo_log)  
					   <tr>
					   
					    <td>
					   {{$get_rbo_log->dealer_id}}
					   </td>
					   
					   
					   <td>
					   {{$get_rbo_log->dealer_name}}
					   </td>
					   <td>
					   
					   <a href="{{url('car/detail/')}}/{{$get_rbo_log->car_id}}" target="_blank">Rbo Car Link</a>
					  
					   </td>
					   <td>
					   {{date('d-m-Y H:i:s', strtotime($get_rbo_log->created_at))}}
					   </td>
					   
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> No RBO Found </h4>  
					    @endif
					   
					   </table>
					   
					
				</div>
							
					<div class="tab-pane" id="3a">
					<br>
					 <table class="table table-striped test2" width="90%">
					   <th>Dealer Id</th>
					   <th>Dealer Name</th>
					   <th>Chat Channel Name</th>
					   <th>Chat Date</th>
					   <th>Chat Status</th>	
			        	<?php 
					   $get_chat_logs = DB::table('twilio_chat')->where('user_id',$user[0]['id'])
					   ->join('dealer','twilio_chat.dealer_id','=','dealer.dealer_id')
					   ->get();
					   ?>
					   @if(count($get_chat_logs))
					   @foreach($get_chat_logs  as $get_chat_log)  
					   <tr>
					   
					    <td>
					   {{$get_chat_log->dealer_id}}
					   </td>
					   
					   <td>
					   {{$get_chat_log->dealer_name}}
					   </td>
					   <td>
					  {{$get_chat_log->chat_channel}}
					   </td>
					   <td>
					   {{date('d-m-Y H:i:s', strtotime($get_chat_log->created_at))}}
					   </td>
					   
					   <td>
					    @if($get_chat_log->is_block_current == '0')
						<span class="green"> Active </span>	
						@else
						<span class="red"> Blocked </span>	
						@endif	
					   </td>
					   
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> No Chat Activity Found </h4>  
					    @endif
					   
					   </table>
					
							</div>
					 
					 
					 <div class="tab-pane" id="4a">
					 
					 <br>
					 <table class="table table-striped test2" width="90%">
			
					   <th>Dealer Id</th>
					   <th>Dealer Name</th>
					   <th>Subject</th>
					   <th>Message</th>
					   <th>Emal Date</th>
			        	<?php 
					   $get_email_logs = DB::table('email_box')->where('client_id',$user[0]['id'])->where('reply_to',NULL)
					   ->join('dealer','email_box.dealer_id','=','dealer.dealer_id')
					   ->get();
					   ?>
					   @if(count($get_email_logs))
					   @foreach($get_email_logs  as $get_email_log)  
					   <tr>
					   
					   <td>
					   {{$get_email_log->dealer_id}}
					   </td>
					   <td>
					   {{$get_email_log->dealer_name}}
					   </td>
					   <td>
					   {{$get_email_log->subject}}
					   </td>
					   
					      <td>
					   {{$get_email_log->message}}
					   </td>
					   
					   <td>
					   {{date('d-m-Y H:i:s', strtotime($get_email_log->created_at))}}
					   </td>
					
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> No Email Found </h4>  
					    @endif
					   
					   </table>
					 
						</div>
						
						
						
						
					 <div class="tab-pane" id="5a">
					 
					 <br>
					 <table class="table table-striped test2" width="90%">
					   <th>Dealer id</th>
					   <th>Dealer Name</th>
					   <th>Review Title</th>
					   <th>Message</th>
					    <th>Rating</th>
					   <th>Review Date</th>
			        	<?php 
					   $get_review_logs = DB::table('client_reviews')->where('client_id',$user[0]['id'])->where('reply_to',NULL)
					   ->join('dealer','client_reviews.dealer_id','=','dealer.dealer_id')
					   ->get();
					   ?>
					   @if(count($get_review_logs))
					   @foreach($get_review_logs  as $get_review_log)  
					   <tr>
					   
					   <td>
					   {{$get_review_log->dealer_id}}
					   </td>
					   
					   <td>
					   {{$get_review_log->dealer_name}}
					   </td>
					   <td>
					   {{$get_review_log->title}}
					   </td>
					   
					    <td>
					   {{$get_review_log->message}}
					   </td>
					   
					    <td>
					   {{$get_review_log->rating}}.0
					   </td>
					   
					   <td>
					   {{date('d-m-Y H:i:s', strtotime($get_review_log->created_at))}}
					   </td>
					
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> No Review Found </h4>  
					    @endif
					   
					   </table>
					 
						</div>
						
					
					<div class="tab-pane" id="6a"> 
					 <br>
					 <table class="table table-striped test2" width="90%">
			
					   <th>Quiz Title</th>
					   <th>Quiz Description</th>
					   <th>Quiz Query Url</th>
					   <th>Quiz Date</th>
			        	<?php 
					   	$get_quiz_logs = DB::table('quiz_query')->join('quiz','quiz_id','=','quiz.id')
						->where('user_id',$user[0]['id'])
					    ->get();
					   ?>
					   
					   @if(count($get_quiz_logs))
					   @foreach($get_quiz_logs  as 	$get_quiz_log)  
					   <tr>
					   <td>
					   {{$get_quiz_log->title}}
					   </td>
					   <td>
					   {{$get_quiz_log->description}}
					   </td>
					   
					    <td>
					   <a href="{{url('search-car.html?')}}{{$get_quiz_log->quiz_query}}" target="_blank">Quiz Query Link</a> 
					   </td>
					   
					    <td>
					   {{$get_quiz_log->created_at}}
					   </td>
					   
					 
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> Quiz Not Found </h4>  
					    @endif
					   
					   </table>
					 
						</div>
						
						
						
						
					 <div class="tab-pane" id="7a"> 
					 <br>
					 <table class="table table-striped test2" width="90%">
			
					   <th>User Name</th>
					   <th>Saved Search Link</th>
					   <th>Date</th>
					   
			        	<?php 
					   	$get_saved_cars_logs = DB::table('client_favourite')->join('users','client_favourite.client_id','users.id')
						->where('client_id',$user[0]['id'])
					    ->get();
					   ?>
					   
					   @if(count($get_saved_cars_logs))
					   @foreach($get_saved_cars_logs  as $get_saved_cars_log)  
					   <tr>
					   <td>
					   {{$get_saved_cars_log->name}}
					   </td>
					   
					
					    <td>
					   <a href="{{url('car/detail/')}}/{{$get_saved_cars_log->car_id}}" target="_blank">Saved Car Link</a> 
					   </td>
					   
					    <td>
					   {{$get_quiz_log->created_at}}
					   </td>
					   
					 
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> Saved Car Not Found </h4>  
					    @endif
					   
					   </table>
					 
						</div>
						
						
						
					<div class="tab-pane" id="8a"> 
					 <br>
					 <table class="table table-striped test2" width="90%">
			
					   <th>User Name</th>
					   <th>Title</th>
					   <th>Description</th>
					   <th>Saved Search Link</th>
					   <th>Date</th>
					   
			        	<?php 
					   	$get_saved_search_logs = DB::table('client_save_search')->join('users','client_save_search.client_id','users.id')
						->where('client_id',$user[0]['id'])
					    ->get();
					   ?>
					   
					   @if(count($get_saved_search_logs))
					   @foreach($get_saved_search_logs  as $get_saved_search_log)  
					   <tr>
					   <td>
					   {{$get_saved_search_log->name}}
					   </td>
					   
					   <td>
					   {{$get_saved_search_log->save_search_name}}
					   </td>
					   
					    <td>
					   {{$get_saved_search_log->save_search_description}}
					   </td>
					
					   <td>
					   <a href="{{url('search-car.html?')}}{{$get_saved_search_log->save_search_url}}" target="_blank">Saved Search Url </a> 
					   </td>
					   
					    <td>
					   {{$get_saved_search_log->created_at}}
					   </td>
					   
					 
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> Saved Searches Not Found </h4>  
					    @endif
					   
					   </table>
					 
						</div>
						
						
						
					</div>
					</div>



	
  
    </section>
    <!-- /.content -->
  </div>

@endsection 