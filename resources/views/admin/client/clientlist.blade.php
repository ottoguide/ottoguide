@extends('layouts.admin')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Clients
                <small>All Client List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Client List</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
							<!--
                            <div class="row">
                                <div class="col-xs-10"></div>
                                <div class="col-xs-2">
                                    <a href="{{route('create_user')}}" class="btn btn-primary btn-block margin-bottom">
                                        Add New User
                                    </a>
                                </div>
                            </div> -->
                            <table width="100%" id="data-table" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr class="test3">
                                    <th class="text-center" width="4%">Client</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th class="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection

@section('page_script')
    <script>
		$('#data-table').DataTable($.extend(datatable_options, {
		"scrollX": true,
			ajax: '{{route('client_list_table')}}',
			columns: [
				{ data: 'DT_Row_Index', name: 'id' },
				{ data: 'name', name: 'name' },
				{ data: 'email', name: 'email' },
				{ data: 'mobile', name: 'mobile' },
				{ data: 'action', name: 'action', orderable: false, searchable: false},
			]
		}));
    </script>
@endsection