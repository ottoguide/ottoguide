 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Administrator</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
             <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
           </button>
           </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="index.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
          </li>
          <li class="header">Admin</li>
        <li class="">
          <a href="usergroup.php">
            <i class="fa fa-users"></i>
            <span>Usergroup</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          </li>
        <li>
          <a href="userlist.php">
            <i class="fa fa-user"></i> <span>User</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
          </a>
        </li>

		<li class="header">Registered Customer</li>
		   <li class="">
			<a href="delaerlist.php">
			<i class="fa fa-users"></i>
			<span>Dealers</span>
					
		     </a>
		  </li>
			<li>
			<a href="client.php">
			<i class="fa fa-user"></i> <span>Clients</span>
			</a>
			</li>
		<li>
          <a href="dealerchat.php">
            <i class="fa fa-user"></i> <span>Dealer & Client Activity</span>
            
          </a>
        </li>

        <li class="header">System Subscription</li>
        <li>
          <a href="systemsubription.php">
            <i class="fa fa-tv"></i> <span>System Subscription</span>
            </a>
          </li>
        
        
		 <li class="header">Rating & Reviews</li>
        <li class="">
          <a href="admin/reviews/report">
            <i class="fa fa-line-chart"></i>
            <span>Reviews Report</span>
          </a>
          </li>

		
		 <li class="header"> Chat Communication </li>
         <li class="">
          <a href="admin/reviews/report">
            <i class="fa fa-line-chart"></i>
            <span>Block User</span>
          </a>
          </li>
		
		
		
		
		<li class="header">Email Template</li>
        <li class="">
          <a href="emailtemplete.php">
            <i class="fa fa-users"></i>
            <span>Dealer Register</span>
            
          </a>
          </li>
        <li>
          <a href="pages/widgets.html">
            <i class="fa fa-user"></i> <span>User Register</span>
            
          </a>
        </li>
		<li>
          <a href="pages/widgets.html">
            <i class="fa fa-user"></i> <span>On Chat</span>
            
          </a>
        </li>        
        
        <li class="header">Content Management</li>
        
        </li> 
        
          <li class="header">Report</li>
        
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>