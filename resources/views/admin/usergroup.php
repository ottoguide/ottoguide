<?php include('header.php');
?>
  <!-- Left side column. contains the logo and sidebar -->
   <?php include('sidebar.php');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Group
        
      </h1>
      <ol class="breadcrumb">
        <li><a href="usergroup2.php" class="btn btn-primary test5"> Add Group</a></li>
        
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <table class="table table-striped test2">
    <thead>
      <tr class="test3">
        <th class="test1">Name</th>
        <th>Employee<i class="fa fa-angle-down test4"></i></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Super-Admin</td>
        <td></td>
      </tr>
      <tr>
        <td>Manager</td>
        <td></td>
      </tr>
      <tr>
        <td>Operator</td>
        <td></td>
      </tr>
      <tr>
        <td>My Custom User Group </td>
        <td><a>Edit/</a><a>Delete</a></td>
      </tr>
    </tbody>
  </table>

    </section>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
   <?php include('footer.php');
?>