@extends('layouts.admin')
   <!-- Bootstrap CSS -->
@section('content')
<style>
.box-header{
background-color:#fff;	
padding: 20px;
border: 1px solid #ccc;
margin-bottom: 10px;
}

.user-block .username, .user-block .description, .user-block .comment{
margin-left: 65px;	
}
.phone{
margin-left: 10px;		
}
.user-block img {
width: 55px;
height: 55px;
   
}
.box-header>.box-tools{
top:12px;	
}

}
</style>
<link href="http://otto-guide.ethnicjob.ca/css/common.css" rel="stylesheet">
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
      @if (session('message'))
          <div class="alert alert-success">
       {{ session('message') }}
          </div>
     @endif
      
    </section>
	
    <!-- Main content -->
    <section class="content">
      
   <h4><b>Dealer Detail:</b></h4>
   <p>
  
   @foreach($dealer as $dealers)@endforeach
   
   <table width="100%" class="table">
   <tr>
   <td><b>Dealer Name:</b> @if(!empty($dealers->dealer_name) > 0)
    {{$dealers->dealer_name}}
       @endif</td>
   <td><b>Dealer Contact:</b> @if(!empty($dealers->dealer_phone) > 0)
    {{$dealers->dealer_phone}}
       @endif</td>
   <td><b>Dealer Email:</b> @if(!empty($dealers->dealer_email) > 0)
    {{$dealers->dealer_email}}
       @endif</td>
   </tr>
   
   <tr>
   <td><b>Dealer Country:</b>  @if(!empty($dealers->dealer_country) > 0)
    {{$dealers->dealer_country}}
       @endif</td>
   <td><b>Dealer City:</b> @if(!empty($dealers->dealer_city) > 0)
    {{$dealers->dealer_city}}
       @endif</td>
   <td><b>Dealer Website:</b> @if(!empty($dealers->dealer_website) > 0)
    {{$dealers->dealer_website}}
       @endif </td>
   </tr>
  
   <tr>
   <td><b>Dealer Address:</b> @if(!empty($dealers->dealer_street) > 0)
    {{$dealers->dealer_street}}
       @endif  </td>
   <td><b>Dealer Created On:</b> @if(!empty($dealers->created_at) > 0)
    {{$dealers->created_at}}
      @endif </td>
	  
    <td>
	
  </td>
   </tr>
   
   </table>
   </p>

	   <table class="table table-striped test2">
		<div class="py-5">
		<div class="container">	 
	   
	    <div class="message"width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
	   
		</div>
    <div class="container">
      <div class="form-row">   
    		 <!-- client sidebar -->
    		
    		<!-- client sidebar -->
          <div class="col-sm-7 col-md-10 col-lg-10">
            <div class="card">
           
              <div class="card-body">
                <div class="row">				        				
		
			
			@foreach($car_data as $data) @endforeach	
                <div class="col-md-4">                  
             
				  <div class="cxm-advert-item mb-4">   
				  
                <div class="cxm-img">
                      <!--<div class="cxm-img-badge position-absolute"><span class="fa fa-camera"></span> </div>-->
                      <a href="{{url('car/detail').'/'.$car_data->id}}"><img class="img-fluid" src="@if(!empty($data->media->photo_links[0]) > 0)
            						{{$data->media->photo_links[0]}}
            						@else
            						{{asset('public/images/no-image.jpeg')}}
                      @endif" width="250"></a>
                    </div>	

                    <div class="cxm-content">
                      <a class="fs18 lh18 text-dark" href="#">
            			@if(!empty($data->heading) > 0)
            			{{$data->heading}}
                        @endif
          				    </a>
                      <hr class="hr1">
                      <div><span class="fs24 fc1">
					  $ @if(!empty($data->ref_price) > 0)
						{{number_format((float)$data->ref_price)}}
						@endif
						</span>
						</div>
						<ul class="cxm-facts fs12 bg-secondary p-2 rounded">
                        <li><span class="fa fa-modx text-primary"></span> 
						@if(!empty($data->build->year) > 0)
						{{$data->build->year}}
						@endif
						</li>
                        <li><span class="fa fa-barcode text-primary"></span> 
						@if(!empty($data->build->body_type) > 0)
						{{$data->build->body_type}}
                        @endif
						</li>
                        <li><span class="fa fa-road text-primary"></span> 
						@if(!empty($data->miles) > 0)
						{{$data->miles}}
                        @endif  miles
						</li>
                        <li><span class="fa fa-toggle-on text-primary"></span>
						@if(!empty($data->build->transmission) > 0)
						{{$data->build->transmission}}
                       @endif
						</li>
                        <li><span class="fa fa-spinner text-primary"></span> 
						
						@if(!empty($data->build->engine) > 0)
						{{$data->build->engine}}
                       @endif
						</li>
                        <li><span class="fa fa-fire text-primary"></span> 
					  @if(!empty($data->build->fuel_type) > 0)
						{{$data->build->fuel_type}}
                       @endif
						</li>
                      </ul>
                      
                    </div>
                   <hr class="hr1">
                  </div>                
                </div>
			 
				
                </div>
               
              </div>
			  
			 
            </div>
          </div>  
	
		    
        </div>
      </div>      
    </div>
	   </table>


  
    </section>
    <!-- /.content -->
  </div>
  
  
@endsection