@extends('layouts.admin')
   <!-- Bootstrap CSS -->
@section('content')
<style>
.box-header{
background-color:#fff;	
padding: 20px;
border: 1px solid #ccc;
margin-bottom: 10px;
}

.user-block .username, .user-block .description, .user-block .comment{
margin-left: 65px;	
}
.phone{
margin-left: 10px;		
}
.user-block img {
width: 55px;
height: 55px;
   
}
.box-header>.box-tools{
top:12px;	
}


.green{
background-color:#00a65a;	
color:#fff;
border-radius:3px;
padding-left:8px;
padding-right:8px;
}
.red{
background-color:#cc0000;	
color:#fff;	
border-radius:3px;
padding-left:8px;
padding-right:8px;
}

th{
background-color:#e2e2e2;	
}

.nav-pills{
	border-bottom:1px solid #3c8dbc;
}

</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
        @if (session('message'))
          <div class="alert alert-success">
       {{ session('message') }}
          </div>
     @endif
      
    </section>
	
    <!-- Main content -->
    <section class="content">
      
   <h4><b>Dealer Detail:</b></h4>
   <p>
  
   @foreach($dealer as $dealers)@endforeach
   
   <table width="100%" class="table">
   <tr>
   <td><b>Dealer Name:</b>  @if(!empty($dealers->dealer_name) > 0)
    {{$dealers->dealer_name}}
       @endif</td>
   <td><b>Dealer Contact:</b>  @if(!empty($dealers->dealer_phone) > 0)
    {{$dealers->dealer_phone}}
       @endif</td>
   <td><b>Dealer Email:</b> @if(!empty($dealers->dealer_email) > 0)
    {{$dealers->dealer_email}}
       @endif</td>
   </tr>
   
   <tr>
   <td><b>Dealer Country:</b>  @if(!empty($dealers->dealer_country) > 0)
    {{$dealers->dealer_country}}
       @endif</td>
   <td><b>Dealer City:</b> @if(!empty($dealers->dealer_city) > 0)
    {{$dealers->dealer_city}}
       @endif</td>
   <td><b>Dealer Website:</b> @if(!empty($dealers->dealer_website) > 0)
    {{$dealers->dealer_website}}
       @endif </td>
   </tr>
  
   <tr>
   <td><b>Dealer Address:</b> @if(!empty($dealers->dealer_street) > 0)
    {{$dealers->dealer_street}}
       @endif  </td>
   <td><b>Dealer Created On:</b> @if(!empty($dealers->created_at) > 0)
    {{date('d-m-Y H:i:s', strtotime($dealers->created_at))}}
       @endif </td>  
  
    <td><b>Status: </b> 
	
	@if($dealers->is_block == '0')
	<span class="green"> Active </span>
	@endif
	
	@if($dealers->is_block == '1')
	<span class="red"> Blocked </span>
	@endif
	
	
	</td>
  <td>
  
	
  </td>
   </tr>
   
   <tr>
   <td> 
    <b>Dealer Registration Status:   </b> 
   @if($dealers->is_registered == '1')
	<span class="green"> Registered </span>
	@endif
	
	@if($dealers->is_registered == '0')
	<span class="red"> Unregistered </span>
	@endif
   
   </td>
   </tr>
   
   </table>
   
   
		@if($dealers->is_block =='0')
	
		<a href="{{route('dealer/account/block/', ['dealer_id' => $dealers->dealer_id])}}"
				   class="btn btn-danger test5 pull-right" onclick="return confirm(\'Are you sure you want to block?\')">
					<i class="fa fa-times"></i> Deactivated dealer account
				</a>
	
		@endif
		
	    @if($dealers->is_block =='1')
			
		<a href="{{route('dealer/account/unblock/', ['dealer_id' => $dealers->dealer_id])}}"
				   class="btn btn-success test5 pull-right" onclick="return confirm(\'Are you sure you want to unblock?\')">
					<i class="fa fa-right"></i> Activate dealer account	</a>
		@endif
   
   
   
   
   </p>
		<hr>

			<div id="exTab1">	
			<ul  class="nav nav-pills">
					  <li class="active"><a  href="#1a" data-toggle="tab">Subscription Logs</a></li>
					  <li><a href="#2a" data-toggle="tab">RBO Logs</a></li>
					  <li><a href="#3a" data-toggle="tab">Chat Logs</a></li>
					  <li><a href="#4a" data-toggle="tab">Email Logs</a></li>
					  <li><a href="#5a" data-toggle="tab">Reviews Logs</a></li>	  
					
					</ul>

				<div class="tab-content clearfix">
				
				<div class="tab-pane active" id="1a">
					 
					 <br>
					 
					 <table class="table table-striped test2" width="90%">
   
					   <th>Subscription Name</th>
					   <th>Subscription Status</th>
					   <th>Subscription Date</th>
					   <th>Updated at</th>
					   
					   <?php 
					   $get_subscription_logs = DB::table('dealer_subscription_logs')->where('dealer_id',$dealers->dealer_id)->get();
					   ?>
					   @if(count($get_subscription_logs))
					   @foreach($get_subscription_logs as $get_subscription_log)  
					   <tr>
					   <td>
					   {{str_replace('_',' ',ucfirst($get_subscription_log->subscription_name))}}
					   </td>
					   <td>
					   @if($get_subscription_log->subscription_action =='add')
					   <span class="green"> {{ ucfirst($get_subscription_log->subscription_action)}}</span>
					   @else
					   <span class="red"> {{ ucfirst($get_subscription_log->subscription_action)}}</span>  
					   @endif
					   </td>
					   <td>
					   {{date('d-m-Y H:i:s', strtotime($get_subscription_log->created_at))}}
					   </td>
					   <td>
					   {{date('d-m-Y H:i:s', strtotime($get_subscription_log->updated_at))}}
					   </td>
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> No Subscription Found </h4>  
					    @endif
					   
					   </table>
					   
					    
					   
 
				</div>
					
 

				<div class="tab-pane" id="2a">
					 <br>
				 <table class="table table-striped test2" width="90%">
   
					   <th>User Name</th>
					   <th>RBO Link</th>
					   <th>RBO Date</th>	
			        	<?php 
					   $get_rbo_logs = DB::table('request_best_offer')->where('dealer_id',$dealers->dealer_id)
					   ->join('users','request_best_offer.user_id','=','users.id')
					   ->get();
					   ?>
					   @if(count($get_rbo_logs))
					   @foreach($get_rbo_logs as $get_rbo_log)  
					   <tr>
					   <td>
					   {{$get_rbo_log->name}}{{$get_rbo_log->lastname}}
					   </td>
					   <td>
					   
					   <a href="{{url('car/detail/')}}/{{$get_rbo_log->car_id}}" target="_blank">Rbo Car Link</a>
					  
					   </td>
					   <td>
					   {{date('d-m-Y H:i:s', strtotime($get_rbo_log->created_at))}}
					   </td>
					   
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> No RBO Found </h4>  
					    @endif
					   
					   </table>
					   
					
				</div>
							
					<div class="tab-pane" id="3a">
					<br>
					 <table class="table table-striped test2" width="90%">
			
					   <th>User Name</th>
					   <th>Chat Channel Name</th>
					   <th>Chat Date</th>
					   <th>Chat Status</th>	
			        	<?php 
					   $get_chat_logs = DB::table('twilio_chat')->where('dealer_id',$dealers->dealer_id)
					   ->join('users','twilio_chat.user_id','=','users.id')
					   ->get();
					   ?>
					   @if(count($get_chat_logs))
					   @foreach($get_chat_logs  as $get_chat_log)  
					   <tr>
					   <td>
					   {{$get_chat_log->name}}{{$get_chat_log->lastname}}
					   </td>
					   <td>
					  {{$get_chat_log->chat_channel}}
					   </td>
					   <td>
					   {{date('d-m-Y H:i:s', strtotime($get_chat_log->created_at))}}
					   </td>
					   
					   <td>
					    @if($get_chat_log->is_block_current == '0')
						<span class="green"> Active </span>	
						@else
						<span class="red"> Blocked </span>	
						@endif	
					   </td>
					   
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> No Chat Activity Found </h4>  
					    @endif
					   
					   </table>
					
							</div>
					 
					 
					 <div class="tab-pane" id="4a">
					 
					 <br>
					 <table class="table table-striped test2" width="90%">
			
					   <th>User Name</th>
					   <th>Subject</th>
					   <th>Message</th>
					   <th>Emal Date</th>
			        	<?php 
					   $get_email_logs = DB::table('email_box')->where('dealer_id',$dealers->dealer_id)->where('reply_to',NULL)
					   ->join('users','email_box.client_id','=','users.id')
					   ->get();
					   ?>
					   @if(count($get_email_logs))
					   @foreach($get_email_logs  as $get_email_log)  
					   <tr>
					   <td>
					   {{$get_email_log->name}}{{$get_email_log->lastname}}
					   </td>
					   <td>
					   {{$get_email_log->subject}}
					   </td>
					   
					      <td>
					   {{$get_email_log->message}}
					   </td>
					   
					   <td>
					   {{date('d-m-Y H:i:s', strtotime($get_email_log->created_at))}}
					   </td>
					
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> No Email Found </h4>  
					    @endif
					   
					   </table>
					 
						</div>
						
						
						
						
					 <div class="tab-pane" id="5a">
					 
					 <br>
					 <table class="table table-striped test2" width="90%">
			
					   <th>User Name</th>
					   <th>Review Title</th>
					   <th>Message</th>
					    <th>Rating</th>
					   <th>Review Date</th>
			        	<?php 
					   $get_review_logs = DB::table('client_reviews')->where('dealer_id',$dealers->dealer_id)->where('reply_to',NULL)
					   ->join('users','client_reviews.client_id','=','users.id')
					   ->get();
					   ?>
					   @if(count($get_review_logs))
					   @foreach($get_review_logs  as $get_review_log)  
					   <tr>
					   <td>
					   {{$get_review_log->name}}{{$get_review_log->lastname}}
					   </td>
					   <td>
					   {{$get_review_log->title}}
					   </td>
					   
					    <td>
					   {{$get_review_log->message}}
					   </td>
					   
					    <td>
					   {{$get_review_log->rating}}.0
					   </td>
					   
					   <td>
					   {{date('d-m-Y H:i:s', strtotime($get_review_log->created_at))}}
					   </td>
					
					   </tr>
					   @endforeach
					   
						@else
					    <h4 align="center"> No Review Found </h4>  
					    @endif
					   
					   </table>
					 
						</div>
						
						
						
						
						</div>
			  </div>








   <table class="table table-striped test2">
    
 @foreach($data as $user)
 
	<div class="col-md-11 col-sm-11 col-xs-12" >
         <div class="box-header with-border">
              <div class="user-block">
                <img class="img-circle" src="<?php if(!empty($user['image']) > 0){
				echo $user['image'];
			    } ?>" alt="User Image">
                <span class="username">
				@if(!empty($user['name']) > 0)
				{{$user['name']}}
			   @endif
				</span>
                <span class="description">
				@if(!empty($user['email']) > 0)
				{{$user['email']}}
			    @endif
				</span> 
				<span class="phone">
				@if(!empty($user['mobile']) > 0)
				{{$user['mobile']}}
			    @endif
				</span>
						
              </div>
              <!-- /.user-block -->
              <div class="box-tools">
			  
              <p><a href="{{route('client_lp_list',['client_id' => $user['client_id'],'dealer_id' => $dealers->dealer_id])}}"><button type="button" class="btn btn-small btn-primary">Lease/Purchase</button></a></p>
                <p><button type="button" class="btn btn-small btn-primary">Chat</button></p>
          
              </div>
              <!-- /.box-tools -->
            </div>
          <!-- /.info-box -->
        </div>
	   
	@endforeach
	
  </table>


  
    </section>
    <!-- /.content -->
  </div>
  
  
@endsection