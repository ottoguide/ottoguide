
@extends('layouts.admin')

   <!-- Bootstrap CSS -->


@section('content')

<style>
.green{
	background-color: #9DCE2B;
	color: #fff;
	padding:3px;
   font-size:12px;
	text-align: center;
	border-radius: 5px;
}
.red{
	background-color: #cc0000;
	color: #fff;
	padding:3px;
	font-size:12px;
	border-radius: 5px;
	text-align: center;
}
</style>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Delear Activity
        <small></small>
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">
       <table id="data-table" width="100%" class="table table-striped">
    <thead>
      <tr class="test3">
        <th class="primary">Dealer Id</th>
        <th class="primary">Name</th>
        <th class="primary">Email</th>
        <th class="primary">Phone</th>
		<th class="primary">Created On</th>
		<th class="primary">Status</th>
		<th class="primary">Action</th>
   
      </tr>
    </thead>
   
  </table>

    </section>

    <!-- /.content -->
  </div>
  @endsection

@section('page_script')
   <script>
		$('#data-table').DataTable($.extend(datatable_options, {
		"scrollX": true,
			ajax: '{{route('dealer_activity_list_table')}}',
			columns: [
				{ data: 'dealer_id', name: 'dealer_id' },
				{ data: 'dealer_name', name: 'dealer_name' },
				{ data: 'dealer_email', name: 'dealer_email' },
				{ data: 'dealer_phone', name: 'dealer_phone' },
                { data: 'created_at', name: 'created_at' },
				{ data: 'status', name: 'status' },
				{ data: 'action', name: 'action' },
				
			]
		}));
    </script>


	
@endsection