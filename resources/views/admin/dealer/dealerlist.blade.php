
@extends('layouts.admin')

   <!-- Bootstrap CSS -->


@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Delear List
        <small></small>
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">
       <table id="data-table" width="100%" class="table table-striped">
    <thead>
      <tr class="test3">
        <th class="primary">Dealer Id</th>
        <th class="primary">Name</th>
        <th class="primary">Email</th>
        <th class="primary">Mobile</th>
		<th class="primary">Created On</th>
		<th class="primary">Action</th>
   
      </tr>
    </thead>
   
  </table>

    </section>

    <!-- /.content -->
  </div>
  @endsection

@section('page_script')
   <script>
		$('#data-table').DataTable($.extend(datatable_options, {
		"scrollX": true,
			ajax: '{{route('dealer_list_table')}}',
			columns: [
				{ data: 'dealer_id', name: 'dealer_id' },
				{ data: 'dealer_name', name: 'dealer_name' },
				{ data: 'dealer_email', name: 'dealer_email' },
				{ data: 'login_phone', name: 'login_phone' },
                { data: 'created_at', name: 'created_at' },
				{ data: 'action', name: 'action' },
				
			]
		}));
    </script>


	
@endsection