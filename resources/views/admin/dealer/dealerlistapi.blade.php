
@extends('layouts.admin')

   <!-- Bootstrap CSS -->


@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Delear List From API
        <small></small>
      </h1>
     
	   @if (session('success'))
		 <br>
       <div class="alert alert-success">
         {{ session('success') }}
         </div>
		 <br>
        @endif
	 
	 <!--<a href="{{url('get-dealer.html')}}"><button class="btn btn-medium btn-success pull-right import">IMPORT DEALERS </button></a>-->
	 
    </section>
	
	<br><br>

   <!-- Main content -->
    <section class="content">
       <table id="data-table" width="100%" class="table table-striped">
    <thead>
      <tr class="test3">
        <th class="primary">Dealer Id</th>
        <th class="primary">Name</th>
        <th class="primary">Email</th>
        <th class="primary">Phone</th> 
		<th class="primary">State</th>
		<th class="primary">Created On</th>
		<th class="primary">Action</th>
      </tr>
    </thead>
   
    </table>
	
    </section>

    <!-- /.content -->
  </div>
  @endsection
  
 
@section('page_script')

    <script>
	$(".import").click(function() {
	$(".import").attr('disabled',true);
	});
	</script>

   <script>
		$('#data-table').DataTable($.extend(datatable_options, {
		"scrollX": true,
			ajax: '{{route('dealer_table_api')}}',
			columns: [
				{ data: 'dealer_id', name: 'dealer_id' },
				{ data: 'dealer_name', name: 'dealer_name' },
				{ data: 'dealer_email', name: 'dealer_email' },
				{ data: 'dealer_phone', name: 'dealer_phone' },
				{ data: 'dealer_state', name: 'dealer_state' },
                { data: 'created_at', name: 'created_at' },
				{ data: 'action', name: 'action' },	
			]
		}));
		

		
		
		
    </script>


@endsection