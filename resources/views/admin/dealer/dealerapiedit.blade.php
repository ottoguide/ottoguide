@extends('layouts.admin')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dealer
                <small>Edit</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Edit</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Edit Dealer</h3>
                </div>
                <div class="box-body form-horizontal">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    {!! Form::open(array('url' => 'dealer/api/update', 'enctype' => 'multipart/form-data')) !!}
                    {!! Form::hidden('_token', csrf_token()) !!}

                    @include('admin.dealer.dealerform.formapi', $dealer)

                    <div class="row">
                        <div class="col-sm-2 col-sm-offset-10">
                            {!! Form::hidden('dealer_id', $dealer->dealer_id) !!}
							{!! Form::hidden('old-password', $dealer->password) !!}
                            {!! Form::submit('Update Dealer', ['class' => 'btn btn-success btn-block']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


@endsection
