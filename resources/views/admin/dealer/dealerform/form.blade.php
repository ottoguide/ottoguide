	<div class="form-group">
		<div class="col-sm-6">
			{!! Form::label('name', 'Dealer Name') !!}
			{!! Form::text('name', isset($dealer) ? $dealer->dealer_name : null, ['class' => 'form-control']) !!}
		</div>
		
		
		<div class="col-sm-6">
			{!! Form::label('Contact Name', 'Dealer Contact Name') !!}
			{!! Form::text('dealer_contact_name', isset($dealer) ? $dealer->dealer_contact_name : null, ['class' => 'form-control']) !!}
		</div>
	
	</div>



	<div class="form-group">
	  
	  
	  	<div class="col-sm-6">
			{!! Form::label('Email', 'Email') !!}
			{!! Form::text('email', isset($dealer) ? $dealer->dealer_email : null, ['class' => 'form-control']) !!}
		</div>
	  
	  
		<div class="col-sm-6">
			{!! Form::label('dealer phone', 'Dealer phone') !!}
			{!! Form::number('dealer_phone', isset($dealer) ? $dealer->dealer_phone : null, ['class' => 'form-control']) !!}
		</div>
		
	</div>



	<div class="form-group">
		   
			 <div class="col-sm-6">
							<?php                                                                        
								$dealer_phones = explode(',', $dealer->dealer_other_phone); 
								?>	
										
								{!! Form::label('Dealer Other Phones', 'Dealer Other Phones') !!}								
								@foreach($dealer_phones as  $dealer_phone)
														
								@if(!empty($dealer_phone))
								   
								<input type="number" class="form-control" value="{{$dealer_phone}}" name="dealer_other_phone[]">
								<br>
								
								@endif
								
								@endforeach
			
			</div>
		   
		   
		   
			  <div class="col-sm-6">
							<?php                                                                        
								$dealer_emails = explode(',', $dealer->dealer_other_emails); 
								?>	
										
								{!! Form::label('Dealer Other Emails', 'Dealer Other Emails') !!}								
								@foreach($dealer_emails as  $dealer_email)
														
								@if(!empty($dealer_email))
								   
								<input type="email" class="form-control" value="{{$dealer_email}}" name="dealer_other_emails[]">
								<br>
								
								@endif
								
								@endforeach
			
			</div>
			
			
			 
		</div>



	<div class="form-group">
	 
	 <div class="col-sm-6">
			{!! Form::label('dealer Zipcode', 'Dealer zipcode') !!}
			{!! Form::number('dealer_zip', isset($dealer) ? $dealer->dealer_zip : null, ['class' => 'form-control']) !!}
		</div>
		
		<div class="col-sm-6">
			{!! Form::label('dealer website', 'Dealer Website') !!}
			{!! Form::text('dealer_website', isset($dealer) ? $dealer->dealer_website : null, ['class' => 'form-control']) !!}
		</div>
	 
	 </div>

	<div class="form-group">
	   
		<div class="col-sm-6">
			{!! Form::label('Address', 'Address') !!}
			{!! Form::text('address', isset($dealer) ? $dealer->dealer_street : null, ['class' => 'form-control']) !!}
		</div>
	   
		   <div class="col-sm-6">
			{!! Form::label('Radius', 'Radius') !!}
			{!! Form::text('radius', isset($dealer) ? $dealer->radius : null, ['class' => 'form-control']) !!}
		</div>
	   
		 
	</div>


	
	</div>


	<div class="form-group">
		<div class="col-sm-6">
			{!! Form::label('password', 'Password') !!}
			{!! Form::password('password', ['class' => 'password form-control']) !!}
		</div>
		<div class="col-sm-6">
			{!! Form::label('password_confirmation', 'Confirm Password') !!}
			{!! Form::password('password_confirmation', ['class' => 'cpassword form-control']) !!}
		</div>
	</div>


	<div class="form-group">
	   
			<div class="col-sm-6">
	<br><br>
		<button type="button" class="btn btn-xs btn-primary change_password">Change Password</button>
		</div>
	</div>
<!--
<div class="form-group">
    <div class="col-sm-6">
        {!! Form::label('photos', 'User Image') !!}
        {!! Form::file('photos', ['class' => 'form-control']) !!}
    </div>
</div>
-->