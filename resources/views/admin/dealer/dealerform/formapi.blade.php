<div class="form-group">
    <div class="col-sm-6">
        {!! Form::label('name', 'First Name') !!}
        {!! Form::text('name', isset($dealer) ? $dealer->dealer_name : null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('Email', 'Email') !!}
        {!! Form::text('email', isset($dealer) ? $dealer->dealer_email : null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group">
  
    <div class="col-sm-6">
        {!! Form::label('dealer phone', 'Dealer phone') !!}
        {!! Form::text('dealer_phone', isset($dealer) ? $dealer->dealer_phone : null, ['class' => 'form-control']) !!}
    </div>
	
	 <div class="col-sm-6">
        {!! Form::label('Address', 'Address') !!}
        {!! Form::text('address', isset($dealer) ? $dealer->dealer_street : null, ['class' => 'form-control']) !!}
    </div>
	
</div>

<div class="form-group">
 
 <div class="col-sm-6">
        {!! Form::label('dealer Zipcode', 'Dealer zipcode') !!}
        {!! Form::text('dealer_zip', isset($dealer) ? $dealer->dealer_zip : null, ['class' => 'form-control']) !!}
    </div>
	
	<div class="col-sm-6">
        {!! Form::label('dealer website', 'Dealer Website') !!}
        {!! Form::text('dealer_website', isset($dealer) ? $dealer->dealer_website : null, ['class' => 'form-control']) !!}
    </div>
 
 </div>



<!--
<div class="form-group">
    <div class="col-sm-6">
        {!! Form::label('photos', 'User Image') !!}
        {!! Form::file('photos', ['class' => 'form-control']) !!}
    </div>
</div>
-->