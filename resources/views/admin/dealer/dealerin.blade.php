
@extends('layouts.admin')

   <!-- Bootstrap CSS -->

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     <section class="content-header">
        @if (session('message'))
          <div class="alert alert-success">
       {{ session('message') }}
          </div>
     @endif
      
    </section>
	
    <!-- Main content -->
    <section class="content">
      
   <h4>Dealer Detail:</h4>
   <p>
  
   @foreach($dealer as $dealers)@endforeach
   
   <table width="100%" class="table">
   <tr>
   <td><b>Dealer Name:</b>  @if(!empty($dealers->dealer_name) > 0)
    {{$dealers->dealer_name}}
       @endif</td>
   <td><b>Dealer Contact:</b>  @if(!empty($dealers->dealer_phone) > 0)
    {{$dealers->dealer_phone}}
       @endif</td>
   <td><b>Dealer Email:</b> @if(!empty($dealers->dealer_email) > 0)
    {{$dealers->dealer_email}}
       @endif</td>
   </tr>
   
   <tr>
   <td><b>Dealer Country:</b>  @if(!empty($dealers->dealer_country) > 0)
    {{$dealers->dealer_country}}
       @endif</td>
   <td><b>Dealer City:</b> @if(!empty($dealers->dealer_city) > 0)
    {{$dealers->dealer_city}}
       @endif</td>
   <td><b>Dealer Website:</b>  @if(!empty($dealers->dealer_website) > 0)
    {{$dealers->dealer_website}}
       @endif </td>
   </tr>
   
   <tr>
   <td><b>Dealer Address:</b> @if(!empty($dealers->dealer_street) > 0)
    {{$dealers->dealer_street}}
       @endif  </td>
   <td><b>Dealer Created On:</b> @if(!empty($dealers->created_at) > 0)
    {{$dealers->created_at}}
       @endif </td>
  <td>
  
		@if(!$dealer_status->isEmpty())	
		<b>Status:</b> Active
		@else
		<b>Status:</b> Blocked
	@endif
  </td>
   </tr>
   </table>
   </p>

   <sub>Last Activity on: 27-Feb-2018 4:50pm </sub>

   <!--<h4>Dealer Subscription:</h4>

   <table class="table table-striped test2">
    <thead>
      <tr class="test3">
        <th class="">Subscription Name</th>
        <th class="">Expired On</th>
        <th class="">Current Status</th>
        <th class="">Amount</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Golden</td>
        <td>Next month</td>
        <td>active</td>
        <td>$200</td>
        
      </tr>
      <tr>
        <td>Golden</td>
        <td>Next month</td>
        <td>active</td>
        <td>$200</td>
      </tr>
      
      
    </tbody>
  </table>

  <h4 class="fz19"><b>Total Amount Paid:</b> $200</h4>
  -->
   <div class="row fz3">
	
		@if(!$dealer_status->isEmpty())	
    	<div class="col-md-3">
		<a href="{{route('dealer/block/', ['dealer_id' => encrypt($dealer[0]['dealer_id'])])}}"
				   class="btn btn-primary test5" onclick="return confirm(\'Are you sure you want to block?\')">
					<i class="fa fa-lock"></i> Block this dealer
				</a>
		</div>
		@else
		<div class="col-md-3">
		<a href="{{route('dealer/unblock/', ['dealer_id' => encrypt($dealer[0]['dealer_id'])])}}"
				   class="btn btn-success test5" onclick="return confirm(\'Are you sure you want to unblock?\')">
					<i class="fa fa-unlock"></i> Unblock this dealer
				</a>
		</div>
		
		@endif
			
		
		
    	<div class="col-md-6">
		</div>
    	<div class="col-md-3"><a href="{{route('dealer-list')}}"><button class="btn btn-primary test5">Cancel</button></a></div>
    </div>
  
  
  <div class="row">
  <div class="col-md-4">
  
  <h4><b>Competition Radius</b></h4>

		<form action="" method="post" id="sort_form">
       
	   <select class="form-control" class="dealer_radius" name="dealer_radius">
         <option value="" disabled selected>Select Dealer Radius</option>
		<option value="10" <?php if(isset($dealer[0]['radius'])) { echo $dealer[0]['radius'] == '10' ? 'selected' : ''; }?>>10 Miles</option>
        <option value="20" <?php if(isset($dealer[0]['radius'])) { echo $dealer[0]['radius'] == '20' ? 'selected' : ''; }?>>20 Miles</option>
        <option value="30" <?php if(isset($dealer[0]['radius'])) { echo $dealer[0]['radius'] == '30' ? 'selected' : ''; }?>>30 Miles</option>
        <option value="40" <?php if(isset($dealer[0]['radius'])) { echo $dealer[0]['radius'] == '40' ? 'selected' : ''; }?>>40 Miles</option>
        <option value="50" <?php if(isset($dealer[0]['radius'])) { echo $dealer[0]['radius'] == '50' ? 'selected' : ''; }?>>50 Miles</option>

        </select>
		
	<input type="hidden" value="{{$dealer[0]['dealer_id']}}" class="dealer_id" name="dealer_id">	
		<br>
 
	   
		</div>
		
	 </form>	
  </div>
  
  
    </section>
    <!-- /.content -->
  </div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

  
	<script>
	$('select').on('change', function() {
		
	  var radius = this.value;
	  var dealer = $('.dealer_id').val();

			$.ajax({
				type:'POST',
				data: {
				"_token": "{{ csrf_token() }}",	
				 radius: radius,
				 dealer_id:dealer
				},
				
				url:'{{url('dealer_add_radius.html')}}',
				
				success:  function () {
				alert('Dealer Radius Added Successfully');
				}
				
		}); 
	 
	});
	</script> 
  
@endsection