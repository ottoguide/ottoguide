@extends('layouts.application')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users Roles
                <small>All Roles List</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Roles List</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-10"></div>
                                <div class="col-xs-2">
                                    <a href="{{route('create_role')}}" class="btn btn-primary btn-block margin-bottom">
                                        Add New Role
                                    </a>
                                </div>
                            </div>
                            <table width="100%" id="data-table" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center" width="4%">#</th>
                                    <th>Role Name</th>
                                    <th>Description</th>
                                    <th>System Defined</th>
                                    <th>Permissions</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
@endsection


@section('page_script')
    <script>
		$('#data-table').DataTable($.extend(datatable_options, {
		"scrollX": true,
			ajax: '{{route('roles_list_table')}}',
			columns: [
				{ data: 'DT_Row_Index', name: 'display_name' },
				{ data: 'display_name', name: 'display_name' },
				{ data: 'description', name: 'description' },
				{ data: 'system_defined', name: 'system_defined' },
				{ data: 'permissions', name: 'permissions' },
				{ data: 'action', name: 'action', orderable: false, searchable: false},
			]
		}));
    </script>
@endsection