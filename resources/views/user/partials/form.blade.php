<div class="form-group">
    <div class="col-sm-6">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', isset($user) ? $user->name : null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('email', 'Email Address') !!}
        {!! Form::email('email', isset($user) ? $user->email : null, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-6">
        {!! Form::label('phone', 'Contact Number') !!}
        {!! Form::number('phone', isset($user) ? $user->phone : null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('role', 'User Role') !!}
        {!! Form::select('role', $roles_list, isset($user) ? $user->roles[0]->id : null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-6">
        {!! Form::label('password', 'Password') !!}
        {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('password_confirmation', 'Confirm Password') !!}
        {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-6">
        {!! Form::label('photos', 'User Image') !!}
        {!! Form::file('photos', ['class' => 'form-control']) !!}
    </div>
</div>