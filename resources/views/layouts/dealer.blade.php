@section('header')
    @include('includes.dealer-header')

@show

@yield('content')

@section('web-footer')
    @include('includes.dealer-footer')
@show