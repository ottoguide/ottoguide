@section('header')
    @include('includes.admin-header')
	@include('includes.admin-sidebar')
@show

@yield('content')

@section('web-footer')
    @include('includes.admin-footer')
@show