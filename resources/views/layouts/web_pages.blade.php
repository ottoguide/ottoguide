@section('header')
    @include('includes.header')
@show

@yield('content')

@section('web-footer')
    @include('includes.footer')
@show

@yield('searchPageScript')