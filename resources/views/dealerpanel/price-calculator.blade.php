<style>
	.scrollable_list {
		list-style: none;
		margin: 0;
		background: #f3f3f3;
		padding: 0;
		max-height: 200px;
		overflow: auto;
	}
	.scrollable_list li label {
		margin: 0;
	}
	.scrollable_list li {
		padding: 5px 10px;
	}
	.scrollable_list li:hover {
		background: #fffadc;
	}
</style>

<div class="card">
	<div class="card-body">

		<nav class="mb-3">
			<div class="nav nav-pills nav-justified" id="search_type_tabs" role="tablist">
				<a class="nav-item nav-link active show" data-toggle="tab" href="#tab_search_used" role="tab" aria-controls="used" aria-selected="true">Used</a>
				<a class="nav-item nav-link" data-toggle="tab" href="#tab_search_new" role="tab" aria-controls="new" aria-selected="false">New</a>
				<a class="nav-item nav-link" data-toggle="tab" href="#tab_search_used" role="tab" aria-controls="certified" aria-selected="false">Certified</a>
			</div>
		</nav>
		<div class="tab-content" id="search_type_tabs_content">
			<div class="tab-pane fade show active" id="tab_search_used" role="tabpanel">

				<div class="text-center mb-3">
					<div class="btn-group" role="group" aria-label="Basic example">
						<button type="button" class="btn btn-secondary used-search-type-btn active" data-type="car">By Car</button>
						<button type="button" class="btn btn-secondary used-search-type-btn" data-type="body">By Body Style</button>
						<button type="button" class="btn btn-secondary used-search-type-btn" data-type="price">By Price</button>
					</div>
				</div>

				<div class="used-search-type-car">
					<form action="" method="get" id="search_used_car_form">
						<input type="hidden" name="page" value="{{ (isset($_GET['page']) ? $_GET['page'] : 1 ) }}">
						<input type="hidden" name="sort_by" value="{{ (isset($_GET['sort_by']) ? $_GET['sort_by'] : 'PRICE_ASC' ) }}">
						<input type="hidden" name="car_type" value="used">
						<div class="form-group">
							<select name="make" onchange="load_make_param($(this).val());" id="make_select"  class="form-control">
								<option value="">All Makes</option>
								<option value="Acura">Acura</option>
								<option value="Audi">Audi</option>
								<option value="BMW">BMW</option>
								<option value="Buick">Buick</option>
								<option value="Cadillac">Cadillac</option>
								<option value="Chevrolet">Chevrolet</option>
								<option value="Chrysler">Chrysler</option>
								<option value="Dodge">Dodge</option>
								<option value="FIAT">FIAT</option>
								<option value="Ford">Ford</option>
								<option value="GMC">GMC</option>
								<option value="HUMMER">HUMMER</option>
								<option value="Honda">Honda</option>
								<option value="Hyundai">Hyundai</option>
								<option value="INFINITI">INFINITI</option>
								<option value="Isuzu">Isuzu</option>
								<option value="Jaguar">Jaguar</option>
								<option value="Jeep">Jeep</option>
								<option value="KIA">KIA</option>
								<option value="Land Rover">Land Rover</option>
								<option value="Lexus">Lexus</option>
								<option value="Lincoln">Lincoln</option>
								<option value="MINI">MINI</option>
								<option value="Mazda">Mazda</option>
								<option value="Mercedes-Benz">Mercedes-Benz</option>
								<option value="Mercury">Mercury</option>
								<option value="Mitsubishi">Mitsubishi</option>
								<option value="Nissan">Nissan</option>
								<option value="Pontiac">Pontiac</option>
								<option value="Porsche">Porsche</option>
								<option value="RAM">RAM</option>
								<option value="Saturn">Saturn</option>
								<option value="Scion">Scion</option>
								<option value="Subaru">Subaru</option>
								<option value="Suzuki">Suzuki</option>
								<option value="Toyota">Toyota</option>
								<option value="Volkswagen">Volkswagen</option>
								<option value="Volvo">Volvo</option>
							</select>
						</div>
						<div class="form-group">
							<select name="model" onchange="load_model_param($(this).val());" id="model_select" class="form-control">
								<option value="">All Models</option>
							</select>
						</div>
						<div class="form-group row">
							<div class="col-sm-5">
								<select name="year_from" id="year_from" class="form-control">
									<option value="">All Years</option>
								</select>
							</div>
							<div class="col-sm-2 text-center pt-2">to</div>
							<div class="col-sm-5">
								<select name="year_to" id="year_to" class="form-control">
									<option value="">All Years</option>
								</select>
							</div>
						</div>
						@if(false):
						<div class="form-group">
							<label class="mb-0">Zip</label>
							<input type="number" class="form-control text-right" name="zip">
						</div>
						<div class="form-group">
							<label class="mb-0">Radius</label>
							<select name="radius" class="form-control">
								<option value="500">Any Distance</option>
								<option value="10">10 Miles</option>
								<option value="20">20 Miles</option>
								<option value="30">30 Miles</option>
								<option value="40">40 Miles</option>
								<option value="50">50 Miles</option>
							</select>
						</div>
					@endif
						<div class="text-center">
							<button type="submit" class="btn btn-warning">SEARCH</button>
						</div>
					</form>
				</div>

				<div class="used-search-type-other" style="display:none;">
					<form action="" method="get" id="search_used_other_form">
						<input type="hidden" name="page" value="{{ (isset($_GET['page']) ? $_GET['page'] : 1 ) }}">
						<input type="hidden" name="sort_by" value="{{ (isset($_GET['sort_by']) ? $_GET['sort_by'] : 'PRICE_ASC' ) }}">
						<input type="hidden" name="car_type" value="used">
						<div class="form-group used-search-type-body">
							<label class="mb-0">Body Style</label>
							<select name="body_type" class="form-control">
								<option value="">Any</option>
								<option value="Sedan">Sedan</option>
								<option value="SUV">SUV / Crossover</option>
								<option value="Hatchback">Hatchback</option>
								<option value="Convertible">Convertible</option>
								<option value="Van">Van</option>
								<option value="Minivan">Minivan</option>
								<option value="Pickup ">Pickup Truck</option>
								<option value="Coupe">Coupe</option>
								<option value="Wagon">Wagon</option>
							</select>
						</div>
						@if(false):
						<div class="form-group row">
							<div class="col-sm-7">
								<label class="mb-0">Zip</label>
								<input type="number" class="form-control text-right" name="zip">
							</div>
							<div class="col-sm-5">
								<label class="mb-0">Radius</label>
								<select name="radius" class="form-control">
									<option value="500">Any Distance</option>
									<option value="10">10 Miles</option>
									<option value="20">20 Miles</option>
									<option value="30">30 Miles</option>
									<option value="40">40 Miles</option>
									<option value="50">50 Miles</option>
								</select>
							</div>
						</div>
						@endif
						<div class="form-group">
							<label class="mb-0">Years</label>
							<div class="row">
								<div class="col-sm-5">
									<select name="year_from" class="form-control">
										<option value="">All</option>
										<?php
										$years = range (date('Y'), 1980);
										foreach($years as $year) { echo '<option value="'.$year.'">'.$year.'</option>'; }
										?>
									</select>
								</div>
								<div class="col-sm-2 text-center pt-2">to</div>
								<div class="col-sm-5">
									<select name="year_to" class="form-control">
										<option value="">All</option>
										<?php
										$years = range (date('Y'), 1980);
										foreach($years as $year) { echo '<option value="'.$year.'">'.$year.'</option>'; }
										?>
									</select>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="mb-0">Price</label>
							<div class="row">
								<div class="col-sm-5">
									<input type="number" class="form-control text-right" name="price_from">
								</div>
								<div class="col-sm-2 text-center pt-2">to</div>
								<div class="col-sm-5">
									<input type="number" class="form-control text-right" name="price_to">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="mb-0">Maximum Mileage</label>
							<div class="row">
								<div class="col-sm-7">
									<input type="number" class="form-control text-right" name="max_miles">
								</div>
								<div class="col-sm-5 pt-2">Miles</div>
							</div>
						</div>

						<div class="form-group">
							<label class="mb-0">Transmission</label>
							<div>
								<div class="form-check-inline">
									<label class="form-check-label"><input type="radio" class="form-check-input" name="filter_transmission" value="any" checked> Any</label>
								</div>
								<div class="form-check-inline">
									<label class="form-check-label"><input type="radio" class="form-check-input" name="filter_transmission" value="automatic"> Automatic</label>
								</div>
								<div class="form-check-inline">
									<label class="form-check-label"><input type="radio" class="form-check-input" name="filter_transmission" value="manual"> Manual</label>
								</div>
							</div>
						</div>

						<div class="text-center">
							<button type="submit" class="btn btn-warning">SEARCH</button>
						</div>
					</form>
				</div>


			</div>


			<div class="tab-pane fade" id="tab_search_new" role="tabpanel">
				<form action="" method="get" id="search_new_form">
					<input type="hidden" name="page" value="{{ (isset($_GET['page']) ? $_GET['page'] : 1 ) }}">
					<input type="hidden" name="sort_by" value="{{ (isset($_GET['sort_by']) ? $_GET['sort_by'] : 'PRICE_ASC' ) }}">
					<input type="hidden" name="car_type" value="new">
					<div class="form-group">
						<select name="make" onchange="load_make_param_new($(this).val());" id="make_select_new"  class="form-control">
							<option value="">All Makes</option>
							<option value="Acura">Acura</option>
							<option value="Audi">Audi</option>
							<option value="BMW">BMW</option>
							<option value="Buick">Buick</option>
							<option value="Cadillac">Cadillac</option>
							<option value="Chevrolet">Chevrolet</option>
							<option value="Chrysler">Chrysler</option>
							<option value="Dodge">Dodge</option>
							<option value="FIAT">FIAT</option>
							<option value="Ford">Ford</option>
							<option value="GMC">GMC</option>
							<option value="HUMMER">HUMMER</option>
							<option value="Honda">Honda</option>
							<option value="Hyundai">Hyundai</option>
							<option value="INFINITI">INFINITI</option>
							<option value="Isuzu">Isuzu</option>
							<option value="Jaguar">Jaguar</option>
							<option value="Jeep">Jeep</option>
							<option value="KIA">KIA</option>
							<option value="Land Rover">Land Rover</option>
							<option value="Lexus">Lexus</option>
							<option value="Lincoln">Lincoln</option>
							<option value="MINI">MINI</option>
							<option value="Mazda">Mazda</option>
							<option value="Mercedes-Benz">Mercedes-Benz</option>
							<option value="Mercury">Mercury</option>
							<option value="Mitsubishi">Mitsubishi</option>
							<option value="Nissan">Nissan</option>
							<option value="Pontiac">Pontiac</option>
							<option value="Porsche">Porsche</option>
							<option value="RAM">RAM</option>
							<option value="Saturn">Saturn</option>
							<option value="Scion">Scion</option>
							<option value="Subaru">Subaru</option>
							<option value="Suzuki">Suzuki</option>
							<option value="Toyota">Toyota</option>
							<option value="Volkswagen">Volkswagen</option>
							<option value="Volvo">Volvo</option>
						</select>
					</div>
					<div class="form-group">
						<select name="model" onchange="load_model_param_new($(this).val());" id="model_select_new" class="form-control">
							<option value="">All Models</option>
						</select>
					</div>
					<div class="form-group row">
						<div class="col-sm-5">
							<select name="year_from" id="year_from_new" class="form-control">
								<option value="">All Years</option>
							</select>
						</div>
						<div class="col-sm-2 text-center pt-2">to</div>
						<div class="col-sm-5">
							<select name="year_to" id="year_to_new" class="form-control">
								<option value="">All Years</option>
							</select>
						</div>
					</div>
					@if(false):
					<div class="form-group">
						<label class="mb-0">Zip</label>
						<input type="number" class="form-control text-right" name="zip">
					</div>
					<div class="form-group">
						<label class="mb-0">Radius</label>
						<select name="radius" class="form-control">
							<option value="500">Any Distance</option>
							<option value="10">10 Miles</option>
							<option value="20">20 Miles</option>
							<option value="30">30 Miles</option>
							<option value="40">40 Miles</option>
							<option value="50">50 Miles</option>
						</select>
					</div>
				@endif
					<div class="text-center">
						<button type="submit" class="btn btn-warning">SEARCH</button>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>


<?php
// filters
$filter_price = (isset($_GET['filter_price']) && $_GET['filter_price'] != '') ? explode('-', $_GET['filter_price']) : array(0, 200000);
$filter_miles = (isset($_GET['filter_miles']) && $_GET['filter_miles'] != '') ? explode('-', $_GET['filter_miles']) : array(0, 58000);
$filter_market_days = (isset($_GET['filter_market_days']) && $_GET['filter_market_days'] != '') ? explode('-', $_GET['filter_market_days']) : array(0, 300);
$filter_fuel = (isset($_GET['filter_fuel']) && $_GET['filter_fuel'] != '') ? explode('-', $_GET['filter_fuel']) : array(20, 80);
?>

<div class="card mt-3">
	<div class="card-body">
		<h4>Filter Results</h4>

		<form action="" method="get" id="filter_form">
			<input type="hidden" name="page" value="{{ (isset($_GET['page']) ? $_GET['page'] : 1 ) }}">
			<input type="hidden" name="sort_by" value="{{ (isset($_GET['sort_by']) ? $_GET['sort_by'] : 'PRICE_ASC' ) }}">
			<input type="hidden" name="car_type" value="{{ (isset($_GET['car_type']) ? $_GET['car_type'] : '' ) }}">
			<input type="hidden" name="make" value="{{ (isset($_GET['make']) ? $_GET['make'] : '' ) }}">
			<input type="hidden" name="model" value="{{ (isset($_GET['model']) ? $_GET['model'] : '' ) }}">
			<input type="hidden" name="year_from" value="{{ (isset($_GET['year_from']) ? $_GET['year_from'] : '' ) }}">
			<input type="hidden" name="year_to" value="{{ (isset($_GET['year_to']) ? $_GET['year_to'] : '' ) }}">
			<input type="hidden" name="zip" value="{{ (isset($_GET['zip']) ? $_GET['zip'] : '' ) }}">
			<input type="hidden" name="radius" value="{{ (isset($_GET['radius']) ? $_GET['radius'] : '' ) }}">
			<input type="hidden" name="body_type" value="{{ (isset($_GET['body_type']) ? $_GET['body_type'] : '' ) }}">
			<div class="form-group">
				<label>Price</label>
				<div id="filter_price_slider"></div>
				<span class="text-muted filter_price_slider_text">${{$filter_price[0]}} - ${{$filter_price[1]}}</span>
				<input type="hidden" name="filter_price" class="filter_price_slider_value" value="{{implode('-', $filter_price)}}">
			</div>

			<div class="form-group">
				<label>Mileage</label>
				<div id="filter_miles_slider"></div>
				<span class="text-muted filter_miles_slider_text">{{$filter_miles[0]}} - {{$filter_miles[1]}} miles</span>
				<input type="hidden" name="filter_miles" class="filter_miles_slider_value" value="{{implode('-', $filter_miles)}}">
			</div>

			<div class="form-group">
				<label>Transmission</label>
				<div>
					<div class="btn-group btn-group-toggle" data-toggle="buttons">
						<label class="btn btn-secondary <?= (!isset($_GET['filter_transmission']) || (isset($_GET['filter_transmission']) && $_GET['filter_transmission'] == 'any')) ? 'active' : '' ?>">
							<input type="radio" name="filter_transmission" value="any" <?= (!isset($_GET['filter_transmission']) || (isset($_GET['filter_transmission']) && $_GET['filter_transmission'] == 'any')) ? 'checked' : '' ?>> Any
						</label>
						<label class="btn btn-secondary <?= (isset($_GET['filter_transmission']) && $_GET['filter_transmission'] == 'Manual') ? 'active' : '' ?>">
							<input type="radio" name="filter_transmission" value="manual" <?= (isset($_GET['filter_transmission']) && $_GET['filter_transmission'] == 'manual') ? 'checked' : '' ?>> Manual
						</label>
						<label class="btn btn-secondary <?= (isset($_GET['filter_transmission']) && $_GET['filter_transmission'] == 'Automatic') ? 'active' : '' ?>">
							<input type="radio" name="filter_transmission" value="automatic" <?= (isset($_GET['filter_transmission']) && $_GET['filter_transmission'] == 'automatic') ? 'checked' : '' ?>> Automatic
						</label>
					</div>
				</div>
			</div>

			{{--<div class="form-group">
				<label>Nationwide Shipping</label>
				<div>
					<div class="checkbox">
						<label><input type="checkbox" name="" > Hide nationwide shipping</label>
					</div>
				</div>
			</div>--}}

			<div class="form-group">
				<label>Days on Market</label>
				<div id="filter_market_days_slider"></div>
				<span class="text-muted filter_market_days_slider_text">{{$filter_market_days[0]}} - {{$filter_market_days[1]}} days</span>
				<input type="hidden" name="filter_market_days" class="filter_market_days_slider_value" value="{{implode('-', $filter_market_days)}}">
			</div>

			<?php if(isset($datas) && count($datas) > 0) { ?>
			<div class="form-group">
				<label>Color</label>
				<ul class="scrollable_list">
					<?php
						$all_colors = array();
					foreach($datas as $key => $data) {
						if (isset($data['search_data']->car_data) &&
							isset(json_decode($data['search_data']->car_data)->exterior_color)) {
							$color = json_decode($data['search_data']->car_data)->exterior_color;
							if (!in_array($color, $all_colors)) {
								$all_colors[] = $color;
								echo '<li>
								<label><input type="checkbox" name="filter_color[]" value="'.$color.'"> '.$color.'</label>
							</li>';
							}

						}
					}
					?>
				</ul>
			</div>
			<?php } ?>



			<?php if(isset($datas) && count($datas) > 0) { ?>
			<div class="form-group">
				<label>Engine</label>
				<ul class="scrollable_list">
					<?php
						$all_engine = array();
					foreach($datas as $key => $data) {
						if (isset($data['search_data']->car_data) &&
							isset(json_decode($data['search_data']->car_data)->build->engine)) {
							$engine = json_decode($data['search_data']->car_data)->build->engine;
							if (!in_array($engine, $all_engine)) {
								$all_engine[] = $engine;
								echo '<li>
								<label><input type="checkbox" name="filter_engine[]" value="'.$engine.'"> '.$engine.'</label>
							</li>';
							}

						}
					}
					?>
				</ul>
			</div>
			<?php } ?>


			<!--
			<div class="form-group">
				<label>Options</label>
				<ul class="scrollable_list">
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Alloy Wheels"> Alloy Wheels</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Backup Camera"> Backup Camera</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Bluetooth"> Bluetooth</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Climate Package"> Climate Package</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Comfort Package"> Comfort Package</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Driver Assistance Package"> Driver Assistance Package</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Heat Package"> Heat Package</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Leather Seats"> Leather Seats</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="M Sport Package"> M Sport Package</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Navigation System"> Navigation System</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Premium Package"> Premium Package</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Sport Package"> Sport Package</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Sunroof/Moonroof"> Sunroof/Moonroof</label>
					</li>
					<li>
						<label><input type="checkbox" name="filter_option[]" value="Technology Package"> Technology Package</label>
					</li>
				</ul>
			</div>
			-->

			{{--<div class="form-group">
				<label>Fuel Economy</label>
				<div id="filter_fuel_slider"></div>
				<span class="text-muted filter_fuel_slider_text">{{$filter_fuel[0]}} - {{$filter_fuel[1]}} mpg</span>
				<input type="hidden" name="filter_fuel" class="filter_fuel_slider_value" value="{{implode('-', $filter_fuel)}}">
			</div>--}}

			<div class="text-center">
				<button type="submit" class="btn btn-warning">FILTER</button>
			</div>
		</form>
	</div>
</div>

<script>
	$(document).on('click', '.used-search-type-btn', function (e) {
		e.stopImmediatePropagation();

		var type = $(this).attr('data-type');

		$('.used-search-type-btn').removeClass('active');
		$(this).addClass('active');

		if (type == 'car') {
			$('.used-search-type-car').show();
			$('.used-search-type-other').hide();
			$('.used-search-type-body').hide();
		} else if (type == 'body') {
			$('.used-search-type-car').hide();
			$('.used-search-type-other').show();
			$('.used-search-type-body').show();
		} else if (type == 'price') {
			$('.used-search-type-car').hide();
			$('.used-search-type-other').show();
			$('.used-search-type-body').hide();
		}
	});

	$('#filter_price_slider').slider({
		range: true,
		min: 0,
		max: 200000,
		values: <?= json_encode($filter_price) ?>,
		slide: function( event, ui ) {
			$('.filter_price_slider_text').text("$" + ui.values[0] + " - $" + ui.values[1]);
			$('.filter_price_slider_value').val(ui.values[0] + "-" + ui.values[1]);
		}
	});
	$('#filter_miles_slider').slider({
		range: true,
		min: 0,
		max: 200000,
		values: <?= json_encode($filter_miles) ?>,
		slide: function( event, ui ) {
			$('.filter_miles_slider_text').text(ui.values[0] + " - " + ui.values[1] + " miles");
			$('.filter_miles_slider_value').val(ui.values[0] + "-" + ui.values[1]);
		}
	});
	$('#filter_market_days_slider').slider({
		range: true,
		min: 0,
		max: 1000,
		values: <?= json_encode($filter_market_days) ?>,
		slide: function( event, ui ) {
			$('.filter_market_days_slider_text').text(ui.values[0] + " - " + ui.values[1] + " days");
			$('.filter_market_days_slider_value').val(ui.values[0] + "-" + ui.values[1]);
		}
	});
	$('#filter_fuel_slider').slider({
		range: true,
		min: 20,
		max: 50,
		values: <?= json_encode($filter_fuel) ?>,
		slide: function( event, ui ) {
			$('.filter_fuel_slider_text').text(ui.values[0] + " - " + ui.values[1] + " mpg");
			$('.filter_fuel_slider_value').val(ui.values[0] + "-" + ui.values[1]);
		}
	});



    function load_make_param(type){

        var data = "_token={{csrf_token()}}";
        	data += "&load_model=1&make="+type;
        	data += "&car_type=used";

        $.ajax({
            url: "{{url('/load/param_search')}}",
            type:'POST',
            data: data,
            beforeSend: function(){
                $("#model_select").html('<option value="" >Loading...</option>');
            },
            success: function(html) {
                console.log(html);
                $("#model_select").html(html);
            },
        });

        return false;
    }

    function load_model_param(type){

        var data = "_token={{csrf_token()}}";
        data += "&make=" + $("#make_select").val();
        data += "&model="+type;
        data += "&type=year";
        data += "&car_type=used";

        $.ajax({
            url: "{{url('/load/param_search')}}",
            type:'POST',
            data: data,
            beforeSend: function(){
                $("#year_from").html('<option value="">Loading...</option>');
                $("#year_to").html('<option value="">Loading...</option>');
            },
            success: function(html) {
                console.log(html);
                $("#year_from").html(html);
                $("#year_to").html(html);
            },
        });

        return false;
    }

    function load_make_param_new(type){

        var data = "_token={{csrf_token()}}";
        data += "&load_model=1&make="+type;
        data += "&car_type=new";

        $.ajax({
            url: "{{url('/load/param_search')}}",
            type:'POST',
            data: data,
            beforeSend: function(){
                $("#model_select_new").html('<option value="">Loading...</option>');
            },
            success: function(html) {
                console.log(html);
                $("#model_select_new").html(html);
            },
        });

        return false;
    }

    function load_model_param_new(type){

        var data = "_token={{csrf_token()}}";
        data += "&make=" + $("#make_select_new").val();
        data += "&model="+type;
        data += "&type=year";
        data += "&car_type=new";


        $.ajax({
            url: "{{url('/load/param_search')}}",
            type:'POST',
            data: data,
            beforeSend: function(){
                $("#year_from_new").html('<option value="">Loading...</option>');
                $("#year_to_new").html('<option value="">Loading...</option>');
            },
            success: function(html) {
                console.log(html);
                $("#year_from_new").html(html);
                $("#year_to_new").html(html);
            },
        });

        return false;
    }


</script>
