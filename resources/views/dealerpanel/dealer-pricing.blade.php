@extends('layouts.web_pages')
@section('header')
    @parent

    <style>
        .car-box {
            position: relative;
            padding: 20px 10px;
            border-bottom: 1px solid #ccc;
        }
        .car-box:hover {
            background: #fffadc;
        }
        .car-box .car-box-top {
            border-bottom: 1px solid #eee;
            padding-bottom: 5px;
            margin-bottom: 10px;
        }
        .car-box .car-box-top:after {
            content: '';
            display: block;
            clear: both;
        }
        .car-box .car-edit-btn {
            position: absolute;
            top: 20px;
            right: 0;
        }
        .car-box .car-image-area {
            float: left;
            width: 160px;
            position: relative;
        }
        .car-box .car-image-area img {
            width: 100%;
            height: 110px;
            border: 1px solid #ccc;
        }
        .car-box .car-status {
            text-align: center;
            background: rgba(255, 179, 60, 0.6);
            color: #fff;
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
        }
        .car-box .car-details-area {
            float: left;
            width: calc(100% - 200px);
            padding: 0 10px;
        }
        .car-box .car-name {
            font-size: 120%;
        }
        .car-box table.car-details-table {
            width: 100%;
            border: 0;
        }
        .car-box .car-deal-area:after {
            content: '';
            display: block;
            clear: both;
        }
        .car-box .car-deal-type {
            float: left;
            width: 160px;
            min-height: 1px;
            position: relative;
            text-align: center;
        }
        .car-box .car-deal-details {
            float: left;
            width: calc(100% - 200px);
            padding: 20px 10px 0;
        }
        .car-box table.car-deal-table {
            width: 100%;
            border: 0;
        }
        .car-box .car-deal-type img {
            width: 60px;
        }
        .car-box .car-deal-type i.fa {
            font-size: 3em;
            margin-bottom: 5px;
        }
        .car-box .car-deal-type p {
            font-weight: bold;
        }
        .car-box input.car-new-price {
            color: #388E3C;
            padding: 2px 5px;
            border: 1px solid #ccc;
            display: block;
            width: 100%;
        }

        .ui-slider-range {
            background: #158cba;
        }
        .ui-slider-handle {
            border-color: #158cba;
        }

        .price_slider_box {
            display: inline-block;
            width: 87%;
        }

        .save-car-price-range {
            position: relative;
            z-index: 20;
            display: inline-block;
            margin-left: 10px;
        }
    </style>

@endsection

@section('content')
    <div class="header-margin py-5">
        <div class="container">
            @include('dealer.includes.nav', array(
                'tab' => 'dealer-pricing',
				'page_title' => 'Pricing Tool'
            ))

            <div class="row bg-secondary">
                <div class="col-md-12">

                    <div class="row ">
                        <div class="col-md-12">

                            <div class="row">
                                <aside class="col-sm-4 margin">
                                    @include('dealerpanel.price-calculator')
                                </aside>


                                <div class="col-md-8">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <form action="" method="get" id="sort_form">
                                                <input type="hidden" name="page" value="{{ (isset($_GET['page']) ? $_GET['page'] : 1 ) }}">
                                                <select class="form-control" name="sort_by" onchange="$('#sort_form').submit();">
                                                    <option value="PRICE_ASC" class="ASC" <?= (isset($_GET['sort_by']) && $_GET['sort_by'] == 'PRICE_ASC') ? 'selected' : '' ?>>Lowest price first</option>
                                                    <option value="PRICE_DESC" class="DESC" <?= (isset($_GET['sort_by']) && $_GET['sort_by'] == 'PRICE_DESC') ? 'selected' : '' ?>>Highest price first</option>
                                                    <option value="MILES_ASC" class="ASC" <?= (isset($_GET['sort_by']) && $_GET['sort_by'] == 'MILES_ASC') ? 'selected' : '' ?>>Lowest mileage first</option>
                                                    <option value="MILES_DESC" class="DESC" <?= (isset($_GET['sort_by']) && $_GET['sort_by'] == 'MILES_DESC') ? 'selected' : '' ?>>Highest mileage first</option>
                                                    <option value="YEAR_ASC" class="ASC" <?= (isset($_GET['sort_by']) && $_GET['sort_by'] == 'YEAR_ASC') ? 'selected' : '' ?>>Oldest first (by car year)</option>
                                                    <option value="YEAR_DESC" class="DESC" <?= (isset($_GET['sort_by']) && $_GET['sort_by'] == 'YEAR_DESC') ? 'selected' : '' ?>>Newest first (by car year)</option>
                                                    <option value="DOM_ASC" class="ASC" <?= (isset($_GET['sort_by']) && $_GET['sort_by'] == 'DOM_ASC') ? 'selected' : '' ?>>Newest listings first</option>
                                                    <option value="DOM_DESC" class="DESC" <?= (isset($_GET['sort_by']) && $_GET['sort_by'] == 'DOM_DESC') ? 'selected' : '' ?>>Oldest listings first</option>

                                                    {{--<option value="DEAL_ASC" class="ASC" <?= (isset($_GET['sort_by']) && $_GET['sort_by'] == 'DEAL_ASC') ? 'selected' : '' ?>>Best deals first</option>--}}
                                                    {{--<option value="DEAL_DESC" class="DESC" <?= (isset($_GET['sort_by']) && $_GET['sort_by'] == 'DEAL_DESC') ? 'selected' : '' ?>>Worst deals first</option>--}}
                                                    {{--<option value="PROXIMITY" class="ASC">Closest first</option>--}}
                                                    {{--<option value="PROXIMITY" class="DESC">Farthest first</option>--}}

                                                </select>
                                            </form>
                                        </div>
                                        <div class="col-sm-8 text-right">
                                            <p class="text-muted small">
                                                @if(isset($datas) && count($datas) > 0)
                                                    <?php
                                                    $page = isset($_GET['page']) ? (($_GET['page'] * 10) - 10) : 1;
                                                    if ($page <= 0) {
                                                    	$page = 1;
                                                    }
                                                    $page_to = isset($_GET['page']) ? (($_GET['page'] * 10)) : 10;
                                                    if ($page_to > $total_records) {
                                                    	$page_to = $total_records;
                                                    }
													?>
                                                <strong>{{$page}} - {{$page_to}}</strong> out of <strong>{{$total_records}}</strong> listings
                                                @endif
                                            </p>
                                        </div>
                                    </div>

                                    <div class="card" style="margin-top: 20px">
                                        <div class="card-header text-white bg-primary">All Listings</div>
                                        <div class="card-body">
                                            @if(isset($datas) && count($datas) > 0)
                                            @foreach($datas as $key => $data)
                                                <div class="car-box" data-id="{{$data['car_id']}}">
                                                    <div class="car-box-top">
                                                        {{--<button type="button" class="btn btn-default car-edit-btn">Edit</button>--}}
                                                        <div class="car-image-area">
                                                            <img src="{{$data['pics'][0]}}" alt="">
                                                            <?php
                                                            if (isset($data['search_data']->search_rank) && $data['search_data']->search_rank != 'N/A') {
                                                            	echo '<div class="car-status">Viewed</div>';
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="car-details-area">
                                                            <div class="car-name">{{$data['title']}}</div>
                                                            <table class="car-details-table">
                                                                <tr>
                                                                    <th>Price:</th>
                                                                    <td>${{number_format($data['price'])}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Mileage:</th>
                                                                    <td>{{$data['miles']}} mi</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Location:</th>
                                                                    {{--<td>Houston, TX <small>82 mi</small></td>--}}
                                                                </tr>
                                                                <tr>
                                                                    <th>Days on Market:</th>
                                                                    <td>{{$data['market_days']}} Days at dealership</td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="car-deal-area">
                                                        <div class="car-deal-type" id="car_deal_type_{{$key}}">
                                                            <?php
                                                            if (isset($data['search_data']->car_deal)) {
                                                            	if ($data['search_data']->car_deal == 'great' || $data['search_data']->car_deal == 'good') {
                                                            		echo '<i class="fa fa-arrow-down text-success"></i>';
                                                                } else if ($data['search_data']->car_deal == 'average') {
																	echo '<i class="fa fa-arrows-v text-warning"></i>';
                                                                } else if ($data['search_data']->car_deal == 'high') {
																	echo '<i class="fa fa-arrow-up text-danger"></i>';
                                                                }
                                                                //echo '<img src="http://dev.ottoguide.com/public/dealer/images/arrow_right.png" alt="">';
                                                                echo '<p>'. strtoupper($data['search_data']->car_deal) .' PRICE</p>';
                                                            } else {
																echo '<i class="fa fa-ban text-danger"></i>';
																echo '<p>NO PRICE</p>';
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="car-deal-details">
                                                            <table class="car-deal-table">
                                                                <tr>
                                                                    <th>New Price:</th>
                                                                    <td><input type="text" class="car-new-price" id="car_price_{{$key}}" value="${{number_format($data['price'])}}" readonly></td>
                                                                    <th>Search Rank:</th>
                                                                    <td>{{$data['search_data']->search_rank}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Saving:</th>
                                                                    <td><strong>0</strong></td>
                                                                    <th>Search Page:</th>
                                                                    <td><strong>{{$data['search_data']->search_page}}</strong></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="car-price-range" >
                                                        <div id="price_slider_{{$key}}" class="price_slider_box"></div>
                                                        <button class="btn btn-sm save-car-price-range" id="save_car_price_range_{{$key}}" value="{{$data['price']}}"><i class="fa fa-check"></i></button>
                                                        <button title="Reset" class="btn btn-sm btn-info"
                                                                onclick='
                                                                        $("#car_price_{{$key}}").val("$"+$("#price_{{$key}}").val());
                                                                        _slider_{{$key}}.slider("value",$("#price_{{$key}}").val());'
                                                                value="{{$data['price']}}"><i class="fa fa-refresh"></i></button>
                                                       <input type="hidden" id="price_{{$key}}" value="{{$data['price']}}">
                                                        <script>

															var _slider_{{$key}} = $('#price_slider_{{$key}}').slider({
																range: "min",
																min: parseInt('{{$data['price'] - (($data['price'] * 50) / 100)}}'),
																max: parseInt('{{$data['price'] + (($data['price'] * 50) / 100)}}'),
																value: parseInt('{{$data['price']}}'),
																slide: function( event, ui ) {
																	$('#car_price_{{$key}}').val("$" + ui.value.format());

																	{{--var myColor = getTheColor(ui.value, parseInt('{{$data['price'] + (($data['price'] * 50) / 100)}}'));--}}
																	{{--$( "#price_slider_{{$key}} .ui-slider-range" ).css( "background-color", myColor );--}}
																	{{--$( "#price_slider_{{$key}} .ui-state-default, #price_slider_{{$key}} .ui-widget-content .ui-state-default" ).css( "background-color", myColor );--}}
																},
																change: function( event, ui ) {
																	console.log('update! ', ui.value);
                                                                    $("#save_car_price_range_{{$key}}").prop('value', ui.value);
																	$.ajax({
																		type:    'post',
																		url:     '<?= route('dealer-update-car-price-deal') ?>',
																		data:    {
																			new_price: "{{$data['price']}}",
																			_token: '{{csrf_token()}}',
                                                                            car_id: '{{$data['car_id']}}'
                                                                        },
																		success: function (data) {
																			$('#car_deal_type_{{$key}}').html(data);
																		},
																		error:   function () {
																		}
																	});
                                                                }
															});

                                                            $("#save_car_price_range_{{$key}}").click(function () {

                                                                var carPrice = $("#save_car_price_range_{{$key}}").val();
                                                                console.log('update! ', carPrice);

                                                                $.ajax({
                                                                    type:    'post',
                                                                    url:     '<?= route('dealer-update-car-price-deal') ?>',
                                                                    data:    {
                                                                        new_price: carPrice,
                                                                        _token: '{{csrf_token()}}',
                                                                        car_id: '{{$data['car_id']}}'
                                                                    },
                                                                    success: function (data) {
                                                                        $("#price_{{$key}}").val(carPrice);
                                                                    },
                                                                    error:   function () {
                                                                    }
                                                                });

                                                                $.ajax({
                                                                    type:    'post',
                                                                    url:     '<?= route('dealer-update-car-price-deal-addon-log') ?>',
                                                                    data:    {
                                                                        new_price: carPrice,
                                                                        _token: '{{csrf_token()}}',
                                                                        car_id: '{{$data['car_id']}}'
                                                                    },
                                                                    success: function (data) {
                                                                    },
                                                                    error:   function () {
                                                                    }
                                                                });
                                                            });

                                                        </script>
                                                    </div>
                                                </div>

                                            @endforeach
                                            @else
                                                <p class="text-center">No Records found!</p>
                                            @endif
                                        </div>
                                        <div class="card-body">
                                            <ul class="pagination justify-content-center">
												<?php
												$links = $datas->render();
												$links = str_replace("<a", "<a class='page-link ' ", $links);
												$links = str_replace("<li", "<li class='page-item' ", $links);
												$links = str_replace("<span", "<span class='page-link'",$links);
												echo $links;
												?>
                                            </ul>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
@endsection



@section('web-footer')
    @parent

    <script>

		function getTheColor(colorVal, max_price) {
			var percent = (colorVal * 100) / max_price;
			console.log('p='+percent+' - c='+colorVal+' - m='+max_price);
			var theColor = "";
			var myRed = "";
			var myGreen = "";
			if ( percent > 50 ) {
				myRed = 255;
				myGreen = parseInt( ( ( percent * 2 ) * 255 ) / 100 );
			} else {
				myRed = parseInt( ( ( 100 - percent ) * 2 ) * 255 / 100 );
				myGreen = 255;
			}
			theColor = "rgb(" + myRed + "," + myGreen + ",0)";
			return( theColor );
		}

		Number.prototype.format = function(n, x) {
			var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
			return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
		};
    </script>

@endsection