<?php $active1 = "active"; ?>
<?php include('head.php');?>
<?php $page_title = "Dashboard"; ?>
<?php include('header.php');

?>

		<div class="row bg-secondary">
	
					<div class="panel with-nav-tabs panel-primary">
						<div class="panel-heading">
								
							<ul class="nav nav-tabs">
									<li class="active"><a href="#tab1primary" data-toggle="tab">Interaction Summary</a></li>
									<li><a href="#tab1primary" data-toggle="tab">Vehicle Details Page (VDP)</a></li>
									<li><a href="#tab3primary" data-toggle="tab">Search Result</a></li>
									<li><a href="#tab2primary" data-toggle="tab">Dealer Inside</a></li>
						     </ul>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								
								<div class="tab-pane fade in active" id="tab1primary">
								
								<div align="center"><h3>Interaction by Month</h3></div>	
								<?php include('bar-graph.php');?>
								</div>
								
								<div class="tab-pane fade" id="tab2primary">
								<?php include('pie-graph.php');?>
								</div>
								
								<div class="tab-pane fade" id="tab3primary">
								<?php include('bar-graph2.php');?>
								
								</div>
								
								<div class="tab-pane fade" id="tab4primary">
								<div align="center"><h3>Vehicle Detail Page(VDP) Views by Month</h3></div>	
								
								</div>	
								
								</div>
								
								<div class="tab-pane fade" id="tab5primary">Primary 5</div>
						</div>
						</div>
					</div>
		
		</div>		
	

	</div>

	
</div>



<!-- ========================= SECTION CONTENT END// ========================= -->


<!-- ========================= FOOTER ========================= -->
<footer class="section-footer bg-secondary">
	<div class="container">

			<br> 
		</section>
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-6"> 
				<p class="text-white-50">  <br> .</p>
			</div>
			<div class="col-sm-6 text-right">
				<p class="text-sm-right text-white-50">
	Copyright &copy 2018 <br>
<a href="" class="text-white-50">miniMAX Solution</a>
				</p>
			</div>
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


</body>
<?php include('footer-scripts.php');?>

</html>