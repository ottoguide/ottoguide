
	
	
	<div class="cg-dealFinder-search-wrap ui-corner-all text-center">

    <div id="listingModelContextToggle" class="btn-group btn-group-justified text-center" data-toggle="buttons">
      <label class="btn btn-sm cg-listingResults-searchModelContextBtn active" data-value="usedCarModels">
        <input type="radio" name="model-context" id="usedModels" autocomplete="off" checked="">Search Used
      </label>
      <label class="btn btn-sm cg-listingResults-searchModelContextBtn" data-value="newCarModels">
        <input type="radio" name="model-context" id="newModels" autocomplete="off">Search New
      </label>
    </div>


    <div id="listingSearchTypeToggle" class="cg-dealFinder-searchType-toggle btn-group btn-group-justified text-center" data-toggle="buttons" style="">
      <label class="btn btn-default btn-sm active" data-value="car">
        <input type="radio" name="search-type" autocomplete="off" checked="">By Car
      </label>
        <label class="btn btn-default btn-sm" data-value="bodyStyle">
          <input type="radio" name="search-type" autocomplete="off">By Body Style
        </label>
		
      <label class="btn btn-default btn-sm" data-value="price">
        <input type="radio" name="search-type" autocomplete="off">By Price
      </label>
    </div>

		<div id="listingSearchUsedSearchFormsDiv" class="cg-dealFinder-searchSectionsWrap ft-listing-results__used-car-form" style="">
			  <div>
		<div id="vehicleSearch">

		<div class="cg-dealFinder-searchByCar-form">

		<form id="newSearchHeaderForm_UsedCar" name="newSearchHeaderForm_UsedCar" onsubmit="return processSearchWithBlock(this, 'listingsViewMacrosNew_UsedCar', function() {return validateInventorySearchForm($('#newSearchHeaderForm_UsedCar')[0], 'car');});" action="#" method="GET">

		<input type="hidden" name="sourceContext" value="HomePageModel" id="newSearchHeaderForm_UsedCar_sourceContext">

		<input type="hidden" name="newSearchFromOverviewPage" value="true" id="newSearchHeaderForm_UsedCar_newSearchFromOverviewPage">

		<input type="hidden" name="inventorySearchWidgetType" value="AUTO" id="newSearchHeaderForm_UsedCar_inventorySearchWidgetType">


		<div id="listingsViewMacrosNew_UsedCar">
		<script>
		  if (typeof CG === 'object' && CG !== null && CG.hasOwnProperty('CarPicker')) {
			CG.CarPicker.createInstance(
			  "listingsViewMacrosNew_UsedCar",
			  "entitySelectingHelper.selectedEntity",
			  "entitySelectingHelper.selectedEntity2",
			  "",
			  "CG.Inventory.adjustSearchRadius(this.form, 100, 500);",
			  "CG.Inventory.adjustSearchRadius(this.form, 100, 500);",
			  "CG.Inventory.adjustSearchRadius(this.form, 100, 500);",
			  "",
			  "CG.Inventory.adjustSearchRadius(this.form), 100, 500);",
			  "en",
			false ,
			true).initialize();
		  } else if (typeof CG_FTL_LOGGER === 'object' && CG_FTL_LOGGER !== null) {
			CG_FTL_LOGGER.error('Error creating CarPicker in ftl: inventorylistings/headerSelectCar.ftl');
		  }
		</script>
		</div>


		<input type="hidden" name="entitySelectingHelper.selectedEntity" value="d2262" id="listingsViewMacrosNew_UsedCarentitySelectingHelper.selectedEntity">


		<input type="hidden" name="entitySelectingHelper.selectedEntity2" value="" id="newSearchHeaderForm_UsedCar_entitySelectingHelper_selectedEntity2">
			<div>

		<select class="select maker-select-dropdown btn-small form-control ft-make-selector" style="width: 260px;" cg-data-showinactive="false" cg-data-newcarsonly="false" cg-data-useinventoryservice="true" cg-data-name="entitySelectingHelper.selectedEntity" cg-data-includeallmodelsoption="false" cg-data-carswithregressiononly="false" cg-data-groupbypopularity="true" cg-name="ign-makerId-entitySelectingHelper.selectedEntity" id="select-37816082">
			<option value="-1">All Makes</option>


			<optgroup label="Popular Makes">
				  
			<option value="m4">Acura</option> 
			<option value="m124">Alfa Romeo</option> 
			<option value="m19">Audi</option> 
			<option selected="selected" value="m3">BMW</option> 
			<option value="m21">Buick</option> 
			<option value="m22">Cadillac</option> 
			<option value="m1">Chevrolet</option> 
			<option value="m23">Chrysler</option> 
			<option value="m24">Dodge</option> 
			<option value="m98">FIAT</option> 
			<option value="m2">Ford</option> 
			<option value="m26">GMC</option> 
			<option value="m6">Honda</option> 
			<option value="m28">Hyundai</option> 
			<option value="m84">INFINITI</option> 
			<option value="m31">Jaguar</option> 
			<option value="m32">Jeep</option> 
			<option value="m33">Kia</option> 
			<option value="m35">Land Rover</option> 
			<option value="m37">Lexus</option> 
			<option value="m38">Lincoln</option> 
			<option value="m40">Maserati</option> 
			<option value="m42">Mazda</option> 
			<option value="m43">Mercedes-Benz</option> 
			<option value="m44">Mercury</option> 
			<option value="m45">MINI</option> 
			<option value="m46">Mitsubishi</option> 
			<option value="m12">Nissan</option> 
			<option value="m47">Pontiac</option> 
			<option value="m48">Porsche</option> 
			<option value="m191">Ram</option> 
			<option value="m52">Scion</option> 
			<option value="m53">Subaru</option> 
			<option value="m7">Toyota</option> 
			<option value="m55">Volkswagen</option> 
			<option value="m56">Volvo</option> 
			</optgroup>
					
			<optgroup label="All Makes">
			<option value="m4">Acura</option> 
			<option value="m124">Alfa Romeo</option> 
			<option value="m79">AM General</option> 
			<option value="m133">AMC</option> 
			<option value="m110">Aston Martin</option> 
			<option value="m19">Audi</option> 
			<option value="m119">Austin</option> 
			<option value="m139">Austin-Healey</option> 
			<option value="m234">Autobianchi</option> 
			<option value="m230">Avanti</option> 
			<option value="m20">Bentley</option> 
			<option value="m3">BMW</option> 
			<option value="m196">Bricklin</option> 
			<option value="m113">Bugatti</option> 
			<option value="m21">Buick</option> 
			<option value="m22">Cadillac</option> 
			<option value="m1">Chevrolet</option> 
			<option value="m23">Chrysler</option> 
			<option value="m117">Citroen</option> 
			<option value="m81">Daewoo</option> 
			<option value="m95">Daihatsu</option> 
			<option value="m96">Datsun</option> 
			<option value="m179">De Tomaso</option> 
			<option value="m97">DeLorean</option> 
			<option value="m208">DeSoto</option> 
			<option value="m24">Dodge</option> 
			<option value="m82">Eagle</option> 
			<option value="m174">Edsel</option> 
			<option value="m25">Ferrari</option> 
			<option value="m98">FIAT</option> 
			<option value="m183">Fisker</option> 
			<option value="m2">Ford</option> 
			<option value="m216">Franklin</option> 
			<option value="m99">Freightliner</option> 
			<option value="m203">Genesis</option> 
			<option value="m83">Geo</option> 
			<option value="m26">GMC</option> 
			<option value="m6">Honda</option> 
			<option value="m186">Hudson</option> 
			<option value="m27">Hummer</option> 
			<option value="m28">Hyundai</option> 
			<option value="m84">INFINITI</option> 
			<option value="m158">International Harvester</option> 
			<option value="m30">Isuzu</option> 
			<option value="m31">Jaguar</option> 
			<option value="m32">Jeep</option> 
			<option value="m200">Jensen</option> 
			<option value="m189">Kaiser</option> 
			<option value="m233">Karma</option> 
			<option value="m33">Kia</option> 
			<option value="m34">Lamborghini</option> 
			<option value="m100">Lancia</option> 
			<option value="m35">Land Rover</option> 
			<option value="m37">Lexus</option> 
			<option value="m38">Lincoln</option> 
			<option value="m39">Lotus</option> 
			<option value="m40">Maserati</option> 
			<option value="m41">Maybach</option> 
			<option value="m42">Mazda</option> 
			<option value="m141">McLaren</option> 
			<option value="m43">Mercedes-Benz</option> 
			<option value="m44">Mercury</option> 
			<option value="m102">MG</option> 
			<option value="m45">MINI</option> 
			<option value="m46">Mitsubishi</option> 
			<option value="m150">Morgan</option> 
			<option value="m151">Morris</option> 
			<option value="m199">Nash</option> 
			<option value="m12">Nissan</option> 
			<option value="m85">Oldsmobile</option> 
			<option value="m103">Opel</option> 
			<option value="m176">Packard</option> 
			<option value="m121">Pagani</option> 
			<option value="m86">Panoz</option> 
			<option value="m104">Peugeot</option> 
			<option value="m105">Pininfarina</option> 
			<option value="m87">Plymouth</option> 
			<option value="m47">Pontiac</option> 
			<option value="m48">Porsche</option> 
			<option value="m191">Ram</option> 
			<option value="m49">Rolls-Royce</option> 
			<option value="m131">Rover</option> 
			<option value="m50">Saab</option> 
			<option value="m108">Saleen</option> 
			<option value="m51">Saturn</option> 
			<option value="m221">Saxon</option> 
			<option value="m52">Scion</option> 
			<option value="m135">Shelby</option> 
			<option value="m111">smart</option> 
			<option value="m143">Spyker</option> 
			<option value="m194">SRT</option> 
			<option value="m109">Sterling</option> 
			<option value="m164">Studebaker</option> 
			<option value="m53">Subaru</option> 
			<option value="m132">Sunbeam-Talbot</option> 
			<option value="m54">Suzuki</option> 
			<option value="m112">Tesla</option> 
			<option value="m7">Toyota</option> 
			<option value="m134">Trabant</option> 
			<option value="m137">Triumph</option> 
			<option value="m55">Volkswagen</option> 
			<option value="m56">Volvo</option> 
			<option value="m205">VPG</option> 
			<option value="m214">Willys</option> 
					</optgroup>
				</select>
				
			</div>
			<div>

			<select class="select model-select-dropdown btn-small form-control ft-model-selector" style="width: 260px;" cg-name="ign-modelId-entitySelectingHelper.selectedEntity" cg-data-yeardropdownheading="All Years" cg-data-trimdropdownheading="Select Trim" cg-data-groupbypopularity="true"> 
					
			<option>All Models</option>

				
			<optgroup label="Popular Models" class="activeModelGroup">

			<option value="d1052">1 Series</option> 
			<option selected="selected" value="d2262">2 Series</option> 
			<option value="d1512">3 Series</option> 
			<option value="d2240">3 Series Gran Turismo</option> 
			<option value="d2244">4 Series</option> 
			<option value="d1628">5 Series</option> 
			<option value="d2075">5 Series Gran Turismo</option> 
			<option value="d1513">6 Series</option> 
			<option value="d2615">6 Series Gran Turismo</option> 
			<option value="d1517">7 Series</option> 
			<option value="d2263">i3</option> 
			<option value="d2274">i8</option> 
			<option value="d2396">M2</option> 
			<option value="d390">M3</option> 
			<option value="d2258">M4</option> 
			<option value="d391">M5</option> 
			<option value="d825">M6</option> 
			<option value="d2160">X1</option> 
			<option value="d2623">X2</option> 
			<option value="d392">X3</option> 
			<option value="d2271">X4</option> 
			<option value="d393">X5</option> 
			<option value="d2120">X5 M</option> 
			<option value="d1137">X6</option> 
			<option value="d2139">X6 M</option> 
			<option value="d394">Z3</option> 
			<option value="d2161">Z3 M</option> 
			<option value="d395">Z4</option> 

					</optgroup>
					<optgroup label="Other Models" class="inactiveModelGroup">
			<option value="d2172">1M</option> 
			<option value="d1266">2002</option> 
			<option value="d2380">3.0CS</option> 
			<option value="d1627">8 Series</option> 
			<option value="d2186">ActiveHybrid 3</option> 
			<option value="d2180">ActiveHybrid 5</option> 
			<option value="d2181">ActiveHybrid 7</option> 
			<option value="d1626">Isetta</option> 
			<option value="d2162">Z4 M</option> 
			<option value="d396">Z8</option> 
					</optgroup>
			  </select>
		</div>
		<div>
			<table>
				<tbody><tr>
					<td>
			

		 <select class="select car-select-dropdown btn-small form-control ft-year" cg-data-carswithregressiononly="false" cg-data-includeunknowntrimoption="false" style="width: 110px;" cg-name="ign-carId-entitySelectingHelper.selectedEntity">
		<option>All Years</option>
		<option value="c27097">2018</option> 
		<option value="c26137">2017</option> 
		<option value="c25071">2016</option> 
		<option value="c24538">2015</option> 
		<option value="c24317">2014</option> 
		  </select>
					</td>
					<td class="tdLabel carYearsSeparator">
						to
					</td>
					<td>
    
				<select class="select car-select-dropdown btn-small form-control ft-year" style="width: 110px;" cg-name="ign-car2Id-entitySelectingHelper.selectedEntity" disabled=""><option value="">All Years</option>
				<option value="c27097">2018</option> 
			<option value="c26137">2017</option> 
			<option value="c25071">2016</option> 
			<option value="c24538">2015</option> 
			<option value="c24317">2014</option> 
				</select>
						</td>
					</tr>
				</tbody></table>
			</div>


         <table class="cg-dealFinder-searchByCar-zipRadius">
           <tbody><tr>
             <td class="tdLabel"><label for="zip">ZIP</label></td>
             <td class="tdContent">

		<input type="text" name="zip" maxlength="5" value="75850" class="cg-postalCode form-control" style="margin-top: 1px;">  </td>
           </tr>
           <tr>
            <td class="tdLabel"><label for="distance">Radius</label></td>
            <td class="tdContent">


		<select name="distance" id="newSearchHeaderForm_UsedCar_distance" class="distance form-control">
			<option value="10">10 mi</option>
			<option value="25">25 mi</option>
			<option value="50">50 mi</option>
			<option value="75">75 mi</option>
			<option value="100" selected="selected">100 mi</option>
			<option value="150">150 mi</option>
			<option value="200">200 mi</option>
			<option value="500">500 mi</option>
			<option value="50000">Nationwide</option>


		</select>

            </td>
           </tr>
         </tbody></table>
         <div>
          <input type="submit" value="Search" class="btn btn-warning newSearchSubmitButton ft-submit" style="margin:15px auto 0 auto;display:block;">
         </div>
    <div class="clearInvisible"></div>



</form>

    </div>
  </div>
      </div>

      <div>
	  <div id="bodyStyleSearch" style="display:none;">
		<div class="cg-dealFinder-searchByBodyStyle-form">



        
		<form id="newBodyStyleSearchHeaderForm" name="newBodyStyleSearchHeaderForm" onsubmit="return processSearchWithBlock(this, '', function() {return validateInventorySearchForm($('#newBodyStyleSearchHeaderForm')[0], 'bodystyle');});" action="#" method="GET">

		<input type="hidden" name="sourceContext" value="HomePageModel" id="newBodyStyleSearchHeaderForm_sourceContext">

		<input type="hidden" name="newSearchFromOverviewPage" value="true" id="newBodyStyleSearchHeaderForm_newSearchFromOverviewPage">

		<input type="hidden" name="inventorySearchWidgetType" value="BODYSTYLE" id="newBodyStyleSearchHeaderForm_inventorySearchWidgetType">


		<div style="clear:both;">
		   <div><label for="bodyTypeGroup">Body Style<span title="required" class="required">*</span></label></div>

          <div>

		<select name="bodyTypeGroup" id="bodyTypeGroup" class="select form-control">
			<option value="">Select Body Style</option>
			<option value="bg6">Sedan</option>
			<option value="bg7">SUV / Crossover</option>
			<option value="bg3">Hatchback</option>
			<option value="bg1">Convertible</option>
			<option value="bg8">Van</option>
			<option value="bg4">Minivan</option>
			<option value="bg5">Pickup Truck</option>
			<option value="bg0">Coupe</option>
			<option value="bg9">Wagon</option>


		</select>

		</div>
			</div>
			<table style="width:100%;">
			  <tbody><tr>
				<td>
				  <label>ZIP<span title="Required" class="required">*</span></label>
				  <div>

	<input type="text" name="zip" maxlength="7" value="75850" id="newBodyStyleSearchHeaderForm_zip" class="cg-postalCode form-control" style="margin-top: 1px;"></div>
				</td>
				<td class="radiusSection">
				  <label>Radius</label>
				  <div>

	<select name="distance" id="newBodyStyleSearchHeaderForm_distance" class="select form-control">
		<option value="10">10 mi</option>
		<option value="25">25 mi</option>
		<option value="50">50 mi</option>
		<option value="75">75 mi</option>
		<option value="100" selected="selected">100 mi</option>
		<option value="150">150 mi</option>
		<option value="200">200 mi</option>


	</select>

	</div>
				</td>
          </tr>
        </tbody></table>
        <div>
        <div><label for="startYear">Years</label></div>
        <select name="startYear" class="select form-control select-start-year">
          <option value="" class="firstSelectOption">All</option>
            <option value="2019" class="selectOption">2019</option>
            <option value="2018" class="selectOption">2018</option>
            <option value="2017" class="selectOption">2017</option>
            <option value="2016" class="selectOption">2016</option>
            <option value="2015" class="selectOption">2015</option>
            <option value="2014" class="selectOption">2014</option>
            <option value="2013" class="selectOption">2013</option>
            <option value="2012" class="selectOption">2012</option>
            <option value="2011" class="selectOption">2011</option>
            <option value="2010" class="selectOption">2010</option>
            <option value="2009" class="selectOption">2009</option>
            <option value="2008" class="selectOption">2008</option>
            <option value="2007" class="selectOption">2007</option>
            <option value="2006" class="selectOption">2006</option>
            <option value="2005" class="selectOption">2005</option>
            <option value="2004" class="selectOption">2004</option>
            <option value="2003" class="selectOption">2003</option>
            <option value="2002" class="selectOption">2002</option>
            <option value="2001" class="selectOption">2001</option>
            <option value="2000" class="selectOption">2000</option>
            <option value="1999" class="selectOption">1999</option>
            <option value="1998" class="selectOption">1998</option>
            <option value="1997" class="selectOption">1997</option>
            <option value="1996" class="selectOption">1996</option>
            <option value="1995" class="selectOption">1995</option>
            <option value="1994" class="selectOption">1994</option>
            <option value="1993" class="selectOption">1993</option>
            <option value="1992" class="selectOption">1992</option>
            <option value="1991" class="selectOption">1991</option>
            <option value="1990" class="selectOption">1990</option>
            <option value="1989" class="selectOption">1989</option>
            <option value="1988" class="selectOption">1988</option>
            <option value="1987" class="selectOption">1987</option>
            <option value="1986" class="selectOption">1986</option>
            <option value="1985" class="selectOption">1985</option>
            <option value="1984" class="selectOption">1984</option>
            <option value="1983" class="selectOption">1983</option>
            <option value="1982" class="selectOption">1982</option>
            <option value="1981" class="selectOption">1981</option>
            <option value="1980" class="selectOption">1980</option>
            <option value="1979" class="selectOption">1979</option>
            <option value="1978" class="selectOption">1978</option>
            <option value="1977" class="selectOption">1977</option>
            <option value="1976" class="selectOption">1976</option>
            <option value="1975" class="selectOption">1975</option>
            <option value="1974" class="selectOption">1974</option>
            <option value="1973" class="selectOption">1973</option>
            <option value="1972" class="selectOption">1972</option>
            <option value="1971" class="selectOption">1971</option>
            <option value="1970" class="selectOption">1970</option>
            <option value="1969" class="selectOption">1969</option>
            <option value="1968" class="selectOption">1968</option>
            <option value="1967" class="selectOption">1967</option>
            <option value="1966" class="selectOption">1966</option>
            <option value="1965" class="selectOption">1965</option>
            <option value="1964" class="selectOption">1964</option>
            <option value="1963" class="selectOption">1963</option>
            <option value="1962" class="selectOption">1962</option>
            <option value="1961" class="selectOption">1961</option>
            <option value="1960" class="selectOption">1960</option>
            <option value="1959" class="selectOption">1959</option>
            <option value="1958" class="selectOption">1958</option>
            <option value="1957" class="selectOption">1957</option>
            <option value="1956" class="selectOption">1956</option>
            <option value="1955" class="selectOption">1955</option>
            <option value="1954" class="selectOption">1954</option>
            <option value="1953" class="selectOption">1953</option>
            <option value="1952" class="selectOption">1952</option>
            <option value="1951" class="selectOption">1951</option>
            <option value="1950" class="selectOption">1950</option>
            <option value="1949" class="selectOption">1949</option>
            <option value="1948" class="selectOption">1948</option>
            <option value="1947" class="selectOption">1947</option>
            <option value="1946" class="selectOption">1946</option>
            <option value="1945" class="selectOption">1945</option>
            <option value="1944" class="selectOption">1944</option>
            <option value="1943" class="selectOption">1943</option>
            <option value="1942" class="selectOption">1942</option>
            <option value="1941" class="selectOption">1941</option>
            <option value="1940" class="selectOption">1940</option>
            <option value="1939" class="selectOption">1939</option>
            <option value="1938" class="selectOption">1938</option>
            <option value="1937" class="selectOption">1937</option>
            <option value="1936" class="selectOption">1936</option>
            <option value="1935" class="selectOption">1935</option>
            <option value="1934" class="selectOption">1934</option>
            <option value="1933" class="selectOption">1933</option>
            <option value="1932" class="selectOption">1932</option>
            <option value="1931" class="selectOption">1931</option>
            <option value="1930" class="selectOption">1930</option>
            <option value="1929" class="selectOption">1929</option>
            <option value="1928" class="selectOption">1928</option>
            <option value="1927" class="selectOption">1927</option>
            <option value="1926" class="selectOption">1926</option>
            <option value="1925" class="selectOption">1925</option>
            <option value="1924" class="selectOption">1924</option>
            <option value="1923" class="selectOption">1923</option>
            <option value="1922" class="selectOption">1922</option>
            <option value="1921" class="selectOption">1921</option>
            <option value="1920" class="selectOption">1920</option>
            <option value="1919" class="selectOption">1919</option>
            <option value="1918" class="selectOption">1918</option>
            <option value="1917" class="selectOption">1917</option>
            <option value="1916" class="selectOption">1916</option>
            <option value="1915" class="selectOption">1915</option>
            <option value="1914" class="selectOption">1914</option>
            <option value="1913" class="selectOption">1913</option>
            <option value="1912" class="selectOption">1912</option>
            <option value="1911" class="selectOption">1911</option>
            <option value="1910" class="selectOption">1910</option>
            <option value="1909" class="selectOption">1909</option>
            <option value="1908" class="selectOption">1908</option>
            <option value="1907" class="selectOption">1907</option>
            <option value="1906" class="selectOption">1906</option>
        </select>
        <span class="tdLabel cg-dealFinder-searchByBodyStyle-labelSeparator">to</span>
        <select name="endYear" class="select form-control select-end-year">
          <option value="" class="firstSelectOption">All</option>
            <option value="2019" class="selectOption">2019</option>
            <option value="2018" class="selectOption">2018</option>
            <option value="2017" class="selectOption">2017</option>
            <option value="2016" class="selectOption">2016</option>
            <option value="2015" class="selectOption">2015</option>
            <option value="2014" class="selectOption">2014</option>
            <option value="2013" class="selectOption">2013</option>
            <option value="2012" class="selectOption">2012</option>
            <option value="2011" class="selectOption">2011</option>
            <option value="2010" class="selectOption">2010</option>
            <option value="2009" class="selectOption">2009</option>
            <option value="2008" class="selectOption">2008</option>
            <option value="2007" class="selectOption">2007</option>
            <option value="2006" class="selectOption">2006</option>
            <option value="2005" class="selectOption">2005</option>
            <option value="2004" class="selectOption">2004</option>
            <option value="2003" class="selectOption">2003</option>
            <option value="2002" class="selectOption">2002</option>
            <option value="2001" class="selectOption">2001</option>
            <option value="2000" class="selectOption">2000</option>
            <option value="1999" class="selectOption">1999</option>
            <option value="1998" class="selectOption">1998</option>
            <option value="1997" class="selectOption">1997</option>
            <option value="1996" class="selectOption">1996</option>
            <option value="1995" class="selectOption">1995</option>
            <option value="1994" class="selectOption">1994</option>
            <option value="1993" class="selectOption">1993</option>
            <option value="1992" class="selectOption">1992</option>
            <option value="1991" class="selectOption">1991</option>
            <option value="1990" class="selectOption">1990</option>
            <option value="1989" class="selectOption">1989</option>
            <option value="1988" class="selectOption">1988</option>
            <option value="1987" class="selectOption">1987</option>
            <option value="1986" class="selectOption">1986</option>
            <option value="1985" class="selectOption">1985</option>
            <option value="1984" class="selectOption">1984</option>
            <option value="1983" class="selectOption">1983</option>
            <option value="1982" class="selectOption">1982</option>
            <option value="1981" class="selectOption">1981</option>
            <option value="1980" class="selectOption">1980</option>
            <option value="1979" class="selectOption">1979</option>
            <option value="1978" class="selectOption">1978</option>
            <option value="1977" class="selectOption">1977</option>
            <option value="1976" class="selectOption">1976</option>
            <option value="1975" class="selectOption">1975</option>
            <option value="1974" class="selectOption">1974</option>
            <option value="1973" class="selectOption">1973</option>
            <option value="1972" class="selectOption">1972</option>
            <option value="1971" class="selectOption">1971</option>
            <option value="1970" class="selectOption">1970</option>
            <option value="1969" class="selectOption">1969</option>
            <option value="1968" class="selectOption">1968</option>
            <option value="1967" class="selectOption">1967</option>
            <option value="1966" class="selectOption">1966</option>
            <option value="1965" class="selectOption">1965</option>
            <option value="1964" class="selectOption">1964</option>
            <option value="1963" class="selectOption">1963</option>
            <option value="1962" class="selectOption">1962</option>
            <option value="1961" class="selectOption">1961</option>
            <option value="1960" class="selectOption">1960</option>
            <option value="1959" class="selectOption">1959</option>
            <option value="1958" class="selectOption">1958</option>
            <option value="1957" class="selectOption">1957</option>
            <option value="1956" class="selectOption">1956</option>
            <option value="1955" class="selectOption">1955</option>
            <option value="1954" class="selectOption">1954</option>
            <option value="1953" class="selectOption">1953</option>
            <option value="1952" class="selectOption">1952</option>
            <option value="1951" class="selectOption">1951</option>
            <option value="1950" class="selectOption">1950</option>
            <option value="1949" class="selectOption">1949</option>
            <option value="1948" class="selectOption">1948</option>
            <option value="1947" class="selectOption">1947</option>
            <option value="1946" class="selectOption">1946</option>
            <option value="1945" class="selectOption">1945</option>
            <option value="1944" class="selectOption">1944</option>
            <option value="1943" class="selectOption">1943</option>
            <option value="1942" class="selectOption">1942</option>
            <option value="1941" class="selectOption">1941</option>
            <option value="1940" class="selectOption">1940</option>
            <option value="1939" class="selectOption">1939</option>
            <option value="1938" class="selectOption">1938</option>
            <option value="1937" class="selectOption">1937</option>
            <option value="1936" class="selectOption">1936</option>
            <option value="1935" class="selectOption">1935</option>
            <option value="1934" class="selectOption">1934</option>
            <option value="1933" class="selectOption">1933</option>
            <option value="1932" class="selectOption">1932</option>
            <option value="1931" class="selectOption">1931</option>
            <option value="1930" class="selectOption">1930</option>
            <option value="1929" class="selectOption">1929</option>
            <option value="1928" class="selectOption">1928</option>
            <option value="1927" class="selectOption">1927</option>
            <option value="1926" class="selectOption">1926</option>
            <option value="1925" class="selectOption">1925</option>
            <option value="1924" class="selectOption">1924</option>
            <option value="1923" class="selectOption">1923</option>
            <option value="1922" class="selectOption">1922</option>
            <option value="1921" class="selectOption">1921</option>
            <option value="1920" class="selectOption">1920</option>
            <option value="1919" class="selectOption">1919</option>
            <option value="1918" class="selectOption">1918</option>
            <option value="1917" class="selectOption">1917</option>
            <option value="1916" class="selectOption">1916</option>
            <option value="1915" class="selectOption">1915</option>
            <option value="1914" class="selectOption">1914</option>
            <option value="1913" class="selectOption">1913</option>
            <option value="1912" class="selectOption">1912</option>
            <option value="1911" class="selectOption">1911</option>
            <option value="1910" class="selectOption">1910</option>
            <option value="1909" class="selectOption">1909</option>
            <option value="1908" class="selectOption">1908</option>
            <option value="1907" class="selectOption">1907</option>
            <option value="1906" class="selectOption">1906</option>
        </select>
      </div>
        <div><label for="minPrice">Price</label></div>
        <div class="cg-dealFinder-searchByBodyStyle-valueRangeSection">
          <select name="minPrice" onchange="CG.ListingsView.adjustMaxPriceSelector(this.value, false)" class="minPriceSelector form-control select-min-price">
                <option value="0">---</option>
        <option value="1000">$1,000</option>
      <option value="2000">$2,000</option>
      <option value="3000">$3,000</option>
      <option value="4000">$4,000</option>
      <option value="5000">$5,000</option>
      <option value="6000">$6,000</option>
      <option value="7000">$7,000</option>
      <option value="8000">$8,000</option>
      <option value="9000">$9,000</option>
      <option value="10000">$10,000</option>
      <option value="12000">$12,000</option>
      <option value="14000">$14,000</option>
      <option value="16000">$16,000</option>
      <option value="18000">$18,000</option>
      <option value="20000">$20,000</option>
      <option value="22000">$22,000</option>
      <option value="24000">$24,000</option>
      <option value="26000">$26,000</option>
      <option value="28000">$28,000</option>
      <option value="30000">$30,000</option>
      <option value="35000">$35,000</option>
      <option value="40000">$40,000</option>
      <option value="45000">$45,000</option>
      <option value="50000">$50,000</option>
      <option value="55000">$55,000</option>
      <option value="60000">$60,000</option>
      <option value="65000">$65,000</option>
      <option value="70000">$70,000</option>
      <option value="75000">$75,000</option>
      <option value="80000">$80,000</option>
      <option value="85000">$85,000</option>
      <option value="90000">$90,000</option>
      <option value="95000">$95,000</option>
      <option value="100000">$100,000</option>
      <option value="110000">$110,000</option>
      <option value="120000">$120,000</option>
      <option value="130000">$130,000</option>
      <option value="140000">$140,000</option>
      <option value="150000">$150,000</option>
      <option value="160000">$160,000</option>
      <option value="170000">$170,000</option>
      <option value="180000">$180,000</option>
      <option value="190000">$190,000</option>
      <option value="200000">$200,000</option>


          </select>
          <span class="cg-dealFinder-searchByBodyStyle-labelSeparator">to</span>
          <select name="maxPrice" onchange="CG.ListingsView.adjustMinPriceSelector(this.value, false)" class="maxPriceSelector form-control select-max-price">
                    <option value="1000">$1,000</option>
      <option value="2000">$2,000</option>
      <option value="3000">$3,000</option>
      <option value="4000">$4,000</option>
      <option value="5000">$5,000</option>
      <option value="6000">$6,000</option>
      <option value="7000">$7,000</option>
      <option value="8000">$8,000</option>
      <option value="9000">$9,000</option>
      <option value="10000">$10,000</option>
      <option value="12000">$12,000</option>
      <option value="14000">$14,000</option>
      <option value="16000">$16,000</option>
      <option value="18000">$18,000</option>
      <option value="20000">$20,000</option>
      <option value="22000">$22,000</option>
      <option value="24000">$24,000</option>
      <option value="26000">$26,000</option>
      <option value="28000">$28,000</option>
      <option value="30000">$30,000</option>
      <option value="35000">$35,000</option>
      <option value="40000">$40,000</option>
      <option value="45000">$45,000</option>
      <option value="50000">$50,000</option>
      <option value="55000">$55,000</option>
      <option value="60000">$60,000</option>
      <option value="65000">$65,000</option>
      <option value="70000">$70,000</option>
      <option value="75000">$75,000</option>
      <option value="80000">$80,000</option>
      <option value="85000">$85,000</option>
      <option value="90000">$90,000</option>
      <option value="95000">$95,000</option>
      <option value="100000">$100,000</option>
      <option value="110000">$110,000</option>
      <option value="120000">$120,000</option>
      <option value="130000">$130,000</option>
      <option value="140000">$140,000</option>
      <option value="150000">$150,000</option>
      <option value="160000">$160,000</option>
      <option value="170000">$170,000</option>
      <option value="180000">$180,000</option>
      <option value="190000">$190,000</option>
      <option value="200000">$200,000</option>

    <option value="">---</option>

          </select>
        </div>
        <div><label for="">Maximum Mileage</label></div>
        <div class="cg-dealFinder-searchByBodyStyle-valueRangeSection">
  <select name="maxMileage" class="form-control select-max-mileage">
                        <option value="10000">10,000</option>
          <option value="20000">20,000</option>
          <option value="30000">30,000</option>
          <option value="40000">40,000</option>
          <option value="50000">50,000</option>
          <option value="60000">60,000</option>
          <option value="70000">70,000</option>
          <option value="80000">80,000</option>
          <option value="90000">90,000</option>
          <option value="100000">100,000</option>
          <option value="110000">110,000</option>
          <option value="120000">120,000</option>
          <option value="130000">130,000</option>
          <option value="140000">140,000</option>
          <option value="150000">150,000</option>
          <option value="160000">160,000</option>
          <option value="170000">170,000</option>
          <option value="180000">180,000</option>
          <option value="190000">190,000</option>
          <option value="200000">200,000</option>
          <option value="210000">210,000</option>
          <option value="220000">220,000</option>
          <option value="230000">230,000</option>
          <option value="240000">240,000</option>
          <option value="250000">250,000</option>
      <option value="" selected="">---</option>

            </select>
            <span class="cg-dealFinder-searchByBodyStyle-labelSeparator">miles</span>
        </div>
        <div><label>Transmission</label></div>

        <div class="cg-dealFinder-searchByBodyStyle-transmissionSection">


<div class="form-group  ">    <div class="  controls">
    <label for="transmission-1" class="radio-inline">
        <input type="radio" name="transmission" id="transmission-1" value="ANY">Any    </label>
    <label for="transmission-2" class="radio-inline">
        <input type="radio" name="transmission" id="transmission-2" value="A">Automatic    </label>
    <label for="transmission-3" class="radio-inline">
        <input type="radio" name="transmission" id="transmission-3" value="M">Manual    </label>
</div>
</div>

        </div>
      <div>
        <input type="submit" value="Search" class="btn btn-warning newSearchSubmitButton ft-submit" style="margin:15px auto 0 auto;display:block;">
      </div>
    <div class="clearInvisible"></div>



</form>

    </div>
  </div>
      </div>

        <div>
  <div id="priceSearch" style="display:none;">
    <div class="cg-dealFinder-searchByBodyStyle-form">



        
<form id="newPriceSearchHeaderForm" name="newPriceSearchHeaderForm" onsubmit="return processSearchWithBlock(this, '', function() {return validateInventorySearchForm($('#newPriceSearchHeaderForm')[0], 'price');});" action="/Cars/inventorylisting/viewDetailsFilterViewInventoryListing.action" method="GET">

<input type="hidden" name="sourceContext" value="HomePageModel" id="newPriceSearchHeaderForm_sourceContext">

<input type="hidden" name="newSearchFromOverviewPage" value="true" id="newPriceSearchHeaderForm_newSearchFromOverviewPage">

<input type="hidden" name="inventorySearchWidgetType" value="PRICE" id="newPriceSearchHeaderForm_inventorySearchWidgetType">


        <table style="width:100%;">
          <tbody><tr>
            <td>
              <label>ZIP<span title="Required" class="required">*</span></label>
              <div>

<input type="text" name="zip" maxlength="7" value="75850" id="newPriceSearchHeaderForm_zip" class="cg-postalCode form-control" style="margin-top: 1px;"></div>
            </td>
            <td class="radiusSection">
              <label>Radius</label>
              <div>


<select name="distance" id="newPriceSearchHeaderForm_distance" class="select form-control">
    <option value="10">10 mi</option>
    <option value="25">25 mi</option>
    <option value="50">50 mi</option>
    <option value="75">75 mi</option>
    <option value="100" selected="selected">100 mi</option>


</select>

              </div>
            </td>
          </tr>
        </tbody></table>
        <div>
          <div><label for="startYear">Years</label></div>
          <select name="startYear" class="select form-control select-start-year">
            <option value="" class="firstSelectOption">All</option>
              <option value="2019" class="selectOption">2019</option>
              <option value="2018" class="selectOption">2018</option>
              <option value="2017" class="selectOption">2017</option>
              <option value="2016" class="selectOption">2016</option>
              <option value="2015" class="selectOption">2015</option>
              <option value="2014" class="selectOption">2014</option>
              <option value="2013" class="selectOption">2013</option>
              <option value="2012" class="selectOption">2012</option>
              <option value="2011" class="selectOption">2011</option>
              <option value="2010" class="selectOption">2010</option>
              <option value="2009" class="selectOption">2009</option>
              <option value="2008" class="selectOption">2008</option>
              <option value="2007" class="selectOption">2007</option>
              <option value="2006" class="selectOption">2006</option>
              <option value="2005" class="selectOption">2005</option>
              <option value="2004" class="selectOption">2004</option>
              <option value="2003" class="selectOption">2003</option>
              <option value="2002" class="selectOption">2002</option>
              <option value="2001" class="selectOption">2001</option>
              <option value="2000" class="selectOption">2000</option>
              <option value="1999" class="selectOption">1999</option>
              <option value="1998" class="selectOption">1998</option>
              <option value="1997" class="selectOption">1997</option>
              <option value="1996" class="selectOption">1996</option>
              <option value="1995" class="selectOption">1995</option>
              <option value="1994" class="selectOption">1994</option>
              <option value="1993" class="selectOption">1993</option>
              <option value="1992" class="selectOption">1992</option>
              <option value="1991" class="selectOption">1991</option>
              <option value="1990" class="selectOption">1990</option>
              <option value="1989" class="selectOption">1989</option>
              <option value="1988" class="selectOption">1988</option>
              <option value="1987" class="selectOption">1987</option>
              <option value="1986" class="selectOption">1986</option>
              <option value="1985" class="selectOption">1985</option>
              <option value="1984" class="selectOption">1984</option>
              <option value="1983" class="selectOption">1983</option>
              <option value="1982" class="selectOption">1982</option>
              <option value="1981" class="selectOption">1981</option>
              <option value="1980" class="selectOption">1980</option>
              <option value="1979" class="selectOption">1979</option>
              <option value="1978" class="selectOption">1978</option>
              <option value="1977" class="selectOption">1977</option>
              <option value="1976" class="selectOption">1976</option>
              <option value="1975" class="selectOption">1975</option>
              <option value="1974" class="selectOption">1974</option>
              <option value="1973" class="selectOption">1973</option>
              <option value="1972" class="selectOption">1972</option>
              <option value="1971" class="selectOption">1971</option>
              <option value="1970" class="selectOption">1970</option>
              <option value="1969" class="selectOption">1969</option>
              <option value="1968" class="selectOption">1968</option>
              <option value="1967" class="selectOption">1967</option>
              <option value="1966" class="selectOption">1966</option>
              <option value="1965" class="selectOption">1965</option>
              <option value="1964" class="selectOption">1964</option>
              <option value="1963" class="selectOption">1963</option>
              <option value="1962" class="selectOption">1962</option>
              <option value="1961" class="selectOption">1961</option>
              <option value="1960" class="selectOption">1960</option>
              <option value="1959" class="selectOption">1959</option>
              <option value="1958" class="selectOption">1958</option>
              <option value="1957" class="selectOption">1957</option>
              <option value="1956" class="selectOption">1956</option>
              <option value="1955" class="selectOption">1955</option>
              <option value="1954" class="selectOption">1954</option>
              <option value="1953" class="selectOption">1953</option>
              <option value="1952" class="selectOption">1952</option>
              <option value="1951" class="selectOption">1951</option>
              <option value="1950" class="selectOption">1950</option>
              <option value="1949" class="selectOption">1949</option>
              <option value="1948" class="selectOption">1948</option>
              <option value="1947" class="selectOption">1947</option>
              <option value="1946" class="selectOption">1946</option>
              <option value="1945" class="selectOption">1945</option>
              <option value="1944" class="selectOption">1944</option>
              <option value="1943" class="selectOption">1943</option>
              <option value="1942" class="selectOption">1942</option>
              <option value="1941" class="selectOption">1941</option>
              <option value="1940" class="selectOption">1940</option>
              <option value="1939" class="selectOption">1939</option>
              <option value="1938" class="selectOption">1938</option>
              <option value="1937" class="selectOption">1937</option>
              <option value="1936" class="selectOption">1936</option>
              <option value="1935" class="selectOption">1935</option>
              <option value="1934" class="selectOption">1934</option>
              <option value="1933" class="selectOption">1933</option>
              <option value="1932" class="selectOption">1932</option>
              <option value="1931" class="selectOption">1931</option>
              <option value="1930" class="selectOption">1930</option>
              <option value="1929" class="selectOption">1929</option>
              <option value="1928" class="selectOption">1928</option>
              <option value="1927" class="selectOption">1927</option>
              <option value="1926" class="selectOption">1926</option>
              <option value="1925" class="selectOption">1925</option>
              <option value="1924" class="selectOption">1924</option>
              <option value="1923" class="selectOption">1923</option>
              <option value="1922" class="selectOption">1922</option>
              <option value="1921" class="selectOption">1921</option>
              <option value="1920" class="selectOption">1920</option>
              <option value="1919" class="selectOption">1919</option>
              <option value="1918" class="selectOption">1918</option>
              <option value="1917" class="selectOption">1917</option>
              <option value="1916" class="selectOption">1916</option>
              <option value="1915" class="selectOption">1915</option>
              <option value="1914" class="selectOption">1914</option>
              <option value="1913" class="selectOption">1913</option>
              <option value="1912" class="selectOption">1912</option>
              <option value="1911" class="selectOption">1911</option>
              <option value="1910" class="selectOption">1910</option>
              <option value="1909" class="selectOption">1909</option>
              <option value="1908" class="selectOption">1908</option>
              <option value="1907" class="selectOption">1907</option>
              <option value="1906" class="selectOption">1906</option>
          </select>
          <span class="tdLabel cg-dealFinder-searchByBodyStyle-labelSeparator">to</span>
          <select name="endYear" class="select form-control select-end-year">
            <option value="" class="firstSelectOption">All Years</option>
              <option value="2019" class="selectOption">2019</option>
              <option value="2018" class="selectOption">2018</option>
              <option value="2017" class="selectOption">2017</option>
              <option value="2016" class="selectOption">2016</option>
              <option value="2015" class="selectOption">2015</option>
              <option value="2014" class="selectOption">2014</option>
              <option value="2013" class="selectOption">2013</option>
              <option value="2012" class="selectOption">2012</option>
              <option value="2011" class="selectOption">2011</option>
              <option value="2010" class="selectOption">2010</option>
              <option value="2009" class="selectOption">2009</option>
              <option value="2008" class="selectOption">2008</option>
              <option value="2007" class="selectOption">2007</option>
              <option value="2006" class="selectOption">2006</option>
              <option value="2005" class="selectOption">2005</option>
              <option value="2004" class="selectOption">2004</option>
              <option value="2003" class="selectOption">2003</option>
              <option value="2002" class="selectOption">2002</option>
              <option value="2001" class="selectOption">2001</option>
              <option value="2000" class="selectOption">2000</option>
              <option value="1999" class="selectOption">1999</option>
              <option value="1998" class="selectOption">1998</option>
              <option value="1997" class="selectOption">1997</option>
              <option value="1996" class="selectOption">1996</option>
              <option value="1995" class="selectOption">1995</option>
              <option value="1994" class="selectOption">1994</option>
              <option value="1993" class="selectOption">1993</option>
              <option value="1992" class="selectOption">1992</option>
              <option value="1991" class="selectOption">1991</option>
              <option value="1990" class="selectOption">1990</option>
              <option value="1989" class="selectOption">1989</option>
              <option value="1988" class="selectOption">1988</option>
              <option value="1987" class="selectOption">1987</option>
              <option value="1986" class="selectOption">1986</option>
              <option value="1985" class="selectOption">1985</option>
              <option value="1984" class="selectOption">1984</option>
              <option value="1983" class="selectOption">1983</option>
              <option value="1982" class="selectOption">1982</option>
              <option value="1981" class="selectOption">1981</option>
              <option value="1980" class="selectOption">1980</option>
              <option value="1979" class="selectOption">1979</option>
              <option value="1978" class="selectOption">1978</option>
              <option value="1977" class="selectOption">1977</option>
              <option value="1976" class="selectOption">1976</option>
              <option value="1975" class="selectOption">1975</option>
              <option value="1974" class="selectOption">1974</option>
              <option value="1973" class="selectOption">1973</option>
              <option value="1972" class="selectOption">1972</option>
              <option value="1971" class="selectOption">1971</option>
              <option value="1970" class="selectOption">1970</option>
              <option value="1969" class="selectOption">1969</option>
              <option value="1968" class="selectOption">1968</option>
              <option value="1967" class="selectOption">1967</option>
              <option value="1966" class="selectOption">1966</option>
              <option value="1965" class="selectOption">1965</option>
              <option value="1964" class="selectOption">1964</option>
              <option value="1963" class="selectOption">1963</option>
              <option value="1962" class="selectOption">1962</option>
              <option value="1961" class="selectOption">1961</option>
              <option value="1960" class="selectOption">1960</option>
              <option value="1959" class="selectOption">1959</option>
              <option value="1958" class="selectOption">1958</option>
              <option value="1957" class="selectOption">1957</option>
              <option value="1956" class="selectOption">1956</option>
              <option value="1955" class="selectOption">1955</option>
              <option value="1954" class="selectOption">1954</option>
              <option value="1953" class="selectOption">1953</option>
              <option value="1952" class="selectOption">1952</option>
              <option value="1951" class="selectOption">1951</option>
              <option value="1950" class="selectOption">1950</option>
              <option value="1949" class="selectOption">1949</option>
              <option value="1948" class="selectOption">1948</option>
              <option value="1947" class="selectOption">1947</option>
              <option value="1946" class="selectOption">1946</option>
              <option value="1945" class="selectOption">1945</option>
              <option value="1944" class="selectOption">1944</option>
              <option value="1943" class="selectOption">1943</option>
              <option value="1942" class="selectOption">1942</option>
              <option value="1941" class="selectOption">1941</option>
              <option value="1940" class="selectOption">1940</option>
              <option value="1939" class="selectOption">1939</option>
              <option value="1938" class="selectOption">1938</option>
              <option value="1937" class="selectOption">1937</option>
              <option value="1936" class="selectOption">1936</option>
              <option value="1935" class="selectOption">1935</option>
              <option value="1934" class="selectOption">1934</option>
              <option value="1933" class="selectOption">1933</option>
              <option value="1932" class="selectOption">1932</option>
              <option value="1931" class="selectOption">1931</option>
              <option value="1930" class="selectOption">1930</option>
              <option value="1929" class="selectOption">1929</option>
              <option value="1928" class="selectOption">1928</option>
              <option value="1927" class="selectOption">1927</option>
              <option value="1926" class="selectOption">1926</option>
              <option value="1925" class="selectOption">1925</option>
              <option value="1924" class="selectOption">1924</option>
              <option value="1923" class="selectOption">1923</option>
              <option value="1922" class="selectOption">1922</option>
              <option value="1921" class="selectOption">1921</option>
              <option value="1920" class="selectOption">1920</option>
              <option value="1919" class="selectOption">1919</option>
              <option value="1918" class="selectOption">1918</option>
              <option value="1917" class="selectOption">1917</option>
              <option value="1916" class="selectOption">1916</option>
              <option value="1915" class="selectOption">1915</option>
              <option value="1914" class="selectOption">1914</option>
              <option value="1913" class="selectOption">1913</option>
              <option value="1912" class="selectOption">1912</option>
              <option value="1911" class="selectOption">1911</option>
              <option value="1910" class="selectOption">1910</option>
              <option value="1909" class="selectOption">1909</option>
              <option value="1908" class="selectOption">1908</option>
              <option value="1907" class="selectOption">1907</option>
              <option value="1906" class="selectOption">1906</option>
          </select>
        </div>
        <div><label for="minPrice">Price</label></div>
        <div class="cg-dealFinder-searchByBodyStyle-valueRangeSection">
          <select name="minPrice" onchange="CG.ListingsView.adjustMaxPriceSelector(this.value, false)" class="minPriceSelector form-control select-min-price">
                <option value="0">---</option>
        <option value="1000">$1,000</option>
      <option value="2000">$2,000</option>
      <option value="3000">$3,000</option>
      <option value="4000">$4,000</option>
      <option value="5000">$5,000</option>
      <option value="6000">$6,000</option>
      <option value="7000">$7,000</option>
      <option value="8000">$8,000</option>
      <option value="9000">$9,000</option>
      <option value="10000">$10,000</option>
      <option value="12000">$12,000</option>
      <option value="14000">$14,000</option>
      <option value="16000">$16,000</option>
      <option value="18000">$18,000</option>
      <option value="20000">$20,000</option>
      <option value="22000">$22,000</option>
      <option value="24000">$24,000</option>
      <option value="26000">$26,000</option>
      <option value="28000">$28,000</option>
      <option value="30000">$30,000</option>
      <option value="35000">$35,000</option>
      <option value="40000">$40,000</option>
      <option value="45000">$45,000</option>
      <option value="50000">$50,000</option>
      <option value="55000">$55,000</option>
      <option value="60000">$60,000</option>
      <option value="65000">$65,000</option>
      <option value="70000">$70,000</option>
      <option value="75000">$75,000</option>
      <option value="80000">$80,000</option>
      <option value="85000">$85,000</option>
      <option value="90000">$90,000</option>
      <option value="95000">$95,000</option>
      <option value="100000">$100,000</option>
      <option value="110000">$110,000</option>
      <option value="120000">$120,000</option>
      <option value="130000">$130,000</option>
      <option value="140000">$140,000</option>
      <option value="150000">$150,000</option>
      <option value="160000">$160,000</option>
      <option value="170000">$170,000</option>
      <option value="180000">$180,000</option>
      <option value="190000">$190,000</option>
      <option value="200000">$200,000</option>


          </select>
          <span class="cg-dealFinder-searchByBodyStyle-labelSeparator">to</span>
          <select name="maxPrice" onchange="CG.ListingsView.adjustMinPriceSelector(this.value, false)" class="maxPriceSelector form-control select-min-price">
                    <option value="1000">$1,000</option>
      <option value="2000">$2,000</option>
      <option value="3000">$3,000</option>
      <option value="4000">$4,000</option>
      <option value="5000">$5,000</option>
      <option value="6000">$6,000</option>
      <option value="7000">$7,000</option>
      <option value="8000">$8,000</option>
      <option value="9000">$9,000</option>
      <option value="10000">$10,000</option>
      <option value="12000">$12,000</option>
      <option value="14000">$14,000</option>
      <option value="16000">$16,000</option>
      <option value="18000">$18,000</option>
      <option value="20000">$20,000</option>
      <option value="22000">$22,000</option>
      <option value="24000">$24,000</option>
      <option value="26000">$26,000</option>
      <option value="28000">$28,000</option>
      <option value="30000">$30,000</option>
      <option value="35000">$35,000</option>
      <option value="40000">$40,000</option>
      <option value="45000">$45,000</option>
      <option value="50000">$50,000</option>
      <option value="55000">$55,000</option>
      <option value="60000">$60,000</option>
      <option value="65000">$65,000</option>
      <option value="70000">$70,000</option>
      <option value="75000">$75,000</option>
      <option value="80000">$80,000</option>
      <option value="85000">$85,000</option>
      <option value="90000">$90,000</option>
      <option value="95000">$95,000</option>
      <option value="100000">$100,000</option>
      <option value="110000">$110,000</option>
      <option value="120000">$120,000</option>
      <option value="130000">$130,000</option>
      <option value="140000">$140,000</option>
      <option value="150000">$150,000</option>
      <option value="160000">$160,000</option>
      <option value="170000">$170,000</option>
      <option value="180000">$180,000</option>
      <option value="190000">$190,000</option>
      <option value="200000">$200,000</option>

    <option value="">---</option>

          </select>
        </div>
        <div><label for="">Maximum Mileage</label></div>
        <div class="cg-dealFinder-searchByBodyStyle-valueRangeSection">
          <select name="maxMileage" class="form-control select-max-mileage">
                      <option value="10000">10,000</option>
          <option value="20000">20,000</option>
          <option value="30000">30,000</option>
          <option value="40000">40,000</option>
          <option value="50000">50,000</option>
          <option value="60000">60,000</option>
          <option value="70000">70,000</option>
          <option value="80000">80,000</option>
          <option value="90000">90,000</option>
          <option value="100000">100,000</option>
          <option value="110000">110,000</option>
          <option value="120000">120,000</option>
          <option value="130000">130,000</option>
          <option value="140000">140,000</option>
          <option value="150000">150,000</option>
          <option value="160000">160,000</option>
          <option value="170000">170,000</option>
          <option value="180000">180,000</option>
          <option value="190000">190,000</option>
          <option value="200000">200,000</option>
          <option value="210000">210,000</option>
          <option value="220000">220,000</option>
          <option value="230000">230,000</option>
          <option value="240000">240,000</option>
          <option value="250000">250,000</option>
      <option value="" selected="">---</option>

          </select>
          <span class="cg-dealFinder-searchByBodyStyle-labelSeparator">miles</span>
        </div>
        <div><label>Transmission</label></div>
        <div class="cg-dealFinder-searchByBodyStyle-transmissionSection">


<div class="form-group  ">    <div class="  controls">
    <label for="transmission-1" class="radio-inline">
        <input type="radio" name="transmission" id="transmission-1" value="ANY">Any    </label>
    <label for="transmission-2" class="radio-inline">
        <input type="radio" name="transmission" id="transmission-2" value="A">Automatic    </label>
    <label for="transmission-3" class="radio-inline">
        <input type="radio" name="transmission" id="transmission-3" value="M">Manual    </label>
</div>
</div>

        </div>
        <div>
          <input type="submit" value="Search" class="btn btn-warning newSearchSubmitButton ft-submit" style="margin:15px auto 0 auto;display:block;">
        </div>
    <div class="clearInvisible"></div>



</form>

    </div>
  </div>
        </div>

      <script>
            $(document).ready(function() {
                if (typeof CG === 'object' && CG !== null && CG.hasOwnProperty('Inventory')) {
                  var form = $("#newSearchHeaderForm_UsedCar")[0];
                  CG.Inventory.adjustSearchRadius(form,  100 , 500);
                  form = $("#newPriceSearchHeaderForm")[0];
                  CG.Inventory.adjustSearchRadius(form, 100 , 500);

                  CG.ListingsView.adjustMaxPriceSelector(0, true);
                  CG.ListingsView.adjustMinPriceSelector("", true);
                }
            });
      </script>
    </div>

    <div id="listingSearchQuotableSearchFormDiv" class="cg-dealFinder-searchSectionsWrap ft-listing-results__new-car-form" style="display: none;">
      <div>
  <div>

    <div class="cg-dealFinder-searchByCar-form">




        
<form id="newSearchHeaderForm_NewCar" name="newSearchHeaderForm_NewCar" onsubmit="return processSearchWithBlock(this, 'listingsViewMacrosNew_NewCar', function() {return validateInventorySearchForm($('#newSearchHeaderForm_NewCar')[0], 'car');});" action="/Cars/new/searchresults.action" method="GET">


<input type="hidden" name="sourceContext" value="HomePageModel" id="newSearchHeaderForm_NewCar_sourceContext">

<input type="hidden" name="newSearchFromOverviewPage" value="true" id="newSearchHeaderForm_NewCar_newSearchFromOverviewPage">

<input type="hidden" name="inventorySearchWidgetType" value="AUTO" id="newSearchHeaderForm_NewCar_inventorySearchWidgetType">



<div id="listingsViewMacrosNew_NewCar">
<script>
  if (typeof CG === 'object' && CG !== null && CG.hasOwnProperty('CarPicker')) {
    CG.CarPicker.createInstance(
      "listingsViewMacrosNew_NewCar",
      "entitySelectingHelper.selectedEntity",
      "entitySelectingHelper.selectedEntity2",
      "",
      "CG.Inventory.adjustSearchRadius(this.form, 100, 500);",
      "CG.Inventory.adjustSearchRadius(this.form, 100, 500);",
      "CG.Inventory.adjustSearchRadius(this.form, 100, 500);",
      "",
      "CG.Inventory.adjustSearchRadius(this.form), 100, 500);",
      "en",
    false ,
    true).initialize();
  } else if (typeof CG_FTL_LOGGER === 'object' && CG_FTL_LOGGER !== null) {
    CG_FTL_LOGGER.error('Error creating CarPicker in ftl: inventorylistings/headerSelectCar.ftl');
  }
</script>
</div>




<input type="hidden" name="entitySelectingHelper.selectedEntity" value="" id="listingsViewMacrosNew_NewCarentitySelectingHelper.selectedEntity">


<input type="hidden" name="entitySelectingHelper.selectedEntity2" value="" id="newSearchHeaderForm_NewCar_entitySelectingHelper_selectedEntity2">
<div>

    <select class="select maker-select-dropdown btn-small form-control ft-make-selector" style="width: 260px;" cg-data-showinactive="false" cg-data-newcarsonly="true" cg-data-useinventoryservice="false" cg-data-name="entitySelectingHelper.selectedEntity" cg-data-quotableonly="true" cg-data-includeallmodelsoption="false" cg-data-carswithregressiononly="false" cg-data-groupbypopularity="true" cg-name="ign-makerId-entitySelectingHelper.selectedEntity" id="select-20754111">
        <option value="-1">All Makes</option>


    	  <optgroup label="Popular Makes">
      
<option value="m4">Acura</option> 
<option value="m124">Alfa Romeo</option> 
<option value="m19">Audi</option> 
<option value="m3">BMW</option> 
<option value="m21">Buick</option> 
<option value="m22">Cadillac</option> 
<option value="m1">Chevrolet</option> 
<option value="m23">Chrysler</option> 
<option value="m24">Dodge</option> 
<option value="m25">Ferrari</option> 
<option value="m98">FIAT</option> 
<option value="m2">Ford</option> 
<option value="m203">Genesis</option> 
<option value="m26">GMC</option> 
<option value="m6">Honda</option> 
<option value="m28">Hyundai</option> 
<option value="m84">INFINITI</option> 
<option value="m31">Jaguar</option> 
<option value="m32">Jeep</option> 
<option value="m33">Kia</option> 
<option value="m35">Land Rover</option> 
<option value="m37">Lexus</option> 
<option value="m38">Lincoln</option> 
<option value="m40">Maserati</option> 
<option value="m42">Mazda</option> 
<option value="m43">Mercedes-Benz</option> 
<option value="m45">MINI</option> 
<option value="m46">Mitsubishi</option> 
<option value="m12">Nissan</option> 
<option value="m48">Porsche</option> 
<option value="m191">Ram</option> 
<option value="m111">smart</option> 
<option value="m53">Subaru</option> 
<option value="m7">Toyota</option> 
<option value="m55">Volkswagen</option> 
<option value="m56">Volvo</option> 
      
        </optgroup>
        <optgroup label="All Makes">
<option value="m4">Acura</option> 
<option value="m124">Alfa Romeo</option> 
<option value="m110">Aston Martin</option> 
<option value="m19">Audi</option> 
<option value="m3">BMW</option> 
<option value="m21">Buick</option> 
<option value="m22">Cadillac</option> 
<option value="m1">Chevrolet</option> 
<option value="m23">Chrysler</option> 
<option value="m24">Dodge</option> 
<option value="m25">Ferrari</option> 
<option value="m98">FIAT</option> 
<option value="m2">Ford</option> 
<option value="m203">Genesis</option> 
<option value="m26">GMC</option> 
<option value="m6">Honda</option> 
<option value="m28">Hyundai</option> 
<option value="m84">INFINITI</option> 
<option value="m31">Jaguar</option> 
<option value="m32">Jeep</option> 
<option value="m33">Kia</option> 
<option value="m34">Lamborghini</option> 
<option value="m35">Land Rover</option> 
<option value="m37">Lexus</option> 
<option value="m38">Lincoln</option> 
<option value="m39">Lotus</option> 
<option value="m40">Maserati</option> 
<option value="m42">Mazda</option> 
<option value="m141">McLaren</option> 
<option value="m43">Mercedes-Benz</option> 
<option value="m45">MINI</option> 
<option value="m46">Mitsubishi</option> 
<option value="m12">Nissan</option> 
<option value="m48">Porsche</option> 
<option value="m191">Ram</option> 
<option value="m49">Rolls-Royce</option> 
<option value="m111">smart</option> 
<option value="m53">Subaru</option> 
<option value="m7">Toyota</option> 
<option value="m55">Volkswagen</option> 
<option value="m56">Volvo</option> 
        </optgroup>
    </select>
    
        <script>
            $(document).ready(function() {
                var firstOptionValue = $("select[id='select-20754111'] option:first").val();
                $("select[id='select-20754111']").val(firstOptionValue);
            });
        </script>
</div>
<div>

  <select class="select model-select-dropdown btn-small form-control ft-model-selector" style="width: 260px;" cg-name="ign-modelId-entitySelectingHelper.selectedEntity" cg-data-yeardropdownheading="All Years" cg-data-trimdropdownheading="Select Trim" cg-data-groupbypopularity="true" disabled=""> 
		
      <option>All Models</option>

    
    
  </select>
</div>
<div>
	<table>
        <tbody><tr>
            <td>
    

  <select class="select car-select-dropdown btn-small form-control ft-year" cg-data-carswithregressiononly="false" cg-data-includeunknowntrimoption="false" style="width: 110px;" cg-name="ign-carId-entitySelectingHelper.selectedEntity" disabled="">
    <option>All Years</option>
  </select>
            </td>
            <td class="tdLabel carYearsSeparator">
                to
			</td>
            <td>
    
    <select class="select car-select-dropdown btn-small form-control ft-year" style="width: 110px;" cg-name="ign-car2Id-entitySelectingHelper.selectedEntity" disabled=""><option value="">All Years</option>
        </select>
            </td>
        </tr>
    </tbody></table>
</div>


         <table class="cg-dealFinder-searchByCar-zipRadius">
           <tbody><tr>
             <td class="tdLabel"><label for="zip">ZIP</label></td>
             <td class="tdContent">


<input type="text" name="zip" maxlength="7" value="75850" id="newSearchHeaderForm_NewCar_zip" class="cg-postalCode form-control" style="margin-top: 1px;">             </td>
           </tr>
           <tr>
            <td class="tdLabel"><label for="distance">Radius</label></td>
            <td class="tdContent">


<select name="distance" id="newSearchHeaderForm_NewCar_distance" class="distance form-control">
    <option value="10">10 mi</option>
    <option value="25">25 mi</option>
    <option value="50">50 mi</option>
    <option value="75">75 mi</option>
    <option value="100" selected="selected">100 mi</option>
    <option value="150">150 mi</option>
    <option value="200">200 mi</option>
    <option value="500">500 mi</option>
    <option value="50000">Nationwide</option>


</select>

            </td>
           </tr>
         </tbody></table>
         <div>
          <input type="submit" value="Search" class="btn btn-warning newSearchSubmitButton ft-submit" style="margin:15px auto 0 auto;display:block;">
         </div>
    <div class="clearInvisible"></div>



</form>

    </div>
  </div>
      </div>
    </div>

  <script type="text/javascript" charset="utf-8">
    $(function(){
      var possibleOptions;
      CG.ListingsView.init(possibleOptions);
    });
  </script>
    <div class="clearInvisible"></div>
        </div>
		
		
		
	<!-- NEW Filter -->


<div class="cg-usedCarSearch-filtersWrap ft-listing-results__filters">
        <div class="cg-dealFinder-leftColumnFilterSection-heading">
          <h3>Filter Results</h3>
          <div class="totalListingsCountAndLabel">
            <span class="totalListingsCount totalListingsCount--updating">11</span> <span class="totalListingsLabel">listings</span>
          </div>
        </div>
        <div class="cg-dealFinder-allFilters-wrap">
  <div>
   
     <div class="cg-dealFinder-filter-heading">
        <h3>Price</h3>
      </div>
   <!-- Price Range Slider -->
		<div class="price-range-slider">
		
		  <div id="slider-range" class="range-bar"></div>
		   <p class="range-value">
			<input type="text" id="amount" readonly>
		  </p>
		</div> 
   
   <!-- END Price Range Slider -->  
   
    <div class="cg-dealFinder-filter-heading">
        <h3>Mileage</h3>
      </div>
   
   
      <!-- miles range slider -->
		<div class="miles-range-slider">
		  <div id="miles-slider-range" class="miles-range-bar"></div>
		   <p class="miles-range-value">
			<input type="text" id="miles" readonly>
		  </p>
		</div> 
   
   <!-- END Price Range Slider -->  
    
  </div>


  <div id="transmissionFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all">  <div>
    <div class="cg-dealFinder-filter-heading">
      <h3>Transmission</h3>
    </div>
    <div id="transmissionFilter_OptionListContainer">
        <div id="transmissionFilter_OptionList" class="btn-group js-multi-checkbox-filter-expanded" data-toggle="buttons" data-role="controlgroup">
          <label class="btn btn-sm btn-default active" data-cg-value="ANY" id="option_any">
            <input type="radio" name="transmission-options" autocomplete="off" data-ga-event-label="Any" data-ga-trigger-event="change">Any
          </label>
          <label class="btn btn-sm btn-default" data-cg-value="M" id="option_manual">
            <input type="radio" name="transmission-options" autocomplete="off" data-ga-event-label="Manual" data-ga-trigger-event="change">Manual
          </label>
          <label class="btn btn-sm btn-default" data-cg-value="A" id="option_auto">
            <input type="radio" name="transmission-options" autocomplete="off" data-ga-event-label="Automatic" data-ga-trigger-event="change">Automatic
          </label>
        </div>
    </div>
  </div>
</div>
	  <script>
		if (typeof transmissionFilter === "object" && transmissionFilter !== null) {
			transmissionFilter.generateFilterContainerHtml("tempTransmissionFilterContainer");
		}
	  </script>

	<div id="nationwideShippingDivId" class="cg-dealFinder-filter-wrap ui-corner-all">
	  <fieldset data-role="controlgroup">
		  <div class="cg-dealFinder-filter-heading">
			<h3>Nationwide Shipping</h3>
		  </div>
		<div class="cg-dealFinder-singleCheckboxFilter">
			  <label for="nationwideShippingFilterCheckbox"><input type="checkbox" id="nationwideShippingFilterCheckbox" onclick="nationwideShippingFilter.handleChange();">Hide nationwide shipping
				<span class="cg-dealFinder-checkBoxFilter-labelCount" id="nationwideShippingCountMatching">(8)</span> </label>
		</div>
	  </fieldset>
	</div>

		<div id="trimFilterContainer" class="cg-dealFinder-filter-wrap ui-corner-all clearfix">
		<div class="cg-dealFinder-filter-heading clearfix">
			<span>(<a href="javascript:void(0);" onclick="trimFilter.clear();return false;">clear</a>)</span>
			<h3>Trim</h3>
		</div>

		<div class="dealfinderFilterChecklist clearfix">
		  <div id="trimFilterChecklist" style="overflow-y: hidden;">
			<div id="trimFilterChecklistLoadingPlaceholder" class="invisibleLayer">
			  Loading...
			</div>
		  <fieldset id="trim_filter" class="cg-dealFinder-checkboxFilter-wrap"><fieldset id="d2262"><div class="cg-dealFinder-checkBoxFilter-heading invisibleLayer"><span>BMW 2 Series</span><span class="cg-dealFinder-checkBoxFilter-selectAll"><span>(</span><a href="javascript:void(0);" data-ga-trigger-event="click">all</a><span>)</span></span></div><div data-role="controlgroup" style="padding-left: 25px;"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow"><label class="cg-dealFinder-checkBoxFilter-label" for="d2262228icouperwd57559458217"><input type="checkbox" value="d2262,228i Coupe RWD" id="d2262228icouperwd57559458217" data-ga-trigger-event="change"><span>228i Coupe RWD</span><span class="cg-dealFinder-checkBoxFilter-labelCount" id="c47869185700">(4)</span></label></div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow"><label class="cg-dealFinder-checkBoxFilter-label" for="d2262230iconvertiblerwd39894394642"><input type="checkbox" value="d2262,230i Convertible RWD" id="d2262230iconvertiblerwd39894394642" data-ga-trigger-event="change"><span>230i Convertible RWD</span><span class="cg-dealFinder-checkBoxFilter-labelCount" id="c73225362617">(1)</span></label></div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow"><label class="cg-dealFinder-checkBoxFilter-label" for="d2262230icouperwd73795642288"><input type="checkbox" value="d2262,230i Coupe RWD" id="d2262230icouperwd73795642288" data-ga-trigger-event="change"><span>230i Coupe RWD</span><span class="cg-dealFinder-checkBoxFilter-labelCount" id="c30740405175">(0)</span></label></div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow"><label class="cg-dealFinder-checkBoxFilter-label" for="d2262m235icouperwd75794376993"><input type="checkbox" value="d2262,M235i Coupe RWD" id="d2262m235icouperwd75794376993" data-ga-trigger-event="change"><span>M235i Coupe RWD</span><span class="cg-dealFinder-checkBoxFilter-labelCount" id="c11264853206">(6)</span></label></div></div></fieldset></fieldset></div>
		</div>
		<span id="expandTrimsFilter" class="cg-dealFinder-trims-filter-showMoreLess invisibleLayer"><a title="Show all trims" href="javascript:void(0);" onclick="trimFilter.expandList()">Show all trims ↓</a></span>
		<span id="collapseTrimsFilter" class="cg-dealFinder-trims-filter-showMoreLess invisibleLayer"><a title="Show fewer trims" href="javascript:void(0);" onclick="trimFilter.collapseList()">Show fewer trims ↑</a></span>
	  </div>


 
 
 
 
    <div class="cg-dealFinder-filter-heading">
        <h3>Days on Market</h3>
      </div>
   
      <!-- DAYS range slider -->
		<div class="days-range-slider">
		  <div id="days-slider-range" class="days-range-bar"></div>
		   <p class="days-range-value">
			<input type="text" id="days" readonly>
		  </p>
		</div> 
   
   <!-- END DAYS Range Slider -->  
 
 
 
 
 
	<div id="sinceLastVisitDivId" class="cg-dealFinder-filter-wrap ui-corner-all">
	  <fieldset data-role="controlgroup">
		  <div class="cg-dealFinder-filter-heading">
			<h3>Recent Listings</h3>
		  </div>
		<div class="cg-dealFinder-singleCheckboxFilter">
			  <label for="sinceLastVisitFilterCheckbox"><input type="checkbox" id="sinceLastVisitFilterCheckbox" onclick="sinceLastVisitFilter.handleChange();">Since my last visit (3 days ago)
				<span class="cg-dealFinder-checkBoxFilter-labelCount" id="sinceLastVisitCountMatching">(0)</span> </label>
		</div>
	  </fieldset>
	</div>



  <div id="wheelSystemFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all invisibleLayer">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="wheelSystemFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>Drivetrain</h3>
    </div>

    <div id="wheelSystemFilter_OptionListContainer">
        <div id="wheelSystemFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="wheelSystemFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-expanded" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_RWD_74681782122" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="RWD" id="option_RWD_74681782122" data-ga-event-label="RWD" data-ga-trigger-event="change">
         <span>RWD</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c8342549769506935">(11)</span> 
       
    </label>
  </div></div>

        <!--<span id="expandwheelSystemFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show all ↓</span>
        <span id="collapsewheelSystemFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>-->

    </div>
  </div>
</div>
  <script>
    if (typeof wheelSystemFilter === "object" && wheelSystemFilter !== null) {
      wheelSystemFilter.generateFilterContainerHtml("tempDriveTrainFilterContainer");
    }
  </script>
  <div id="exteriorColorFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="exteriorColorFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>Color</h3>
    </div>

    <div id="exteriorColorFilter_OptionListContainer">
        <div id="exteriorColorFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="exteriorColorFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-expanded" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_Black_1970072769" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="Black" id="option_Black_1970072769" data-ga-event-label="Black" data-ga-trigger-event="change">
         <span>Black</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c23099831351472954">(3)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_Blue_61064756886" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="Blue" id="option_Blue_61064756886" data-ga-event-label="Blue" data-ga-trigger-event="change">
         <span>Blue</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c6400602705117811">(2)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_Gray_53949529255" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="Gray" id="option_Gray_53949529255" data-ga-event-label="Gray" data-ga-trigger-event="change">
         <span>Gray</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c3830344061502233">(2)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_Silver_25299904292" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="Silver" id="option_Silver_25299904292" data-ga-event-label="Silver" data-ga-trigger-event="change">
         <span>Silver</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c5222836626806855">(1)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_White_17801834171" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="White" id="option_White_17801834171" data-ga-event-label="White" data-ga-trigger-event="change">
         <span>White</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c8746943363910548">(3)</span> 
       
    </label>
  </div></div>

        <span id="expandexteriorColorFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show all ↓</span>
        <span id="collapseexteriorColorFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>

    </div>
  </div>
</div>
  <script>
    if (typeof colorFilter === "object" && colorFilter !== null) {
      colorFilter.generateFilterContainerHtml("tempExteriorColorFilterContainer");
    }
  </script>

  <div id="engineFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="engineFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>Engine</h3>
    </div>

    <div id="engineFilter_OptionListContainer">
        <div id="engineFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="engineFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-expanded" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_I_92754854824" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="I4" id="option_I_92754854824" data-ga-event-label="I4" data-ga-trigger-event="change">
         <span>I4</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c3191839379439012">(5)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_I_7000817959" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="I6" id="option_I_7000817959" data-ga-event-label="I6" data-ga-trigger-event="change">
         <span>I6</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c6495165781495582">(6)</span> 
       
    </label>
  </div></div>

        <span id="expandengineFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show all ↓</span>
        <span id="collapseengineFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>

    </div>
  </div>
</div>
  <script>
    if (typeof engineFilter === "object" && engineFilter !== null) {
      engineFilter.generateFilterContainerHtml("tempEngineFilterContainer");
    }
  </script>


  <div id="installedOptionsFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="installedOptionsFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>Options</h3>
    </div>

    <div id="installedOptionsFilter_OptionListContainer">
        <div id="installedOptionsFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="installedOptionsFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-condensed" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__88112978746" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="6" id="option__88112978746" data-ga-event-label="Alloy Wheels" data-ga-trigger-event="change">
         <span>Alloy Wheels</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c61302010390138115">(3)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__50624998956" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="121" id="option__50624998956" data-ga-event-label="Backup Camera" data-ga-trigger-event="change">
         <span>Backup Camera</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c12173065193397959">(5)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__39150216856" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="120" id="option__39150216856" data-ga-event-label="Bluetooth" data-ga-trigger-event="change">
         <span>Bluetooth</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c14285324656113874">(10)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__3634020312" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="14" id="option__3634020312" data-ga-event-label="Climate Package" data-ga-trigger-event="change">
         <span>Climate Package</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c7996017973301843">(1)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__78251004427" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="29" id="option__78251004427" data-ga-event-label="Comfort Package" data-ga-trigger-event="change">
         <span>Comfort Package</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c8620671182392137">(1)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__22530031455" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="129" id="option__22530031455" data-ga-event-label="Driver Assistance Package" data-ga-trigger-event="change">
         <span>Driver Assistance Package</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c5192522609046369">(3)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__47828711121" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="43" id="option__47828711121" data-ga-event-label="Heat Package" data-ga-trigger-event="change">
         <span>Heat Package</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c7507800257792744">(1)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__25638813370" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="1" id="option__25638813370" data-ga-event-label="Leather Seats" data-ga-trigger-event="change">
         <span>Leather Seats</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c5363687234713761">(5)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__10412039410" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="125" id="option__10412039410" data-ga-event-label="M Sport Package" data-ga-trigger-event="change">
         <span>M Sport Package</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c24232001735816967">(2)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__86943348600" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="4" id="option__86943348600" data-ga-event-label="Navigation System" data-ga-trigger-event="change">
         <span>Navigation System</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c16966277703545507">(7)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__91610571253" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="12" id="option__91610571253" data-ga-event-label="Premium Package" data-ga-trigger-event="change">
         <span>Premium Package</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c8940044754235829">(2)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__68173581715" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="96" id="option__68173581715" data-ga-event-label="Sport Package" data-ga-trigger-event="change">
         <span>Sport Package</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c835190890852237">(3)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__51253164844" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="2" id="option__51253164844" data-ga-event-label="Sunroof/Moonroof" data-ga-trigger-event="change">
         <span>Sunroof/Moonroof</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c59076262129868164">(8)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__42559978176" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="103" id="option__42559978176" data-ga-event-label="Technology Package" data-ga-trigger-event="change">
         <span>Technology Package</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c9506427254213135">(2)</span> 
       
    </label>
  </div></div>

        <span id="expandinstalledOptionsFilter_OptionList" class="cg-options-filter-show-moreless">Show all ↓</span>
        <span id="collapseinstalledOptionsFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>

    </div>
  </div>
</div>
  <script>
    if (typeof installedOptionsFilter === "object" && installedOptionsFilter !== null) {
      installedOptionsFilter.generateFilterContainerHtml("installedOptionsFilterContainer");
    }
  </script>


  <div id="bodyTypeFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="bodyTypeFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>Body Style</h3>
    </div>

    <div id="bodyTypeFilter_OptionListContainer">
        <div id="bodyTypeFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="bodyTypeFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-expanded" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__71655970709" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="1" id="option__71655970709" data-ga-event-label="Convertible" data-ga-trigger-event="change">
         <span>Convertible</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c6132124942853361">(1)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__15427853376" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="0" id="option__15427853376" data-ga-event-label="Coupe" data-ga-trigger-event="change">
         <span>Coupe</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c6496993414993277">(10)</span> 
       
    </label>
  </div></div>

        <span id="expandbodyTypeFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show all ↓</span>
        <span id="collapsebodyTypeFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>

    </div>
  </div>
</div>
  <script>
    if (typeof bodyTypeFilter === "object" && bodyTypeFilter !== null) {
      bodyTypeFilter.generateFilterContainerHtml("tempBodyTypeFilterContainer");
    }
  </script>

  
  


  <script>
    if (typeof ratingFilter === "object" && ratingFilter !== null) {
      ratingFilter.generateFilterContainerHtml("tempRatingFilterContainer");
    }
  </script>
  <div id="fuelTypeFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all invisibleLayer">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="fuelTypeFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>Fuel Type</h3>
    </div>

    <div id="fuelTypeFilter_OptionListContainer">
        <div id="fuelTypeFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="fuelTypeFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-expanded" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__21903418088" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="2" id="option__21903418088" data-ga-event-label="Gasoline" data-ga-trigger-event="change">
         <span>Gasoline</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c5431960710417914">(11)</span> 
       
    </label>
  </div></div>

        <span id="expandfuelTypeFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show all ↓</span>
        <span id="collapsefuelTypeFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>

    </div>
  </div>
</div>
  <script>
    if (typeof fuelTypeFilter === "object" && fuelTypeFilter !== null) {
      fuelTypeFilter.generateFilterContainerHtml("tempFuelTypeFilterContainer");
    }
  </script>

  <div id="newUsedFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="newUsedFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>New / Used / CPO</h3>
    </div>

    <div id="newUsedFilter_OptionListContainer">
        <div id="newUsedFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="newUsedFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-expanded" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__8948749263" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="3" id="option__8948749263" data-ga-event-label="Certified Pre-Owned" data-ga-trigger-event="change">
         <span>Certified Pre-Owned</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c7437422143974448">(2)</span> 
       
    </label>
  </div><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__15670114670" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="2" id="option__15670114670" data-ga-event-label="Used" data-ga-trigger-event="change">
         <span>Used</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c3612379987235266">(9)</span> 
       
    </label>
  </div></div>

        <span id="expandnewUsedFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show all ↓</span>
        <span id="collapsenewUsedFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>

    </div>
  </div>
</div>
  <script>
    if (typeof newUsedFilter === "object" && newUsedFilter !== null) {
      newUsedFilter.generateFilterContainerHtml("tempNewUsedFilterContainer");
    }
  </script>


  
  <div id="truckCabinFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all invisibleLayer">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="truckCabinFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>Cabin</h3>
    </div>

    <div id="truckCabinFilter_OptionListContainer">
        <div id="truckCabinFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="truckCabinFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-expanded" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_Unknown_85992055467" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="Unknown" id="option_Unknown_85992055467" data-ga-event-label="Unknown" data-ga-trigger-event="change">
         <span>Unknown</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c20459995972357105">(11)</span> 
       
    </label>
  </div></div>

        <span id="expandtruckCabinFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show all ↓</span>
        <span id="collapsetruckCabinFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>

    </div>
  </div>
</div>
  <script>
    if (typeof truckCabinFilter === "object" && truckCabinFilter !== null) {
      truckCabinFilter.generateFilterContainerHtml("tempTruckCabinFilterContainer");
    }
  </script>
  <div id="truckBedSizeFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all invisibleLayer">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="truckBedSizeFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>Beds</h3>
    </div>

    <div id="truckBedSizeFilter_OptionListContainer">
        <div id="truckBedSizeFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="truckBedSizeFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-expanded" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_Unknown_5665274869" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="Unknown" id="option_Unknown_5665274869" data-ga-event-label="Unknown" data-ga-trigger-event="change">
         <span>Unknown</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c17294439951114372">(11)</span> 
       
    </label>
  </div></div>

        <span id="expandtruckBedSizeFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show all ↓</span>
        <span id="collapsetruckBedSizeFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>

    </div>
  </div>
</div>
  <script>
    if (typeof truckBedSizeFilter === "object" && truckBedSizeFilter !== null) {
      truckBedSizeFilter.generateFilterContainerHtml("tempTruckBedSizeFilterContainer");
    }
  </script>
  <div id="truckRearWheelFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all invisibleLayer">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="truckRearWheelFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>Rear Wheel</h3>
    </div>

    <div id="truckRearWheelFilter_OptionListContainer">
        <div id="truckRearWheelFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="truckRearWheelFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-expanded" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_Unknown_12183356230" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="Unknown" id="option_Unknown_12183356230" data-ga-event-label="Unknown" data-ga-trigger-event="change">
         <span>Unknown</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c3430659588694942">(11)</span> 
       
    </label>
  </div></div>

        <span id="expandtruckRearWheelFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show all ↓</span>
        <span id="collapsetruckRearWheelFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>

    </div>
  </div>
</div>
  <script>
    if (typeof truckRearWheelFilter === "object" && truckRearWheelFilter !== null) {
      truckRearWheelFilter.generateFilterContainerHtml("tempTruckRearWheelFilterContainer");
    }
  </script>
  <div id="seatingFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all invisibleLayer">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="seatingFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>Seating Capacity</h3>
    </div>

    <div id="seatingFilter_OptionListContainer">
        <div id="seatingFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="seatingFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-expanded" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option__67596368060" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="4" id="option__67596368060" data-ga-event-label="4" data-ga-trigger-event="change">
         <span>4</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c5687300347382429">(11)</span> 
       
    </label>
  </div></div>

        <span id="expandseatingFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show all ↓</span>
        <span id="collapseseatingFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>

    </div>
  </div>
</div>
  <script>
    if (typeof seatingFilter === "object" && seatingFilter !== null) {
      seatingFilter.generateFilterContainerHtml("tempSeatingFilterContainer");
    }
  </script>

  <div id="sellerTypeFilter_ContainerId" class="cg-dealFinder-filter-wrap ui-corner-all invisibleLayer">  <div>
    <div class="cg-dealFinder-filter-heading clearfix">
      <span id="sellerTypeFilter_Clear"><a href="javascript:void(0);">(clear)</a></span>
      <h3>Seller Type</h3>
    </div>

    <div id="sellerTypeFilter_OptionListContainer">
        <div id="sellerTypeFilter_LoadingPlaceholder" class="cg-dealFinder-filterPlaceholder invisibleLayer">
            Loading...
        </div>

        <div id="sellerTypeFilter_OptionList" class="cg-dealFinder-checkboxFilter-wrap js-multi-checkbox-filter-height js-multi-checkbox-filter-expanded" data-role="controlgroup"><div class="cg-dealFinder-checkBoxFilter-checkBoxRow">
    <label for="option_Dealer_67729446973" class="cg-dealFinder-checkBoxFilter-options-label">
       <input type="checkbox" value="Dealer" id="option_Dealer_67729446973" data-ga-event-label="Dealer" data-ga-trigger-event="change">
         <span>Dealer</span>
           <span class="cg-dealFinder-checkBoxFilter-labelCount" id="c2600707742724968">(11)</span> 
       
    </label>
  </div></div>

        <span id="expandsellerTypeFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show all ↓</span>
        <span id="collapsesellerTypeFilter_OptionList" class="cg-options-filter-show-moreless invisibleLayer">Show less ↑</span>

    </div>
  </div>
</div>
  <script>
    if (typeof sellerTypeFilter === "object" && sellerTypeFilter !== null) {
      sellerTypeFilter.generateFilterContainerHtml("tempSellerTypeFilterContainer");
    }
  </script>

<div id="photosDivId" class="cg-dealFinder-filterPhotos-wrap ui-corner-all">
  <fieldset data-role="controlgroup">
      <div class="cg-dealFinder-filter-heading">
        <h3>Photos</h3>
      </div>
    <div class="cg-dealFinder-singleCheckboxFilter">
          <label for="photosFilterCheckbox"><input type="checkbox" id="photosFilterCheckbox" onclick="photosFilter.handleChange();">Hide vehicles without photos
            <span class="cg-dealFinder-checkBoxFilter-labelCount" id="photosCountMatching">(2)</span> </label>
    </div>
  </fieldset>
</div>

  
  
  
   <div class="cg-dealFinder-filter-heading">
        <h3>Fuel Economy</h3>
      </div>
   
      <!-- DAYS range slider -->
		<div class="fuel-range-slider">
		  <div id="fuel-slider-range" class="fuel-range-bar"></div>
		   <p class="fuel-range-value">
			<input type="text" id="fuel" readonly>
		  </p>
		</div> 
   
   <!-- END Price Range Slider -->  
  
  <br><br>
  

        </div>
      </div>	
		
		
<?php include('footer-scripts.php');?>
