<!DOCTYPE HTML>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="max-age=604800" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="">
<style>
body{
  background: url("images/background.jpg");
    background-repeat: no-repeat;
	 background-attachment: top-fixed;
    background-position: top-center;
padding-top: 8%;	
}
</style>

		<title>OttoGuide Dealer</title>

		<link href="images/favicon.ico" rel="shortcut icon" type="image/x-icon">

		<!-- Font awesome 5 -->
		<link href="fonts/fontawesome/css/fontawesome-all.min.css" type="text/css" rel="stylesheet">
		
		<link href="css/price-calculator.css" type="text/css" rel="stylesheet">

		<!-- custom style -->
		<link href="css/uikit.css" rel="stylesheet" type="text/css"/>	
		
		<link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 900)" />

		<!-- custom javascript -->
		<script src="js/script.js" type="text/javascript"></script>

		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

		<link rel="stylesheet" type="text/css" media="all" href="og/tabs.css">

		<link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon" >
		
		<script type="text/javascript" src="js/librarary.js"></script>
		

		<script type="text/javascript">
			$(document).ready(function() {
				// Sets the cookie if the href of the current window is a paid search URL.
				CG.CookieUtils.setPaidReferrerCookie();

				//This is required so that deep objects (like arrays don't get serialized
				//in a "new" way).  This change is as of jquery 1.4.3
				$.ajaxSettings.traditional = true;
				$(document).ajaxError(CG.Utils.defaultAjaxErrorHandler);

			});
		</script>



		<script type="text/javascript">
		/// some script

		// jquery ready start
		$(document).ready(function() {
			// jQuery code

		}); 
		// jquery end
		</script>

		<style>
		.container {
			max-width: 85%;
		}
		</style>
		</head>