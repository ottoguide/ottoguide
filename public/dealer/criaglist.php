<?php $active5 = "active"; ?>
<?php include('head.php');?>
<?php $page_title = "Listings to Criagslist "; ?>
<?php include('header.php');?>

<style>

.nice-ul {
	position: relative;
	padding-left: 32px;
	list-style-type: none;
}
.nice-ul li {
	margin-bottom: 8px;
}
.nice-ul li:last-child {
	margin-bottom: 0;
}
.nice-ul li::before {
	content: "\2713";
	position: absolute;
	left: 0;
	padding: 2px 8px;
	font-size: 1em;
	color: #1C90F3;
}
 
.nice-ol {
	position: relative;
	padding-left: 32px;
	list-style-type: none;
	margin-left: 5%;
}
.nice-ol li {
	counter-increment: step-counter;
	margin-bottom: 6px;
	padding: 8px;
}
.nice-ol li:last-child {
	margin-bottom: 0;
}
.nice-ol li::before {
	content: counter(step-counter);
	position: absolute;
	left: 0;
	padding: 3px 8px;
	font-size: 0.9em;
	color: white;
	font-weight: bold;
	background-color: #217C9D;
	border-radius: 50%;
}
.square-box{
border:1px solid #999;		
}
.description-box{
margin-left: 10px;
padding: 5px;	
	
}
small{
margin-left: 10px;	
color:#222;
}
.border{
border:1px solid #ccc;
}
.items{
padding-left:0px;
padding-right:0px;		
}
.heading{
background-color: #217C9D;
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1f6591+1,2492a5+100 */
background: #1f6591; /* Old browsers */
background: -moz-linear-gradient(top, #1f6591 1%, #2492a5 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #1f6591 1%,#2492a5 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #1f6591 1%,#2492a5 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1f6591', endColorstr='#2492a5',GradientType=0 ); /* IE6-9 */
padding:6px;
color:#fff;
font-weight: bold;
}
.block-info{
padding: 10px;
border-bottom: 1px solid #ccc;
padding-bottom: 30px;	
}
.width{
width: 95%;
margin-left:5%;		
}
h5{
font-weight: bold;
color:#555;	
}
</style>
	
	
	   <br><br>
		<div class="width">
		<div class="row">
	

<div>



	
	<ol class="nice-ol">
		<li>Go to <a href="#"><b>Criaglist</b></a> and navigate to a city closest to you.</li>
		<li>Select the <b>post to classified </b> link in the upper left comer of the page.</li>
		<li>Select <b>for sale by dealer</b> and then select <b>cars & trucks.</b></li>
		<li>Find the listings you want to post from the list below. Download the cover and use the copy-paste text to help you save time typing. <br>Click
		<b>continue </b> after filling out the form.</li>
		<li> Upload the cover photo then upload your listing pictures.</li>
		<li> Follow the rest of the on screen direction to preview and publish your listing.</li>
	</ol>
</div>

		<br>
			
		<div class="items col-xs-12 col-sm-12 col-md-12 col-lg-12 border clearfix">
				<div class="heading">Listings </div>
                <?php for($i = 1; $i < 8; $i++){ ?>
			   <div class="info-block block-info clearfix">
                    <div class="square-box pull-left">
                    <span><img src="https://static.cargurus.com/images/forsale/2018/05/02/22/13/2018_bmw_2_series-pic-3260133447778141451-152x114.jpeg"width="100" height="70"></span>
                    </div>
					<div class="description-box pull-left">
                     <h5>2006 Nissan Sentra GXE     <small> Stock Number: </small> T768555 </h5>
                     <p><i class="fa fa-download" aria-hidden="true"></i> <a href="#">Download cover Photo </a>   &nbsp;   <i class="fa fa-copy" aria-hidden="true"></i> <a href="#"> Get the copy paste text</a></p>
                    </p>
					</div>
					<br><br>
                </div>
				
				<?php } ?>
        </div>	
			
			<div class="page-nation" align="center">
                                        <ul class="pagination pagination-large">
                                        <li class="disabled"><span>«</span></li>
                                        <li class="active"><span>1</span></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">6</a></li>
                                        <li class="disabled"><span>...</span></li><li>
                                        <li><a rel="next" href="#">Next</a></li>
                
                                     </ul>
                            </div>
		</div>		
					
					
			</div>	
			
	
<!-- ========================= SECTION CONTENT END// ========================= -->


<!-- ========================= FOOTER ========================= -->



<footer class="section-footer bg-secondary">

			<br> 
		</section>
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-6"> 
				<p class="text-white-50">  <br> .</p>
			</div>
			<div class="col-sm-6 text-right">
				<p class="text-sm-right text-white-50">
	Copyright &copy 2018 <br>
<a href="" class="text-white-50">miniMAX Solution</a>
				</p>
			</div>
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


</body>
<?php include('footer-scripts.php');?>

</html>