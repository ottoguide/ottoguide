<?php $active3 = "active"; ?>
<?php $page_title = "Pricing Tool"; ?>
<?php include('head.php');?>
<?php include('header.php');?>
<style>
.navbar{
background-color: #eaf4fd;	
}
.top-head{
background-color: #eaf4fd;
padding:12px;
border-bottom:1px solid #ccc;
	
}
.top-head h4{
font-weight: bold;
color:#222;		
	
}
.navbar-nav > li {
    border-right: 0px solid #fff;
	text-decoration: none;
	margin:3px;
}

.navbar-default .navbar-nav > li > a, .navbar-default .navbar-text {
    color: #217C9D;
	margin:3px;
	text-decoration: none;
}

.navbar-default .navbar-nav > li > a:hover {
    color: #fff;
    background-color: #217C9D;
	border-radius: 5px;
}

.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover {
    color: #fff;
    background-color: #217C9D;
	border-radius: 5px;
	margin:3px;
	text-decoration: none;
}

.bg-secondary{
background-color: #fefefe;		
}
.margin{
margin-left: -25px;	
}

</style>


<br>
	
	
	 <aside class="col-sm-4 margin">

	<?php include('price-calculator.php');?>
		
	</aside>
	
	<div class="col-md-8">
	
	<div class="pagingNav cg-dealFinder-sortHeader-wrap ui-corner-top clearfix js-paging-nav">
      <div class="cg-listingSearch-pagingPanel cg-listingSearch-upperPagingPanel">
          <a href="#" class="firstPageElement js-go-to-first-page " data-ga-action="Listings:Pagination" data-ga-event-label="First" data-ga-trigger-event="click" style="display: none;"><span class="glyphicon glyphicon-fast-backward"></span>First</a>
          <a href="#" class="previousPageElement js-go-to-previous-page " data-ga-action="Listings:Pagination" data-ga-event-label="Previous" data-ga-trigger-event="click" style="display: none;"><span class="glyphicon glyphicon-chevron-left"></span>Previous</a>
          <div id="displayedListingsCount" class="pagingLabel"><strong>1 - 13</strong> out of <strong>13</strong> listings</div>
          <a href="#" class="nextPageElement js-go-to-next-page " data-ga-action="Listings:Pagination" data-ga-event-label="Next" data-ga-trigger-event="click" style="display: none;">Next<span class="glyphicon glyphicon-chevron-right"></span></a>
      </div>

      <div class="cg-dealFinder-sortHeader-form">
      <select id="dealFinder-sortHeader-select" data-mini="true" class="form-control " data-ga-action="Listings:Sort" data-ga-trigger-event="change" data-dynamic-value="true">
      <option value="DEAL_RANK" class="ASC">Best deals first</option>
      <option value="DEAL_RANK" class="DESC">Worst deals first</option>
      <option value="PRICE" class="ASC">Lowest price first</option>
      <option value="PRICE" class="DESC">Highest price first</option>
      <option value="MILEAGE" class="ASC">Lowest mileage first</option>
      <option value="MILEAGE" class="DESC">Highest mileage first</option>
          <option value="PROXIMITY" class="ASC">Closest first</option>
          <option value="PROXIMITY" class="DESC">Farthest first</option>
      <option value="CAR_YEAR" class="ASC">Oldest first (by car year)</option>
      <option value="CAR_YEAR" class="DESC">Newest first (by car year)</option>
      <option value="DAYS_ON_MARKET" class="ASC">Newest listings first</option>
      <option value="DAYS_ON_MARKET" class="DESC">Oldest listings first</option>
          </select>
      </div>
	</div>
	
	
	<div class="cg-listingDivider clearfix"><span>All Listings</span></div>
	
	<?php for($i = 1; $i < 7; $i++){ ?>
	
  <div id="featured_listing_206314707">
  <div class="ft-car cg-dealFinder-result-wrap clearfix" title="Click to see details...">
    <div>
      <img class="cg-dealFinder-certified-image" src="https://static1.cargurus.com/gfx/spacer.gif" title="Certified Pre-Owned">
      <div class="hidden">CPO</div>
      <div class="cg-dealFinder-result-img" style="background-color:_#bgColor#_;">
        <div class="cg-dealFinder-result-img__imageContainer">
          <img class="" onerror="CG.Utils.replaceImageUrl(this, 'https://static1.cargurus.com/gfx/cg/noImageAvailable_152x114.png', 152, 114)" src="https://static.cargurus.com/images/forsale/2018/05/02/22/13/2018_bmw_2_series-pic-3260133447778141451-152x114.jpeg"
            id="206314707" style="background-color:_#bgColor#_;">
          <p id="contactedListingIcon206314707" class="contacted-listing cg-listing-infoOverlay invisibleLayer">Contacted</p>
          <p class="cg-listing-infoOverlay label-visited">Viewed</p>
          <div class="cg-dealFinder-seller-avatar hidden">
            <div class="cg-user-avatar cg-user-avatar--md">
              <span class="cg-user-avatar__facebook-icon _#privateSellerOnFacebook#_"></span>
              <span class="cg-user-avatar__googleplus-icon _#privateSellerOnGoogleplus#_"></span>

              <div class="avatar-image">
                <span style="background-image: url(_#privateSellerPhoto#_);" title=""></span>
              </div>

            </div>
          </div>
        </div>

      </div>
      <div class="cg-dealFinder-actionLinks">
        <div id="saveLink206314707" class="btn btn-default btn-sm listingsHoverOverActionClass " title="Click to save this listing" onclick="listingsSearchManager.saveButtonClick(event, 206314707);return false;">
          Edit
        </div>
        <div id="savedListingIcon206314707" class="btn btn-default btn-sm listingsHoverOverActionClass saved-listing invisibleLayer" title="This listing is saved" onclick="listingsSearchManager.unsaveButtonClick(event, 206314707);return false;">
           Unsave
        </div>
      </div>
      <h4 class="cg-dealFinder-result-model"><span class="invisibleLayer cg-listingSearch-tick"></span>2018 BMW 2 Series 230i Convertible RWD

      </h4>
   
      <div class="cg-dealFinder-result-stats">
        <p><strong>Price:</strong><span class="cg-dealFinder-priceAndMoPayment">
            <span>$40,956</span>

          <span class="bladeMonthlyPaymentCallout"> $762/mo est.</span>
          </span>
        </p>
        <p><strong>Mileage:</strong> 4,599 mi</p>
        <p class="cg-dealFinder-result-stats-distance-wrap">
          <strong>Location:</strong>
          <span class="_#resultStatsCityAndDistanceClass#_">
              <span class="cg-dealFinder-result-stats-distance">Houston, TX<span class="cg-dealFinder-result-stats-milesAway"> 82 mi</span></span>
          </span>
        </p>
        <p class=" hidden">
          <strong>&nbsp;</strong><span> <i class="cg-icon-map"></i> _#nationwideShippingTo#_ </span>
        </p>

        <div class="invisibleLayer hidden cg-listingBlade-privateSeller">
          <p><strong>Private Seller:</strong> _#privateSellerFirstNameOrScreenName#_</p>
        </div>
        <div class="cg-listingStub-renderFinancingEligibilityBadgeSection clearfix hidden">
          <div class="cg-financingEligibilityBadgeContainer clearfix">
            <i class="glyphicon glyphicon-usd"></i>
            <p class="cg-textBadgeTitle">Online Financing Available</p>
          </div>
        </div>

        <p class="cg-dealFinder-result-dealerRatingLabel cg-dealFinder-result-noborder">
          <strong>Days on Market </strong> 43 Days at dealership

        </p>
        

		  </div>
		</div>
	  </div>
	</div>
		
	<?php } ?>
	</div>
		

		
	</div>






  


<!-- ========================= SECTION CONTENT END// ========================= -->


<!-- ========================= FOOTER ========================= -->
<footer class="section-footer bg-secondary">
	<div class="container">
		
			<br> 
		</section>
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-6"> 
				<p class="text-white-50">  <br> .</p>
			</div>
			<div class="col-sm-6 text-right">
			
			</div>
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


</body>
<?php include('footer-scripts.php');?>

</html>