<html>
<head>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<style>
	.demo{
		margin-top: 7%;
		width: 100%;	
		
	}
	#DataTables_Table_0_wrapper{
	width: 100%;		
	}
	.panel{
	color: #333;		
		
	}
	
	th{
		font-weight: bold;
	text-align: center;		
	}
	
	td{
	text-align: center;		
	}
	</style>

</head>
<body>
			
<div class="demo">
  <table class="datatable table table-striped table-bordered">
    <thead>
      <tr>
        <th>Listing</th>
        <th>Rating</th>
        <th>Views</th>
        <th width="45%">Interactions</th>
        <th>On Site</th>
      </tr>
    </thead>
    <tbody>
    
     <?php for($i = 1; $i < 20; $i++){ ?>
   
       <tr class="grade A">
        <td><a href="#">WABA5557C82509</a> <p>2015 BMW 5-series</p><p>Stock#: T6972A</p></td>
        <td><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Good Deal</td>
        <td><i class="fa fa-binoculars" aria-hidden="true"></i> 2497</td>
        <td class="center"><i class="fa fa-envelope" aria-hidden="true"></i> 17 emails Leads   <i class="fa fa-heart" aria-hidden="true"></i> . 92 Saves  .   <i class="fa fa-link" aria-hidden="true"></i> 21 Websites Click</td>
        <td class="center">107 Days</td>
      </tr>
	  
	 <?php } ?>
	 
    </tbody>
    <tfoot>
      <tr>
      
      </tr>
    </tfoot>
  </table>
</div><!--/.container.demo -->
	
<script src="og/datatable-big.js"></script>
	
</body>
</html>