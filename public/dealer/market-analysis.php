<?php $active4 = "active"; ?>
<?php include('head.php');?>
<?php $page_title = "Market Analysis"; ?>
<?php include('header.php');?>
<style>
.form-control {
    width: 47%;
    height: 34px;
    padding: 6px 12px;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	float: left;
	margin-left:1%;
}
</style>
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="og/datatable.css">
	
		<div class="row bg-secondary">
	
		
					<div class="panel with-nav-tabs panel-primary">
						<div class="panel-heading">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tab1primary" data-toggle="tab">In Demand Inventory</a></li>
									<li><a href="#tab1primary" data-toggle="tab">Trending Local Searches </a></li>
								
								</ul>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab1primary">
							
				<div class="row">
				
				  <table>
							<thead>
							  <tr>
								<th width="40%"><h4><b>Search by Model in Your Area </b></h4></th>
								<th width="30%" align="center"><b>Searches per inventory Piece</b></th>
								<th width="20%" align="center"><b>Local Searches</b></th>
								<th width="10%" align="center"><b>Local Inventory</b></th>
							  </tr>
							</thead>
							<tbody>
						
							  <tr>
								<td>
								<form>
								<select name="cars" class="form-control">
								<option value="Chevrolet">Chevrolet (1750459)</option>
								<option value="Toyota">Toyota (1315508)</option>
								<option value="Honda">Honda (952361)</option>
								<option value="Nissan">Nissan (923242)</option>
								<option value="Jeep">Jeep (839784)</option>
								<option value="Hyundai">Hyundai (497964)</option>
								</select>
								<select name="cars" class="form-control">
								  <option value="volvo">Model 1</option>
								  <option value="saab">Model 2</option>
								  <option value="fiat">Model 3</option>
								  <option value="audi">Model 4</option>
								</select><br><br>
								&nbsp;&nbsp;<button type="button" class="btn btn-primary btn-small">Search</button>

								</form>
								</td>
								<td align="center">17</td>
								<td align="left">939</td>
								 <td align="left">53</td>
							  </tr>
							 
							
							</tbody>
						  </table>
				
				
					  <div id="admin" class="col s12">
						<div class="card material-table">
						
						  <table id="datatable">
							<h3><b>Top Searches by Model In Your Area </b></h3>
							<thead>
							  <tr>
								<th width="3%"><b>#</b></th>
								<th width="30%"><b>Model</b></th>
								<th><b>Searches (last month)</b></th>
								<th width="30%"><b>Change</b></th>
								<th><b>Searches per inventory Piece</b></th>
							 
							  </tr>
							</thead>
							<tbody>
							<?php $no=1;?>
							 <?php for($i = 1; $i < 15; $i++){ ?>
							  <tr>
								<td><?php echo $no++;?></td>
								<td>Chevrolet Silverado 1500 </td>
								<td>16,15</td>
								<td>1.662(11%)</td>
								 <td>25</td>
							  </tr>
							 <?php } ?>
							
							</tbody>
						  </table>
						</div>
					  </div>
					</div>
													</div>
								
								
								<div class="tab-pane fade" id="tab2primary">
								<br><br><br>
								<button type="button" class="btn btn-primary">Add Payment Method</button>
								<br><br><br>
								<p>Having issues entering billing information?   <b><a href="#">Click here to support</a></b></p>
								
									<br><br><br>
								<small>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages
								</div>
								
								
							</div>
								
								
							</div>
						</div>
					
					
					
		
		</div>
		
			
	
<!-- ========================= SECTION CONTENT END// ========================= -->


<!-- ========================= FOOTER ========================= -->

<script src="og/datatable.js"></script>

<footer class="section-footer bg-secondary">

			<br> 
		</section>
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-6"> 
				<p class="text-white-50">  <br> .</p>
			</div>
			<div class="col-sm-6 text-right">
				<p class="text-sm-right text-white-50">
	Copyright &copy 2018 <br>
<a href="" class="text-white-50">miniMAX Solution</a>
				</p>
			</div>
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


</body>
<?php include('footer-scripts.php');?>

</html>