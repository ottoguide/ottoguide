<script>
$(document).ready(function(){
    $('.container').fadeIn();
});
</script>
<?php include('head.php');?>
<link rel="stylesheet" type="text/css" media="all" href="og/filter.css">
<link rel="stylesheet" type="text/css" media="all" href="og/filter2.css">


<script type="text/javascript" src="js/external_jq_ui.entry.js"></script>

<script type="text/javascript" src="js/listings.entry.en_US.js"></script>
<script type="text/javascript" src="js/listings.entry.js"></script>
		
		
<style>
.navbar{
background-color: #eaf4fd;
margin-bottom: 0px;	
}
.top-head{
background-color: #eaf4fd;
padding:12px;
border-bottom:1px solid #ccc;
	
}
.top-head h4{
font-weight: bold;
color:#222;		
	
}
.navbar-nav > li {
    border-right: 0px solid #fff;
	text-decoration: none;
	margin:3px;
}

.navbar-default .navbar-nav > li > a, .navbar-default .navbar-text {
    color: #217C9D;
	margin:3px;
	text-decoration: none;
}

.navbar-default .navbar-nav > li > a:hover {
    color: #fff;
    background-color: #217C9D;
	border-radius: 5px;
}

.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover {
    color: #fff;
    background-color: #217C9D;
	border-radius: 5px;
	margin:3px;
	text-decoration: none;
}

.bg-secondary{
background-color: #fefefe;	
}
.grey{
background-color: #f2f2f2;			
}

.graph-tab-active{
 background-color: #217C9D;
}
	.tag {
    padding: 10px;
    background-color: #FFF6CA;
    font-size: 12px;
    color: #333;
    border-radius: 4px;
    text-align: center;
    font-weight: bold;
		}
		
	.margin{
	margin-left:-25px;	
		
	}
	
	.border-top{
	border-top:1px solid #ccc;
margin-top: 3px;	
		
	}
	
#big-amount{
border: 1px solid #ccc;
padding: 8px;
width: 36%;
color:green;
text-align:left;	
	
}

#big-amount2{
border: 1px solid #ccc;
padding: 8px;
width: 36%;
color:green;
text-align:left;	
	
}


.price-tool-header{
	background-color: #ccc;
	padding: 10px;
}	
</style>

<body>

	<br>
	<br>
	<br>
		
		<div class="container">
  
   <div class="row top-head">
   <div class="col-md-4"><h4>Classic Nissan for Statesville</h4></div>
    <div class="col-md-3"><h5><b>View/Edit Inventory</b></h5></div>
	<div class="col-md-5"><div class="tag">Give us your feedback to help to improve our product offering.</div></div>
   </div>
  
  
    <div class="row">
    
      <div id="navbar">    
  <nav class="navbar navbar-default navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            
          <div class="collapse navbar-collapse" id="navbar-collapse-1">
                <ul class="nav navbar-nav">
           <li class=""><a href="dashboard.php">Dashboard</a></li>
           
            <li class="">
              <a class="nav-link" href="billing.php">Billing</a>
            </li>
            <li class="active">
              <a class="nav-link" href="dealer-pricing.php">Dealer Pricing Tool</a>
            </li>
			
			<li class="">
              <a class="nav-link" href="market-analysis.php">Market Analysis</a>
            </li>
			
			<li class="nav-item">
              <a class="nav-link" href="criaglist.php">Craiglist Assistant</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="sales-review.php">Manager Review</a>
            </li>
			<li class="nav-item">
              <a class="nav-link" href="settings.php">Setting </a>
            </li>
               
			<li class="nav-item">
              <a class="nav-link" href="leads.php">Leads</a>
            </li>   
                    
            <li class="nav-item">
            <a class="nav-link" href="dealer-badges.php">Badges</a>
            </li>        
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
		</div>
		
	
	</div>
	
	
	<div class="row grey">
			<div class="col-md-4"><h2>Pricing Tool</h2></div>
			<div class="col-md-4"></div>
			<div class="col-md-2"><br>
			<b>Account Manager</b>
			<p>Key File Wanger</p>
			</div>
			<div class="col-md-2"><br>
			<b>General Support</b>
			<p>(800) 270-4847</p>
			</div>
		   </div>
	
	<br>
	 <aside class="col-sm-4 margin">

	<?php include('price-calculator.php');?>
		
	</aside>
	
	
	<div class="col-md-8">
	
	
	<div class="pagingNav cg-dealFinder-sortHeader-wrap ui-corner-top clearfix js-paging-nav">
      <div class="cg-listingSearch-pagingPanel cg-listingSearch-upperPagingPanel">
          <a href="#" class="firstPageElement js-go-to-first-page " data-ga-action="Listings:Pagination" data-ga-event-label="First" data-ga-trigger-event="click" style="display: none;"><span class="glyphicon glyphicon-fast-backward"></span>First</a>
          <a href="#" class="previousPageElement js-go-to-previous-page " data-ga-action="Listings:Pagination" data-ga-event-label="Previous" data-ga-trigger-event="click" style="display: none;"><span class="glyphicon glyphicon-chevron-left"></span>Previous</a>
          <div id="displayedListingsCount" class="pagingLabel"><strong>1 - 13</strong> out of <strong>13</strong> listings</div>
          <a href="#" class="nextPageElement js-go-to-next-page " data-ga-action="Listings:Pagination" data-ga-event-label="Next" data-ga-trigger-event="click" style="display: none;">Next<span class="glyphicon glyphicon-chevron-right"></span></a>
      </div>

      <div class="cg-dealFinder-sortHeader-form">
      <select id="dealFinder-sortHeader-select" data-mini="true" class="form-control " data-ga-action="Listings:Sort" data-ga-trigger-event="change" data-dynamic-value="true">
      <option value="DEAL_RANK" class="ASC">Best deals first</option>
      <option value="DEAL_RANK" class="DESC">Worst deals first</option>
      <option value="PRICE" class="ASC">Lowest price first</option>
      <option value="PRICE" class="DESC">Highest price first</option>
      <option value="MILEAGE" class="ASC">Lowest mileage first</option>
      <option value="MILEAGE" class="DESC">Highest mileage first</option>
          <option value="PROXIMITY" class="ASC">Closest first</option>
          <option value="PROXIMITY" class="DESC">Farthest first</option>
      <option value="CAR_YEAR" class="ASC">Oldest first (by car year)</option>
      <option value="CAR_YEAR" class="DESC">Newest first (by car year)</option>
      <option value="DAYS_ON_MARKET" class="ASC">Newest listings first</option>
      <option value="DAYS_ON_MARKET" class="DESC">Oldest listings first</option>
          </select>
      </div>
	</div>
	
	
	
	<div class="cg-listingDivider clearfix"><span>All Listings</span></div>
	
	
  <div id="featured_listing_206314707">
  <div class="ft-car cg-dealFinder-result-wrap clearfix" title="Click to see details...">
    <div>
      <img class="cg-dealFinder-certified-image" src="images/spacer.gif" title="Certified Pre-Owned">
      <div class="hidden">CPO</div>
      <div class="cg-dealFinder-result-img" style="background-color:_#bgColor#_;">
        <div class="cg-dealFinder-result-img__imageContainer">
         <img src="images/2018_bmw_2_series-pic.jpg" style="background-color:_#bgColor#_;">
          <p id="contactedListingIcon206314707" class="contacted-listing cg-listing-infoOverlay invisibleLayer">Contacted</p>
          <p class="cg-listing-infoOverlay label-visited">Viewed</p>
         
        </div>

      </div>
      <div class="cg-dealFinder-actionLinks">
        <div id="saveLink206314707" class="btn btn-default btn-sm listingsHoverOverActionClass " title="Click to save this listing">
          Edit
        </div>
        <div id="savedListingIcon206314707" class="btn btn-default btn-sm listingsHoverOverActionClass saved-listing invisibleLayer" title="This listing is saved" onclick="listingsSearchManager.unsaveButtonClick(event, 206314707);return false;">
           Unsave
        </div>
      </div>
      <h4 class="cg-dealFinder-result-model"><span class="invisibleLayer cg-listingSearch-tick"></span>2018 BMW 2 Series 230i Convertible RWD

      </h4>
   
      <div class="cg-dealFinder-result-stats">
        <p><strong>Price:</strong><span class="cg-dealFinder-priceAndMoPayment">
            <span>$40,956</span>

          <span class="bladeMonthlyPaymentCallout"> $762/mo est.</span>
          </span>
        </p>
        <p><strong>Mileage:</strong> 4,599 mi</p>
        <p class="cg-dealFinder-result-stats-distance-wrap">
          <strong>Location:</strong>
          <span class="_#resultStatsCityAndDistanceClass#_">
              <span class="cg-dealFinder-result-stats-distance">Houston, TX<span class="cg-dealFinder-result-stats-milesAway"> 82 mi</span></span>
          </span>
        </p>
        <p class=" hidden">
          <strong>&nbsp;</strong><span> <i class="cg-icon-map"></i> _#nationwideShippingTo#_ </span>
        </p>

        <div class="invisibleLayer hidden cg-listingBlade-privateSeller">
          <p><strong>Private Seller:</strong> _#privateSellerFirstNameOrScreenName#_</p>
        </div>
        <div class="cg-listingStub-renderFinancingEligibilityBadgeSection clearfix hidden">
          <div class="cg-financingEligibilityBadgeContainer clearfix">
            <i class="glyphicon glyphicon-usd"></i>
            <p class="cg-textBadgeTitle">Online Financing Available</p>
          </div>
        </div>

        <p class="cg-dealFinder-result-dealerRatingLabel cg-dealFinder-result-noborder">
          <strong>Days on Market </strong> 43 Days at dealership
        </p>
        

		  </div>
		</div>
		
		
		

	<div class="col-md-12 border-top">
		<br>
	  <div class="col-md-2">
	 <img src="images/arrow_right.png" width="35" align="center" style="margin-left:30%;">
	  <b style="color:green;"> Good Deal </b>
	  </div>
	   <div class="col-md-4">
	    <p class="range-value">
		New Price: <input type="text" id="big-amount" readonly>
		Saving: <b>0</b> 
		</p>
	   </div>
	 <div class="col-md-4">Search Rank: <b>48</b> out of <b>255</b> <br> Search Page: <b>4</div>
	  <div class="big-price-range-slider">
	   <br>
		  <div id="big-slider-range" class="range-bar"></div>
	  </div> 
	  
	  </div>
	  

	<!-- Price Range Slider -->
		
	 
   <!-- END Price Range Slider -->  
	
		
		
		
	  </div>
	
	</div>
	
		
	




	<div id="featured_listing_206314707">
  <div class="ft-car cg-dealFinder-result-wrap clearfix" title="Click to see details...">
    <div>
      <img class="cg-dealFinder-certified-image" src="images/spacer.gif" title="Certified Pre-Owned">
      <div class="hidden">CPO</div>
      <div class="cg-dealFinder-result-img" style="background-color:_#bgColor#_;">
        <div class="cg-dealFinder-result-img__imageContainer">
         <img src="images/2018_bmw_2_series-pic.jpg" style="background-color:_#bgColor#_;">
          <p id="contactedListingIcon206314707" class="contacted-listing cg-listing-infoOverlay invisibleLayer">Contacted</p>
          <p class="cg-listing-infoOverlay label-visited">Viewed</p>
         
        </div>

      </div>
      <div class="cg-dealFinder-actionLinks">
        <div id="saveLink206314707" class="btn btn-default btn-sm listingsHoverOverActionClass " title="Click to save this listing">
          Edit
        </div>
        <div id="savedListingIcon206314707" class="btn btn-default btn-sm listingsHoverOverActionClass saved-listing invisibleLayer" title="This listing is saved" onclick="listingsSearchManager.unsaveButtonClick(event, 206314707);return false;">
           Unsave
        </div>
      </div>
      <h4 class="cg-dealFinder-result-model"><span class="invisibleLayer cg-listingSearch-tick"></span>2018 BMW 2 Series 230i Convertible RWD

      </h4>
   
      <div class="cg-dealFinder-result-stats">
        <p><strong>Price:</strong><span class="cg-dealFinder-priceAndMoPayment">
            <span>$40,956</span>

          <span class="bladeMonthlyPaymentCallout"> $762/mo est.</span>
          </span>
        </p>
        <p><strong>Mileage:</strong> 4,599 mi</p>
        <p class="cg-dealFinder-result-stats-distance-wrap">
          <strong>Location:</strong>
          <span class="_#resultStatsCityAndDistanceClass#_">
              <span class="cg-dealFinder-result-stats-distance">Houston, TX<span class="cg-dealFinder-result-stats-milesAway"> 82 mi</span></span>
          </span>
        </p>
        <p class=" hidden">
          <strong>&nbsp;</strong><span> <i class="cg-icon-map"></i> _#nationwideShippingTo#_ </span>
        </p>

        <div class="invisibleLayer hidden cg-listingBlade-privateSeller">
          <p><strong>Private Seller:</strong> _#privateSellerFirstNameOrScreenName#_</p>
        </div>
        <div class="cg-listingStub-renderFinancingEligibilityBadgeSection clearfix hidden">
          <div class="cg-financingEligibilityBadgeContainer clearfix">
            <i class="glyphicon glyphicon-usd"></i>
            <p class="cg-textBadgeTitle">Online Financing Available</p>
          </div>
        </div>

        <p class="cg-dealFinder-result-dealerRatingLabel cg-dealFinder-result-noborder">
          <strong>Days on Market </strong> 43 Days at dealership
        </p>
        

		  </div>
		</div>
		
		
		
	<!-- Price Range Slider 2 -->

		<div class="col-md-12 border-top" style="font-weight: normal;">
		 <br>
		  <div class="col-md-2">
		 <img src="images/arrow_right2.png" width="35" align="center" style="margin-left:30%;">
		  <b style="color:#C6B31C;"> Fair Deal </b>
		  </div>
		   <div class="col-md-4">
			<p class="range-value">
			New Price: <input type="text" id="big-amount2" readonly>
			Saving: <b>0</b> 
			</p>
		   </div>
		 <div class="col-md-4">Search Rank: <b>36</b> out of <b>150</b> <br> Search Page: <b>6</b></div>
		  <div class="big-price-range-slider2">
		   <br>
			  <div id="big-slider-range2" class="range-bar"></div>
		  </div> 
		  
		</div>
	  
	 
   <!-- END Price Range Slider -->  
	
		
		
		
	  </div>
	
	</div>
	

	


	</div>
		

		
	</div>



<!-- ========================= SECTION CONTENT END// ========================= -->


<!-- ========================= FOOTER ========================= -->
<footer class="section-footer bg-secondary">
	<div class="container">
		
			<br> 
		</section>
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-6"> 
				<p class="text-white-50">  <br> .</p>
			</div>
			<div class="col-sm-6 text-right">
	
			</div>
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


</body>
<?php include('footer-scripts.php');?>

</html>