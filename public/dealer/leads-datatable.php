<html>
<head>
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<style>
	.demo{
		margin-top: 7%;
		width: 100%;	
		
	}
	#DataTables_Table_0_wrapper{
	width: 100%;		
	}
	.panel{
	color: #333;		
		
	}
	
	th{
		font-weight: bold;
	text-align: center;		
	}
	
	td{
	text-align: center;		
	}
	</style>

</head>
<body>
			
<div class="demo">
  <table class="datatable table table-striped table-bordered">
    <thead>
      <tr>
        <th>Date</th>
        <th>Name</th>
		<th>Zip</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Car</th>
		<th>VIN</th>
      </tr>
    </thead>
    <tbody>
    
     <?php for($i = 1; $i < 20; $i++){ ?>
   
       <tr class="grade A">
        <td>May,31 2018 <br>3:17:30 PM EDT</td>
        <td>MiniMax</td>
        <td>27470</td>
        <td>info@minimaxsolution.com</td>
        <td>(021) 913-5008</td>
		<td>2017 Nissan Sentra</td>
		<td>3N1AB7AP3HY0776</td>
      </tr>
	  
	 <?php } ?>
	 
    </tbody>
    <tfoot>
      <tr>
      
      </tr>
    </tfoot>
  </table>
</div><!--/.container.demo -->
	
<script src="og/datatable-big.js"></script>
	
</body>
</html>