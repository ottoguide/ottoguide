
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="CXM">
    <link rel="icon" href="http://dev.ottoguide.com/public/images/favicon.ico">
    <title>Otto Guide</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="http://dev.ottoguide.com/css/slider-pro.min.css">
    <link rel="stylesheet" href="http://dev.ottoguide.com/css/font-awesome.min.css">
    <link href="http://dev.ottoguide.com/css/bootstrap.min.css" rel="stylesheet">    
    <link href="http://dev.ottoguide.com/css/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="http://dev.ottoguide.com/css/common.css" rel="stylesheet">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	

</head>
  <body class="">


  <div class="header-margin py-5">
    <div class="container">
      <div class="row">                    
        <div class="col-lg-8 offset-lg-2">
          <div class="card">
            <div class="card-header font-weight-bold fs18">Sign in to Dealer Panel</div>
              <div class="card-body">
                <form method="POST" action="dashboard.php" id="register-login-form">     
                <input type="hidden" name="_token" value="oLkU2onbfQzVWWSM3sBNeud1PF3s3kp10kDM4EJj">  
                <div class="row">
                  <div class="col-sm-6 border-right">
                   <label class="radio-inline">
                      &nbsp;<input type="radio" onclick="$('.login_fields').show(); $('.log_out_fields').hide(); $('#register-login-form').attr('action',login_url);" name="sinin" checked>Sign in
                    </label>&nbsp;&nbsp;&nbsp;
                    <label class="radio-inline">
                      &nbsp;<input type="radio" onclick="$('.log_out_fields').show(); $('.login_fields').hide();  $('#register-login-form').attr('action',register_url); " name="sinin">Sign Up
                    </label><br>   

                    <label for="email">Email</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><span class="fa fa-envelope-o"></span></span>
                      </div>
                      <input type="text" class="form-control" name="email" placeholder="Email">
                    </div>                    
                    <label for="password">Password</label>
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><span class="fa fa-lock"></span></span>
                      </div>
                      <input type="password" name="password" class="form-control" autocomplete="off" placeholder="Password">
                    </div>
                    <label style="display: none;" class="log_out_fields" for="password">Mobile No</label>
                    <div style="display: none;" class="log_out_fields input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><span class="fa fa-phone"></span></span>
                      </div>
                      <input type="text" name="phone" class="form-control" placeholder="12345678">
                    </div>    
                    <div style="display: none;" class="row mb-3 log_out_fields">
                      <div class="text-center col-sm-12">                        
                        <button type="submit" class="btn btn-primary"><span class="fa fa-sign-in"></span> Sign Up</button>
                      </div>                      
                    </div>

                    <div class="row mb-3 login_fields">
                      <div class="col-sm-6">                        
                        <button type="submit" class="btn btn-primary"><span class="fa fa-sign-in"></span> Login</button>
                      </div>
                      <div class="col-sm-6">
                        <div class="custom-control custom-checkbox mt-2 fs14">
                          <input type="checkbox" class="custom-control-input" id="customChecklogin">
                          <label class="custom-control-label" for="customChecklogin">Remember me</label>
                        </div>
                      </div>
                    </div>
                                                          </form>
                    <a class="fs12 login_fields" href="#">Forgotten your password?</a>
                  </div>                  
                  <div class="col-sm-6 d-flex align-items-center">
                    <div>
                      <div class="input-group gplus-login">
                        <div class="input-group-prepend">
                          <span class="input-group-text bgc3" id="basic-addon1"><a href="http://dev.ottoguide.com/auth/google"><span class="fa fa-google-plus"></span></a></span>
                        </div>
                        <div class="input-group-append">
                          <a href="http://dev.ottoguide.com/auth/google"><button class="btn btn-light bgc3" type="button">Sign in with Google +</button></a>
                        </div>
                      </div>
               
                      <div class="input-group tt-login">
                        <div class="input-group-prepend">
                          <span class="input-group-text bgc4" id="basic-addon1"><a href="http://dev.ottoguide.com/auth/twitter"><span class="fa fa-twitter"></span></a></span>
                        </div>
                        <div class="input-group-append">
                         <a href="http://dev.ottoguide.com/auth/twitter"> <button class="btn btn-light bgc4" type="button">Sign in with Twitter</button></a>
                        </div>
                      </div>
                      <div class="input-group linkedin-login">
                        <div class="input-group-prepend">
                          <span class="input-group-text bgc5" id="basic-addon1"><span class="fa fa-linkedin"></span></span>
                        </div>
                        <div class="input-group-append">
                        <a href="http://dev.ottoguide.com/auth/linkedin">  <button class="btn btn-light bgc5" type="button">Sign in with LinkedIn</button></a>
                        </div>
                      </div>     
                    </div>               
                  </div>
                </div>  
                <div class="border-top pt-3 mt-4 text-center text-dark fs14"><span class="fa fa-lock fc1 fs24"></span> We'll always protect your Auto Trader account and keep your details safe.</div>
						<br>
					  <div class="message" width="50%" align="center">
                    		      </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
	
	
<br><br><br><br><br><br><br>
   
<!-- Placed at the end of the document so the pages load faster -->

 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="http://dev.ottoguide.com/js/popper.min.js"></script>
<script src="http://dev.ottoguide.com/js/bootstrap.min.js"></script>
<script src="http://dev.ottoguide.com/js/ie10-viewport-bug-workaround.js"></script>
<script src="http://dev.ottoguide.com/js/common.js"></script> 
