<?php $active9 = "active"; ?>
<?php include('head.php');?>
<?php $page_title = "Deal Rating Badges"; ?>
<?php include('header.php');?>
<style>

.nice-ul {
	position: relative;
	list-style-type: none;
}
.nice-ul li {
	margin-bottom: 8px;
}
.nice-ul li:last-child {
	margin-bottom: 0;
}
.nice-ul li::before {
	content: "\2713";
	position: absolute;
	left: 0;
	padding: 2px 8px;
	font-size: 1em;
	color: #1C90F3;
}
 
.nice-ol {
	position: relative;
	padding-left: 32px;
	list-style-type: none;
	margin-left: 5%;
}
.nice-ol li {
	counter-increment: step-counter;
	margin-bottom: 5px;
	padding: 2px;
}
.nice-ol li:last-child {
	margin-bottom: 0;
}
.nice-ol li::before {
	content: counter(step-counter);
	position: absolute;
	left: 0;
	padding: 3px 8px;
	font-size: 0.9em;
	color: white;
	font-weight: bold;
	background-color: #5AC405;
	border-radius: 50%;
}
.square-box{
border:1px solid #999;		
}
.description-box{
margin-left: 10px;
padding: 5px;	
	
}
small{
margin-left: 10px;	
color:#222;
}
.border{
border:1px solid #ccc;
}
.items{
padding-left:0px;
padding-right:0px;		
}
.heading{
background-color: #217C9D;
/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#1f6591+1,2492a5+100 */
background: #1f6591; /* Old browsers */
background: -moz-linear-gradient(top, #1f6591 1%, #2492a5 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #1f6591 1%,#2492a5 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #1f6591 1%,#2492a5 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1f6591', endColorstr='#2492a5',GradientType=0 ); /* IE6-9 */
padding:6px;
color:#fff;
font-weight: bold;
}
.block-info{
padding: 10px;
border-bottom: 1px solid #ccc;
padding-bottom: 30px;	
}
.width{
width: 100%;
}
h5{
font-weight: bold;
color:#555;	
}
.background-secondary{
background-color: #f2f2f2;
padding:10px;	
}
.margin{
	
margin-top: 2%;	
}
</style>
	   <br>
		<div class="width">
		<div class="row">
	
		<div class="col-md-12">
		<h3 align="center"><b>FREE for all dealers with inventory on OttoGuide</b></h3>
		<h4 align="center">You can now highlight Good/Great with our new Badges!</h4>
		</div>
		<br><br>	
	<div class="col-md-12 background-secondary">
		<div class="col-md-6">
		<img src="images/ad.png" class="img-responsive">
		</div>
		<div class="col-md-6">
		<br>
		<ol class="nice-ol">
		<li>FREE for all dealers with inventory.</li>
		<li>Enhanced trust with a valuable third-party endorsement.</li>
		<li>Only highlights Good/Great Deals.</li>
		<li>Close more effectively by showing customers that they getting a great deal.</li>
		<li>Badges are configurable to fit your website look and feel.</li>
	</ol>
		</div>
		</div>
	
	
	
		<div class="col-md-12 margin">
		<div class="col-md-6">
		<img src="images/ad2.png" class="img-responsive">
		</div>
		<div class="col-md-6">
		<br>
		<ol class="nice-ol">
		<li>FREE for all dealers with inventory.</li>
		<li>Enhanced trust with a valuable third-party endorsement.</li>
		<li>Only highlights Good/Great Deals.</li>
		<li>Close more effectively by showing customers that they getting a great deal.</li>
		<li>Badges are configurable to fit your website look and feel.</li>
	</ol>
		</div>
		</div>
<!-- ========================= SECTION CONTENT END// ========================= -->


<!-- ========================= FOOTER ========================= -->



<footer class="section-footer bg-secondary">

			<br> 
		</section>
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-6"> 
				<p class="text-white-50">  <br> .</p>
			</div>
			
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


</body>
<?php include('footer-scripts.php');?>

</html>