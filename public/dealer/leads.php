<?php $active8 = "active"; ?>
<?php include('head.php');?>
<?php $page_title = "Leads Report"; ?>
<?php include('header.php');

?>
<style>
.filter{
background-color: #eaf4fd;	
padding: 25px;
height: 120px;
}
</style>
		<div class="row bg-secondary">
	
					<div class="panel with-nav-tabs panel-primary">
						
						<div class="panel-heading">
								
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab1primary" data-toggle="tab">Email Leads</a></li>
								<li><a href="#tab1primary" data-toggle="tab">Phone Leads</a></li>
								<li><a href="#tab1primary" data-toggle="tab">SMS Leads</a></li>
								<li><a href="#tab1primary" data-toggle="tab">Chat Leads</a></li>
						     </ul>
						</div>
						
						<div class="panel-body">
						
							<div class="tab-content">
								
								<div class="tab-pane fade in active" id="tab1primary">
								
								<div class="row">
								<div class="col-md-12 filter">
								<form>
								<div class="col-md-5 col-lg-5"><label>From</label><input type="date" class="form-control"></div>
								<div class="col-md-5 col-lg-5"><label>To</label><input type="date" class="form-control"></div>
								<div class="col-md-2 col-lg-2"><br><button class="btn btn-small btn-primary">Filter</button>
								&nbsp;
								<button class="btn btn-small btn-primary">Export</button>
								</div>
								</form>
								</div>
								</div>
								
								<?php include('leads-datatable.php');?>
								
								</div>	
								
							</div>
								
								<div class="tab-pane fade" id="tab2primary">
								
								</div>
								
								<div class="tab-pane fade" id="tab3primary">
								
								
								</div>
								
								<div class="tab-pane fade" id="tab4primary">
								
								
								</div>	
								
								</div>
								
								<div class="tab-pane fade" id="tab5primary"></div>
						</div>
						</div>
					</div>
		
		</div>		
	

	</div>

	
</div>



<!-- ========================= SECTION CONTENT END// ========================= -->


<!-- ========================= FOOTER ========================= -->
<footer class="section-footer bg-secondary">
	<div class="container">

			<br> 
		</section>
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-6"> 
				<p class="text-white-50">  <br> .</p>
			</div>
			<div class="col-sm-6 text-right">
				<p class="text-sm-right text-white-50">
	Copyright &copy 2018 <br>
<a href="" class="text-white-50">miniMAX Solution</a>
				</p>
			</div>
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


</body>
<?php include('footer-scripts.php');?>

</html>