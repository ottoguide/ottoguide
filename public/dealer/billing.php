<?php $active2 = "active"; ?>
<?php include('head.php');?>
<?php $page_title = "Billing"; ?>
<?php include('header.php');?>
<script src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="og/datatable.css">
	
		<div class="row bg-secondary">
	
		
					<div class="panel with-nav-tabs panel-primary">
						<div class="panel-heading">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#tab1primary" data-toggle="tab">Invoices</a></li>
									<li><a href="#tab2primary" data-toggle="tab">Payment Method</a></li>
								
								</ul>
						</div>
						<div class="panel-body">
							<div class="tab-content">
								<div class="tab-pane fade in active" id="tab1primary">
							
								<div class="row">
  <div id="admin" class="col s12">
    <div class="card material-table">
  
      <table id="datatable">
        <thead>
          <tr>
            <th><b>Invoice date</b></th>
            <th></th>
            <th><b>Due Date</b></th>
            <th><b>Amount</b></th>
            <th><b>Balance</b></th>
            <th><b>Status</b></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>June 1, 2018</td>
            <td><a href="#" style="color:#3E94E1;">Download</a></td>
            <td>July 1, 2018</td>
            <td>$12,0000</td>
            <td>$12,0000</td>
            <td><b style="color: red;">Due</b></td>
          </tr>
         
		 <?php for($i = 1; $i < 15; $i++){ ?>
           <tr>
            <td>June 10, 2018</td>
            <td><a href="#" style="color:#3E94E1;">Download</a></td>
            <td>July 10, 2018</td>
            <td>$17,0000</td>
            <td>$0.00</td>
            <td><b style="color: green;">Resolved<b></td>
          </tr>
		 <?php } ?>
         
        </tbody>
      </table>
    </div>
  </div>
</div>
								</div>
								
								
								<div class="tab-pane fade" id="tab2primary">
								<br><br><br>
								<button type="button" class="btn btn-primary">Add Payment Method</button>
								<br><br><br>
								<p>Having issues entering billing information?   <b><a href="#">Click here to support</a></b></p>
								
									<br><br><br>
								<small>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages
								</div>
								
								
							</div>
								
								
							</div>
						</div>
					
					
					
		
		</div>
		
			
	
<!-- ========================= SECTION CONTENT END// ========================= -->


<!-- ========================= FOOTER ========================= -->



<script src="og/datatable.js"></script>

<footer class="section-footer bg-secondary">

			<br> 
		</section>
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-6"> 
				<p class="text-white-50">  <br> .</p>
			</div>
			<div class="col-sm-6 text-right">
				<p class="text-sm-right text-white-50">
	Copyright &copy 2018 <br>
<a href="" class="text-white-50">miniMAX Solution</a>
				</p>
			</div>
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


</body>
<?php include('footer-scripts.php');?>

</html>