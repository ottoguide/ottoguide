
		<style>
		.navbar{
		background-color: #eaf4fd;
		margin-bottom: 0px;	
		}
		.top-head{
		background-color: #eaf4fd;
		padding:12px;
		border-bottom:1px solid #ccc;
			
		}
		.top-head h4{
		font-weight: bold;
		color:#222;		
			
		}
		.navbar-nav > li {
			border-right: 0px solid #fff;
			text-decoration: none;
			margin:3px;
		}

		.navbar-default .navbar-nav > li > a, .navbar-default .navbar-text {
			color: #217C9D;
			margin:3px;
			text-decoration: none;
		}

		.navbar-default .navbar-nav > li > a:hover {
			color: #fff;
			background-color: #217C9D;
			border-radius: 5px;
		}

	    .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .active > a:hover {
			color: #fff;
			background-color: #217C9D;
			border-radius: 5px;
			margin:3px;
			text-decoration: none;
		 }

		.bg-secondary{
		background-color: #fefefe;	
		}
		.grey{
		background-color: #f2f2f2;			
		}

		.graph-tab-active{
		 background-color: #217C9D;
		}
		
		.tag {
    padding: 10px;
    background-color: #FFF6CA;
    font-size: 12px;
    color: #333;
    border-radius: 4px;
    text-align: center;
    font-weight: bold;

		}
		.pie-active{
		padding:10px;
		background-color :#217C9D;
		color: #fff;	
		}

	input[type="search"] {
	  text-align: left;
	}
		

		</style>

	<body>

	<br>
	<br>
	<br>
		
		<div class="container">
  
   <div class="row top-head">
   <div class="col-md-4"><h4>Classic Nissan for Statesville</h4></div>
    <div class="col-md-3"><h5><b>View/Edit Inventory</b></h5></div>
	<div class="col-md-5"><div class="tag">Give us your feedback to help to improve our product offering.</div></div>
   </div>
  
    <div class="row">
		
      <div id="navbar">    
	<nav class="navbar navbar-default navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            
            <div class="collapse navbar-collapse" id="navbar-collapse-1">
                <ul class="nav navbar-nav">
           <li class="<?php echo $active1;?>"><a href="dashboard.php">Dashboard</a></li>
           
            <li class="<?php echo $active2;?>">
              <a class="nav-link" href="billing.php">Billing</a>
            </li>
            
			<li class="<?php echo $active3;?>">
              <a class="nav-link" href="dealer-pricing.php">Dealer Pricing Tool</a>
            
			</li>
			<li class="<?php echo $active4;?>">
              <a class="nav-link" href="market-analysis.php">Market Analysis</a>
            </li>
			
			<li class="<?php echo $active5;?>">
              <a class="nav-link" href="criaglist.php">Craiglist Assistant</a>
            </li>
			
			<li class="<?php echo $active6;?>">
              <a class="nav-link" href="sales-review.php">Manager Review</a>
            </li>
			
			<li class="<?php echo $active7;?>">
              <a class="nav-link" href="settings.php">Settings</a>
            </li>	
			
			<li class="<?php echo $active8;?>">
            <a class="nav-link" href="leads.php">Leads</a>
            </li>
			
			<li class="<?php echo $active9;?>">
            <a class="nav-link" href="dealer-badges.php">Badges</a>
            </li>
			
			  
                </ul>
            </div><!-- /.navbar-collapse -->
		</nav>
		</div>
		
	
	</div>
	
			<div class="row grey">
			<div class="col-md-4"><h2><?php echo $page_title;?></h2></div>
			<div class="col-md-4"></div>
			<div class="col-md-2"><br>
			<b>Account Manager</b>
			<p>Key File Wanger</p>
			</div>
			<div class="col-md-2"><br>
			<b>General Support</b>
			<p>(800) 270-4847</p>
			</div>
		   </div>
