<?php $active7 = "active"; ?>
<?php include('head.php');?>
<?php $page_title = "Dealership Settings"; ?>
<?php include('header.php');?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.0';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<style>

.card-inner{
    margin-left: 4rem;
}
.p-0{
padding-left:0px;	
}
.b-0{
margin-bottom:0px;	
}

.fa-star{
color: #ffc107!important;	
}
.border-top{
border-top: 1px solid #ccc;
margin-bottom:5px;
	
}
.twitter-share-button{
margin-bottom:-5px;	
}
</style>
	   <br><br>
		<div class="width">
		<div class="row">
	
		<div class="col-md-12">
		<p><b>Lead Email Addresses</b></p>
		<p>When a customer fills out the <b>Contact Dealer</b> from on Otto Guide We will send the email on address.</p>
		<br>
		
		<table class="datatable table table-striped">
		
		<thead>
		<th>Email Address</th>
		<th>Email Format</th>
		<th>For Inventory</th>
		</thead>
		
		<tbody>
		<td>classicnissan@ottoguide.com</td>
		<td>ADF</td>
		<td>New & Used</td>
		<td><b>No Known Issues </b></td>
		<td><button class="btn btn-success btn-small">Edit Email</td>
		</tbody>
		
		</table>
		
		<p><button class="btn btn-primary"> Add New Email </button></p>
		<br>
		<p><b>Phone</b></p>
		<p>Current phone :<b> (888) 617-0892</b></p>
		<input type="text" class="form-control" value="(888) 617- 0892">
		<br>
		<p><button class="btn btn-primary">Update Sales Phone Number</button></p>
		<p><b>Website: </b></p><p>Website Url: <input type="text" class="form-control"></p>
		<p><input type="checkbox"> Enable deep linking to your VDP pages</p>
		<small>(if we are able to deep to dealer website.It will take effect in 24 hours checking the box.)</small><br><br>
		<p><input type="checkbox"> Over ride automatically generated tracking URL</p><br>
		<p><button class="btn btn-primary">Save Settings</button></p>

		</div>	
			
	
<!-- ========================= SECTION CONTENT END// ========================= -->


<!-- ========================= FOOTER ========================= -->



<footer class="section-footer bg-secondary">

			<br> 
		</section>
		<section class="footer-bottom row border-top-white">
			<div class="col-sm-6"> 
				<p class="text-white-50">  <br> .</p>
			</div>
			
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->


</body>
<?php include('footer-scripts.php');?>

</html>