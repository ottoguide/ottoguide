
<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light1", // "light1", "light2", "dark1", "dark2"
	title:{
		text: ""
	},
	axisY: {
		title: ""
	},
	data: [{        
		type: "column",  
		showInLegend: true, 
		legendMarkerColor: "grey",
		legendText: "Interaction = One million",
		dataPoints: [      
			{ y: 300878, label: "Dec 2017" },
			{ y: 266455,  label: "Jan 2018" },
			{ y: 169709,  label: "Feb 2018" },
			{ y: 158400,  label: "March 2018" },
			{ y: 142503,  label: "April 2018" },
			{ y: 101500, label: "May 2018" },
			{ y: 97800,  label: "June 2018" },
			{ y: 80000,  label: "July 2018" }
		]
	}]
});
chart.render();

}
</script>

						<div class="col-md-2">
						
								<p class="graph-tab-active">
								<h3>15,0000</h3>
								<p>Total Interaction</p>
								</p>
								<hr>
								
								<p>
								<h3>5,000</h3>
								<p>Interaction <br> <small>(Past 30 Days)</small></p>
								</p>
								
								<hr>
								
								<p>
								<h3>8,000</h3>
								<p>Interaction <br> <small>(Past 4 Weeks)</small></p>
								</p>
								
								<hr>
								<p>
								<h3>10,0000</h3>
								<p>Interaction <br> <small>(Past 6 Months)</small></p>
								</p>
								
						</div>
								
							<div class="col-md-10">
							
							<div id="chartContainer" style="height: 350px; width: 100%;"></div>
						   
							</div>
							
							<div class="col-md-12">
							<?php include('datatable.php');?>
							</div>
							
							
							
							
			<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>		
		
