<?php $this->load->view('includes/after-login/header'); ?>
<?php $this->load->view('includes/after-login/sidebar'); ?>

<script
    src="https://code.jquery.com/jquery-2.1.4.min.js"
    integrity="sha256-8WqyJLuWKRBVhxXIL1jBDD7SDxU936oZkCnxQbWwJVw="
    crossorigin="anonymous">
</script>
<style>
button, input, select, textarea {
    color: white !important;
}
.dark{
background-color: #222;
margin-left:1px;
margin-right:1px;
padding-top:5px;		
}
.custom-padding{
 padding: 7px 8px;
} 
.minus{
margin-top:6px;		
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Cargar Reporte			
            <small>Editar</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('historical-list.html'); ?>"><i class="fa fa-dashboard"></i> Cargar reporte</a></li>
            <li class="active">Editar</li>
        </ol>
    </section>

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> Nombre de la carpeta</h3>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <form action="<?= base_url('historical-edit-submit.html') ?>" method="post" id="crd_form" enctype="multipart/form-data">

						 <div class="row">
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <label>Nombre de la carpeta</label>
									<input type="text" class="form-control" name="category_title"
										value="<?= html_escape ($this->formdata->category_title) ?>"
									id="category_title">
								
                                    </div>
                                </div>
                            </div>

						
							
                            <div class="row">
                                 <div class="col-sm-5">
                                    <div class="form-group">
                                        <label for="region_id">Sector</label>
                                        <select class="form-control" name="region_id" id="region_id">
                                            <option value=""> - Seleccionar el sector - </option>
				<?php foreach ($this->db->where('is_deleted',0)->get('emerson_region')->result_array () as $row): ?>
												<?php $selected = ($row['id']== $this->formdata->region_id)?'selected':''; ?>
                                                <option <?=$selected?> value="<?=  ($row['id']) ?>"><?= html_escape (ucwords($row['region_title'])) ?></option>
											<?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
								
                                <div class="col-sm-5">
                                    <div class="form-group">
									  <label for="region_id">Site</label>
                                        <select class="form-control" name="site_id" >
                                            <option value=""> - Seleccione una sitio - </option>
				<?php foreach ($this->db->where('is_deleted',0)->get('emerson_site')->result_array () as $row): ?>
												<?php $selected = ($row['id']== $this->formdata->site_id)?'selected':''; ?>
                                                <option <?=$selected?> value="<?=  ($row['id']) ?>"><?= html_escape (ucwords($row['site_title'])) ?></option>
											<?php endforeach; ?>
                                        </select>

										
                                    </div>
                                </div>

								 <div class="col-sm-2">
								 </div>
						
                            </div>


							<div class="row">
							   <div class="col-md-6">
                                    <div class="">
									<label>Fecha del reporte</label>
                                        <input type="text" name="daterange"
                                               class="form-control"
											   value="<?= html_escape ($this->formdata->report_date) ?>"
                                               id="historical_report_date"placeholder="Fecha de informe" />
                                    </div>
                                </div>
							</div>
							<br>
						  <div class="report">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Datos de la tarea</label>
                                        <input type="text" class="form-control" name="parrent_report_title"
											value="<?= html_escape ($this->formdata->report_title) ?>"
										id="parrent_report_title">
	<input type="hidden" class="form-control" value="<?= html_escape ($this->formdata->id) ?>" name="main_report_id">
									
								
								
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Adjuntar un archivo</label>
                                        <input type="file" class="form-control"
											value="<?= html_escape ($this->formdata->report_file) ?>"

										id="pdf_report" name="pdf_report"><br>
											<input type="hidden" class="form-control" value="<?= html_escape ($this->formdata->report_file) ?>" name="parrent_report_file">
		
										<a href="<?php echo base_url();?>site-uploads/pdf-reports/ <?= html_escape ($this->formdata->report_file) ?>"target="_blank"><i class="fa fa-file-pdf-o"></i>   <?= html_escape ($this->formdata->report_file) ?></a>
                                    </div>
                                </div>
							</div>	
						
							</div>
							
<input type="hidden" class="form-control" value="<?= html_escape ($this->formdata->is_parrent) ?>" name="parrent_id">
								
							    <script>
								$(function() {
									$("#addMore").click(function(e) {
										e.preventDefault();
										var count = $('.js-from_add_field').length;
									
										var final_html = "<div class='row dark js-from_add_field'>";
										final_html += ("<div class='col-sm-5'><div class='form-group'><label>Datos de la tarea</label><input type='text' class='form-control' name='child_report_title2["+count+"]'></div></div>");
										final_html += ("<div class='col-sm-6'><div class='form-group'> <label>Adjuntar un archivo</label><input type='file'  class='form-control child_report_files2' name='child_report_files2_"+count+"'><input type='hidden' name='child_report_files_check2_["+count+"]' class='check_file2' value=''></div></div>");
										final_html += '<br><div class="col-sm-1 minus"><button type="button" class="btn btn-danger btn-sm remove_more_fld"><i class="fa fa-times"></i></button></div>';
										final_html += '</div><br>';
										$(".report").append(final_html);
									});

									$(document).on('click', '.remove_more_fld', function (e) {
										e.stopImmediatePropagation();

										$(this).parent().parent().remove();
									});
									
									$(document).on('change', '.child_report_files2', function() {
										
										if ($(this)[0].files.length > 0) {
											$(this).next('.check_file2').val('1');
										}
									
									})
								});
                                </script>
			
<?php 
$no = 0;
?>			
								
     <?php 
	  foreach ($this->db->where('is_deleted',0)->where('is_parrent',$this->formdata->id)->order_by('id','ASC')->limit(10)->offset(1)->get('emerson_historical_reports')->result_array () as $row): ?>
						
					<div class="row dark">
					<div class="col-sm-6">
					<div class="form-group">
					<label>Datos de la tarea</label>
					

					<input type="text" class="form-control" value="<?= html_escape ($row['report_title']) ?>" name="child_report_title[<?php echo $no++;?>]">
					
					
					<input type="hidden" class="form-control" value="<?php echo $row['id']; ?>" name="child_report_id[]">
					
					<input type="hidden" class="form-control" value="<?= html_escape ($row['report_file']) ?>" name="child_report_file">
					</div>
					</div>
					<div class="col-sm-6">
					<div class="form-group"> <label>Adjuntar un archivo</label>
					<input type="file" class="form-control" value="<?= html_escape ($row['report_file']) ?>" name="child_report_files_[<?php echo $no++;?>]"><br>
					
					<a href="<?php echo base_url();?>site-uploads/pdf-reports/<?php $row['report_file'];?>"target="_blank"><i class="fa fa-file-pdf-o"></i><?= html_escape ($row['report_file']) ?></a>
					
					</div>
					</div>
					<br>
					
					<!--<div class="col-sm-1 minus">
					<button type="button" class="btn btn-danger btn-sm remove_more_fld">
					<i class="fa fa-times"></i></button>
					</div>-->
					</div>
					<br>
					
							
					
				<?php endforeach; ?>
					
					
                                     
	
					<div class="row">
								<div class="col-sm-2 ">
                                    <div class="form-group"><br>
                                    <button id="addMore" class="btn btn-primary"style="color:#fff;">Añadir</button>
                                    </div>
                                </div>
								</div>
                            <div class="row">
                                <div class="col-sm-2 col-sm-offset-10 text-right">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <button type="button" class="btn btn-primary btn-block js-form_btn" style="color: #fff !important;">Guardar</button>
                                    </div>
                                </div>
                            </div>
							
                            <p class="bg-danger js-msgbox"></p>

                        </form>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
    <!-- /.content -->
</div><!-- /.content-wrapper -->


<script src="<?= site_url() ?>js/jquery.min.js"></script>
<script src="<?= site_url() ?>js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?= site_url() ?>js/ie10-viewport-bug-workaround.js"></script>
<script src="<?= site_url() ?>js/wow.min.js"></script>
<script src="<?= site_url() ?>js/common.js"></script>


<?php $this->load->view('includes/after-login/footer-tag'); ?>
<!-- Control Sidebar -->
<?php $this->load->view('includes/after-login/right-side-bar'); ?>
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
	 immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div><!-- ./wrapper -->
<?php $this->load->view('includes/after-login/footer'); ?>


<script src="<?= site_url(); ?>js/doFormValidation.js"></script>

<script>

	date_from = date_to = "<?=date('Y-m-d')?>";

	$('input[name="daterange"]').datepicker({
		language: 'es'
    });

	$('input[name="daterange"]').on('change', function () {
		date_from = $("#from_date").val();
		date_to = $("#to_date").val();
	});


</script>

<script type="text/javascript">

  		$(document).ready(function()

  		{

		
		 function MM_openBrWindow(theURL,winName,features) { 

        window.open(theURL,winName,features);

      }
		
		
		</script>
<script>
	$(function ()
	{
		var faqadd = {'form':'#crd_form', 'msgbox':'#crd_form .js-msgbox' , 'btnClick':'#crd_form .js-form_btn' ,'urlValidator': "<?= base_url("historical-edit-validate.html"); ?>" ,'loadingImg':"<?php echo site_url("media/load-indicator.gif"); ?>"};
		doFormValidation (faqadd);
	});
</script>
</body>
</html>
