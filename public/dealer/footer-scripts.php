<script>
    // Feature detection
    (function (d, tests) {
      var i, classArr,
          features = tests;

      if (typeof CG === 'object' && CG !== null && CG.hasOwnProperty('Utils')) {
        for (var prop in features) {
          if (CG.Utils.isSupportedCSS(prop)) {
            CG.Utils.classList.remove('no-' + features[prop]);
            CG.Utils.classList.add(features[prop]);
          }
          else {
            CG.Utils.classList.add('no-' + features[prop]);
          }
        }
      }
    })(document, { 'box-flex': 'flexbox', 'flex': 'flexbox' });
  </script>
  <script>
    if (typeof CG === 'object' && CG !== null && CG.hasOwnProperty('Utils')) {
      if (CG.Utils.isTouch()) {
        CG.Utils.classList.add('touch');
      }
      else {
        CG.Utils.classList.add('no-touch');
      }
    }
  </script>


        <script type="text/javascript">
            var isModal = false;
            $(window).on("load", function() {
                if (typeof CG === 'object' && CG !== null && CG.hasOwnProperty('UserFeedbackWidget')) {
                  CG.UserFeedbackWidget.init({
                    showAsModal: isModal,
                    locale: "en_US",
                    src: '!function(e,o,n){window.HSCW=o,window.HS=n,n.beacon=n.beacon||{};var t=n.beacon;t.userConfig={},t.readyQueue=[],t.config=function(e){this.userConfig=e},t.ready=function(e){this.readyQueue.push(e)},o.config={docs:{enabled:!0,baseUrl:"//cargurus.helpscoutdocs.com/"},contact:{enabled:!0,formId:"f8d1ebfd-96f6-11e6-91aa-0a5fecc78a4d"}};var r=e.getElementsByTagName("script")[0],c=e.createElement("script");c.type="text/javascript",c.async=!0,c.src="https://djtflbt20bdde.cloudfront.net/",r.parentNode.insertBefore(c,r)}(document,window.HSCW||{},window.HS||{});'
                  });
                }
              });
        </script>

<script type="text/javascript">
  if (typeof CG === 'object' && CG !== null && CG.hasOwnProperty('GoogleData')) {
    CG.GoogleData.assembleAndPush('data_tag');
  }
</script>



    <script type="text/javascript">
      if (typeof CG === 'object' && CG !== null && CG.hasOwnProperty('pageTracker')) {
        CG.pageTracker.trackCurrentGAPageview("ForSale-MODEL-false");
      }
    </script>
  <script type="text/javascript">
    //Hide jquery migrate traces
    jQuery.migrateTrace =  false;
    jQuery.migrateMute =  true;
    $(window).on("load", function() {
          $(window).trigger("LogTime", ["/Cars/inventorylisting/viewDetailsFilterViewInventoryListing.action", 'load']);
    });
	

	//-----JS for Price Range slider-----

$(function() {
	$( "#slider-range" ).slider({
	  range: true,
	  min: 10000,
	  max: 60000,
	  values: [ 2000, 58000 ],
	  slide: function( event, ui ) {
		$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
	  }
	});
	$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
	  " - $" + $( "#slider-range" ).slider( "values", 1 ) );
});



$(function() {
	$( "#big-slider-range" ).slider({
	  range: true,
	  min: 10000,
	  max: 60000,
	  values: [ 2000, 60000 ],
	  slide: function( event, ui ) {
		$( "#big-amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
	  }
	});
	$( "#big-amount" ).val( "$" + $( "#big-slider-range" ).slider( "values", 0 ) +
	  " - $" + $( "#big-slider-range" ).slider( "values", 1 ) );
});


$(function() {
	$( "#big-slider-range2" ).slider({
	  range: true,
	  min: 10000,
	  max: 60000,
	  values: [ 2000, 60000 ],
	  slide: function( event, ui ) {
		$( "#big-amount2" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
	  }
	});
	$( "#big-amount2" ).val( "$" + $( "#big-slider-range2" ).slider( "values", 0 ) +
	  " - $" + $( "#big-slider-range2" ).slider( "values", 1 ) );
});




$(function() {
	$( "#miles-slider-range" ).slider({
	  range: true,
	  min: 3000,
	  max: 43000,
	  values: [ 2000, 42000 ],
	  slide: function( event, ui ) {
		$( "#miles" ).val( ui.values[ 0 ] + " miles - "   + ui.values[ 1  ] + " - miles " );
	  }
	});
	$( "#miles" ).val( $( "#miles-slider-range" ).slider( "values", 0 )+ " miles - "  +
	  $( "#slider-range" ).slider( "values", 1 )+ "  miles"  );
});



$(function() {
	$( "#miles-slider-range" ).slider({
	  range: true,
	  min: 3000,
	  max: 43000,
	  values: [ 2000, 42000 ],
	  slide: function( event, ui ) {
		$( "#miles" ).val( ui.values[ 0 ] + " miles - "   + ui.values[ 1  ] + " - miles " );
	  }
	});
	$( "#miles" ).val( $( "#miles-slider-range" ).slider( "values", 0 )+ " miles - "  +
	  $( "#slider-range" ).slider( "values", 1 )+ "  miles"  );
});


$(function() {
	$( "#days-slider-range" ).slider({
	  range: true,
	  min: 0,
	  max: 80,
	  values: [ 0, 80 ],
	  slide: function( event, ui ) {
		$( "#days" ).val( ui.values[ 0 ] + " days - "   + ui.values[ 1  ] + " - days " );
	  }
	});
	$( "#days" ).val( $( "#days-slider-range" ).slider( "values", 0 )+ " days - "  +
	  $( "#days-slider-range" ).slider( "values", 1 )+ "  days"  );
});
	

	
	$(function() {
	$( "#fuel-slider-range" ).slider({
	  range: true,
	  min: 27,
	  max: 30,
	  values: [ 27, 30 ],
	  slide: function( event, ui ) {
		$( "#fuel" ).val( ui.values[ 0 ] + " mpg - "   + ui.values[ 1  ] + "  mpg " );
	  }
	});
	$( "#fuel" ).val( $( "#fuel-slider-range" ).slider( "values", 0 )+ " mpg - "  +
	  $( "#fuel-slider-range" ).slider( "values", 1 )+ "  mpg"  );
});
	
  </script>