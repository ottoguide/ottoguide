$(function (){

	// set sidebar active
	$('ul.sidebar-menu li').removeClass('active');
	$('ul.sidebar-menu li a[href="'+location.href+'"]').parents('li').addClass('active');
	if ($('ul.sidebar-menu li a[href="'+location.href+'"]').parents('.treeview').length > 0) {
		$('ul.sidebar-menu li a[href="'+location.href+'"]').parents('.treeview').addClass('active');
	}

});

function upload_picture() {
	var input = document.getElementById('upl_profile_pic');

	input.click();
	input.addEventListener('change', function (e) {
		$('#uploadForm').submit();
	})
}