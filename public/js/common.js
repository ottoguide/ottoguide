$(function(){	
	$('.btn-close, .cxm-menu a').click(function(){
		var id = $(this).attr('href');
		$('.menu-box').removeClass('show');
		if(id && id !='#'){
			$('.menu-overlay').addClass('show');			
		}
		//return false;
	});
	
	var duration = 500; 
    $(window).scroll(function() {
		var $this = $(this);
		if ($this.scrollTop() > 620) {
			//alert('OK'); 
			$('.back-to-top').fadeIn(duration);
			$('.header').addClass('bg-header');
		} else {
			$('.back-to-top').fadeOut(duration);
			$('.header').removeClass('bg-header');
		}

	});
	
	$('.back-to-top').click(function(ev){
		ev.preventDefault();
		$('html, body').animate({scrollTop: 0}, duration);
		return false;
	});
});