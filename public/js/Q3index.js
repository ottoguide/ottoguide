/*! jQuery visible 1.0.0 teamdf.com/jquery-plugins | teamdf.com/jquery-plugins/license */

$(document).ready(function(){ 

  jQuery.noConflict();

 
  $(window).scroll(function(){
    
    /* Intro */
    if($('#intro').show(true)){$('#intro').addClass('open');}
    if(!$('#intro').show(true)){$('#intro').removeClass('open');}

    /* Single Choice */
    if($('#single-choice').show(true)){$('#single-choice').addClass('open');}
    if(!$('#single-choice').show(true)){$('#single-choice').removeClass('open');}
    
    /* Multiple Text */
    if($('#multiple-text').show(true)){$('#multiple-text').addClass('open');}
    if(!$('#multiple-text').show(true)){$('#multiple-text').removeClass('open');}

    /* Multiple Choice */
    if($('#multiple-choice').show(true)){$('#multiple-choice').addClass('open');}
    if(!$('#multiple-choice').show(true)){$('#multiple-choice').removeClass('open');}

   /* Select */
    if($('#select').show(true)){$('#select').addClass('open');}
    if(!$('#select').show(true)){$('#select').removeClass('open');}

    /* Text */
    if($('#text').show(true)){$('#text').addClass('open');}
    if(!$('#text').show(true)){$('#text').removeClass('open');}

    /* Info */
    if($('#info').show(true)){$('#info').addClass('open');}
    if(!$('#info').show(true)){$('#info').removeClass('open');}

  });
  
  $('.button').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'data') ).offset().top - 175
    }, 500);
    return false;
});  
    
});