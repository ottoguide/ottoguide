@extends('layouts.web_pages')
@section('content')


<script  src="js/Questioner.js"></script>
<link rel='stylesheet' href='css/questioner2.css'>

   <link rel="stylesheet" href="css/radios-to-slider.css">

   <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	
    {{--<div class="header-margin py-4 bg-secondary">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-sm-12">--}}
                    {{--<h1> Questioner Search</h1>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

 <form id="email" action="{{url('quiz-search-submit.html')}}" method="post">
		 {!! Form::hidden('_token', csrf_token()) !!}
 
      <div class="container-fluid questioner">
      <div class="container">
	  <div class="row">
      <div class="col-md-12 col-sm-12 ">
         
		 
		 <ul>
  
		 <li id="intro" class="open">
			<p class="quote">
                @if($quiz->welcome_screen == 1 && $quiz->media_url != '')
                    <?php
                    $imgExts = array("gif", "jpg", "jpeg", "png", "tiff", "tif");
                    $url = $quiz->media_url;
                    $urlExt = pathinfo($url, PATHINFO_EXTENSION);
                    if (in_array($urlExt, $imgExts)) {
                        echo '<img src="'.$quiz->media_url.'" align="center" style="width: 420; height: 315px">';
                    }
                    else{
                        echo '<iframe width="420" height="315" src="'.$quiz->media_url.'" frameborder="0"></iframe>';
                    }

                    ?>
                @else
                    <img src="images/start.png" align="center">
                @endif

			</p> 
			<p class="quote">
                @if($quiz->welcome_screen == 1 && $quiz->welcome_msg != '')
                    {{$quiz->welcome_msg}}
                @else
			    Where you'll never get all answers right. Or will you?
                @endif
			</p>
             <p align="center" style="font-size: 14px">
                 @if($quiz->welcome_screen == 1 && $quiz->welcome_description != '')
                     {{$quiz->welcome_description}}
                 @endif
             </p>
			<p class="quote">
			<a class="button" onclick="quizRoutes(0,'{{$get_quiz_show[0]->question_id}}','start')"  align="center">
                @if($quiz->action_button_label != '' && $quiz->action_button_label != null)
                    {{$quiz->action_button_label}}
                @else
                    Start Quiz
                @endif
                {{--<span>&#9997;</span>--}}
            </a>
			</p>
		 </li>

    @php($index = 1)
    @php($quizCount = count($get_quiz_show) )
	@foreach($get_quiz_show as $get_question)

    @if($index == $quizCount)
        @php($nextQuestion = 9999999999)
    @else
        @php($nextQuestion = $get_quiz_show[$index]->question_id)
	@endif
        <?php
        if(strlen($nextQuestion) == 2){
            $nextQuestion =  "000".$nextQuestion;
        }
        else if(strlen($nextQuestion) == 3){
            $nextQuestion =  "00".$nextQuestion;
        }
        else if(strlen($nextQuestion) == 4){
            $nextQuestion =  "0".$nextQuestion;
        }



        if(strlen($get_question->question_id) == 2){
            $question_id =  "000".$get_question->question_id;
        }
        else if(strlen($get_question->question_id) == 3){
            $question_id =  "00".$get_question->question_id;
        }
        else if(strlen($get_question->question_id) == 4){
            $question_id =  "0".$get_question->question_id;
        }
        else{
            $question_id =  $get_question->question_id;
        }
        ?>
    @if($get_question->question_type == "radio" && $get_question->question_subtype == "text")


    <li id="{{$question_id}}">
   
    <p style="margin-bottom: 0.2em; color:#016475;"> @if(!empty($get_question->question_text) > 0) {{$get_question->question_text}} @endif ?</p>
        
		<div>
         
          <span>
		
		<?php $get_options = explode(',',$get_question->question_options);?>
		 
		 <input type="hidden" name="question[]" value="{{$get_question->query_param}}" class="field radio" />
		
		 @foreach($get_options  as $get_option)
		  <input name="answer[{{$get_question->question_id}}]" type="radio" value="{{$get_option}}" class="field radio" />
          <label class="choice" for="Field1_3" >
		  {{$get_option}}
           </label>
		 @endforeach
          </span>
		  
          <br>
		  
		 <a class="button" onclick="quizRoutes('{{$get_question->question_id}}','{{$nextQuestion}}','radio','{{$get_question->is_required}}')" > Continue </a> <b>press ENTER</b>

         </div>

        </li>
    
	   @endif

  
  
  
	@if($get_question->question_type == "radio" && $get_question->question_subtype == "image") 
	  
    <li id="{{$question_id}}">
    <p style="margin-bottom: 0.2em; color:#016475;"> @if(!empty($get_question->question_text) > 0) {{$get_question->question_text}} @endif ?</p>
        
	    <div>
        
		<span>
		
		<?php $get_options = explode(',',$get_question->question_options);?>
		
		<input type="hidden" name="question[]" value="{{$get_question->query_param}}" class="field radio" />
		
		@foreach($get_options  as $get_option)
		   <label>
		   <input id="radio-image" name="answer[{{$question_id}}]" value="{{$get_option}}" type="radio" class="field radio"/>
		   <img src="{{$get_option}}" width="175px">
		   </label>	&nbsp; &nbsp; &nbsp;	   
		 @endforeach
          </span>
		  
		  
          <br>
		  <a class="button" onclick="quizRoutes('{{$question_id}}','{{$nextQuestion}}','radio','{{$get_question->is_required}}')" > Continue </a> <b>press ENTER</b>
	
        </div>

      </li>
    
	 @endif
	 
	 
	 
	 @if($get_question->question_type == "yes_no") 
	  
     <li id="{{$question_id}}">
   
     <p style="margin-bottom: 0.2em; color:#016475;"> @if(!empty($get_question->question_text) > 0) {{$get_question->question_text}} @endif ?</p>
        
		<div>
         
          <span>
		
		 <?php $get_options = explode(',',$get_question->question_options);?>
		  <input  name="question[]" value="{{$get_question->query_param}}" type="hidden" class="field radio"  /> 
		 @foreach($get_options  as $get_option)
		   <input id="{{$get_question->query_param}}" name="answer[{{$question_id}}]" value="{{$get_option}}" type="radio" class="field radio"  />
		     {{$get_option}}
			 &nbsp;
		  @endforeach
          </span>
		  
		  
          <br>
		  <a class="button" onclick="quizRoutes('{{$question_id}}','{{$nextQuestion}}','radio','{{$get_question->is_required}}')" > Continue </a> <b>press ENTER</b>
	
        </div>

      </li>
    
	 @endif
	 
	 
	 
  
  @if($get_question->question_type == 'checkbox' && $get_question->question_subtype == "image")  

   <li id="{{$question_id}}">
    
   <p style="margin-bottom:1.5em; color:#016475">@if(!empty($get_question->question_text) > 0) {{$get_question->question_text}} @endif ?</p>

   
      <span>
		
    <?php $get_checkboxs = explode(',',$get_question->question_options);?>
		
	   <input  name="question[]" value="{{$get_question->query_param}}" type="hidden" class="field radio" /> 

		@foreach($get_checkboxs  as $get_checkbox_value)
		  
		   <label>
		   <input id="radio-image" name="answer[{{$question_id}}]" value="{{$get_checkbox_value}}" type="checkbox" class="field radio"/>
		   <img src="{{$get_checkbox_value}}" width="175px">
		   </label>	&nbsp; &nbsp; &nbsp;	
		  
		 @endforeach
          </span>
  

         <br>
    <a class="button" onclick="quizRoutes('{{$question_id}}','{{$nextQuestion}}','radio','{{$get_question->is_required}}')" >Continue </a> <b>press ENTER</b>

    </li>
    @endif

  
  
  
  
    @if($get_question->question_type == 'checkbox' && $get_question->question_subtype == "text")  

	
    <li id="{{$question_id}}">
    
   <p style="margin-bottom:1.5em; color:#016475">@if(!empty($get_question->question_text) > 0) {{$get_question->question_text}} @endif ?</p>

   
      <span>
		
		<?php $get_checkboxs = explode(',',$get_question->question_options);?>
	
	    <input  name="question[]" value="{{$get_question->query_param}}" type="hidden" class="field radio" /> 

		@foreach($get_checkboxs  as $get_checkbox_value)
		    <input id="{{$get_question->query_param}}" name="answer[{{$question_id}}]" value="{{$get_checkbox_value}}" type="checkbox" class="field radio" />
            <label class="choice" for="Field1_3" >
			{{$get_checkbox_value}}
            </label> &nbsp; &nbsp; &nbsp;
		 @endforeach
          </span>
  
  
         <br>
    <a class="button" onclick="quizRoutes('{{$question_id}}','{{$nextQuestion}}','radio','{{$get_question->is_required}}')">Continue </a> <b>press ENTER</b>

    
  </li>
    @endif

  
  
  
  
  @if($get_question->question_type == 'dropdown') 
 
	<li id="{{$question_id}}">
 
   <p style="margin-bottom:1.5em; color:#016475">@if(!empty($get_question->question_text) > 0) {{$get_question->question_text}} @endif ?</p>
   
    <div class="select">
       
	  <input name="question[]" value="{{$get_question->query_param}}" type="hidden" class="field radio" /> 
 
	  <select id="{{$get_question->query_param}}" name="answer[{{$question_id}}]" class="field select medium" tabindex="31" >
          
	  <?php $get_dropdown = explode(',',$get_question->question_options);?>
	

		 @foreach($get_dropdown  as $get_dropdown_value)  
		  <option id="dropdown_option">
           {{$get_dropdown_value}}
          </option>
          @endforeach
          
        </select>
      </div>
	  
	  
	  <br>
     <a class="button" onclick="quizRoutes('{{$question_id}}','{{$nextQuestion}}','dropdown','{{$get_question->is_required}}')"> Continue </a> <b>press ENTER</b>
  
	</li>
  
  
   @endif  
  
  
  
   @if($get_question->question_type == 'rating' && $get_question->question_options == '1-10' ) 
 
	<li id="{{$question_id}}">
 
    <p style="margin-bottom:0.5em; color:#016475">@if(!empty($get_question->question_text) > 0) {{$get_question->question_text}} @endif ?</p>
      
	<input name="question[]" value="{{$get_question->query_param}}" type="hidden" class="field radio" /> 
   
    <div class='starrr' id='star2'></div>
    <br />
    <input type='text' name='answer[{{$question_id}}]'  id='star2_input' />

    <br>
   <a class="button" onclick="quizRoutes('{{$question_id}}','{{$nextQuestion}}','text','{{$get_question->is_required}}')"> Continue </a> <b>press ENTER</b>
  
   </li>
  
   @endif
  
  
 
   @if($get_question->question_type == 'rating' && $get_question->question_options == '1-5') 
 
	<li id="{{$question_id}}">
 
    <p style="margin-bottom:0.5em; color:#016475">@if(!empty($get_question->question_text) > 0) {{$get_question->question_text}} @endif ?</p>
   
    <input name="question[]" value="{{$get_question->query_param}}" type="hidden" class="field radio" /> 
	
     <div class='starrr' id='star1'></div>
    <div>&nbsp;
      <span class='your-choice-was' style='display: none;'>
        Your rating was <span class='choice'></span>.
      </span>
	  <input type='text' name='answer[{{$question_id}}]'  id='star1_input' />
    </div>
    <br />

    <br>
   <a class="button" onclick="quizRoutes('{{$question_id}}','{{$nextQuestion}}','text','{{$get_question->is_required}}')"> Continue </a> <b>press ENTER</b>
  
   </li>
  
  
  @endif
 
 
 
    @if($get_question->question_type == 'scale') 
 
		<li id="{{$question_id}}">
 
		<p style="margin-bottom:0.5em; color:#016475">@if(!empty($get_question->question_text) > 0) {{$get_question->question_text}} @endif ?</p>

		

    	<div id="radios">
    				
		<?php $get_opinions = explode(',',$get_question->question_options); $si=1;?>
		 
		 <input name="question[]" value="{{$get_question->query_param}}" type="hidden" class="field radio" /> 
		 
		 @foreach($get_opinions  as $get_opinion)  
		 <input id="option{{$si++}}" name="answer[{{$question_id}}]" value="{{$get_opinion}}" type="radio">
         <label><br> {{$get_opinion}} </label>
          @endforeach	
				
    	</div>

     
		<br>
		<a class="button" onclick="quizRoutes('{{$question_id}}','{{$nextQuestion}}','radio','{{$get_question->is_required}}')"> Continue </a> <b>press ENTER</b>
  
		</li>
  
  
	@endif
 
 
 
  @if($get_question->question_type == 'date') 
 
	<li id="{{$question_id}}">
 
	<input name="question[]" value="{{$get_question->query_param}}" type="hidden" class="field radio" /> 
	
    <p style="margin-bottom:0.5em; color:#016475">@if(!empty($get_question->question_text) > 0) {{$get_question->question_text}} @endif ?</p>
		<label class="choice" for="Field1_3" >
			Select Date:</label>   
	<input id="{{$get_question->query_param}}" name="answer[{{$question_id}}]" type="date" class="form-control" style="width: 30%;" />
     

    <br>
   <a class="button" onclick="quizRoutes('{{$question_id}}','{{$nextQuestion}}','text','{{$get_question->is_required}}')"> Continue </a> <b>press ENTER</b>
  
   </li>
  
  
  @endif
 
 
 
 
  @if($get_question->question_type == 'number') 
 
	<li id="{{$question_id}}">
 
 	<input name="question[]" value="{{$get_question->query_param}}" type="hidden" class="field radio" /> 

 
    <p style="margin-bottom:0.5em; color:#016475">@if(!empty($get_question->question_text) > 0) {{$get_question->question_text}} @endif ?</p>
		<label class="choice" for="Field1_3" >
			Select Number:</label>   
	 <input id="{{$get_question->question_id}}" name="answer[{{$question_id}}]" type="number" class="form-control" style="width: 30%;"/>
      <br>
     <a class="button" onclick="quizRoutes('{{$question_id}}','{{$nextQuestion}}','text','{{$get_question->is_required}}')"> Continue </a> <b>press ENTER</b>
  
   </li>
  
  
  @endif

        @php($index++)
  @endforeach

             <li id="9999999999">


                 <p class="quote">
                     @if($quiz->welcome_screen == 1 && $quiz->thankyou_msg != '')
                         {{$quiz->thankyou_msg}}
                     @else
                         Thank you for talking the Quiz, To see the result click on Submit button
                     @endif
                 </p>
                 <p class="quote">
                     <input type="submit" value ="Submit" class="button" align="center">
                 </p>

                 <p class="quote">
                         @if($quiz->welcome_screen == 1 && $quiz->facebook_link != '')
                             <a href="{{$quiz->facebook_link}}" target="_blank"><i class="fa fa-facebook"></i></a>
                         @endif
                         @if($quiz->welcome_screen == 1 && $quiz->twitter_link != '')
                             <a href="{{$quiz->twitter_link}}" target="_blank"><i class="fa fa-twitter"></i></a>
                         @endif
                         @if($quiz->welcome_screen == 1 && $quiz->google_link != '')
                             <a href="{{$quiz->google_link}}" target="_blank"><i class="fa fa-google"></i></a>
                         @endif
                         @if($quiz->welcome_screen == 1 && $quiz->linkedin_link != '')
                             <a href="{{$quiz->linkedin_link}}" target="_blank"><i class="fa fa-linkedin"></i></a>
                         @endif
                         @if($quiz->welcome_screen == 1 && $quiz->youtube_link != '')
                             <a href="{{$quiz->youtube_link}}" target="_blank"><i class="fa fa-youtube"></i></a>
                         @endif
                         @if($quiz->welcome_screen == 1 && $quiz->instagram_link != '')
                             <a href="{{$quiz->instagram_link}}" target="_blank"><i class="fa fa-instagram"></i></a>
                         @endif
                         @if($quiz->welcome_screen == 1 && $quiz->pinterest_link != '')
                             <a href="{{$quiz->pinterest_link}}" target="_blank"><i class="fa fa-pinterest"></i></a>
                         @endif
                 </p>


             </li>

         </ul>
      </div>
      </div>
      </div>

      </div>

 </form>
  




<script src='js/starr-min.js'></script>

  <script>
  
	 $('#star1').starrr({
      change: function(e, value2){
        if (value2) {
          $('.your-choice-was').show();
          $('.choice').text(value2);
        } else {
          $('.your-choice-was').hide();
        }
      }
    });
	
	
	
    var $s2input = $('#star2_input');
    $('#star2').starrr({
      max: 10,
      rating: $s2input.val(),
      change: function(e, value){
        $s2input.val(value).trigger('#star2_input');
      }
    });
	
	// opinion 
	$(document).ready(function() {

    $("#radios").radiosToSlider();


	});
	
	
	$('#radios').radiosToSlider({
    animation: true,
});
	

	function quizRoutes(question_id,next_question_id,type,is_required) {

	    if(type == 'start'){

            $('html, body').animate({
                scrollTop: $("#"+next_question_id).offset().top - 175
            }, 1000);
            $("#"+next_question_id).addClass('open');

        }
        else{
            var answer = '';

            if(type == 'text'){
                console.log($('#'+question_id).children('input[name="answer['+question_id+']"]').val());
                answer = $('#'+question_id).children('input[name="answer['+question_id+']"]').val();
            }

            if(type == 'radio'){
                console.log($('input[name="answer['+question_id+']"]:checked').val());
                answer = $('input[name="answer['+question_id+']"]:checked').val();
            }

            if(type == 'dropdown'){
                console.log($('select[name="answer['+question_id+']"]').val());
                answer = $('select[name="answer['+question_id+']"]').val();
            }

            if(is_required == 1 && (answer == '' || answer == undefined)){

                alert('This question is required!')

            }
            else{
                $.ajax({
                    url: '/check_route',
                    type: 'POST',
                    data: {_token: "{{ csrf_token() }}", quiz_id: "{{$quiz->id}}", question_id: question_id, answer:answer},
                    dataType: 'JSON',
                    success: function (response) {
                        console.log(response)
                        if(response.status){
                            console.log(response.next_question);
                            $('html, body').animate({
                                scrollTop: $("#"+response.next_question).offset().top - 175
                            }, 1000);
                            $("#"+response.next_question).addClass('open');
                        }
                        else{
                            console.log('no route found');
                            $('html, body').animate({
                                scrollTop: $("#"+next_question_id).offset().top - 175
                            }, 1000);
                            $("#"+next_question_id).addClass('open');
                        }

                    }
                });
            }



        }

    }

  </script>

  
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-visible/1.2.0/jquery.visible.js'></script>

 <script src="http://localhost/scale/js/jquery-1.10.2.min.js"></script>
	<script src="js/jquery.radios-to-slider.js"></script>
	<script>
	$(document).ready(function() {
    $("#radios").radiosToSlider();
	});

	</script>








	
	
@endsection 
