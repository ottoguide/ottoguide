<?php
require __DIR__ . '/vendor/autoload.php';

use Twilio\Rest\Client;

$ACCOUNT_SID = "AC85a8fe913314e6049feff220c4095dcb";
$TOKEN = "4f08d3307d1e8812cb07dbf70c45d808";
$TWILIO_NUMBER = "+15104803765";
$user_phone = '';
$dealer_phone = "";

$dealer_id = "";
if (isset($_REQUEST['dealer_id'])) {
    $dealer_id = $_REQUEST['dealer_id'];
}

$other_phone = "";
if (isset($_REQUEST['other_phone'])) {
    $other_phone = $_REQUEST['other_phone'];
}

if (isset($_REQUEST['user_phone'])) {
    if (strlen(preg_replace('/[^0-9]/', '', $_REQUEST['user_phone'])) == 10) {
        $user_phone = "1" . preg_replace('/[^0-9]/', '', $_REQUEST['user_phone']);
    } else if (strlen(preg_replace('/[^0-9]/', '', $_REQUEST['user_phone'])) == 11) {
        $user_phone = preg_replace('/[^0-9]/', '', $_REQUEST['user_phone']);
    }
}

if (isset($_REQUEST['dealer_phone'])) {

    if (strlen(preg_replace('/[^0-9]/', '', $_REQUEST['dealer_phone'])) == 10) {
        $dealer_phone = "1" . preg_replace('/[^0-9]/', '', $_REQUEST['dealer_phone']);
    } else if (strlen(preg_replace('/[^0-9]/', '', $_REQUEST['dealer_phone'])) == 11) {
        $dealer_phone = preg_replace('/[^0-9]/', '', $_REQUEST['dealer_phone']);
    }

}


$protocol = 'http';
if (isset($_SERVER['HTTPS'])) {
    $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
}
$url = $protocol . "://" . $_SERVER['SERVER_NAME'];


// Create authenticated REST client using account credentials in
$client = new Client($ACCOUNT_SID, $TOKEN);

try {

    $call = $client->calls->create(
        "+" . $user_phone, // Visitor's phone number
        $TWILIO_NUMBER, // A Twilio number in your account
        array(
            "url" => $url . "/twilio-call-chat/outbound.php?dealer_phone=$dealer_phone&dealer_id=$dealer_id&other_phone=$other_phone"
        )
    );

} catch (Exception $e) {

    echo json_encode(array('status' => false, 'error' => $e));
    die;

}

// return a JSON response
echo json_encode(array('status' => true, 'call_sid' => $call->sid));
die();
