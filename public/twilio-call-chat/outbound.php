<?php
// Require the bundled autoload file - the path may need to change
// based on where you downloaded and unzipped the SDK

ini_set('display_errors', 0);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

require __DIR__ . '/vendor/autoload.php';
use Twilio\Rest\Client;

header('Content-Type: text/xml');



$dealer_primary_phone = $_REQUEST['dealer_phone'];
$dealer_id = $_REQUEST['dealer_id'];
$other_phone = $_REQUEST['other_phone'];
$sayMessage = 'Thanks for contacting us. Our dealer will take your call.';


$protocol = 'http';
if (isset($_SERVER['HTTPS'])) { $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http"; }
$url = $protocol . "://" . $_SERVER['SERVER_NAME'];


$all_phones[] = $dealer_primary_phone;
if($other_phone != '') {
    $dealer_phone = explode(",", $other_phone);

    if(!is_array($dealer_phone)){
        $dealer_phone = array($other_phone);
    }


    foreach ($dealer_phone as $d_phone){

        if (strlen(preg_replace('/[^0-9]/', '', $d_phone)) == 10) {
            $d_phone = "1" . preg_replace('/[^0-9]/', '', $d_phone);
        }
        else if (strlen(preg_replace('/[^0-9]/', '', $d_phone)) == 11) {
            $d_phone = preg_replace('/[^0-9]/', '', $d_phone);
        }

        $all_phones[] = $d_phone;
    }

}


$twiml = new Twilio\Twiml();
$twiml->say($sayMessage, array('voice' => 'alice'));
$dial = $twiml->dial('');

foreach ($all_phones as $phone){
    $dial->number('+' . $phone);
}



print $twiml;