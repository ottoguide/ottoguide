// Define app configuration in a single location, but pull in values from
// system environment variables (so we don't check them in to source control!)
module.exports = {
    //flag to get Twilio Account Setting from this file or from environment
    // If set to 'Y' , the values are read from config.js and not from environment
    getTwiliAccountSettingsfromFile: 'N',


    // Your primary Twilio Account SID
    accountSid: 'AC85a8fe913314e6049feff220c4095dcb',

    // API Key/Secret Pair - generate a pair in the console
    apiKey: 'SK7663354378544866f5edfdd679e68854',
    apiSecret: '9t9C9cU9UoeOP55fjuUrFBVuAVfdET1h',

    // Your Chat service instance SID
    serviceSid: 'IScb72065876ab4a76ac43687187c1dd50',

    // Defines whether or not this application is deployed in a production
    // environment
    nodeEnv: process.env.NODE_ENV || 'development',

    // In production, this is the base host name for web app on the public
    // internet, like "jimmysbbq.herokuapp.com".  This should be the same host
    // you use in your Twilio Number config for voice or messaging URLs
    host: process.env.HOST || 'localhost',

    // The port your web application will run on
    port: process.env.PORT || 3000

};