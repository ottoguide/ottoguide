<!DOCTYPE html>
<html lang="en">
  <?php include('include/head.php'); ?>
  </head>

  <body>
    <?php include('include/header.php'); ?>
    <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <h1>Mitsubishi Carisma 1.9 MIRAGE DI-D 5d 101 BHP</h1>            
          </div>
        </div>
      </div>      
    </div>
    
    <div class="py-5">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
          <?php if(!FALSE){ ?>
          <div id="cxm-sliderPro" class="slider-pro">
            <div class="sp-slides">
              <?php $img = 0; for($i = 1; $i < 15; $i++){ ?>
              <div class="sp-slide">
                <img class="sp-image" src="media/car-<?php echo $img; ?>.jpg" 
                    data-src="media/car-<?php echo $img; ?>.jpg" 
                    data-small="media/car-<?php echo $img; ?>.jpg"
                    data-medium="media/car-<?php echo $img; ?>.jpg"
                    data-large="media/car-<?php echo $img; ?>.jpg"
                    data-retina="media/car-<?php echo $img; ?>.jpg"/>
              </div>
              <?php if($img > 3){$img = 0;}else{$img++;} 
			  } ?>
            </div>
  
            <div class="sp-thumbnails">
              <?php $img = 0; for($i = 1; $i < 15; $i++){ ?>
              <img class="sp-thumbnail" src="media/car-<?php echo $img; ?>.jpg"/>
              <?php if($img > 3){$img = 0;}else{$img++;}
			  } ?>
            </div>
    	  </div>
          <?php } ?>   
          </div>
          <div class="col-md-4">
            <div class="bg-secondary p-3">
              <ul class="cxm-facts text-center">
                <li><span class="fa fa-modx text-primary"></span> 2014</li>
                <li><span class="fa fa-barcode text-primary"></span> SUV</li>
                <li><span class="fa fa-road text-primary"></span> 65,658 miles</li>
                <li><span class="fa fa-toggle-on text-primary"></span> Manual</li>
                <li><span class="fa fa-spinner text-primary"></span> 0.2L</li>
                <li><span class="fa fa-fire text-primary"></span> Diesel</li>
              </ul>
              
              <p class="fs14">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam efficitur, erat ac malesuada condimentum, quam lorem faucibus eros, vitae vehicula velit massa quis massa. Sed lacus ante, semper et velit sit amet, blandit laoreet ante. Etiam rhoncus lacus erat, ut aliquet orci faucibus ut. Cras consectetur sem sit amet neque pretium rhoncus.</p>
              
              <div class="row">
                <div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="#"><span class="fa fa-phone"></span> Phone For Otto Price</a></div>
              </div>
              <div class="row">
                <div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="#"><span class="fa fa-mobile"></span> SMS For Otto Price</a></div>
              </div>
              <div class="row">
                <div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="#"><span class="fa fa-envelope"></span> Email For Otto Price</a></div>
              </div>
              <div class="row">
                <div class="col-12"><a class="btn btn-primary btn-block" href="#"><span class="fa fa-comments"></span> Chat For Otto Price</a></div>
              </div>  
            </div>
          </div>
        </div>
      </div>      
    </div>
    
	<?php include('include/footer.php'); ?>
    <?php include('include/footer-scripts.php'); ?>
    <script type="text/javascript" src="js/jquery.sliderPro.min.js"></script>
	<script type="text/javascript">
	$( document ).ready(function( $ ) {
		$( '#cxm-sliderPro' ).sliderPro({
			width: 960,
			height: 500,
			fade: false,
			arrows: true,
			buttons: false,
			fullScreen: true,
			shuffle: !true,
			smallSize: 500,
			mediumSize: 1000,
			largeSize: 3000,
			thumbnailArrows: true,
			autoplay: true
		});
	});
    </script>    
  </body>
</html>
