@extends('layouts.web_pages')
@section('content')
    <style>

        .box-eq {
            color: #000;
            text-transform: capitalize;
            width: 700px;
            padding: 10px;
            border-top: 3px #158CBA solid;
            margin-bottom: 3px;
            box-shadow: 0px 0px 5px #999;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            background-color: #fff;
            margin-top: 3px;

        }

        .box-eq:hover {
            color: #000;
        }
    </style>




    <div class="header-margin py-4 bg-secondary">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1> Questioner Search</h1>
                </div>
            </div>
        </div>
    </div>



    <div id="cxm-home-slider" class="carousel slide" data-ride="carousel">

        <div class="cxm-slider-content">
            <div class="container py-5">
                <h2 class="question_text text-center" style="margin-bottom: 20px"></h2>
                <div class="row justify-content-center">
					<?php
					if (isset($question)) {
						$options = json_decode($question->option);
						//echo '<pre>';
						//print_r($options);
						//echo '</pre>';
						function _find_unique_id($id, $uid, $questions) {
							if ($uid == 'top') {
								echo '<script>$(".question_text").html("' . $questions[0]->question . '")</script>';
								foreach ($questions[0]->answer as $answer) {
									if ($answer->answer_type == 'search') {
										echo '<div class="col-sm-8 col-lg-8 text-center">
                                    <a class="box-eq 3" href="' . $answer->search_url . '">' . $answer->answer_text . '</a>
                                    </div>';
									} else {
										echo '<div class="col-sm-8 col-lg-8 text-center">
                                    <a class="box-eq 3" href="http://otto-guide.ethnicjob.ca/faq-search.html?id=' . $id . '&unique_id=' . $answer->unique_id . '">' . $answer->answer_text . '</a>
                                    </div>';
									}
								}
							} else {
								foreach ($questions as $ques) {
									if ($ques->unique_id == $uid) {
										_print_answers($ques->answer, $id, $ques->question);
									} elseif ($ques->answer) {
										foreach ($ques->answer as $ans) {
											if ($ans->answer_type == 'question') {
												if ($ans->unique_id == $uid) {
													_print_questions($ans->questions, $id, $ans->answer_text);
												} else {
													_find_unique_id($id, $uid, $ans->questions);
												}
											}
										}
									}
								}
							}
						}

						function _print_questions($questions, $id, $text) {
							echo '<script>$(".question_text").html("' . $text . '")</script>';
							foreach ($questions as $question) {
								echo '<div class="col-sm-8 col-lg-8 text-center">
                                    <a class="box-eq 3" href="http://otto-guide.ethnicjob.ca/faq-search.html?id=' . $id . '&unique_id=' . $question->unique_id . '">' . $question->question . '</a>
                                    </div>';
							}
						}

						function _print_answers($answers, $id, $question) {
							echo '<script>$(".question_text").html("' . $question . '")</script>';
							foreach ($answers as $answer) {
								if ($answer->answer_type == 'search') {
									echo '<div class="col-sm-8 col-lg-8 text-center">
                                    <a class="box-eq 3" href="' . $answer->search_url . '">' . $answer->answer_text . '</a>
                                    </div>';
								} else {
									echo '<div class="col-sm-8 col-lg-8 text-center">
                                    <a class="box-eq 3" href="http://otto-guide.ethnicjob.ca/faq-search.html?id=' . $id . '&unique_id=' . $answer->unique_id . '">' . $answer->answer_text . '</a>
                                    </div>';
								}
							}
						}

						_find_unique_id($id, $unique_id, $options);
					}
					?>
                </div>
            </div>
        </div>

        <div class="carousel-inner">

            <div class="carousel-item active">
                <img class="d-block w-100" src="<?= url('images/slide-1-light.jpg') ?>"
                     alt="Third slide">
            </div>
        </div>

    </div>

















@endsection 
