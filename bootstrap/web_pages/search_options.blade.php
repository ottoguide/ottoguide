@extends('layouts.web_pages')
@section('content') 
    
<div id="cxm-home-slider" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#cxm-home-slider" data-slide-to="0" class=""></li>
        <li data-target="#cxm-home-slider" data-slide-to="1" class=""></li>
        <li data-target="#cxm-home-slider" data-slide-to="2" class="active"></li>
      </ol>
      <div class="cxm-slider-content">
        <div class="container py-5">
          <div class="row justify-content-center">
            <div class="col-sm-6 col-lg-5 text-center">
              <a class="box-eq bg-trans2" href="/search-car.html"><img class="img-fluid" src="/images/search-car.png"><br>I Know Find My Perfect Car</a>
            </div>          
            <div class="col-sm-6 col-lg-5 offset-lg-1 text-center">
              <a class="box-eq bg-trans3" href="/faq-search.html"><img class="img-fluid" src="/images/search-car.png"><br>I Don't Know Find My Perfect Car</a>
            </div>
          </div>
        </div>
      </div>
        
      <div class="carousel-inner">
      
        <div class="carousel-item active">
          <img class="d-block w-100" src="/images/slide-2.jpg" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#cxm-home-slider" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"><span class="fa fa-chevron-left"></span></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#cxm-home-slider" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"><span class="fa fa-chevron-right"></span></span>
        <span class="sr-only">Next</span>
      </a>
    </div>	  
	  
	  
	  
	  
	  
	@endsection 