  @if(!empty($all_car_datas))    
        <div class="row">
          <div class="col-sm-12">
            <div class="cxm-tabs">             
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="overview" role="tabpanel">
                <h3 align="center"> Your have selected {{count($all_car_datas)}} cars.</h3>                  
              <div class="form-row">
                <!-- START FOREACH -->          
                    @foreach($all_car_datas as $no => $data)  
                    <div class="col-sm-12 col-md-12 col-lg-12">            
                      <div class="cxm-vihecle-box card border-secondary mb-3">
                      <div class="cxm-img">
                        <div class="cxm-img-badge-cls"></div>     
                          <div class="cxm-img-badge position-absolute"></div>                            
                            <img style="width: 50% !important; " class="img-fluid" src="@if(!empty($data->media->photo_links[0]) > 0)
                            {{$data->media->photo_links[0]}}
                            @else
                            {{asset('public/images/no-image.jpeg')}}
                            @endif">                          
                      </div> 
                      <div class="text-center lh16">
                        <b> @if(!empty($data->heading) > 0)
                          {{$data->heading}}
                          @endif
                        </b>
                        </div>    
                        <div class="card-body p-2">                    
                          <hr class="hr1">
                          <div class="form-row">
                            <div class="col-6 mb-2 border-right">
                              <span class="badge badge-secondary">Price</span>
                              <div><span class="fc1 fs24">
                                  $ @if(!empty($data->price) > 0)
                                  {{number_format((float)$data->price)}}
                                  @endif
                                  </span></div>
                            </div>
                            <div class="col-6 mb-2 text-right">
                             <span class="badge badge-secondary">MILES</span>
                             <div><span class="fc1 fs24">
                               @if(!empty($data->miles) > 0)
                               {{$data->miles}}
                               @endif
                               </span></div>
                            </div>
                          </div>                        
                        </div>
                      </div>          
                    </div>
                  <!-- END FOREACH -->                                         
                  @endforeach
                  <a class="btn btn-primary btn-block pull-right" href="{{route('CarComparePage')}}">Compare Car</a>
                  @endif
                          </div>
                        </div>
              </div>
            </div>
          </div>
        </div>                
     