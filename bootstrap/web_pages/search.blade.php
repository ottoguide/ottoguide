@extends('layouts.web_pages')
@section('content')
	<link href="{{asset('public/css/rating.css')}}" rel="stylesheet"> 	

    <style>
        p {
            margin-bottom: 0.3rem;
        }
        select :disabled.red-option{
            color: #d8d3d3;
        }
        .img-fixed img {
            height: 250px;
            width: 100%;
        }

        .img-fixed2 img {
            height: 178px;
            width: 100%;
        }
        .cxm-detail-border{
            border-top: 3px #158CBA solid;
            padding: 10px;
            margin-bottom: 35px;
            box-shadow: 0px 0px 5px #999;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
            margin:8px;
        }

        .cxm-pro-list .carousel-item .img-fluid {
            height: 150px;
            width: 100%;
        }
		
		.autosearch,.autosearchbutton{
		display:none;	
		}
		
	.modal-header-primary {
	color:#fff;
    padding:9px 15px;
    border-bottom:1px solid #eee;
    background-color: #428bca;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
	}
		
	.rating-title{
		font-size:15px;
		color:#fff;	
		font-weight: bold;
		padding-left:10px;		
	}	
	
	
	.incoming_msg_img {
	  display: inline-block;
	  width: 6%;
	}
	.incoming_msg{	
	  margin-top:2%;	
	}
	.outgoing_msg_img {
	  display: inline-block;
	  width: 6%;
	  float: right;
	  padding-left: 5px;
	}

	 .received_msg {
	  display: inline-block;
	  display: inline-block;
	  padding: 0 0 0 10px;
	  vertical-align: top;
	   margin-left: 4%;
	 }

	 
	.input_msg_write input {
	  background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
	  border: medium none;
	  color: #2c2c2c;
	  font-size: 14px;
	  padding:5px;
	  min-height: 40px;
	  width: 100%;
	}
	 
   .success-box {
    margin: 0px 0;
    padding: 0px 0px;
    border: 0px solid #eee;
    font-size: 16px;
	} 
		
	.rating-stars{
     padding-left: 15px;	
	}	 

	input:-webkit-autofill, textarea:-webkit-autofill, select:-webkit-autofill {
    background-color: #fff !important;
    background-image: none !important;
    color: rgb(0, 0, 0) !important;
	}
	
	.type_msg {border-bottom: 0px solid #c4c4c4;  5px; padding:5px; position: relative;} 

	.title_msg,.write_msg {
	 font-size: 16px;	
	} 

	.success-box {
    margin: 0px 0;
    padding: 0px 0px; 
    border: 0px solid #eee;
}
	
	
    </style>
	


<?php $view =(isset($_GET['view']) ? $_GET['view'] : 1 );?>

    <div class="header-margin">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card refine-search">
                        <div class="card-header font-weight-bold"><a data-toggle="collapse" href="#refineSearch" role="button" aria-expanded="false" aria-controls="refineSearch">Select Features that you would like in your car</a></div>
                        <div class="collapse show" id="refineSearch">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        Vehicle Condition:
                                        <div class="custom-control custom-checkbox mt-2 fs14 d-inline-block mx-1">
                                            <input type="checkbox" name="car_type" value="new" class="custom-control-input chkBox" id="customCheckvc1">
                                            <label class="custom-control-label" for="customCheckvc1">New</label>
                                        </div>
                                        <div class="custom-control custom-checkbox mt-2 fs14 d-inline-block mx-1">
                                            <input type="checkbox" name="car_type" value="used" class="custom-control-input chkBox" id="customCheckvc2">
                                            <label class="custom-control-label" for="customCheckvc2">Used</label>
                                        </div>
                                        <div class="custom-control custom-checkbox mt-2 fs14 d-inline-block mx-1">
                                            <input type="checkbox" name="car_type" value="certified" class="custom-control-input chkBox" id="customCheckvc3">
                                            <label class="custom-control-label" for="customCheckvc3">Certified</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3 text-left text-md-right">
                                        Find me a Car within
                                        <select class="form-control form-control-sm w-auto d-inline-block mb-1" id="radius_select" onchange="$('#radius').val($(this).val());">
                                            <option value="">Any distance</option>
                                            <option value="10">10 miles</option>
                                            <option value="20">20 miles</option>
                                            <option value="30">30 miles</option>
                                            <option value="40">40 miles</option>
                                            <option value="50">50 miles</option>
                                        </select>
                                        of
                                        <a data-toggle="collapse" href="#location-menu" id="geo_ip"></a>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-sm-3 mb-3">
                                        <select onchange="load_param($(this).val());" id="main_select" class="form-control form-control-sm">
                                            <option value="main_category">-Select Feature Category-</option>
                                            {{--<option value="Country">Country</option>--}}
                                            <option value="Price">Price</option>
                                            <option value="Miles">Miles</option>
                                            <option value="Year">Year</option>
                                            <option value="Make">Make</option>
                                            <option value="Engine">Engine</option>
                                            <option value="Body_type">Body Type</option>
                                            <option value="Body_subtype">Body Sub Type</option>
                                            <option value="Transmission">Transmission</option>
                                            <option value="Seller_type">Seller Type</option>
                                            <option value="Trim">Trim</option>
                                            <option value="Drivetrain">Drive Train</option>
                                            <option value="Fuel">Fuel</option>
                                            <option value="Doors">Doors</option>
                                            <option value="Exterior_color">Exterior Color</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select id="sub_select" disabled="disabled" onchange="load_param($(this).val(),'model');" class="form-control form-control-sm">
                                            <option>-Select Feature Category First-</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2 model-div" >
                                        <select style="display:none;" id="model_select" disabled="disabled" class="form-control form-control-sm">
                                            <option value="">-Select Model-</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3 mb-3 text-center">
									<!--
                                        <div class="custom-control custom-checkbox mt-2 fs14 d-inline-block">
                                            <input type="checkbox" class="custom-control-input" id="customCheckmh1">
                                            <label class="custom-control-label" for="customCheckmh1"><span class="d-sm-none d-md-inline-block">Must Have</span> Feature</label>
                                        </div>
									-->	
                                    </div>
                                    <div class="col-sm-2 mb-3">
                                        <button  onclick="update_tag($('#main_select').val(),$('#sub_select').val());" class="btn btn-primary btn-block autosearch">
										<span class="fa fa-plus-circle"></span> Add <span class="d-sm-none d-md-inline-block">this</span> feature</button>
                                    </div>
                                </div>
                                <div class="tag-div form-row">
                                    <div class="col-md-2 fs18 font-weight-bold lh18 d-flex align-items-center">Selected Features:</div>
                                    <div class="col-md-10">
                                        <input type="hidden" id="search_tage" data-role="tagsinput">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="row">
                                    <div class="col-sm-6 text-center text-sm-left mb-3 mb-sm-0">
                                        <a href="javascript:;" data-toggle="modal" data-target="#save_search_model" class="btn btn-primary">Save This Search</a>
                                    </div>
                                    <div class="col-sm-6 text-center text-sm-right">
                                        <a href="<?=url('/search-car.html')?>" class="btn btn-primary">Clear</a>
                                        <form method="GET" action="" style="display: none;" id="searh-form">
                                            <input type="hidden" class="form-field" name="price" id="search_Price">
                                            <input type="hidden" class="form-field" name="country" id="search_Country">
                                            <input type="hidden" class="form-field" name="miles" id="search_Miles">
                                            <input type="hidden" class="form-field" name="year" id="search_Year">
                                            <input type="hidden" class="form-field" name="make" id="search_Make">
                                            <input type="hidden" class="form-field" name="model" id="search_Model">
                                            <input type="hidden" class="form-field" name="door" id="search_Doors">
                                            <input type="hidden" class="form-field" name="drivetrain" id="search_Drivetrain">
                                            <input type="hidden" class="form-field" name="exterior_color" id="search_Exterior_color">
                                            <input type="hidden" class="form-field" name="engine" id="search_Engine">
                                            <input type="hidden" class="form-field" name="body_type" id="search_Body_type">
                                            <input type="hidden" class="form-field" name="body_subtype" id="search_Body_subtype">
                                            <input type="hidden" class="form-field" name="transmission" id="search_Transmission">
                                            <input type="hidden" class="form-field" name="seller_type" id="search_Seller_type">
                                            <input type="hidden" class="form-field" name="trim" id="search_Trim">
                                            <input type="hidden" class="form-field" name="drivetrain" id="search_Drivetrain">
                                            <input type="hidden" name="filter_search" value="filter">
											<?php
											if(isset($_GET['filter_search'])){
											foreach ($_GET as $key => $input) {
											if(stripos($key, '-fea') !== false){ ?>
                                            <input type="hidden" name="<?=$key?>" value="1" id="<?=$key?>">
											<?php } } } ?>
                                            <input type="hidden" name="radius" id="radius">
                                            <input type="hidden" name="car_type" id="car_type">
                                            <input type="hidden" name="latitude" id="latitude">
                                            <input type="hidden" name="longitude" id="longitude">
                                            <input type="hidden" name="zip" id="zip">
                                            <input type="hidden" name="per_page" id="per_page_field">
                                            <input type="hidden" name="view" id="view_field">
                                        </form>
                                        <button type="submit" onclick="$('#searh-form').submit();" class="btn btn-primary autosearchbutton">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="py-5">
        <div class="container">
            @if(isset($_GET['filter_search']))
                <div class="message" width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>

                <div class="message" width="50%" align="center">
                    @if (session('error'))
                        <div class="alert alert-danger" width="50%">
                            {{ session('error') }}
                        </div>
                    @endif
                </div>
				<?php if($all_cars->total() != 0 ) { ?>
				<?php $pg_url = str_replace(array('?view=1','?view=2','?view=3','&view=1','&view=2','&view=3'),array("","","","","",""), $_SERVER['REQUEST_URI']);?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="cxm-view-icon text-center text-sm-left mb-2 mb-sm-0">
                            <a onclick="$('#view_field').val(1);" class="{{(($view == 1) ? 'active' : '')}}" href="<?=$pg_url;?><?=((stripos($pg_url,'?') === false) ? '?' : '&');?>view=1"><span class="fa fa-th-large"></span>
                            </a>
                            <a onclick="$('#view_field').val(2);" class="{{(($view == 2) ? 'active' : '')}}" href="<?=$pg_url;?><?=((stripos($pg_url,'?') === false) ? '?' : '&');?>view=2"><span class="fa fa-list"></span>
                            </a>
                            <a onclick="$('#view_field').val(3);" class="{{(($view == 3) ? 'active' : '')}}" href="<?=$pg_url;?><?=((stripos($pg_url,'?') === false) ? '?' : '&');?>view=3"><span class="fa fa-th"></span>
                            </a>
                            <!--<a href="#"><span class="fa fa-list-ul"></span></a>-->
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="cxm-sort text-center text-sm-right">
                            Sort By:
                            <select onchange="updateQueryStringParameter(document.URL,'sort_by',this.value)" class="form-control form-control-sm w-auto d-inline-block rounded-0" id="sort_select">
                                <option value="price-desc">Price - Highest</option>
                                <option value="price-asc">Price - Lowest</option>
                                <option value="miles-desc">Miles - Highest</option>
                                <option value="miles-asc">Miles - Lowest</option>
                                <option value="year-desc">Year - Highest</option>
                                <option value="year-asc">Year - Lowest</option>
                            </select>
                            Per Page:
                            <select id="per_page" onchange="updateQueryStringParameter(document.URL ,'per_page',this.value);" class="form-control form-control-sm w-auto d-inline-block rounded-0">
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                                <option value="50">50</option>
                            </select>
                            Page {{ isset($_GET['page']) ? $_GET['page'] : 1 }}  of {{$all_cars->lastPage()}}
                        </div>
                    </div>
                </div>
                <hr>
				<?php } ?>
                <div class="card bg-light">
                    <div class="card-body p-2 text-center ">
                        @if($all_cars->total() == 0)
                            <div style="width:30%;margin: 0 auto;" class="fs18"><span class="fa24">Result not found:</span> Sorry, but nothing matched your search term please try again with different keywords</div>
                        @else
                            <span class="font-weight-bold fs18">Found:</span> {{number_format((float)$all_cars->total())}} records
                        @endif
                    </div>
                </div>
                <hr>
                <!-- TEST WORK -->
				<?php
				if($all_cars){
				$email_address = "syedshaharif@gmail.com";
				$numOfCols = 2;
				$rowCount = 0;
				$bootstrapColWidth = 12 / $numOfCols;
				if($view ==1){
					echo '<div class="cxm-pro-list">';
					echo'<div class="row no-gutters">';
				}
				if($view ==3){
					echo '<div class="cxm-pro-grid">';
					echo'<div class="row">';
				}
				foreach ($all_cars as $i => $car) { ?>

				<?php if($view ==3){?>
					
				@include('web_pages.includes.view3')
				
				<?php } ?>
				
				<?php if($view ==1){?>
                
				@include('web_pages.includes.view1')

				<?php } ?>
				<?php if($view ==2){?>
                
				@include('web_pages.includes.view2')
				
				<?php } ?>


				<?php
				$dealerRecord = $dealer_model::findOrNew($car->dealer->id);
				$dealerRecord->dealer_id = $car->dealer->id;
				$dealerRecord->dealer_latitude = (isset($car->dealer->latitude) ? $car->dealer->latitude : '');
				$dealerRecord->dealer_longitude = (isset($car->dealer->longitude) ? $car->dealer->longitude : '');
				$dealerRecord->dealer_country = (isset($car->dealer->country) ? $car->dealer->country : '');
				$dealerRecord->dealer_website = (isset($car->dealer->website) ? $car->dealer->website : '');
				if (!$dealerRecord->exists) {
					$dealerRecord->dealer_name = $car->dealer->name;
					$dealerRecord->dealer_email = $email_address;
					$dealerRecord->dealer_phone = (isset($car->dealer->phone) ? $car->dealer->phone : '');
				}
				$dealerRecord->dealer_street = (isset($car->dealer->street) ? $car->dealer->street : '');
				$dealerRecord->dealer_city = (isset($car->dealer->city) ? $car->dealer->city : '');
				$dealerRecord->dealer_zip = (isset($car->dealer->zip) ? $car->dealer->zip : '');
				$dealerRecord->save();


				$per_page_dealer_car = (isset($_GET['per_page']) ? $_GET['per_page'] : 10);
				$search_rank = ($i + 1);
				if (isset($_GET['page']) && $_GET['page'] > 1) {
					$search_rank += ($_GET['page'] * $per_page_dealer_car);
                }
				$dealerCarRecord = $dealer_car_model::firstOrNew([
					'dealer_id' => $car->dealer->id,
					'car_id' => $car->id,
					'type'=> 'search',
                ]);
				$dealerCarRecord->dealer_id = (isset($car->dealer->id)) ? $car->dealer->id : '';
				$dealerCarRecord->type = 'search';
				$dealerCarRecord->car_id = $car->id; 
				$dealerCarRecord->search_rank = $search_rank . ' out of ' . $all_cars->total();
				$dealerCarRecord->search_page = (isset($_GET['page']) ? $_GET['page'] : 1);
				$dealerCarRecord->car_data = json_encode($car);
				$dealerCarRecord->save();

				// search car view data
				$SearchCarViews = $SearchCarViews_model::create();
				$SearchCarViews->car_id =  (isset($car->id)) ? $car->id : '';								
				$SearchCarViews->make = (isset($car->build->make)) ? $car->build->make: '';
				$SearchCarViews->model = (isset($car->build->model)) ? $car->build->model: '';
				$SearchCarViews->dealer_id = (isset($car->dealer->id)) ? $car->dealer->id: '';
				$SearchCarViews->search_type = "search";
				$SearchCarViews->save();

				// inserting to cron email //
				$dealerCron = $dealer_cron::firstOrCreate([
						'dealer_id' => $car->dealer->id,
						'dealer_name' => $car->dealer->name,
						'dealer_email' => $email_address,
						'date' => date("Y-m-d")
					]
				);

				$dealer_cron->where('id',$dealerCron->id)
					->update(['total_in_search' => $dealerCron->total_in_search+1]);
				}
				if($view ==1 || $view ==3 ){
					echo '</div></div>';
				} } ?>
            <!-- TEST WORK -->

                <div class="container">
                    <div class="row">
                        <div class="col-9">
                            <ul class="pagination justify-content-center">
								<?php
								$links = $all_cars->render();
								$links = str_replace("<a", "<a class='page-link ' ", $links);
								$links = str_replace("<li", "<li class='page-item' ", $links);
								$links = str_replace("<span", "<span class='page-link'",$links);
								echo $links;
								?>
                            </ul>
                        </div>
                        <div class="col-3">
                            <a class="btn btn-primary btn-block" href="{{route('CarComparePage')}}">Compare Car</a>
                        </div>
                    </div>
                </div>
            @else
                <div>
                    <div class="card bg-light">
                        <div class="card-body p-2 text-center ">
                            <div style="width:30%;margin: 0 auto;" class="fs18">
                                Search your desired car by entering the parameters.
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="save_search_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Save Search</h5>
                </div>
                <div class="modal-body">
                    @auth
                        <form action="{{route('save-my-search')}}" method="post" id="js-saveQuery_form">
                            <div class="row">
                                {{csrf_field()}}
                                <div class="col-sm-12 gutter-bottom">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="search_name" id="search_name" placeholder="Search Name">
                                    </div>
                                </div>
                            </div>
                            <input name="search_query" type="hidden" value="{{$_SERVER['QUERY_STRING']}}" />
                        </form>
                    @endauth
                    @guest
                        <p>Please login to the system in order to save this.</p>
                    @endguest
                </div>
                <div class="modal-footer">
                    @auth
                        <button type="button" class="btn btn-primary" onclick="submitQueryStr ();">Save</button>
                    @endauth
                    &nbsp;
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

	
	<!-- client reviews-- >
	
		
	<!-- START Rating Save Modal -->
			
	@include('web_pages.includes.rating-save-modal')

	<!-- END Rating Save Modal -->
	
	@guest 
	  
	   <div class="modal" id="guest">
		<div class="modal-dialog">
		  <div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header modal-header-primary">
			  <h4 class="modal-title rating-title">Rate and review <br>
			  <small>2018 Toyota Corolla XLi Automatic</small>
			  </h4> <br>
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			
			<!-- Modal body -->
			
			<div class="modal-body">
			<h5 align="center"> Please login to the system for rating and review.</h5>
			</div>
		 
		  
		  </div>
		</div>
		
	  </div>
	
	  @endguest
	
	
	@auth 
	
	   <div class="modal" id="rating_blocked">
		<div class="modal-dialog">
		  <div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header modal-header-primary">
			  <h4 class="modal-title rating-title">Rate and review <br>
			  <small>2018 Toyota Corolla XLi Automatic</small>
			  </h4> <br>
			  <button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			
			<div class="modal-body">
			<h5 align="center"> Ratings & Reviews is not allowed to this vehicle.</h5>
			</div>
		 
		  
		  </div>
		</div>
		
	  </div>
	  
	@endauth
	
	
	
    <div class="modal fade" id="compare_modal" tabindex="-1" role="dialog" aria-hidden="true">
        
		<div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Car for Comparision</h5>
                </div>
                <div class="modal-body" id="compare_div">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
	
	
    <script type="text/javascript">

		function load_param(type,other = ""){

			if(type == "main_category"){
				$("#sub_select").html('<option value="main_category">-Select Feature Category-</option>');
				$('#sub_select').attr('disabled',true);
				return false;
			}

			else if ( type == "Miles" || type == "Price" || type== "Country" ){
				options="";
				if(type == "Country"){
					options ='<option>US</option><option>CA</option><option>All</option>';
				}
				else if (type == "Miles"){
					options ='<option>1000 - 5000</option>';
					options +='<option>5000 - 10000</option>';
					options +='<option>10000 - 15000</option>';
					options +='<option>15000 - 20000</option>';
					options +='<option>20000 - 25000</option>';
					options +='<option>20000 - 30000</option>';
					options +='<option>30000 - 40000</option>';
					options +='<option>40000 - 50000</option>';
					options +='<option>50000 - 60000</option>';
					options +='<option>60000 - 70000</option>';
					options +='<option>70000 - 80000</option>';
					options +='<option>80000 - 90000</option>';
					options +='<option>90000 - 100000</option>';
					options +='<option>100000 - 150000</option>';
					options +='<option>150000 - 200000</option>';
				}
				else{
					options ='<option>$1000 - $5000</option>';
					options +='<option>$5000 - $10000</option>';
					options +='<option>$10000 - $15000</option>';
					options +='<option>$15000 - $20000</option>';
					options +='<option>$20000 - $25000</option>';
					options +='<option>$20000 - $30000</option>';
					options +='<option>$30000 - $40000</option>';
					options +='<option>$40000 - $50000</option>';
					options +='<option>$50000 - $60000</option>';
					options +='<option>$60000 - $70000</option>';
					options +='<option>$70000 - $80000</option>';
					options +='<option>$80000 - $90000</option>';
					options +='<option>$90000 - $100000</option>';
					options +='<option>$100000 - $150000</option>';
					options +='<option>$150000 - $200000</option>';
				}
				
				$('#sub_select').attr('disabled',false);
				
				$("#sub_select").html(options);
				
				
				return false;
			}

			if (type == "Make")
				$("#model_select").show();

			
			else
				$("#model_select").hide();

			$('#sub_select').attr('disabled',false);

			var data = "_token={{csrf_token()}}";
			
			
			if(other == "model"){
				if($("#main_select").val() == "Make"){
					$("#model_select").show();
					data += "&load_model=1&make="+$("#sub_select").val();
				}
				else{
					$("#model_select").hide();
					return false;
				}

			}

			else
				data += "&type="+type;
			$.ajax({
				url: "{{url('/load/param')}}",
				type:'POST',
				data: data,
				beforeSend: function(){
					if(other == "model")
						$("#model_select").html('<option>Loading...</option>');

					else
						$("#sub_select").html('<option>Loading...</option>');
				},
				success: function(html) {
					if(other == "model"){
						$('#model_select').attr('disabled',false);
						$("#model_select").html(html);
					}

					else
						$("#sub_select").html(html);

					if(type == 'Make')
						$("#sub_select").change();
				
				},
				
			
				
			});
	
	
		}

		// auto search 
		
			 $('#main_select').change(function () {
			  
			  var getvalue = $('option:selected', this).val();

			if (getvalue == "Make") {
				  
				$( "#model_select" ).change(function() {
					
				$('.autosearch').trigger( "click" );
			
				$('.autosearchbutton').trigger( "click" );
			
		     	});
		
			 }
			  
			else{
			
				$( "#sub_select" ).change(function() {
					
				$('.autosearch').trigger( "click" );
			
				$('.autosearchbutton').trigger( "click" );
			
			    });
			
		    }
			
			});	
			
		    $( "#radius_select" ).change(function() {
				
			   $('.autosearchbutton').trigger( "click" );
				
			});
						
		
		function save_car(numb){
			var data = "_token={{csrf_token()}}";
			data += "&car_post_data="+$("#save_car_"+numb).val();
			data += "&car_post_id="+$("#save_car_id_"+numb).val();

			$.ajax({
				url: "{{url('/save/car')}}",
				type:'POST',
				data: data,
				beforeSend: function(){
					$("#save_car_span_"+numb).removeClass("fa-save");
					$("#jq_save_tag_"+numb).html(" Saving...");
					$("#save_car_span_"+numb).addClass("fa-refresh fa-spin");
				},
				success: function() {
					$("#save_car_span_"+numb).removeClass("fa-refresh fa-spin");
					$("#save_car_span_"+numb).addClass("fa-check");
					$("#jq_save_tag_"+numb).html("Saved");
					$("#save_car_span_"+numb).parent().attr("href","javascript:void(0)");
				},
			});
		}

		function save_car_compare(carid){
			$('#compare_modal').modal('show');
			var data = "_token={{csrf_token()}}";
			data += "&car_id="+carid;

			$.ajax({
				url: "{{url('/save/compare/car')}}",
				type:'POST',
				data: data,
				beforeSend: function(){
					$("#compare_div").html('loading...');
				},
				success: function(html){
					$("#compare_div").html(html);
				},
			});
		}
		
		
	
    function rating_blocked(){

	 $('#rating_blocked').modal('show');

	}

	function car_rating_guest(){
	$('#guest').modal('show');
	
	}
// POST RATING & REVIEW
		
		function car_rating(carid,dealerid){
		$('#primary').modal('show');
		$(document).on('click','.post_message',function(){ 
	     var title = $('.title_msg').val();
		 var message = $('.write_msg').val();
		 var car_id =  carid;
		 var dealer_id = dealerid;
		 var rating = $('#rating').val();

		$.ajax({
			type:'POST',
			data: {
				"_token": "{{ csrf_token() }}",
				title: title,
				message: message,
				car_id: car_id,
				dealer_id:dealer_id,
				rating:rating
			},

			url:'{{url('save/reviews/ajax')}}',
			
			success:  function () {			
				
			$('.modal-body').fadeIn().html('Rating and review sumbitted successfully').css({"font-size": "18px", "color": "green", "text-align": "center"});

			setTimeout(function(){
			$('.modal').modal('hide');
			}, 3000);
				
			location.reload();			
			
			
			//window.location.href = "car/detail/"+car_id; */

			}

		});

	});
	
	}	

		function submitQueryStr (){
			
			if ($.trim($('#search_name').val ()).length == 0)
				alert ('Please write the search query name first');

			else
				$('#js-saveQuery_form').submit();
		}
    </script>
@endsection


@section('searchPageScript')
    <script src="{{asset('js/bootstrap-tagsinput.js')}}"></script>
    <script type="text/javascript">
		function updateQueryStringParameter(uri,key,value){

			if(key == 'sort_by'){

				uri = window.location.href.split('?')[0];

				var pairs = window.location.search.substring(1).split("&"),
					obj="";
				pair="";
				i="";
				for ( i in pairs ) {
					console.log(pairs[i]);
					pair = pairs[i].split("=");
					if ( pair[0] != "sort_by" && pair[0] != "sort_order" && pair[0] != "" ){
						obj += "&"+pairs[i];
					}
				}
				obj = obj.substr(1);
				sortArr = value.split('-');
				sortBy=sortArr[0];
				sortOrder=sortArr[1];

				uri =uri+"?"+obj+'&sort_by='+sortBy+'&sort_order='+sortOrder;
			}
			else{
				$("#per_page_field").val(value);

				return false;

				var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
				var separator = uri.indexOf('?') !== -1 ? "&" : "?";
				if (uri.match(re)) {
					uri = uri.replace(re, '$1' + key + "=" + value + '$2');
				}
				else {
					uri = uri + separator + key + "=" + value;
				}
			}
			window.location = uri;
		}

		
		
		
		function update_tag(main_select ,sub_select,pre_value=""){
			
			tag_val1="";

			if(pre_value == ""){

				if($("#sub_select").val() == 'Loading...')
					return false;

				if(main_select == "Year")
					$("#search_"+main_select).val($("#search_"+main_select).val()+","+sub_select);

				else if(main_select == "Make"){
					$("#search_Make").val($("#sub_select").val());

					if($("#model_select").val() != "" && $("#model_select").val() != 'Loading...')
						
					$("#search_Model").val($("#model_select").val());
				}

				else
					$("#search_"+main_select).val(sub_select);

				$("#search_"+main_select).attr('data-featured', $('#customCheckmh1').is(':checked'));

				$("#"+main_select+"-fea").remove();

				if($('#customCheckmh1').is(':checked'))
					$("#searh-form").append('<input type="hidden" value="1" id="'+main_select+"-fea"+'" name="'+main_select+"-fea"+'">');

				$(".form-field").each(function(){
					if($(this).val() != ""){

						field = $(this).attr('name')[0].toUpperCase()+$(this).attr('name').slice(1);
						field =field.replace("_"," ");

						if($(this).attr('name') == 'year') {

							y_tag= "";
							ye_arr = $(this).val().split(",");

							for (index = 0; index < ye_arr.length; ++index) {

								if(ye_arr[index] != "")
									y_tag += ',Year : '+ye_arr[index];
							}
							tag_val1 += ','+y_tag;
						}

						else
							tag_val1 += ','+(field+': ' + $(this).val());
					}
				});
				
			
				
				// this value
				
				tag_val1 = tag_val1.substr(1);
				$("#customCheckmh1").prop('checked',false);
				$("#main_select").val("main_category");
				$("#model_select").hide();
				$("#main_select").change();
			}
			else{
				$(".form-field").each(function(){
					if($(this).val() != ""){

						field = $(this).attr('name')[0].toUpperCase()+$(this).attr('name').slice(1);
						field =field.replace("_"," ");

						if($(this).attr('name') == 'year') {
							y_tag= "";
							ye_arr = $(this).val().split(",");

							for (index = 0; index < ye_arr.length; ++index) {

								if(ye_arr[index] != "")
									y_tag += ',Year : '+ye_arr[index];
							}

							console.log(y_tag);
							tag_val1 += ','+y_tag;
						}

						else
							tag_val1 += ','+(field+': ' + $(this).val());
					}
				});
				
				tag_val1 = tag_val1.substr(1);
			
			}

	
		
			$("#search_tage").tagsinput('removeAll');
			
			$("#search_tage").tagsinput('add', tag_val1);

			
			setTimeout(function(){
				$('.bootstrap-tagsinput .tag').each(function(){
					t_val = $(this).text().split(':')[0];
					if($("#search_"+t_val).attr('data-featured') == true || $("#search_"+t_val).attr('data-featured') == 'true'){
						$(this).css('background-color','#158cba');
						$(this).css('color','#fff');
					}
					else if($('#'+t_val+'-fea').val()){
						$(this).css('background-color','#158cba');
						$(this).css('color','#fff');
					}
				});

				//
				if($('.chkBox:checked').val() == "new"){
					$("#main_select option[value='Miles']").attr('disabled',true);
					$("#main_select option[value='Miles']").addClass('red-option');
					$("#main_select option[value='Year']").attr('disabled',true);
					$("#main_select option[value='Year']").addClass('red-option');
				}
				else{
					$("#main_select option[value='Miles']").attr('disabled',false);
					$("#main_select option[value='Miles']").removeClass('red-option');
					$("#main_select option[value='Year']").attr('disabled',false);
					$("#main_select option[value='Year']").removeClass('red-option');
				}
			},1000)
		}


		
		$(function(){

			$('#geo_ip').text($("#z_c").val());

			$('#search_tage').on('beforeItemRemove', function(event) {
				tagVal = event.item;
				tagVal = tagVal.split(":");
				f_name = tagVal[0].trim();
				f_name = f_name.replace(" ","_");
				console.log("#search_"+f_name);

				if (f_name == 'Door') {
					f_name = 'Doors';
                }

                if (f_name == 'Year') {
					if (tagVal[1] != '') {
						var remove_val = tagVal[1].trim();
						var year_val = $("#search_"+f_name).val().split(",");
						console.log(year_val);

						$("#search_"+f_name).val($("#search_"+f_name).val().replace(',' + remove_val, ''))
                    }
                } else {
					$("#search_"+f_name).val("");
				}
			});

			setTimeout(function(){ getLatLong($("#z_c").val());},3000);
			$('.chkBox').click(function() {

				if($(this).val() == "new"){
					$("#main_select option[value='Miles']").attr('disabled',true);
					$("#main_select option[value='Miles']").addClass('red-option');
					$("#main_select option[value='Year']").attr('disabled',true);
					$("#main_select option[value='Year']").addClass('red-option');
				 }

				else{
					$("#main_select option[value='Miles']").attr('disabled',false);
					$("#main_select option[value='Miles']").removeClass('red-option');
					$("#main_select option[value='Year']").attr('disabled',false);
					$("#main_select option[value='Year']").removeClass('red-option');
				}

				$(".chkBox").not(this).prop('checked', false);

				if($(this).is(':checked'))
					$("#car_type").val($(this).val());

				else
					$(this).prop('checked', true);
			});

			$("#searh-form").submit(function() {
				
				$(this).find(":input").filter(function(){
					return !this.value;
				}).attr("disabled", "disabled");
				$("#searh-form").submit;
				return true; // ensure form still submits
			});
			<?php
				if(isset($_GET['car_type'])){ ?>
				car_type="{{$_GET['car_type']}}";
			$(":checkbox[value="+car_type+"]").prop("checked","true");
			<?php } else { ?>
			$(":checkbox[value='new']").click();
			<?php } ?>
		});
		<?php if(isset($_GET['filter_search'])) { ?>
			<?php unset($_GET['filter_search']) ?>

		price = "<?=(isset($_GET['price']) ? $_GET['price'] : '');?>";
		body_type = "<?=(isset($_GET['body_type']) ? $_GET['body_type'] : '');?>";
		body_subtype = "<?=(isset($_GET['body_subtype']) ? $_GET['body_subtype'] : '');?>";
		country = "<?=(isset($_GET['country']) ? $_GET['country'] : '');?>";
		color = "<?=(isset($_GET['exterior_color']) ? $_GET['exterior_color'] : '');?>";
		miles = "<?=(isset($_GET['miles']) ? $_GET['miles'] : '');?>";
		year = "<?=(isset($_GET['year']) ? $_GET['year'] : '');?>";
		make = "<?=(isset($_GET['make']) ? $_GET['make'] : '');?>";
		engine = "<?=(isset($_GET['engine']) ? $_GET['engine'] : '');?>";
		model = "<?=(isset($_GET['model']) ? $_GET['model'] : '');?>";
		car_type = "<?=(isset($_GET['car_type']) ? $_GET['car_type'] : '');?>";
		door = "<?=(isset($_GET['door']) ? $_GET['door'] : '');?>";
		sort_by = "<?=(isset($_GET['sort_by']) ? $_GET['sort_by'] : '');?>";
		sort_order = "<?=(isset($_GET['sort_order']) ? $_GET['sort_order'] : '');?>";
		transmission = "<?=(isset($_GET['transmission']) ? $_GET['transmission'] : '');?>";
		radius = "<?=(isset($_GET['radius']) ? $_GET['radius'] : '');?>";
		seller_type = "<?=(isset($_GET['seller_type']) ? $_GET['seller_type'] : '');?>";
		trim = "<?=(isset($_GET['trim']) ? $_GET['trim'] : '');?>";
		drivetrain = "<?=(isset($_GET['drivetrain']) ? $_GET['drivetrain'] : '');?>";

		$("#search_Body_type").val(body_type);
		if(radius != "")
		$("#radius_select").val(radius);
		$("#search_Seller_type").val(seller_type);
		$("#search_Trim").val(trim);
		$("#search_Drivetrain").val(drivetrain);
		$("#search_Price").val(price);
		$("#search_Transmission").val(transmission);
		$("#search_Body_subtype").val(body_subtype);
		$("#search_Engine").val(engine);
		$("#search_Doors").val(door);
		$("#search_Model").val(model);
		$("#search_Exterior_color").val(color);
		$("#search_Country").val(country);
		$("#search_Miles").val(miles);
		$("#search_Year").val(year);
		$("#search_Make").val(make);
		$("#car_type").val(car_type);

		if(sort_by != "" && sort_order != ""){
			selectedOpt = sort_by+'-'+sort_order;
			$("#sort_select").val(selectedOpt);
		}

		update_tag(null,null,'q_string');

		<?php } ?>
		
			<?php if(isset($_GET['per_page'])) { ?>
			per_page = "<?=$_GET['per_page'];?>";
		$("#per_page").val(per_page);
		$("#per_page_field").val(per_page);
		<?php } ?>
			<?php if(isset($_GET['page'])) { ?>
			page = "<?=$_GET['page'];?>";
		$(".page-item").each(function(){
			if($(this).text() == page)
				$(this).addClass("active");
		});
		<?php } ?>
		<?php if(isset($_GET['view'])) { ?>
		$("#view_field").val("<?=$_GET['view'];?>");
		<?php } ?>
    </script>
	

<script>



// Rating Post


$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
    if (ratingValue > 1) {
        message = "Thanks! You rated this";
		msg = "" + ratingValue + "";
		message2 = " stars ";
		
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + "stars";
    }
	
	$("#rating2").val(ratingValue);
	
    responseMessage(msg);
	responseMessage2(message);
	responseMessage3(message2);
    
  });
  
  
});


	function responseMessage(msg) {
	  $('.success-box').fadeIn(200); 
	  $('.success-box div.text-message').html("<span>" + msg + "</span>");
	  var rating = msg; 
	  document.getElementById("rating").value = rating; 
	}

	function responseMessage2(message) {
	  $('.success-box').fadeIn(200); 
	  $('.success-box div.text-message2').html("<span>" + message + "</span>");
	}

	function responseMessage3(message3) {
	  $('.success-box').fadeIn(200); 
	  $('.success-box div.text-message3').html("<span>" + message2 + "</span>");
	}
	

	</script>
  	
	
@endsection