@extends('layouts.web_pages')
@section('content')
   <div id="cxm-home-slider" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#cxm-home-slider" data-slide-to="0" class="active"></li>
        <li data-target="#cxm-home-slider" data-slide-to="1"></li>
        <li data-target="#cxm-home-slider" data-slide-to="2"></li>
      </ol>
      <div class="cxm-slider-content">
        <div class="container py-5">
          <div class="row justify-content-center">
            <div class="col-sm-6 col-lg-5 text-center">
              <a class="box-eq bg-trans2" href="{{route('search_option')}}"><img class="img-fluid" src="{{asset('images/search-car.png')}}"><br>I want to find my perfect Car</a>
            </div>          
            <div class="col-sm-6 col-lg-5 offset-lg-1 text-center">
              <a class="box-eq bg-trans3" href="{{route('CarService')}}"><img class="img-fluid" src="{{asset('images/service-car.png')}}"><br>I want to service my Car</a>
            </div>
          </div>
        </div>
      </div>
        
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="{{asset('images/slide-1.jpg')}}" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="{{asset('images/slide-2.jpg')}}" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="{{asset('images/slide-3.jpg')}}" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#cxm-home-slider" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"><span class="fa fa-chevron-left"></span></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#cxm-home-slider" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"><span class="fa fa-chevron-right"></span></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    
    <div class="py-5 bg-light">    
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="heading heading-lg fc1 text-center mb-5">Car buying, the Otto Guide way</div>
            
            <div class="timeline">
              <div class="timeline-container timeline-left">
                <div class="num">1</div>
                <div class="timeline-content text-center">
                  <div class="heading mb-3">We Buy Together</div>
                  <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fringilla vel quam vel feugiat. Etiam in tortor metus.</p>
                  <div class="text-center mt-3"><a class="mr-3" href="#"><img width="25%" class="img-fluid" src="{{asset('public/images/search-car.svg')}}"></a> <a href="#"><img width="25%" class="img-fluid" src="{{asset('public/images/service-car.svg')}}"></a></div>
                </div>
              </div>
              <div class="timeline-container timeline-right">
                <div class="num">2</div>
                <div class="timeline-content text-center">
                  <div class="heading mb-3">Communicate Anonymously</div>
                  <p class="mb-0">Communicate with dealers thru site No endless marketing calls or emails.</p>
                  <div class="text-center mb-3">
                    <img class="img-fluid" src="{{asset('images/comm-icons1.png')}}">
                </div>
                </div>
              </div>
              <div class="timeline-container timeline-left">
                <div class="num">3</div>
                <div class="timeline-content text-center">
                  <div class="heading mb-3">Upfront Reduced Pricing</div>
                  <p class="mb-0">We have relationships with thousands of dealers and thet love our customers.</p>
                  <div class="text-center"><img class="img-fluid" src="{{asset('images/13.png')}}"></div>
                </div>
              </div>
              <div class="timeline-container timeline-right">
                <div class="num">4</div>
                <div class="timeline-content text-center">
                  <div class="heading mb-3">Review Your Experience</div>
                  <p class="mb-0">Communicate with dealers thru site No endless marketing calls or emails.</p>
                  <div class="text-center"><img width="87px" class="img-fluid" src="{{asset('images/14.png')}}"></div>
                  
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>      
    </div>
    
    <div class="py-5">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="text-center mt-3"><a class="" href="#" style="margin: 0 50px;"><img width="12%" class="img-fluid" src="{{asset('public/images/search-car.svg')}}"></a> <a href="#" style=" margin: 0 50px;"><img width="12%" class="img-fluid" src="{{asset('public/images/service-car.svg')}}"></a></div>
          </div>
        </div>
      </div>    
    </div>
    
    <div class="py-5 bgc2">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="heading heading-lg mb-4">Trends and Research</div>
            <?php if(!FALSE){ ?>
            <div id="cxm-slider-trands" class="carousel slide mb-5" data-ride="carousel" data-interval="false">  
              <div class="carousel-inner w-75 mx-auto">
                <?php for($i = 0; $i < 2; $i++){ ?>
                <div class="carousel-item<?php echo ($i === 0)?' active' :''; ?>">
                  <div class="form-row">
                    <div class="col-4">
                      <div class="trands text-white<?php echo ($i === 0)?' active' :''; ?>">
                        <img class="img-fluid" src="{{asset('images/car-0.jpg')}}" alt="IMG">
                        <div class="text-center">Best in Class</div>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="trands">
                        <img class="img-fluid" src="{{asset('images/car-3.jpg')}}" alt="IMG">
                        <div class="text-center">Most Popular</div>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="trands"> 
                        <img class="img-fluid" src="{{asset('images/car-2.jpg')}}" alt="IMG">
                        <div class="text-center">Top Rated</div>
                      </div>  
                    </div>                  
                  </div>                  
                </div>
                <?php } ?>
              </div>

              <a class="carousel-control-prev" href="#cxm-slider-trands" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#cxm-slider-trands" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            <?php } ?>            
            <div id="cxm-slider-searches" class="carousel slide mb-2" data-ride="carousel" data-interval="false">  
              <div class="carousel-inner">
                <?php for($i = 0; $i < 2; $i++){ ?>
                <div class="carousel-item<?php echo ($i === 0)?' active' :''; ?>">
                  <div class="row no-gutters justify-content-center">
                    <div class="col">
                      <div class="search<?php echo ($i === 0)?' active' :''; ?>">Sedan</div>
                    </div>
                    <div class="col">
                      <div class="search">Truck</div>
                    </div>
                    <div class="col">
                      <div class="search">Convertible</div>
                    </div>
                    <div class="col">
                      <div class="search">SUV</div>
                    </div>
                    <div class="col">
                      <div class="search">Crossover</div>
                    </div>                                      
                  </div>                                    
                </div>
                <?php } ?>
              </div>
              
              <a class="carousel-control-prev" href="#cxm-slider-searches" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#cxm-slider-searches" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            <?php $c_nm = array('Sedan', 'Truck', 'Convertible', 'SUV', 'Crossover');
            ?>
            <div id="cxm-slider" class="carousel slide" data-ride="carousel">  
              <div class="carousel-inner">
                <?php for($i = 0; $i < 5; $i++){ ?>
                <div class="carousel-item<?php echo ($i === 0)?' active' :''; ?>">
                  <div class="slide-heading">
                    Best in Class <?php echo $c_nm[$i]; ?>
                    <div class="fs24">2017 Honda Accord</div>
                  </div>
                  <img class="img-fluid" src="{{asset('images/car-'.$i.'.jpg')}}" alt="IMG">
                </div>
                <?php } ?>
              </div>
            </div>                       
          </div>
        </div>
      </div>
    </div>
    
    <div class="py-5 bgi3 bg-cover">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-lg-4">
            <div class="card rounded-0 border-0 bg-trans1">
              <div class="card-body">
                <div class="heading heading-sm font-weight-bold fc1 mb-3">Car buying, the Otta Guide way</div>
                <ul class="list-unstyled list-quiz fs14">
                  <li><span class="fa fa-check-square-o text-primary"></span> Item 1</li>
                  <li><span class="fa fa-check-square-o text-primary"></span> Item 2</li>
                  <li><span class="fa fa-check-square-o text-primary"></span> Item 3</li>
                  <li><span class="fa fa-check-square-o text-primary"></span> Item 4</li>
                </ul>
                <p class="fs12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fringilla vel quam vel feugiat. Etiam in tortor metus. In hac habitasse platea dictumst. Donec justo odio, ultricies eu orci et, varius fermentum dui.</p>
                <div class="font-weight-bold fc1">Lorem ipsum</div>
                <div class="fs24 font-weight-bold fc1">$ 00.00</div> 
                <div class="mt-3">
                  <a class="btn btn-primary" href="#">Take Our Car Quiz</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>    
    </div>
    
    <div class="py-5">
      <div class="container">
        <h2 class="fc1 mb-3">What our satisfied customers are saying...</h2>
        <div class="row">
          <div class="col-sm-12">
            <div id="cxm-slider-testinonials" class="carousel slide" data-ride="carousel">  
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <div class="border p-4">
                    <div class="form-row">
                      <div class="col-3 text-center"><img class="img-fluid" src="{{asset('images/icon-logo.png')}}" alt="IMG"></div>
                      <div class="col-9">
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fringilla vel quam vel feugiat. Etiam in tortor metus. In hac habitasse platea dictumst. Donec justo odio, ultricies eu orci et, varius fermentum dui. Nunc ut lorem dictum, consequat lorem quis, accumsan metus. In hac habitasse platea dictumst. Aliquam erat volutpat. Maecenas tempus libero nec feugiat aliquet."</p>
                        <div class="font-weight-bold fs14 text-dark">Author Name</div>
                        <div class="fs14 text-muted">Company Name</div>
                      </div>
                    </div>             
                  </div>     
                </div>
                <div class="carousel-item">
                  <div class="border p-4">
                    <div class="form-row">
                      <div class="col-3 text-center"><img class="img-fluid" src="{{asset('images/icon-logo.png')}}" alt="IMG"></div>
                      <div class="col-9">
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fringilla vel quam vel feugiat. Etiam in tortor metus. In hac habitasse platea dictumst. Donec justo odio, ultricies eu orci et, varius fermentum dui. Nunc ut lorem dictum, consequat lorem quis, accumsan metus. In hac habitasse platea dictumst. Aliquam erat volutpat. Maecenas tempus libero nec feugiat aliquet."</p>
                        <div class="font-weight-bold fs14 text-dark">Author Name</div>
                        <div class="fs14 text-muted">Company Name</div>
                      </div>
                    </div>             
                  </div>     
                </div>
                <div class="carousel-item">
                  <div class="border p-4">
                    <div class="form-row">
                      <div class="col-3 text-center"><img class="img-fluid" src="{{asset('images/icon-logo.png')}}" alt="IMG"></div>
                      <div class="col-9">
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam fringilla vel quam vel feugiat. Etiam in tortor metus. In hac habitasse platea dictumst. Donec justo odio, ultricies eu orci et, varius fermentum dui. Nunc ut lorem dictum, consequat lorem quis, accumsan metus. In hac habitasse platea dictumst. Aliquam erat volutpat. Maecenas tempus libero nec feugiat aliquet."</p>
                        <div class="font-weight-bold fs14 text-dark">Author Name</div>
                        <div class="fs14 text-muted">Company Name</div>
                      </div>
                    </div>             
                  </div>     
                </div>
              </div>

              <a class="carousel-control-prev" href="#cxm-slider-testinonials" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon fa fa-angle-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#cxm-slider-testinonials" role="button" data-slide="next">
                <span class="carousel-control-next-icon fa fa-angle-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            
          </div>
        </div>
      </div>
    </div> 
@endsection