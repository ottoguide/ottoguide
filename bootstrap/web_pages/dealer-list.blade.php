@extends('layouts.web_pages')
@section('content')
	<style>

		.table tbody > tr > td {
			font-size:13px;
			background: #f5f5f5;
			background-color: rgb(245, 245, 245);
			border-top: 2px solid #fff;
			border-right: 2px solid #fff;
			vertical-align: middle;
			padding: 9px 7px;
			overflow: hidden;
		}


		.card-header{
			font-family: 'Century Gothic',CenturyGothic,AppleGothic,sans-serif;
			background: #127ba3;
			color:#fff !important;
		}
		.card-header b{
			color:#fff;
		}

		.services{
			padding:10px;
		}

		.refine-search .card-header a {
			color: #fff;
		}
		
		.map-border{
		  border-top: 3px #158CBA solid;		
		}
		
	</style>
	
	<br>
	
	<div class="header-margin">
		<div class="container">


			@if($errors->any())
				<p class="alert alert-success">Search Saved successfully </p>
			@endif


			<div class="row">
				<div class="col-sm-12">
					<div class="card refine-search">
						<div class="card-header font-weight-bold">
							<a data-toggle="collapse" href="#refineSearch" role="button" aria-expanded="false" aria-controls="refineSearch"><b>Your Selected Services Details</b></a>
						</div>
						<div class="collapse show" id="refineSearch">
							<div class="">
								@if(!empty(Session::get('services')))
									<table class="table services" style="margin: 0;">
										
										<tr>
											<td class="text-center"><i class="fa fa-arrow-circle-down"></i> <b>Year</b></td>
											<td class="text-center"><i class="fa fa-arrow-circle-down"></i> <b>Model</b></td>
											<td class="text-center"><i class="fa fa-arrow-circle-down"></i> <b>Make</b></td>
											<td class="text-center"><i class="fa fa-arrow-circle-down"></i> <b>Trim</b></td>
											<td class="text-center"><i class="fa fa-arrow-circle-down"></i> <b>Mileage</b></td>
										</tr>
										
										<tr>
											<td class="text-center">{{Session::get('services')['Year']}}</td>
											<td class="text-center">{{Session::get('services')['Model']}}</td>
											<td class="text-center">{{Session::get('services')['Make']}}</td>
											<td class="text-center">{{Session::get('services')['Trim']}}</td>
											<td class="text-center">{{Session::get('services')['Mileage']}}</td>
										</tr>

										
									  
										
										<?php
											foreach(Session::get('services') as $key=> $description) {
												if ($key == 'Year' || $key == 'Model' || $key == 'Make' || $key == 'Trim' || $key == 'Mileage') {
													continue;
												}
										?>
											<tr>
												<td colspan="2">
													<h6><i class="fa fa-arrow-circle-right"></i> <b>{{$key}}</b></h6>
												</td>
												<td colspan="3">
													<h6>{{ucfirst($description)}}</h6>
												</td>
											</tr>
										<?php } ?>
									</table>
								@endif
							</div>
						</div>

					</div>
				</div>
			</div>

		</div>
	</div>


	<div class="py-3">
		<div class="container">

			<div class="message" width="50%" align="center">
				@if (session('message'))
					<div class="alert alert-success" width="50%">
						{{ session('message') }}
					</div>
				@endif
			</div>

			<div class="message" width="50%" align="center">
				@if (session('error'))
					<div class="alert alert-danger" width="50%">
						{{ session('error') }}
					</div>
				@endif
			</div>


			<div class="row">

				<div class="col-lg-12">

					<div class="card bg-light">
						<div class="card-body p-2">
							<h3>Dealers List</h3>
						</div>
					</div>

					<div class="main-box clearfix">
						
						
						<div class="row custom-padding">

						@if(!empty($all_dealers) > 0)							
							@foreach($all_dealers as $dealer_detail)
								<?php
									$email_address="syedshaharif@gmail.com";
									
									$dealerRecord = $dealer_model::findOrNew($dealer_detail->id);
									$dealerRecord->dealer_id = $dealer_detail->id;
									$dealerRecord->dealer_latitude = (isset($dealer_detail->latitude) ? $dealer_detail->latitude : '');
									$dealerRecord->dealer_longitude = (isset($dealer_detail->longitude) ? $dealer_detail->longitude : '');
									$dealerRecord->dealer_country = (isset($dealer_detail->country) ? $dealer_detail->country : '');
									$dealerRecord->dealer_website = (isset($dealer_detail->website) ? $dealer_detail->website : '');
									if (!$dealerRecord->exists) {
										$dealerRecord->dealer_name = $dealer_detail->seller_name;
										$dealerRecord->dealer_email = $email_address;
										$dealerRecord->dealer_phone = (isset($dealer_detail->seller_phone) ? $dealer_detail->seller_phone : '');
									}
									$dealerRecord->dealer_street = (isset($dealer_detail->street) ? $dealer_detail->street : '');
									$dealerRecord->dealer_city = (isset($dealer_detail->city) ? $dealer_detail->city : '');
									$dealerRecord->dealer_zip = (isset($dealer_detail->zip) ? $dealer_detail->zip : '');
									$dealerRecord->save();
								?>
			
					<div class="col-md-4 col-lg-4">
                    <div id="cxm-slider-pro" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                          
                     <iframe src="https://maps.google.com/maps?q={{$dealer_detail->latitude}},{{$dealer_detail->longitude}}&hl=es;z=8&amp;output=embed" frameBorder="0" height="300" width="100%" class="map-border"></iframe>

                        </div>
                    
                       
                    </div>
                    <div class="cxm-content">
                       
                        <hr class="hr1">
                        <div class="text-muted fs12"><span class="text-dark fs14">DEALER NAME :</span> &nbsp; {{$dealer_detail->seller_name}}</div>
                        <div class="text-muted fs12"><span class="text-dark fs14">DEALER CITY :</span> &nbsp; {{$dealer_detail->city}} </div>
                        <div class="text-muted fs12"><span class="text-dark fs14">DEALER STREET :</span> &nbsp; {{$dealer_detail->street}}</div>
                        <hr class="hr1">
                        
						
							<a href="javascript:services_email('<?=$dealer_detail->id ?>','<?=$dealer_detail->seller_name ?>');" class="btn btn-primary btn-lg btn-block">
							<span class="fa fa-comment"></span> Anonymous Contact 
							<?php //preg_match_all( "/[0-9]/", (isset($dealer_detail->seller_phone) ? $dealer_detail->seller_phone : '--')) ?>
							</a>				
						
                    <br><br>	
					</div>
							
					 </div>		
					
							<!-- SERVICES EMAIL TO DEALER -->
	
							@endforeach

							<div>
							
							<div>
							</div>	
							</div>
							</div>
							
								<ul class="pagination justify-content-center">
									<?php
									$links = $all_dealers->render();
									$links = str_replace("<a", "<a class='page-link ' ", $links);
									$links = str_replace("<li", "<li class='page-item' ", $links);
									$links = str_replace("<span", "<span class='page-link'",$links);
									echo $links;
									?>
								</ul>
								@endif
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	
	<form id="services_email" method="post" action="{{url('user-services-email.html')}}">
	{!! Form::hidden('_token', csrf_token()) !!}
	<input type="hidden" name="dealer_id" class="dealer_id">
	<input type="hidden" name="dealer_name" class="dealer_name">
	</form>

		
<!-- Modal -->
<div class="modal fade" id="dealer-contact-modal" tabindex="-1" role="dialog" aria-labelledby="contact-modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="contact-modal">Dealer Contact</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<center>
      		@auth
		        <a href="javascript:void(0);" style="width: 50%;" class="call-button btn btn-block btn-primary">
		        <i class="fa fa-phone "></i> Call Dealer</a>
		        
				<button style="width: 50%;" class="btn btn-block btn-primary"><i class="fa fa-comments"></i> Chat Dealer</button>
				<button type="submit" style="width: 50%;" form="services_email" class="btn btn-block btn-primary"><i class="fa fa-envelope"></i>Email</button>
		      
				<div id="div1"></div>
	        @endauth
	       
		   @guest	        
		        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary"><i class="fa fa-phone"></i> Call Dealer</a>
		        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary"><i class="fa fa-comments"></i> Chat Dealer</a>
		        <a href="{{route('login')}}" style="width: 50%;" class="btn btn-block btn-primary"><i class="fa fa-envelope"></i> Email</a>
	        @endguest
        </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>        
      </div>
    </div>
  </div>
</div>



@endsection
@section('searchPageScript')
@endsection 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">

   function services_email(dealerid,dealername){

		$('#dealer-contact-modal').modal('show');
		
		$(".dealer_id").val(dealerid);
		
		$(".dealer_name").val(dealername);

   }



	$(function(){

		$(".call-button").click(function(){
			$(this).html('connecting to call...');
    		$.ajax({
    			url: "http://ethnicjob.ca/twilio-call-chat/call.php",
    			jsonp: 'jsonp_callback',     			
    			type: "POST",
		        crossDomain: true,
    			data: {id: 1},
    			dataType: 'jsonp',
    			headers: {
			        "Access-Control-Allow-Origin": "*",
			        "Access-Control-Allow-Headers": "origin, content-type, accept"
			    },
			    error: function(xhr, status, error) {

                     alert(xhr.status+status+error);

                 },
    			success: function(result){    				
        			$("#div1").html(result);
    			}
    		});
		});
	})
</script>