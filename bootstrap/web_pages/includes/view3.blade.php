 <div class="col-md-6 col-lg-4">
                    <div id="cxm-slider-pro-<?php echo $i; ?>" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
							<?php
							$car_images = $car->media->photo_links;
							if($i >= 4){$active = 1;}else {$active = $i;}
							if($car_images){
							foreach($car_images as $pimg => $car_image ) {
							if($pimg == 4)
								break;
							?>
                            <div class="carousel-item<?php echo (($pimg == 0)? ' active' :''); ?>">
                                <div class="cxm-img img-fixed">
                                    <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                                    <a href="{{url('car/detail').'/'.$car->id}}"><img class="img-fluid" onerror="this.onerror=null;this.src='{{asset('public/images/no-image.jpeg')}}';" src="{{$car_image}}"></a>
                                </div>
                            </div>
							<?php } } else { ?>
                            <div class="carousel-item active">
                                <div class="cxm-img img-fixed">
                                    <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                                    <a href="{{url('car/detail').'/'.$car->id}}"><img class="img-fluid" src="{{asset('public/images/no-image.jpeg')}}"></a>
                                </div>
                            </div>
							<?php } ?>
                        </div>
                        <a class="carousel-control-prev" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon fa fa-angle-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="next">
                            <span class="carousel-control-next-icon fa fa-angle-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <div class="cxm-content">
                        <div class="form-row text-muted">
                            <div class="col-6">{{isset($car->build->year)?$car->build->year:''}}   {{(isset($car->build->make) ? $car->build->make : '' )}}</div>
                            <div class="col-6 text-right">{{isset($car->miles) ? $car->miles:'--'}} MILES</div>
                        </div>
                        <div class="form-row text-muted">
                            <div class="col-6 lh14"><span class="text-dark">{{isset($car->build->model)?$car->build->model:''}}</span><br>{{isset($car->inventory_type)?$car->inventory_type:''}}</div>

                            <div class="col-6 text-right"><div class="price">$ {{isset($car->price) ? number_format((float)$car->price) : '--'}}</div></div>
                        </div>
                        <hr class="hr1">
                        <div class="text-muted fs12"><span class="text-dark fs14">EXT.COLOR</span> &nbsp; {{isset($car->exterior_color)?$car->exterior_color:''}}</div>
                        <div class="text-muted fs12"><span class="text-dark fs14">INT.COLOR</span> &nbsp; {{isset($car->interior_color)?$car->interior_color:''}}</div>
                        <hr class="hr1">
                        <div class="text-dark fs14">More Features</div>
                        <div class="text-muted fs12">{{isset($car->build->year)?$car->build->year:''}} {{(isset($car->build->model) ? $car->build->model : '' )}} {{isset($car->build->engine)?$car->build->engine :''}}</div>
						
						<!--<div class="row mt-2">
						<div class="col-12 mb-1"><a class="btn btn-primary btn-block" href="{{url('car/detail').'/'.$car->id}}"><span class="fa fa-eye"></span> SEE MORE DETAIL</a></div>
						</div>-->

                        <div class="row mt-2">
                            <div class="col-12 mb-1"><a class="btn btn-primary btn-block" href="javascript:save_car_compare('<?=$car->id?>');"><span class="fa fa-clone"></span> Compare</a></div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12 mb-1"><a class="btn btn-primary btn-block" href="#primary" ><span class="fa fa-line-chart"></span> Rating</a></div>
                        <h4><i class="glyphicon glyphicon-eye-open"></i> 
			
						</div>
                        <div class="row mt-2">
                            <div class="col-12 mb-1"><a class="btn btn-primary btn-block" href="#"><span class="fa fa-expand"></span> Dealer</a></div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12 mb-1"><a class="btn btn-primary btn-block" href="{{route('CarHistorySave',['car_id'=> $car->id])}}"><span class="fa fa-history"></span> History</a></div>
                        </div>
                    </div>
                </div>
