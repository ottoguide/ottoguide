<div class="cxm-pro-list cxm-detail-border">
                    <div class="row no-gutters">
                        <div class="col-sm-9">
                            <div class="cxm-detail p-3">
                                <div class="row no-gutters">
                                    <div class="col-sm-4">
                                        <div id="cxm-slider-pro-<?php echo $i; ?>" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner">
												<?php
												$car_images = $car->media->photo_links;
												if($i >= 4){$active = 1;}else {$active = $i;}
												if($car_images){
												foreach($car_images as $pimg => $car_image ) {
												if($pimg == 4)
													break;
												?>
                                                <div class="carousel-item<?php echo (($pimg == 0)? ' active' :''); ?>">
                                                    <div class="cxm-img img-fixed2">
                                                        <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                                                        <a href="{{url('car/detail').'/'.$car->id}}"><img onerror="this.onerror=null;this.src='{{asset('public/images/no-image.jpeg')}}';" class="img-fluid" src="{{$car_image}}"></a>
                                                    </div>
                                                </div>
												<?php } } else { ?>
                                                <div class="carousel-item active">
                                                    <div class="cxm-img img-fixed2">
                                                        <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                                                        <a href="{{url('car/detail').'/'.$car->id}}"><img class="img-fluid" src="{{asset('public/images/no-image.jpeg')}}"></a>
                                                    </div>
                                                </div>
												<?php } ?>
                                            </div>

                                            <a class="carousel-control-prev" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="prev">
                                                <span class="carousel-control-prev-icon fa fa-angle-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="carousel-control-next" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon fa fa-angle-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="cxm-content">
                                            <h2><a href="{{url('car/detail').'/'.$car->id}}">@if(!empty($car->heading) > 0){{$car->heading}}@endif</a></h2>
                                            <div class="form-row">
                                                <div class="col-md-4">
                                                    @if(!empty($car->price) > 0)
                                                        <div class="fs24 fc1">$ {{isset($car->price) ? number_format((float)$car->price) : '--'}}</div>
                                                    @endif
                                                    <div class="text-muted">{{isset($car->miles) ? $car->miles:'--'}} MILES</div>
                                                    <div><span class="fa fa-square fs18" {{isset($car->exterior_color) ? 'style="color:'.$car->exterior_color.' ;"' :''}}></span> Color</div>
                                                    <div class="cxm-features">{{isset($car->build->year)?$car->build->year:''}} {{(isset($car->build->model) ? $car->build->model : '' )}} {{isset($car->build->engine)?$car->build->engine :''}}
                                                    </div>
                                                    <div class="text-muted">{{ucfirst($car->inventory_type)}}</div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="fc1">Description:</div>
                                                    <p>Make: {{(isset($car->build->make) ? $car->build->make : '') }}</p>
                                                    <p>Model: {{(isset($car->build->model) ? $car->build->model : '' )}}</p>
                                                    <p>Trim: {{isset($car->build->trim) ? $car->build->trim : ' ' }}</p>
                                                    <p>Body Type: {{isset($car->build->body_type) ? $car->build->body_type : ' '}}</p>
                                                    <p>Vehicle Type: {{isset($car->build->vehicle_type) ? $car->build->vehicle_type : ''}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pt-3">
                                    <span class="fa fa-video-camera"></span> 0 <span class="fa fa-camera ml-4"></span> {{count($car_images)}} <span class="ml-4">VIN: {{$car->vin}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="cxm-actions">
                                <div class="row">
                                    <div class="col-12 mb-2">
                                        @auth
											<?php $tot = DB::table('client_favourite')->where('client_id',Auth::id())->where('car_id',$car->id)->count();?>
                                            @if($tot==0)
												<?php
												$save_car= array(
													'id' => (isset($car->$car->id) ? $car->$car->id : ''),
													'title' => (isset($car->heading) ? $car->heading : '--'),
													'car_type' => (isset ($car->inventory_type) ? $car->inventory_type : '--'),
													'price' => (isset($car->price) ? $car->price : '--'),
													'miles' => (isset($car->miles) ? $car->miles : '--'),
													'build' => (isset($car->build)) ? $car->build: '',
													'images' => $car_images
												);?>
                                                <input type="hidden" id="save_car_id_{{$i}}" value="{{$car->id}}">
                                                <input type="hidden" id="save_car_{{$i}}" value="{{base64_encode(json_encode($save_car))}}">
                                                <a class="btn btn-primary btn-block" href="javascript:save_car({{$i}});"><span id="save_car_span_{{$i}}" class="fa fa-save"></span> <span id="jq_save_tag_{{$i}}">Save</span></a>
                                            @else
                                                <a class="btn btn-primary btn-block" href="javascript:void(0);">
                                                    <span class="fa fa-check"></span>
                                                    <span>Saved</span>
                                                </a>
                                            @endif
                                        @endauth
																				
                                        @guest
                                        <a class="btn btn-primary btn-block" data-toggle="modal" data-target="#save_search_model"  href="javascript:void(0);"><span class="fa fa-save"></span> Save </a>
                                        @endguest
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="javascript:save_car_compare('<?=$car->id?>');"><span class="fa fa-clone"></span> Compare</a></div>
                                </div>
                                <div class="row">
                                    <div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="#primary" data-toggle="modal"><span class="fa fa-line-chart"></span> Rating</a></div>
                                </div>
                                <div class="row">
                                    <div class="col-12 mb-2"><a class="btn btn-primary btn-block" href="#"><span class="fa fa-expand"></span> Dealer</a></div>
                                </div>
                                <div class="row">
                                    <div class="col-12"><a class="btn btn-primary btn-block" href="{{route('CarHistorySave',['car_id'=> $car->id])}}"><span class="fa fa-history"></span> History</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
