<div class="col-md-<?php echo $bootstrapColWidth; ?> box pull-right">
                    <div class="cxm-detail p-3 cxm-detail-border">
                        <div class="row no-gutters">
                            <div class="col-sm-5">
                                <div class="cxm-img">
                                    <div id="cxm-slider-pro-<?php echo $i; ?>" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
											<?php
											$car_images = (isset($car->media->photo_links) ? $car->media->photo_links : array() );
											if($i >= 4){$active = 1;}else {$active = $i;}
											if($car_images){
											foreach($car_images as $pimg => $car_image ) {
											if($pimg == 4)
												break;
											?>
                                            <div class="carousel-item<?php echo (($pimg == 0)? ' active' :''); ?>">
                                                <div class="cxm-img">
                                                    <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                                                    <a href="{{url('car/detail').'/'.$car->id}}"><img class="img-fluid" onerror="this.onerror=null;this.src='{{asset('public/images/no-image.jpeg')}}';" src="{{$car_image}}"></a>
                                                </div>
                                            </div>
											<?php } } else { ?>
                                            <div class="carousel-item active">
                                                <div class="cxm-img">
                                                    <div class="cxm-img-badge d-none"><span class="fa fa-camera"></span> 4</div>
                                                    <a href="{{url('car/detail').'/'.$car->id}}"><img class="img-fluid" src="{{asset('public/images/no-image.jpeg')}}"></a>
                                                </div>
                                            </div>
											<?php } ?>
                                        </div>

                                        <a class="carousel-control-prev" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon fa fa-angle-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#cxm-slider-pro-<?php echo $i; ?>" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon fa fa-angle-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="cxm-content ">
                                    <h2><a href="{{url('car/detail').'/'.$car->id}}">@if(!empty($car->heading) > 0){{mb_strimwidth($car->heading,0,25,"...")}}@endif</a></h2>
                                    @if(!empty($car->price) > 0)
                                        <div class="fs24 fc1">$ {{isset($car->price) ? number_format((float)$car->price) : ''}}</div>
                                    @endif
                                    <div class="cxm-features">{{isset($car->build->year)?$car->build->year:''}} {{(isset($car->build->model) ? $car->build->model : '' )}} {{isset($car->build->engine)?$car->build->engine :''}}</div>
                                    <p>Make: {{(isset($car->build->make) ? $car->build->make : '') }}</p>
                                    <p>Model: {{(isset($car->build->model) ? $car->build->model : '' )}}</p>
                                    <p>Trim: {{isset($car->build->trim) ? $car->build->trim : ' ' }}</p>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-xs-6">
                                @auth
									<?php $tot = DB::table('client_favourite')->where('client_id',Auth::id())->where('car_id',$car->id)->count();?>
                                    @if($tot==0)
										<?php
										$save_car= array(
											'id' => (isset($car->id)) ? $car->id: '', 
											'title' => (isset($car->heading)) ? $car->heading: '',
											'car_type' => $car->inventory_type,
											'price' => (isset($car->price) ? $car->price : '--'),
											'miles' => (isset($car->miles) ? $car->miles : '--'),
											'build' => (isset($car->build)) ? $car->build: '',
											'images' =>(isset($car_images)) ? $car_images: '',
										);
										?>
                                        <hr>
										
										
                                        <input type="hidden" id="save_car_id_{{$i}}" value="{{$car->id}}">
                                        <input type="hidden" id="save_car_{{$i}}" value="{{base64_encode(json_encode($save_car))}}">
                                        <a class="btn btn-primary btn-sm" href="javascript:save_car({{$i}});"><span id="save_car_span_{{$i}}" class="fa fa-save"></span> <span id="jq_save_tag_{{$i}}">Save</span></a>
                                    @else
                                        <a class="btn btn-primary btn-sm" href="javascript:void(0);">
                                            <span class="fa fa-check"></span><span>Saved</span></a>
                                @endif
                                @endauth
                                @guest
                                    <a class="btn btn-primary btn-sm" data-toggle="modal" data-target="#save_search_model"  href="javascript:void(0);"><span class="fa fa-save"></span> Save</a>
                                @endguest

                                <a href="javascript:save_car_compare('<?=$car->id?>');">
								<button type="button" class="btn btn-primary btn-sm"><span class="fa fa-clone"></span> Compare</button></a>
						
							   @if(count(Auth::user()))
	
							   <?php
							   $rating_status = DB::table('client_reviews')->where('client_id',Auth::user()->id)->where('car_id','=',$car->id)->where('reply_to','=',null)->get()->first();
							   $check_block_status = DB::table('settings')->where('car_id',$car->id)->where('reviews_off',1)->get()->first();
							   ?>
					
							   @if(count($check_block_status))
							   <a class="btn btn-primary btn-sm" href="javascript:rating_blocked();"><span class="fa fa-line-chart"></span> Rating </a>
							   @else
								   
							   @if(count($rating_status))
							   <a class="btn btn-success btn-sm" href="car/detail/<?=$car->id?>"><span class="fa fa-line-chart" style="color:#fff;"></span> Rated</a>
							 
 							   @else
							   <a class="btn btn-primary btn-sm" href="javascript:car_rating('<?=$car->id?>', <?=$car->dealer->id ?>);"><span class="fa fa-line-chart"></span> Rating </a>
							   
							   @endif
							   
							   @endif
							 
							    @if(!count($check_block_status))
								@include('web_pages.includes.rating-save-modal')
								@endif
							 
							   @endif
							  
							
							   @guest
							   <a class="btn btn-primary btn-sm" href="javascript:car_rating_guest();"><span class="fa fa-line-chart"></span> Rating </a>
                               @endguest	
							 
								
                                <a href="{{route('CarHistorySave',['car_id'=> $car->id])}}"><button type="button" class="btn btn-primary btn-sm"><span class="fa fa-history"></span> History</button></a>
                            </div>
							
							
							
							<!-- Rating post -->
							<tr>
							<input type="hidden" id="car_ID" name="car_id" value="{{$car->id}}">
							<input type="hidden" id="dealer_ID" name="dealer_id" value="{{$car->dealer->id}}">							
							<input type="hidden" id="rating" name="rating">
							</tr>
							
                        </div>
                        <br>
                    </div>
                </div>