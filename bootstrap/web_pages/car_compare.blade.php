
<?php $user_id= Auth::id();?>
<style>
.cxm-img-badge-cls a{
color:#fff;	

td:nth-child(1) {
background-color: #ccc;
}​
.border{
border:1px solid #158CBA;
box-shadow: 5px;
-webkit-box-shadow: 0px 0px 10px -1px rgba(0,0,0,0.53);
-moz-box-shadow: 0px 0px 10px -1px rgba(0,0,0,0.53);
box-shadow: 0px 0px 10px -1px rgba(0,0,0,0.53);	
}

h3{
font-size: 16px;

}
	
}

.message {
  animation: cssAnimation 0s 5s forwards;
  opacity: 1; 
}

@keyframes cssAnimation {
  to   { opacity: 0; }
}

</style>
@extends('layouts.web_pages')
@section('content') 

 <div class="header-margin py-4 bg-secondary">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <h1>Compare Vehicles</h1>            
          </div>
          <div class="col-sm-6">
            <div class="text-center text-sm-right mt-2">
            <a class="btn btn-primary" href="{{ route('search_car') }}"><span class="fa fa-plus-circle"></span> Add Vehicle</a>
			<a class="btn btn-primary" href="#"><span class="fa fa-save"></span> Save Comparision</a>            </div>
          </div>
        </div>
      </div>      
    </div>
	@if(!empty($all_car_datas) > 0)
	<div class="py-5">
      <div class="container">
	  
	   <div class="message" width="50%" align="center">
                    @if (session('message'))
                        <div class="alert alert-success" width="50%">
                            {{ session('message') }}
                        </div>
                    @endif
		      </div>
			  
		    <div class="message" width="50%" align="center">
                    @if (session('error'))
                        <div class="alert alert-danger" width="50%">
                            {{ session('error') }}
                        </div>
                    @endif
		      </div>	  
			  
	  
        <div class="row">
          <div class="col-sm-12">
            <div class="cxm-tabs">
              <nav>
                <ul class="nav nav-tabs d-md-none" id="nav-tab" role="tablist">
                  <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Option Menu</a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item active"  data-toggle="tab" href="#overview" role="tab">Summary</a>
                      <a class="dropdown-item" data-toggle="tab" href="#model" role="tab">Build</a>
                      <a class="dropdown-item" data-toggle="tab" href="#rating" role="tab">Features</a>
                      <a class="dropdown-item" data-toggle="tab" href="#reviews" role="tab">Safety</a>
                    </div>
                  </li>
  				</ul>
                <div class="d-none d-md-block">
                  <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" data-toggle="tab" href="#overview" role="tab">Summary</a>
                    <a class="nav-item nav-link" data-toggle="tab" href="#model" role="tab">Build</a>
                    <a class="nav-item nav-link" data-toggle="tab" href="#rating" role="tab">Features</a>
                    <a class="nav-item nav-link" data-toggle="tab" href="#reviews" role="tab">Safety</a>
                  </div>
                </div>
              </nav>
			  
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="overview" role="tabpanel">
				<p><h4>Popular Side-by-Side Car Comparisons</h4>
					We've grouped sets of the most popular cars on the road so you can see how they're similar and how they're different</p>
				<h3 align="center"> Your have selected {{count($all_car_datas)}} cars to comparisons.</h3>
                  
				  <div class="form-row">

				  <!-- START FOREACH -->
					
                    @foreach($all_car_datas as $no=> $data)
  
                    <div class="col-sm-6 col-md-4 col-lg-4">            
                      <div class="cxm-vihecle-box card border-secondary mb-3">
                      <div class="cxm-img">
					  <div class="cxm-img-badge-cls">
					  <a href="{{route('car_compare_delete',['car_id'=> $data->id])}}"><span class="fa fa-remove"></span></a>
					  </div>
     
					 <div class="cxm-img-badge position-absolute"></div>
                      
					  <a href="{{url('car/detail').'/'.$data->id}}"><img class="img-fluid" src="@if(!empty($data->media->photo_links[0]) > 0)
            			{{$data->media->photo_links[0]}}
            			@else
            			{{asset('public/images/no-image.jpeg')}}
						@endif">
					  </a>
					  
                     </div>	
                        <div class="text-center lh16">
						<b>	@if(!empty($data->heading) > 0)
							{{$data->heading}}
							@endif
						</b>
						</div>    
                        <div class="card-body p-2">                    
                          <hr class="hr1">
                          <div class="form-row">
                            <div class="col-6 mb-2 border-right">
                              <span class="badge badge-secondary">Price</span>
                              <div><span class="fc1 fs24">
								$ @if(!empty($data->price) > 0)
								{{number_format((float)$data->price)}}
								@endif
							  </span></div>
                            </div>
                            <div class="col-6 mb-2 text-right">
                             <span class="badge badge-secondary">MILES</span>
                             <div><span class="fc1 fs24">
							 @if(!empty($data->miles) > 0)
							 {{$data->miles}}
							 @endif
							 </span></div>
                            </div>
                          </div>
                          <div class="text-center"><a class="fs14 font-weight-bold" href="#"><span class="fa fa-money"></span> See Dealer Offer</a></div>
                          <hr class="hr1">                    
                          <div class="card mb-1">
                            <div class="card-header p-0"><a class="collapsed" data-toggle="collapse" href="#warranty<?php echo $no;?>" role="button" aria-expanded="false" aria-controls="warranty<?php echo $no;?>">Build</a></div>
                            <div class="collapse" id="warranty<?php echo $no;?>">
                            <?php foreach($data->build as $desc => $values){ ?>
							<div class="card-body p-2"><?php echo str_replace("_","  ","<b>".ucwords($desc)." : </b>");  echo  $values;?> </div>
							<?php } ?>
                            </div>
                          </div>
                          <div class="card mb-1">
                            <div class="card-header p-0"><a class="collapsed" data-toggle="collapse" href="#intirior<?php echo $no;?>" role="button" aria-expanded="false" aria-controls="intirior<?php echo $no;?>">Colors</a></div>
                            <div class="collapse" id="intirior<?php echo $no;?>">
                             <div class="card-body p-2">
							 @if(!empty($data->interior_color) > 0)
							  Interior Color:{{$data->interior_color}}
							 @endif
							 </div>  
							 <div class="card-body p-2">
							 @if(!empty($data->exterior_color) > 0)
							 Exterior Color:{{$data->exterior_color}}
							 @endif
							 </div>
					
                            </div>
                          </div>
                          <div class="card mb-1">
                            <div class="card-header p-0"><a class="collapsed" data-toggle="collapse" href="#extirior<?php echo $no;?>" role="button" aria-expanded="false" aria-controls="extirior<?php echo $no;?>">All Power Seats</a></div>
                            <div class="collapse" id="extirior<?php echo $no;?>">
                              <div class="card-body p-2">Extirior Feature</div>
                            </div>
                          </div>
                        </div>
                      </div>          
                    </div>
				 @endforeach
                   
				
				   
				
				
		<!-- END FOREACH --> 
				   
                  </div>
                                    
                </div>
                
				<div class="tab-pane fade" id="model" role="tabpanel">
                 <table width="100%">
				<tr>
				
				@foreach($all_car_datas as $no=> $data)
				<td valign="top">
				<p align="center"><b>{{$data->heading}}</b></p>
				<table width="98%">
                <?php foreach($data->build as $desc => $values){ ?>
				
				<tr width="100%" class="border">
				<td width="40%"><?php echo str_replace("_","  ","<b>".ucwords($desc)." : </b>");?></td>
				<td width="60%"><?php echo  $values;?></td>
				</tr>
				
				<?php } ?>
                </table>       
              
				
				
			   </td>
				@endforeach
				</tr>
				 
				 </table>
                </div>
                
				<div class="tab-pane fade" id="rating" role="tabpanel">
				   <table width="100%">
				<tr>
				
				@foreach($all_car_datas as $no=> $data)
				<td valign="top">
				<p align="left"><b>{{$data->heading}}</b></p>
				<table width="98%">
				@if(!empty($data->extra->features) > 0)
                <?php foreach($data->extra->features as  $values){ ?>
				
				<tr width="100%" class="border">
				<td width="60%"><?php echo  $values;?></td>
				</tr>
				
				<?php } ?>
				
				@endif
                </table>       
              
				
				
			   </td>
				@endforeach
				</tr>
				 
				 </table>
                </div>
				
                <div class="tab-pane fade" id="reviews" role="tabpanel">
				   <table width="100%">
				<tr>
				
				@foreach($all_car_datas as $no=> $data)
				<td valign="top">
				<p align="left"><b>{{$data->heading}}</b></p>
				<table width="98%">
				@if(!empty($data->extra->safety_f) > 0)
                <?php foreach($data->extra->safety_f as  $values){ ?>
				<tr width="100%" class="border">
				<td width="60%"><?php echo  $values;?></td>
				</tr>
				<?php } ?>
				@endif
                </table>       
              
				
				
			   </td>
				@endforeach
				</tr>
				 
				 </table>
                </div>
				
              </div>
            </div>
          </div>
        </div>                
      </div>      
    </div>
	
   @else
			
				   <br> <br>
				   <h2 align="center">Plase add vehicle for comparison</h2>
				    <br> <br>
  @endif 
	
@endsection 