<?php

use Illuminate\Support\Facades\Artisan;

if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
	    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::post('check/dealer/register', 'DealerController@check_dealer');

Route::get('/', 'HomeController@index')->name('home');

Route::get('/get-dealer.html', 'HomeController@get_dealers')->name('get_dealers');

Route::post('/token', 'TokenController@generate')->name('token-generate');

Route::get('delete_channel.html', 'chatController@delete_chat_channel')->name('delete_chat_channel');

Route::get(
    'conversation.html', ['as' => 'home', function () {
        return response()->view('client.conversation');
    }]
);



Route::get('/user/forgot/password/', 'ClientController@forgot_password_page')->name('forgot_password_page');
		
Route::post('/user/reset/password', 'ClientController@forgot_password')->name('forgot_password');
	
Route::post('/user/update/password', 'ClientController@update_password')->name('user_update_password');


// VDP & SRP SESSION ROUTES
Route::get('/vdp/session', 'HomeController@create_vdp_session')->name('vdp_session');
Route::get('/srp/session', 'HomeController@create_srp_session')->name('srp_session');


// SEARCH BUTTON SESSION
Route::get('/search/button/session', 'HomeController@create_search_button_session')->name('search_buttons_session');


///// web pages //

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/who-we-are.html', 'HomeController@who_we_are')->name('whoweare');
Route::get('/how-it-work.html', 'HomeController@how_it_work')->name('howitwork');
Route::get('/dealership-overview.html', 'HomeController@dealership')->name('dealership');
Route::get('/career.html', 'HomeController@career')->name('career');
Route::get('/faq.html', 'HomeController@faq')->name('faq');
Route::get('/contact-us.html', 'HomeController@contact_us')->name('contactus');
Route::post('/contact_us_submit.html', 'HomeController@contact_us_submit')->name('contact_us_submit');
Route::get('/contact_us_thankyou.html', 'HomeController@contact_us_thankyou')->name('contact_us_thankyou');
Route::get('/affiliate.html', 'HomeController@affiliate')->name('affiliate');
Route::post('/affiliate_submit.html', 'HomeController@affiliate_submit')->name('affiliate_submit');
Route::get('/affiliate_thankyou.html', 'HomeController@affiliate_thankyou')->name('affiliate_thankyou');

Route::get('search-car.html', 'HomeController@search_car')->name('search_car');
Route::get('search-option.html', 'HomeController@search_option')->name('search_option');
Route::get('faq-search.html', 'HomeController@quiz_list')->name('faq-search'); // TODO: not in use
Route::get('questioner-search.html', 'HomeController@quiz_show')->name('questioner-search'); // TODO: not in use

Route::post('user-check-activation.html', 'HomeController@check_user_activation')->name('check_user_activation');

Route::get('car/detail/{id}', 'HomeController@car_detail');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::post('/profile/update', 'HomeController@profile_submit');
Route::post('/profile/update_image', 'HomeController@profile_image_upload')->name('/profile/update_image');
Route::post('/profile/password_update', 'HomeController@profile_password_update')->name('/profile/password_update');
Route::any('/token',['uses' => 'TokenController@generate', 'as' => 'token-generate']);

///// validate ////
Route::post('login/validate', 'Auth\LoginController@login_validate');
Route::post('register/validate', 'Auth\RegisterController@register_validator');
Route::get('/confirm/registration/{token}', 'Auth\RegisterController@verifyUser');

//quiz search

Route::post('quiz-search-submit.html', 'HomeController@quiz_search_submit')->name('quiz_search');
Route::post('/check_route','HomeController@check_route');

Route::get('save/quiz/session', 'HomeController@quiz_session_submit')->name('save_quiz_session');

//dashboard
Route::get('/user-dashboard.html', 'ClientController@user_dashboard')->name('user-dashboard');

//social media login
Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');
Route::get('mychat.html', 'ClientController@my_chat')->name('mychat');
Route::get('my-chat-list.html', 'ClientController@my_chat_list')->name('chat_list');

Route::get('save-adverts.html', 'ClientController@saved_adverts')->name('save_adverts');
Route::get('save-reviews.html', 'ClientController@saved_reviews')->name('saved_reviews');
Route::get('save_searches.html', 'ClientController@save_searches')->name('save_searches');
Route::get('contact_request.html', 'ClientController@contact_request')->name('contact_request');
Route::get('my_account.html', 'ClientController@my_account')->name('my_account');
Route::get('my_history.html', 'ClientController@my_history')->name('my_history');
Route::post('/save/search/submit', 'ClientController@save_my_search')->name('save-my-search');
Route::post('save/car', 'ClientController@save_my_car');
Route::post('load/param', 'ClientController@load_param');
Route::post('load/param_search', 'ClientController@load_param_for_search');
Route::post('load/param_search_range', 'ClientController@load_param_for_search_range');
Route::post('load/facetsValues', 'ClientController@load_facets_values');
Route::post('load/param_checkbox', 'ClientController@load_param_checkbox');
Route::post('load/param_radiobox', 'ClientController@load_param_radiobox');
Route::post('load/param_dropdown', 'ClientController@load_param_dropdown');
Route::get('unblock-request.html', 'ClientController@ublock_request')->name('ublock_request');

Route::post('unblock_request_send.html', 'ClientController@ublock_request_send')->name('ublock_request_send');

//Save Searches Routes //
Route::post('/save/search/edit', 'ClientController@save_search_edit')->name('save_search_edit');
Route::get('save/searches/delete/{save_searches_id}', 'ClientController@save_searches_delete')->name('delete_save_search');
Route::get('save/searches/delete/all/{client_id}', 'ClientController@save_searches_delete_all')->name('delete_all_save_search');


// EMAIL BOX FROM USER SIDE
Route::get('user-email-box.html', 'ClientController@user_emailbox')->name('user_email_box');
Route::any('user-services-email.html', 'ClientController@user_services_email')->name('user_services_email');
Route::post('user-services-email-send.html', 'ClientController@user_services_email_send')->name('user_services_email_send');
Route::get('user-mail-read.html', 'ClientController@email_read_user')->name('message/read/user');
Route::post('user-mail-reply.html', 'ClientController@mail_reply_user')->name('message/reply/user');

//update radius
Route::post('services_edit_radius.html', 'ClientController@services_radius_update')->name('services/radius/update');


#delete emails
Route::post('user-temp-delete.html', 'ClientController@mail_temp_delete')->name('message/temp/delete');
Route::any('user-empty-trash.html', 'ClientController@empty_trash')->name('user/empty/trash');



#Questioner Search
Route::get('questioner-attempt-list.html', 'ClientController@user_questioner_list')->name('user_questioner_list');
Route::get('questioner-attempt-view.html', 'ClientController@user_questioner_view')->name('user_questioner_view');
Route::get('questioner-delete.html', 'ClientController@user_questioner_delete')->name('user_questioner_delete');


#SMS
Route::get('sms/send', 'SmsController@sms_compose')->name('SmsSend');
#SMS NOTIFICATION
Route::get('notification-send.html', 'SmsController@notification_send')->name('NotificationSend');


//save dealer
Route::get('delete/dealer/{dealer_id}', 'ClientController@delete_mydealer')->name('delete_my_dealer');
Route::get('save-dealer.html', 'ClientController@get_mydealer')->name('get_my_dealer');
Route::get('save/dealer/{dealer_id}', 'ClientController@save_mydealer')->name('save_my_dealer');

//Save Adverts Routes ///
Route::get('save/advert/delete/{favourite_id}', 'ClientController@save_advert_delete')->name('delete_save_advert');
Route::get('save/advert/delete/all/{client_id}', 'ClientController@save_advert_delete_all')->name('delete_all_save_advert');
Route::get('car/services', 'ClientController@car_service')->name('CarService');
Route::post('car/services/get_facets', 'ClientController@car_service_facets');
Route::post('save/compare/car', 'ClientController@car_compare');
Route::get('car/compare/page', 'ClientController@car_compare_page')->name('CarComparePage');
//Route::get('car/compare/{car_id}','ClientController@car_compare')->name('CarCompareInsert');
Route::get('car/compare/delete/{car_id}', 'ClientController@car_compare_delete')->name('car_compare_delete');
Route::get('car/rating/{car_id}', 'ClientController@car_rating')->name('CarRating');

// Reviews and rating
Route::post('save/rating', 'ClientController@rating_save')->name('SaveRating');
Route::post('save/reviews', 'ClientController@review_save')->name('SaveReview');
Route::post('save/reviews/ajax', 'ClientController@review_save_ajax')->name('SaveReviewAjax');
Route::post('save/reply/ajax', 'ClientController@review_reply_ajax')->name('SaveReplyAjax');
Route::post('update/reviews/ajax', 'ClientController@review_update_ajax')->name('UpdateReviewAjax');
Route::post('delete/reviews/ajax', 'ClientController@review_delete_ajax')->name('DeleteReviewAjax');
Route::post('block/reviews/ajax', 'ClientController@review_block_ajax')->name('BlockReviewAjax');
Route::post('unblock/reviews/ajax', 'ClientController@review_unblock_ajax')->name('UnBlockReviewAjax');
Route::post('save/reply/dealer/ajax', 'ClientController@review_reply_dealer_ajax')->name('SaveReplyDealerAjax');Route::post('report/message/ajax', 'ClientController@message_report_ajax')->name('ReportMessageAjax');

Route::get('car/history/{car_id}', 'ClientController@save_history')->name('CarHistorySave');
Route::get('car/history/delete/{car_id}', 'ClientController@delete_history')->name('CarHistoryDelete');

//Contact Request
Route::get('contact/request/{status}', 'ClientController@ContactReuqest')->name('ContactReuqest');
Route::get('contact/request/detail/{car_id}', 'ClientController@ContactReuqestDetail')->name('ContactReuqestDetail');

//Lease and purchase offers
Route::get('lease/offer/{status}', 'ClientController@LeasePurchase')->name('offer');
Route::get('cron-test', 'ClientController@sendEmail');


// REQUEST BEST OFFER 

Route::get('rbo/session', 'ClientController@set_rbo_session')->name('rbo_session');

Route::post('car/compare/delete/', 'RBOController@car_compare_delete')->name('car_compare_delete');
Route::get('request_best_offer.html', 'RBOController@rbo_send')->name('RequestBestOffer');
Route::get('request_best_offer_reject.html', 'RBOController@rbo_reject')->name('RequestBestOfferReject');
Route::get('user-rbo-saved.html', 'RBOController@get_user_rbo_list')->name('UserRboList');
Route::post('rbo/compare/car', 'RBOController@rbo_compare_send');
Route::get('rbo/compare/save', 'RBOController@rbo_compare_save')->name('rbo_compare_save');
Route::get('request-best-offer-quote-list.html', 'RBOController@rbo_quote_list')->name('rbo-quote-form-list');

//customer quote action
Route::get('request-best-offer-quote-customer-view.html', 'RBOController@rbo_quote_customer_view')->name('rbo-quote-form-customer-view');

Route::get('request-best-offer-quote-customer-details.html', 'RBOController@rbo_quote_customer_details')->name('rbo-quote-form-details');

Route::get('request-best-offer-quote-customer-accept.html', 'RBOController@rbo_quote_customer_status')->name('rbo-quote-form-customer-status');

Route::post('request-best-offer-quote-customer-submit.html', 'RBOController@rbo_quote_customer_submit')->name('rbo-quote-form-customer-submit');




// Car Services

Route::any('/save/services', 'ClientController@save_services')->name('SaveServices');
Route::any('/services/show/dealers', 'ClientController@services_show_dealers')->name('ServicesShowDeales');
Route::any('/clear/services', 'ClientController@clear_services')->name('ClearServices');
Route::any('/dealer_info', 'ClientController@dealer_info')->name('dealer_info');


// admin Route
Route::get('/admin', 'AdminController@admin_login')->name('login_page');
Route::get('dashboard.html', 'AdminController@dashboard')->name('dashboard');
Route::post('admin/login', 'AdminController@login')->name('admin_login');
Route::get('admin/logout', 'AdminController@logout')->name('admin_logout');

Route::get('admin/reviews/report', 'reviewsController@reviews_report')->name('reviews_report');
Route::get('reviews/report/list', 'reviewsController@reviews_report_table')->name('reviews_report_table');
Route::post('reviews/user/block', 'reviewsController@reviews_user_block')->name('reviews_user_block');


Route::post('chat/user/block', 'chatController@chat_user_block')->name('chat_user_block');
Route::get('admin/chat/block', 'chatController@chat_block')->name('chat_block');
Route::get('chat/block/list', 'chatController@chat_block_table')->name('chat_block_table');




//Request unblock
Route::get('request/unblock/list', 'AdminController@request_ublock_list')->name('request_unblock_list');
Route::get('request/unblock/table', 'AdminController@request_ublock_table')->name('request_unblock_table');




// ROLL ROUTES
Route::get('/roles/list', 'RoleController@role_list')->name('roles_list');
Route::get('/roles/list/table', 'RoleController@role_list_table')->name('roles_list_table');
Route::get('/roles/create', 'RoleController@create')->name('create_role');
Route::post('/roles/store', 'RoleController@store');
Route::get('/roles/edit/{role_id}', 'RoleController@edit')->name('roles/edit/');
Route::post('/roles/update', 'RoleController@update');
Route::get('/roles/remove/{role_id}', 'RoleController@remove')->name('roles/remove/');

// USERS ROUTES
Route::get('/user/list', 'UserController@list_user')->name('user_list');
Route::get('/user/list/table', 'UserController@list_user_table')->name('user_list_table');
Route::get('/user/create', 'UserController@create')->name('create_user');
Route::post('/user/store', 'UserController@store');
Route::get('/user/edit/{user_id}', 'UserController@edit')->name('user/edit/');
Route::post('/user/update', 'UserController@update');
Route::get('/user/remove/{user_id}', 'UserController@remove')->name('user/remove/');
Route::get('/client/list', 'UserController@list_client')->name('client_list');
Route::get('/client/list/table', 'UserController@client_list_table')->name('client_list_table');
Route::get('/client/create', 'UserController@create')->name('create_client');
Route::post('/client/store', 'UserController@store');
Route::get('/client/edit/{user_id}', 'UserController@client_edit')->name('client/edit/');
Route::post('/client/update', 'UserController@client_update');
Route::get('/client/remove/{user_id}', 'UserController@client_remove')->name('client/remove/');
Route::get('/client/detail/{user_id}', 'AdminController@client_detail')->name('client/detail/');
Route::get('/client/unblock/{user_id}', 'AdminController@client_unblock')->name('client/unblock/');
Route::get('/client/block/{user_id}', 'AdminController@client_block')->name('client/block/');


//DEALER
Route::get('dealer-list.html', 'AdminController@dealer_list')->name('dealer-list');
Route::get('dealer/list/table', 'AdminController@dealer_table')->name('dealer_list_table');

Route::get('/dealer/account/block/{dealer_id}', 'AdminController@dealer_account_block')->name('dealer/account/block/');
Route::get('/dealer/account/unblock/{dealer_id}', 'AdminController@dealer_account_unblock')->name('dealer/account/unblock/');
// GET DEALER DIRECT FROM API
Route::get('dealer-list-api.html', 'AdminController@dealer_list_api')->name('dealer-list-api');
Route::get('dealer/list/table/api', 'AdminController@dealer_table_api')->name('dealer_table_api');

Route::get('/dealer/detail/{dealer_id}', 'AdminController@dealer_detail')->name('dealer/detail/');
Route::get('/dealer/remove/{dealer_id}', 'AdminController@dealer_remove')->name('dealer/remove/');
Route::get('/dealer/api/remove/{dealer_id}', 'AdminController@dealer_api_remove')->name('dealer/api/remove/');

//DEALER ACTIVITY
Route::get('/dealer/activity', 'AdminController@dealer_activity')->name('dealer-activity-list');
Route::get('dealer/activity/table', 'AdminController@dealer_activity_table')->name('dealer_activity_list_table');
Route::get('/dealer/activity/detail/{dealer_id}', 'AdminController@dealer_activity_detail')->name('dealer/activity/detail/');
Route::get('/client/activity/detail/', 'AdminController@client_lp_list')->name('client_lp_list');
Route::get('/client/activity/detail/', 'AdminController@client_lp_list')->name('client_lp_list');
Route::get('/client/offers/detail/', 'AdminController@client_lp_detail')->name('client_lp_detail');
Route::get('/dealer/unblock/{dealer_id}', 'AdminController@dealer_unblock')->name('dealer/unblock/');
Route::get('/dealer/block/{dealer_id}', 'AdminController@dealer_block')->name('dealer/block/');
Route::get('/subscription/list', 'subscriptionController@list_subscription')->name('subscription_list');
Route::get('/subscription/list/table', 'subscriptionController@list_subscription_table')->name('subscription_list_table');
Route::get('/subscription/create', 'subscriptionController@create')->name('create_subscription');
Route::post('/subscription/store', 'subscriptionController@store');
Route::get('/subscription/edit/{subscription_id}', 'subscriptionController@edit')->name('subscription/edit/');
Route::post('/subscription/update', 'subscriptionController@update');
Route::get('/subscription/remove/{subscription_id}', 'subscriptionController@remove')->name('subscription/remove/');



Route::get('/dealer/edit/{dealer_id}', 'AdminController@dealer_edit')->name('dealer/edit/');

Route::get('/dealer/api/edit/{dealer_id}', 'AdminController@dealer_api_edit')->name('dealer/api/edit/');


Route::post('dealer/update', 'AdminController@dealer_update')->name('dealer/update/');

Route::post('dealer/api/update', 'AdminController@dealer_api_update')->name('dealer/api/update/');



// questioner  // TODO: not in use - replace by Quiz Search
//Route::get('/question/list', 'questionController@list_question')->name('question_list');
//Route::get('/questions/list/table', 'questionController@list_question_table')->name('questions_list_table');
//Route::get('/question/create', 'questionController@create')->name('create_questions');
//Route::post('/questions/store', 'questionController@store');
//Route::get('/question/edit/{question_id}', 'questionController@edit')->name('question/edit/');
//Route::post('/question/update', 'questionController@update');
//Route::get('/question/remove/{question_id}', 'questionController@remove')->name('question/remove/');


Route::post('get/lat/long', 'ClientController@get_lat_long');
Route::post('get/auto/location', 'ClientController@get_auto_location');
Route::get('remove/tracking/location', 'ClientController@remove_location');

// Admin Quiz Search
	Route::middleware([\App\Http\Middleware\AdminAuth::class])->group(function () {
	Route::get('/quiz/{id}/preview', 'QuizController@preview_quiz');
	Route::get('/quiz/list', 'QuizController@list_quiz');
	Route::post('/quiz/add', 'QuizController@add_quiz')->name('/quiz/add');
	Route::get('/quiz/{id}', 'QuizController@create_quiz');
	Route::post('/save_quiz', 'QuizController@save_quiz');
	Route::post('/save_question_set', 'QuizController@save_question_set');
	Route::post('/save_quiz_routes', 'QuizController@save_quiz_routes');
    Route::get('/quiz/{id}/edit', 'QuizController@edit_quiz');
    Route::get('/quiz/{id}/delete', 'QuizController@delete_quiz');
    Route::post('/publish_quiz', 'QuizController@publish_quiz');
    Route::post('/upload_file', 'QuizController@upload_file');
    Route::post('/image/store','QuizController@save_images');
    Route::get('/image/list','QuizController@list_images');
    Route::get('/question_sets','QuizController@list_question_sets');
    Route::get('/all_question_sets','QuizController@list_all_question_sets');
    Route::post('/image/label','QuizController@save_image_label');
    Route::post('/quiz/welcome','QuizController@update_quiz_welcome_status');
    Route::post('/quiz/save_data','QuizController@update_quiz_data');




});

	// dealer //
	Route::get('dealer/signup', 'DealerController@dealer_signup')->name('dealer_signup');
	Route::get('dealer/terms', 'DealerController@dealer_terms')->name('dealer_terms');
	Route::post('/dealer/register/validate', 'DealerController@signup_validator');
	Route::post('/create/dealer', 'DealerController@save_dealer')->name('create_dealer');
	Route::get('verify/dealer/{id}', 'DealerController@verify_dealer')->name('verify_dealer');

	Route::get('dealer/login', 'DealerController@dealer_login')->name('dealer_login');
	
	Route::post('/dealer/login/validate', 'DealerController@login_validator');
	
	Route::post('/dealer/login/submit', 'DealerController@login_submit')->name('login_submit');
	
	Route::get('/dealer/forgot/password/', 'DealerController@forgot_password_page')->name('forgot_password_page');
		
	Route::post('/dealer/reset/password', 'DealerController@forgot_password')->name('forgot_password');
	
	Route::post('/dealer/update/password', 'DealerController@update_password')->name('update_password');
	
	Route::get('dealer/logout', 'DealerController@dealer_logout')->name('dealer_logout');

	##START DEALER ROUTES+

	Route::middleware([\App\Http\Middleware\DealerAuth::class])->group(function () {
	// dashboard
	Route::get('dealer/portal', 'Dealer\DealerDashboardController@dealer_portal')->name('dealer_portal');
	Route::get('get_dealer_stats.html', 'Dealer\DealerDashboardController@get_dealer_stats')->name('get_dealer_stats');

	Route::get('sub-dealer-form.html', 'DealerController@subdealer_form')->name('subdealer-form');
    Route::post('/subdealer/register/validate', 'DealerController@subdealer_signup_validator');
	Route::post('/create/subdealer', 'DealerController@save_subdealer')->name('create_subdealer');
    Route::get('/subdealer/list', 'DealerController@subdealer_list')->name('subdealer-list');
    Route::get('/subdealer-accounts-table.html', 'DealerController@subdealer_list_table')->name('subdealer-list-table');

	//EDIT DELETE SUB DEALER

	Route::get('/subdealer-delete.html', 'DealerController@subdealer_delete')->name('subdealer-delete');
    Route::get('/subdealer-edit.html', 'DealerController@subdealer_edit')->name('subdealer-edit');
	Route::post('/subdealer/edit/validate', 'DealerController@subdealer_edit_validator');
	Route::post('/subdealer/update', 'DealerController@subdealer_update')->name('update_subdealer');;



	//billing
	Route::get('dealer-billing.html', 'DealerController@dealer_billing_tool')->name('dealer-billing');
	Route::post('add_addon_subscriptions', 'DealerController@add_addon_subscriptions')->name('add_addon_subscriptions');
	Route::post('remove_addon_subscriptions', 'DealerController@remove_addon_subscriptions')->name('remove_addon_subscriptions');
	Route::post('invoice_url', 'DealerController@invoice_url')->name('invoice_url');

	// pricing tool
	Route::get('dealer-pricing.html', 'DealerController@dealer_pricing_tool')->name('dealer-pricing');
	Route::post('dealer-update-car-price-deal', 'DealerController@dealer_update_car_price_deal')->name('dealer-update-car-price-deal');
	Route::post('dealer-update-car-price-deal-addon-log', 'DealerController@dealer_update_car_price_deal_addon_log')->name('dealer-update-car-price-deal-addon-log');

	// reviews
	Route::get('dealer-reviews.html', 'DealerController@dealer_reviews')->name('dealer-reviews');
	Route::get('user_reviews_table.html', 'DealerController@user_reviews_table')->name('user_reviews_table');
	Route::post('review/respond', 'DealerController@respond_save')->name('review/respond');
	Route::get('review/details/{id}', 'ClientController@review_details')->name('review/details');

	// lead stats
	Route::get('leeds-stats.html', 'Dealer\Leeds@leeds_stats')->name('leeds-stats-page');
	Route::get('leeds-stats_table.html', 'Dealer\Leeds@leeds_stats_table')->name('leeds-stats-table');
	Route::get('leeds-phone-stats-table.html', 'Dealer\Leeds@leeds_stats_phone_table')->name('leeds-phone-stats-table');
	Route::get('leeds-sms-stats-table.html', 'Dealer\Leeds@leeds_stats_sms_table')->name('leeds-sms-stats-table');
	Route::get('leeds-chat-stats-table.html', 'Dealer\Leeds@leeds_stats_chat_table')->name('leeds-chat-stats-table');

	// RBO DEALER SIDE
	  Route::get('request-best-offer-list.html', 'Dealer\Rbosystem@request_best_offer_list')->name('request-best-offer-list');
      Route::get('request-best-offer-table.html', 'Dealer\Rbosystem@request_best_offer_table')->name('request-best-offer-table');
	   
	  Route::get('request-best-offer-legal-page.html', 'Dealer\Rbosystem@rbo_legal_page')->name('request-best-offer-legal-page');		
	
	  Route::get('request-best-offer-rejected.html', 'Dealer\Rbosystem@rbo_rejected')->name('rbo-rejected');

      Route::get('request-best-offer-accepted.html', 'Dealer\Rbosystem@rbo_accepted')->name('rbo-accepted');

	  Route::get('request-best-offer-quote-created.html', 'Dealer\Rbosystem@quote_create')->name('rbo-quote-create');

	  Route::get('request-best-offer-edit.html', 'Dealer\Rbosystem@rbo_quoteform_edit')->name('rbo-edit');

	  Route::post('request-best-offer-update.html', 'Dealer\Rbosystem@rbo_quoteform_update')->name('rbo-quote-form-update');


	 // RBO QUOTE FORM

	 Route::post('request-best-offer-quote.html', 'Dealer\Rbosystem@rbo_quoteform_submit')->name('rbo-quote-form-submit');

	 Route::get('request-best-offer-quote-view.html', 'Dealer\Rbosystem@rbo_quoteform_view')->name('rbo-quote-form-view');

	 Route::get('rbo-quote-form-dealer-status.html', 'Dealer\Rbosystem@rbo_quoteform_dealer_status')->name('rbo-quote-form-dealer-status');





	//criagslist
	Route::get('criagslist.html', 'Dealer\Criagslist@criagslist')->name('criagslist');
	Route::get('dealer-badges.html', 'Dealer\Badges@badges_page')->name('dealer-badges');

	//lease purchase
	Route::get('lease-purchase-offer.html', 'Dealer\Leasepurchase@lease_purchase')->name('lease-purchase');
	Route::get('lease-purchase-table.html', 'Dealer\Leasepurchase@lease_purchase_table')->name('leeds-purchase-table');

	// settings
	Route::get('dealer-settings.html', 'Dealer\DealerSettingController@dealer_settings')->name('dealer-settings');
	Route::post('dealer-settings-update-phone', 'Dealer\DealerSettingController@update_phone')->name('dealer-settings-update-phone');
	Route::post('dealer-settings-update-website', 'Dealer\DealerSettingController@update_website')->name('dealer-settings-update-website');
	Route::post('dealer-settings-update-email', 'Dealer\DealerSettingController@update_email')->name('dealer-settings-update-email');
	Route::post('dealer-settings-add-email', 'Dealer\DealerSettingController@add_email')->name('dealer-settings-add-email');
	Route::post('dealer-settings-add-phone', 'Dealer\DealerSettingController@add_phone')->name('dealer-settings-add-phone');

	Route::post('dealer-information-update.html', 'Dealer\DealerSettingController@update_company_info')->name('dealer-information-update');
	Route::post('dealer-email-update.html', 'Dealer\DealerSettingController@update_other_email')->name('dealer-email-update');

	Route::post('delete_other_emails.html', 'Dealer\DealerSettingController@delete_other_emails')->name('delete_other_emails');
	Route::post('delete_phone.html', 'Dealer\DealerSettingController@delete_phone')->name('delete_phone');

	// market analysis
	Route::get('market-analysis.html', 'Dealer\DealerMarketAnalysisController@market_analysis')->name('market-analysis');
	Route::get('market-analysis-top-searches-table', 'Dealer\DealerMarketAnalysisController@top_searches_table')->name('market-analysis-top-searches-table');
	Route::get('market-analysis-in-demand-table', 'Dealer\DealerMarketAnalysisController@in_demand_table')->name('market-analysis-in-demand-table');

   //Radius
   Route::post('dealer_add_radius.html', 'Dealer\DealerSettingController@dealer_add_radius')->name('dealer_add_radius');
	
   // Chat Communication channel report to admin.
   
   Route::any('chat-channel-report.html', 'chatController@report_channel')->name('report_channels');


   //mailbox
   Route::get('dealer-mail-box.html', 'Dealer\Mailbox@mail_box_page')->name('dealer-mail-box'); 
   Route::get('dealer-compose-email.html', 'Dealer\Mailbox@compose_email_page')->name('dealer-compose-mail');
   Route::post('dealer-email-send.html', 'Dealer\Mailbox@email_send')->name('dealer-email-send');
   Route::get('dealer-mail-list.html', 'Dealer\Mailbox@mail_box_table')->name('dealer-mail-list');
   Route::get('dealer-mail-read.html', 'Dealer\Mailbox@mail_read')->name('message/read');
   
   Route::post('dealer-mail-reply.html', 'Dealer\Mailbox@mail_reply_dealer')->name('message/reply/dealer');
   Route::post('dealer-temp-delete.html', 'Dealer\Mailbox@mail_temp_delete')->name('message/temp/delete');
   Route::any('dealer-empty-trash.html', 'Dealer\Mailbox@empty_trash')->name('dealer/empty/trash');
   
   Route::get('chat-communication.html', 'Dealer\Mailbox@chat')->name('dealer-chat');


});
