<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('loginApi', 'Auth\LoginController@loginApi');

Route::post('register', 'Api\RegisterDealer@CreateDealer');

Route::post('login', 'Api\RegisterDealer@LoginDealer')->name('login');

Route::post('forget/password', 'Api\RegisterDealer@forgetPassword');

Route::post('DealerSearchCar', 'Api\RegisterDealer@DealerSearchCar');

Route::post('GetCarDetail', 'Api\RegisterDealer@CarDetail');

Route::post('DealerProfile', 'Api\RegisterDealer@DealerDetail');

Route::post('DealerProfileUpdate', 'Api\RegisterDealer@DealerProfileUpdate');

Route::post('MarkasSold', 'Api\RegisterDealer@MarkasSold');

Route::post('MarkasInactive', 'Api\RegisterDealer@MarkasInactive');

Route::post('MarkasActive', 'Api\RegisterDealer@MarkasActive');

Route::post('MarkasFeature', 'Api\RegisterDealer@Markasfeatured');

Route::post('subscription/packages', 'Api\RegisterDealer@subscription_package_list');

Route::post('buy/subscription', 'Api\RegisterDealer@buy_subscription');

Route::post('my/subscription', 'Api\RegisterDealer@my_subscription');

Route::post('renew/subscription', 'Api\RegisterDealer@my_subscription_renew');

Route::post('view/live/advertising', 'Api\RegisterDealer@view_live_advertising');

Route::post('ads/status', 'Api\RegisterDealer@dealer_ads_status');

Route::post('offers', 'Api\RegisterDealer@Offers');

Route::post('lease/purchase', 'Api\RegisterDealer@LeasePurchase');

Route::post('lease/purchase/by/client', 'Api\RegisterDealer@LeasePurchase_byclient');

Route::post('contact/request', 'Api\RegisterDealer@ContactRequest');

Route::post('my/services', 'Api\RegisterDealer@MyServices');

Route::post('forget/password/update', 'Api\RegisterDealer@forgetPasswordUpdate');

Route::post('webhooks', 'Api\ChargebeeWebhook@chargebeeWebhooks');

Route::group(['middleware' => 'auth:api'], function() {

});

